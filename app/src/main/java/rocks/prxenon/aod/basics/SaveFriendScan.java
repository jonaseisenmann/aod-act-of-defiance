package rocks.prxenon.aod.basics;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.muddzdev.styleabletoast.StyleableToast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import rocks.prxenon.aod.BuildConfig;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.billing.Donation;
import rocks.prxenon.aod.pages.profile.ProfileMain;

import static rocks.prxenon.aod.basics.AODConfig.database;
import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mbfriendsrequest;
import static rocks.prxenon.aod.basics.AODConfig.mbpasscodes;
import static rocks.prxenon.aod.basics.AODConfig.mdbroot;
import static rocks.prxenon.aod.basics.AODConfig.user;

/**
 * Created by PrXenon on 12.09.2017.
 */

public class SaveFriendScan {

    // INIT DATABASE
    public SaveFriendScan() {
        if (database == null) {
            database = FirebaseDatabase.getInstance();
        }
    }

    // SAVE AGENT AFTER SCAN AND PRESS SAVE
    public void saveScannedAgent(final Activity axt, final Context ctx, final String otheremail) {
        final Map<String, Object> childUpdates = new HashMap<>();
        // System.out.println("LOG GET DATA-->" + qrid + " 2 " + owner + " location" + lat);  //prints "Do you have data? You'll love Firebase."


        mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (!snapshot.child(otheremail).exists()) {

                    childUpdates.put("/timestamp", System.currentTimeMillis() / 1000);
                    childUpdates.put("/agentuid", otheremail);
                    childUpdates.put("/status", true);
                    childUpdates.put("/connected", "QR");

                    mDatabase.child(user.getUid()).child("friends").child(otheremail).updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();
                                new ProfileMain().scanedone(true, axt, ctx);
                            } else if (task.isCanceled()) {
                                new ProfileMain().scanedone(false, axt, ctx);
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            new ProfileMain().scanedone(false, axt, ctx);
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                            //signOut();
                        }
                    });
                    //  mDatabase.child(md5(email)).child("scrapbook").child(md5(otheremail)).child(randid).child("lng").setValue(lng);

                } else {
                    new ProfileMain().alreadyFriends(true, axt, ctx);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                new ProfileMain().scanedone(false, axt, ctx);
            }
        });


        // qrdatabase.child(qrid).child("scanedagnet").setValue("-");

    }


    // SAVE AGENT AFTER SCAN AND PRESS SAVE
    public void saveScannedAgentInviteExist(final Activity axt, final Context ctx, final String otheremail) {
        final Map<String, Object> updaterunning = new HashMap<>();

        updaterunning.put("/status", true);


        mDatabase.child(otheremail).child("friends").child(user.getUid()).updateChildren(updaterunning).addOnCompleteListener(new OnCompleteListener<Void>() {


            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mbfriendsrequest.child(user.getUid()).child(otheremail).setValue(null);
                    mbfriendsrequest.child(otheremail).child(user.getUid()).setValue(null);
                    final Map<String, Object> childUpdates2 = new HashMap<>();
                    childUpdates2.put("/timestamp", System.currentTimeMillis() / 1000);
                    childUpdates2.put("/agentuid", otheremail);
                    childUpdates2.put("/status", true);
                    childUpdates2.put("/connected", "Code");

                    mDatabase.child(user.getUid()).child("friends").child(otheremail).updateChildren(childUpdates2).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                StyleableToast.makeText(ctx, "Successfully connected", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                String pushkey = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

                                mDatabase.child(user.getUid()).child("pushkey").setValue(pushkey);
                                new SendPushinBgacceptedboath(pushkey, "accepted", AgentData.getagentname(), otheremail).execute();


                                final Map<String, Object> agentinbox = new HashMap<>();


                                agentinbox.put("/text", AgentData.getagentname());


                                agentinbox.put("/title", "new friend");
                                agentinbox.put("/timestamp", System.currentTimeMillis());
                                agentinbox.put("/read", false);


                                mdbroot.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshotexist) {
                                        if (snapshotexist.hasChild(AgentData.getagentname())) {
                                            agentinbox.put("/fromuid", snapshotexist.child(AgentData.getagentname()).getValue().toString());
                                            mDatabase.child(otheremail).child("newinbox").child(String.valueOf(System.currentTimeMillis())).updateChildren(agentinbox);
                                        } else {
                                            Log.i("Error DB -->", " 407 ");
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        // StyleableToast.makeText(getApplicationContext(), "Error 526", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                        Log.i("Error DB -->", "889");
                                    }
                                });
                                //  final TextView friend_withdraw = knopfView.findViewById(R.id.friend_withdraw);


                            } else if (task.isCanceled()) {

                                Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {

                            Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                            //signOut();
                        }
                    });


                } else if (task.isCanceled()) {
                    StyleableToast.makeText(ctx, "Error, try again", Toast.LENGTH_LONG, R.style.defaulttoastb).show();


                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1055 Login abgebrochen");

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                StyleableToast.makeText(ctx, "Error, something wrong", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

            }

        });


    }


    // SAVE AGENT AFTER SCAN AND PRESS SAVE
    public void saveManuellAgent(final Activity axt, final Context ctx, final String otheremail) {
        final Map<String, Object> childUpdates = new HashMap<>();
        // System.out.println("LOG GET DATA-->" + qrid + " 2 " + owner + " location" + lat);  //prints "Do you have data? You'll love Firebase."


        mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (!snapshot.child(otheremail).exists()) {

                    mbfriendsrequest.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot snapshotinvites) {
                            //snapshot.child("name").getValue();
                            // SetLoginName(snapshot.child("name").getValue().toString());
                            if (snapshotinvites.child(otheremail).exists()) {

                                saveScannedAgentInviteExist(axt, ctx, otheremail);

                            }
                            // NO REQUEST EXIST
                            else {
                                final Map<String, Object> childUpdates2 = new HashMap<>();
                                childUpdates.put("/timestamp", System.currentTimeMillis());
                                childUpdates.put("/agentuid", otheremail);
                                childUpdates.put("/status", false);
                                childUpdates.put("/connected", "Code");

                                mDatabase.child(user.getUid()).child("friends").child(otheremail).updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            childUpdates2.put("/timestamp", System.currentTimeMillis());
                                            childUpdates2.put("/agentuid", user.getUid());
                                            mbfriendsrequest.child(otheremail).child(user.getUid()).updateChildren(childUpdates2).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        //finish();

                                                        new ProfileMain().scanedone(true, axt, ctx);
                                                    } else if (task.isCanceled()) {
                                                        new ProfileMain().scanedone(false, axt, ctx);
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    new ProfileMain().scanedone(false, axt, ctx);
                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                                                    //signOut();
                                                }
                                            });
                                        } else if (task.isCanceled()) {
                                            new ProfileMain().scanedone(false, axt, ctx);
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        new ProfileMain().scanedone(false, axt, ctx);
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                                        //signOut();
                                    }
                                });
                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                    // REQUEST FROM OTHER EXIST ALSO


                    //  mDatabase.child(md5(email)).child("scrapbook").child(md5(otheremail)).child(randid).child("lng").setValue(lng);

                } else {
                    // STILL FRIENDS
                    if (Boolean.parseBoolean(snapshot.child(otheremail).child("status").getValue().toString())) {
                        new ProfileMain().alreadyFriends(true, axt, ctx);
                    }
                    // FRIENDS REQUEST SEND
                    else if (!Boolean.parseBoolean(snapshot.child(otheremail).child("status").getValue().toString())) {
                        new ProfileMain().alreadyFriendssend(true, axt, ctx);
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                new ProfileMain().scanedone(false, axt, ctx);
            }
        });


        // qrdatabase.child(qrid).child("scanedagnet").setValue("-");

    }


    // SAVE AGENT AFTER SCAN AND PRESS SAVE
    public void redeemPasscode(final Activity axt, final Context ctx, final String aodpasscode, final String niapasscode) {
        final Map<String, Object> childUpdates = new HashMap<>();
        // System.out.println("LOG GET DATA-->" + qrid + " 2 " + owner + " location" + lat);  //prints "Do you have data? You'll love Firebase."

        childUpdates.put("/timestamp", System.currentTimeMillis() / 1000);
        childUpdates.put("/agentuid", user.getUid());
        childUpdates.put("/redeemed", true);


        final Map<String, Object> childUpdates2 = new HashMap<>();
        mbpasscodes.child(aodpasscode).updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    //finish();
                    childUpdates2.put("/badge", aodpasscode);
                    childUpdates2.put("/passcode", niapasscode);

                    mDatabase.child(user.getUid()).child("badges").child(aodpasscode).updateChildren(childUpdates2).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();

                                new ProfileMain().newpasscode(true, axt, ctx, aodpasscode);
                            } else if (task.isCanceled()) {
                                new ProfileMain().newpasscode(false, axt, ctx, "null");
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            new ProfileMain().newpasscode(false, axt, ctx, "null");
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                            //signOut();
                        }
                    });
                } else if (task.isCanceled()) {
                    new ProfileMain().newpasscode(false, axt, ctx, "null");
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                new ProfileMain().newpasscode(false, axt, ctx, "null");
                Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                //signOut();
            }
        });


        // qrdatabase.child(qrid).child("scanedagnet").setValue("-");

    }


    // SAVE AGENT AFTER SCAN AND PRESS SAVE
    public void redeemBadge(final Activity axt, final Context ctx, final String badgename, final String text) {
        final Map<String, Object> childUpdates = new HashMap<>();
        // System.out.println("LOG GET DATA-->" + qrid + " 2 " + owner + " location" + lat);  //prints "Do you have data? You'll love Firebase."

        childUpdates.put("/timestamp", System.currentTimeMillis() / 1000);
        childUpdates.put("/agentuid", user.getUid());
        childUpdates.put("/redeemed", true);


        final Map<String, Object> childUpdates2 = new HashMap<>();
        childUpdates2.put("/badge", badgename);
        childUpdates2.put("/text", text);

        mDatabase.child(user.getUid()).child("badges").child(badgename).updateChildren(childUpdates2).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    //finish();

                    new Donation().newbadge(true, axt, ctx, badgename);
                } else if (task.isCanceled()) {
                    new Donation().newbadge(false, axt, ctx, "null");
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                new Donation().newbadge(false, axt, ctx, "null");
                Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                //signOut();
            }
        });


        // qrdatabase.child(qrid).child("scanedagnet").setValue("-");

    }


    // SAVE AGENT AFTER SCAN AND PRESS SAVE
    public void removeScannedAgent(final Activity axt, final Context ctx, final String otheremail) {

        // System.out.println("LOG GET DATA-->" + qrid + " 2 " + owner + " location" + lat);  //prints "Do you have data? You'll love Firebase."


        mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.child(otheremail).exists()) {


                    mDatabase.child(user.getUid()).child("friends").child(otheremail).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();

                                mDatabase.child(otheremail).child("friends").child(user.getUid()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            new ProfileMain().scanedone(true, axt, ctx);
                                        } else if (task.isCanceled()) {
                                            new ProfileMain().scanedone(false, axt, ctx);
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        new ProfileMain().scanedone(false, axt, ctx);
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                                        //signOut();
                                    }
                                });
                            } else if (task.isCanceled()) {
                                new ProfileMain().scanedone(false, axt, ctx);
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            new ProfileMain().scanedone(false, axt, ctx);
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                            //signOut();
                        }
                    });
                    //  mDatabase.child(md5(email)).child("scrapbook").child(md5(otheremail)).child(randid).child("lng").setValue(lng);

                } else {
                    new ProfileMain().alreadyFriends(false, axt, ctx);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                new ProfileMain().scanedone(false, axt, ctx);
            }
        });


        // qrdatabase.child(qrid).child("scanedagnet").setValue("-");

    }

    //GET AGENTS DETAILS WITH TOKEN
    public class SendPushinBgacceptedboath extends AsyncTask<String, Void, String> {
        //sendpushaccepted
        String thepushid, theart, theagent, sendtoa;

        public SendPushinBgacceptedboath(String s, String art, String agentname, String sendTo) {
            thepushid = s;
            theart = art;
            theagent = agentname;
            sendtoa = sendTo;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {


            String regurl = BuildConfig.BASEURL + "sendpushaccepted.php?pw=" + thepushid + "&art=" + theart + "&agent=" + theagent + "&uid=" + user.getUid() + "&to=" + sendtoa;


            StringBuilder sb = null;
            BufferedReader reader = null;
            String serverResponse = null;
            try {

                URL url = new URL(regurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(200);
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                //Log.e("statusCode", "" + statusCode);
                if (statusCode == 200) {
                    sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                }

                connection.disconnect();
                if (sb != null)
                    serverResponse = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return serverResponse;
        }

        @Override
        protected void onPostExecute(String json) {

            if (json != null) {


            }
        }
    }
}

