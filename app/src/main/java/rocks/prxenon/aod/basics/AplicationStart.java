package rocks.prxenon.aod.basics;


import android.content.Context;


import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;


public class AplicationStart extends android.app.Application {
    public static FirebaseRemoteConfig remoteconfig;


    @Override
    public void onCreate() {
        super.onCreate();



        /* Enable disk persistence  */
        //  FirebaseDatabase.getInstance();


        remoteconfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(30)
                // .setMinimumFetchIntervalInSeconds(3600)
                .build();
        remoteconfig.setConfigSettingsAsync(configSettings);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // MultiDex.install(this);
    }

}