package rocks.prxenon.aod.basics;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import rocks.prxenon.aod.receiver.NetworkStateReceiver;

import static android.content.Context.MODE_PRIVATE;

public class AODConfig {
    public static FirebaseDatabase database;
    public static FirebaseUser user;
    public static com.google.firebase.messaging.FirebaseMessaging fmessaging = FirebaseMessaging.getInstance();
    public static SharedPreferences pref;
    public static DatabaseReference mDatabase, mhackvent, mDatabasemissions, mDatabaseaod, mBevents, mDatabaserunnung, mdbroot, mDatabaseagentCodes, qrdatabase, mbfriendsrequest, mbgdpr, mbpvp, mbpasscodes, mDatabaserunnungpvp, mbpvplinks;
    public static NetworkStateReceiver networkStateReceiver;
    public static String originalagent, scanneragent;
    public static boolean scanisopen, qrisscaned;
    public static ValueEventListener listenertempscansave;
    public static ChildEventListener qrsavelistener;
    public static String child1 = "agentname";
    public static String child2 = "faction";
    public static String child3 = "googleid";
    public static String child4 = "profileimage";
    public static FirebaseAnalytics mFirebaseAnalytics;
    private static Context ctxx;


    public AODConfig() {

        if (database == null) {
            database = FirebaseDatabase.getInstance();
            database.setPersistenceEnabled(true);
            database.goOnline();
        }
        mDatabase = database.getReference("users");
        mDatabasemissions = database.getReference("missions");
        mbpvp = database.getReference("pvp");
        mhackvent = database.getReference("hackvent");
        mdbroot = database.getReference("root");
        mDatabaseaod = database.getReference("aod");
        mDatabaserunnung = database.getReference("running");
        mDatabaserunnungpvp = database.getReference("runningpvp");
        mDatabaseagentCodes = database.getReference("agentcodes");
        qrdatabase = database.getReference("tempqr");
        mbfriendsrequest = database.getReference("friendrequests");
        mbgdpr = database.getReference("gdpr");
        mbpasscodes = database.getReference("passcodes");
        mbpvplinks = database.getReference("pvplinks");
        mBevents = database.getReference("niaevents");


        listenertempscansave = listenertempscansave;
        qrsavelistener = qrsavelistener;

        // mDatabasemissions.keepSynced(true);
        //mDatabaserunnung.keepSynced(true);
        //mdbroot.keepSynced(true);

    }

    public AODConfig(Context cont) {

        ctxx = cont;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(ctxx);
    }

    public AODConfig(String refferer, Context context) {

        ctxx = context;
        database = FirebaseDatabase.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        pref = context.getSharedPreferences("AppPref", MODE_PRIVATE);

        try {
            PackageInfo pInfo = ctxx.getPackageManager().getPackageInfo(ctxx.getPackageName(), 0);
            int verCode = pInfo.versionCode;
            new AgentData("appversion", verCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Nullable
    public String getStringFromRawRes(int rawRes) {

        InputStream inputStream;
        try {
            inputStream = ctxx.getResources().openRawResource(rawRes);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
            return null;
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        try {
            while ((length = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, length);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                inputStream.close();
                byteArrayOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String resultString;
        try {
            resultString = byteArrayOutputStream.toString("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

        return resultString;
    }

    public void tempscanCodescanned(String email, final String qrid, String owner, boolean open, final boolean scaned) {
        System.out.println("LOG GET DATA-->" + qrid + " 2 " + owner);  //prints "Do you have data? You'll love Firebase."

        if (qrdatabase.child(qrid).child("isopen").getKey() != null) {

            qrdatabase.child(qrid).child("scanedagnet").setValue(owner);
            qrdatabase.child(qrid).child("scaned").setValue(scaned);


            listenertempscansave = qrdatabase.child(qrid).child("creator").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    //System.out.println(snapshot.toString() + "687655");  //prints "Do you have data? You'll love Firebase."


                    if (originalagent == null) {
                        originalagent = snapshot.toString();
                    }


                    qrsavelistener = qrdatabase.child(qrid).addChildEventListener(new ChildEventListener() {


                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                            if (dataSnapshot.getKey().equalsIgnoreCase("creator")) {
                                System.out.println(dataSnapshot.getValue().toString() + " creator  v4");
                                originalagent = dataSnapshot.getValue().toString();

                            }
                            if (dataSnapshot.getKey().equalsIgnoreCase("scanedagnet")) {
                                scanneragent = dataSnapshot.getValue().toString();
                                pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                                if (scanneragent.equalsIgnoreCase("-")) {
                                    pref.edit().putBoolean("scanedagnet", false).apply();
                                } else {
                                    pref.edit().putBoolean("scanedagnet", true).apply();
                                }
                                System.out.println(dataSnapshot.getValue().toString() + " scanedagnet updated " + scanneragent);

                            }
                            if (dataSnapshot.getKey().equalsIgnoreCase("isopen")) {
                                scanisopen = dataSnapshot.getValue().toString().equalsIgnoreCase("true");

                            }
                            if (dataSnapshot.getKey().equalsIgnoreCase("scaned")) {
                                if (dataSnapshot.getValue().toString().equalsIgnoreCase("true")) {
                                    qrisscaned = true;
                                    pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                                    pref.edit().putBoolean("isscaned", true).apply();
                                    Log.i("SHARED 5-->", "" + qrisscaned + " true");
                                } else {
                                    qrisscaned = false;
                                    pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                                    pref.edit().putBoolean("isscaned", false).apply();
                                    System.out.println(dataSnapshot.getValue().toString() + " scaned ");
                                    Log.i("SHARED 6-->", "" + qrisscaned + " false");
                                }

                            }

                            System.out.println("first data1 c " + dataSnapshot);

                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            if (dataSnapshot.getKey().equalsIgnoreCase("creator")) {
                                System.out.println(dataSnapshot.getValue().toString() + " creator xx ");
                                originalagent = dataSnapshot.getValue().toString();

                            } else if (dataSnapshot.getKey().equalsIgnoreCase("scanedagnet")) {
                                scanneragent = dataSnapshot.getValue().toString();
                                //originalagent = originalagent;
                                pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                                if (scanneragent.equalsIgnoreCase("-")) {
                                    pref.edit().putBoolean("scanedagnet", false).apply();
                                } else {
                                    pref.edit().putBoolean("scanedagnet", true).apply();
                                }
                                System.out.println(dataSnapshot.getValue().toString() + " scanedagnet updated" + scanneragent + " " + originalagent);


                            } else if (dataSnapshot.getKey().equalsIgnoreCase("isopen")) {
                                scanisopen = dataSnapshot.getValue().toString().equalsIgnoreCase("true");


                            } else if (dataSnapshot.getKey().equalsIgnoreCase("scaned")) {
                                if (dataSnapshot.getValue().toString().equalsIgnoreCase("true")) {
                                    qrisscaned = true;
                                    pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                                    pref.edit().putBoolean("isscaned", true).apply();
                                    Log.i("SHARED 7-->", "" + qrisscaned + " true");
                                } else {
                                    qrisscaned = false;
                                    pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                                    pref.edit().putBoolean("isscaned", false).apply();
                                    Log.i("SHARED 8-->", "" + qrisscaned + " false");
                                }
                                System.out.println(dataSnapshot.getValue().toString() + " scaned2");

                            }

                            System.out.println("first data2 " + dataSnapshot);


                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError error) {

                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError error) {
                }
            });


        }
        // NO SCANCODE FOUND
        else {
            pref.edit().putBoolean("scanedagnet", true).apply();
            pref.edit().putBoolean("isscaned", true).apply();

            Log.i("QR NOT FOUND", "" + qrisscaned);
        }

        //  mDatabase.child("users").child(userId).setValue(admin);
    }

    public void tempscanCoderemove(String email, String qrid, String owner) {

        //qrdatabase.child(qrid).child("creator").setValue(owner);
        // originalagent = owner;
        Log.i("THE OWNER IS 667", "" + originalagent + "id->" + qrid);
        if (qrid != null) {
            qrdatabase.child(qrid).removeValue();
        }

        //qrdatabase.removeEventListener(qrsavelistener);

    }

    public void tempscanCode(String email, String qrid, String owner, boolean open, boolean scaned) {


        qrdatabase.child(qrid).child("creator").setValue(AgentData.getagentname());
        qrdatabase.child(qrid).child("isopen").setValue(open);
        qrdatabase.child(qrid).child("scaned").setValue(scaned);
        qrdatabase.child(qrid).child("scanedagnet").setValue("-");


        qrsavelistener = qrdatabase.child(qrid).addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                if (dataSnapshot.getKey().equalsIgnoreCase("creator")) {
                    System.out.println(dataSnapshot.getValue().toString() + " creator ");
                    originalagent = dataSnapshot.getValue().toString();

                }
                if (dataSnapshot.getKey().equalsIgnoreCase("isopen")) {
                    scanisopen = dataSnapshot.getValue().toString().equalsIgnoreCase("true");

                }
                if (dataSnapshot.getKey().equalsIgnoreCase("scaned")) {
                    if (dataSnapshot.getValue().toString().equalsIgnoreCase("true")) {
                        qrisscaned = true;
                        pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                        pref.edit().putBoolean("isscaned", true).apply();
                        Log.i("SHARED 1-->", "" + qrisscaned + " true");
                    } else {
                        qrisscaned = false;
                        pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                        pref.edit().putBoolean("isscaned", false).apply();
                        System.out.println(dataSnapshot.getValue().toString() + " scaned ");
                        Log.i("SHARED 2-->", "" + qrisscaned + " false");
                    }

                }
                if (dataSnapshot.getKey().equalsIgnoreCase("scanedagnet")) {
                    scanneragent = dataSnapshot.getValue().toString();
                    pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                    if (scanneragent.equalsIgnoreCase("-")) {
                        pref.edit().putBoolean("scanedagnet", false).apply();
                    } else {
                        pref.edit().putBoolean("scanedagnet", true).apply();
                    }


                }

                System.out.println("first data1x" + dataSnapshot);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.getKey().equalsIgnoreCase("creator")) {
                    System.out.println(dataSnapshot.getValue().toString() + " creator ");
                    originalagent = dataSnapshot.getValue().toString();

                }
                if (dataSnapshot.getKey().equalsIgnoreCase("isopen")) {
                    scanisopen = dataSnapshot.getValue().toString().equalsIgnoreCase("true");


                }

                if (dataSnapshot.getKey().equalsIgnoreCase("scaned")) {
                    if (dataSnapshot.getValue().toString().equalsIgnoreCase("true")) {
                        qrisscaned = true;
                        pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                        pref.edit().putBoolean("isscaned", true).apply();
                        Log.i("SHARED 3-->", "" + qrisscaned + " true");
                    } else {
                        qrisscaned = false;
                        pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                        pref.edit().putBoolean("isscaned", false).apply();
                        System.out.println(dataSnapshot.getValue().toString() + " scaned2 ");
                        Log.i("SHARED 4-->", "" + qrisscaned + " false");
                    }
                    System.out.println(dataSnapshot.getValue().toString() + " scaned2");

                }
                if (dataSnapshot.getKey().equalsIgnoreCase("scanedagnet")) {
                    scanneragent = dataSnapshot.getValue().toString();
                    pref = ctxx.getSharedPreferences("AppPref", MODE_PRIVATE);
                    if (scanneragent.equalsIgnoreCase("-")) {
                        pref.edit().putBoolean("scanedagnet", false).apply();
                    } else {
                        pref.edit().putBoolean("scanedagnet", true).apply();
                    }

                }

                System.out.println("first data2" + dataSnapshot);


            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });

        //  mDatabase.child("users").child(userId).setValue(admin);
    }

    public String QRId(String randomizer) {
        Date now = new Date();
        String allowedis = randomizer + "qwertyuioZVXQRTAOD";
        String qrid = new SimpleDateFormat("ddHHmmss", Locale.GERMANY).format(now);
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder(15);
        for (int i = 0; i < 15; ++i)
            sb.append(allowedis.charAt(random.nextInt(allowedis.length())));

        qrid = qrid + sb;
        return qrid;
    }

}
