package rocks.prxenon.aod.basics;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import it.gmariotti.changelibs.library.view.ChangeLogRecyclerView;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.pages.AODMain;


/**
 * Example with Dialog
 *
 * @author Gabriele Mariotti (gabri.mariotti@gmail.com)
 */
public class Fragment_ChangeLog extends DialogFragment {
    String thisVersion;

    public Fragment_ChangeLog() {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        ChangeLogRecyclerView chgList = (ChangeLogRecyclerView) layoutInflater.inflate(R.layout.fragment_changelog, null);
        thisVersion = "";
        try {
            thisVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(),
                    0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            thisVersion = "";
            // Log.e(TAG, "could not get version name from manifest!");
            e.printStackTrace();
        }
        return new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle)
                .setTitle("Changelog")
                .setView(chgList)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                AODMain aodMainis = new AODMain();
                                aodMainis.updateVersionInPreferences(getActivity(), thisVersion);
                                dialog.dismiss();
                            }
                        }
                )
                .create();

    }


}