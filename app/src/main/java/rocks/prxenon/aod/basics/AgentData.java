package rocks.prxenon.aod.basics;

import android.content.SharedPreferences;
import android.util.Log;

import java.util.List;

import static rocks.prxenon.aod.basics.AODConfig.pref;


/**
 * Created by PrXenon on 04.09.2017.
 */

public class AgentData {

    public static int appversion;
    public static String agentname, faction, agentimage, ebl_token;
    public static String trickster;
    public static String tg_en, tg_de, tg_news, tg_ru, tg_es, MERCHANT_ID;
    public static String c_year, c_month, c_week;
    public static boolean internet, shouldAllowBack, hasfilter, hasfilterregio, hasfiltercity, hasfiltercountry, hasfiltertype, redacted, eblinstalled, hackvent;
    public static int getscanproof = 0;
    public static String connectedwith;
    public static int privacystatus;
    public static List skudata, skudata_extra, alltimelist, monthlist, weeklist, nowlist;
    public static String otheragentimage, otheragentfaction, otheragentemail, status, min_ingress;
    public static List<String> filterdatacountry, filterdatatyp;

    public AgentData() {

    }

    public AgentData(String ref, int data) {
        Log.i("GREFFERER", "" + ref);
        if (ref.equalsIgnoreCase("appversion")) {

            appversion = data;
        }
        if (ref.equalsIgnoreCase("privacystatus")) {

            privacystatus = data;
        }

    }

    public AgentData(String refx, List datax, String dummy) {
        if (refx.equalsIgnoreCase("sku")) {

            skudata = datax;
            Log.i("SKU-->", " " + skudata.toString());
        }
        if (refx.equalsIgnoreCase("sku_extra")) {

            skudata_extra = datax;
            Log.i("SKU EXTRA-->", " " + skudata_extra.toString());
        }
        if (refx.equalsIgnoreCase("alltime")) {

            alltimelist = datax;
            Log.i("ALLTIME-->", " " + alltimelist.toString());
        }
        if (refx.equalsIgnoreCase("month")) {

            monthlist = datax;
            Log.i("MONTHDATA-->", " " + monthlist.toString());
        }
        if (refx.equalsIgnoreCase("week")) {

            weeklist = datax;
            Log.i("WEEKDATA-->", " " + weeklist.toString());
        }
        if (refx.equalsIgnoreCase("now")) {

            nowlist = datax;
            Log.i("NOWDATA-->", " " + nowlist.toString());
        }
    }


    public AgentData(String ref, String data) {
        Log.i("GREFFERER", "" + ref);
        if (ref.equalsIgnoreCase("status")) {
            status = data;
        }
        if (ref.equalsIgnoreCase("agentname")) {

            agentname = data;
        }

        if (ref.equalsIgnoreCase("userimage")) {

            agentimage = data;
        }
        if (ref.equalsIgnoreCase("faction")) {

            faction = data;
        }

        if (ref.equalsIgnoreCase("trickster")) {

            trickster = data;
        }

        if (ref.equalsIgnoreCase("tg_news")) {

            tg_news = data;
        }
        if (ref.equalsIgnoreCase("tg_en")) {

            tg_en = data;
        }
        if (ref.equalsIgnoreCase("tg_de")) {

            tg_de = data;
        }
        if (ref.equalsIgnoreCase("tg_ru")) {

            tg_ru = data;
        }
        if (ref.equalsIgnoreCase("tg_es")) {

            tg_es = data;
        }
        if (ref.equalsIgnoreCase("MERCHANT_ID")) {

            MERCHANT_ID = data;
        }
        if (ref.equalsIgnoreCase("min_ingress")) {

            min_ingress = data;
        }

        if (ref.equalsIgnoreCase("ebl_token")) {

            ebl_token = data;
        }


        if (ref.equalsIgnoreCase("year")) {

            c_year = data;
        }

        if (ref.equalsIgnoreCase("month")) {

            c_month = data;
        }

        if (ref.equalsIgnoreCase("week")) {

            c_week = data;
        }

        if (ref.equalsIgnoreCase("connectedwith")) {
            connectedwith = data;

        }

        if (ref.equalsIgnoreCase("otheragentemail")) {
            otheragentemail = data;

        }


        if (ref.equalsIgnoreCase("otheragentimage")) {
            otheragentimage = data;

            getscanproof++;
            Log.i("787664", "" + getscanproof);
            if (getscanproof == 2) {
                Log.i(" proof 1", "" + otheragentimage + " " + otheragentfaction);
                pref.edit().putBoolean("hasalldata", true).apply();
            }
        }

        if (ref.equalsIgnoreCase("otheragentfaction")) {
            otheragentfaction = data;
            getscanproof++;
            Log.i("78766ff4", "" + getscanproof);
            if (getscanproof == 2) {
                Log.i(" proof 2", "" + otheragentimage + " " + otheragentfaction);
                pref.edit().putBoolean("hasalldata", true).apply();

            }
        }


    }

    public AgentData(String ref, List<String> data) {
        if (ref.equalsIgnoreCase("filterdatacountry")) {
            Log.i(" FilterSet100-->", "" + data.toString());
            filterdatacountry = data;

        }

        if (ref.equalsIgnoreCase("filterdatatype")) {
            Log.i(" FilterSet200-->", "" + data.toString());
            filterdatacountry = data;

        }
    }

    public AgentData(String ref, boolean data) {

        if (ref.equalsIgnoreCase("internet")) {
            Log.i(" SAVE REQUEST I -->", "" + data);
            internet = data;
        }

        if (ref.equalsIgnoreCase("allowback")) {
            Log.i(" SAVE REQUEST I -->", "" + data);
            shouldAllowBack = data;
        }

        if (ref.equalsIgnoreCase("filter")) {
            Log.i(" FilterSet1-->", "" + data);
            hasfilter = data;
        }
        if (ref.equalsIgnoreCase("filterregio")) {
            Log.i(" FilterSet2-->", "" + data);
            hasfilterregio = data;
        }
        if (ref.equalsIgnoreCase("filtercity")) {
            Log.i(" FilterSet3-->", "" + data);
            hasfiltercity = data;
        }
        if (ref.equalsIgnoreCase("filtercountry")) {
            Log.i(" FilterSet4-->", "" + data);
            hasfiltercountry = data;
        }
        if (ref.equalsIgnoreCase("filtertype")) {
            Log.i(" FilterSet5-->", "" + data);
            hasfiltertype = data;
        }
        if (ref.equalsIgnoreCase("redacted")) {
            Log.i(" FilterSet6-->", "" + data);
            redacted = data;
        }

        if (ref.equalsIgnoreCase("hackvent19")) {
            Log.i(" FilterSetHackvent-->", "" + data);
            hackvent = data;
        }

        if (ref.equalsIgnoreCase("ebl")) {
            Log.i(" FilterSet7-->", "" + data);
            eblinstalled = data;
        }
    }

    public AgentData(String ref, SharedPreferences getpref) {
        pref = getpref;

    }

    public static int getappversion(String ref) {
        int returnstring = 0;
        if (ref.equalsIgnoreCase("appversion")) {
            //Log.i(" GET REQUEST I -->", "" + appversion);
            // return minversion;
            returnstring = appversion;

        }
        return returnstring;
    }

    public static String getEbl_token() {

        ebl_token = pref.getString("ebl_token", "null");
        return ebl_token;
    }

    public static String getagentname() {


        return agentname;
    }

    public static String getFaction() {


        return faction;
    }

    public static String getTrickster() {


        return trickster;
    }

    public static String getC_week() {


        return c_week;
    }

    public static String getC_month() {


        return c_month;
    }

    public static List getsku_extra() {


        return skudata_extra;
    }

    public static List getsku() {


        return skudata;
    }

    public static String getC_year() {


        return c_year;
    }

    public static Boolean gethasfilter() {


        return hasfilter;
    }

    public static Boolean gethackvent() {


        if (hackvent) {
            return hasfilter;
        } else {
            return false;
        }
    }


    public static String getmin_ingress() {


        return min_ingress;
    }

    public static Boolean getredacted() {


        return redacted;
    }


    public static Boolean gethasfiltercountry() {


        return hasfiltercountry;
    }

    public static Boolean gethasfilterregio() {


        return hasfilterregio;
    }

    public static Boolean gethasfiltercity() {


        return hasfiltercity;
    }

    public static Boolean gethasfiltertype() {


        return hasfiltertype;
    }

    public static Boolean geteblinstalled() {


        return eblinstalled;
    }

    public static List<String> getfilterdatacountry() {
        return filterdatacountry;
    }

    public static List<String> getfilterdatatyp() {
        return filterdatatyp;
    }

    public static String getTG_news() {
        return tg_news;
    }

    public static String getTG_en() {
        return tg_en;
    }

    public static String getTG_de() {
        return tg_de;
    }

    public static String getTG_ru() {
        return tg_ru;
    }

    public static String getTG_es() {
        return tg_es;
    }

    public static String getMERVHANTID() {
        return MERCHANT_ID;
    }

    public boolean getInternet(String ref) {

        if (ref.equalsIgnoreCase("internet")) {
            Log.i(" GET REQUEST I -->", "" + internet);
            return internet;
        } else {
            return false;
        }

    }

    public String getAgentdata(String ref) {
        String returndata = null;
        if (ref.equalsIgnoreCase("userimage")) {
            returndata = agentimage;
        }

        if (ref.equalsIgnoreCase("faction")) {
            returndata = faction;
        }

        if (ref.equalsIgnoreCase("otheragentimage")) {
            returndata = otheragentimage;
        }
        if (ref.equalsIgnoreCase("otheragentfaction")) {
            returndata = otheragentfaction;
        }

        if (ref.equalsIgnoreCase("otheragentemail")) {
            returndata = otheragentemail;
        }


        return returndata;
    }

    public String getOtherAgentdata(String ref) {
        String returndata = null;


        if (ref.equalsIgnoreCase("otheragentimage")) {
            returndata = otheragentimage;
        }
        if (ref.equalsIgnoreCase("otheragentfaction")) {
            returndata = otheragentfaction;
        }

        if (ref.equalsIgnoreCase("otheragentemail")) {
            returndata = otheragentemail;
        }

        return returndata;
    }

}
