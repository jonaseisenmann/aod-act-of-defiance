package rocks.prxenon.aod.pages.history;


import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.perf.metrics.AddTrace;
import com.muddzdev.styleabletoast.StyleableToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.RankingAdapter;
import rocks.prxenon.aod.adapter.RankingHistoryAdapter;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;
import static rocks.prxenon.aod.basics.AODConfig.mDatabasemissions;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnung;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;


public class RankingHistoryOld extends AppCompatActivity {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private ProgressBar progressBarm;
    private Animation animZoomIn, animZoomNull;
    private Typeface type, proto;
    private TextView mTitleTextView, subline;
    private TextView missionname, rank_anzahl_agents, progress_txt, missiontext;
    private CircleImageView missionimage, missionimage2;
    private ViewPager mPager;
    private AppBarLayout appbarlayout;
    private FrameLayout ranking_history_container;

    private Bundle bundle;
    private ListView rankinglist, ranking_history;
    private NestedScrollView netstescroll;
    private RelativeLayout historyview, historynoselected;
    private TabLayout tabLayout, tabLayoutrank;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;
    private int theselectedpos;

    public static String getMonthShortName(int monthNumber) {
        String monthName = "";

        if (monthNumber >= 0 && monthNumber < 12)
            try {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.MONTH, monthNumber);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM");
                //simpleDateFormat.setCalendar(calendar);
                monthName = simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                if (e != null)
                    e.printStackTrace();
            }
        return monthName;
    }

    public static Bitmap getScreenShot(View view) {
        View screenView = view;
        screenView.findViewById(R.id.share_rank).setVisibility(View.GONE);
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_ranking_history);


        new AODConfig("default", this);
        //Get Firebase auth instance

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);

        appbarlayout = findViewById(R.id.appbarlayout);


        new AgentData("prefs", pref);
        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");


        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_ranking_history, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        subline = actionBar.findViewById(R.id.titlesub_text);

        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        netstescroll = findViewById(R.id.netstescroll);

        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);


        //  subline.setText(getResources().getString(R.string.month_mission_text));
        //  monthof.setText(getMonthShortName((Integer.parseInt( bundle.getString("themonth"))-1)));


        // TODO SET MONTH / WEEK TO STRING
        mTitleTextView.setText(R.string.title_activity_ranking_history);

        ranking_history_container = findViewById(R.id.ranking_history_container);
        rankinglist = findViewById(R.id.rankinglist);
        ranking_history = findViewById(R.id.ranking_history);
        historyview = findViewById(R.id.historyview);
        historynoselected = findViewById(R.id.historynoselected);


        tabLayout = findViewById(R.id.tabsmain);
        tabLayoutrank = findViewById(R.id.tabsrank);


        if (pref.getBoolean("hasfriends", false)) {
            tabLayoutrank.addTab(tabLayoutrank.newTab().setText("FRIENDS"));
            //  removeTab(1);

        }


        if (pref.getBoolean("hasteam", false)) {
            tabLayoutrank.addTab(tabLayoutrank.newTab().setText("TEAM"));
        }


        tabLayoutrank.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tabLayoutrank.getSelectedTabPosition() == 0) {
                    if (tabLayout.getSelectedTabPosition() == 0) {
                        // Toast.makeText(RankingHistory.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                        getrankinghostory("week", tabLayoutrank.getTabAt(tabLayoutrank.getSelectedTabPosition()).getText().toString());

                    } else if (tabLayout.getSelectedTabPosition() == 1) {
                        // Toast.makeText(RankingHistory.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                        getrankinghostory("month", tabLayoutrank.getTabAt(tabLayoutrank.getSelectedTabPosition()).getText().toString());

                    }
                    //  Toast.makeText(RankingMain.this, "Tab 0 " + tabLayout.getSelectedTabPosition()+ " "+tab.getText().toString(), Toast.LENGTH_LONG).show();

                } else {
                    //   Toast.makeText(RankingMain.this, "Tab  " + tabLayout.getSelectedTabPosition()+ " "+tab.getText().toString(), Toast.LENGTH_LONG).show();
                    if (tab.getText().toString().equalsIgnoreCase("FRIENDS")) {

                        //    startrankingfriends(bundle);
                        if (tabLayout.getSelectedTabPosition() == 0) {
                            // Toast.makeText(RankingHistory.this, "FRIENDS W", Toast.LENGTH_LONG).show();
                            // Toast.makeText(RankingHistory.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                            getrankinghostory("week", tabLayoutrank.getTabAt(tabLayoutrank.getSelectedTabPosition()).getText().toString());

                        } else if (tabLayout.getSelectedTabPosition() == 1) {
                            // Toast.makeText(RankingHistory.this, "FRIENDS M", Toast.LENGTH_LONG).show();
                            // Toast.makeText(RankingHistory.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                            getrankinghostory("month", tabLayoutrank.getTabAt(tabLayoutrank.getSelectedTabPosition()).getText().toString());

                        }

                    } else {
                        // TODO TEAM RANKING
                        //  Toast.makeText(RankingMain.this, "TEAM", Toast.LENGTH_LONG).show();
                        if (tabLayout.getSelectedTabPosition() == 0) {
                            // Toast.makeText(RankingHistory.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                            //getrankinghostory("week");

                        } else if (tabLayout.getSelectedTabPosition() == 1) {
                            // Toast.makeText(RankingHistory.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                            // getrankinghostory("month");

                        }
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                ranking_history_container.setVisibility(View.GONE);
                if (tabLayout.getSelectedTabPosition() == 0) {
                    // Toast.makeText(RankingHistory.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();

                    getrankinghostory("week", "START");
                    if (ranking_history_container.isShown()) {

                        ranking_history_container.setVisibility(View.GONE);
                    }
                    if (historyview.isShown()) {
                        historyview.setVisibility(View.GONE);
                        historynoselected.setVisibility(View.VISIBLE);


                    }
                } else if (tabLayout.getSelectedTabPosition() == 1) {
                    // Toast.makeText(RankingHistory.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    getrankinghostory("month", "START");
                    if (ranking_history_container.isShown()) {
                        ranking_history_container.setVisibility(View.GONE);
                    }
                    if (historyview.isShown()) {
                        historyview.setVisibility(View.GONE);
                        historynoselected.setVisibility(View.VISIBLE);


                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //  Toast.makeText(RankingHistory.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
        //  startranking(bundle);
        getrankinghostory("week", "START");

        ImageButton scroll_btn = findViewById(R.id.scroll_btn_list);
        scroll_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                ranking_history_container.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();

        if (ranking_history_container.isShown()) {
            ranking_history_container.setVisibility(View.GONE);
        }

        if (historyview.isShown()) {
            historyview.setVisibility(View.GONE);
            historynoselected.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");


    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");


    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();


    }

    private void getrankinghostory(final String theartis, final String listart) {
        Log.i("Hieriam -->", "On Way 78766");
        final String monthweekval;
        final List<String> badgeid = new ArrayList();
        final List<String> keymonthweek = new ArrayList();
        final List<Integer> agentssum = new ArrayList();
        final List<Integer> sumall = new ArrayList();
        final List<String> missionid = new ArrayList();
        final List<String> theyearofmission = new ArrayList();
        final List<String> thetitle = new ArrayList();
        final List<String> badgenia = new ArrayList();

        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);

        if (mDatabasemissions == null) {
            new AODConfig();
        }

        if (theartis.equalsIgnoreCase("week")) {
            monthweekval = AgentData.getC_week();
        } else {
            monthweekval = AgentData.getC_month();
        }

        //Query myTopPostsQuery = mDatabasemissions.child(theartis).child(AgentData.getC_year()).orderByKey();
        // Query myTopPostsQuery = mDatabasemissions.child(theartis).child(AgentData.getC_year()).orderByKey();
        Query myTopPostsQuery = mDatabasemissions.child(theartis).child(String.valueOf(Integer.parseInt(AgentData.getC_year()) - 1)).orderByKey();


        Log.i("LISTENART ---->", listart);
        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot child : snapshot.getChildren()) {


                    theyearofmission.add(snapshot.getKey());
                    badgeid.add(child.child("badge").getValue().toString());
                    keymonthweek.add(child.getKey());
                    sumall.add(Integer.parseInt(child.child("community").child("current").getValue().toString()));
                    agentssum.add(Integer.parseInt(child.child("agents").getValue().toString()));
                    missionid.add(child.child("id").getValue().toString());
                    if (child.hasChild("title")) {
                        thetitle.add(child.child("title").getValue().toString());
                        badgenia.add(child.child("badgenia").getValue().toString());
                    } else {
                        thetitle.add("???");
                        badgenia.add("???");
                    }



                }

                Collections.reverse(badgeid);
                Collections.reverse(keymonthweek);
                Collections.reverse(agentssum);
                Collections.reverse(sumall);
                Collections.reverse(missionid);
                Collections.reverse(thetitle);
                Collections.reverse(badgenia);

                if (missionid.size() > 0) {
                    RankingHistoryAdapter adapter5 = new RankingHistoryAdapter(RankingHistoryOld.this, badgeid, keymonthweek, agentssum, sumall, missionid, theyearofmission);
                    rankinglist.setAdapter(adapter5);
                    adapter5.notifyDataSetChanged();

                    if (listart.equalsIgnoreCase("FRIENDS") || listart.equalsIgnoreCase("TEAM")) {

                        mDatabaseaod.child("badge").child(badgeid.get(theselectedpos)).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(final DataSnapshot snapshotbadge) {

                                missionname = findViewById(R.id.missionname);
                                rank_anzahl_agents = findViewById(R.id.rank_anzahl_agents);
                                rank_anzahl_agents.setText("" + agentssum.get(theselectedpos));
                                progress_txt = findViewById(R.id.rank_own_progress);
                                progress_txt.setText("" + sumall.get(theselectedpos) + " " + snapshotbadge.child("short1").getValue().toString());
                                missiontext = findViewById(R.id.missiontext);
                                missiontext.setText(snapshotbadge.child("short2").getValue().toString());
                                missionname.setText(thetitle.get(theselectedpos));
                                missionname.setSelected(true);
                                missionimage = findViewById(R.id.missionimage);
                                int badgemonthid_content_com = getResources().getIdentifier(snapshotbadge.child("drawable").getValue().toString(), "drawable", getPackageName());

                                //badgemonth = getResources().obtainTypedArray(badgemonthid);
                                if (badgemonthid_content_com != 0) {
                                    //   Log.w("BADGE -->", " id :"+badgemonthid_content);


                                    missionimage.setImageDrawable(getDrawable(badgemonthid_content_com));
                                }

                                Query myTopPostsQuery = mDatabaserunnung.child(theyearofmission.get(theselectedpos)).child(theartis).child(missionid.get(theselectedpos)).orderByChild("currentvalue");
                                final HashMap<String, String> data = new HashMap<String, String>();
                                final List<String> listUID = new ArrayList();
                                final List<Long> rankUID = new ArrayList();
                                final List<Integer> positionUID = new ArrayList();

                                // RANKING FOR THE AGENT
                                myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                    int counterloop = 0;

                                    @Override
                                    public void onDataChange(final DataSnapshot snapshot) {

                                        for (final DataSnapshot child : snapshot.getChildren()) {

                                            if (Long.parseLong(child.child("currentvalue").getValue().toString()) >= 0) {
                                                mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {

                                                    @Override
                                                    public void onDataChange(DataSnapshot snapshotfriends) {
                                                        counterloop++;
                                                        //   Log.i("SNAPP IS ", snapshotteam.child("email").getValue().toString() + "");
                                                        if (snapshotfriends.hasChild(child.child("uid").getValue().toString()) || child.child("uid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                                            Log.i("Freinds -->", "" + child.child("uid").getValue().toString());
                                                            if (!child.child("uid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                                                if (Boolean.parseBoolean(snapshotfriends.child(child.child("uid").getValue().toString()).child("status").getValue().toString())) {
                                                                    rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                                                                    String test = child.child("uid").getValue().toString();
                                                                    listUID.add(test);

                                                                }

                                                            } else if (child.child("uid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                                                rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                                                                String test = child.child("uid").getValue().toString();
                                                                listUID.add(test);
                                                            }

                                                        } else {
                                                            Log.i("Notfriend -->", "" + child.child("uid").getValue().toString());
                                                        }

                                                        Log.i("WHERE-->", "c: " + counterloop + " size: " + snapshot.getChildrenCount());
                                                        if (counterloop == snapshot.getChildrenCount()) {

                                                            Collections.reverse(listUID);
                                                            Collections.reverse(rankUID);
                                                            //  mList.indexOf(user.getUid())
                                                            int ownpositioninList = listUID.indexOf(user.getUid());
                                                            int ownrankis = 0;
                                                            boolean hasranking = false;
                                                            Log.i("OWN POS LIST ---->", "" + ownpositioninList + " sizeof--- " + listUID.size());
                                                            rank_anzahl_agents.setText("" + rankUID.size());
                                                            if (ownpositioninList == -1) {
                                                                //  rank_own_progress.setText("-");
                                                                //  agents_ownrank.setText("-");
                                                                for (int i = 0; i < rankUID.size(); i++) {
                                                                    Log.i("UID -->", "Value:" + listUID.get(i) + "List Size " + rankUID.size());

                                                                    if (i == 0) {

                                                                        ownrankis++;
                                                                        positionUID.add(ownrankis);
                                                                        if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                            hasranking = true;
                                                                            if (rankUID.get(i) > 0) {

                                                                                //rank_own_progress.setText("" + rankUID.get(i));
                                                                                Log.i("RANKING  oizhd-->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                // agents_ownrank.setText("" + ownrankis);

                                                                            } else {
                                                                                if (!hasranking) {
                                                                                    // agents_ownrank.setText("-");
                                                                                    // rank_own_progress.setText("-");

                                                                                }

                                                                            }
                                                                        } else {
                                                                            if (!hasranking) {
                                                                                // agents_ownrank.setText("-");
                                                                                // rank_own_progress.setText("-");


                                                                            }
                                                                        }
                                                                    } else {

                                                                        ownrankis++;
                                                                        positionUID.add(ownrankis);
                                                                        if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                            hasranking = true;
                                                                            Log.i("RANKING c -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                            if (rankUID.get(i) > 0) {

                                                                                Log.i("RANKING 555 -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                // agents_ownrank.setText("" + ownrankis);
                                                                                // rank_own_progress.setText("" + rankUID.get(i));
                                                                            } else {
                                                                                if (!hasranking) {
                                                                                    // agents_ownrank.setText("-");
                                                                                    // rank_own_progress.setText("-");

                                                                                }
                                                                            }
                                                                        } else {
                                                                            if (!hasranking) {
                                                                                // agents_ownrank.setText("-");
                                                                                // rank_own_progress.setText("-");


                                                                            }
                                                                        }


                                                                    }
                                                                }
                                                                //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                                                                if (ownrankis <= 0) {

                                                                    // agents_ownrank.setText("-");
                                                                }

                                                            } else {
                                                                for (int i = 0; i < rankUID.size(); i++) {
                                                                    //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                                                                    if (i == 0) {

                                                                        ownrankis++;
                                                                        positionUID.add(ownrankis);
                                                                        if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                            hasranking = true;
                                                                            if (rankUID.get(i) > 0) {

                                                                                // rank_own_progress.setText("" + rankUID2.get(i));
                                                                                //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                //   agents_ownrank.setText("" + ownrankis);

                                                                            } else {
                                                                                if (!hasranking) {
                                                                                    // agents_ownrank.setText("-");
                                                                                    // rank_own_progress.setText("-");

                                                                                }

                                                                            }
                                                                        } else {
                                                                            if (!hasranking) {
                                                                                // agents_ownrank.setText("-");
                                                                                // rank_own_progress.setText("-");


                                                                            }
                                                                        }
                                                                    } else {
                                                                        if (rankUID.get(i) > rankUID.get(ownpositioninList)) {
                                                                            ownrankis++;
                                                                            positionUID.add(ownrankis);
                                                                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                                hasranking = true;
                                                                                Log.i("RANKING tt-->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                if (rankUID.get(i) > 0) {

                                                                                    // rank_own_progress.setText("" + rankUID.get(i));
                                                                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                    //  agents_ownrank.setText("" + ownrankis);
                                                                                } else {
                                                                                    if (!hasranking) {
                                                                                        // agents_ownrank.setText("-");
                                                                                        // rank_own_progress.setText("-");


                                                                                    }
                                                                                }
                                                                            } else {
                                                                                if (!hasranking) {
                                                                                    // agents_ownrank.setText("-");
                                                                                    // rank_own_progress.setText("-");


                                                                                }
                                                                            }
                                                                        } else {
                                                                            ownrankis++;
                                                                            positionUID.add(ownrankis);
                                                                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                                hasranking = true;
                                                                                Log.i("RANKING ggg-->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                if (rankUID.get(i) > 0) {

                                                                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                    // agents_ownrank.setText("" + ownrankis);
                                                                                    // rank_own_progress.setText("" + rankUID.get(i));
                                                                                } else {
                                                                                    if (!hasranking) {
                                                                                        // agents_ownrank.setText("-");
                                                                                        // rank_own_progress.setText("-");

                                                                                    }
                                                                                }
                                                                            } else {
                                                                                if (!hasranking) {
                                                                                    // agents_ownrank.setText("-");
                                                                                    // rank_own_progress.setText("-");


                                                                                }
                                                                            }
                                                                        }

                                                                    }
                                                                }
                                                                //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                                                                if (ownrankis <= 0) {

                                                                    //  agents_ownrank.setText("-");
                                                                }

                                                            }


                                                            if (positionUID.size() > 0) {
                                                                RankingAdapter adapter6 = new RankingAdapter(RankingHistoryOld.this, listUID, rankUID, positionUID, theyearofmission.get(theselectedpos), keymonthweek.get(theselectedpos));
                                                                ranking_history.setAdapter(adapter6);
                                                                adapter6.notifyDataSetChanged();
                                                                coordinator.setVisibility(View.VISIBLE);
                                                                progressBarHolder.setVisibility(View.GONE);
                                                            } else {
                                                                coordinator.setVisibility(View.VISIBLE);
                                                                progressBarHolder.setVisibility(View.GONE);
                                                                StyleableToast.makeText(getApplicationContext(), getString(R.string.string_ranking_nodata), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {


                                                    }
                                                });

                                            }


                                        }


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        coordinator.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);
                                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                coordinator.setVisibility(View.VISIBLE);
                                progressBarHolder.setVisibility(View.GONE);

                            }
                        });
                    } else if (listart.equalsIgnoreCase("WORLD")) {

                        coordinator.setVisibility(View.GONE);
                        progressBarHolder.setVisibility(View.VISIBLE);
                        ranking_history_container.setVisibility(View.VISIBLE);
                        Log.i("Hieriam -->", "On Way Pommes4566 - 1");
                        if (!historyview.isShown()) {
                            historyview.setVisibility(View.VISIBLE);
                            historynoselected.setVisibility(View.GONE);
                        }

                        mDatabaseaod.child("badge").child(badgeid.get(theselectedpos)).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(final DataSnapshot snapshotbadge) {

                                missionname = findViewById(R.id.missionname);
                                rank_anzahl_agents = findViewById(R.id.rank_anzahl_agents);
                                rank_anzahl_agents.setText("" + agentssum.get(theselectedpos));
                                progress_txt = findViewById(R.id.rank_own_progress);
                                progress_txt.setText("" + sumall.get(theselectedpos) + " " + snapshotbadge.child("short1").getValue().toString());
                                missiontext = findViewById(R.id.missiontext);
                                missiontext.setText(snapshotbadge.child("short2").getValue().toString());
                                missionname.setText(snapshotbadge.child("title").getValue().toString());
                                missionname.setSelected(true);
                                missionimage = findViewById(R.id.missionimage);
                                int badgemonthid_content_com = getResources().getIdentifier(badgenia.get(theselectedpos), "drawable", getPackageName());

                                //badgemonth = getResources().obtainTypedArray(badgemonthid);
                                if (badgemonthid_content_com != 0) {
                                    //   Log.w("BADGE -->", " id :"+badgemonthid_content);


                                    missionimage.setImageDrawable(getDrawable(badgemonthid_content_com));
                                }

                                Query myTopPostsQuery = mDatabaserunnung.child(theyearofmission.get(theselectedpos)).child(theartis).child(missionid.get(theselectedpos)).orderByChild("currentvalue");
                                final HashMap<String, String> data = new HashMap<String, String>();
                                final List<String> listUID = new ArrayList();
                                final List<Long> rankUID = new ArrayList();
                                final List<Integer> positionUID = new ArrayList();

                                myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(final DataSnapshot snapshot) {


                                        for (DataSnapshot child : snapshot.getChildren()) {
                                            if (Long.parseLong(child.child("currentvalue").getValue().toString()) > 0 || child.child("uid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                                rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                                                String test = child.child("uid").getValue().toString();
                                                listUID.add(test);
                                            }
                                        }


                                        Collections.reverse(listUID);
                                        Collections.reverse(rankUID);
                                        //  mList.indexOf(user.getUid())
                                        int ownrankis = 0;

                                        for (int i = 0; i < rankUID.size(); i++) {
                                            //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());


                                            ownrankis++;
                                            positionUID.add(ownrankis);


                                        }


                                        if (positionUID.size() > 0) {
                                            RankingAdapter adapter6 = new RankingAdapter(RankingHistoryOld.this, listUID, rankUID, positionUID, theyearofmission.get(theselectedpos), keymonthweek.get(theselectedpos));
                                            ranking_history.setAdapter(adapter6);
                                            adapter6.notifyDataSetChanged();
                                            coordinator.setVisibility(View.VISIBLE);
                                            progressBarHolder.setVisibility(View.GONE);


                                        } else {
                                            StyleableToast.makeText(getApplicationContext(), getString(R.string.string_ranking_nodata), Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                            coordinator.setVisibility(View.VISIBLE);
                                            progressBarHolder.setVisibility(View.GONE);
                                        }

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                        coordinator.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);
                                    }
                                });
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                coordinator.setVisibility(View.VISIBLE);
                                progressBarHolder.setVisibility(View.GONE);

                            }
                        });
                    }


                    rankinglist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                            //text.setText((String) (lv.getItemAtPosition(position)));
                            //  StyleableToast.makeText(getApplicationContext(), "pos"+position, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            theselectedpos = position;
                            coordinator.setVisibility(View.GONE);
                            progressBarHolder.setVisibility(View.VISIBLE);
                            ranking_history_container.setVisibility(View.VISIBLE);
                            Log.i("Hieriam -->", "On Way Pommes4566 -2" + tabLayoutrank.getTabAt(tabLayoutrank.getSelectedTabPosition()).getText().toString());
                            if (!historyview.isShown()) {
                                historyview.setVisibility(View.VISIBLE);
                                historynoselected.setVisibility(View.GONE);
                            }

                            if (tabLayoutrank.getTabAt(tabLayoutrank.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("WORLD")) {

                                mDatabaseaod.child("badge").child(badgeid.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(final DataSnapshot snapshotbadge) {

                                        missionname = findViewById(R.id.missionname);
                                        rank_anzahl_agents = findViewById(R.id.rank_anzahl_agents);
                                        rank_anzahl_agents.setText("" + agentssum.get(position));
                                        progress_txt = findViewById(R.id.rank_own_progress);
                                        progress_txt.setText("" + sumall.get(position) + " " + snapshotbadge.child("short1").getValue().toString());
                                        missiontext = findViewById(R.id.missiontext);
                                        missiontext.setText(snapshotbadge.child("short2").getValue().toString());
                                        missionname.setText(thetitle.get(theselectedpos));
                                        missionname.setSelected(true);
                                        missionimage = findViewById(R.id.missionimage);
                                        int badgemonthid_content_com = getResources().getIdentifier(badgenia.get(theselectedpos), "drawable", getPackageName());

                                        //badgemonth = getResources().obtainTypedArray(badgemonthid);
                                        if (badgemonthid_content_com != 0) {
                                            //   Log.w("BADGE -->", " id :"+badgemonthid_content);


                                            missionimage.setImageDrawable(getDrawable(badgemonthid_content_com));
                                        }

                                        Query myTopPostsQuery = mDatabaserunnung.child(theyearofmission.get(position)).child(theartis).child(missionid.get(position)).orderByChild("currentvalue");
                                        final HashMap<String, String> data = new HashMap<String, String>();
                                        final List<String> listUID = new ArrayList();
                                        final List<Long> rankUID = new ArrayList();
                                        final List<Integer> positionUID = new ArrayList();

                                        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(final DataSnapshot snapshot) {


                                                for (DataSnapshot child : snapshot.getChildren()) {
                                                    if (Long.parseLong(child.child("currentvalue").getValue().toString()) > 0 || child.child("uid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                                        rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                                                        String test = child.child("uid").getValue().toString();
                                                        listUID.add(test);
                                                    }
                                                }


                                                Collections.reverse(listUID);
                                                Collections.reverse(rankUID);
                                                //  mList.indexOf(user.getUid())
                                                int ownrankis = 0;

                                                for (int i = 0; i < rankUID.size(); i++) {
                                                    //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());


                                                    ownrankis++;
                                                    positionUID.add(ownrankis);


                                                }


                                                if (positionUID.size() > 0) {
                                                    RankingAdapter adapter6 = new RankingAdapter(RankingHistoryOld.this, listUID, rankUID, positionUID, theyearofmission.get(position), keymonthweek.get(position));
                                                    ranking_history.setAdapter(adapter6);
                                                    adapter6.notifyDataSetChanged();
                                                    coordinator.setVisibility(View.VISIBLE);
                                                    progressBarHolder.setVisibility(View.GONE);


                                                } else {
                                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.string_ranking_nodata), Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                                    coordinator.setVisibility(View.VISIBLE);
                                                    progressBarHolder.setVisibility(View.GONE);
                                                }

                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                coordinator.setVisibility(View.VISIBLE);
                                                progressBarHolder.setVisibility(View.GONE);
                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                        coordinator.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);

                                    }
                                });

                            } else {

                                if (tabLayoutrank.getTabAt(tabLayoutrank.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("FRIENDS") || tabLayoutrank.getTabAt(tabLayoutrank.getSelectedTabPosition()).getText().toString().equalsIgnoreCase("TEAM")) {

                                    mDatabaseaod.child("badge").child(badgeid.get(theselectedpos)).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(final DataSnapshot snapshotbadge) {

                                            missionname = findViewById(R.id.missionname);
                                            rank_anzahl_agents = findViewById(R.id.rank_anzahl_agents);
                                            rank_anzahl_agents.setText("" + agentssum.get(theselectedpos));
                                            progress_txt = findViewById(R.id.rank_own_progress);
                                            progress_txt.setText("" + sumall.get(theselectedpos) + " " + snapshotbadge.child("short1").getValue().toString());
                                            missiontext = findViewById(R.id.missiontext);
                                            missiontext.setText(snapshotbadge.child("short2").getValue().toString());
                                            missionname.setText(thetitle.get(theselectedpos));
                                            missionname.setSelected(true);
                                            missionimage = findViewById(R.id.missionimage);
                                            int badgemonthid_content_com = getResources().getIdentifier(badgenia.get(theselectedpos), "drawable", getPackageName());

                                            //badgemonth = getResources().obtainTypedArray(badgemonthid);
                                            if (badgemonthid_content_com != 0) {
                                                //   Log.w("BADGE -->", " id :"+badgemonthid_content);


                                                missionimage.setImageDrawable(getDrawable(badgemonthid_content_com));
                                            }

                                            Query myTopPostsQuery = mDatabaserunnung.child(theyearofmission.get(theselectedpos)).child(theartis).child(missionid.get(theselectedpos)).orderByChild("currentvalue");
                                            final HashMap<String, String> data = new HashMap<String, String>();
                                            final List<String> listUID = new ArrayList();
                                            final List<Long> rankUID = new ArrayList();
                                            final List<Integer> positionUID = new ArrayList();

                                            // RANKING FOR THE AGENT
                                            myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                                                int counterloop = 0;

                                                @Override
                                                public void onDataChange(final DataSnapshot snapshot) {

                                                    for (final DataSnapshot child : snapshot.getChildren()) {

                                                        if (Long.parseLong(child.child("currentvalue").getValue().toString()) >= 0) {
                                                            mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {

                                                                @Override
                                                                public void onDataChange(DataSnapshot snapshotfriends) {
                                                                    counterloop++;
                                                                    //   Log.i("SNAPP IS ", snapshotteam.child("email").getValue().toString() + "");
                                                                    if (snapshotfriends.hasChild(child.child("uid").getValue().toString()) || child.child("uid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                                                        Log.i("Freinds -->", "" + child.child("uid").getValue().toString());
                                                                        rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                                                                        String test = child.child("uid").getValue().toString();
                                                                        listUID.add(test);

                                                                    } else {
                                                                        Log.i("Notfriend -->", "" + child.child("uid").getValue().toString());
                                                                    }

                                                                    Log.i("WHERE-->", "c: " + counterloop + " size: " + snapshot.getChildrenCount());
                                                                    if (counterloop == snapshot.getChildrenCount()) {

                                                                        Collections.reverse(listUID);
                                                                        Collections.reverse(rankUID);
                                                                        //  mList.indexOf(user.getUid())
                                                                        int ownpositioninList = listUID.indexOf(user.getUid());
                                                                        int ownrankis = 0;
                                                                        boolean hasranking = false;
                                                                        Log.i("OWN POS LIST ---->", "" + ownpositioninList + " sizeof--- " + listUID.size());
                                                                        rank_anzahl_agents.setText("" + rankUID.size());
                                                                        if (ownpositioninList == -1) {
                                                                            //  rank_own_progress.setText("-");
                                                                            //  agents_ownrank.setText("-");
                                                                            for (int i = 0; i < rankUID.size(); i++) {
                                                                                Log.i("UID -->", "Value:" + listUID.get(i) + "List Size " + rankUID.size());

                                                                                if (i == 0) {

                                                                                    ownrankis++;
                                                                                    positionUID.add(ownrankis);
                                                                                    if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                                        hasranking = true;
                                                                                        if (rankUID.get(i) > 0) {

                                                                                            //rank_own_progress.setText("" + rankUID.get(i));
                                                                                            Log.i("RANKING  oizhd-->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                            // agents_ownrank.setText("" + ownrankis);

                                                                                        } else {
                                                                                            if (!hasranking) {
                                                                                                // agents_ownrank.setText("-");
                                                                                                // rank_own_progress.setText("-");

                                                                                            }

                                                                                        }
                                                                                    } else {
                                                                                        if (!hasranking) {
                                                                                            // agents_ownrank.setText("-");
                                                                                            // rank_own_progress.setText("-");


                                                                                        }
                                                                                    }
                                                                                } else {

                                                                                    ownrankis++;
                                                                                    positionUID.add(ownrankis);
                                                                                    if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                                        hasranking = true;
                                                                                        Log.i("RANKING c -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                        if (rankUID.get(i) > 0) {

                                                                                            Log.i("RANKING 555 -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                            // agents_ownrank.setText("" + ownrankis);
                                                                                            // rank_own_progress.setText("" + rankUID.get(i));
                                                                                        } else {
                                                                                            if (!hasranking) {
                                                                                                // agents_ownrank.setText("-");
                                                                                                // rank_own_progress.setText("-");

                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        if (!hasranking) {
                                                                                            // agents_ownrank.setText("-");
                                                                                            // rank_own_progress.setText("-");


                                                                                        }
                                                                                    }


                                                                                }
                                                                            }
                                                                            //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                                                                            if (ownrankis <= 0) {

                                                                                // agents_ownrank.setText("-");
                                                                            }

                                                                        } else {
                                                                            for (int i = 0; i < rankUID.size(); i++) {
                                                                                //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                                                                                if (i == 0) {

                                                                                    ownrankis++;
                                                                                    positionUID.add(ownrankis);
                                                                                    if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                                        hasranking = true;
                                                                                        if (rankUID.get(i) > 0) {

                                                                                            // rank_own_progress.setText("" + rankUID2.get(i));
                                                                                            //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                            //   agents_ownrank.setText("" + ownrankis);

                                                                                        } else {
                                                                                            if (!hasranking) {
                                                                                                // agents_ownrank.setText("-");
                                                                                                // rank_own_progress.setText("-");

                                                                                            }

                                                                                        }
                                                                                    } else {
                                                                                        if (!hasranking) {
                                                                                            // agents_ownrank.setText("-");
                                                                                            // rank_own_progress.setText("-");


                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    if (rankUID.get(i) > rankUID.get(ownpositioninList)) {
                                                                                        ownrankis++;
                                                                                        positionUID.add(ownrankis);
                                                                                        if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                                            hasranking = true;
                                                                                            Log.i("RANKING tt-->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                            if (rankUID.get(i) > 0) {

                                                                                                // rank_own_progress.setText("" + rankUID.get(i));
                                                                                                //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                                //  agents_ownrank.setText("" + ownrankis);
                                                                                            } else {
                                                                                                if (!hasranking) {
                                                                                                    // agents_ownrank.setText("-");
                                                                                                    // rank_own_progress.setText("-");


                                                                                                }
                                                                                            }
                                                                                        } else {
                                                                                            if (!hasranking) {
                                                                                                // agents_ownrank.setText("-");
                                                                                                // rank_own_progress.setText("-");


                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        ownrankis++;
                                                                                        positionUID.add(ownrankis);
                                                                                        if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                                                                            hasranking = true;
                                                                                            Log.i("RANKING ggg-->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                            if (rankUID.get(i) > 0) {

                                                                                                //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                                                                // agents_ownrank.setText("" + ownrankis);
                                                                                                // rank_own_progress.setText("" + rankUID.get(i));
                                                                                            } else {
                                                                                                if (!hasranking) {
                                                                                                    // agents_ownrank.setText("-");
                                                                                                    // rank_own_progress.setText("-");

                                                                                                }
                                                                                            }
                                                                                        } else {
                                                                                            if (!hasranking) {
                                                                                                // agents_ownrank.setText("-");
                                                                                                // rank_own_progress.setText("-");


                                                                                            }
                                                                                        }
                                                                                    }

                                                                                }
                                                                            }
                                                                            //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                                                                            if (ownrankis <= 0) {

                                                                                //  agents_ownrank.setText("-");
                                                                            }

                                                                        }


                                                                        if (positionUID.size() > 0) {
                                                                            RankingAdapter adapter6 = new RankingAdapter(RankingHistoryOld.this, listUID, rankUID, positionUID, theyearofmission.get(theselectedpos), keymonthweek.get(theselectedpos));
                                                                            ranking_history.setAdapter(adapter6);
                                                                            adapter6.notifyDataSetChanged();
                                                                            coordinator.setVisibility(View.VISIBLE);
                                                                            progressBarHolder.setVisibility(View.GONE);
                                                                        } else {
                                                                            coordinator.setVisibility(View.VISIBLE);
                                                                            progressBarHolder.setVisibility(View.GONE);
                                                                            StyleableToast.makeText(getApplicationContext(), getString(R.string.string_ranking_nodata), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                        }
                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                    coordinator.setVisibility(View.VISIBLE);
                                                                    progressBarHolder.setVisibility(View.GONE);
                                                                }
                                                            });

                                                        }


                                                    }

                                                    coordinator.setVisibility(View.VISIBLE);
                                                    progressBarHolder.setVisibility(View.GONE);
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    coordinator.setVisibility(View.VISIBLE);
                                                    progressBarHolder.setVisibility(View.GONE);
                                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                }
                                            });
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                            coordinator.setVisibility(View.VISIBLE);
                                            progressBarHolder.setVisibility(View.GONE);

                                        }
                                    });
                                } else {
                                    coordinator.setVisibility(View.VISIBLE);
                                    progressBarHolder.setVisibility(View.GONE);
                                }
                            }

                        }

                    });

                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);

                } else {
                    StyleableToast.makeText(getApplicationContext(), getString(R.string.string_ranking_nodata), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);

            }
        });

    }


}