package rocks.prxenon.aod.pages.profile;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.firebase.ui.auth.AuthUI;
import com.github.siyamed.shapeimageview.OctogonImageView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.perf.metrics.AddTrace;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.muddzdev.styleabletoast.StyleableToast;
import com.nightonke.boommenu.Animation.BoomEnum;
import com.nightonke.boommenu.Animation.EaseEnum;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Util;

import org.apmem.tools.layouts.FlowLayout;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;
import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.BuildConfig;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.FriendsInvitesAdapter;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.basics.SaveFriendScan;
import rocks.prxenon.aod.fcm.DeleteTokenService;
import rocks.prxenon.aod.helper.GetNotifications;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.pages.FriendsMain;
import rocks.prxenon.aod.pages.ShowMyQR;
import rocks.prxenon.aod.pages.pvp.PvPCreate;
import rocks.prxenon.aod.receiver.NetworkStateReceiver;
import rocks.prxenon.aod.scanner.SmallCaptureActivity;
import rocks.prxenon.aod.settings.MainSettings;
import rocks.prxenon.aod.signin.FirebaseUIActivity;

import static rocks.prxenon.aod.basics.AODConfig.listenertempscansave;
import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseagentCodes;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;
import static rocks.prxenon.aod.basics.AODConfig.mbfriendsrequest;
import static rocks.prxenon.aod.basics.AODConfig.mbpasscodes;
import static rocks.prxenon.aod.basics.AODConfig.mdbroot;
import static rocks.prxenon.aod.basics.AODConfig.networkStateReceiver;
import static rocks.prxenon.aod.basics.AODConfig.originalagent;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.qrdatabase;
import static rocks.prxenon.aod.basics.AODConfig.qrsavelistener;
import static rocks.prxenon.aod.basics.AODConfig.scanneragent;
import static rocks.prxenon.aod.basics.AODConfig.user;
import static rocks.prxenon.aod.basics.AgentData.connectedwith;
import static rocks.prxenon.aod.basics.AgentData.otheragentemail;
import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;
import static rocks.prxenon.aod.helper.GetNotifications.newagentsprofile;
import static rocks.prxenon.aod.helper.GetNotifications.newagentsprofileownrequest;


public class ProfileMain extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, NetworkStateReceiver.NetworkStateReceiverListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private static final int REQUEST_CAM = 200;
    public static TextView openrequests;
    public static TextView morefriends;
    public static FrameLayout open_friends_container;
    public static View selectedbadge;
    private static String ALLOWED_CHARACTERS;
    private static final String[] PERMISSIONS_CAM = {android.Manifest.permission.CAMERA};
    final List<String> badgesnames = new ArrayList();
    final List<String> badgesdrawable = new ArrayList();
    final List<String> badgescontent = new ArrayList();
    final List<String> badgeskey = new ArrayList();
    final List<Boolean> badgesstufen = new ArrayList();
    final List<String> friendid = new ArrayList();
    final List<Boolean> friendstatus = new ArrayList();


    final List<String> allbadgeskey = new ArrayList();
    final List<String> allbadgesbadge = new ArrayList();
    final List<String> allbadgesart = new ArrayList();
    public long allmyfriends;
    public long allopenrequests;
    public FriendsInvitesAdapter friendsadapter;
    public ListView friendslistview;
    OctogonImageView prevbadge;
    AlertDialog.Builder builderfaction;
    AlertDialog.Builder builderagent;
    private boolean badgedetails;
    private ProgressBar progressBar;
    private Animation animZoomIn, animZoomNull;
    private Typeface type, proto;
    private TextView mTitleTextView;
    private TextView agentname, agentname2;
    private CircleImageView agentimg, agentimg2;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private AppBarLayout appbarlayout;
    private CollapsingToolbarLayout collapingtoolbar;
    private Toolbar toolbarx;
    private TextView medals_count_field, medals_txt, agentcode;
    private ImageButton share_rank, edit_agent_btn, edit_faction_btn;
    private RelativeLayout edit_agent_change;
    private final boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;
    private AlertDialog.Builder dialogBuilder;
    private LinearLayout badgeframemain;
    private LinearLayout closedframe;
    private State mCurrentState = State.IDLE;
    private BoomMenuButton qr_btn;
    private String[] menuitems;
    private TypedArray menuitems_drawble;
    private RelativeLayout agentcode_frame;
    private FrameLayout badge_container;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;
    private ImageButton logoutbtn;
    private String theqrcodestring;
    private String otheragentimage;
    private String otheragentfaction;
    private TextView profil_level, level_txt;

    private SharedPreferences globalsp;

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    private static String getRandomString(final int sizeOfRandomString) {
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sizeOfRandomString; ++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));

        return sb.toString();
    }

    public static Bitmap getScreenShot(View view) {
        View screenView = view;
        screenView.findViewById(R.id.share_rank).setVisibility(View.GONE);
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_profile);

        bindActivity();
        new AODConfig("default", this);
        //Get Firebase auth instance

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);
        pref.registerOnSharedPreferenceChangeListener(this);
        appbarlayout = findViewById(R.id.appbarlayout);
        collapingtoolbar = findViewById(R.id.collapingtoolbar);
        toolbarx = findViewById(R.id.toolbarx);
        toolbarx.setVisibility(View.GONE);
        closedframe = findViewById(R.id.closedframe);
        appbarlayout.addOnOffsetChangedListener(this);
        agentcode_frame = findViewById(R.id.agentcode_frame);
        badge_container = findViewById(R.id.badge_container);
        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");

        profil_level = findViewById(R.id.profil_level);
        level_txt = findViewById(R.id.level_txt);

        allmyfriends = 0;
        allopenrequests = 0;
        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);
        morefriends = findViewById(R.id.morefriends);
        openrequests = findViewById(R.id.openrequests);
        open_friends_container = findViewById(R.id.open_friends_container);

        ALLOWED_CHARACTERS = AgentData.getagentname().toUpperCase();
        qr_btn = findViewById(R.id.qr_btn);
        qr_btn.setButtonEnum(ButtonEnum.TextInsideCircle);
        qr_btn.setBoomInWholeScreen(true);
        qr_btn.setAutoHide(true);

        qr_btn.setBackgroundColor(getResources().getColor(R.color.transparent, getTheme()));
        qr_btn.setBackgroundResource(R.drawable.round_action_drawable);
        qr_btn.setUse3DTransformAnimation(false);
        new Thread(new Runnable() {
            @Override
            public void run() {

                Log.i("InstanceID", " " + FirebaseInstanceId.getInstance().getId());

            }
        }).start();


        qr_btn.setDimColor(getResources().getColor(R.color.colorPrimaryDarkSemiTrans, getTheme()));
        menuitems = getResources().getStringArray(R.array.menuqr);
        menuitems_drawble = getResources().obtainTypedArray(R.array.menuqr_drawable);

        for (int i = 0; i < qr_btn.getPiecePlaceEnum().pieceNumber(); i++) {
            TextInsideCircleButton.Builder builder = new TextInsideCircleButton.Builder()
                    //.isRound(false)
                    .shadowCornerRadius(Util.dp2px(5))
                    //  .buttonCornerRadius(Util.dp2px(5))

                    .shadowEffect(true)
                    .imageRect(new Rect(50, 15, Util.dp2px(60), Util.dp2px(60)))
                    .imagePadding(new Rect(10, 15, 10, 8))
                    .textSize(13)
                    .highlightedColorRes(R.color.colorAccent)
                    .normalImageRes(menuitems_drawble.getResourceId(i, -1))
                    .normalText(menuitems[i])
                    .rippleEffect(true)

                    .typeface(proto)

                    .textPadding(new Rect(5, 4, 5, 0))
                    .normalColorRes(R.color.colorPrimaryDark)

                    .pieceColorRes(R.color.transparent);


            qr_btn.addBuilder(builder);
            qr_btn.setOnBoomListener(new OnBoomListener() {


                @Override
                public void onClicked(int index, BoomButton boomButton) {


                    // TODO

                    if (index == 0) {

                        if (new AgentData().getInternet("internet")) {
                            qr_btn.setAutoHide(false);
                            if (qr_btn.isBoomed()) {
                                qr_btn.reboom();
                            }
                            scanQR();
                        } else {
                            Toast.makeText(ProfileMain.this, boomButton.getTextView().getText().toString() + " need a network connection!", Toast.LENGTH_SHORT).show();
                        }

                        //i.putExtra("PersonID", personID);

                    }
                    if (index == 1) {
                        if (new AgentData().getInternet("internet")) {
                            qr_btn.setAutoHide(false);
                            if (qr_btn.isBoomed()) {
                                qr_btn.reboom();
                            }
                            pref.edit().remove("isscaned").apply();
                            Intent i = new Intent(getBaseContext(), ShowMyQR.class);
                            //i.putExtra("PersonID", personID);
                            startActivity(i);
                        } else {
                            Toast.makeText(ProfileMain.this, boomButton.getTextView().getText().toString() + " need a network connection!", Toast.LENGTH_SHORT).show();
                        }

                    }
                    if (index == 2) {
                        // MANUELL
                        if (new AgentData().getInternet("internet")) {
                            qr_btn.setAutoHide(false);
                            if (qr_btn.isBoomed()) {
                                qr_btn.reboom();
                            }
                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileMain.this, R.style.CustomDialog);

                            // builder.setMessage("Text not finished...");
                            LayoutInflater inflater = getLayoutInflater();
                            View dialog = inflater.inflate(R.layout.dialog_inputagentcode, null);
                            builder.setView(dialog);
                            final EditText input = dialog.findViewById(R.id.inputcodetext);
                            input.setAllCaps(true);
                            input.setTypeface(type);
                            input.setLines(1);

                            input.setHint("AGENT CODE OR PASSCODE");
                            input.setInputType(InputType.TYPE_CLASS_NUMBER);

                            input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                                @Override
                                public void onFocusChange(View view, boolean b) {
                                    if (b) {
                                        input.setError(null);
                                    }
                                }
                            });
                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                            //localTextView1.setTypeface(proto);
                            builder.setPositiveButton("SEND", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Log.i("Hallo", "hallo");
                                    if (input.getText().toString().equalsIgnoreCase(agentcode.getText().toString().replaceAll(" ", ""))) {
                                        StyleableToast.makeText(getApplicationContext(), "Agent Code is your own code!", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                    } else {
                                        if (input.getText().toString().length() > 3) {
                                            if (input.getText().toString().length() == 5) {
                                                handlepasscode(input.getText().toString());
                                            } else {
                                                handleinputcode(input.getText().toString());
                                            }
                                        } else {
                                            input.setError("to short");
                                            StyleableToast.makeText(getApplicationContext(), "Agent Code is to short!", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                        }
                                    }
                                    // Todo publish the code to database
                                }
                            });

                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                }
                            });

                            builder.setCancelable(true);

                            if (!isFinishing()) {
                                builder.create().show();

                            }
                        } else {
                            Toast.makeText(ProfileMain.this, boomButton.getTextView().getText().toString() + " need a network connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (index == 3) {
                        // SHARE
                        qr_btn.setAutoHide(false);
                        if (qr_btn.isBoomed()) {
                            qr_btn.reboom();
                        }

                        Intent share = new Intent(android.content.Intent.ACTION_SEND);
                        share.setType("text/plain");
                        share.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);

                        // Add data to the intent, the receiving app will decide
                        // what to do with it.
                        share.putExtra(Intent.EXTRA_TITLE, "AOD Agent Code // " + AgentData.getagentname());

                        share.putExtra(Intent.EXTRA_SUBJECT, "My AOD Agent Code (" + AgentData.getagentname() + ") : " + agentcode.getText().toString().replaceAll(" ", ""));
                        share.putExtra(Intent.EXTRA_TEXT, "https://aod.prxenon.rocks/share/" + agentcode.getText().toString().replaceAll(" ", ""));

                        startActivity(Intent.createChooser(share, "Share your AgentCode"));


                    }
                    if (index == 4) {
                        // RESET
                        qr_btn.setAutoHide(false);
                        if (qr_btn.isBoomed()) {
                            qr_btn.reboom();
                        }

                        resetAgentCode();
                    }

                }

                @Override
                public void onBackgroundClick() {

                }

                @Override
                public void onBoomWillHide() {
                    // bmb_settings.setVisibility(View.GONE);
                    // bmb_filter.setVisibility(View.VISIBLE);
                    mTitleTextView.setText(getResources().getString(R.string.title_activity_profile));
                }

                @Override
                public void onBoomDidHide() {

                }


                @Override
                public void onBoomWillShow() {
                    // bmb_settings.setVisibility(View.VISIBLE);
                    //     bmb_filter.setVisibility(View.GONE);
                    mTitleTextView.setText("QR ..MENU");
                }

                @Override
                public void onBoomDidShow() {

                }


            });
        }


        // Log.i("AGENTNAME", pref.getString(API.child1,"null"));
        qr_btn.setBoomEnum(BoomEnum.LINE);
        qr_btn.setShowScaleEaseEnum(EaseEnum.EaseOutBack);

        agentname = findViewById(R.id.agentname);
        agentimg = findViewById(R.id.agentimage);
        agentname2 = findViewById(R.id.agentname2);
        agentimg2 = findViewById(R.id.agentimage2);
        agentname.setTypeface(proto);
        agentname.setText(AgentData.getagentname());
        agentname2.setText(AgentData.getagentname());


        medals_count_field = findViewById(R.id.medals_count_field);
        medals_txt = findViewById(R.id.medals_txt);

        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_profile, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(R.string.title_activity_profile);

        logoutbtn = actionBar.findViewById(R.id.btn_logout);

        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);
        agentcode = findViewById(R.id.agentcode);
        edit_agent_btn = findViewById(R.id.edit_agent_btn);
        edit_faction_btn = findViewById(R.id.edit_faction_btn);
        agentname.setSelected(true);

        share_rank = findViewById(R.id.share_rank);

        agentcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("AOD Agent Code", agentcode.getText().toString().replaceAll(" ", ""));
                clipboard.setPrimaryClip(clip);

                StyleableToast.makeText(getApplicationContext(), "Agent Code copied to clipboard", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

            }
        });

        edit_agent_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                changetheAgentname();
            }
        });

        share_rank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                File pictureFileDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "AOD");
                if (!pictureFileDir.exists()) {
                    boolean isDirectoryCreated = pictureFileDir.mkdirs();
                    if (!isDirectoryCreated)
                        Log.i("TAG", "Can't create directory to save the image");
                    // return null;
                }
                String filename = pictureFileDir.getPath() + File.separator + System.currentTimeMillis() + ".jpg";
                File pictureFile = new File(filename);
                Bitmap b = getScreenShot(findViewById(R.id.edit_agent_change));
                try {
                    pictureFile.createNewFile();
                    FileOutputStream oStream = new FileOutputStream(pictureFile);
                    b.compress(Bitmap.CompressFormat.PNG, 100, oStream);
                    oStream.flush();
                    oStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.i("TAG", "There was an issue saving the image.");
                }


                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                //BitMap to Parsable Uri (needs write permissions)
                // String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), b,"title", null);
                Uri bmpUri = Uri.fromFile(pictureFile);

                //Share the image
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Agent " + AgentData.getagentname() + " AOD Level: " + profil_level.getText().toString() + " #aod #Profile");
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "send"));
                share_rank.setVisibility(View.VISIBLE);
            }
        });

        edit_faction_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                changetheFaction();
            }
        });

        ImageButton scroll_btn = findViewById(R.id.scroll_btn);
        scroll_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (selectedbadge.isSelected()) {
                    OctogonImageView bbx = (OctogonImageView) selectedbadge;
                    bbx.setBorderColor(getColor(R.color.colorPrimaryYellowLight));
                    bbx.setBorderWidth(4);
                    bbx.setSelected(false);


                }
                badgedetails = false;
                badge_container.setVisibility(View.GONE);
            }
        });

        logoutbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  pref.edit().clear().apply();


                AlertDialog.Builder logoutdialog = new AlertDialog.Builder(ProfileMain.this, R.style.CustomDialog);

                // builder.setMessage("Text not finished...");
                LayoutInflater inflater = getLayoutInflater();
                View dialog = inflater.inflate(R.layout.dialog_logout, null);
                logoutdialog.setView(dialog);


                logoutdialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        pref.edit().remove("userexist").apply();
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("reminderweek_monday");
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("reminderweek_sunday");
                        FirebaseMessaging.getInstance().unsubscribeFromTopic("event_channel");
                        FirebaseMessaging.getInstance().subscribeToTopic("aod_news");
                        globalsp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        globalsp.edit().putBoolean("notifyset", false).apply();
                        globalsp.edit().remove("reminderweek_monday").apply();
                        globalsp.edit().remove("reminderweek_sunday").apply();
                        globalsp.edit().remove("event_channel").apply();
                        globalsp.edit().remove("aod_news").apply();

                        ShortcutManager shortcutManager = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
                            shortcutManager = getSystemService(ShortcutManager.class);
                            if (shortcutManager.getDynamicShortcuts().size() > 0) {

                                if (shortcutManager.getManifestShortcuts().get(0) != null) {
                                    ShortcutInfo shortcut = shortcutManager.getManifestShortcuts().get(0);

                                    if (shortcutManager.getManifestShortcuts().get(1) != null) {
                                        ShortcutInfo shortcut2 = shortcutManager.getManifestShortcuts().get(1);

                                        if (shortcutManager.getManifestShortcuts().get(2) != null) {
                                            ShortcutInfo shortcut3 = shortcutManager.getManifestShortcuts().get(2);
                                            shortcutManager.removeDynamicShortcuts(Arrays.asList(shortcut.getId(), shortcut2.getId(), shortcut3.getId()));
                                        } else {
                                            shortcutManager.removeDynamicShortcuts(Arrays.asList(shortcut.getId(), shortcut2.getId()));
                                        }

                                    } else {
                                        shortcutManager.removeDynamicShortcuts(Arrays.asList(shortcut.getId()));
                                    }
                                }


                            }


                        }
                        mDatabase.child(user.getUid()).child("fcm").child("fcmtoken").setValue(null);
                        AuthUI.getInstance()
                                .signOut(ProfileMain.this)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    public void onComplete(@NonNull Task<Void> task) {
                                        Intent service = new Intent(ProfileMain.this, DeleteTokenService.class);
                                        startService(service);
                                        Intent intent = new Intent("rocks.prxenon.aod.fcm.NotificationDismissedReceiver");

                                        sendBroadcast(intent);

                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    FirebaseInstanceId.getInstance().deleteInstanceId();
                                                    Intent i = new Intent(ProfileMain.this, FirebaseUIActivity.class);
                                                    i.putExtra("login", false);
                                                    startActivity(i);
                                                    finish();
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                    Intent i = new Intent(ProfileMain.this, FirebaseUIActivity.class);
                                                    i.putExtra("login", false);
                                                    startActivity(i);
                                                    finish();
                                                }
                                            }
                                        }).start();


                                    }
                                });

                    }
                });

                logoutdialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                logoutdialog.setCancelable(true);

                if (!isFinishing()) {
                    logoutdialog.create().show();

                }


            }
        });

        // bgani = (ImageView) findViewById(R.id.bgani);


        // myRoot.addView(a);

    }

    private void bindActivity() {

        closedframe = findViewById(R.id.closedframe);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;
        if (offset == 0) {
            //Log.i("OFFSET1 --> ", ""+offset);
            setCurrentStateAndNotify(appBarLayout, State.EXPANDED);
            // handleAlphaOnTitle(percentage);
            toolbarx.setVisibility(View.GONE);
            closedframe.setVisibility(View.INVISIBLE);
            agentcode_frame.setVisibility(View.VISIBLE);
            agentcode_frame.animate()
                    .translationY(0)
                    .alpha(1f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            //agentcode_frame.setVisibility(View.GONE);
                        }
                    });
        } else if (Math.abs(offset) >= appBarLayout.getTotalScrollRange()) {
            setCurrentStateAndNotify(appBarLayout, State.COLLAPSED);
            //  handleAlphaOnTitle(percentage);
            //Log.i("OFFSET2 --> ", ""+offset);
            agentcode_frame.animate()
                    .translationY(-100)
                    .alpha(0.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            agentcode_frame.setVisibility(View.GONE);
                        }
                    });
            toolbarx.setVisibility(View.VISIBLE);
            closedframe.setVisibility(View.VISIBLE);
        } else {
            //Log.i("OFFSET3 --> ", ""+offset);
            toolbarx.setVisibility(View.VISIBLE);
            setCurrentStateAndNotify(appBarLayout, State.IDLE);
        }
        // handleToolbarTitleVisibility(percentage);
    }

    private void setCurrentStateAndNotify(AppBarLayout appBarLayout, State state) {
        if (mCurrentState != state) {
            onStateChanged(appBarLayout, state);
        }
        mCurrentState = state;
    }

    public void onStateChanged(AppBarLayout appBarLayout, State state) {

    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(closedframe, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(closedframe, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();
        pref = getSharedPreferences("AppPref", MODE_PRIVATE);
        pref.registerOnSharedPreferenceChangeListener(this);
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        badge_container.setVisibility(View.GONE);
        open_friends_container.setVisibility(View.GONE);
        badgedetails = false;
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);

        loadbadges();
        loadfriends();

        new GetNotifications().getFriendRequestsProfile(this, getApplicationContext(), morefriends, openrequests, open_friends_container);
        qr_btn.setAutoHide(false);
        if (qr_btn.isBoomed()) {
            qr_btn.reboom();
        }

        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorENL));
            agentimg2.setBorderColor(getResources().getColor(R.color.colorENL));
        } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorRES));
            agentimg2.setBorderColor(getResources().getColor(R.color.colorRES));
        } else {
            agentimg.setBorderColor(getResources().getColor(R.color.colorGray));
            agentimg2.setBorderColor(getResources().getColor(R.color.colorGray));
        }
        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getProviderData().get(1).getPhotoUrl().toString() + "?sz=400")
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(agentimg);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }
        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getProviderData().get(1).getPhotoUrl().toString() + "?sz=80")
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(agentimg2);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

        if (getIntent().getAction() != null) {
            Log.i("ACTION -->", getIntent().getAction() + " pom");
            if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.pages.profile.ProfileMain.openqrfromAction")) {
                openqrfromAction();
                getIntent().setAction(null);

            } else if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.pages.profile.ProfileMain.scanqrfromAction")) {
                scanQR();
                getIntent().setAction(null);
            } else if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.pages.profile.ProfileMain.savefromlink")) {

                mDatabaseagentCodes.child(getIntent().getExtras().getString("agentcode")).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override

                    public void onDataChange(DataSnapshot snapshot) {

                        if (snapshot.exists()) {
                            // CODE FOUND IN THE SYSTEM

                            if (snapshot.getValue().toString().equalsIgnoreCase(user.getUid())) {

                                StyleableToast.makeText(getApplicationContext(), "Agent Code is your own code!", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                getIntent().setAction(null);
                            } else {

                                handleinputcode(getIntent().getExtras().getString("agentcode"));

                            }


                        } else {
                            getIntent().setAction(null);
                            coordinator.setVisibility(View.VISIBLE);
                            progressBarHolder.setVisibility(View.GONE);


                            final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                            Snackbar snack = Snackbar.make(rootlayout, "Code not found.. please try another code",
                                    Snackbar.LENGTH_INDEFINITE)
                                    .setAction("OK", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {


                                        }
                                    });

                            snack.show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        getIntent().setAction(null);
                    }
                });


            } else {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                getIntent().setAction(null);


                final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                Snackbar snack = Snackbar.make(rootlayout, "Code not found.. please try another code",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                            }
                        });

                snack.show();
            }
        } else {
            Log.i("no Action", "damn");
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");
        //pref.edit().remove("scanedagnet").apply();

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (newagentsprofile != null) {
            mbfriendsrequest.removeEventListener(newagentsprofile);

        }

        if (newagentsprofileownrequest != null) {
            mDatabase.removeEventListener(newagentsprofileownrequest);

        }

        if (builderfaction != null) {
            if (builderfaction.create().isShowing()) {
                builderfaction.create().dismiss();
                builderfaction = null;
            }
        }

        if (builderagent != null) {
            if (builderagent.create().isShowing()) {
                builderagent.create().dismiss();
                builderagent = null;
            }
        }

        getIntent().setAction(null);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (newagentsprofile != null) {
            mbfriendsrequest.removeEventListener(newagentsprofile);

        }

        if (newagentsprofileownrequest != null) {
            mDatabase.removeEventListener(newagentsprofileownrequest);

        }

        if (builderfaction != null) {
            if (builderfaction.create().isShowing()) {
                builderfaction.create().dismiss();
                builderfaction = null;
            }
        }

        if (builderagent != null) {
            if (builderagent.create().isShowing()) {
                builderagent.create().dismiss();
                builderagent = null;
            }
        }

        getIntent().setAction(null);


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }


        if (builderfaction != null) {
            if (builderfaction.create().isShowing()) {
                builderfaction.create().dismiss();
                builderfaction = null;
            }
        }

        if (builderagent != null) {
            if (builderagent.create().isShowing()) {
                builderagent.create().dismiss();
                builderagent = null;
            }
        }


        AgentData.getscanproof = 0;

        if (qrsavelistener != null) {
            qrdatabase.removeEventListener(qrsavelistener);
            qrsavelistener = null;
        }

        if (listenertempscansave != null) {
            qrdatabase.removeEventListener(listenertempscansave);
            listenertempscansave = null;
        }
        pref.edit().remove("scanedagnet").apply();
        pref.edit().remove("hasalldata").apply();
        pref.edit().remove("isscaned").apply();

        pref.unregisterOnSharedPreferenceChangeListener(this);


        try {
            if (networkStateReceiver != null) {
                this.unregisterReceiver(networkStateReceiver);
                Log.i("", "broadcastReceiver unregistered");
            }
        } catch (Exception e) {
            Log.i("", "broadcastReceiver is already unregistered");
            networkStateReceiver = null;
        }

        if (newagentsprofile != null) {
            mbfriendsrequest.removeEventListener(newagentsprofile);

        }

        if (newagentsprofileownrequest != null) {
            mDatabase.removeEventListener(newagentsprofileownrequest);

        }

        getIntent().setAction(null);

    }

    @Override
    public void networkAvailable() {


        try {
            if (qr_btn.isBoomed()) {
                //  bmb.reboom();
            }

        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            //e.printStackTrace();
        }

        /* TODO: Your connection-oriented stuff here */
    }

    @Override
    public void networkUnavailable() {


        try {
            if (qr_btn.isBoomed()) {
                //  bmb.reboom();
            }


        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            // e.printStackTrace();
        }
        /* TODO: Your disconnection-oriented stuff here */
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {

            case REQUEST_CAM: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    scanQR();
                    // System.out.println("GPS ERLAUBT");

                } else {
                    // item2.setIcon(getResources().getDrawable(R.drawable.ic_action_eye_open));
                    // requestgps();
                    System.out.println("Camera Verboten");
                    final View rootlayout2 = getWindow().getDecorView().findViewById(android.R.id.content);
                    Snackbar snack = Snackbar.make(rootlayout2, "no permission",
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                }
                            });

                    snack.show();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onBackPressed() {

        if (qr_btn.isBoomed()) {

            if (qr_btn.isBoomed()) {
                qr_btn.reboom();
            }
        } else {

            super.onBackPressed();
        }


    }

    private void changetheAgentname() {
        final String uuidis = user.getUid();
        final String currentAgentname = AgentData.getagentname();

        //StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
        builderagent = new AlertDialog.Builder(this, R.style.CustomDialog);

        // builder.setMessage("Text not finished...");
        LayoutInflater inflater = getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_editagentname, null);
        builderagent.setView(dialog);

        final EditText input = dialog.findViewById(R.id.agentname);

        input.setLines(1);
        input.setText(currentAgentname);
        builderagent.setPositiveButton("save", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Objects.requireNonNull(input.getText()).toString().trim().length() < 3 || input.getText().toString().trim().contains(" ") || input.getText().toString().trim().equalsIgnoreCase(currentAgentname)) {
                    if (input.getText().toString().equalsIgnoreCase(currentAgentname)) {
                        input.setError(getResources().getString(R.string.string_agentname_same));
                        StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.string_agentname_same), Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                    } else {
                        input.setError(getResources().getString(R.string.string_bad_agent));
                        StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.string_bad_agent), Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                    }

                } else {
                    coordinator.setVisibility(View.GONE);
                    progressBarHolder.setVisibility(View.VISIBLE);
                    final Map<String, Object> agentupdate = new HashMap<>();
                    agentupdate.put("/agentname", input.getText().toString().trim());
                    agentupdate.put("/oldagentname", currentAgentname);
                    input.setError(null);
                    //agentname.setEnabled(false);

                    mdbroot.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshotexist) {
                            if (snapshotexist.hasChild(input.getText().toString().trim())) {


                                // delaccount.setVisibility(View.VISIBLE);
                                input.setError(getResources().getString(R.string.string_agent_exist));
                            } else {
                                mDatabase.child(user.getUid()).updateChildren(agentupdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            new AgentData("agentname", input.getText().toString().trim());
                                            StyleableToast.makeText(getApplicationContext(), "Agentname updated", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                            input.setError(null);

                                            mdbroot.child(input.getText().toString().trim()).setValue(user.getUid()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {

                                                        mdbroot.child(currentAgentname).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    agentname.setText(input.getText().toString().trim());
                                                                    agentname2.setText(input.getText().toString().trim());
                                                                    coordinator.setVisibility(View.VISIBLE);
                                                                    progressBarHolder.setVisibility(View.GONE);
                                                                    recreate();
                                                                } else if (task.isCanceled()) {
                                                                    coordinator.setVisibility(View.VISIBLE);
                                                                    progressBarHolder.setVisibility(View.GONE);
                                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1088 Login error ");
                                                                }
                                                            }

                                                        }).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(Exception e) {
                                                                coordinator.setVisibility(View.VISIBLE);
                                                                progressBarHolder.setVisibility(View.GONE);
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

                                                            }
                                                        });
                                                    } else if (task.isCanceled()) {
                                                        coordinator.setVisibility(View.VISIBLE);
                                                        progressBarHolder.setVisibility(View.GONE);
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 1088 Login error ");
                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    coordinator.setVisibility(View.VISIBLE);
                                                    progressBarHolder.setVisibility(View.GONE);
                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

                                                }
                                            });
                                        } else if (task.isCanceled()) {

                                            coordinator.setVisibility(View.VISIBLE);
                                            progressBarHolder.setVisibility(View.GONE);
                                            input.setError("try again");
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 506 Login error " + e.getMessage());
                                        //  importfromscreen.setVisibility(View.VISIBLE);
                                        coordinator.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);
                                        input.setError("try again");
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            coordinator.setVisibility(View.VISIBLE);
                            progressBarHolder.setVisibility(View.GONE);
                            StyleableToast.makeText(getApplicationContext(), "Error try again", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            Log.i("Error DB -->", this.getClass().getSimpleName() + " 407 " + databaseError.getMessage());
                        }
                    });
                }
                // Todo publish the code to database
            }
        });

        builderagent.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                // Todo publish the code to database
            }
        });

        builderagent.setCancelable(true);

        if (!isFinishing()) {
            builderagent.create().show();

        }


    }


    private void changetheFaction() {

        final String uuidis = user.getUid();
        final String currentAgentname = AgentData.getagentname();
        final String prefaction = AgentData.getFaction();
        //StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
        builderfaction = new AlertDialog.Builder(this, R.style.CustomDialog);

        // builder.setMessage("Text not finished...");
        LayoutInflater inflater = getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_changefaction, null);
        builderfaction.setView(dialog);
        //final EditText input = (EditText) dialog.findViewById(R.id.agentname);
        final RadioRealButtonGroup factiongroup = dialog.findViewById(R.id.groupradiofaction);
        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
            factiongroup.setPosition(0, true);
            factiongroup.setSelectorColor(getColor(R.color.colorENL));
        } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
            factiongroup.setPosition(2, true);
            factiongroup.setSelectorColor(getColor(R.color.colorRES));
        } else {
            factiongroup.setPosition(1, true);
            factiongroup.setSelectorColor(getColor(R.color.colorGray));
        }


        factiongroup.setOnPositionChangedListener(new RadioRealButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(RadioRealButton button, int currentPosition, int lastPosition) {
                if (currentPosition == 0) {
                    factiongroup.setSelectorColor(getColor(R.color.colorENL));
                } else if (currentPosition == 1) {
                    factiongroup.setSelectorColor(getColor(R.color.colorGray));
                } else {
                    factiongroup.setSelectorColor(getColor(R.color.colorRES));
                }
            }
        });


        builderfaction.setPositiveButton("save", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                coordinator.setVisibility(View.GONE);
                progressBarHolder.setVisibility(View.VISIBLE);
                final String faction;
                if (factiongroup.getPosition() == 0) {
                    faction = "ENLIGHTENED";
                } else if (factiongroup.getPosition() == 1) {
                    faction = "ANONYM";
                } else {

                    faction = "RESISTANCE";
                }
                final Map<String, Object> agentupdate2 = new HashMap<>();

                agentupdate2.put("/faction", faction);

                mDatabase.child(user.getUid()).updateChildren(agentupdate2).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            //finish();
                            new AgentData("faction", faction);
                            coordinator.setVisibility(View.VISIBLE);
                            progressBarHolder.setVisibility(View.GONE);
                            recreate();


                        } else if (task.isCanceled()) {
                            coordinator.setVisibility(View.VISIBLE);
                            progressBarHolder.setVisibility(View.GONE);
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 605 Login abgebrochen");

                        }
                    }

                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        coordinator.setVisibility(View.VISIBLE);
                        progressBarHolder.setVisibility(View.GONE);
                        Log.w("ERROR ", this.getClass().getSimpleName() + " 606 Login error " + e.getMessage());

                    }
                });

            }
        });

        builderfaction.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);


            }
        });

        builderfaction.setCancelable(true);

        if (!isFinishing()) {
            builderfaction.create().show();

        }


    }

    private String getCharForNumber(int i) {
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        Log.i("NUM2 -->", ":" + i);
        if (i > 25) {
            return null;
        }
        return Character.toString(alphabet[i]);
    }

    private void resetAgentCode() {
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);
        qr_btn.setAutoHide(false);
        if (qr_btn.isBoomed()) {
            qr_btn.reboom();
        }
        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotbadges) {

                // START DATA AGENT CODE

                String codecurrent;
                Long tsLong = System.currentTimeMillis() / 100;
                final String ts = tsLong.toString();
                if (snapshotbadges.hasChild("agentcode")) {


                    codecurrent = snapshotbadges.child("agentcode").getValue().toString();
                    mDatabaseagentCodes.child(codecurrent).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                String challidis = "";
                                for (int i = 0; i < ts.length(); i++) {

                                    Log.i("NUM--->", ": " + ts.charAt(i));
                                    challidis += getCharForNumber(Integer.parseInt("" + ts.charAt(i)));
                                    //challidis +=  css.charAt(ts.charAt(i));
                                    //challidis += Integer.toBinaryString(Integer.parseInt(""+ts.charAt(i)));
                                }
                                // progressBar = (ProgressBar) findViewById(R.id.progressBar);
                                Log.i("Code 2", "" + (getRandomString(8) + challidis));
                                challidis = challidis + getRandomString(8);

                                Log.i("CODE -->", challidis);

                                agentcode.setText(String.valueOf(Math.abs(challidis.hashCode())).replaceAll("...", "$0 "));


                                final Map<String, Object> agentCodeUpdate = new HashMap<>();
                                // IF hasmissions
                                agentCodeUpdate.put("/agentcode", String.valueOf(Math.abs(challidis.hashCode())));


                                final String finalChallidis = challidis;
                                mDatabase.child(user.getUid()).updateChildren(agentCodeUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            mDatabaseagentCodes.child(String.valueOf(Math.abs(finalChallidis.hashCode()))).setValue(user.getUid()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {

                                                        coordinator.setVisibility(View.VISIBLE);
                                                        progressBarHolder.setVisibility(View.GONE);
                                                        StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.string_new_code), Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                                    } else if (task.isCanceled()) {
                                                        coordinator.setVisibility(View.VISIBLE);
                                                        progressBarHolder.setVisibility(View.GONE);
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 1055 Login abgebrochen");

                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    coordinator.setVisibility(View.VISIBLE);
                                                    progressBarHolder.setVisibility(View.GONE);
                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

                                                }
                                            });

                                        } else if (task.isCanceled()) {
                                            coordinator.setVisibility(View.VISIBLE);
                                            progressBarHolder.setVisibility(View.GONE);
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                                        } else {
                                            coordinator.setVisibility(View.VISIBLE);
                                            progressBarHolder.setVisibility(View.GONE);
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        coordinator.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");


                                    }
                                });

                            } else if (task.isCanceled()) {
                                coordinator.setVisibility(View.VISIBLE);
                                progressBarHolder.setVisibility(View.GONE);
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 1088 Login error ");
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            coordinator.setVisibility(View.VISIBLE);
                            progressBarHolder.setVisibility(View.GONE);
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

                        }
                    });
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                //StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
    }


    private void loadfriends() {
        friendid.clear();
        friendstatus.clear();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int newwidth = ((size.x) / 5) - 10;


        final FlowLayout myRootfriends = findViewById(R.id.my_friends);
        myRootfriends.removeAllViews();
        //   LinearLayout a = new LinearLayout(this);

        myRootfriends.setOrientation(LinearLayout.HORIZONTAL);

        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg_pattern);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        myRootfriends.setBackgroundDrawable(bitmapDrawable);

        final LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View view2 = inflater.inflate(R.layout.list_nofriends_adapter, myRootfriends, false);
        // set item content in view
        final TextView mybadge = view2.findViewById(R.id.nofriends);
        mybadge.setText("please wait...");
        myRootfriends.addView(view2);
        int lastagents = 8;
        mDatabase.child(user.getUid()).child("friends").limitToLast(lastagents).orderByChild("timestamp").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshotfriends) {

                if (snapshotfriends.hasChildren()) {
                    int countfriends = 0;
                    int countall = 0;
                    for (DataSnapshot child : snapshotfriends.getChildren()) {
                        countall++;

                        if (child.child("agentuid").exists() && Boolean.parseBoolean(child.child("status").getValue().toString())) {

                            if (countfriends < 8 || snapshotfriends.getChildrenCount() < countall) {
                                countfriends++;
                                friendid.add(child.child("agentuid").getValue().toString());
                                friendstatus.add(Boolean.parseBoolean(child.child("status").getValue().toString()));
                            }
                        }

                        //  Log.i("TEST LOG--->",child.getValue().toString());

                    }

                    if (countfriends == 8 || snapshotfriends.getChildrenCount() == countall) {

                        //2. durchlauf weniger 8 ergebnisse
                        if (countfriends < 8 && snapshotfriends.getChildrenCount() == 8) {
                            friendid.clear();
                            friendstatus.clear();

                            int lastagents2 = 9;
                            mDatabase.child(user.getUid()).child("friends").limitToLast(lastagents2).orderByChild("timestamp").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(final DataSnapshot snapshotfriends) {

                                    if (snapshotfriends.hasChildren()) {
                                        int countfriends = 0;
                                        int countall = 0;
                                        for (DataSnapshot child : snapshotfriends.getChildren()) {
                                            countall++;

                                            if (child.child("agentuid").exists() && Boolean.parseBoolean(child.child("status").getValue().toString())) {

                                                if (countfriends < 8 || snapshotfriends.getChildrenCount() < countall) {
                                                    countfriends++;
                                                    friendid.add(child.child("agentuid").getValue().toString());
                                                    friendstatus.add(Boolean.parseBoolean(child.child("status").getValue().toString()));
                                                }
                                            }

                                            //  Log.i("TEST LOG--->",child.getValue().toString());

                                        }

                                        if (countfriends == 8 || snapshotfriends.getChildrenCount() == countall) {

                                            // if(countfriends < 8 && snapshotfriends)
                                            Collections.reverse(friendid);
                                            Collections.reverse(friendstatus);
                                            final int sizex = friendid.size();

                                            if (sizex > 0) {

                                                for (int i = 0; i < sizex; i++) {

                                                    final int finalI = i;
                                                    mDatabase.child(friendid.get(finalI)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(final DataSnapshot snapshotfriendsdetails) {

                                                            if (snapshotfriendsdetails.exists()) {
                                                                myRootfriends.removeAllViews();
                                                                myRootfriends.setOrientation(LinearLayout.HORIZONTAL);
                                                                if (friendstatus.get(finalI)) {
                                                                    mDatabase.child(friendid.get(finalI)).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(final DataSnapshot snapshotagentimage) {
                                                                            View view = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                                                                            // set item content in view
                                                                            final OctogonImageView mybadge = view.findViewById(R.id.badgeimage);
                                                                            mybadge.getLayoutParams().width = newwidth;
                                                                            mybadge.getLayoutParams().height = newwidth;
                                                                            final TextView friendname = view.findViewById(R.id.friendname);

                                                                            friendname.setText(snapshotfriendsdetails.getValue().toString());
                                                                            friendname.setSelected(true);

                                                                            try {
                                                                                PicassoCache.getPicassoInstance(getApplicationContext())
                                                                                        .load(snapshotagentimage.getValue().toString() + "?sz=200")
                                                                                        .placeholder(R.drawable.full_logo)
                                                                                        .error(R.drawable.full_logo)

                                                                                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                                        //.transform(new CropRoundTransformation())
                                                                                        .into(mybadge);
                                                                                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                                            } catch (Exception e) {
                                                                                //Log.e("Error", e.getMessage());

                                                                                e.printStackTrace();
                                                                            }


                                                                            mybadge.setBorderWidth(4);
                                                                            mybadge.setBorderColor(getColor(R.color.colorLightBrown));

                                                                            mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                                                            mybadge.setPadding(8, 8, 8, 8);

                                                                            mybadge.setOnLongClickListener(new View.OnLongClickListener() {

                                                                                @Override
                                                                                public boolean onLongClick(View v) {
                                                                                    Log.i("LOOONGPRESS", "true ");


                                                                                    //  removeFriend(friendid.get(finalI), snapshotagentimage.getValue().toString());
                                                                                    return true;
                                                                                }
                                                                            });
                                                                            mybadge.setOnClickListener(new View.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(View v) {
                                                                                    //your stuff
                                                                                    // Log.i("SinglePress", "true");
                                                                                    final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ProfileMain.this);
                                                                                    View sheetView = getLayoutInflater().inflate(R.layout.dialog_friends_action, null);
                                                                                    mBottomSheetDialog.setContentView(sheetView);
                                                                                    mBottomSheetDialog.show();

                                                                                    LinearLayout action_del = sheetView.findViewById(R.id.friendaction_delete);
                                                                                    action_del.setOnClickListener(new View.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(View view) {
                                                                                            mBottomSheetDialog.dismiss();
                                                                                            //StyleableToast.makeText(getApplicationContext(), "DEL", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                                            removeFriend(friendid.get(finalI), snapshotagentimage.getValue().toString());
                                                                                        }
                                                                                    });

                                                                                    LinearLayout action_pvp = sheetView.findViewById(R.id.friendaction_pvp);
                                                                                    action_pvp.setOnClickListener(new View.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(View view) {


                                                                                            mDatabase.child(user.getUid()).child("pvp").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                                long countpvps = 0;
                                                                                                long activepvp = 0;

                                                                                                @Override
                                                                                                public void onDataChange(DataSnapshot snapshotpvps) {

                                                                                                    if (snapshotpvps.hasChildren()) {

                                                                                                        for (DataSnapshot childpvp : snapshotpvps.getChildren()) {
                                                                                                            countpvps++;
                                                                                                            if (!childpvp.hasChild("finished")) {
                                                                                                                activepvp++;
                                                                                                            }

                                                                                                            if (countpvps >= snapshotpvps.getChildrenCount()) {
                                                                                                                if (activepvp >= 3) {
                                                                                                                    mBottomSheetDialog.dismiss();
                                                                                                                    StyleableToast.makeText(getApplicationContext(), "Sorry, you can only have 3 active / pending PvPs.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                                                                } else {
                                                                                                                    // StyleableToast.makeText(getApplicationContext(), "No friends found... only PvP by invite by link possible.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                                                                                    Intent openPvP = new Intent(ProfileMain.this, PvPCreate.class);
                                                                                                                    openPvP.putExtra("agentidpvp", friendid.get(finalI));
                                                                                                                    openPvP.putExtra("agentnamepvp", friendname.getText().toString());
                                                                                                                    startActivity(openPvP);
                                                                                                                    finish();

                                                                                                                }
                                                                                                            }
                                                                                                        }

                                                                                                    } else {
                                                                                                        if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {

                                                                                                            // StyleableToast.makeText(getApplicationContext(), "No friends found... only PvP by invite by link possible.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                                                                            Intent openPvP = new Intent(ProfileMain.this, PvPCreate.class);
                                                                                                            openPvP.putExtra("agentidpvp", friendid.get(finalI));
                                                                                                            openPvP.putExtra("agentnamepvp", friendname.getText().toString());
                                                                                                            startActivity(openPvP);
                                                                                                            finish();


                                                                                                        } else {
                                                                                                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileMain.this, R.style.CustomDialog);

                                                                                                            // builder.setMessage("Text not finished...");
                                                                                                            LayoutInflater inflater = getLayoutInflater();
                                                                                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                                                                                            builder.setView(dialog);
                                                                                                            final TextView input = dialog.findViewById(R.id.caltext);
                                                                                                            // input.setAllCaps(true);
                                                                                                            input.setTypeface(type);

                                                                                                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                                                                                            //localTextView1.setTypeface(proto);
                                                                                                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                                                                                @Override
                                                                                                                public void onClick(DialogInterface dialog, int which) {

                                                                                                                    // Log.i("Hallo", "hallo");
                                                                                                                    //handleinputcode(input.getText().toString());
                                                                                                                    // Todo publish the code to database
                                                                                                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                                                                                    //i.putExtra("PersonID", personID);
                                                                                                                    startActivity(i);
                                                                                                                    finish();

                                                                                                                }
                                                                                                            });

                                                                                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                                                                                @Override
                                                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                                                    mBottomSheetDialog.dismiss();

                                                                                                                    // Todo publish the code to database
                                                                                                                }
                                                                                                            });

                                                                                                            builder.setCancelable(true);

                                                                                                            if (!isFinishing()) {
                                                                                                                builder.create().show();

                                                                                                            }

                                                                                                        }
                                                                                                    }


                                                                                                }

                                                                                                @Override
                                                                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                                                    mBottomSheetDialog.dismiss();
                                                                                                }
                                                                                            });


                                                                                            // StyleableToast.makeText(getApplicationContext(), "PVP Soon", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                                        }
                                                                                    });

                                                                                    LinearLayout action_open = sheetView.findViewById(R.id.friendaction_open);
                                                                                    action_open.setOnClickListener(new View.OnClickListener() {
                                                                                        @Override
                                                                                        public void onClick(View view) {
                                                                                            mBottomSheetDialog.dismiss();
                                                                                            StyleableToast.makeText(getApplicationContext(), "Profile soon", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                                        }
                                                                                    });

                                                                                }
                                                                            });
                                                                            myRootfriends.addView(view);
                                                                        }

                                                                        @Override
                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                            coordinator.setVisibility(View.VISIBLE);
                                                                            progressBarHolder.setVisibility(View.GONE);
                                                                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                                        }
                                                                    });
                                                                } else {
                                                                    if (sizex - 1 <= 0) {
                                                                        myRootfriends.removeAllViews();
                                                                        View view = inflater.inflate(R.layout.list_nofriends_adapter, myRootfriends, false);
                                                                        // set item content in view
                                                                        final TextView mybadge = view.findViewById(R.id.nofriends);

                                                                        myRootfriends.addView(view);
                                                                    }
                                                                }
                                                            } else {
                                                                mDatabase.child(user.getUid()).child("friends").child(friendid.get(finalI)).setValue(null);
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            coordinator.setVisibility(View.VISIBLE);
                                                            progressBarHolder.setVisibility(View.GONE);
                                                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        }
                                                    });

                                                }
                                            } else {
                                                StyleableToast.makeText(getApplicationContext(), "no friends", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                            }
                                        }


                                    } else {

                                        myRootfriends.removeAllViews();
                                        View view = inflater.inflate(R.layout.list_nofriends_adapter, myRootfriends, false);
                                        // set item content in view
                                        final TextView mybadge = view.findViewById(R.id.nofriends);

                                        myRootfriends.addView(view);
                                    }

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    coordinator.setVisibility(View.VISIBLE);
                                    progressBarHolder.setVisibility(View.GONE);
                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                }
                            });

                        } else {
                            Collections.reverse(friendid);
                            Collections.reverse(friendstatus);
                            final int sizex = friendid.size();

                            if (sizex > 0) {

                                for (int i = 0; i < sizex; i++) {

                                    final int finalI = i;
                                    mDatabase.child(friendid.get(finalI)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(final DataSnapshot snapshotfriendsdetails) {

                                            if (snapshotfriendsdetails.exists()) {
                                                myRootfriends.removeAllViews();
                                                myRootfriends.setOrientation(LinearLayout.HORIZONTAL);
                                                if (friendstatus.get(finalI)) {
                                                    mDatabase.child(friendid.get(finalI)).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(final DataSnapshot snapshotagentimage) {
                                                            View view = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                                                            // set item content in view
                                                            final OctogonImageView mybadge = view.findViewById(R.id.badgeimage);
                                                            mybadge.getLayoutParams().width = newwidth;
                                                            mybadge.getLayoutParams().height = newwidth;
                                                            final TextView friendname = view.findViewById(R.id.friendname);

                                                            friendname.setText(snapshotfriendsdetails.getValue().toString());
                                                            friendname.setSelected(true);

                                                            try {
                                                                PicassoCache.getPicassoInstance(getApplicationContext())
                                                                        .load(snapshotagentimage.getValue().toString() + "?sz=200")
                                                                        .placeholder(R.drawable.full_logo)
                                                                        .error(R.drawable.full_logo)

                                                                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                        //.transform(new CropRoundTransformation())
                                                                        .into(mybadge);
                                                                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                            } catch (Exception e) {
                                                                //Log.e("Error", e.getMessage());

                                                                e.printStackTrace();
                                                            }


                                                            mybadge.setBorderWidth(4);
                                                            mybadge.setBorderColor(getColor(R.color.colorLightBrown));

                                                            mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                                            mybadge.setPadding(8, 8, 8, 8);

                                                            mybadge.setOnLongClickListener(new View.OnLongClickListener() {

                                                                @Override
                                                                public boolean onLongClick(View v) {
                                                                    Log.i("LOOONGPRESS", "true ");


                                                                    //  removeFriend(friendid.get(finalI), snapshotagentimage.getValue().toString());
                                                                    return true;
                                                                }
                                                            });
                                                            mybadge.setOnClickListener(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View v) {
                                                                    //your stuff
                                                                    // Log.i("SinglePress", "true");
                                                                    final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ProfileMain.this);
                                                                    View sheetView = getLayoutInflater().inflate(R.layout.dialog_friends_action, null);
                                                                    mBottomSheetDialog.setContentView(sheetView);
                                                                    mBottomSheetDialog.show();

                                                                    LinearLayout action_del = sheetView.findViewById(R.id.friendaction_delete);
                                                                    action_del.setOnClickListener(new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View view) {
                                                                            mBottomSheetDialog.dismiss();
                                                                            //StyleableToast.makeText(getApplicationContext(), "DEL", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                            removeFriend(friendid.get(finalI), snapshotagentimage.getValue().toString());
                                                                        }
                                                                    });

                                                                    LinearLayout action_pvp = sheetView.findViewById(R.id.friendaction_pvp);
                                                                    action_pvp.setOnClickListener(new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View view) {


                                                                            mDatabase.child(user.getUid()).child("pvp").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                long countpvps = 0;
                                                                                long activepvp = 0;

                                                                                @Override
                                                                                public void onDataChange(DataSnapshot snapshotpvps) {

                                                                                    if (snapshotpvps.hasChildren()) {

                                                                                        for (DataSnapshot childpvp : snapshotpvps.getChildren()) {
                                                                                            countpvps++;
                                                                                            if (!childpvp.hasChild("finished")) {
                                                                                                activepvp++;
                                                                                            }

                                                                                            if (countpvps >= snapshotpvps.getChildrenCount()) {
                                                                                                if (activepvp >= 3) {
                                                                                                    mBottomSheetDialog.dismiss();
                                                                                                    StyleableToast.makeText(getApplicationContext(), "Sorry, you can only have 3 active / pending PvPs.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                                                } else {
                                                                                                    // StyleableToast.makeText(getApplicationContext(), "No friends found... only PvP by invite by link possible.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                                                                    Intent openPvP = new Intent(ProfileMain.this, PvPCreate.class);
                                                                                                    openPvP.putExtra("agentidpvp", friendid.get(finalI));
                                                                                                    openPvP.putExtra("agentnamepvp", friendname.getText().toString());
                                                                                                    startActivity(openPvP);
                                                                                                    finish();

                                                                                                }
                                                                                            }
                                                                                        }

                                                                                    } else {
                                                                                        if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {

                                                                                            // StyleableToast.makeText(getApplicationContext(), "No friends found... only PvP by invite by link possible.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                                                            Intent openPvP = new Intent(ProfileMain.this, PvPCreate.class);
                                                                                            openPvP.putExtra("agentidpvp", friendid.get(finalI));
                                                                                            openPvP.putExtra("agentnamepvp", friendname.getText().toString());
                                                                                            startActivity(openPvP);
                                                                                            finish();


                                                                                        } else {
                                                                                            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileMain.this, R.style.CustomDialog);

                                                                                            // builder.setMessage("Text not finished...");
                                                                                            LayoutInflater inflater = getLayoutInflater();
                                                                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                                                                            builder.setView(dialog);
                                                                                            final TextView input = dialog.findViewById(R.id.caltext);
                                                                                            // input.setAllCaps(true);
                                                                                            input.setTypeface(type);

                                                                                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                                                                            //localTextView1.setTypeface(proto);
                                                                                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                                                                @Override
                                                                                                public void onClick(DialogInterface dialog, int which) {

                                                                                                    // Log.i("Hallo", "hallo");
                                                                                                    //handleinputcode(input.getText().toString());
                                                                                                    // Todo publish the code to database
                                                                                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                                                                    //i.putExtra("PersonID", personID);
                                                                                                    startActivity(i);
                                                                                                    finish();

                                                                                                }
                                                                                            });

                                                                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                                                                @Override
                                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                                    mBottomSheetDialog.dismiss();

                                                                                                    // Todo publish the code to database
                                                                                                }
                                                                                            });

                                                                                            builder.setCancelable(true);

                                                                                            if (!isFinishing()) {
                                                                                                builder.create().show();

                                                                                            }

                                                                                        }
                                                                                    }


                                                                                }

                                                                                @Override
                                                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                                    mBottomSheetDialog.dismiss();
                                                                                }
                                                                            });


                                                                            // StyleableToast.makeText(getApplicationContext(), "PVP Soon", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                        }
                                                                    });

                                                                    LinearLayout action_open = sheetView.findViewById(R.id.friendaction_open);
                                                                    action_open.setOnClickListener(new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View view) {
                                                                            mBottomSheetDialog.dismiss();
                                                                            StyleableToast.makeText(getApplicationContext(), "Profile soon", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                        }
                                                                    });

                                                                }
                                                            });
                                                            myRootfriends.addView(view);
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            coordinator.setVisibility(View.VISIBLE);
                                                            progressBarHolder.setVisibility(View.GONE);
                                                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        }
                                                    });
                                                } else {
                                                    if (sizex - 1 <= 0) {
                                                        myRootfriends.removeAllViews();
                                                        View view = inflater.inflate(R.layout.list_nofriends_adapter, myRootfriends, false);
                                                        // set item content in view
                                                        final TextView mybadge = view.findViewById(R.id.nofriends);

                                                        myRootfriends.addView(view);
                                                    }
                                                }
                                            } else {
                                                mDatabase.child(user.getUid()).child("friends").child(friendid.get(finalI)).setValue(null);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            coordinator.setVisibility(View.VISIBLE);
                                            progressBarHolder.setVisibility(View.GONE);
                                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                        }
                                    });

                                }
                            } else {
                                StyleableToast.makeText(getApplicationContext(), "no friends", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }
                        }
                    }


                } else {

                    myRootfriends.removeAllViews();
                    View view = inflater.inflate(R.layout.list_nofriends_adapter, myRootfriends, false);
                    // set item content in view
                    final TextView mybadge = view.findViewById(R.id.nofriends);

                    myRootfriends.addView(view);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
    }

    private void loadbadges() {

        // TODO BETA BADGE LISTNER
        badgesnames.clear();
        badgesdrawable.clear();
        badgescontent.clear();
        badgesstufen.clear();
        badgeskey.clear();

        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);

        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotbadges) {

                // START DATA AGENT CODE
                String challidis = "";
                Long tsLong = System.currentTimeMillis() / 100;
                String ts = tsLong.toString();

                if (snapshotbadges.hasChild("agentcode")) {
                    agentcode.setText(snapshotbadges.child("agentcode").getValue().toString().replaceAll("...", "$0 "));
                } else {
                    for (int i = 0; i < ts.length(); i++) {

                        Log.i("NUM--->", ": " + ts.charAt(i));
                        challidis += getCharForNumber(Integer.parseInt("" + ts.charAt(i)));
                        //challidis +=  css.charAt(ts.charAt(i));
                        //challidis += Integer.toBinaryString(Integer.parseInt(""+ts.charAt(i)));
                    }
                    // progressBar = (ProgressBar) findViewById(R.id.progressBar);
                    Log.i("Code 2", "" + (getRandomString(8) + challidis));
                    challidis = challidis + getRandomString(8);

                    Log.i("CODE -->", challidis);

                    agentcode.setText(String.valueOf(Math.abs(challidis.hashCode())).replaceAll("...", "$0 "));


                    final Map<String, Object> agentCodeUpdate = new HashMap<>();
                    // IF hasmissions
                    agentCodeUpdate.put("/agentcode", String.valueOf(Math.abs(challidis.hashCode())));


                    final String finalChallidis = challidis;
                    mDatabase.child(user.getUid()).updateChildren(agentCodeUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                mDatabaseagentCodes.child(String.valueOf(Math.abs(finalChallidis.hashCode()))).setValue(user.getUid()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {


                                        } else if (task.isCanceled()) {

                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 1055 Login abgebrochen");

                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

                                    }
                                });

                            } else if (task.isCanceled()) {
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                            } else {
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");


                        }
                    });
                }

                // START DATA BADGES
                long badge_gold = 0;
                long badge_platin = 0;
                long badge_onyx = 0;
                long badges_all = 0;
                long badgeslasttime = 0;
                long pointsall = 0;

                final int[] specialcount = {0};

                Log.i("CurrentTimestamp ", "" + System.currentTimeMillis() / 1000);

                if (Long.parseLong(snapshotbadges.child("accountcreated").getValue().toString()) < (System.currentTimeMillis() / 1000)) {
                    badgesnames.add("Alpha");
                    badgesdrawable.add("badge_alpha");
                    badgescontent.add("alpha user");
                    badgesstufen.add(false);
                    badgeskey.add("alpha");

                }


                if (snapshotbadges.hasChild("badges")) {
                    badges_all = snapshotbadges.child("badges").getChildrenCount();
                    boolean hasspecials = false;


                    // FOR THE POINTS START


                    Calendar now = Calendar.getInstance(Locale.getDefault());
                    // now.setTime(date);
                    now.setTimeZone(TimeZone.getDefault());
                    now.setFirstDayOfWeek(Calendar.MONDAY);
                    int currentweek = now.get(Calendar.WEEK_OF_YEAR);
                    String cweeksend;
                    if (currentweek <= 9) {
                        currentweek = Integer.parseInt("0" + currentweek);
                        cweeksend = "0" + currentweek;
                    } else {
                        cweeksend = String.valueOf(currentweek);
                    }

                    int weeks_extra_year = 12 - currentweek;

                    int weeks_extra_year_looper = 12 - currentweek;

                    String theyearnow = String.valueOf(Calendar.getInstance().get(Calendar.YEAR) % 100);
                    String theyearlast = String.valueOf((Calendar.getInstance().get(Calendar.YEAR) % 100) - 1);


                    // DATE FOR POINTS END

                    for (DataSnapshot agentbadges : snapshotbadges.child("badges").getChildren()) {
                        //  Log.i("timestamp", historyis.getValue().toString());


                        if (agentbadges.child("badge").getValue().toString().equalsIgnoreCase("GOLD")) {
                            badge_gold++;
                            if (badge_gold == 1) {
                                badgesnames.add("Gold Medals");
                                badgesdrawable.add("badge_gold");
                                badgescontent.add("Gold in the week and month missions");
                                badgesstufen.add(true);
                                badgeskey.add("gold");

                            }
                            specialcount[0]++;

                            // START POINTS
                            if (weeks_extra_year > 0 && weeks_extra_year_looper > 0) {
                                // TODO GEHT ONLY LAST KW
                                weeks_extra_year_looper--;
                            }


                            // WEEK LAST YEAR
                            if (agentbadges.getKey().startsWith("W" + theyearlast)) {

                                pointsall = pointsall + 2;


                            }

                            // SPECIAL
                            if (agentbadges.getKey().endsWith("AP") || agentbadges.getKey().equalsIgnoreCase("AURORA") || agentbadges.getKey().startsWith("E")) {

                                pointsall = pointsall + 15;


                            }

                            // MONTH LAST YEAR
                            if (agentbadges.getKey().startsWith("M" + theyearlast)) {

                                pointsall = pointsall + 8;


                            }

                            // WEEK YEAR NOW
                            if (agentbadges.getKey().startsWith("W" + theyearnow)) {

                                pointsall = pointsall + 8;

                            }

                            // MONTH YEAR NOW
                            if (agentbadges.getKey().startsWith("M" + theyearnow)) {

                                pointsall = pointsall + 16;

                            }


                        }
                        if (agentbadges.child("badge").getValue().toString().equalsIgnoreCase("PLATIN")) {
                            badge_platin++;
                            badge_gold++;
                            specialcount[0]++;
                            if (badge_gold == 1) {
                                badgesnames.add("Gold Medals");
                                badgesdrawable.add("badge_gold");
                                badgescontent.add("Gold in the week and month missions");
                                badgesstufen.add(true);
                                badgeskey.add("gold");

                            }

                            if (badge_platin == 1) {
                                badgesnames.add("Platin Medals");
                                badgesdrawable.add("badge_platin");
                                badgescontent.add("Platin in the week and month missions");
                                badgesstufen.add(true);
                                badgeskey.add("platin");

                            }


                            // WEEK LAST YEAR
                            if (agentbadges.getKey().startsWith("W" + theyearlast)) {

                                pointsall = pointsall + 4;


                            }

                            // MONTH LAST YEAR
                            if (agentbadges.getKey().startsWith("M" + theyearlast)) {

                                pointsall = pointsall + 16;


                            }

                            // SPECIAL
                            if (agentbadges.getKey().endsWith("AP") || agentbadges.getKey().equalsIgnoreCase("AURORA") || agentbadges.getKey().startsWith("E")) {

                                pointsall = pointsall + 30;


                            }

                            // WEEK YEAR NOW
                            if (agentbadges.getKey().startsWith("W" + theyearnow)) {

                                pointsall = pointsall + 16;

                            }

                            // MONTH YEAR NOW
                            if (agentbadges.getKey().startsWith("M" + theyearnow)) {

                                pointsall = pointsall + 32;

                            }

                        }
                        if (agentbadges.child("badge").getValue().toString().equalsIgnoreCase("ONYX")) {
                            badge_onyx++;
                            badge_platin++;
                            badge_gold++;
                            specialcount[0]++;

                            if (badge_gold == 1) {
                                badgesnames.add("Gold Medals");
                                badgesdrawable.add("badge_gold");
                                badgescontent.add("Gold in the week and month missions");
                                badgesstufen.add(true);
                                badgeskey.add("gold");

                            }

                            if (badge_platin == 1) {
                                badgesnames.add("Platin Medals");
                                badgesdrawable.add("badge_platin");
                                badgescontent.add("Platin in the week and month missions");
                                badgesstufen.add(true);
                                badgeskey.add("platin");

                            }

                            if (badge_onyx == 1) {
                                badgesnames.add("Onyx Medals");
                                badgesdrawable.add("badge_onyx");
                                badgescontent.add("Onyx in the week and month missions");
                                badgesstufen.add(true);
                                badgeskey.add("onyx");

                            }

                            // WEEK LAST YEAR
                            if (agentbadges.getKey().startsWith("W" + theyearlast)) {

                                pointsall = pointsall + 8;


                            }

                            // MONTH LAST YEAR
                            if (agentbadges.getKey().startsWith("M" + theyearlast)) {

                                pointsall = pointsall + 32;


                            }

                            // SPECIAL
                            if (agentbadges.getKey().endsWith("AP") || agentbadges.getKey().equalsIgnoreCase("AURORA") || agentbadges.getKey().startsWith("E")) {

                                pointsall = pointsall + 60;


                            }

                            // WEEK YEAR NOW
                            if (agentbadges.getKey().startsWith("W" + theyearnow)) {

                                pointsall = pointsall + 32;

                            }

                            // MONTH YEAR NOW
                            if (agentbadges.getKey().startsWith("M" + theyearnow)) {

                                pointsall = pointsall + 64;

                            }

                        }
                        if (agentbadges.child("badge").getValue().toString().startsWith("AOD")) {
                            specialcount[0]++;

                            if (agentbadges.child("badge").getValue().toString().startsWith("AOD03") || agentbadges.child("badge").getValue().toString().startsWith("AOD04") || agentbadges.child("badge").getValue().toString().startsWith("AOD05")) {


                                badgesnames.add("Biocard 2019");
                                badgesdrawable.add("badge_biocard_first");
                                badgescontent.add(agentbadges.child("passcode").getValue().toString());
                                badgesstufen.add(true);
                                badgeskey.add("biocard");


                            } else if (agentbadges.child("badge").getValue().toString().startsWith("AOD06") || agentbadges.child("badge").getValue().toString().startsWith("AOD07") || agentbadges.child("badge").getValue().toString().startsWith("AOD08")) {


                                badgesnames.add("UMBRA Biocard 2019");
                                badgesdrawable.add("badge_biocard_second");
                                badgescontent.add(agentbadges.child("passcode").getValue().toString());
                                badgesstufen.add(true);
                                badgeskey.add("biocardpromo01");


                            }


                        }

                        if (agentbadges.child("badge").getValue().toString().startsWith("BIOCARDSUPPORTER2019")) {
                            specialcount[0]++;

                            badgesnames.add("BioCard Supporter 2019");
                            badgesdrawable.add("badge_supporter_first");
                            badgescontent.add(agentbadges.child("text").getValue().toString());
                            badgesstufen.add(true);
                            badgeskey.add("biocardsupporter2019");


                        }

                        if (agentbadges.child("badge").getValue().toString().startsWith("BIOCARDSUPPORTER2020")) {
                            specialcount[0]++;

                            badgesnames.add("BioCard Supporter 2020");
                            badgesdrawable.add("badge_supporter_second");
                            badgescontent.add(agentbadges.child("text").getValue().toString());
                            badgesstufen.add(true);
                            badgeskey.add("biocardsupporter2020");


                        }

                        if (agentbadges.child("badge").getValue().toString().startsWith("BIOCARDSUPPORTER2021")) {
                            specialcount[0]++;

                            badgesnames.add("BioCard Supporter 2021");
                            badgesdrawable.add("badge_supporter_second");
                            badgescontent.add(agentbadges.child("text").getValue().toString());
                            badgesstufen.add(true);
                            badgeskey.add("biocardsupporter2021");


                        }


                        if (agentbadges.child("badge").getValue().toString().startsWith("SPECIAL")) {
                            hasspecials = true;
                            final AtomicBoolean done = new AtomicBoolean(false);
                            Log.i("BADGE  ----->", " Extra badge init" + specialcount[0]);
                            final long evalue = Long.parseLong(agentbadges.child("value").getValue().toString());
                            mDatabaseaod.child("aodbadges").child(agentbadges.child("badge").getValue().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(final DataSnapshot snapshotaodbadges) {
                                    specialcount[0]++;
                                    Log.i("BADGE  ----->", " Extra badge" + specialcount[0]);

                                    String btitle = "null";
                                    String badgeurl = "badge_supporter_first";
                                    if (evalue >= Long.parseLong(snapshotaodbadges.child("valsilver").getValue().toString())) {
                                        btitle = snapshotaodbadges.child("titlesilver").getValue().toString();
                                        badgeurl = snapshotaodbadges.child("drawablesilver").getValue().toString();
                                    }
                                    if (evalue >= Long.parseLong(snapshotaodbadges.child("valgold").getValue().toString())) {
                                        btitle = snapshotaodbadges.child("titlegold").getValue().toString();
                                        badgeurl = snapshotaodbadges.child("drawablegold").getValue().toString();
                                    }
                                    if (evalue >= Long.parseLong(snapshotaodbadges.child("valplatin").getValue().toString())) {
                                        btitle = snapshotaodbadges.child("titleplatin").getValue().toString();
                                        badgeurl = snapshotaodbadges.child("drawableplatin").getValue().toString();
                                    }
                                    if (evalue >= Long.parseLong(snapshotaodbadges.child("valonyx").getValue().toString())) {
                                        btitle = snapshotaodbadges.child("titleonyx").getValue().toString();
                                        badgeurl = snapshotaodbadges.child("drawable").getValue().toString();
                                    }

                                    badgesnames.add(btitle);
                                    badgesdrawable.add(badgeurl);
                                    badgescontent.add(snapshotaodbadges.child("title").getValue().toString());
                                    badgesstufen.add(false);
                                    badgeskey.add(snapshotaodbadges.child("id").getValue().toString());

                                    done.set(true);
                                    // specialcount[0]++;

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    //   specialcount[0]++;

                                    Log.i("ERROR654 ", "" + databaseError.getMessage());

                                }
                            });
                            while (!done.get()) ;


                        }


                        Log.i("BADGE  ----->", agentbadges.child("badge").getValue().toString());

                        if (specialcount[0] >= badges_all) {
                            final long finalBadge_onyx = badge_onyx;
                            final long finalBadge_platin = badge_platin;
                            final long finalBadge_gold = badge_gold;
                            final long finalBadges_all = badges_all;
                            mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                long friends_all = 0;

                                @Override
                                public void onDataChange(final DataSnapshot snapshotmyfriends) {
                                    if (snapshotmyfriends.exists()) {
                                        friends_all = snapshotmyfriends.getChildrenCount();
                                        if (friends_all >= 5) {
                                            badgesnames.add("Friends");
                                            badgesdrawable.add("badge_friends");
                                            badgescontent.add("Friends connected");
                                            badgesstufen.add(true);
                                            badgeskey.add("friends");
                                        } else {
                                            badgesnames.add("Friends");
                                            badgesdrawable.add("badge_friends_locked");
                                            badgescontent.add("Friends connected");
                                            badgesstufen.add(true);
                                            badgeskey.add("friends");
                                        }

                                    } else {
                                        badgesnames.add("Friends");
                                        badgesdrawable.add("badge_friends_locked");
                                        badgescontent.add("Friends connected");
                                        badgesstufen.add(true);
                                        badgeskey.add("friends");
                                    }

                                    badgesnames.add("PvP");
                                    badgesdrawable.add("badge_pvp_locked");
                                    badgescontent.add("PvP");
                                    badgesstufen.add(true);
                                    badgeskey.add("pvp");


                                    // ADDED LOCKED BADGES
                                    if (finalBadge_onyx < 1) {

                                        badgesnames.add("Onyx Medals");
                                        badgesdrawable.add("badge_onyx_locked");
                                        badgescontent.add("Onyx in the week and month missions");
                                        badgesstufen.add(true);
                                        badgeskey.add("onyx");

                                    }

                                    if (finalBadge_platin < 1) {
                                        badgesnames.add("Platin Medals");
                                        badgesdrawable.add("badge_platin_locked");
                                        badgescontent.add("Platin in the week and month missions");
                                        badgesstufen.add(true);
                                        badgeskey.add("platin");

                                    }

                                    if (finalBadge_gold < 1) {
                                        badgesnames.add("Gold Medals");
                                        badgesdrawable.add("badge_gold_locked");
                                        badgescontent.add("Gold in the week and month missions");
                                        badgesstufen.add(true);
                                        badgeskey.add("gold");

                                    }


                                    Log.i("BADGES ALL  ----->", "" + finalBadges_all + " g:" + finalBadge_gold + " p:" + finalBadge_platin + " o:" + finalBadge_onyx);

                                    medals_count_field.setText("" + finalBadges_all);
                                    medals_txt.setText("" + finalBadges_all);
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int newwidth = (size.x) / 9;


                                    FlowLayout myRoot = findViewById(R.id.my_badges);
                                    myRoot.removeAllViews();
                                    //   LinearLayout a = new LinearLayout(this);

                                    myRoot.setOrientation(LinearLayout.HORIZONTAL);

                                    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg_pattern);
                                    BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

                                    bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

                                    myRoot.setBackgroundDrawable(bitmapDrawable);
                                    LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                                    int sizex = badgesnames.size();

                                    for (int i = 0; i < sizex; i++) {
                                        View view = inflater.inflate(R.layout.list_badges_adapter, myRoot, false);
                                        // set item content in view
                                        final OctogonImageView mybadge = view.findViewById(R.id.badgeimage);
                                        mybadge.getLayoutParams().width = newwidth;
                                        mybadge.getLayoutParams().height = newwidth;

                                        if (!badgesdrawable.get(i).startsWith("https")) {


                                            int badgefor_content = getResources().getIdentifier(badgesdrawable.get(i), "drawable", getPackageName());
                                            if (badgefor_content != 0) {
                                                //   Log.w("BADGE -->", " id :"+badgemonthid_content);

                                                mybadge.setImageDrawable(getDrawable(badgefor_content));

                                            }

                                            mybadge.setBorderWidth(4);

                                            if (badgesdrawable.get(i).contains("_locked")) {
                                                mybadge.setBorderColor(getColor(R.color.colorLightBrown));
                                            } else {
                                                mybadge.setBorderColor(getColor(R.color.colorPrimaryYellowLight));
                                            }
                                            mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                            mybadge.setPadding(8, 8, 8, 8);
                                            myRoot.addView(view);
                                        } else {

                                            Log.i("BADGES EXTRA -->", "" + badgesdrawable.get(i));
                                            try {
                                                PicassoCache.getPicassoInstance(getApplicationContext())
                                                        .load(badgesdrawable.get(i))
                                                        .placeholder(R.drawable.full_logo)
                                                        .error(R.drawable.full_logo)


                                                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                        //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                        //.transform(new CropRoundTransformation())
                                                        .into(mybadge);
                                                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                            } catch (Exception e) {
                                                //Log.e("Error", e.getMessage());

                                                e.printStackTrace();
                                            }

                                            mybadge.setBorderWidth(4);


                                            mybadge.setBorderColor(getColor(R.color.colorPrimaryYellowLight));

                                            mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                            mybadge.setPadding(8, 8, 8, 8);
                                            myRoot.addView(view);
                                        }
                                        //badgemonth = getResources().obtainTypedArray(badgemonthid);


                                        final int finalI = i;

                                        mybadge.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                // TODO OPEN BADGE
                                                if (mybadge.isSelected()) {
                                                    if (badgesdrawable.get(finalI).contains("_locked")) {
                                                        mybadge.setBorderColor(getColor(R.color.colorLightBrown));
                                                        mybadge.setBorderWidth(4);
                                                    } else {
                                                        mybadge.setBorderColor(getColor(R.color.colorPrimaryYellowLight));
                                                        mybadge.setBorderWidth(4);
                                                    }
                                                    mybadge.setSelected(false);
                                                    badgedetails = false;
                                                    badge_container.setVisibility(View.GONE);
                                                } else {


                                                    if (badgedetails) {

                                                        prevbadge.setSelected(false);
                                                        prevbadge.setBorderColor(getColor(R.color.colorPrimaryYellowLight));
                                                        prevbadge.setBorderWidth(4);
                                                        badgedetails = false;

                                                        // StyleableToast.makeText(getApplicationContext(),"a badge is opend", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                    }
                                                    prevbadge = mybadge;
                                                    mybadge.setSelected(true);
                                                    if (badgesdrawable.get(finalI).contains("_locked")) {
                                                        mybadge.setBorderColor(getColor(R.color.colorLightBrown));
                                                        StyleableToast.makeText(getApplicationContext(), badgesnames.get(finalI), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                        badge_container.setVisibility(View.GONE);
                                                    } else {
                                                        mybadge.setSelected(true);
                                                        badgedetails = true;
                                                        selectedbadge = view;

                                                        mybadge.setBorderColor(getColor(R.color.colorAccent));
                                                        mybadge.setBorderWidth(9);
                                                        if (badgesnames.get(finalI).equalsIgnoreCase("Friends")) {
                                                            openBadgeDetailsOther(badgesnames.get(finalI), badgesdrawable.get(finalI), badgescontent.get(finalI), badgesstufen.get(finalI), badgeskey.get(finalI), friends_all);
                                                        } else if (badgesnames.get(finalI).equalsIgnoreCase("Biocard 2019")) {
                                                            openBadgeDetailsOther(badgesnames.get(finalI), badgesdrawable.get(finalI), badgescontent.get(finalI), badgesstufen.get(finalI), badgeskey.get(finalI), 0);
                                                        } else if (badgesnames.get(finalI).startsWith("AOD0")) {
                                                            openBadgeDetailsOther(badgesnames.get(finalI), badgesdrawable.get(finalI), badgescontent.get(finalI), badgesstufen.get(finalI), badgeskey.get(finalI), 0);
                                                        } else {
                                                            openBadgeDetails(badgesnames.get(finalI), badgesdrawable.get(finalI), badgescontent.get(finalI), badgesstufen.get(finalI), badgeskey.get(finalI), finalBadge_gold, finalBadge_platin, finalBadge_onyx);
                                                        }
                                                    }


                                                }
                                            }
                                        });
                                    }


                                    coordinator.setVisibility(View.VISIBLE);
                                    progressBarHolder.setVisibility(View.GONE);

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    Log.i("ERROR655 ", "" + databaseError.getMessage());
                                    coordinator.setVisibility(View.VISIBLE);
                                    progressBarHolder.setVisibility(View.GONE);
                                }
                            });


                            long thelevelbasic = Math.round(pointsall / 9 + 1);
                            Log.i("POINTS ALL", "Grundpunkte" + pointsall + " Blevel = " + thelevelbasic);

                            profil_level.setText("L" + thelevelbasic);
                            level_txt.setText("L" + thelevelbasic);
                            levelupdate(thelevelbasic, pointsall);
                        }


                    }


                } else {
                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

    }

    private void levelupdate(final long thelevelbasic, final long pointsall) {

        allbadgeskey.clear();
        allbadgesbadge.clear();
        allbadgesart.clear();
        final long[] extrapoints = {0};
        final int maxweek = 12;
        final int maxmonth = 3;

        final int[] weeksofyear = {52};
        Query myTopPostsQuerybadegs = mDatabase.child(user.getUid()).child("badges").orderByKey();
        myTopPostsQuerybadegs.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotbadgesdetails) {
                for (DataSnapshot agentbadgesall : snapshotbadgesdetails.getChildren()) {

                    if (agentbadgesall.getKey().startsWith("W") || agentbadgesall.getKey().startsWith("M")) {
                        allbadgesbadge.add(agentbadgesall.child("badge").getValue().toString());
                        allbadgeskey.add(agentbadgesall.getKey());

                        if (agentbadgesall.getKey().startsWith("W")) {
                            allbadgesart.add("week");
                        }

                        if (agentbadgesall.getKey().startsWith("M")) {
                            allbadgesart.add("month");
                        }
                    }

                }

                Collections.reverse(allbadgesbadge);
                Collections.reverse(allbadgeskey);
                Collections.reverse(allbadgesart);


                Calendar now = Calendar.getInstance(Locale.getDefault());
                // now.setTime(date);
                now.setTimeZone(TimeZone.getDefault());
                now.setFirstDayOfWeek(Calendar.MONDAY);
                int currentweek = now.get(Calendar.WEEK_OF_YEAR);
                String cweeksend;
                String cmonthsend;


                int weeks_extra_year = currentweek;


                weeksofyear[0] = weeksofyear[0] + currentweek;
                int weeks_extra_year_looper = 12 - currentweek;

                String theyearnow = String.valueOf(Calendar.getInstance().get(Calendar.YEAR) % 100);
                String theyearlast = String.valueOf((Calendar.getInstance().get(Calendar.YEAR) % 100) - 1);

                SimpleDateFormat fmonth = new SimpleDateFormat("MM");

                int themonth = Integer.parseInt(fmonth.format(new Date()));

                int month_extra_year = themonth;

                int month_extra_year_looper = 3 - themonth;

                if (allbadgeskey.size() > 0) {
                    for (int i = 0, n = allbadgeskey.size(); i < n; i++) {

                        if (allbadgesart.get(i).equalsIgnoreCase("week")) {

                            if (currentweek <= 9 && currentweek > 0) {
                                currentweek = Integer.parseInt("0" + currentweek);
                                cweeksend = "0" + currentweek;
                            } else {
                                cweeksend = String.valueOf(currentweek);
                            }


                            if (allbadgeskey.get(i).startsWith("W" + theyearnow + "" + cweeksend) && i <= weeks_extra_year && i <= maxweek) {

                                if (allbadgesbadge.get(i).equalsIgnoreCase("GOLD")) {
                                    extrapoints[0] = extrapoints[0] + 16;
                                }

                                if (allbadgesbadge.get(i).equalsIgnoreCase("PLATIN")) {
                                    extrapoints[0] = extrapoints[0] + 32;
                                }

                                if (allbadgesbadge.get(i).equalsIgnoreCase("ONYX")) {
                                    extrapoints[0] = extrapoints[0] + 64;
                                }

                            } else if (allbadgeskey.get(i).startsWith("W" + theyearlast + "" + weeksofyear[0]) && i <= weeks_extra_year_looper && i <= maxweek) {

                                if (allbadgesbadge.get(i).equalsIgnoreCase("GOLD")) {
                                    extrapoints[0] = extrapoints[0] + 16;
                                }

                                if (allbadgesbadge.get(i).equalsIgnoreCase("PLATIN")) {
                                    extrapoints[0] = extrapoints[0] + 32;
                                }

                                if (allbadgesbadge.get(i).equalsIgnoreCase("ONYX")) {
                                    extrapoints[0] = extrapoints[0] + 64;
                                }

                            }

                            weeksofyear[0]--;
                            currentweek--;

                        }
                        if (allbadgesart.get(i).equalsIgnoreCase("month")) {

                            if (themonth <= 9 && themonth > 0) {
                                themonth = Integer.parseInt("0" + themonth);
                                Log.i("MONTH ", " " + themonth);
                                cmonthsend = "0" + themonth;
                            } else {
                                cmonthsend = String.valueOf(themonth);
                            }
                            if (allbadgeskey.get(i).startsWith("M" + theyearnow + "" + cmonthsend) && i <= month_extra_year && i <= maxmonth) {

                                if (allbadgesbadge.get(i).equalsIgnoreCase("GOLD")) {
                                    extrapoints[0] = extrapoints[0] + 32;
                                }

                                if (allbadgesbadge.get(i).equalsIgnoreCase("PLATIN")) {
                                    extrapoints[0] = extrapoints[0] + 64;
                                }

                                if (allbadgesbadge.get(i).equalsIgnoreCase("ONYX")) {
                                    extrapoints[0] = extrapoints[0] + 128;
                                }

                            } else if (allbadgeskey.get(i).startsWith("M" + theyearlast + "" + weeksofyear[0]) && i <= month_extra_year_looper && i <= maxmonth) {

                                if (allbadgesbadge.get(i).equalsIgnoreCase("GOLD")) {
                                    extrapoints[0] = extrapoints[0] + 32;
                                }

                                if (allbadgesbadge.get(i).equalsIgnoreCase("PLATIN")) {
                                    extrapoints[0] = extrapoints[0] + 64;
                                }

                                if (allbadgesbadge.get(i).equalsIgnoreCase("ONYX")) {
                                    extrapoints[0] = extrapoints[0] + 128;
                                }

                            }

                            weeksofyear[0]--;
                            currentweek--;
                        }
                        Log.i("ALL PONTS  ", " " + extrapoints[0] + " + " + pointsall);

                        Log.i("ALL BADGES ", " " + allbadgeskey.get(i));

                        long thelevelbasic2 = Math.round(extrapoints[0] / 9);
                        Log.i("POINTS ALL", "Grundpunkte" + pointsall + " Blevel = " + thelevelbasic + " " + thelevelbasic2);

                        profil_level.setText("L" + (thelevelbasic + thelevelbasic2));
                        level_txt.setText("L" + (thelevelbasic + thelevelbasic2));

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
    }

    private void openBadgeDetails(String badgname, String badgedrawab, String badgecont, Boolean hasstufen, String keyword, long finalBadge_gold, long finalBadge_platin, long finalBadge_onyx) {
        badge_container.setVisibility(View.VISIBLE);
        open_friends_container.setVisibility(View.GONE);
        long badgesanzahl = 0;
        if (hasstufen) {
            if (keyword.equalsIgnoreCase("gold")) {
                badgesanzahl = finalBadge_gold;
            } else if (keyword.equalsIgnoreCase("platin")) {
                badgesanzahl = finalBadge_platin;
            } else if (keyword.equalsIgnoreCase("onyx")) {
                badgesanzahl = finalBadge_onyx;
            } else {
                badgesanzahl = 1;
            }
        } else {
            badgesanzahl = 1;
        }
        TextView nameofbadge = findViewById(R.id.nameofbadge);
        if (badgesanzahl > 0 && hasstufen) {
            nameofbadge.setText(badgname + " (" + badgesanzahl + ")");
        } else {
            nameofbadge.setText(badgname);
        }

        OctogonImageView badgeimagefull = findViewById(R.id.badgeimagefull);
        int badgefor_content = getResources().getIdentifier(badgedrawab, "drawable", getPackageName());

        //badgemonth = getResources().obtainTypedArray(badgemonthid);


        if (badgefor_content != 0) {
            //   Log.w("BADGE -->", " id :"+badgemonthid_content);
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int newwidth = (size.x) / 3;
            badgeimagefull.getLayoutParams().width = newwidth;
            badgeimagefull.getLayoutParams().height = newwidth;

            badgeimagefull.setImageDrawable(getDrawable(badgefor_content));
            badgeimagefull.setScaleType(ImageView.ScaleType.FIT_CENTER);
            badgeimagefull.setPadding(8, 8, 8, 8);


        }

        TextView badcontent_field = findViewById(R.id.badcontent_field);
        badcontent_field.setText(badgecont);

        TextView badcount_field = findViewById(R.id.badcount_field);
        badcount_field.setText(String.valueOf(badgesanzahl));

        badcontent_field.setOnClickListener(null);


    }

    private void openBadgeDetailsOther(String badgname, String badgedrawab, final String badgecont, Boolean hasstufen, String keyword, long thevalue) {
        badge_container.setVisibility(View.VISIBLE);
        open_friends_container.setVisibility(View.GONE);
        long badgesanzahl = 0;
        if (hasstufen) {

            badgesanzahl = thevalue;

        } else {
            badgesanzahl = thevalue;
        }
        TextView nameofbadge = findViewById(R.id.nameofbadge);
        if (badgesanzahl > 0 && hasstufen) {
            nameofbadge.setText(badgname + " (" + badgesanzahl + ")");
        } else {
            nameofbadge.setText(badgname);
        }

        OctogonImageView badgeimagefull = findViewById(R.id.badgeimagefull);
        int badgefor_content = getResources().getIdentifier(badgedrawab, "drawable", getPackageName());

        //badgemonth = getResources().obtainTypedArray(badgemonthid);


        if (badgefor_content != 0) {
            //   Log.w("BADGE -->", " id :"+badgemonthid_content);
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int newwidth = (size.x) / 3;
            badgeimagefull.getLayoutParams().width = newwidth;
            badgeimagefull.getLayoutParams().height = newwidth;

            badgeimagefull.setImageDrawable(getDrawable(badgefor_content));
            badgeimagefull.setScaleType(ImageView.ScaleType.FIT_CENTER);
            badgeimagefull.setPadding(8, 8, 8, 8);


        }

        TextView badcontent_field = findViewById(R.id.badcontent_field);
        badcontent_field.setText(badgecont);
        badcontent_field.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Passcode", "" + badgecont);
                clipboard.setPrimaryClip(clip);

                StyleableToast.makeText(getApplicationContext(), "Passcode has copied!", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
            }
        });

        TextView badcount_field = findViewById(R.id.badcount_field);
        if (badgesanzahl > 0) {
            badcount_field.setText(String.valueOf(badgesanzahl));
        } else {
            badcount_field.setText("Special Badge");
        }


    }

    public void scanQR() {
        pref.registerOnSharedPreferenceChangeListener(this);

        pref.edit().putBoolean("qrscan", true).apply();
        pref.edit().remove("isscaned").apply();


        if (ActivityCompat.checkSelfPermission(ProfileMain.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileMain.this,
                    android.Manifest.permission.CAMERA)) {

                ActivityCompat
                        .requestPermissions(ProfileMain.this, PERMISSIONS_CAM,
                                REQUEST_CAM);
            } else {
                ActivityCompat.requestPermissions(ProfileMain.this, PERMISSIONS_CAM, REQUEST_CAM);
            }
            return;
        }
        IntentIntegrator integrator = new IntentIntegrator(ProfileMain.this);

        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("SCAN QR");
        integrator.setOrientationLocked(true);
        integrator.setCaptureActivity(SmallCaptureActivity.class);
        integrator.createScanIntent();
        //  integrator.setCaptureActivity(QR.class);
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.setBeepEnabled(true);
        //integrator.setBarcodeImageEnabled(true);
        integrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String toast = "scanning...";
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (requestCode == 250) {
            recreate();
        }
        if (result != null) {
            if (result.getContents() == null) {
                toast = "Cancelled scan";
                pref.edit().putBoolean("qrscan", false).apply();
                final View rootlayout2 = getWindow().getDecorView().findViewById(android.R.id.content);
                Snackbar snack = Snackbar.make(rootlayout2, toast,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                            }
                        });

                snack.show();
                // displayToast(toast);
            } else {

                toast = result.getContents();
                if (toast.startsWith("https://aod.prxenon.rocks/scan/")) {
                    toast = toast.replace("https://aod.prxenon.rocks/scan/", "");
                    new QRScanned(toast).execute();
                } else {
                    toast = "" + getResources().getString(R.string.error_tryagain);
                    final View rootlayout2 = getWindow().getDecorView().findViewById(android.R.id.content);
                    Snackbar snack = Snackbar.make(rootlayout2, toast,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                }
                            });

                    snack.show();
                }
            }

            // At this point we may or may not have a reference to the activity


        }
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        Log.i("CHANGED--- > ", " KEY: " + key);

        if (key.equalsIgnoreCase("hasalldata") && pref.getBoolean("hasalldata", false)) {
            coordinator.setVisibility(View.GONE);
            progressBarHolder.setVisibility(View.VISIBLE);
            Log.i("CHANGEDDATA --- > ", " original: " + originalagent + " other " + scanneragent);
            if (originalagent.equalsIgnoreCase(scanneragent)) {


                pref.edit().remove("scanedagnet").apply();
                pref.edit().remove("hasalldata").apply();
                AgentData.getscanproof = 0;
                final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                Snackbar snack = Snackbar.make(rootlayout, getResources().getText(R.string.ownqr),
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                coordinator.setVisibility(View.VISIBLE);
                                progressBarHolder.setVisibility(View.GONE);
                                recreate();
                            }
                        });

                snack.show();
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        savetheAgent(connectedwith, new AgentData().getOtherAgentdata("otheragentimage"), new AgentData().getOtherAgentdata("otheragentfaction"));


                    }
                }, 1500L);

            }


        }

        if (key.equalsIgnoreCase("scanedagnet") && pref.getBoolean("scanedagnet", false)) {

            //  qrcodeimg.setImageBitmap(result);
            //  progressbarx.setVisibility(View.GONE);7

            Log.i("HERE 4006-->", " Log " + scanneragent + " " + originalagent);
            final String connectedwith;


            if (pref.contains("scanedagnet")) {
                if (scanneragent.equalsIgnoreCase(AgentData.getagentname())) {
                    // ACTION FOR THE AGENT WHO NOT SCANED  THE QR
                    connectedwith = originalagent;
                    new AgentData("connectedwith", originalagent);


                } else {
                    // ACTION FOR THE AGENT WHO SCANED THE QR

                    connectedwith = scanneragent;
                    new AgentData("connectedwith", scanneragent);

                }


                //SCAN IS CORRECT

                if (scanneragent.equalsIgnoreCase(AgentData.getagentname())) {
                    Log.i("HERE 400999-->", " Log " + theqrcodestring + " " + AgentData.getagentname());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new AODConfig("qrcloser", getApplicationContext()).tempscanCoderemove(user.getEmail(), theqrcodestring, AgentData.getagentname());

                        }
                    }, 2000L);

                }

                otheragentimage = null;
                otheragentfaction = null;

                //get AGENT SCANNED MAIL

                ValueEventListener detailslist = mdbroot.child(connectedwith).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        //snapshot.child("name").getValue();
                        // SetLoginName(snapshot.child("name").getValue().toString());

                        // HAS FOUND AGENT
                        System.out.println(snapshot.getValue() + "7898");
                        //System.out.println(snapshot+ "7898");
                        //    new AgentData("otheragentemail", snapshot.getValue().toString());
                        //get AGENT SCANNED Faction
                        if (snapshot.getValue() != null) {

                            new AgentData("otheragentemail", snapshot.getValue().toString());
                            mDatabase.child(snapshot.getValue().toString()).child("faction").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot snapshot) {
                                    //snapshot.child("name").getValue();
                                    // SetLoginName(snapshot.child("name").getValue().toString());

                                    // HAS FOUND FACTION
                                    System.out.println(snapshot.getValue());
                                    otheragentfaction = snapshot.getValue().toString();
                                    new AgentData("otheragentfaction", snapshot.getValue().toString());

                                    // new AgentData("otheragentemail", snapshot.getValue().toString());


                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });


                            mDatabase.child(snapshot.getValue().toString()).child("profileimage").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot snapshot) {
                                    //snapshot.child("name").getValue();
                                    // SetLoginName(snapshot.child("name").getValue().toString());

                                    // HAS FOUND Userimage
                                    System.out.println(snapshot.getValue());
                                    new AgentData("otheragentimage", snapshot.getValue().toString());
                                    otheragentimage = snapshot.getValue().toString();
                                    //savetheAgent(latis, lngis, connectedwith, otheragentimage, otheragentfaction);
                                    // pref.edit().remove("scanedagnet").apply();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                }
                            });

                        } else {
                            Log.i("ERROR ROOT -->", "ON SCAN");
                            final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                            Snackbar snack = Snackbar.make(rootlayout, getText(R.string.string_qrcodenotfound),
                                    Snackbar.LENGTH_INDEFINITE)
                                    .setAction("OK", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {


                                        }
                                    });

                            snack.show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });


                // savetheAgent(latis, lngis, connectedwith, otheragentimage, otheragentfaction);
                //  pref.edit().remove("scanedagnet").apply();
            }

        }


        //System.out.println("9992 root" + key);
    }

    public void savetheAgent(final String connectedwith, final String otheragentimage, final String otheragentfaction) {


        pref.edit().remove("scanedagnet").apply();
        pref.edit().remove("hasalldata").apply();
        Log.i("OTHERDATA --> ", otheragentimage + "" + otheragentfaction);
        coordinator.setVisibility(View.VISIBLE);
        progressBarHolder.setVisibility(View.GONE);
        dialogBuilder = new AlertDialog.Builder(this, R.style.CustomDialog);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dialog_addfriend, null);

        dialogBuilder.setView(dialog);
        dialogBuilder.setCancelable(false);

        final TextView localTextView1 = dialog.findViewById(R.id.basistit);
        final TextView localTextView2 = dialog.findViewById(R.id.textdescr);
        ImageView own = dialog.findViewById(R.id.ownimage);
        CircleImageView other = dialog.findViewById(R.id.otherimage);
        if (otheragentfaction.equalsIgnoreCase("ENLIGHTENED")) {
            other.setBorderColor(getResources().getColor(R.color.colorENL));
        } else if (otheragentfaction.equalsIgnoreCase("RESISTANCE")) {
            other.setBorderColor(getResources().getColor(R.color.colorRES));
        } else {
            other.setBorderColor(getResources().getColor(R.color.colorGray));
        }
        final String otheragenturl = otheragentimage + "?sz=300";
        String personPhotoUrl = user.getProviderData().get(1).getPhotoUrl().toString() + "?sz=300";

        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(personPhotoUrl)
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(own);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(otheragenturl)
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(other);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }


        mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //snapshot.child("name").getValue();
                // SetLoginName(snapshot.child("name").getValue().toString());
                if (!snapshot.child(otheragentemail).exists()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        localTextView1.setText(Html.fromHtml(String.format(getString(R.string.connectedwith), connectedwith), Html.FROM_HTML_MODE_COMPACT));
                        localTextView2.setText(Html.fromHtml(String.format(getString(R.string.connectedwith_next), connectedwith), Html.FROM_HTML_MODE_COMPACT));
                    } else {
                        localTextView1.setText(Html.fromHtml(String.format(getString(R.string.connectedwith), connectedwith)));
                        localTextView2.setText(Html.fromHtml(String.format(getString(R.string.connectedwith_next), connectedwith)));
                    }


                    dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            AgentData.getscanproof = 0;
                            Log.e("EMAIL", otheragentemail);

                            new SaveFriendScan().saveScannedAgent(ProfileMain.this, getApplicationContext(), otheragentemail);


                        }
                    });

                    dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            AgentData.getscanproof = 0;
                            Log.e("EMAIL", otheragentemail);

                        }
                    });


                    if (!isFinishing()) {


                        if (dialog.getParent() == null) {
                            dialogBuilder.create().show();
                        }


                    }
                } else {
                    final View rootlayout2 = getWindow().getDecorView().findViewById(android.R.id.content);
                    Snackbar snack = Snackbar.make(rootlayout2, String.format(getString(R.string.string_still_friends), connectedwith),
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    recreate();

                                }
                            });

                    snack.show();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


    }

    public void scanedone(boolean resultis, Activity act, Context ctx) {

        if (resultis) {
            Log.i("SCANE TRUE --->", "" + resultis);
            act.recreate();
        } else {
            Log.i("SCANE FAILED --->", "" + resultis);
            act.recreate();
        }

    }

    public void newpasscode(boolean resultis, final Activity act, Context ctx, String passcodeid) {

        if (resultis) {
            Log.i("SCANE TRUE --->", "" + resultis);
            AlertDialog.Builder confirmdialog = new AlertDialog.Builder(act, R.style.CustomDialog);

            // builder.setMessage("Text not finished...");
            LayoutInflater inflater = act.getLayoutInflater();
            View dialog = inflater.inflate(R.layout.dialog_redeemed_passcode, null);
            confirmdialog.setView(dialog);

            TextView agentname = dialog.findViewById(R.id.agentname);
            agentname.setText("You redeemed an AOD passcode and unlocked a new badge.\n\nNote: The Ingress passcode may have already been redeemed. Just try and be lucky.");

            confirmdialog.setPositiveButton("Wuhuu", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    act.recreate();
                    //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                }
            });


            confirmdialog.setCancelable(true);

            if (!isFinishing()) {
                confirmdialog.create().show();

            }
            //act.recreate();
        } else {
            Log.i("SCANE FAILED --->", "" + resultis);
            act.recreate();
        }

    }

    public void alreadyFriends(boolean friendsare, Activity act, Context ctx) {

        if (friendsare) {
            Log.i("FRIENDS --->", "" + friendsare);
        } else {
            Log.i("FRIENDS NO --->", "" + friendsare);
            String textis = "This agent is not in your friends list.";

            final View rootlayout2 = act.getWindow().getDecorView().findViewById(android.R.id.content);
            Snackbar snack = Snackbar.make(rootlayout2, textis,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                        }
                    });

            snack.show();
        }

    }

    public void alreadyFriendssend(boolean friendsare, Activity act, Context ctx) {


        Log.i("FRIENDS NO --->", "" + friendsare);
        String textis = "Already send a friend request";

        final View rootlayout2 = act.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snack = Snackbar.make(rootlayout2, textis,
                Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                    }
                });

        snack.show();


    }

    private void removeFriend(final String friendID, String otheragentimageis) {
        Log.i("Friendid", friendID);

        dialogBuilder = new AlertDialog.Builder(this, R.style.CustomDialog);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dialog_addfriend, null);

        dialogBuilder.setView(dialog);
        dialogBuilder.setCancelable(false);

        final TextView localTextView1 = dialog.findViewById(R.id.basistit);
        final TextView localTextView2 = dialog.findViewById(R.id.textdescr);
        ImageView own = dialog.findViewById(R.id.ownimage);
        CircleImageView other = dialog.findViewById(R.id.otherimage);

        other.setBorderColor(getResources().getColor(R.color.colorAccent));

        final String otheragenturl = otheragentimageis + "?sz=300";
        String personPhotoUrl = user.getProviderData().get(1).getPhotoUrl().toString() + "?sz=300";

        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(personPhotoUrl)
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(own);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(otheragenturl)
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(other);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

        mDatabase.child(friendID).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //snapshot.child("name").getValue();
                // SetLoginName(snapshot.child("name").getValue().toString());
                if (snapshot.exists()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        localTextView1.setText(Html.fromHtml(String.format(getString(R.string.notconnectedwith), snapshot.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                        localTextView2.setText(Html.fromHtml(String.format(getString(R.string.notconnectedwith_next), snapshot.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                    } else {
                        localTextView1.setText(Html.fromHtml(String.format(getString(R.string.notconnectedwith), snapshot.getValue().toString())));
                        localTextView2.setText(Html.fromHtml(String.format(getString(R.string.notconnectedwith_next), snapshot.getValue().toString())));
                    }


                    dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {


                            new SaveFriendScan().removeScannedAgent(ProfileMain.this, getApplicationContext(), friendID);


                        }
                    });

                    dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {


                        }
                    });


                    if (!isFinishing()) {


                        if (dialog.getParent() == null) {
                            dialogBuilder.create().show();
                        }


                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void openqrfromAction() {


        qr_btn.setAutoHide(false);
        pref.edit().remove("isscaned").apply();
        Intent i = new Intent(getBaseContext(), ShowMyQR.class);
        //i.putExtra("PersonID", personID);
        startActivity(i);

    }

    public void FriendsRequestsOpen(long countis, Activity activityis, Context contextis, TextView textis) {

        allopenrequests = allopenrequests + countis;
        Log.i("Test -->", "" + allopenrequests);


    }

    public void FriendsRequestsOpenAll(long countis, long allfriends, final Activity activityis, final Context contextis, TextView textis, TextView textis2, final FrameLayout container) {

        allopenrequests = allopenrequests + countis;
        allmyfriends = allmyfriends + allfriends;


        Log.i("Test2 -->", "" + allopenrequests);
        Log.i("Test3 -->", "" + allmyfriends);

        // friendslistview

        if (allopenrequests > 0) {
            final List<String> nameofagent = new ArrayList();
            final List<String> imageofagent = new ArrayList();
            final List<String> uuidofagent = new ArrayList();
            final List<Integer> getartofrequest = new ArrayList();


            // final List<Long> rankUID = new ArrayList();
            // final List<Integer> positionUID = new ArrayList();
            friendslistview = container.findViewById(R.id.friendslistview);
            textis2.setEnabled(true);
            textis2.setText(contextis.getResources().getString(R.string.string_openrequests) + " (" + allopenrequests + ")");
            textis2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // GET FRIENDS I HAVE TO CONFIRM
                    nameofagent.clear();
                    imageofagent.clear();
                    uuidofagent.clear();
                    getartofrequest.clear();
                    mbfriendsrequest.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot snapshot) {
                            //snapshot.child("name").getValue();
                            // SetLoginName(snapshot.child("name").getValue().toString());
                            if (snapshot.exists()) {
                                // START A LOOP
                                int looperfirst = 0;
                                for (final DataSnapshot snapis : snapshot.getChildren()) {
                                    Log.i("KEY OF", "" + snapis.getKey());
                                    // GET DETAILS FOR THIS AGENT IAMGE AND NAME

                                    looperfirst++;
                                    mDatabase.child(snapis.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(final DataSnapshot snapshotimage) {
                                            //snapshot.child("name").getValue();
                                            // SetLoginName(snapshot.child("name").getValue().toString());
                                            if (snapshotimage.exists()) {


                                                mDatabase.child(snapis.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot snapshotname) {
                                                        //snapshot.child("name").getValue();
                                                        // SetLoginName(snapshot.child("name").getValue().toString());
                                                        if (snapshotname.exists()) {
                                                            uuidofagent.add(snapis.getKey());
                                                            imageofagent.add(snapshotimage.getValue().toString());
                                                            nameofagent.add(snapshotname.getValue().toString());
                                                            getartofrequest.add(1);

                                                            friendslistview.invalidateViews();

                                                            friendsadapter = new FriendsInvitesAdapter(activityis, uuidofagent.size(), uuidofagent, imageofagent, nameofagent, getartofrequest);
                                                            friendslistview.setAdapter(friendsadapter);
                                                            friendsadapter.notifyDataSetChanged();
                                                        }


                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                    }
                                                });
                                            }


                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });

                                    if (looperfirst >= snapshot.getChildrenCount()) {
                                        // RET REQUESTS I WAIT FORM CONFIRMATION
                                        mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(final DataSnapshot snapshotrequests) {
                                                //snapshot.child("name").getValue();
                                                // SetLoginName(snapshot.child("name").getValue().toString());
                                                if (snapshotrequests.exists()) {


                                                    for (final DataSnapshot snapisfriends : snapshotrequests.getChildren()) {
                                                        Log.i("KEY OF Friends 1", "" + snapisfriends.getKey());
                                                        // GET DETAILS FOR THIS AGENT IAMGE AND NAME

                                                        if (snapisfriends.getChildrenCount() > 0) {
                                                            if (!Boolean.parseBoolean(snapisfriends.child("status").getValue().toString())) {

                                                                mDatabase.child(snapisfriends.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(final DataSnapshot snapshotimage) {
                                                                        //snapshot.child("name").getValue();
                                                                        // SetLoginName(snapshot.child("name").getValue().toString());
                                                                        if (snapshotimage.exists()) {


                                                                            mDatabase.child(snapisfriends.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                @Override
                                                                                public void onDataChange(DataSnapshot snapshotname) {
                                                                                    //snapshot.child("name").getValue();
                                                                                    // SetLoginName(snapshot.child("name").getValue().toString());
                                                                                    if (snapshotname.exists()) {
                                                                                        uuidofagent.add(snapisfriends.getKey());
                                                                                        imageofagent.add(snapshotimage.getValue().toString());
                                                                                        nameofagent.add(snapshotname.getValue().toString());
                                                                                        getartofrequest.add(2);


                                                                                        friendslistview.invalidateViews();

                                                                                        friendsadapter = new FriendsInvitesAdapter(activityis, uuidofagent.size(), uuidofagent, imageofagent, nameofagent, getartofrequest);
                                                                                        friendslistview.setAdapter(friendsadapter);
                                                                                        friendsadapter.notifyDataSetChanged();


                                                                                    }


                                                                                }

                                                                                @Override
                                                                                public void onCancelled(DatabaseError databaseError) {
                                                                                }
                                                                            });
                                                                        }


                                                                    }


                                                                    @Override
                                                                    public void onCancelled(DatabaseError databaseError) {
                                                                    }
                                                                });
                                                            } else {
                                                                if (uuidofagent.size() > 0) {
                                                                    friendslistview.invalidateViews();

                                                                    friendsadapter = new FriendsInvitesAdapter(activityis, uuidofagent.size(), uuidofagent, imageofagent, nameofagent, getartofrequest);
                                                                    friendslistview.setAdapter(friendsadapter);
                                                                    friendsadapter.notifyDataSetChanged();
                                                                }
                                                            }

                                                            // setopenFriendsList();
                                                        } else {
                                                            // NO FREINDS
                                                            // TODO NOTHING NO FRIENDS
                                                        }
                                                    }

                                                }


                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                            }
                                        });
                                    }
                                }


                            } else {

                                // RET REQUESTS I WAIT FORM CONFIRMATION
                                mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(final DataSnapshot snapshotrequests) {
                                        //snapshot.child("name").getValue();
                                        // SetLoginName(snapshot.child("name").getValue().toString());
                                        if (snapshotrequests.exists()) {


                                            for (final DataSnapshot snapisfriends : snapshotrequests.getChildren()) {
                                                Log.i("KEY OF Friends 2", "" + snapisfriends.getKey());
                                                // GET DETAILS FOR THIS AGENT IAMGE AND NAME

                                                if (snapisfriends.getChildrenCount() > 0) {
                                                    if (!Boolean.parseBoolean(snapisfriends.child("status").getValue().toString())) {

                                                        mDatabase.child(snapisfriends.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(final DataSnapshot snapshotimage) {
                                                                //snapshot.child("name").getValue();
                                                                // SetLoginName(snapshot.child("name").getValue().toString());
                                                                if (snapshotimage.exists()) {


                                                                    mDatabase.child(snapisfriends.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(DataSnapshot snapshotname) {
                                                                            //snapshot.child("name").getValue();
                                                                            // SetLoginName(snapshot.child("name").getValue().toString());
                                                                            if (snapshotname.exists()) {
                                                                                uuidofagent.add(snapisfriends.getKey());
                                                                                imageofagent.add(snapshotimage.getValue().toString());
                                                                                nameofagent.add(snapshotname.getValue().toString());
                                                                                getartofrequest.add(2);


                                                                                friendslistview.invalidateViews();

                                                                                friendsadapter = new FriendsInvitesAdapter(activityis, uuidofagent.size(), uuidofagent, imageofagent, nameofagent, getartofrequest);
                                                                                friendslistview.setAdapter(friendsadapter);
                                                                                friendsadapter.notifyDataSetChanged();


                                                                            }


                                                                        }

                                                                        @Override
                                                                        public void onCancelled(DatabaseError databaseError) {
                                                                        }
                                                                    });
                                                                }


                                                            }


                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {
                                                            }
                                                        });
                                                    } else {
                                                        if (uuidofagent.size() > 0) {
                                                            friendslistview.invalidateViews();

                                                            friendsadapter = new FriendsInvitesAdapter(activityis, uuidofagent.size(), uuidofagent, imageofagent, nameofagent, getartofrequest);
                                                            friendslistview.setAdapter(friendsadapter);
                                                            friendsadapter.notifyDataSetChanged();
                                                        }
                                                    }


                                                    // setopenFriendsList();
                                                } else {
                                                    // NO FREINDS
                                                    // TODO NOTHING NO FRIENDS
                                                }
                                            }

                                        }


                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });


                    container.setVisibility(View.VISIBLE);


                    badgedetails = false;
                    badge_container = activityis.findViewById(R.id.badge_container);
                    badge_container.setVisibility(View.GONE);
                    ImageButton scroll_btn_open_friends = activityis.findViewById(R.id.scroll_btn_open_friends);
                    scroll_btn_open_friends.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            activityis.recreate();
                        }
                    });
                }
            });

        } else {
            textis2.setEnabled(false);
            textis2.setText(contextis.getResources().getString(R.string.string_openrequests));
        }

        if (allmyfriends > 0) {
            textis.setEnabled(true);
            textis.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activityis, FriendsMain.class);
                    //i.putExtra("PersonID", personID);
                    activityis.startActivity(i);

                }
            });

        } else {
            textis.setEnabled(false);
        }

    }

    private void handleinputcode(final String theinputcode) {
        getIntent().setAction(null);
        getIntent().setData(null);
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);
        mDatabaseagentCodes.child(theinputcode).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override

            public void onDataChange(final DataSnapshot snapshot) {

                if (snapshot.exists()) {
                    // CODE FOUND IN THE SYSTEM
                    Log.i("Code-->", "Exist " + theinputcode);
                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);

                    AlertDialog.Builder confirmdialog = new AlertDialog.Builder(ProfileMain.this, R.style.CustomDialog);

                    // builder.setMessage("Text not finished...");
                    LayoutInflater inflater = getLayoutInflater();
                    View dialog = inflater.inflate(R.layout.dialog_confirm, null);
                    confirmdialog.setView(dialog);


                    confirmdialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            new SaveFriendScan().saveManuellAgent(ProfileMain.this, getApplicationContext(), snapshot.getValue().toString());


                        }
                    });

                    confirmdialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    });

                    confirmdialog.setCancelable(true);

                    if (!isFinishing()) {
                        confirmdialog.create().show();

                    }


                } else {

                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);

                    Log.i("Code-->", "not Exist " + theinputcode);
                    final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                    Snackbar snack = Snackbar.make(rootlayout, "Code not found.. please try another code",
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                }
                            });

                    snack.show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    private void handlepasscode(final String theinputcode) {
        getIntent().setAction(null);
        getIntent().setData(null);
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);
        mbpasscodes.child("AOD" + theinputcode).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override

            public void onDataChange(final DataSnapshot snapshot) {

                if (snapshot.exists()) {
                    // CODE FOUND IN THE SYSTEM
                    Log.i("Code-->", "Exist AOD" + theinputcode);
                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);


                    if (!Boolean.parseBoolean(snapshot.child("redeemed").getValue().toString())) {


                        mDatabase.child(user.getUid()).child("badges").child(snapshot.child("title").getValue().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override

                            public void onDataChange(final DataSnapshot snapshotproof) {

                                if (!snapshotproof.exists()) {

                                    mDatabase.child(user.getUid()).child("badges").child(snapshot.child("aodcode").getValue().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override

                                        public void onDataChange(final DataSnapshot snapshotproof2) {

                                            if (!snapshotproof2.exists()) {


                                                mDatabase.child(user.getUid()).child("badges").addListenerForSingleValueEvent(new ValueEventListener() {

                                                    @Override
                                                    public void onDataChange(final DataSnapshot snapshotproof3) {
                                                        int looperchild = 0;
                                                        boolean hasthisbadge = false;
                                                        String theinput = "AOD" + theinputcode;
                                                        for (DataSnapshot allbadges : snapshotproof3.getChildren()) {
                                                            looperchild++;

                                                            Log.i(" SNAP --> ", " " + allbadges.getKey());
                                                            if ((allbadges.getKey().startsWith("AOD06") || allbadges.getKey().startsWith("AOD07") || allbadges.getKey().startsWith("AOD08")) && (snapshot.child("aodcode").getValue().toString().startsWith("AOD06") || snapshot.child("aodcode").getValue().toString().startsWith("AOD07") || snapshot.child("aodcode").getValue().toString().startsWith("AOD08"))) {
                                                                hasthisbadge = true;
                                                            }

                                                            if (looperchild >= snapshotproof3.getChildrenCount()) {
                                                                Log.i("Code-->", "Exist 5 " + theinputcode);
                                                                coordinator.setVisibility(View.VISIBLE);
                                                                progressBarHolder.setVisibility(View.GONE);

                                                                if (hasthisbadge) {
                                                                    Log.i("Code-->", "Badge already exist " + hasthisbadge);
                                                                    final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                                                                    Snackbar snack = Snackbar.make(rootlayout, "this Badge already exist",
                                                                            Snackbar.LENGTH_INDEFINITE)
                                                                            .setAction("OK", new View.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(View view) {


                                                                                }
                                                                            });

                                                                    snack.show();
                                                                } else {


                                                                    AlertDialog.Builder confirmdialog = new AlertDialog.Builder(ProfileMain.this, R.style.CustomDialog);

                                                                    // builder.setMessage("Text not finished...");
                                                                    LayoutInflater inflater = getLayoutInflater();
                                                                    View dialog = inflater.inflate(R.layout.dialog_confirm_passcode, null);
                                                                    confirmdialog.setView(dialog);


                                                                    confirmdialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                                                        @Override
                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                            new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                                                        }
                                                                    });

                                                                    confirmdialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                                                                        @Override
                                                                        public void onClick(DialogInterface dialog, int which) {


                                                                        }
                                                                    });

                                                                    confirmdialog.setCancelable(true);

                                                                    if (!isFinishing()) {
                                                                        confirmdialog.create().show();

                                                                    }
                                                                }
                                                            }

                                                        }

                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {
                                                    }
                                                });


                                            } else {
                                                coordinator.setVisibility(View.VISIBLE);
                                                progressBarHolder.setVisibility(View.GONE);

                                                Log.i("Code-->", "not Exist " + theinputcode);
                                                final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                                                Snackbar snack = Snackbar.make(rootlayout, "Code not found.. please try another code",
                                                        Snackbar.LENGTH_INDEFINITE)
                                                        .setAction("OK", new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {


                                                            }
                                                        });

                                                snack.show();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });


                                } else {

                                    coordinator.setVisibility(View.VISIBLE);
                                    progressBarHolder.setVisibility(View.GONE);

                                    Log.i("Code-->", "not Exist " + theinputcode);
                                    final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                                    Snackbar snack = Snackbar.make(rootlayout, "Code not found.. please try another code",
                                            Snackbar.LENGTH_INDEFINITE)
                                            .setAction("OK", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {


                                                }
                                            });

                                    snack.show();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });

                    } else {
                        coordinator.setVisibility(View.VISIBLE);
                        progressBarHolder.setVisibility(View.GONE);

                        Log.i("Code-->", "reedeemed " + theinputcode);
                        final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                        Snackbar snack = Snackbar.make(rootlayout, "Passcode has already been redeemed.",
                                Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {


                                    }
                                });

                        snack.show();

                    }


                } else {

                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);

                    Log.i("Code-->", "not Exist " + theinputcode);
                    final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
                    Snackbar snack = Snackbar.make(rootlayout, "Code not found.. please try another code",
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {


                                }
                            });

                    snack.show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public void withdrawRequest(final Activity context, final int position, final Integer artofrequest, final String agentsuuid, final View knopfView) {


        final Map<String, Object> updaterunning = new HashMap<>();


        mDatabase.child(user.getUid()).child("friends").child(agentsuuid).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {


            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    StyleableToast.makeText(context, "Request successfully withdrawn", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    TextView friend_withdraw = knopfView.findViewById(R.id.friend_withdraw);
                    knopfView.setBackgroundColor(context.getResources().getColor(R.color.greytrans, context.getTheme()));
                    friend_withdraw.setText(context.getResources().getString(R.string.string_request_removed));
                    friend_withdraw.setClickable(false);

                    mbfriendsrequest.child(agentsuuid).child(user.getUid()).setValue(null);

                    final Map<String, Object> agentinbox2 = new HashMap<>();


                    agentinbox2.put("/text", AgentData.getagentname());


                    agentinbox2.put("/title", "friend request withdraw");
                    agentinbox2.put("/timestamp", System.currentTimeMillis());
                    agentinbox2.put("/read", false);

                    agentinbox2.put("/fromuid", user.getUid());
                    mDatabase.child(agentsuuid).child("newinbox").child(String.valueOf(System.currentTimeMillis())).updateChildren(agentinbox2);


                } else if (task.isCanceled()) {
                    StyleableToast.makeText(context, "Error, try again", Toast.LENGTH_LONG, R.style.defaulttoastb).show();


                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1055 Login abgebrochen");

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                StyleableToast.makeText(context, "Error, something wrong", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

            }

        });


    }

    public void rejectRequest(final Activity context, final int position, final Integer artofrequest, final String agentsuuid, final View knopfView) {


        final Map<String, Object> updaterunning = new HashMap<>();


        mDatabase.child(agentsuuid).child("friends").child(user.getUid()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {


            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    //mbfriendsrequest.child(agentsuuid).child(user.getUid()).setValue(null);
                    mbfriendsrequest.child(user.getUid()).child(agentsuuid).setValue(null);
                    StyleableToast.makeText(context, "Request successfully rejected", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                    String pushkey = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

                    mDatabase.child(user.getUid()).child("pushkey").setValue(pushkey);
                    new SendPushinBg(pushkey, "rejected", AgentData.getagentname(), agentsuuid).execute();
                    //  final TextView friend_withdraw = knopfView.findViewById(R.id.friend_withdraw);
                    final TextView friend_dismiss = knopfView.findViewById(R.id.friend_dismiss);
                    final TextView friend_confirm = knopfView.findViewById(R.id.friend_confirm);
                    knopfView.setBackgroundColor(context.getResources().getColor(R.color.greytrans, context.getTheme()));
                    friend_confirm.setClickable(false);
                    friend_dismiss.setClickable(false);
                    friend_dismiss.setVisibility(View.GONE);
                    friend_confirm.setVisibility(View.GONE);
                    final Map<String, Object> agentinbox = new HashMap<>();


                    agentinbox.put("/text", AgentData.getagentname());


                    agentinbox.put("/title", "friend rejected");
                    agentinbox.put("/timestamp", System.currentTimeMillis());
                    agentinbox.put("/read", false);

                    agentinbox.put("/fromuid", user.getUid());
                    mDatabase.child(agentsuuid).child("newinbox").child(String.valueOf(System.currentTimeMillis())).updateChildren(agentinbox);


                } else if (task.isCanceled()) {
                    StyleableToast.makeText(context, "Error, try again", Toast.LENGTH_LONG, R.style.defaulttoastb).show();


                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1055 Login abgebrochen");

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                StyleableToast.makeText(context, "Error, something wrong", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

            }

        });


    }

    public void acceptRequest(final Activity context, final int position, final Integer artofrequest, final String agentsuuid, final View knopfView) {


        final Map<String, Object> updaterunning = new HashMap<>();

        updaterunning.put("/status", true);


        mDatabase.child(agentsuuid).child("friends").child(user.getUid()).updateChildren(updaterunning).addOnCompleteListener(new OnCompleteListener<Void>() {


            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mbfriendsrequest.child(user.getUid()).child(agentsuuid).setValue(null);

                    final Map<String, Object> childUpdates2 = new HashMap<>();
                    childUpdates2.put("/timestamp", System.currentTimeMillis() / 1000);
                    childUpdates2.put("/agentuid", agentsuuid);
                    childUpdates2.put("/status", true);
                    childUpdates2.put("/connected", "Code");

                    mDatabase.child(user.getUid()).child("friends").child(agentsuuid).updateChildren(childUpdates2).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                StyleableToast.makeText(context, "Request successfully accepted", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                String pushkey = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();

                                mDatabase.child(user.getUid()).child("pushkey").setValue(pushkey);
                                new SendPushinBgaccepted(pushkey, "accepted", AgentData.getagentname(), agentsuuid).execute();
                                //  final TextView friend_withdraw = knopfView.findViewById(R.id.friend_withdraw);
                                final TextView friend_dismiss = knopfView.findViewById(R.id.friend_dismiss);
                                final TextView friend_confirm = knopfView.findViewById(R.id.friend_confirm);
                                knopfView.setBackgroundColor(context.getResources().getColor(R.color.greytrans, context.getTheme()));
                                friend_confirm.setClickable(false);
                                friend_dismiss.setClickable(false);
                                friend_dismiss.setVisibility(View.GONE);
                                friend_confirm.setVisibility(View.GONE);

                            } else if (task.isCanceled()) {

                                Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {

                            Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                            //signOut();
                        }
                    });


                } else if (task.isCanceled()) {
                    StyleableToast.makeText(context, "Error, try again", Toast.LENGTH_LONG, R.style.defaulttoastb).show();


                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1055 Login abgebrochen");

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                StyleableToast.makeText(context, "Error, something wrong", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

            }

        });


    }

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    private class QRScanned extends AsyncTask<Object, Object, Void> {

        Handler handler = new Handler();

        public QRScanned(String toast) {
            theqrcodestring = toast;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i("LOOOOOOOOG", "HERE3");

        }

        @Override
        protected Void doInBackground(Object... params) {
            handler.post(new Runnable() {

                @Override
                public void run() {

                    new AODConfig("qrcloser", getApplicationContext()).tempscanCodescanned(user.getEmail(), theqrcodestring, AgentData.getagentname(), true, true);
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            Log.i("SCAN DONE ->", "678889");

            AgentData.getscanproof = 0;

            if (qrsavelistener != null) {
                qrdatabase.removeEventListener(qrsavelistener);
                qrsavelistener = null;
            }

            if (listenertempscansave != null) {
                qrdatabase.removeEventListener(listenertempscansave);
                listenertempscansave = null;
            }
            pref.edit().remove("scanedagnet").apply();
            pref.edit().remove("hasalldata").apply();
            pref.edit().remove("isscaned").apply();

            //   new API("qrcloser", getApplicationContext()).tempscanCoderemove(user.getEmail(),theqrcodestring, nameofagent);

        }
    }

    //GET AGENTS DETAILS WITH TOKEN
    private class SendPushinBg extends AsyncTask<String, Void, String> {
        //sendpushaccepted
        String thepushid, theart, theagent, sendtoa;

        public SendPushinBg(String s, String art, String agentname, String sendTo) {
            thepushid = s;
            theart = art;
            theagent = agentname;
            sendtoa = sendTo;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {


            String regurl = BuildConfig.BASEURL + "sendpush.php?pw=" + thepushid + "&art=" + theart + "&agent=" + theagent + "&uid=" + user.getUid() + "&to=" + sendtoa;
            StringBuilder sb = null;
            BufferedReader reader = null;
            String serverResponse = null;
            try {

                URL url = new URL(regurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(200);
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                //Log.e("statusCode", "" + statusCode);
                if (statusCode == 200) {
                    sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                }

                connection.disconnect();
                if (sb != null)
                    serverResponse = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return serverResponse;
        }

        @Override
        protected void onPostExecute(String json) {

            if (json != null) {


            }
        }
    }

    //GET AGENTS DETAILS WITH TOKEN
    private class SendPushinBgaccepted extends AsyncTask<String, Void, String> {
        //sendpushaccepted
        String thepushid, theart, theagent, sendtoa;

        public SendPushinBgaccepted(String s, String art, String agentname, String sendTo) {
            thepushid = s;
            theart = art;
            theagent = agentname;
            sendtoa = sendTo;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {


            String regurl = BuildConfig.BASEURL + "sendpushaccepted.php?pw=" + thepushid + "&art=" + theart + "&agent=" + theagent + "&uid=" + user.getUid() + "&to=" + sendtoa;


            StringBuilder sb = null;
            BufferedReader reader = null;
            String serverResponse = null;
            try {

                URL url = new URL(regurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(200);
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                //Log.e("statusCode", "" + statusCode);
                if (statusCode == 200) {
                    sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                }

                connection.disconnect();
                if (sb != null)
                    serverResponse = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return serverResponse;
        }

        @Override
        protected void onPostExecute(String json) {

            if (json != null) {


            }
        }
    }
}