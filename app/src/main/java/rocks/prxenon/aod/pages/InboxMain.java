package rocks.prxenon.aod.pages;


import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.InboxAdapter;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.receiver.NetworkStateReceiver;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mbfriendsrequest;
import static rocks.prxenon.aod.basics.AODConfig.networkStateReceiver;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;


public class InboxMain extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {


    final List<String> friendid = new ArrayList();
    final List<Boolean> friendstatus = new ArrayList();

    private Typeface type, proto;
    private TextView mTitleTextView;

    private AlertDialog.Builder dialogBuilder;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;
    private ListView inboxlist;
    private FrameLayout frameempty;


    @Override

    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_inbox);

        bindActivity();
        new AODConfig("default", this);
        //Get Firebase auth instance

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);


        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");

        frameempty = findViewById(R.id.frameempty);
        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);


        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_noaction, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText("INBOX");


        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);

        inboxlist = findViewById(R.id.inbox_list);


    }

    private void bindActivity() {


    }


    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();
        pref = getSharedPreferences("AppPref", MODE_PRIVATE);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        frameempty.setVisibility(View.GONE);
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);


        // new GetNotifications().getFriendRequests(this, getApplicationContext(), notify_count_image, notify_count_text);

        mbfriendsrequest.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot snapshotofrequest) {
                getinbox(snapshotofrequest);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("SNAP NOT EXIST NOTIFY", "error");
                finish();
            }
        });


    }

    private void getinbox(final DataSnapshot ofrequests) {
        final List<Long> timestamp = new ArrayList();
        final List<String> agentuid = new ArrayList();

        final List<String> inboxart = new ArrayList();
        final List<String> linkidpvp = new ArrayList();

        final List<Boolean> readstatus = new ArrayList();
        final List<String> title = new ArrayList();
        final List<String> inboxid = new ArrayList();
        mDatabase.child(user.getUid()).child("newinbox").addListenerForSingleValueEvent(new ValueEventListener() {
            long loopcount = 0;

            @Override
            public void onDataChange(DataSnapshot snapshotofinbox) {
                long loopersum = snapshotofinbox.getChildrenCount() + ofrequests.getChildrenCount();

                Log.i("SUM LOOPER -_>", "" + loopersum + " s: " + snapshotofinbox.getChildrenCount());
                if (ofrequests.getChildrenCount() > 0) {
                    // START
                    Log.i("HAS CHILDS COUNR ->", "" + ofrequests.getChildrenCount());
                    for (DataSnapshot friendsrequests : ofrequests.getChildren()) {
                        loopcount++;
                        inboxart.add("friendrequest");
                        agentuid.add(friendsrequests.getKey());

                        timestamp.add(Long.parseLong(friendsrequests.child("timestamp").getValue().toString()));
                        readstatus.add(false);
                        inboxid.add("null");
                        title.add("New friend request");


                        //  Log.i("timestamp", historyis.getValue().toString());

                    }

                    for (DataSnapshot inboxdata : snapshotofinbox.getChildren()) {

                        if (inboxdata.child("fromuid").exists()) {
                            loopcount++;
                            Log.i("HAS CHILDS COUNt 2 ->", "" + snapshotofinbox.getChildrenCount());
                            agentuid.add(inboxdata.child("fromuid").getValue().toString());
                            timestamp.add(Long.parseLong(inboxdata.getKey()));
                            readstatus.add(Boolean.parseBoolean(inboxdata.child("read").getValue().toString()));
                            if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("new friend")) {
                                inboxart.add("friendaccepted");
                                linkidpvp.add("null");
                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("friend rejected")) {
                                inboxart.add("friendrejected");
                                linkidpvp.add("null");

                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("passcode")) {
                                inboxart.add("passcode");
                                linkidpvp.add("null");
                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("PvP invite")) {
                                inboxart.add("pvpinvite");
                                linkidpvp.add(inboxdata.child("link").getValue().toString());
                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("PvP accepted")) {
                                inboxart.add("pvpaccepted");
                                linkidpvp.add(inboxdata.child("link").getValue().toString());
                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("PvP rejected")) {
                                inboxart.add("pvprejected");
                                linkidpvp.add(inboxdata.child("link").getValue().toString());
                            } else {
                                inboxart.add("other");
                                linkidpvp.add("null");
                            }
                            inboxid.add(inboxdata.getKey());
                            title.add(inboxdata.child("title").getValue().toString());
                        } else {
                            loopcount++;
                            if (snapshotofinbox.getChildrenCount() > 0) {
                                mDatabase.child(user.getUid()).child("newinbox").child(inboxdata.getKey()).setValue(null);
                            }
                        }
                        //  Log.i("timestamp", historyis.getValue().toString());

                    }

                } else {
                    for (DataSnapshot inboxdata : snapshotofinbox.getChildren()) {

                        if (inboxdata.child("fromuid").exists()) {
                            loopcount++;
                            Log.i("HAS CHILDS COUNt 2 ->", "" + snapshotofinbox.getChildrenCount());
                            agentuid.add(inboxdata.child("fromuid").getValue().toString());
                            timestamp.add(Long.parseLong(inboxdata.getKey()));
                            readstatus.add(Boolean.parseBoolean(inboxdata.child("read").getValue().toString()));
                            if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("new friend")) {
                                inboxart.add("friendaccepted");
                                linkidpvp.add("null");
                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("friend rejected")) {
                                inboxart.add("friendrejected");
                                linkidpvp.add("null");

                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("passcode")) {
                                inboxart.add("passcode");
                                linkidpvp.add("null");
                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("PvP invite")) {
                                inboxart.add("pvpinvite");
                                linkidpvp.add(inboxdata.child("link").getValue().toString());
                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("PvP accepted")) {
                                inboxart.add("pvpaccepted");
                                linkidpvp.add(inboxdata.child("link").getValue().toString());
                            } else if (inboxdata.child("title").getValue().toString().equalsIgnoreCase("PvP rejected")) {
                                inboxart.add("pvprejected");
                                linkidpvp.add(inboxdata.child("link").getValue().toString());
                            } else {
                                inboxart.add("other");
                                linkidpvp.add("null");
                            }
                            inboxid.add(inboxdata.getKey());
                            title.add(inboxdata.child("title").getValue().toString());
                        } else {
                            loopcount++;
                            if (snapshotofinbox.getChildrenCount() > 0) {
                                mDatabase.child(user.getUid()).child("newinbox").child(inboxdata.getKey()).setValue(null);
                            }
                        }

                    }
                }

                if (loopcount == loopersum) {
                    Log.i("Loopercount--->", "" + loopcount + " size -->" + agentuid.size() + " soll :" + loopersum);

                    if (loopersum == 0) {

                        frameempty.setVisibility(View.VISIBLE);
                        progressBarHolder.setVisibility(View.GONE);
                    } else {
                        frameempty.setVisibility(View.GONE);
                        Collections.reverse(inboxart);
                        Collections.reverse(title);
                        Collections.reverse(timestamp);
                        Collections.reverse(agentuid);
                        Collections.reverse(readstatus);
                        Collections.reverse(linkidpvp);
                        Collections.reverse(inboxid);

                        coordinator.setVisibility(View.VISIBLE);
                        progressBarHolder.setVisibility(View.GONE);
                        InboxAdapter adapter5 = new InboxAdapter(InboxMain.this, inboxart, title, timestamp, agentuid, readstatus, linkidpvp, inboxid);
                        inboxlist.setAdapter(adapter5);
                        adapter5.notifyDataSetChanged();

                        inboxlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                //  StyleableToast.makeText(getApplicationContext(), "Pos ->"+i, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("SNAP NOT EXIST NOTIFY", "error");
                finish();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");
        //pref.edit().remove("scanedagnet").apply();

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }


        try {
            if (networkStateReceiver != null) {
                this.unregisterReceiver(networkStateReceiver);
                Log.i("", "broadcastReceiver unregistered");
            }
        } catch (Exception e) {
            Log.i("", "broadcastReceiver is already unregistered");
            networkStateReceiver = null;
        }


    }

    @Override
    public void networkAvailable() {




        /* TODO: Your connection-oriented stuff here */
    }

    @Override
    public void networkUnavailable() {



        /* TODO: Your disconnection-oriented stuff here */
    }


    @Override
    public void onBackPressed() {


        super.onBackPressed();


    }


}