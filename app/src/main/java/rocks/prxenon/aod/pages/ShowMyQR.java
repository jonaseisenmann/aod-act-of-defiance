package rocks.prxenon.aod.pages;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.scanner.Contents;
import rocks.prxenon.aod.scanner.QRCodeEncoder;
import rocks.prxenon.aod.signin.FirebaseUIActivity;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.scanisopen;
import static rocks.prxenon.aod.basics.AODConfig.user;


public class ShowMyQR extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static Context ctx;

    TelephonyManager mTelephonyManager;
    MyPhoneStateListener mPhoneStatelistener;
    int mSignalStrength = 0;
    FirebaseDatabase database;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseAuth auth;
    private ProgressBar progressBar;
    private SharedPreferences pref;
    private ImageView bgani;
    private Typeface type, proto;
    private ImageView qrcodeimg;
    private RelativeLayout progressbarx;
    private String nameofagent;
    private String theqrstring;
    private TextView mTitleTextView, qrscrenhelptext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        pref = getSharedPreferences("AppPref", MODE_PRIVATE);
        pref.registerOnSharedPreferenceChangeListener(this);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");
        nameofagent = AgentData.getagentname();
        AgentData.getscanproof = 0;

        pref.edit().putBoolean("hasalldata", false).apply();

        System.out.println("NETWORK" + hasConnectivity());

        ctx = getApplicationContext();
        // set the view now
        setContentView(R.layout.activity_show_qr);

        progressbarx = findViewById(R.id.progressBarx);
        qrscrenhelptext = findViewById(R.id.qrscrenhelptext);
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_noaction, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(Html.fromHtml("QR CODE" + "   <small>" + nameofagent + "</small>"));
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(25);

        progressBar = findViewById(R.id.progressBar);
        qrcodeimg = findViewById(R.id.qrdraw);
        bgani = findViewById(R.id.bgani);


    }

    private boolean savedqr(String theqrcodestring) {

        new AODConfig("qropener", this).tempscanCode(user.getEmail(), theqrcodestring, nameofagent, true, false);
        return true;
    }

    private void displayToast(String gettost) {
        if (ShowMyQR.this != null && gettost != null) {
            Toast.makeText(ShowMyQR.this, gettost, Toast.LENGTH_LONG).show();
            // gettost = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (user != null) {
            mPhoneStatelistener = new MyPhoneStateListener();
            mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            mTelephonyManager.listen(mPhoneStatelistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
            nameofagent = AgentData.getagentname();

            Log.i("FACTION LOG->", " " + new AgentData().getAgentdata(AODConfig.child2));


            // User is signed in
        } else {
            startActivity(new Intent(this, FirebaseUIActivity.class));
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        Log.i("Start", "901");

    }

    @Override
    public void onStop() {
        super.onStop();
        new AODConfig("qrcloser", this).tempscanCoderemove(user.getEmail(), theqrstring, nameofagent);
        pref.edit().remove("isscaned").apply();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        new AODConfig("qrcloser", this).tempscanCoderemove(user.getEmail(), theqrstring, nameofagent);
        pref.edit().remove("isscaned").apply();
    }

    @Override
    public void onBackPressed() {
        // REMOVE QR FROM FIREBASE

        // new API("qrcloser", this).tempscanCoderemove(user.getEmail(),theqrstring, nameofagent);
        pref.edit().remove("isscaned").apply();
        super.onBackPressed();


    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equalsIgnoreCase("isscaned")) {
            if (pref.contains("isscaned")) {
                if (pref.getBoolean("isscaned", false)) {
                    //Log.i("Changed true", API.originalagent + " " + API.scanisopen);
                    new QRUpdateifscanned().execute();


                } else {
                    //Log.i("Changed false", API.originalagent + " " + API.scanisopen);
                }
            }

        }


        //System.out.println("9991 show QR" + key);
    }

    private void stratqr(boolean hassignal) {

        //  generateQR();
        if (hassignal) {
            new QRTask().execute();


            mDatabase.child(user.getUid()).child(AODConfig.child1).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    //System.out.println(snapshot.getValue().toString() + "6765");  //prints "Do you have data? You'll love Firebase."
                    nameofagent = snapshot.getValue().toString();
                    // Toast.makeText(showQR.this, nameofagent, Toast.LENGTH_SHORT).show();

                    // TextView subText = boomButton.getSubTextView();
                    //if (subText != null) subText.setText("I'm changed, too!");
                }

                @Override
                public void onCancelled(DatabaseError error) {
                }
            });
        } else {
            final View rootlayout = getWindow().getDecorView().findViewById(android.R.id.content);
            Snackbar snack = Snackbar.make(rootlayout, "No Network",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();

                        }
                    });

            snack.show();
        }
    }

    public void closeit() {
        scanisopen = false;
        finish();

    }

    public boolean updateifscanned(String original, String scannedby, boolean isopen, boolean scaned) {
        // new API("qrcloser", this).tempscanCoderemove(user.getEmail(),theqrstring, nameofagent);

        return nameofagent.equalsIgnoreCase(scannedby);
    }


    // ON SCANED

    public boolean hasConnectivity() {

        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        int tries = 0;
        while (tries++ < 1) {
            NetworkInfo info = cm.getActiveNetworkInfo();
            if (info != null && info.isConnected()) {
                NetworkInfo.DetailedState state = info.getDetailedState();
                if (state == NetworkInfo.DetailedState.CONNECTED) {
                    return true;
                }
            }
            try {
                Thread.sleep(10L);
            } catch (InterruptedException e) {
            }
        }
        return false;


    }

    private class QRTask extends AsyncTask<Object, Object, Bitmap> {
        FirebaseUser userx;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Log.i("LOOOOOOOOG", "HERE3");
            qrscrenhelptext.setText(getText(R.string.qrcodedesc));
            progressbarx.setVisibility(View.VISIBLE);

        }

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
            Display display = manager.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            int width = point.x;
            int height = point.y;
            int smallerDimension = width < height ? width : height;
            smallerDimension = smallerDimension * 3 / 4;
            theqrstring = new AODConfig().QRId(nameofagent);

            Log.i("QR STRING-->", "" + theqrstring);

            savedqr(theqrstring);
            //Encode with a QR Code image
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder("https://aod.prxenon.rocks/scan/" + theqrstring,
                    null,
                    Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(),
                    smallerDimension);
            try {
                bitmap = qrCodeEncoder.encodeAsBitmap();


                return bitmap;

            } catch (WriterException e) {
                e.printStackTrace();
                return bitmap;
            }

            //return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            qrcodeimg.setImageBitmap(result);
            progressbarx.setVisibility(View.GONE);
        }
    }

    private class QRUpdateifscanned extends AsyncTask<Object, Object, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i("showQR 6676", "Code scanned");
            //  qrscrenhelptext.setText(getText(R.string.qrcodedesc));
            //  progressbarx.setVisibility(View.VISIBLE);

        }

        @Override
        protected Boolean doInBackground(Object... params) {

            boolean isdone = updateifscanned(AODConfig.originalagent, AODConfig.scanneragent, true, true);
            return isdone;
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if (result) {
                Log.i("676464664 -->", "isdone");
                new AODConfig("qrcloser", getApplicationContext()).tempscanCoderemove(user.getEmail(), theqrstring, nameofagent);
                finish();
            } else {
                new AODConfig("qrcloser", getApplicationContext()).tempscanCoderemove(user.getEmail(), theqrstring, nameofagent);
                pref.edit().remove("isscaned").apply();

                finish();

            }

        }
    }

    private class MyPhoneStateListener extends PhoneStateListener {

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            mSignalStrength = signalStrength.getGsmSignalStrength();
            mSignalStrength = (2 * mSignalStrength) - 113; // -> dBm
            if (mSignalStrength > -103) {
                stratqr(true);
            } else {
                // // TODO: 14.10.2017  
                stratqr(true);
            }

            Log.i("SIGNAL -->", "" + mSignalStrength);
            mTelephonyManager.listen(mPhoneStatelistener, PhoneStateListener.LISTEN_NONE);
        }
    }
}