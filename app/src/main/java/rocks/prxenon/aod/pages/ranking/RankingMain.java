package rocks.prxenon.aod.pages.ranking;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.perf.metrics.AddTrace;
import com.muddzdev.styleabletoast.StyleableToast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.RankingAdapterNew;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.helper.PicassoCache;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;
import static rocks.prxenon.aod.basics.AODConfig.mDatabasemissions;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnung;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;


public class RankingMain extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private ProgressBar progressBarm, progress_gold, progress_platin, progress_black;
    private Animation animZoomIn, animZoomNull;
    private Typeface type, proto;
    private TextView mTitleTextView, subline, rank_anzahl_agents, rank_own_progress, onbadgestatus, badgetask, badge_statusleiste, monthof;
    private TextView missionname;
    private CircleImageView missionimage, missionimage2;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private AppBarLayout appbarlayout;
    private CollapsingToolbarLayout collapingtoolbar;
    private Toolbar toolbarx;
    private TextView agents_ownrank;
    private Bundle bundle;
    private ListView rankinglist;
    private NestedScrollView netstescroll;
    private ImageButton share_rank;
    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;
    private TabLayout tabLayout;


    private LinearLayout closedframe;
    private State mCurrentState = State.IDLE;

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    public static String getMonthShortName(int monthNumber) {
        String monthName = "";

        if (monthNumber >= 0 && monthNumber < 12)
            try {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.MONTH, monthNumber);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM");
                //simpleDateFormat.setCalendar(calendar);
                monthName = simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                if (e != null)
                    e.printStackTrace();
            }
        return monthName;
    }

    public static Bitmap getScreenShot(View view) {
        View screenView = view;
        screenView.findViewById(R.id.share_rank).setVisibility(View.GONE);
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_ranking);


        bundle = getIntent().getExtras();
        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);

        if (bundle == null) {


            finish();

        } else {
            bindActivity();
            new AODConfig("default", this);
            //Get Firebase auth instance

            pref = getSharedPreferences("AppPref", MODE_PRIVATE);

            appbarlayout = findViewById(R.id.appbarlayout);
            collapingtoolbar = findViewById(R.id.collapingtoolbar);
            toolbarx = findViewById(R.id.toolbarx);
            closedframe = findViewById(R.id.closedframe);
            appbarlayout.addOnOffsetChangedListener(this);
            tabLayout = findViewById(R.id.tabsrank);

            Log.i("TABS", "" + tabLayout.getTabCount());
            new AgentData("prefs", pref);

            // TABS SET
            if (pref.getBoolean("hasfriends", false)) {
                tabLayout.addTab(tabLayout.newTab().setText("FRIENDS"));
                //  removeTab(1);

            }


            if (pref.getBoolean("hasteam", false)) {
                tabLayout.addTab(tabLayout.newTab().setText("TEAM"));
            }

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    if (tabLayout.getSelectedTabPosition() == 0) {

                        startranking(bundle);

                        //  Toast.makeText(RankingMain.this, "Tab 0 " + tabLayout.getSelectedTabPosition()+ " "+tab.getText().toString(), Toast.LENGTH_LONG).show();

                    } else {
                        //   Toast.makeText(RankingMain.this, "Tab  " + tabLayout.getSelectedTabPosition()+ " "+tab.getText().toString(), Toast.LENGTH_LONG).show();
                        if (tab.getText().toString().equalsIgnoreCase("FRIENDS")) {
                            // Toast.makeText(RankingMain.this, "FRIENDS", Toast.LENGTH_LONG).show();
                            startrankingfriends(bundle);
                        } else {
                            // TODO TEAM RANKING
                            //  Toast.makeText(RankingMain.this, "TEAM", Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });


            // END TABS SET
            type = Typeface.createFromAsset(getAssets(), "main.ttf");
            proto = Typeface.createFromAsset(getAssets(), "main.ttf");


            ActionBar mActionBar = getSupportActionBar();
            assert mActionBar != null;
            mActionBar.setDisplayShowHomeEnabled(true);
            mActionBar.setDisplayShowTitleEnabled(false);
            LayoutInflater mInflater = LayoutInflater.from(this);

            View actionBar = mInflater.inflate(R.layout.custom_actionbar_ranking, null);
            mTitleTextView = actionBar.findViewById(R.id.title_text);
            subline = actionBar.findViewById(R.id.titlesub_text);
            mTitleTextView.setText(R.string.title_activity_ranking);
            mActionBar.setCustomView(actionBar);
            mActionBar.setDisplayShowCustomEnabled(true);
            ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

            netstescroll = findViewById(R.id.netstescroll);
            monthof = findViewById(R.id.monthof);
            mTitleTextView.setTypeface(type);
            mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);

            if (bundle.getString("artis").equalsIgnoreCase("month")) {
                subline.setText(getResources().getString(R.string.month_mission_text));
                monthof.setText(getMonthShortName((Integer.parseInt(bundle.getString("themonth")) - 1)));

            }

            if (bundle.getString("artis").equalsIgnoreCase("week")) {
                subline.setText(getResources().getString(R.string.week_mission_text));
                monthof.setText("");
            }

            if (bundle.getString("artis").equalsIgnoreCase("mini")) {
                subline.setText("MINIMISSION");
                monthof.setText("");
            }

            share_rank = findViewById(R.id.share_rank);
            share_rank.setVisibility(View.GONE);
            /*
              iranking.putExtra("artis", "month");
                iranking.putExtra("theyear", theyearx);
                iranking.putExtra("themonth", themonthx);
                iranking.putExtra("theactionid", cmonthmissionid);
                iranking.putExtra("badgedrawable", monthbadgedrawable);
             */


            agents_ownrank = findViewById(R.id.agents_ownrank);
            rank_anzahl_agents = findViewById(R.id.rank_anzahl_agents);
            rank_own_progress = findViewById(R.id.rank_own_progress);
            rankinglist = findViewById(R.id.rankinglist);
            missionname = findViewById(R.id.missionname);
            missionimage = findViewById(R.id.missionimage);
            onbadgestatus = findViewById(R.id.onbadgestatus);
            progress_gold = findViewById(R.id.progress_gold);
            progress_platin = findViewById(R.id.progress_platin);
            progress_black = findViewById(R.id.progress_black);
            missionimage2 = findViewById(R.id.missionimage2);
            badgetask = findViewById(R.id.badgetask);
            badge_statusleiste = findViewById(R.id.badge_statusleiste);

            missionname.setTypeface(proto);
            missionname.setText(AgentData.getagentname());


            badgetask.setSelected(true);


            missionimage.setBorderColor(getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));
            missionimage2.setBorderColor(getResources().getColor(R.color.colorWhite, getTheme()));


            int badge_id = getResources().getIdentifier(bundle.getString("badgedrawable"), "drawable", getPackageName());

            //badgemonth = getResources().obtainTypedArray(badgemonthid);
            if (badge_id != 0) {
                //   Log.w("BADGE -->", " id :"+badgemonthid_content);
                //getDrawable(badge_id)

                // ImageView month_community_content = findViewById(R.id.content_community_month);
                // month_community_content.setImageDrawable(getDrawable(badge_id));

                try {
                    PicassoCache.getPicassoInstance(getApplicationContext())
                            .load(badge_id)
                            .resize(400, 400)
                            .centerInside()
                            .placeholder(R.drawable.full_logo)
                            .error(R.drawable.full_logo)

                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                            //.transform(new CropRoundTransformation())
                            .into(missionimage);
                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());

                    e.printStackTrace();
                }
                try {
                    PicassoCache.getPicassoInstance(getApplicationContext())
                            .load(badge_id)
                            .placeholder(R.drawable.full_logo)
                            .error(R.drawable.full_logo)
                            .resize(80, 80)
                            .centerInside()

                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                            //.transform(new CropRoundTransformation())
                            .into(missionimage2);
                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());

                    e.printStackTrace();
                }
            }

        }


        // progressBar = (ProgressBar) findViewById(R.id.progressBar);

        // bgani = (ImageView) findViewById(R.id.bgani);
        new AODConfig();
        startranking(bundle);

    }

    private void bindActivity() {

        closedframe = findViewById(R.id.closedframe);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;
        if (offset == 0) {
            Log.i("OFFSET1 --> ", "" + offset);
            setCurrentStateAndNotify(appBarLayout, State.EXPANDED);
            // netstescroll.setNestedScrollingEnabled(true);

            // handleAlphaOnTitle(percentage);
            closedframe.setVisibility(View.INVISIBLE);
        } else if (Math.abs(offset) >= appBarLayout.getTotalScrollRange()) {
            setCurrentStateAndNotify(appBarLayout, State.COLLAPSED);
            //  handleAlphaOnTitle(percentage);
            Log.i("OFFSET2 --> ", "" + offset);
            closedframe.setVisibility(View.VISIBLE);

            //netstescroll.setNestedScrollingEnabled(false);
        } else {
            Log.i("OFFSET3 --> ", "" + offset);
            setCurrentStateAndNotify(appBarLayout, State.IDLE);
        }
        // handleToolbarTitleVisibility(percentage);
    }

    private void setCurrentStateAndNotify(AppBarLayout appBarLayout, State state) {
        if (mCurrentState != state) {
            onStateChanged(appBarLayout, state);
        }
        mCurrentState = state;
    }

    public void onStateChanged(AppBarLayout appBarLayout, State state) {

    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(closedframe, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(closedframe, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();
        new AODConfig("default", this);


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");


    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");


    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();


    }

    private void startranking(final Bundle bundleis) {
        // GET AGENTSDATA FOR THE MISSION
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);
        Log.i("1 -->", "y = " + bundleis.getString("theyear"));
        Log.i("2 -->", "m = " + bundleis.getString("artis"));
        Log.i("3 -->", "1 = " + bundleis.getString("theactionid"));
        Query myTopPostsQuery = mDatabaserunnung.child(bundleis.getString("theyear")).child(bundleis.getString("artis")).child(bundleis.getString("theactionid")).orderByChild("currentvalue");
        final HashMap<String, String> data = new HashMap<String, String>();
        final List<String> listUID = new ArrayList();
        final List<Long> rankUID = new ArrayList();
        final List<Integer> positionUID = new ArrayList();
        final List<Long> timestamprank = new ArrayList();

        progress_gold.setMax(bundle.getInt("monthgold"));
        progress_platin.setMax(bundle.getInt("monthplatin"));
        progress_black.setMax(bundle.getInt("monthblack"));

        mDatabase.child(user.getUid()).child("badges").child(bundleis.getString("theactionid")).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotbadges) {

                if (snapshotbadges.hasChild("badge") && !bundleis.getString("artis").equalsIgnoreCase("mini")) {
                    if (snapshotbadges.child("badge").getValue().toString().equalsIgnoreCase("GOLD")) {
                        onbadgestatus.setText("PLATIN");
                    } else if (snapshotbadges.child("badge").getValue().toString().equalsIgnoreCase("PLATIN")) {
                        onbadgestatus.setText("BLACK");
                    } else if (snapshotbadges.child("badge").getValue().toString().equalsIgnoreCase("BLACK")) {
                        onbadgestatus.setText("BLACK");
                    }
                } else {
                    onbadgestatus.setText("STATS");
                }

                if (bundleis.getString("artis").equalsIgnoreCase("mini")) {
                    mDatabasemissions.child(bundleis.getString("artis")).child(bundleis.getString("themonth")).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshotmission) {

                            mDatabaseaod.child("badge").child(snapshotmission.child("badge").getValue().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(final DataSnapshot snapshotbadge) {

                                    missionname.setText(snapshotbadge.child("title").getValue().toString());
                                    badgetask.setText(snapshotbadge.child("short2").getValue().toString());
                                    badge_statusleiste.setText(snapshotbadge.child("title").getValue().toString());


                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                    coordinator.setVisibility(View.VISIBLE);
                                    progressBarHolder.setVisibility(View.GONE);
                                }
                            });


                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                            coordinator.setVisibility(View.VISIBLE);
                            progressBarHolder.setVisibility(View.GONE);
                        }
                    });
                } else {

                    mDatabasemissions.child(bundleis.getString("artis")).child(bundleis.getString("theyear")).child(bundleis.getString("themonth")).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshotmission) {

                            mDatabaseaod.child("badge").child(snapshotmission.child("badge").getValue().toString()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(final DataSnapshot snapshotbadge) {

                                    missionname.setText(snapshotbadge.child("title").getValue().toString());
                                    badgetask.setText(snapshotbadge.child("short2").getValue().toString());
                                    badge_statusleiste.setText(snapshotbadge.child("title").getValue().toString());


                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                    coordinator.setVisibility(View.VISIBLE);
                                    progressBarHolder.setVisibility(View.GONE);
                                }
                            });


                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                            coordinator.setVisibility(View.VISIBLE);
                            progressBarHolder.setVisibility(View.GONE);
                        }
                    });
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            progress_gold.setProgress(0, true);
            progress_platin.setProgress(0, true);
            progress_black.setProgress(0, true);
        } else {
            progress_gold.setProgress(0);
            progress_platin.setProgress(0);
            progress_black.setProgress(0);
        }

       /*
              iranking.putExtra("artis", "month");
                iranking.putExtra("theyear", theyearx);
                iranking.putExtra("themonth", themonthx);
                iranking.putExtra("theactionid", cmonthmissionid);
                iranking.putExtra("badgedrawable", monthbadgedrawable);

                  iranking.putExtra("monthgold", goalgold);
        iranking.putExtra("monthplatin", goalplatin);
        iranking.putExtra("monthblack", goalblack);
             */

        // RANKING FOR THE AGENT
        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot child : snapshot.getChildren()) {
                    if (Long.parseLong(child.child("currentvalue").getValue().toString()) > 0) {
                        rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                        timestamprank.add(Long.parseLong(child.child("timestamp").getValue().toString()));
                        String test = child.child("uid").getValue().toString();
                        listUID.add(test);
                    }
                }
                Collections.reverse(listUID);
                Collections.reverse(rankUID);
                Collections.reverse(timestamprank);
                //  mList.indexOf(user.getUid())
                int ownpositioninList = listUID.indexOf(user.getUid());
                int ownrankis = 0;
                boolean hasranking = false;

                rank_anzahl_agents.setText("" + rankUID.size());
                if (ownpositioninList == -1) {
                    rank_own_progress.setText("-");
                    agents_ownrank.setText("-");
                    for (int i = 0; i < rankUID.size(); i++) {
                        //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                        if (i == 0) {

                            ownrankis++;
                            positionUID.add(ownrankis);
                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                hasranking = true;
                                if (rankUID.get(i) > 0) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                        progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                        progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                    } else {
                                        progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                        progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                        progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                    }
                                    rank_own_progress.setText("" + rankUID.get(i));
                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    agents_ownrank.setText("" + ownrankis);

                                } else {
                                    if (!hasranking) {
                                        agents_ownrank.setText("-");
                                        rank_own_progress.setText("-");
                                        progress_gold.setProgress(0);
                                        progress_platin.setProgress(0);
                                        progress_black.setProgress(0);
                                    }

                                }
                            } else {
                                if (!hasranking) {
                                    agents_ownrank.setText("-");
                                    rank_own_progress.setText("0");
                                    progress_gold.setProgress(0);
                                    progress_platin.setProgress(0);
                                    progress_black.setProgress(0);
                                }
                            }
                        } else {

                            ownrankis++;
                            positionUID.add(ownrankis);
                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                hasranking = true;
                                //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                if (rankUID.get(i) > 0) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                        progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                        progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                    } else {
                                        progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                        progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                        progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                    }
                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    agents_ownrank.setText("" + ownrankis);
                                    rank_own_progress.setText("" + rankUID.get(i));
                                } else {
                                    if (!hasranking) {
                                        agents_ownrank.setText("-");
                                        rank_own_progress.setText("-");
                                        progress_gold.setProgress(0);
                                        progress_platin.setProgress(0);
                                        progress_black.setProgress(0);
                                    }
                                }
                            } else {
                                if (!hasranking) {
                                    agents_ownrank.setText("-");
                                    rank_own_progress.setText("0");
                                    progress_gold.setProgress(0);
                                    progress_platin.setProgress(0);
                                    progress_black.setProgress(0);
                                }
                            }


                        }
                    }
                    //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                    if (ownrankis <= 0) {

                        agents_ownrank.setText("-");
                    }

                } else {
                    for (int i = 0; i < rankUID.size(); i++) {
                        //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                        if (i == 0) {

                            ownrankis++;
                            positionUID.add(ownrankis);
                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                hasranking = true;
                                if (rankUID.get(i) > 0) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                        progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                        progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                    } else {
                                        progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                        progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                        progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                    }
                                    rank_own_progress.setText("" + rankUID.get(i));
                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    agents_ownrank.setText("" + ownrankis);

                                } else {
                                    if (!hasranking) {
                                        agents_ownrank.setText("-");
                                        rank_own_progress.setText("-");
                                        progress_gold.setProgress(0);
                                        progress_platin.setProgress(0);
                                        progress_black.setProgress(0);
                                    }

                                }
                            } else {
                                if (!hasranking) {
                                    agents_ownrank.setText("-");
                                    rank_own_progress.setText("0");
                                    progress_gold.setProgress(0);
                                    progress_platin.setProgress(0);
                                    progress_black.setProgress(0);
                                }
                            }
                        } else {
                            if (rankUID.get(i) > rankUID.get(ownpositioninList)) {
                                ownrankis++;
                                positionUID.add(ownrankis);
                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    hasranking = true;
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                            progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                            progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                        } else {
                                            progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                            progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                            progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                        }
                                        rank_own_progress.setText("" + rankUID.get(i));
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        agents_ownrank.setText("" + ownrankis);
                                    } else {
                                        if (!hasranking) {
                                            agents_ownrank.setText("-");
                                            rank_own_progress.setText("-");
                                            progress_gold.setProgress(0);
                                            progress_platin.setProgress(0);
                                            progress_black.setProgress(0);
                                        }
                                    }
                                } else {
                                    if (!hasranking) {
                                        agents_ownrank.setText("-");
                                        rank_own_progress.setText("0");
                                        progress_gold.setProgress(0);
                                        progress_platin.setProgress(0);
                                        progress_black.setProgress(0);
                                    }
                                }
                            } else {
                                ownrankis++;
                                positionUID.add(ownrankis);
                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    hasranking = true;
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                            progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                            progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()), true);
                                        } else {
                                            progress_gold.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                            progress_platin.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                            progress_black.setProgress(Integer.parseInt(rankUID.get(i).toString()));
                                        }
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        agents_ownrank.setText("" + ownrankis);
                                        rank_own_progress.setText("" + rankUID.get(i));
                                    } else {
                                        if (!hasranking) {
                                            agents_ownrank.setText("-");
                                            rank_own_progress.setText("-");
                                            progress_gold.setProgress(0);
                                            progress_platin.setProgress(0);
                                            progress_black.setProgress(0);
                                        }
                                    }
                                } else {
                                    if (!hasranking) {
                                        agents_ownrank.setText("-");
                                        rank_own_progress.setText("0");
                                        progress_gold.setProgress(0);
                                        progress_platin.setProgress(0);
                                        progress_black.setProgress(0);
                                    }
                                }
                            }

                        }
                    }
                    //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                    if (ownrankis <= 0) {

                        agents_ownrank.setText("-");
                    }

                }


                if (positionUID.size() > 0) {
                    RankingAdapterNew adapter5 = new RankingAdapterNew(RankingMain.this, listUID, rankUID, positionUID, bundle.getString("theyear"), bundle.getString("themonth"), timestamprank, ownpositioninList, bundleis.getString("artis"));
                    rankinglist.setAdapter(adapter5);
                    adapter5.notifyDataSetChanged();

                    if (agents_ownrank.getText().toString().equalsIgnoreCase("-")) {
                        share_rank.setVisibility(View.GONE);
                    } else {
                        share_rank.setVisibility(View.VISIBLE);
                    }
                    share_rank.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // View to BitMap
                            File pictureFileDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "AOD");
                            if (!pictureFileDir.exists()) {
                                boolean isDirectoryCreated = pictureFileDir.mkdirs();
                                if (!isDirectoryCreated)
                                    Log.i("TAG", "Can't create directory to save the image");
                                // return null;
                            }
                            String filename = pictureFileDir.getPath() + File.separator + System.currentTimeMillis() + ".jpg";
                            File pictureFile = new File(filename);
                            Bitmap b = getScreenShot(findViewById(R.id.missionframe));
                            try {
                                pictureFile.createNewFile();
                                FileOutputStream oStream = new FileOutputStream(pictureFile);
                                b.compress(Bitmap.CompressFormat.PNG, 100, oStream);
                                oStream.flush();
                                oStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                                Log.i("TAG", "There was an issue saving the image.");
                            }


                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                            StrictMode.setVmPolicy(builder.build());
                            //BitMap to Parsable Uri (needs write permissions)
                            // String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), b,"title", null);
                            Uri bmpUri = Uri.fromFile(pictureFile);

                            //Share the image
                            Intent shareIntent = new Intent();
                            shareIntent.setAction(Intent.ACTION_SEND);
                            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                            if (bundle.getString("artis").equalsIgnoreCase("month")) {


                                shareIntent.putExtra(Intent.EXTRA_TEXT, "#aod #MonthMission" + getMonthShortName((Integer.parseInt(bundle.getString("themonth")) - 1)) + " " + AgentData.getagentname() + " " + getResources().getString(R.string.rankin_share_has) + " " + progress_black.getProgress() + " " + badgetask.getText().toString() + " #" + missionname.getText().toString().toLowerCase() + " " + getResources().getString(R.string.rank_share_place) + " " + agents_ownrank.getText().toString());
                            } else {
                                shareIntent.putExtra(Intent.EXTRA_TEXT, "#aod #WeekMission " + AgentData.getagentname() + " " + getResources().getString(R.string.rankin_share_has) + " " + progress_black.getProgress() + " " + badgetask.getText().toString() + " #" + missionname.getText().toString().toLowerCase() + " " + getResources().getString(R.string.rank_share_place) + " " + agents_ownrank.getText().toString());

                            }
                            shareIntent.setType("image/jpeg");
                            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(Intent.createChooser(shareIntent, "send"));
                        }
                    });

                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);
                } else {
                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);
                    StyleableToast.makeText(getApplicationContext(), getString(R.string.string_ranking_nodata), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
    }


    private void startrankingfriends(final Bundle bundleis) {
        // GET AGENTSDATA FOR THE MISSION
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);
        Query myTopPostsQuery = mDatabaserunnung.child(bundleis.getString("theyear")).child(bundleis.getString("artis")).child(bundleis.getString("theactionid")).orderByChild("currentvalue");
        final HashMap<String, String> data = new HashMap<String, String>();
        final List<String> listUID2 = new ArrayList();
        final List<Long> rankUID2 = new ArrayList();
        final List<Integer> positionUID2 = new ArrayList();
        final List<Long> timestamprank2 = new ArrayList();


        // RANKING FOR THE AGENT
        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            int counterloop = 0;

            @Override
            public void onDataChange(final DataSnapshot snapshot) {

                for (final DataSnapshot child : snapshot.getChildren()) {

                    if (Long.parseLong(child.child("currentvalue").getValue().toString()) >= 0) {
                        mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {

                            @Override
                            public void onDataChange(DataSnapshot snapshotfriends) {
                                counterloop++;
                                //   Log.i("SNAPP IS ", snapshotteam.child("email").getValue().toString() + "");
                                if (snapshotfriends.hasChild(child.child("uid").getValue().toString()) || child.child("uid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                    Log.i("Friends -->", "" + child.child("uid").getValue().toString());
                                    if (!child.child("uid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                        if (Boolean.parseBoolean(snapshotfriends.child(child.child("uid").getValue().toString()).child("status").getValue().toString())) {
                                            rankUID2.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                                            String test = child.child("uid").getValue().toString();
                                            timestamprank2.add(Long.parseLong(child.child("timestamp").getValue().toString()));
                                            listUID2.add(test);
                                        }

                                    } else if (child.child("uid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                        rankUID2.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                                        timestamprank2.add(Long.parseLong(child.child("timestamp").getValue().toString()));
                                        String test = child.child("uid").getValue().toString();
                                        listUID2.add(test);
                                    }

                                } else {
                                    Log.i("Notfriend -->", "" + child.child("uid").getValue().toString());
                                }

                                Log.i("WHERE-->", "c: " + counterloop + " size: " + snapshot.getChildrenCount());
                                if (counterloop == snapshot.getChildrenCount()) {

                                    Collections.reverse(listUID2);
                                    Collections.reverse(rankUID2);
                                    Collections.reverse(timestamprank2);
                                    //  mList.indexOf(user.getUid())
                                    int ownpositioninList = listUID2.indexOf(user.getUid());
                                    int ownrankis = 0;
                                    boolean hasranking = false;
                                    Log.i("OWN POS LIST ---->", "" + ownpositioninList + " sizeof--- " + listUID2.size());
                                    rank_anzahl_agents.setText("" + rankUID2.size());
                                    if (ownpositioninList == -1) {
                                        rank_own_progress.setText("-");
                                        agents_ownrank.setText("-");
                                        for (int i = 0; i < rankUID2.size(); i++) {
                                            Log.i("UID -->", "Value:" + listUID2.get(i) + "List Size " + rankUID2.size());

                                            if (i == 0) {

                                                ownrankis++;
                                                positionUID2.add(ownrankis);
                                                if (listUID2.get(i).equalsIgnoreCase(user.getUid())) {
                                                    hasranking = true;
                                                    if (rankUID2.get(i) > 0) {

                                                        rank_own_progress.setText("" + rankUID2.get(i));
                                                        Log.i("RANKING  oizhd-->", "POS: " + i + " Value:" + rankUID2.get(ownpositioninList));
                                                        agents_ownrank.setText("" + ownrankis);

                                                    } else {
                                                        if (!hasranking) {
                                                            agents_ownrank.setText("-");
                                                            rank_own_progress.setText("-");

                                                        }

                                                    }
                                                } else {
                                                    if (!hasranking) {
                                                        agents_ownrank.setText("-");
                                                        rank_own_progress.setText("0");

                                                    }
                                                }
                                            } else {

                                                ownrankis++;
                                                positionUID2.add(ownrankis);
                                                if (listUID2.get(i).equalsIgnoreCase(user.getUid())) {
                                                    hasranking = true;
                                                    Log.i("RANKING c -->", "POS: " + i + " Value:" + rankUID2.get(ownpositioninList));
                                                    if (rankUID2.get(i) > 0) {

                                                        Log.i("RANKING 555 -->", "POS: " + i + " Value:" + rankUID2.get(ownpositioninList));
                                                        agents_ownrank.setText("" + ownrankis);
                                                        rank_own_progress.setText("" + rankUID2.get(i));
                                                    } else {
                                                        if (!hasranking) {
                                                            agents_ownrank.setText("-");
                                                            rank_own_progress.setText("-");

                                                        }
                                                    }
                                                } else {
                                                    if (!hasranking) {
                                                        agents_ownrank.setText("-");
                                                        rank_own_progress.setText("0");

                                                    }
                                                }


                                            }
                                        }
                                        //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                                        if (ownrankis <= 0) {

                                            agents_ownrank.setText("-");
                                        }

                                    } else {
                                        for (int i = 0; i < rankUID2.size(); i++) {
                                            //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                                            if (i == 0) {

                                                ownrankis++;
                                                positionUID2.add(ownrankis);
                                                if (listUID2.get(i).equalsIgnoreCase(user.getUid())) {
                                                    hasranking = true;
                                                    if (rankUID2.get(i) > 0) {

                                                        rank_own_progress.setText("" + rankUID2.get(i));
                                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                        agents_ownrank.setText("" + ownrankis);

                                                    } else {
                                                        if (!hasranking) {
                                                            agents_ownrank.setText("-");
                                                            rank_own_progress.setText("-");

                                                        }

                                                    }
                                                } else {
                                                    if (!hasranking) {
                                                        agents_ownrank.setText("-");
                                                        rank_own_progress.setText("0");

                                                    }
                                                }
                                            } else {
                                                if (rankUID2.get(i) > rankUID2.get(ownpositioninList)) {
                                                    ownrankis++;
                                                    positionUID2.add(ownrankis);
                                                    if (listUID2.get(i).equalsIgnoreCase(user.getUid())) {
                                                        hasranking = true;
                                                        Log.i("RANKING tt-->", "POS: " + i + " Value:" + rankUID2.get(ownpositioninList));
                                                        if (rankUID2.get(i) > 0) {

                                                            rank_own_progress.setText("" + rankUID2.get(i));
                                                            //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                            agents_ownrank.setText("" + ownrankis);
                                                        } else {
                                                            if (!hasranking) {
                                                                agents_ownrank.setText("-");
                                                                rank_own_progress.setText("-");

                                                            }
                                                        }
                                                    } else {
                                                        if (!hasranking) {
                                                            agents_ownrank.setText("-");
                                                            rank_own_progress.setText("0");

                                                        }
                                                    }
                                                } else {
                                                    ownrankis++;
                                                    positionUID2.add(ownrankis);
                                                    if (listUID2.get(i).equalsIgnoreCase(user.getUid())) {
                                                        hasranking = true;
                                                        Log.i("RANKING ggg-->", "POS: " + i + " Value:" + rankUID2.get(ownpositioninList));
                                                        if (rankUID2.get(i) > 0) {

                                                            //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                                            agents_ownrank.setText("" + ownrankis);
                                                            rank_own_progress.setText("" + rankUID2.get(i));
                                                        } else {
                                                            if (!hasranking) {
                                                                agents_ownrank.setText("-");
                                                                rank_own_progress.setText("-");

                                                            }
                                                        }
                                                    } else {
                                                        if (!hasranking) {
                                                            agents_ownrank.setText("-");
                                                            rank_own_progress.setText("0");

                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                                        if (ownrankis <= 0) {

                                            agents_ownrank.setText("-");
                                        }

                                    }


                                    if (positionUID2.size() > 0) {
                                        RankingAdapterNew adapter5 = new RankingAdapterNew(RankingMain.this, listUID2, rankUID2, positionUID2, bundle.getString("theyear"), bundle.getString("themonth"), timestamprank2, ownpositioninList, bundleis.getString("artis"));
                                        rankinglist.setAdapter(adapter5);
                                        adapter5.notifyDataSetChanged();


                                        share_rank.setVisibility(View.GONE);


                                        coordinator.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);
                                    } else {
                                        coordinator.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);
                                        StyleableToast.makeText(getApplicationContext(), getString(R.string.string_ranking_nodata), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {


                            }
                        });

                    }


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
    }


    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }


}