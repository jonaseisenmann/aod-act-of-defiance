package rocks.prxenon.aod.pages;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.ShareCompat;
import androidx.core.math.MathUtils;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.akexorcist.roundcornerprogressbar.TextRoundCornerProgressBar;
import com.chauthai.swipereveallayout.SwipeRevealLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.google.firebase.perf.metrics.AddTrace;
import com.marcoscg.xmassnow.XmasSnow;
import com.muddzdev.styleabletoast.StyleableToast;
import com.nightonke.boommenu.Animation.BoomEnum;
import com.nightonke.boommenu.Animation.EaseEnum;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Util;
import com.pixplicity.generate.OnFeedbackListener;
import com.pixplicity.generate.Rate;
import com.ramotion.foldingcell.FoldingCell;
import com.robinhood.ticker.TickerUtils;
import com.robinhood.ticker.TickerView;
import com.wang.avi.AVLoadingIndicatorView;

import org.apmem.tools.layouts.FlowLayout;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;
import rocks.prxenon.aod.BuildConfig;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.HistoryUpdates;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.basics.Fragment_ChangeLog;
import rocks.prxenon.aod.billing.Donation;
import rocks.prxenon.aod.events.NIAEvents;
import rocks.prxenon.aod.helper.GetNotifications;
import rocks.prxenon.aod.helper.OverlayScreenService;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.helper.ServiceCallBack;
import rocks.prxenon.aod.pages.hackvent.HackventMain;
import rocks.prxenon.aod.pages.history.EarthHistory;
import rocks.prxenon.aod.pages.history.RankingHistory;
import rocks.prxenon.aod.pages.profile.ProfileMain;
import rocks.prxenon.aod.pages.pvp.PvPCreate;
import rocks.prxenon.aod.pages.pvp.PvPDetails;
import rocks.prxenon.aod.pages.ranking.RankingMain;
import rocks.prxenon.aod.settings.MainSettings;
import rocks.prxenon.aod.signin.FirebaseUIActivity;
import rocks.prxenon.aod.slice.MySliceProvider;

import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;
import static rocks.prxenon.aod.basics.AODConfig.database;
import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseagentCodes;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;
import static rocks.prxenon.aod.basics.AODConfig.mDatabasemissions;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnung;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnungpvp;
import static rocks.prxenon.aod.basics.AODConfig.mbfriendsrequest;
import static rocks.prxenon.aod.basics.AODConfig.mbgdpr;
import static rocks.prxenon.aod.basics.AODConfig.mbpvp;
import static rocks.prxenon.aod.basics.AODConfig.mbpvplinks;
import static rocks.prxenon.aod.basics.AODConfig.mdbroot;
import static rocks.prxenon.aod.basics.AODConfig.mhackvent;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.qrdatabase;
import static rocks.prxenon.aod.basics.AODConfig.user;
import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;
import static rocks.prxenon.aod.helper.GetNotifications.newagents;
import static rocks.prxenon.aod.helper.GetNotifications.newinbox;


public class AODMain extends AppCompatActivity implements SearchView.OnClickListener, Animation.AnimationListener, ServiceCallBack {

    private static final int REQUEST_OVERLAY = 2;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    // END SLICE
    private static final int NUM_PAGES = 5;
    private static final String API_KEY = BuildConfig.GOOGLE_VERIFICATION_API_KEY;
    // TEST SLICE
    public static int sTemperature = 16; // Celcius
    public static String monthetextslice = ""; // Celcius
    public static AlertDialog.Builder historybuilder;
    private static final String[] PERMISSIONS_OVERLAY = {
            Manifest.permission.SYSTEM_ALERT_WINDOW
    };
    private static final String[] PERMISSIONS_CLIPBOARD = {
            // Manifest.permission.READ_CLIPBOARD_IN_BACKGROUND
    };
    private static final String[] PERMISSIONS_CAM = {android.Manifest.permission.CAMERA};
    // public DatabaseReference mDatabase;
    private static final String[] PERMISSIONS_LOC = {Manifest.permission.ACCESS_COARSE_LOCATION};
    private final Random mRandom = new SecureRandom();
    public HistoryUpdates historyadapter;
    public boolean hasdialogbefore;
    public ImageView notify_count_image;
    public TextView notify_count_text;
    OverlayScreenService mService;
    boolean mBound = false;
    String themonthx, theweekx;
    String theyearx;
    float initialX, initialY;
    AlertDialog closedialog = null;
    private SharedPreferences mainprefssettings;
    private ProgressBar progressBar;
    private Animation animZoomIn, animZoomNull;
    private Typeface type, proto;
    private BoomMenuButton bmb_main;
    private ImageButton bmb_filter;
    private String[] menuitems, menusettingsone, menusettingstwo, menuitems_map;
    private TypedArray menuitems_drawble, menusettings_drawable, menuitems_drawble_map, badgemonth;
    private HamButton.Builder builder2, builder_filter;
    private TextView mTitleTextView;
    private TextView agentname;
    private CircleImageView agentimg;
    private AlertDialog.Builder dialogBuilder;
    private boolean cansave;
    private DataSnapshot snap;
    private ValueEventListener savewatch;
    private HashMap markerMap;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private CircleImageView imgagent;
    private FancyButton donation_store, content_request_btn_month, content_request_btn_week, content_request_btn_month_manuell, content_request_btn_week_manuell, content_request_btn_earth_manuell, content_request_btn_earth, content_request_btn_hackvent, content_request_btn_mini;
    private FoldingCell fcweek, fcmonth, fcearth, fchackvent, fcmini;
    private RelativeLayout frameloadmonth, monthstartet, monthnotstartet, frameloadweek, weekstartet, weeknotstartet, frameloadearth, earthstartet, earthnotstartet, frameloadhackvent, frameloadmini, hackventstartet, hackventnotstartet, ministartet, mininotstartet;
    private LinearLayout framecardmonth, framecardweek, communityframe, communityframe_week, pvp_frame_base, framecardearth, communityframe_earth, framecardhackvent, framecardmini, communityframe_hackvent, communityframe_mini;
    private FancyButton join_btn_week, join_btn_month, create_pvp, join_btn_earth, info_btn_earth, join_btn_hackvent, info_btn_hackvent, join_btn_mini;
    private String monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield, monthbadgedrawable, monthbadgedrawablegold, monthbadgedrawableplatin, monthbadgeid;
    private String weekbadgetitle, weekbadgeshort1, weekbadgeshort2, weekbadgefield, weekbadgedrawable, weekbadgedrawablegold, weekbadgedrawableplatin, weekbadgeid, minibadgedrawable, minibadgedrawablegold, minibadgedrawableplatin;

    private String minibadgetitle, minibadgeshort1, minibadgeshort2, minibadgefield, minibadgeid;
    private String earthbadgetitle, earthbadgeshort1, earthbadgeshort2, earthbadgefield, earthbadgedrawable, earthbadgedrawablegold, earthbadgedrawableplatin, earthbadgeid;
    private String cmonthmissionid, cweekmissionid, cearthmissionid, cminimissionid;
    private CountDownTimer timer_month, timer_week, timer_pvp_one, timer_pvp_two, timer_pvp_three, timer_pvp_one_finish, timer_pvp_two_finish, timer_pvp_three_finish;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private SwipeRevealLayout swipe_layout_community, swipe_layout_community_week, swipe_layout_community_earth, swipe_layout_community_hackvent, swipe_layout_community_mini;
    private ImageView iscommunitymission, close_btn_community, iscommunitymission_week, iscommunitymission_mini, close_btn_community_week, iscommunitymission_earth, close_btn_community_earth, close_btn_community_mini;
    private final String TAG = AODMain.class.getSimpleName();
    private ListView lv;
    private int themonthgoal, themonthgoalplatin, themonthgoalblack, theearthgoal, theearthgoalplatin, theearthgoalblack, getThemonthgoalcommunity, theweekgoal, theweekgoalplatin, theweekgoalblack, themonthgoalcom, themonthgoalplatincom, themonthgoalblackcom, theweekgoalcom, theweekgoalplatincom, theweekgoalblackcom, theearthgoalcom, theearthgoalplatincom, theearthgoalblackcom, theearthgoalniacom;
    private SwipeRevealLayout swipe_team_month, swipe_team_week, swipe_team_earth, swipe_team_hackvent, swipe_team_mini;
    private LinearLayout monthview_base, weekview_base, earthview_base, hackventview_base, miniventview_base;
    private ConstraintLayout baseframe;
    private ViewParent parent;
    private String lastVersion, thisVersion;
    private String workaround_actionid, workaround_month, workaround_earth, workaround_week, workaround_actionid_week, workaroundpvptit;
    private TickerView tickerView;
    private FrameLayout updateHolder;
    private int goalgold, goalplatin, goalblack, goalgold_week, goalplatin_week, goalblack_week, goalgold_earth, goalplatin_earth, goalblack_earth;
    private RelativeLayout action_ranking, action_ranking_week, action_ranking_mini, action_ranking_earth, action_ranking_hackvent;
    private int mSlop;
    private float mDownX;
    private float mDownY;
    private boolean mSwiping;
    private KonfettiView viewKonfetti;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ValueEventListener listenerpvpjoin;
    private int badgemonthidscreener = 0;
    private int badgeweekidscreener = 0;
    private int badgeearthidscreener = 0;
    private int badgeminiidscreener = 0;
    private PackageInfo packageInfo;
    private boolean primeisinstalled, min_ingressversion, redacted_allowed, multimission, monthrunning, weekrunning;
    private RelativeLayout getgoalmonth, getgoalweek;
    private Timer timerhackvent;
    private TimerTask timerTaskhackvent;
    private Rate mRateDialogDark, mRateDialogLight, mRateBarDark, mRateBarLight;
    private String mResult;
    private List<String> listdebug;
    private String statsart;

    private String stattab;

    // MAP MENUS
    private final ServiceConnection connectionService = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(" VEG3", "onServiceConnected");
            OverlayScreenService.LocalBinder scan = (OverlayScreenService.LocalBinder) service;
            mService = scan.getService();
            mService.setServiceCallBack(AODMain.this);
            mService.setCallbacks(AODMain.this); // register
            mService.startIngress();
            mBound = true;


        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

            Log.d("TAG", "onServiceDisconnected");
            mBound = false;
        }
    };

    @TargetApi(Build.VERSION_CODES.M)
    public static void verifyOverlay(Activity activity) {
        // Check if we have write permission
        if (!Settings.canDrawOverlays(activity)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + activity.getPackageName()));
            activity.startActivityForResult(intent, REQUEST_OVERLAY);
        }
    }

    public static String getMonthShortName(int monthNumber) {
        String monthName = "";

        if (monthNumber >= 0 && monthNumber < 12)
            try {
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.MONTH, monthNumber);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM");
                //simpleDateFormat.setCalendar(calendar);
                monthName = simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                if (e != null)
                    e.printStackTrace();
            }
        return monthName;
    }

    public static String getMonthShorterName(int monthNumber) {
        String monthName = "";

        if (monthNumber >= 0 && monthNumber < 12)
            try {
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.MONTH, monthNumber);
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
                //simpleDateFormat.setCalendar(calendar);
                monthName = simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                if (e != null)
                    e.printStackTrace();
            }
        return monthName;
    }

    public static String getTemperatureString(@NonNull Context context) {
        return context.getString(R.string.temp_string, sTemperature);
    }

    public static String getMonthText(@NonNull Context context) {

        return monthetextslice;
    }


    // JOIN A MISSION

    public static void updateTemperature(Context context, int newValue) {
        newValue = MathUtils.clamp(newValue, 10, 30); // Lets keep temperatures reasonable

        if (newValue != sTemperature) {
            sTemperature = newValue;

            // Should notify the URI to let any slices that might be displaying know to update.
            Uri uri = MySliceProvider.getUri(context, "monthmission");
            context.getContentResolver().notifyChange(uri, null);
        }
    }

    public static Bitmap convertToGrayscale(Bitmap bmpOriginal) {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        Log.i("Gray done ", "true");
        return bmpGrayscale;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_aod);
        new AODConfig("default", this);
        //Get Firebase auth instance
        new AODConfig();

        listdebug = new ArrayList();
        pref = getSharedPreferences("AppPref", MODE_PRIVATE);

        mainprefssettings = PreferenceManager.getDefaultSharedPreferences(AODMain.this);

        lastVersion = pref.getString("PREFS_VERSION_KEY", "");
        try {
            this.thisVersion = getPackageManager().getPackageInfo(getPackageName(),
                    0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            this.thisVersion = "";
            // Log.e(TAG, "could not get version name from manifest!");
            e.printStackTrace();
        }

        String nonceData = "Safety Net Sample: " + System.currentTimeMillis();
        byte[] nonce = getRequestNonce(nonceData);

      /*  SafetyNet.getClient(this).attest(nonce, API_KEY)
                .addOnSuccessListener(this,
                        new OnSuccessListener<SafetyNetApi.AttestationResponse>() {
                            @Override
                            public void onSuccess(SafetyNetApi.AttestationResponse response) {
                                // Indicates communication with the service was successful.
                                // Use response.getJwsResult() to get the result data.

                                mResult = response.getJwsResult();
                                Log.d(TAG, "Success! SafetyNet result:\n" + mResult + "\n");

                                SafetyNet.getClient(AODMain.this)
                                        .enableVerifyApps()
                                        .addOnCompleteListener(new OnCompleteListener<SafetyNetApi.VerifyAppsUserResponse>() {
                                            @Override
                                            public void onComplete(Task<SafetyNetApi.VerifyAppsUserResponse> task) {
                                                if (task.isSuccessful()) {
                                                    SafetyNetApi.VerifyAppsUserResponse result = task.getResult();
                                                    if (result.isVerifyAppsEnabled()) {
                                                        Log.d("MY_APP_TAG", "The user gave consent " +
                                                                "to enable the Verify Apps feature.");

                                                        SafetyNet.getClient(AODMain.this)
                                                                .listHarmfulApps()

                                                                .addOnCompleteListener(new OnCompleteListener<SafetyNetApi.HarmfulAppsResponse>() {
                                                                    @Override
                                                                    public void onComplete(Task<SafetyNetApi.HarmfulAppsResponse> task) {
                                                                        Log.d(TAG, "Received listHarmfulApps() result");

                                                                        if (task.isSuccessful()) {
                                                                            SafetyNetApi.HarmfulAppsResponse result = task.getResult();
                                                                            long scanTimeMs = result.getLastScanTimeMs();

                                                                            List<HarmfulAppsData> appList = result.getHarmfulAppsList();
                                                                            if (appList.isEmpty()) {
                                                                                Log.d("MY_APP_TAG", "There are no known " +
                                                                                        "potentially harmful apps installed.");

                                                                                File f = new File(Environment.getExternalStorageDirectory() + "/TWRP");
                                                                                if(f.exists() && f.isDirectory()) {
                                                                                    Log.d("MY_APP_TAG", "TWRP" +
                                                                                            "found");
                                                                                }
                                                                            } else {
                                                                                Log.e("MY_APP_TAG",
                                                                                        "Potentially harmful apps are installed!");

                                                                                for (HarmfulAppsData harmfulApp : appList) {
                                                                                    Log.e("MY_APP_TAG", "Information about a harmful app:");
                                                                                    Log.e("MY_APP_TAG",
                                                                                            "  APK: " + harmfulApp.apkPackageName);
                                                                                    Log.e("MY_APP_TAG",
                                                                                            "  SHA-256: " + harmfulApp.apkSha256);

                                                                                    // Categories are defined in VerifyAppsConstants.
                                                                                    Log.e("MY_APP_TAG",
                                                                                            "  Category: " + harmfulApp.apkCategory);
                                                                                }
                                                                            }
                                                                        } else {
                                                                            Log.d("MY_APP_TAG", "An error occurred. " +
                                                                                    "Call isVerifyAppsEnabled() to ensure " +
                                                                                    "that the user has consented.");
                                                                        }
                                                                    }
                                                                });
                                                    } else {
                                                        Log.d("MY_APP_TAG", "The user didn't give consent " +
                                                                "to enable the Verify Apps feature.");
                                                    }
                                                } else {
                                                    Log.e("MY_APP_TAG", "A general error occurred.");
                                                }
                                            }
                                        });
                            }
                        })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // An error occurred while communicating with the service.
                        if (e instanceof ApiException) {
                            // An error with the Google Play services API contains some
                            // additional details.
                            ApiException apiException = (ApiException) e;
                            // You can retrieve the status code using the
                            // apiException.getStatusCode() method.
                        } else {
                            // A different, unknown type of error occurred.
                            Log.d(TAG, "Error: " + e.getMessage());
                        }
                    }
                });


       */

        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");

        updateHolder = findViewById(R.id.updateHolder);
        mySwipeRefreshLayout = findViewById(R.id.swiperefresh);

        mySwipeRefreshLayout.setRefreshing(false);

        mySwipeRefreshLayout.setProgressBackgroundColorSchemeColor(getApplicationContext().getResources().getColor(R.color.colorAccent, getTheme()));
        mySwipeRefreshLayout.setColorSchemeColors(getApplicationContext().getResources().getColor(R.color.colorWhite, getTheme()), getApplicationContext().getResources().getColor(R.color.colorPrimaryYellowLight, getTheme()), getApplicationContext().getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mySwipeRefreshLayout.setRefreshing(true);
                        if (listenerpvpjoin != null) {
                            mbpvp.removeEventListener(listenerpvpjoin);
                            listenerpvpjoin = null;
                        }
                        initmissions();


                    }
                }
        );

        create_pvp = findViewById(R.id.create_pvp);
        create_pvp.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        baseframe = findViewById(R.id.baseframe);
        monthview_base = findViewById(R.id.monthview_base);
        hackventview_base = findViewById(R.id.hackventview_base);
        weekview_base = findViewById(R.id.weekview_base);
        earthview_base = findViewById(R.id.earthview_base);
        tickerView = findViewById(R.id.tickerView);
        tickerView.setTypeface(proto);

        mRateBarDark = createSnackBar(baseframe, false);
        mRateBarLight = createSnackBar(baseframe, true);

       /* Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg_pattern_light);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        baseframe.setBackgroundDrawable(bitmapDrawable); */

        getgoalmonth = findViewById(R.id.getgoalmonth);
        getgoalweek = findViewById(R.id.getgoalweek);

        getgoalweek.setOnClickListener(this);
        getgoalmonth.setOnClickListener(this);
        viewKonfetti = findViewById(R.id.viewKonfetti);


        if (mDatabase == null) {
            if (database == null) {
                database = FirebaseDatabase.getInstance();
                database.setPersistenceEnabled(true);
                database.goOnline();
            }
            mDatabase = database.getReference("users");
            mDatabasemissions = database.getReference("missions");
            mdbroot = database.getReference("root");
            mDatabaseaod = database.getReference("aod");
            mDatabaserunnung = database.getReference("running");
            mDatabaseagentCodes = database.getReference("agentcodes");
            qrdatabase = database.getReference("tempqr");
            mbfriendsrequest = database.getReference("friendrequests");
            mbgdpr = database.getReference("gdpr");
        }


        if (user == null) {
            user = FirebaseAuth.getInstance().getCurrentUser();
        }
        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (!snapshot.child("privacy").exists()) {
                    final Map<String, Object> privUpdates = new HashMap<>();

                    privUpdates.put("/privacy", 0);

                    mDatabase.child(user.getUid()).updateChildren(privUpdates);

                    SharedPreferences prefssettings = PreferenceManager.getDefaultSharedPreferences(AODMain.this);
                    prefssettings.edit().putString("profile_visible", "0").apply();
                    new AgentData("privacystatus", 0);
                } else {

                    new AgentData("privacystatus", Integer.parseInt(snapshot.child("privacy").getValue().toString()));
                }


                if (!snapshot.child("pvpcleaned").exists()) {
                    final Map<String, Object> privUpdates2 = new HashMap<>();

                    privUpdates2.put("/pvpcleaned", true);

                    mDatabase.child(user.getUid()).updateChildren(privUpdates2);

                    mDatabase.child(user.getUid()).child("pvp").setValue(null);


                }

                final Map<String, Object> privUpdates3 = new HashMap<>();

                privUpdates3.put("/appversion", "" + AgentData.getappversion("appversion"));

                mDatabase.child(user.getUid()).updateChildren(privUpdates3);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //  StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_sometningwrong), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                //  Log.i("Error DB -->", this.getClass().getSimpleName() + " 407 " + databaseError.getMessage().toString());
            }
        });
        /*
        monthview_base.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getActionMasked();
                int x = (int) event.getX();
                int y = (int) event.getY();
                Log.i("TOUCH VIEW -->", "" + v.getId() + " na:" + v.toString());
                switch (action) {

                    case MotionEvent.ACTION_DOWN:
                        Log.d(TAG, "Action was DOWN");

                        break;


                    case MotionEvent.ACTION_CANCEL:
                        Log.d(TAG, "Action was CANCEL");
                        if (fcmonth.isUnfolded()) {
                            fcmonth.fold(false);
                        }

                        break;


                }

                return true;
            }
        });

          */

        ShortcutManager shortcutManager = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
            shortcutManager = getSystemService(ShortcutManager.class);
            ShortcutInfo shortcut = new ShortcutInfo.Builder(getApplicationContext(), "qrshow")
                    .setShortLabel(getResources().getString(R.string.compose_shortcut_short_label1))
                    .setLongLabel(getResources().getString(R.string.compose_shortcut_long_label1))
                    .setIcon(Icon.createWithResource(getApplicationContext(), R.mipmap.ic_launcher_qr))

                    .setIntent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.openqrfromAction").setClass(this, FirebaseUIActivity.class))
                    .build();

            ShortcutInfo shortcut2 = new ShortcutInfo.Builder(getApplicationContext(), "qrscan")
                    .setShortLabel(getResources().getString(R.string.compose_shortcut_short_label2))
                    .setLongLabel(getResources().getString(R.string.compose_shortcut_long_label2))
                    .setIcon(Icon.createWithResource(getApplicationContext(), R.mipmap.ic_launcher_qr_scan))

                    .setIntent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.scanqrfromAction").setClass(this, FirebaseUIActivity.class))
                    .build();

            ShortcutInfo shortcut3 = new ShortcutInfo.Builder(getApplicationContext(), "events")
                    .setShortLabel(getResources().getString(R.string.compose_shortcut_short_label3))
                    .setLongLabel(getResources().getString(R.string.compose_shortcut_long_label3))
                    .setIcon(Icon.createWithResource(getApplicationContext(), R.mipmap.ic_launcher))

                    .setIntent(new Intent("rocks.prxenon.aod.events.profile.NIAEvents.startfromhome").setClass(this, FirebaseUIActivity.class))
                    .build();
            shortcutManager.setDynamicShortcuts(Arrays.asList(shortcut, shortcut2, shortcut3));
         /*   shortcutManager.setDynamicShortcuts(Arrays.asList(shortcut));

            shortcutManager = getSystemService(ShortcutManager.class);
            ShortcutInfo shortcut = shortcutManager.getManifestShortcuts().get(0);
            shortcutManager.disableShortcuts(Arrays.asList(shortcut.getId()));

            ShortcutInfo shortcut2 = shortcutManager.getManifestShortcuts().get(1);
            shortcutManager.disableShortcuts(Arrays.asList(shortcut2.getId())); */

        }


        parent = baseframe.getParent();
        action_ranking = findViewById(R.id.action_ranking);
        action_ranking.setOnClickListener(this);
        action_ranking_week = findViewById(R.id.action_ranking_week);
        action_ranking_week.setOnClickListener(this);
        action_ranking_mini = findViewById(R.id.action_ranking_mini);
        action_ranking_mini.setOnClickListener(this);
        action_ranking_earth = findViewById(R.id.action_ranking_earth);
        action_ranking_earth.setOnClickListener(this);
        action_ranking_hackvent = findViewById(R.id.action_ranking_hackvent);
        action_ranking_hackvent.setOnClickListener(this);
        frameloadhackvent = findViewById(R.id.frameloadhackvent);
        frameloadmini = findViewById(R.id.frameloadmini);
        frameloadmonth = findViewById(R.id.frameloadmonth);
        framecardmonth = findViewById(R.id.framecardmonth);
        frameloadearth = findViewById(R.id.frameloadearth);
        framecardearth = findViewById(R.id.framecardearth);
        framecardmini = findViewById(R.id.framecardmini);

        frameloadhackvent = findViewById(R.id.frameloadhackvent);
        framecardhackvent = findViewById(R.id.framecardhackvent);
        monthstartet = findViewById(R.id.monthstartet);
        monthnotstartet = findViewById(R.id.monthnotstartet);
        earthstartet = findViewById(R.id.earthstartet);
        earthnotstartet = findViewById(R.id.earthnotstartet);

        hackventstartet = findViewById(R.id.hackventstartet);
        hackventnotstartet = findViewById(R.id.hackventnotstartet);

        pvp_frame_base = findViewById(R.id.pvp_frame_base);
        frameloadweek = findViewById(R.id.frameloadweek);
        framecardweek = findViewById(R.id.framecardweek);
        weekstartet = findViewById(R.id.weekstartet);
        weeknotstartet = findViewById(R.id.weeknotstartet);

        ministartet = findViewById(R.id.ministartet);
        mininotstartet = findViewById(R.id.mininotstartet);
        /* Listeners */
        communityframe = findViewById(R.id.communityframe);
        communityframe_week = findViewById(R.id.communityframe_week);
        communityframe_earth = findViewById(R.id.communityframe_earth);
        communityframe_hackvent = findViewById(R.id.communityframe_hackvent);
        communityframe_mini = findViewById(R.id.communityframe_mini);
        swipe_team_month = findViewById(R.id.swipe_layout_2_month);
        swipe_team_earth = findViewById(R.id.swipe_layout_2_earth);
        swipe_team_hackvent = findViewById(R.id.swipe_layout_2_hackvent);
        swipe_team_week = findViewById(R.id.swipe_layout_2_week);
        swipe_team_mini = findViewById(R.id.swipe_layout_2_mini);
        // user = new API("default", this).user();
        // database = new API("default", this).database();
        // mDatabase = new API("default", this).mDatabase();
        donation_store = findViewById(R.id.donation_store);
        donation_store.setOnClickListener(this);
        content_request_btn_month = findViewById(R.id.content_request_btn_month);
        content_request_btn_mini = findViewById(R.id.content_request_btn_mini);
        content_request_btn_month_manuell = findViewById(R.id.content_request_btn_month_manuell);
        content_request_btn_earth = findViewById(R.id.content_request_btn_earth);
        content_request_btn_earth_manuell = findViewById(R.id.content_request_btn_earth_manuell);

        content_request_btn_hackvent = findViewById(R.id.content_request_btn_hackvent);


        content_request_btn_week = findViewById(R.id.content_request_btn_week);
        content_request_btn_week_manuell = findViewById(R.id.content_request_btn_week_manuell);
        iscommunitymission = findViewById(R.id.is_communitymission);
        iscommunitymission.setOnClickListener(this);
        iscommunitymission_week = findViewById(R.id.is_communitymission_week);
        iscommunitymission_week.setOnClickListener(this);

        iscommunitymission_mini = findViewById(R.id.is_communitymission_mini);
        iscommunitymission_mini.setVisibility(View.GONE);
        iscommunitymission_mini.setOnClickListener(this);
        content_request_btn_mini.setOnClickListener(this);
        //iscommunitymission_hackvent = findViewById(R.id.is_communitymission_hackvent);

        //  iscommunitymission_hackvent.setOnClickListener(this);

        iscommunitymission_earth = findViewById(R.id.is_communitymission_earth);

        iscommunitymission_earth.setOnClickListener(this);
        close_btn_community = findViewById(R.id.close_btn_community);
        close_btn_community.setOnClickListener(this);
        close_btn_community_week = findViewById(R.id.close_btn_community_week);
        close_btn_community_week.setOnClickListener(this);
        close_btn_community_earth = findViewById(R.id.close_btn_community_earth);
        close_btn_community_earth.setOnClickListener(this);

        //close_btn_community_hackvent = findViewById(R.id.close_btn_community_hackvent);
        //close_btn_community_hackvent.setOnClickListener(this);

        content_request_btn_month_manuell.setOnClickListener(this);
        content_request_btn_month.setOnClickListener(this);
        content_request_btn_earth_manuell.setOnClickListener(this);
        content_request_btn_earth.setOnClickListener(this);
        content_request_btn_week.setOnClickListener(this);
        content_request_btn_week_manuell.setOnClickListener(this);
        agentname = findViewById(R.id.agentname);
        agentimg = findViewById(R.id.agentimage);
        agentname.setTypeface(proto);


        agentname.setText(AgentData.getagentname());
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(R.string.app_name);
        notify_count_image = actionBar.findViewById(R.id.notify_count_image);
        notify_count_text = actionBar.findViewById(R.id.notify_count_text);

        notify_count_image.setOnClickListener(this);
        // notify_count_text.setVisibility(View.GONE);
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        fcweek = findViewById(R.id.folding_cell_week);
        fcmonth = findViewById(R.id.folding_cell_month);
        fcearth = findViewById(R.id.folding_cell_earth);

        fchackvent = findViewById(R.id.folding_cell_hackvent);

        fcmini = findViewById(R.id.folding_cell_mini);
        //fcmini.setVisibility(View.GONE);
        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);
        // bmb_filter = (ImageButton) actionBar.findViewById(R.id.bmb_filter);
        //bmb_settings = (BoomMenuButton) actionBar.findViewById(R.id.bmb_settings);


        //bmb_filter.setOnClickListener(this);


        progressBar = findViewById(R.id.progressBar);

        // bgani = (ImageView) findViewById(R.id.bgani);

        Log.i("ANDROID--->", "" + Build.VERSION.RELEASE);
        // load the animation
        animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom_in);

        animZoomNull = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.zoom_null);

        // set animation listener
        animZoomIn.setAnimationListener(this);
        animZoomNull.setAnimationListener(this);

        //bgani.startAnimation(animZoomIn);
        // button click event
        join_btn_week = findViewById(R.id.title_start_btn_week);
        join_btn_month = findViewById(R.id.title_start_btn_month);
        join_btn_earth = findViewById(R.id.title_start_btn_earth);
        info_btn_earth = findViewById(R.id.title_info_btn_earth);
        join_btn_hackvent = findViewById(R.id.title_start_btn_hackvent);
        join_btn_mini = findViewById(R.id.title_start_btn_mini);

        swipe_layout_community = findViewById(R.id.swipe_layout_community);
        swipe_layout_community_week = findViewById(R.id.swipe_layout_community_week);
        swipe_layout_community_earth = findViewById(R.id.swipe_layout_community_earth);
        swipe_layout_community_hackvent = findViewById(R.id.swipe_layout_community_hackvent);
        swipe_layout_community_mini = findViewById(R.id.swipe_layout_community_mini);
        swipe_layout_community_mini.setLockDrag(true);
        findViewById(R.id.month_expanded_header).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getActionMasked();
                int x = (int) event.getX();
                int y = (int) event.getY();
                Log.i("TOUCH VIEW -->", "" + v.getId() + " na:" + v.toString());
                if (fcmonth.isUnfolded()) {
                    fcmonth.fold(false);
                }


                return true;
            }
        });

        // TODO PROOF THIS CONTENT


       /* // Line 941 set to false and aktivate here
        swipe_layout_community_mini.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {

                communityframe_mini.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                communityframe_mini.setVisibility(View.GONE);
                                swipe_team_mini.setAlpha(1f);
                                swipe_team_mini.setVisibility(View.VISIBLE);
                            }
                        });
                // communityframe.setVisibility(View.GONE);


                ViewGroup.LayoutParams params = fcmini.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcmini.setLayoutParams(params);

                // fcmonth.requestLayout();
            }

            @Override
            public void onOpened(SwipeRevealLayout view) {


                swipe_team_mini.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                swipe_team_mini.setVisibility(View.GONE);
                                communityframe_mini.setAlpha(1f);
                                communityframe_mini.setVisibility(View.VISIBLE);
                            }
                        });
                //  swipe_team_month.setVisibility(View.GONE);
                //  communityframe.setVisibility(View.VISIBLE);

                ViewGroup.LayoutParams params = fcmini.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcmini.setLayoutParams(params);
                // fcmonth.requestLayout();
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {
                ViewGroup.LayoutParams params = fcmini.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcmini.setLayoutParams(params);
                fcmini.requestLayout();
            }
        });

        */

        swipe_layout_community.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {

                communityframe.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                communityframe.setVisibility(View.GONE);
                                swipe_team_month.setAlpha(1f);
                                swipe_team_month.setVisibility(View.VISIBLE);
                            }
                        });
                // communityframe.setVisibility(View.GONE);


                ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcmonth.setLayoutParams(params);

                // fcmonth.requestLayout();
            }

            @Override
            public void onOpened(SwipeRevealLayout view) {


                swipe_team_month.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                swipe_team_month.setVisibility(View.GONE);
                                communityframe.setAlpha(1f);
                                communityframe.setVisibility(View.VISIBLE);
                            }
                        });
                //  swipe_team_month.setVisibility(View.GONE);
                //  communityframe.setVisibility(View.VISIBLE);

                ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcmonth.setLayoutParams(params);
                // fcmonth.requestLayout();
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {
                ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcmonth.setLayoutParams(params);
                fcmonth.requestLayout();
            }
        });


        swipe_layout_community_earth.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {

                communityframe_earth.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                communityframe_earth.setVisibility(View.GONE);
                                swipe_team_earth.setAlpha(1f);
                                swipe_team_earth.setVisibility(View.VISIBLE);
                            }
                        });
                // communityframe.setVisibility(View.GONE);


                ViewGroup.LayoutParams params = fcearth.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcearth.setLayoutParams(params);

                // fcmonth.requestLayout();
            }

            @Override
            public void onOpened(SwipeRevealLayout view) {


                swipe_team_earth.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                swipe_team_earth.setVisibility(View.GONE);
                                communityframe_earth.setAlpha(1f);
                                communityframe_earth.setVisibility(View.VISIBLE);
                            }
                        });
                //  swipe_team_month.setVisibility(View.GONE);
                //  communityframe.setVisibility(View.VISIBLE);

                ViewGroup.LayoutParams params = fcearth.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcearth.setLayoutParams(params);
                // fcmonth.requestLayout();
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {
                ViewGroup.LayoutParams params = fcearth.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcearth.setLayoutParams(params);
                fcearth.requestLayout();
            }
        });


        swipe_layout_community_hackvent.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {

                communityframe_hackvent.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                communityframe_hackvent.setVisibility(View.GONE);
                                swipe_team_hackvent.setAlpha(1f);
                                swipe_team_hackvent.setVisibility(View.VISIBLE);
                            }
                        });
                // communityframe.setVisibility(View.GONE);


                ViewGroup.LayoutParams params = fchackvent.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fchackvent.setLayoutParams(params);

                // fcmonth.requestLayout();
            }

            @Override
            public void onOpened(SwipeRevealLayout view) {


                swipe_team_hackvent.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                swipe_team_hackvent.setVisibility(View.GONE);
                                communityframe_hackvent.setAlpha(1f);
                                communityframe_hackvent.setVisibility(View.VISIBLE);
                            }
                        });
                //  swipe_team_month.setVisibility(View.GONE);
                //  communityframe.setVisibility(View.VISIBLE);

                ViewGroup.LayoutParams params = fchackvent.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fchackvent.setLayoutParams(params);
                // fcmonth.requestLayout();
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {
                ViewGroup.LayoutParams params = fchackvent.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fchackvent.setLayoutParams(params);
                fchackvent.requestLayout();
            }
        });


        swipe_layout_community_week.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {

                communityframe_week.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                communityframe_week.setVisibility(View.GONE);
                                swipe_team_week.setAlpha(1f);
                                swipe_team_week.setVisibility(View.VISIBLE);
                            }
                        });
                // communityframe.setVisibility(View.GONE);


                ViewGroup.LayoutParams params = fcweek.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcweek.setLayoutParams(params);

                // fcmonth.requestLayout();
            }

            @Override
            public void onOpened(SwipeRevealLayout view) {


                swipe_team_week.animate()
                        .translationY(0)
                        .alpha(0.0f)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                swipe_team_week.setVisibility(View.GONE);
                                communityframe_week.setAlpha(1f);
                                communityframe_week.setVisibility(View.VISIBLE);
                            }
                        });
                //  swipe_team_month.setVisibility(View.GONE);
                //  communityframe.setVisibility(View.VISIBLE);

                ViewGroup.LayoutParams params = fcweek.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcweek.setLayoutParams(params);
                // fcmonth.requestLayout();
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {
                ViewGroup.LayoutParams params = fcweek.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                fcweek.setLayoutParams(params);
                fcweek.requestLayout();
            }
        });

        bmb_main = actionBar.findViewById(R.id.bmb_main2);
        bmb_main.setButtonEnum(ButtonEnum.TextInsideCircle);
        bmb_main.setBoomInWholeScreen(true);
        bmb_main.setAutoHide(true);

        bmb_main.setBackgroundColor(getResources().getColor(R.color.transparent, getTheme()));
        bmb_main.setBackgroundResource(R.drawable.rounded_settings);
        bmb_main.setUse3DTransformAnimation(false);

        bmb_main.setDimColor(getResources().getColor(R.color.colorPrimaryDarkSemiTrans, getTheme()));
        menuitems = getResources().getStringArray(R.array.menutext);
        menuitems_drawble = getResources().obtainTypedArray(R.array.menutext_drawable);
        for (int i = 0; i < bmb_main.getPiecePlaceEnum().pieceNumber(); i++) {
            TextInsideCircleButton.Builder builder = new TextInsideCircleButton.Builder()
                    //.isRound(false)
                    .shadowCornerRadius(Util.dp2px(5))
                    //  .buttonCornerRadius(Util.dp2px(5))

                    .shadowEffect(true)
                    .imageRect(new Rect(50, 15, Util.dp2px(60), Util.dp2px(60)))
                    .imagePadding(new Rect(10, 15, 10, 8))
                    .textSize(13)
                    .highlightedColorRes(R.color.colorAccent)
                    .normalImageRes(menuitems_drawble.getResourceId(i, -1))
                    .normalText(menuitems[i])
                    .rippleEffect(true)

                    .typeface(proto)

                    .textPadding(new Rect(5, 4, 5, 0))
                    .normalColorRes(R.color.colorPrimaryDark)

                    .pieceColorRes(R.color.transparent);


            if (i == 4) {
                if (AgentData.getFaction() != null) {
                    if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                        builder.normalColorRes(R.color.colorENL);
                    } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                        builder.normalColorRes(R.color.colorRES);
                    } else {
                        builder.normalColorRes(R.color.colorAccent);
                    }
                } else {
                    Intent ix = new Intent(getBaseContext(), FirebaseUIActivity.class);
                    //i.putExtra("PersonID", personID);
                    startActivity(ix);
                    finish();
                }

            }

            bmb_main.addBuilder(builder);
            bmb_main.setOnBoomListener(new OnBoomListener() {


                @Override
                public void onClicked(int index, BoomButton boomButton) {


                    // TODO
                    // MISSION HISTORY
                    if (index == 0) {

                        Intent irankingh = new Intent(getBaseContext(), RankingHistory.class);
                        irankingh.putExtra("theweek", theweekx);
                        irankingh.putExtra("themonth", themonthx);

                        startActivity(irankingh);
                        //i.putExtra("PersonID", personID);
                    }

                    // PVP History
                    if (index == 1) {
                        StyleableToast.makeText(getApplicationContext(), "PvP isnt finished.. working on!", Toast.LENGTH_LONG, R.style.defaulttoast).show();


                    }
                    // EARTH HISTORY
                    if (index == 2) {
                        // Intent i = new Intent(getBaseContext(), Donation.class);
                        //i.putExtra("PersonID", personID);
                        // startActivity(i);

                        Intent irankingh = new Intent(getBaseContext(), EarthHistory.class);
                        irankingh.putExtra("theweek", theweekx);
                        irankingh.putExtra("themonth", themonthx);
                        irankingh.putExtra("earth", "earth");

                        startActivity(irankingh);
                        //  StyleableToast.makeText(getApplicationContext(), "Earth History soon.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                    // PASSPORT
                    if (index == 3) {

                        StyleableToast.makeText(getApplicationContext(), "Passport... coming soon", Toast.LENGTH_LONG, R.style.defaulttoast).show();


                    }
                    // PROFILE
                    if (index == 4) {

                        Intent i = new Intent(getBaseContext(), ProfileMain.class);
                        //i.putExtra("PersonID", personID);
                        startActivity(i);

                        //finish();
                    }
                    // SETTINGS
                    if (index == 5) {
                        StyleableToast.makeText(getApplicationContext(), "Teams can be created soon.", Toast.LENGTH_LONG, R.style.defaulttoast).show();


                    }

                    // INGRESS EVENTS
                    if (index == 6) {

                        Intent i = new Intent(getBaseContext(), NIAEvents.class);
                        //i.putExtra("PersonID", personID);
                        startActivity(i);

                    }
                    // FIRST SATURDAY
                    if (index == 7) {

                        StyleableToast.makeText(getApplicationContext(), "FS .. coming soon", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    }
                    // TEAMS
                    if (index == 8) {
                        Intent i = new Intent(getBaseContext(), MainSettings.class);
                        //i.putExtra("PersonID", personID);
                        startActivity(i);


                    }

                }

                @Override
                public void onBackgroundClick() {

                }

                @Override
                public void onBoomWillHide() {
                    // bmb_settings.setVisibility(View.GONE);
                    // bmb_filter.setVisibility(View.VISIBLE);
                    mTitleTextView.setText("AOD");
                }

                @Override
                public void onBoomDidHide() {

                }


                @Override
                public void onBoomWillShow() {
                    // bmb_settings.setVisibility(View.VISIBLE);
                    //     bmb_filter.setVisibility(View.GONE);
                    mTitleTextView.setText("AOD ..MENU");
                }

                @Override
                public void onBoomDidShow() {

                }


            });
        }


        // Log.i("AGENTNAME", pref.getString(API.child1,"null"));
        bmb_main.setBoomEnum(BoomEnum.LINE);
        bmb_main.setShowScaleEaseEnum(EaseEnum.EaseOutBack);


        if (user != null) {

            //Crashlytics.setUserIdentifier("" + AgentData.getagentname());
            // initmissions();
            // new DataSetInit().execute();
            // User is signed in
        } else {
            startActivity(new Intent(this, FirebaseUIActivity.class));
            finish();
        }

        if (firstRun()) {
            //getLogDialog();
            createNotificationChannel();


        }

        FirebaseMessaging.getInstance().subscribeToTopic(user.getUid());


    }

    private void initmissions() {

        // GET TEAM TODO
        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotteam) {
                //   Log.i("SNAPP IS ", snapshotteam.child("email").getValue().toString() + "");


                // SUCHE FREUNDE
                if (snapshotteam.hasChild("friends")) {
                    Log.i("Friends-->", "Freunde gefunden");
                    pref.edit().putBoolean("hasfriends", true).apply();


                } else {
                    Log.i("Friends -->", "Keine Freunde gefunden");

                    pref.edit().putBoolean("hasfriends", false).apply();


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //  StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
        // TODO
        frameloadmonth.setVisibility(View.VISIBLE);
        framecardmonth.setVisibility(View.GONE);

        // TODO IF HACKVENT
        frameloadhackvent.setVisibility(View.VISIBLE);
        framecardhackvent.setVisibility(View.GONE);

        frameloadweek.setVisibility(View.VISIBLE);
        framecardweek.setVisibility(View.GONE);

        pvp_frame_base.setVisibility(View.GONE);

        frameloadearth.setVisibility(View.VISIBLE);
        framecardearth.setVisibility(View.GONE);


        frameloadmini.setVisibility(View.VISIBLE);
        framecardmini.setVisibility(View.GONE);
        // GET THE DATE
        // SimpleDateFormat fday = new SimpleDateFormat("yyyyMMdd_HHmmss");
        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
        SimpleDateFormat fyear = new SimpleDateFormat("yyyy");
        final String themonth = fmonth.format(new Date());
        final String theyear = fyear.format(new Date());
        new AgentData("year", theyear);
        new AgentData("month", themonth);
        theyearx = theyear;
        themonthx = themonth;
        final long timmi = System.currentTimeMillis();

        Map<String, String> timestampx = new HashMap<>();
        timestampx = ServerValue.TIMESTAMP;
        mDatabase.child(user.getUid()).child("servertime").setValue(timestampx).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {


                    mDatabase.child(user.getUid()).child("servertime").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshottime) {

                            long tstamp = Long.parseLong(snapshottime.getValue().toString());
                            //Log.i("timestamos---> ", "1 = "+timmi+ " diff: "+(tstamp-timmi)+ " fb: "+ ServerValue.TIMESTAMP.get(".sv").toString());

                            if ((tstamp - timmi) > 3600000 || (tstamp - timmi) < -3600000) {
                                // TODO timemanipulazion
                                StyleableToast.makeText(getApplicationContext(), "Time was manipulated", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            } else {
                                DateFormat format = new SimpleDateFormat("dd");
                                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                                Calendar.getInstance(Locale.getDefault()).setTimeZone(TimeZone.getDefault());
                                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

                                final String[] days = new String[7];
                                for (int i = 0; i < 7; i++) {
                                    days[i] = format.format(calendar.getTime());
                                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                                }

                                new AODConfig();
                                new AODConfig("reff", getApplicationContext());
                                if (user == null) {
                                    user = FirebaseAuth.getInstance().getCurrentUser();
                                }

                                String strDate = days[0] + "." + (Integer.parseInt(themonth)) + "." + theyear;
                                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                                Date date = null;
                                try {
                                    date = dateFormat.parse(strDate);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Calendar now = Calendar.getInstance(Locale.GERMANY);
                                // now.setTime(date);
                                now.setTimeZone(TimeZone.getDefault());
                                now.setFirstDayOfWeek(Calendar.MONDAY);
                                int currentweek = now.get(Calendar.WEEK_OF_YEAR);
                                String cweeksend;
                                if (currentweek <= 9) {
                                    currentweek = Integer.parseInt("0" + currentweek);
                                    cweeksend = "0" + currentweek;
                                } else {
                                    cweeksend = String.valueOf(currentweek);
                                }


                                 Log.i("CURRENT WEEK FOR POMMES", "" + cweeksend);
                                final String thefirstweekday = cweeksend + "" + days[0];
                                new AgentData("week", thefirstweekday);
                                theweekx = thefirstweekday;
                                // Log.i("MONTH -->", themonth);
                                //  Log.i("Year -->", theyear);
                                // Log.i("Dayof Week -->", "" + days[0]);

                                mDatabase.child(user.getUid()).child("missions").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshot) {


                                        if (snapshot.child("month").hasChild(themonth)) {
                                            Log.i("MONTH -->", "Monat running gefunden");

                                            monthrunning = true;
                                            // STARTE MONAT GEFUNDEN
                                            initmonth(theyear, themonth, true);


                                        } else {
                                            Log.i("MONTH -->", "Kein Monat running gefunden");
                                            monthrunning = false;
                                            initmonth(theyear, themonth, false);


                                        }

                                        Log.i("GET EARTH 2 ", " - " + remoteconfig.getString("earthmissionname"));
                                        // EARTH MISSIONS
                                        if (remoteconfig.getBoolean("earthmission") && snapshot.child("earth").hasChild(remoteconfig.getString("earthmissionname"))) {
                                            Log.i("EARTH -->", "EARTH running gefunden");


                                            initearth(theyear, remoteconfig.getString("earthmissionname"), true);


                                        } else {
                                            Log.i("EARTH -->", "Kein EARTH  running gefunden");
                                            if (remoteconfig.getBoolean("earthmission")) {
                                                initearth(theyear, remoteconfig.getString("earthmissionname"), false);
                                            } else {
                                                fcearth.setVisibility(View.GONE);
                                            }


                                        }

                                        if (!remoteconfig.getBoolean("hackvent")) {
                                            Log.i("HACKEVNT -->", "hack false");
                                            frameloadhackvent.setVisibility(View.GONE);
                                            framecardhackvent.setVisibility(View.VISIBLE);


                                            inithackvent(theyear);
                                        } else {
                                            Log.i("HACKEVNT -->", "hack true");
                                            fchackvent.setVisibility(View.GONE);
                                        }


                                        if (snapshot.child("week").hasChild(thefirstweekday)) {
                                            fcweek.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    fcweek.toggle(false);
                                                }
                                            });

                                            weekrunning = true;
                                            initweek(theyear, themonth, thefirstweekday, days, true);
                                            Log.i("WEEK -->", "Woche gefunden");

                                        } else {

                                            weekrunning = false;
                                            initweek(theyear, themonth, thefirstweekday, days, false);
                                            Log.i("WEEK -->", "Keine Woche gefunden" + theyear + " " + themonth + " " + thefirstweekday + " " + days);


                                        }

                                        if (snapshot.child("mini").getChildrenCount() > 0) {
                                            for (final DataSnapshot childmini : snapshot.child("mini").getChildren()) {
                                                Log.i("TEST LOG--->", childmini.getKey());
                                                if (System.currentTimeMillis() < Long.parseLong(childmini.getKey())) {
                                                    Log.i("Mini -->", "MINI running gefunden 1");
                                                    Log.i("TEST LOG--->", childmini.getKey());
                                                    initmini(Long.parseLong(childmini.getKey()), true);
                                                } else {
                                                    initmini(System.currentTimeMillis(), false);
                                                }


                                            }


                                            //monthrunning = true;
                                            // STARTE MONAT GEFUNDEN
                                            // initmonth(theyear, themonth, true);


                                        } else {
                                            Log.i("MINI -->", "Kein Monat running gefunden");
                                            // monthrunning = false;
                                            // initmonth(theyear, themonth, false);
                                            initmini(System.currentTimeMillis(), false);


                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });


                                mDatabase.child(user.getUid()).child("pvp").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshotpvp) {


                                        if (snapshotpvp.exists()) {
                                            Log.i("PV -->", "PVP gefunden");

                                            // STARTE MONAT GEFUNDEN
                                            initpvp(snapshotpvp, true);


                                        } else {
                                            Log.i("PVP -->", "Kein PVP gefunden");
                                            initpvp(snapshotpvp, false);


                                        }


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });

                                mySwipeRefreshLayout.setRefreshing(false);

                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                        }
                    });


                } else if (task.isCanceled()) {

                } else {
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                //signOut();
            }
        });


    }

    private void initmonth(final String theyear, final String themonth, final boolean isrunning) {
        Log.i("INIT MONTH -->", "gestartet " + isrunning);
        mDatabasemissions.child("month").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshot) {
                Log.i("INIT MONTH SNAP -->", themonth + " " + theyear);


                if (snapshot.hasChild(themonth)) {
                    Log.i("MONTH SNAP -->", "Monat gefunden");
                    // SET TITLEVIEW
                    int monthd = 0;

                    try {
                        monthd = Integer.parseInt(themonth);
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                    TextView month_txt = findViewById(R.id.title_time_label);
                    month_txt.setText(getMonthShortName(monthd - 1));
                    //final TextView month_txt_content = findViewById(R.id.monthstitle);
                    final TextView month_txt_cat = findViewById(R.id.monthscat);


                    mDatabaseaod.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshotaod) {
                            TextView monatsub = findViewById(R.id.title_to_address);
                            monatsub.setText(snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("title").getValue().toString());
                            // monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield;
                            monthbadgeid = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("id").getValue().toString();
                            monthbadgetitle = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("title").getValue().toString();
                            monthbadgeshort1 = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString();
                            monthbadgeshort2 = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short2").getValue().toString();
                            monthbadgefield = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("field").getValue().toString();
                            monthbadgedrawable = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawable").getValue().toString();
                            monthbadgedrawablegold = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawablegold").getValue().toString();
                            monthbadgedrawableplatin = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawableplatin").getValue().toString();
                            month_txt_cat.setText(snapshotaod.child("cat").child(snapshot.child(themonth).child("cat").getValue().toString()).child("title").getValue().toString());
                            TextView notstartedmonthinfo = findViewById(R.id.notstartedmonthinfo);

                            Log.i("Badges -->", "1 :" + monthbadgedrawable + " 2: " + monthbadgedrawablegold + "3: " + monthbadgedrawableplatin);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                notstartedmonthinfo.setText(Html.fromHtml(String.format(getString(R.string.goal_title), snapshot.child(themonth).child("goal").getValue().toString(), snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                notstartedmonthinfo.setText(Html.fromHtml(String.format(getString(R.string.goal_title), snapshot.child(themonth).child("goal").getValue().toString(), snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString())));

                            }

                            themonthgoal = Integer.parseInt(snapshot.child(themonth).child("goal").getValue().toString());
                            themonthgoalplatin = Integer.parseInt(snapshot.child(themonth).child("goalplatin").getValue().toString());
                            themonthgoalblack = Integer.parseInt(snapshot.child(themonth).child("goalblack").getValue().toString());

                            // SET ICON

                            int badgemonthid = getResources().getIdentifier(snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawable").getValue().toString(), "drawable", getPackageName());

                            //badgemonth = getResources().obtainTypedArray(badgemonthid);
                            if (badgemonthid != 0) {
                                badgemonthidscreener = badgemonthid;
                                Log.w("BADGE -->", " id :" + badgemonthid);
                                ImageView month_badge = findViewById(R.id.title_price);
                                month_badge.setImageDrawable(getDrawable(badgemonthid));
                            }

                            // STARTE GET DATA FOR MONTH
                            if (isrunning) {

                                // GET AGENT MONTH MISSION DETAILS
                                cmonthmissionid = snapshot.child(themonth).child("id").getValue().toString();
                                monthgetagentDetails(snapshot.child(themonth).child("id").getValue().toString(), snapshot.child(themonth), theyear, themonth);

                            } else {

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        frameloadmonth.setVisibility(View.GONE);
                                        framecardmonth.setVisibility(View.VISIBLE);

                                    }
                                }, 1000);
                                Log.i("DATA ID -->", snapshot.child(themonth).child("id").getValue().toString());

                                monthnotstartet.setVisibility(View.VISIBLE);
                                monthstartet.setVisibility(View.GONE);
                                join_btn_month.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                                            Bundle params = new Bundle();
                                            params.putString("agent", AgentData.getagentname());
                                            mFirebaseAnalytics.logEvent("aod_missions_month_joined", params);
                                            mDatabasemissions.child("month").child(theyear).child(themonth).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshotupdate) {
                                                    int upagents = (Integer.parseInt(dataSnapshotupdate.child("agents").getValue().toString()) + 1);
                                                    final Map<String, Object> childUpdatesx = new HashMap<>();
                                                    childUpdatesx.put("/agents", upagents);
                                                    mDatabasemissions.child("month").child(theyear).child(themonth).updateChildren(childUpdatesx).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                //finish();
                                                                joinmission("month", snapshot.child(themonth).child("id").getValue().toString(), snapshot);
                                                            } else if (task.isCanceled()) {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9001");
                                                                errorToast("month", "9001");
                                                            } else {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9002");
                                                                errorToast("month", "9002");
                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 9003");
                                                            errorToast("month", "9003");

                                                            //signOut();
                                                        }
                                                    });


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError error) {
                                                    // Failed to read value
                                                    Log.e("TAG", "Failed to update.", error.toException());
                                                    errorToast("month", "9004");
                                                }
                                            });

                                        } else {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                            // builder.setMessage("Text not finished...");
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                            builder.setView(dialog);
                                            final TextView input = dialog.findViewById(R.id.caltext);
                                            input.setTypeface(type);
                                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Todo publish the code to database
                                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                    //i.putExtra("PersonID", personID);
                                                    startActivity(i);
                                                    //finish();

                                                }
                                            });

                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Todo publish the code to database
                                                }
                                            });

                                            builder.setCancelable(true);

                                            if (!isFinishing()) {
                                                builder.create().show();

                                            }
                                        }
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                        }
                    });
                } else {
                    Log.i("MONTH SNAP-->", "Monat noch nicht aktiv");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
            }
        });


    }


    private void initmini(final Long thetimestamp, final boolean isrunning) {
        Log.i("INIT MINI -->", "gestartet " + isrunning);
        final int[] miniloop = {0};
        final boolean[] hasmini = {false};
        final String[] themininikey = new String[1];
        mDatabasemissions.child("mini").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshot) {

                if (snapshot.getChildrenCount() > 0) {
                    for (final DataSnapshot childmini : snapshot.getChildren()) {
                        miniloop[0]++;
                        Log.i("TEST LOG--->", childmini.getKey());
                        if (System.currentTimeMillis() < Long.parseLong(childmini.getKey())) {
                            Log.i("Mini -->", "MINI running gefunden 2");
                            Log.i("TEST LOG--->", childmini.getKey());
                            hasmini[0] = true;
                            themininikey[0] = childmini.getKey();
                            // initmini(Long.parseLong(childmini.getKey().toString()), true);
                        }


                        if (miniloop[0] >= snapshot.getChildrenCount()) {
                            Log.i("TEST LOG--->", " " + hasmini + " " + miniloop[0]);
                            if (hasmini[0] && !remoteconfig.getBoolean("minimission")) {
                                //if (hasmini[0] && remoteconfig.getBoolean("minimission")) {
                                fcmini.setVisibility(View.VISIBLE);
                                frameloadmini.setVisibility(View.GONE);
                                framecardmini.setVisibility(View.VISIBLE);
                                fcmini.setVisibility(View.VISIBLE);
                                TextView mini_txt = findViewById(R.id.title_time_label_mini);
                                mini_txt.setText("MISSION");
                                //final TextView mini_txt_content = findViewById(R.id.ministitle);
                                final TextView mini_txt_cat = findViewById(R.id.miniscat);


                                mDatabaseaod.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshotaod) {
                                        TextView minisub = findViewById(R.id.title_to_address_mini);
                                        minisub.setText(snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("title").getValue().toString());

                                        minibadgetitle = snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("title").getValue().toString();
                                        minibadgeshort1 = snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("short1").getValue().toString();
                                        minibadgeshort2 = snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("short2").getValue().toString();
                                        minibadgefield = snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("field").getValue().toString();
                                        // minibadgetitle, minibadgeshort1, mminiadgeshort2, minibadgefield;
                                        // minibadgeid = snapshotaod.child("badge").child(snapshot.child(themini).child("badge").getValue().toString()).child("id").getValue().toString();

                                        mini_txt_cat.setText(snapshotaod.child("cat").child(snapshot.child(themininikey[0]).child("cat").getValue().toString()).child("title").getValue().toString());
                                        TextView notstartedminiinfo = findViewById(R.id.notstartedminiinfo);

                                        minibadgedrawable = snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("drawable").getValue().toString();
                                        minibadgedrawablegold = snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("drawablegold").getValue().toString();
                                        minibadgedrawableplatin = snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("drawableplatin").getValue().toString();


                                        notstartedminiinfo.setText(snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("descr").child("en").getValue().toString());


                                        // themonthgoal = Integer.parseInt(snapshot.child(themini).child("goal").getValue().toString());


                                        // SET ICON

                                        int badgeminiid = getResources().getIdentifier(snapshotaod.child("badge").child(snapshot.child(themininikey[0]).child("badge").getValue().toString()).child("drawable").getValue().toString(), "drawable", getPackageName());

                                        //badgemini = getResources().obtainTypedArray(badgeminiid);
                                        if (badgeminiid != 0) {
                                            badgeminiidscreener = badgeminiid;
                                            Log.w("BADGE -->", " id :" + badgeminiid);
                                            ImageView mini_badge = findViewById(R.id.title_price_mini);
                                            mini_badge.setImageDrawable(getDrawable(badgeminiid));
                                        }

                                        // STARTE GET DATA FOR mini
                                        if (isrunning) {
                                            // GET AGENT MINI MISSION DETAILS
                                            cminimissionid = snapshot.child(themininikey[0]).child("id").getValue().toString();
                                            final TextView mission_detals_content_txt_mini = findViewById(R.id.mission_detals_content_txt_mini);
                                            mission_detals_content_txt_mini.setText(snapshot.child(themininikey[0]).child("descr").child("en").getValue().toString());
                                            minigetagentDetails(snapshot.child(themininikey[0]).child("id").getValue().toString(), snapshot.child(themininikey[0]), theyearx, themonthx);

                                        } else {

                                            new Handler().postDelayed(new Runnable() {

                                                @Override
                                                public void run() {
                                                    frameloadmini.setVisibility(View.GONE);
                                                    framecardmini.setVisibility(View.VISIBLE);

                                                }
                                            }, 1000);
                                            Log.i("DATA ID -->", snapshot.child(themininikey[0]).child("id").getValue().toString());

                                            mininotstartet.setVisibility(View.VISIBLE);
                                            ministartet.setVisibility(View.GONE);
                                            join_btn_mini.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                    if (snapshot.child(themininikey[0]).child("id").getValue().toString().equalsIgnoreCase("screen")) {
                                                        if (pref.getBoolean(remoteconfig.getString("pref_calibration_invent"), false)) {
                                                            Bundle params = new Bundle();
                                                            params.putString("agent", AgentData.getagentname());
                                                            mFirebaseAnalytics.logEvent("aod_missions_mini_joined", params);
                                                            mDatabasemissions.child("mini").child(themininikey[0]).addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(DataSnapshot dataSnapshotupdate) {
                                                                    int upagents = (Integer.parseInt(dataSnapshotupdate.child("agents").getValue().toString()) + 1);
                                                                    final Map<String, Object> childUpdatesx = new HashMap<>();
                                                                    childUpdatesx.put("/agents", upagents);
                                                                    mDatabasemissions.child("mini").child(themininikey[0]).updateChildren(childUpdatesx).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull Task<Void> task) {
                                                                            if (task.isSuccessful()) {
                                                                                //finish();
                                                                                joinmission("mini", snapshot.child(themininikey[0]).child("id").getValue().toString(), snapshot);
                                                                            } else if (task.isCanceled()) {
                                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9001");
                                                                                errorToast("month", "9001");
                                                                            } else {
                                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9002");
                                                                                errorToast("month", "9002");
                                                                            }
                                                                        }

                                                                    }).addOnFailureListener(new OnFailureListener() {
                                                                        @Override
                                                                        public void onFailure(Exception e) {
                                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 9003");
                                                                            errorToast("mini", "9003");

                                                                            //signOut();
                                                                        }
                                                                    });


                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError error) {
                                                                    // Failed to read value
                                                                    Log.e("TAG", "Failed to update.", error.toException());
                                                                    errorToast("month", "9004");
                                                                }
                                                            });

                                                        } else {
                                                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                                            // builder.setMessage("Text not finished...");
                                                            LayoutInflater inflater = getLayoutInflater();
                                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                                            builder.setView(dialog);
                                                            final TextView input = dialog.findViewById(R.id.caltext);
                                                            input.setTypeface(type);
                                                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    // Todo publish the code to database
                                                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                                    //i.putExtra("PersonID", personID);
                                                                    startActivity(i);
                                                                    //finish();

                                                                }
                                                            });

                                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    // Todo publish the code to database
                                                                }
                                                            });

                                                            builder.setCancelable(true);

                                                            if (!isFinishing()) {
                                                                builder.create().show();

                                                            }
                                                        }
                                                    } else {
                                                        Bundle params = new Bundle();
                                                        params.putString("agent", AgentData.getagentname());
                                                        mFirebaseAnalytics.logEvent("aod_missions_mini_joined", params);
                                                        mDatabasemissions.child("mini").child(themininikey[0]).addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot dataSnapshotupdate) {
                                                                int upagents = (Integer.parseInt(dataSnapshotupdate.child("agents").getValue().toString()) + 1);
                                                                final Map<String, Object> childUpdatesx = new HashMap<>();
                                                                childUpdatesx.put("/agents", upagents);
                                                                mDatabasemissions.child("mini").child(themininikey[0]).updateChildren(childUpdatesx).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                        if (task.isSuccessful()) {
                                                                            //finish();
                                                                            joinmission("mini", snapshot.child(themininikey[0]).child("id").getValue().toString(), snapshot);
                                                                        } else if (task.isCanceled()) {
                                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 9001");
                                                                            errorToast("month", "9001");
                                                                        } else {
                                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 9002");
                                                                            errorToast("month", "9002");
                                                                        }
                                                                    }

                                                                }).addOnFailureListener(new OnFailureListener() {
                                                                    @Override
                                                                    public void onFailure(Exception e) {
                                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 9003");
                                                                        errorToast("mini", "9003");

                                                                        //signOut();
                                                                    }
                                                                });


                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError error) {
                                                                // Failed to read value
                                                                Log.e("TAG", "Failed to update.", error.toException());
                                                                errorToast("mini", "9004");
                                                            }
                                                        });
                                                    }
                                                }
                                            });


                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });

                                // END MINI
                            } else {
                                fcmini.setVisibility(View.GONE);


                            }
                        }


                    }


                    //monthrunning = true;
                    // STARTE MONAT GEFUNDEN
                    // initmonth(theyear, themonth, true);


                } else {
                    Log.i("MINI -->", "Kein Monat running gefunden");
                    // monthrunning = false;
                    // initmonth(theyear, themonth, false);
                    // initmini(System.currentTimeMillis(), false);


                }
                // Log.i("INIT MINI SNAP -->", " "+isrunning);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
            }
        });


    }

    private void initearth(final String theyear, final String themonth, final boolean isrunning) {
        Log.i("INIT EARTH -->", "gestartet " + isrunning);
        mDatabasemissions.child("earth").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshot) {
                Log.i("INIT earth SNAP -->", themonth + " " + theyear);


                if (snapshot.hasChild(themonth)) {
                    Log.i("EARTH SNAP -->", "EARTH gefunden");
                    // SET TITLEVIEW

                    TextView earth_txt = findViewById(R.id.title_time_label_earth);
                    earth_txt.setText("SPECIAL");
                    //final TextView month_txt_content = findViewById(R.id.monthstitle);
                    final TextView earth_txt_cat = findViewById(R.id.earthscat);


                    mDatabaseaod.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshotaod) {
                            TextView earthsub = findViewById(R.id.title_to_address_earth);
                            earthsub.setText(snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("title").getValue().toString());
                            // monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield;
                            earthbadgeid = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("id").getValue().toString();
                            earthbadgetitle = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("title").getValue().toString();
                            earthbadgeshort1 = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString();
                            earthbadgeshort2 = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short2").getValue().toString();
                            earthbadgefield = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("field").getValue().toString();
                            earthbadgedrawable = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawable").getValue().toString();
                            earthbadgedrawablegold = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawablegold").getValue().toString();
                            earthbadgedrawableplatin = snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawableplatin").getValue().toString();
                            earth_txt_cat.setText(snapshotaod.child("cat").child(snapshot.child(themonth).child("cat").getValue().toString()).child("title").getValue().toString());
                            TextView notstartedearthinfo = findViewById(R.id.notstartedearthinfo);

                            Log.i("Badges -->", "1 :" + earthbadgedrawable + " 2: " + earthbadgedrawablegold + "3: " + earthbadgedrawableplatin);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                //String.format(Locale.getDefault(),"%,d", togo_com)
                                notstartedearthinfo.setText(Html.fromHtml("Special Challenge. " + String.format(getString(R.string.goal_title), String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshot.child(themonth).child("community").child("goalnia").getValue().toString())), snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                notstartedearthinfo.setText(Html.fromHtml("Special Challenge. " + String.format(getString(R.string.goal_title), String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshot.child(themonth).child("community").child("goalnia").getValue().toString())), snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("short1").getValue().toString())));

                            }
                            notstartedearthinfo.setSelected(true);

                            theearthgoal = Integer.parseInt(snapshot.child(themonth).child("goal").getValue().toString());
                            theearthgoalplatin = Integer.parseInt(snapshot.child(themonth).child("goalplatin").getValue().toString());
                            theearthgoalblack = Integer.parseInt(snapshot.child(themonth).child("goalblack").getValue().toString());

                            // SET ICON

                            int badgeearthidnia = getResources().getIdentifier(snapshot.child(themonth).child("badgenia").getValue().toString(), "drawable", getPackageName());
                            int badgeearthid = getResources().getIdentifier(snapshotaod.child("badge").child(snapshot.child(themonth).child("badge").getValue().toString()).child("drawable").getValue().toString(), "drawable", getPackageName());

                            //badgemonth = getResources().obtainTypedArray(badgemonthid);
                            if (badgeearthidnia != 0) {
                                badgeearthidscreener = badgeearthidnia;
                                Log.w("BADGE -->", " id :" + badgeearthidnia);
                                ImageView earth_badge = findViewById(R.id.title_price_earth);
                                earth_badge.setImageDrawable(getDrawable(badgeearthidnia));
                            }

                            // STARTE GET DATA FOR earth
                            if (isrunning) {
                                // TODO GET DETAILS FOR THE AGENT
                                cearthmissionid = snapshot.child(themonth).child("id").getValue().toString();
                                earthgetagentDetails(snapshot.child(themonth).child("id").getValue().toString(), snapshot.child(themonth), theyear, themonth);

                            } else {

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        frameloadearth.setVisibility(View.GONE);
                                        framecardearth.setVisibility(View.VISIBLE);

                                    }
                                }, 1000);
                                Log.i("DATA ID -->", snapshot.child(themonth).child("id").getValue().toString());

                                earthnotstartet.setVisibility(View.VISIBLE);
                                earthstartet.setVisibility(View.GONE);
                                join_btn_earth.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                                            Bundle params = new Bundle();
                                            params.putString("agent", AgentData.getagentname());
                                            mFirebaseAnalytics.logEvent("aod_missions_earth_joined", params);
                                            mDatabasemissions.child("earth").child(theyear).child(themonth).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshotupdate) {
                                                    int upagents = (Integer.parseInt(dataSnapshotupdate.child("agents").getValue().toString()) + 1);
                                                    final Map<String, Object> childUpdatesx = new HashMap<>();
                                                    childUpdatesx.put("/agents", upagents);
                                                    mDatabasemissions.child("earth").child(theyear).child(themonth).updateChildren(childUpdatesx).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                //finish();
                                                                joinmission("earth", snapshot.child(themonth).child("id").getValue().toString(), snapshot);
                                                            } else if (task.isCanceled()) {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9001");
                                                                errorToast("earth", "9001");
                                                            } else {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9002");
                                                                errorToast("earth", "9002");
                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 9003");
                                                            errorToast("earth", "9003");

                                                            //signOut();
                                                        }
                                                    });


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError error) {
                                                    // Failed to read value
                                                    Log.e("TAG", "Failed to update.", error.toException());
                                                    errorToast("earth", "9004");
                                                }
                                            });

                                        } else {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                            // builder.setMessage("Text not finished...");
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                            builder.setView(dialog);
                                            final TextView input = dialog.findViewById(R.id.caltext);
                                            input.setTypeface(type);
                                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Todo publish the code to database
                                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                    //i.putExtra("PersonID", personID);
                                                    startActivity(i);
                                                    // finish();

                                                }
                                            });

                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Todo publish the code to database
                                                }
                                            });

                                            builder.setCancelable(true);

                                            if (!isFinishing()) {
                                                builder.create().show();

                                            }
                                        }
                                    }
                                });


                                info_btn_earth.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Uri uri = Uri.parse(snapshot.child(themonth).child("link").getValue().toString());
                                        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uri);
                                        startActivity(launchBrowser);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                        }
                    });
                } else {
                    Log.i("earth SNAP-->", "earth noch nicht aktiv");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
            }
        });


    }

    private void inithackvent(final String theyear) {
        Log.i("INIT HACKWEVT -->", "gestartet");
        final String[] introtext = new String[1];
        final String[] taskA = new String[1];
        final String[] taskB = new String[1];
        final String[] taskis = new String[1];
        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
        SimpleDateFormat fday = new SimpleDateFormat("dd");
        final String themonth = fmonth.format(new Date());
        final String theday = fday.format(new Date());
        final boolean[] hackventdaystarted = new boolean[1];


        final int thedayis = (Integer.parseInt(theday) - 1);

        if (Integer.parseInt(themonth) == 12) {
            mhackvent.child(theyear).child(String.valueOf(thedayis)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(final DataSnapshot snapshot) {
                    Log.i("INIT hackvent SNAP -->", " " + thedayis);


                    TextView hackvent_txt = findViewById(R.id.title_time_label_hackvent);

                    hackvent_txt.setText(snapshot.child("id").getValue().toString());
                    // hackvent_txt.setText(snapshot.child("title-EN").getValue().toString());
                    //final TextView month_txt_content = findViewById(R.id.monthstitle);
                    final TextView hackvent_txt_cat = findViewById(R.id.hackventscat);
                    String ddtag;
                    if (Integer.parseInt(theday) <= 9) {
                        ddtag = theday;
                    } else {
                        ddtag = theday;
                    }

                    Log.i("INIT hackvent SNAP -->", theday + " d: " + ddtag);
                    final TextView hackventsub = findViewById(R.id.title_to_address_hackvent);
                    final TextView title_date_label_hackvent = findViewById(R.id.title_date_label_hackvent);
                    title_date_label_hackvent.setText("(c) missionday.info");
                    TextView title_from_address_hackvent = findViewById(R.id.title_from_address_hackvent);

                    title_from_address_hackvent.setText(snapshot.child("title-EN").getValue().toString());
                    introtext[0] = snapshot.child("legend-EN").getValue().toString();
                    taskA[0] = snapshot.child("text1-EN").getValue().toString();
                    taskB[0] = snapshot.child("text2-EN").getValue().toString();
                    hackventsub.setText(snapshot.child("legend-EN").getValue().toString());
                    hackventsub.setMaxLines(4);
                    hackventsub.setEllipsize(TextUtils.TruncateAt.END);


                    ImageView hackvent_badge = findViewById(R.id.title_price_hackvent);

                    try {
                        PicassoCache.getPicassoInstance(getApplicationContext())
                                .load("https://advent.missionday.info/_res/dayimg/2019_Day_" + ddtag + ".jpg")
                                .placeholder(R.drawable.hackvent)
                                .error(R.drawable.hackvent)

                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(hackvent_badge);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }


                    mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshothackventuser) {


                            if (!snapshothackventuser.child("hackvent").child(theyear).child(theday).exists()) {
                                hackventdaystarted[0] = false;
                                try {
                                    final int[] timercount = {0};
                                    timerhackvent = new Timer();
                                    timerTaskhackvent = new TimerTask() {
                                        @Override
                                        public void run() {
                                            //Download file here and refresh
                                            if (timercount[0] == 0) {
                                                timercount[0]++;
                                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        hackventsub.setText(snapshot.child("legend-EN").getValue().toString());
                                                        hackventsub.setMaxLines(4);
                                                        hackventsub.setEllipsize(TextUtils.TruncateAt.END);
                                                    }
                                                });

                                            } else if (timercount[0] == 1) {
                                                timercount[0]++;
                                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        hackventsub.setText("A: " + snapshot.child("text1-EN").getValue().toString());
                                                        hackventsub.setMaxLines(4);
                                                        hackventsub.setEllipsize(TextUtils.TruncateAt.END);
                                                    }
                                                });

                                            } else if (timercount[0] == 2) {
                                                timercount[0] = 0;
                                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        hackventsub.setText("B: " + snapshot.child("text2-EN").getValue().toString());
                                                        hackventsub.setMaxLines(4);
                                                        hackventsub.setEllipsize(TextUtils.TruncateAt.END);
                                                    }
                                                });

                                            }

                                        }
                                    };
                                    timerhackvent.schedule(timerTaskhackvent, 4000, 8000);
                                } catch (IllegalStateException e) {
                                    android.util.Log.i("Damn", "resume error");
                                }
                            } else {
                                // STARTE GET DATA FOR earth
                                hackventdaystarted[0] = true;
                                if (snapshothackventuser.child("hackvent").child(theyear).child(theday).child("task").getValue().toString().equalsIgnoreCase("task1")) {
                                    taskis[0] = "task1";
                                    // TASK1
                                    TextView taskison = findViewById(R.id.title_weight_hackvent);
                                    taskison.setTypeface(type);
                                    taskison.setText("A");

                                    TextView title_requests_count_hackvent = findViewById(R.id.title_requests_count_hackvent);

                                    if (snapshothackventuser.child("hackvent").child(theyear).child(theday).child("progress").exists()) {
                                        title_requests_count_hackvent.setText(snapshothackventuser.child("hackvent").child(theyear).child(theday).child("progress").getValue().toString());
                                    } else {
                                        title_requests_count_hackvent.setText("need data");
                                    }
                                    TextView title_weight_hackvent = findViewById(R.id.title_pledge_hackvent);

                                    title_weight_hackvent.setText(snapshot.child("taskType1a value").getValue().toString());
                                    hackventsub.setText("TASK A: " + snapshot.child("text1-EN").getValue().toString());


                                } else if (snapshothackventuser.child("hackvent").child(theyear).child(theday).child("task").getValue().toString().equalsIgnoreCase("task2")) {
                                    // TASK2
                                    taskis[0] = "task2";
                                    TextView taskison = findViewById(R.id.title_weight_hackvent);
                                    taskison.setTypeface(type);
                                    taskison.setText("B");

                                    TextView title_requests_count_hackvent = findViewById(R.id.title_requests_count_hackvent);

                                    title_requests_count_hackvent.setText("running B");

                                    TextView title_weight_hackvent = findViewById(R.id.title_pledge_hackvent);

                                    title_weight_hackvent.setText(snapshot.child("taskType2a value").getValue().toString());

                                    hackventsub.setText("TASK B: " + snapshot.child("text2-EN").getValue().toString());

                                    if (snapshothackventuser.child("hackvent").child(theyear).child(theday).child("progress").exists()) {
                                        title_requests_count_hackvent.setText(snapshothackventuser.child("hackvent").child(theyear).child(theday).child("progress").getValue().toString());
                                    } else {
                                        title_requests_count_hackvent.setText("need data");
                                    }
                                } else {
                                    //NO TASK
                                    taskis[0] = "no task";
                                    TextView title_requests_count_hackvent = findViewById(R.id.title_requests_count_hackvent);

                                    title_requests_count_hackvent.setText("not started");

                                    TextView taskison = findViewById(R.id.title_weight_hackvent);
                                    taskison.setTypeface(type);
                                    taskison.setText("A/B");


                                }
                            }


                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                        }
                    });


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                }
            });
        }


        fchackvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder dialogBuilderhack = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);
                LayoutInflater inflater = getLayoutInflater();
                final View dialog = inflater.inflate(R.layout.dialog_hackvent_details, null);

                dialogBuilderhack.setView(dialog);
                dialogBuilderhack.setCancelable(false);

                final TextView localTextView1 = dialog.findViewById(R.id.basistit);
                final TextView localTextView2 = dialog.findViewById(R.id.textdescr);

                CircleImageView other = dialog.findViewById(R.id.hackventday);

                other.setBorderColor(getResources().getColor(R.color.hackvent_green));
                String ddtag;
                if (Integer.parseInt(theday) <= 9) {
                    ddtag = theday;
                } else {
                    ddtag = theday;
                }

                localTextView1.setTypeface(type);
                localTextView1.setText("DAY " + ddtag);


                localTextView2.setTypeface(type);
                final TextView hack = findViewById(R.id.title_to_address_hackvent);
                localTextView2.setText(introtext[0]);
                final String otheragenturl = "https://advent.missionday.info/_res/dayimg/2019_Day_" + ddtag + ".jpg";
                TabLayout tabLayout = dialog.findViewById(R.id.tasktablayout);

                tabLayout.clearOnTabSelectedListeners();
                Objects.requireNonNull(tabLayout.getTabAt(0)).select();
                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        if (tab.getPosition() == 0) {
                            localTextView2.setText(introtext[0]);
                        }
                        if (tab.getPosition() == 1) {
                            localTextView2.setText(taskA[0]);

                        }
                        if (tab.getPosition() == 2) {
                            localTextView2.setText(taskB[0]);
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });


                try {
                    PicassoCache.getPicassoInstance(getApplicationContext())
                            .load(otheragenturl)
                            .placeholder(R.drawable.hackvent)
                            .error(R.drawable.full_logo)

                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                            //.transform(new CropRoundTransformation())
                            .into(other);
                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());

                    e.printStackTrace();
                }

                dialogBuilderhack.setPositiveButton("close", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        localTextView2.setText(introtext[0]);


                        //  new SaveFriendScan().removeScannedAgent(FriendsMain.this, getApplicationContext(), friendID);


                    }
                });

                // {"status":"OK","update":{"result":80053,"value":80053,"msg":"Wonderful."}}

                if (hackventdaystarted[0]) {
                    if (taskis[0].equalsIgnoreCase("task1")) {
                        dialogBuilderhack.setNeutralButton("TASK A", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                localTextView2.setText(introtext[0]);
                                senddata("task1");

                            }
                        });
                    } else if (taskis[0].equalsIgnoreCase("task2")) {
                        dialogBuilderhack.setNeutralButton("TASK B", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                localTextView2.setText(introtext[0]);
                                senddata("task2");

                            }
                        });
                    }
                } else {

                    dialogBuilderhack.setNegativeButton("TASK B", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            localTextView2.setText(introtext[0]);
                            senddata("task2");

                        }
                    });

                    dialogBuilderhack.setNeutralButton("TASK A", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            localTextView2.setText(introtext[0]);
                            senddata("task1");

                        }
                    });
                }


                if (!isFinishing()) {


                    if (dialog.getParent() == null) {
                        dialogBuilderhack.create().show();
                    }


                }


            }
        });

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                frameloadhackvent.setVisibility(View.GONE);
                framecardhackvent.setVisibility(View.VISIBLE);


            }
        }, 1200);

    }

    private void initpvp(final DataSnapshot pvpkeys, final boolean haspvpfound) {


        final FlowLayout myRoot = findViewById(R.id.folding_cell_pvp);
        myRoot.removeAllViews();

        myRoot.setOrientation(LinearLayout.HORIZONTAL);
        // PROOF AGENT HAS PVPs
        if (haspvpfound) {

            if (!fcmini.isShown()) {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW, R.id.folding_cell_week);
                pvp_frame_base.setLayoutParams(params);

            } else {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.addRule(RelativeLayout.BELOW, R.id.folding_cell_mini);
                pvp_frame_base.setLayoutParams(params);
            }
            pvp_frame_base.setVisibility(View.VISIBLE);
            //GET PVP DETAILS
            int ispvpnr = 0;
            for (final DataSnapshot childpvp : pvpkeys.getChildren()) {

                if (!Boolean.parseBoolean(childpvp.child("finished").getValue().toString())) {
                    ispvpnr++;
                    // PVP KEY
                    Log.i("PVP keys -->", childpvp.getKey());
                    final boolean[] isowner = new boolean[1];
                    final boolean[] canstart = new boolean[1];
                    final boolean[] istarted = new boolean[1];
                    final boolean[] isfinished = new boolean[1];
                    final long[] jointime = new long[1];
                    final long[] startpvpvalue = new long[1];
                    final long[] currentpvpvalue = new long[1];
                    final long[] currentpvpstarttimestamp = new long[1];
                    final long[] lastupdatetimestamp = new long[1];
                    // LOAD DETAILS
                    final int finalIspvpnr = ispvpnr;
                    mDatabaserunnungpvp.child(childpvp.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot snapshotpvpdetails) {

                            final String pvpkey = childpvp.getKey();

                            // DETAILS PVP EXIST
                            if (snapshotpvpdetails.exists()) {
                                // HAS min 2 MEMBERS
                                if (snapshotpvpdetails.getChildrenCount() > 1) {

                                    // PROOF IF OWNER
                                    // AGENT IS OWNER
                                    //AGENT ISNT OWNER
                                    isowner[0] = Boolean.parseBoolean(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("owner").getValue()).toString());
                                    // HAS 2 or more Members
                                    // PROOF PVP IS FINISHED BY AGENT
                                    // PVP IS FINISHED
                                    if (Boolean.parseBoolean(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("finished").getValue()).toString())) {

                                        isfinished[0] = true;// TODO HIDE / NOT LOAD BECAUSE PVP IS FINISHED
                                    }
                                    // PVP IS NOT FINISHED
                                    else {
                                        isfinished[0] = false;
                                        // PROOF PVP IS STARTET BY AGENT
                                        // IS STARTED BY AGENT
                                        if (Boolean.parseBoolean(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("started").getValue()).toString())) {
                                            istarted[0] = true;
                                            canstart[0] = true;
                                        }
                                        // PVP NOT STARTED BY AGENT
                                        else {
                                            istarted[0] = false;
                                            canstart[0] = true; // TODO SET CAN START BUTTON BECAUSE 1 OR MORE AGENTS
                                        }
                                    }

                                    // GET JOINEDTIME


                                    jointime[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("timestamp").getValue()).toString());

                                    startpvpvalue[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("startvalue").getValue()).toString());
                                    currentpvpvalue[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("currentvalue").getValue()).toString());
                                    if (snapshotpvpdetails.child(user.getUid()).child("lastupdate").exists()) {
                                        lastupdatetimestamp[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("lastupdate").getValue()).toString());
                                    } else {
                                        lastupdatetimestamp[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("timestamp").getValue()).toString());
                                    }
                                    if (snapshotpvpdetails.child(user.getUid()).child("starttime").exists()) {
                                        currentpvpstarttimestamp[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("starttime").getValue()).toString());
                                    } else {
                                        currentpvpstarttimestamp[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("timestamp").getValue()).toString());
                                    }

                                    pvpcreateView(isowner[0], canstart[0], istarted[0], isfinished[0], pvpkey, myRoot, finalIspvpnr, jointime[0], currentpvpstarttimestamp[0], startpvpvalue[0], currentpvpvalue[0], lastupdatetimestamp[0], snapshotpvpdetails.getChildrenCount());
                                }
                                // HAS ONLY 1 MEMBER
                                else {

                                    mbpvp.child(childpvp.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot snapshotpvp) {
                                            if (Boolean.parseBoolean(snapshotpvp.child("finished").getValue().toString()) && !Boolean.parseBoolean(snapshotpvp.child("active").getValue().toString())) {
                                                //  isowner[0] = true; // TODO DELETE ACTION BY OWNER
                                                isfinished[0] = true;
                                                canstart[0] = false;
                                                istarted[0] = false;
                                            } else {
                                                isfinished[0] = false;
                                                canstart[0] = false;
                                                istarted[0] = false;
                                            }
                                            // PROOF IF OWNER
                                            // AGENT IS OWNER
                                            // TODO DELETE ACTION BY OWNER
                                            isowner[0] = Boolean.parseBoolean(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("owner").getValue()).toString());

                                            jointime[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("timestamp").getValue()).toString());

                                            startpvpvalue[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("startvalue").getValue()).toString());
                                            currentpvpvalue[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("currentvalue").getValue()).toString());
                                            if (snapshotpvpdetails.child(user.getUid()).child("lastupdate").exists()) {
                                                lastupdatetimestamp[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("lastupdate").getValue()).toString());
                                            } else {
                                                lastupdatetimestamp[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("timestamp").getValue()).toString());
                                            }
                                            if (snapshotpvpdetails.child(user.getUid()).child("starttime").exists()) {
                                                currentpvpstarttimestamp[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("starttime").getValue()).toString());
                                            } else {
                                                currentpvpstarttimestamp[0] = Long.parseLong(Objects.requireNonNull(snapshotpvpdetails.child(user.getUid()).child("timestamp").getValue()).toString());
                                            }
                                            pvpcreateView(isowner[0], canstart[0], istarted[0], isfinished[0], pvpkey, myRoot, finalIspvpnr, jointime[0], currentpvpstarttimestamp[0], startpvpvalue[0], currentpvpvalue[0], lastupdatetimestamp[0], snapshotpvpdetails.getChildrenCount());
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });


                                }


                            }
                            // PVP NOT EXIST
                            else {
                                // TODO PVP NOT EXIST
                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                        }
                    });
                }

                //  Log.i("TEST LOG--->",child.getValue().toString());

            }
        }
        // AGENT HAS NO PVPS
        else {
            pvp_frame_base.setVisibility(View.GONE);
            // TODO HIDE PVPS
        }
    }

    private void pvpcreateView(final boolean isowner, final boolean canstart, final boolean isstarted, final boolean isfinished, final String pvpkey, final FlowLayout myRoot, final int ispvpnr, final long jointime, final long startpvptime, final long startpvpvalue, final long currentpvpvalue, final long lastpvpupdate, final long numagents) {


        Log.i("PVP Actions --> ", " " + isowner + " key: " + pvpkey + " canstart: " + canstart + "isstarted " + isstarted);


        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        final View view = inflater.inflate(R.layout.pvp_title_layout, myRoot, false);
        final LinearLayout mypvplayout = view.findViewById(R.id.thepvplayout);


        final RelativeLayout frameloadpvp = view.findViewById(R.id.frameloadpvp);
        final LinearLayout framecardpvp = view.findViewById(R.id.framecardpvp);
        final AVLoadingIndicatorView avi_pvp = view.findViewById(R.id.avi_pvp);
        final RelativeLayout pvp_rightpart = view.findViewById(R.id.pvp_rightpart);
        final TextView notstartedpvpinfo = view.findViewById(R.id.notstartedpvpinfo);
        final TextView startedpvpinfo = view.findViewById(R.id.startedpvpinfo);
        final int[] pvpbadgeidint = {0};

        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
            avi_pvp.setIndicatorColor(getResources().getColor(R.color.colorENL, getTheme()));
            pvp_rightpart.setBackgroundColor(getResources().getColor(R.color.colorENL, getTheme()));
        } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
            avi_pvp.setIndicatorColor(getResources().getColor(R.color.colorRES, getTheme()));
            pvp_rightpart.setBackgroundColor(getResources().getColor(R.color.colorRES, getTheme()));
        } else {
            avi_pvp.setIndicatorColor(getResources().getColor(R.color.colorGray, getTheme()));
            pvp_rightpart.setBackgroundColor(getResources().getColor(R.color.colorGray, getTheme()));
        }


        frameloadpvp.setVisibility(View.VISIBLE);
        framecardpvp.setVisibility(View.GONE);

        myRoot.addView(view);

        mbpvp.child(pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshotpvpdata) {
                TextView title_from_address_pvp = view.findViewById(R.id.title_from_address_pvp);
                TextView title_to_address_pvp = view.findViewById(R.id.title_to_address_pvp);
                final TextView pvp_countown = view.findViewById(R.id.pvp_countown);
                final TextView pvp_runtimecountown = view.findViewById(R.id.pvp_runtimecountown);
                final RelativeLayout pvpstartet = view.findViewById(R.id.pvpstartet);
                final RelativeLayout pvpnotstartet = view.findViewById(R.id.pvpnotstartet);
                final FancyButton title_start_btn_pvp = view.findViewById(R.id.title_start_btn_pvp);
                final FancyButton title_update_btn_pvp = view.findViewById(R.id.title_update_btn_pvp);

                final FancyButton title_delet_btn_pvp = view.findViewById(R.id.title_delet_btn_pvp);
                final FancyButton title_reshare_btn_pvp = view.findViewById(R.id.title_reshare_btn_pvp);
                final FancyButton title_giveup_btn_pvp = view.findViewById(R.id.title_giveup_btn_pvp);
                final FancyButton title_details_btn_pvp = view.findViewById(R.id.title_details_btn_pvp);
                final FancyButton bt_detailspvp = view.findViewById(R.id.bt_detailspvp);

                CircleImageView title_price_pvp = view.findViewById(R.id.title_price_pvp);
                final CircleImageView title_price_pvp_one = view.findViewById(R.id.title_price_pvp_one);
                final CircleImageView title_price_pvp_two = view.findViewById(R.id.title_price_pvp_two);
                final CircleImageView title_price_pvp_three = view.findViewById(R.id.title_price_pvp_three);
                final RelativeLayout frameone = view.findViewById(R.id.frameone);
                final RelativeLayout frametwo = view.findViewById(R.id.frametwo);
                final RelativeLayout framethree = view.findViewById(R.id.framethree);
                TextView pvp_you = view.findViewById(R.id.pvp_you);
                final TextView pvp_name_one = view.findViewById(R.id.pvp_name_one);
                final TextView pvp_name_two = view.findViewById(R.id.pvp_name_two);
                final TextView pvp_name_three = view.findViewById(R.id.pvp_name_three);
                final ImageView title_from_to_dots_pvp = view.findViewById(R.id.title_from_to_dots_pvp);
                final boolean pvpactive = Boolean.parseBoolean(snapshotpvpdata.child("active").getValue().toString());
                final boolean pvpfinished = Boolean.parseBoolean(snapshotpvpdata.child("finished").getValue().toString());

                final String pvpbadgeid = snapshotpvpdata.child("badgeid").getValue().toString();
                final String pvptitle = snapshotpvpdata.child("pvptitle").getValue().toString();

                mDatabaseaod.child("badge").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshotbadgedata) {
                        int badgeid = getResources().getIdentifier(snapshotbadgedata.child(snapshotpvpdata.child("badgeid").getValue().toString()).child("drawablegold").getValue().toString(), "drawable", getPackageName());

                        //String badgefield = snapshotbadgedata.child(pvpbadgeid).child("field").getValue().toString();
                        //badgemonth = getResources().obtainTypedArray(badgemonthid);
                        if (badgeid != 0) {
                            // Log.w("BADGE -->", " id :" + badgeid);
                            pvpbadgeidint[0] = badgeid;
                            title_from_to_dots_pvp.setImageDrawable(getDrawable(badgeid));
                        }

                        String[] action_text2 = getResources().getStringArray(R.array.badge_text_actions_id);
                        int action_value2 = Arrays.asList(action_text2).indexOf(snapshotbadgedata.child(snapshotpvpdata.child("badgeid").getValue().toString()).child("id").getValue().toString()); // pass value
                        final String action_value_text_community2 = getResources().getStringArray(R.array.badge_text_actions_value)[action_value2];

                        if (isstarted) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                startedpvpinfo.setText(Html.fromHtml(String.format(getResources().getString(R.string.challange_txt_short_details), action_value_text_community2, snapshotbadgedata.child(snapshotpvpdata.child("badgeid").getValue().toString()).child("short1").getValue().toString(), snapshotpvpdata.child("runtime_text").getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                startedpvpinfo.setText(Html.fromHtml(String.format(getResources().getString(R.string.challange_txt_short_details), action_value_text_community2, snapshotbadgedata.child(snapshotpvpdata.child("badgeid").getValue().toString()).child("short1").getValue().toString(), snapshotpvpdata.child("runtime_text").getValue().toString())));

                            }
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                notstartedpvpinfo.setText(Html.fromHtml(String.format(getResources().getString(R.string.challange_txt_short), action_value_text_community2, snapshotbadgedata.child(snapshotpvpdata.child("badgeid").getValue().toString()).child("short1").getValue().toString(), snapshotpvpdata.child("runtime_text").getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                notstartedpvpinfo.setText(Html.fromHtml(String.format(getResources().getString(R.string.challange_txt_short), action_value_text_community2, snapshotbadgedata.child(snapshotpvpdata.child("badgeid").getValue().toString()).child("short1").getValue().toString(), snapshotpvpdata.child("runtime_text").getValue().toString())));

                            }
                        }

                        mypvplayout.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
                        mypvplayout.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;

                        ViewTreeObserver vto = pvpnotstartet.getViewTreeObserver();
                        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                //Log.i("PVP Actions --> ", " "+isowner+" key: "+pvpkey+ " canstart: "+canstart);
                                //      Log.i("POMMES 5465 -->", " Fix high "+isowner+" key: "+pvpkey+ " canstart: "+canstart);
                                framecardpvp.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                                int height = mypvplayout.getMeasuredHeight();
                                //  pvpnotstartet.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) framecardpvp.getLayoutParams();
                                params.height = height + 25;
                                framecardpvp.setLayoutParams(params);
                                // framecardpvp.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                //framecardpvp.invalidate();
                                //pvpnotstartet.invalidate();
                            }
                        });


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {


                    }
                });


                // title_from_to_dots_pvp
                if (isstarted) {
                    pvpstartet.setVisibility(View.VISIBLE);
                    pvpnotstartet.setVisibility(View.GONE);
                    pvp_you.setBackgroundResource(R.drawable.round_bg_started);


                } else {
                    pvpstartet.setVisibility(View.GONE);
                    pvpnotstartet.setVisibility(View.VISIBLE);
                    pvp_you.setBackgroundResource(R.drawable.round_bg_invite);
                }


                if (isowner) {
                    title_delet_btn_pvp.setVisibility(View.VISIBLE);
                    title_delet_btn_pvp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (listenerpvpjoin != null) {
                                mbpvp.removeEventListener(listenerpvpjoin);
                                listenerpvpjoin = null;
                            }
                            if (ispvpnr == 1) {
                                if (timer_pvp_one != null) {
                                    timer_pvp_one.cancel();
                                }
                            }
                            if (ispvpnr == 2) {
                                if (timer_pvp_two != null) {
                                    timer_pvp_two.cancel();
                                }
                            }
                            if (ispvpnr == 3) {
                                if (timer_pvp_three != null) {
                                    timer_pvp_three.cancel();
                                }
                            }
                            deletePvP(pvpkey, snapshotpvpdata.child("art").getValue().toString(), true);
                        }
                    });

                    try {
                        PicassoCache.getPicassoInstance(getApplicationContext())
                                .load(snapshotpvpdata.child("createrimage").getValue().toString())
                                .placeholder(R.drawable.full_logo)
                                .error(R.drawable.full_logo)


                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(title_price_pvp);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }


                    pvp_you.setText(snapshotpvpdata.child("creater").getValue().toString());
                    pvp_you.setSelected(true);

                    // IS INVITE LINK
                    if (snapshotpvpdata.child("art").getValue().toString().equalsIgnoreCase("linkshare")) {

                        // SET ONE TWO THREE
                        // TODO SET IMAGES JOIND
                        mDatabaserunnungpvp.child(pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {

                            @Override
                            public void onDataChange(DataSnapshot snapshotrunning) {
                                int joindagents = 0;

                                if (joindagents == 0) {
                                    try {
                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                .load(R.drawable.ic_filter_1)
                                                .placeholder(R.drawable.full_logo)
                                                .error(R.drawable.full_logo)


                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                //.transform(new CropRoundTransformation())
                                                .into(title_price_pvp_one);
                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                    } catch (Exception e) {
                                        //Log.e("Error", e.getMessage());

                                        e.printStackTrace();
                                    }

                                    pvp_name_one.setText("invite with link");
                                    pvp_name_one.setSelected(true);
                                    frameone.setVisibility(View.VISIBLE);
                                }
                                for (final DataSnapshot invitedagents : snapshotrunning.getChildren()) {

                                    //   Log.i("INVITES --> ", " pommes  x6 " + invitedagents.getKey());

                                    if (joindagents == 0) {
                                        try {
                                            PicassoCache.getPicassoInstance(getApplicationContext())
                                                    .load(R.drawable.ic_filter_1)
                                                    .placeholder(R.drawable.full_logo)
                                                    .error(R.drawable.full_logo)


                                                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                    //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                    //.transform(new CropRoundTransformation())
                                                    .into(title_price_pvp_one);
                                            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                        } catch (Exception e) {
                                            //Log.e("Error", e.getMessage());

                                            e.printStackTrace();
                                        }

                                        pvp_name_one.setText("invite with link");
                                        pvp_name_one.setSelected(true);
                                        frameone.setVisibility(View.VISIBLE);
                                    }

                                    if (!invitedagents.getKey().equalsIgnoreCase(user.getUid())) {
                                        joindagents++;

                                        if (joindagents == 1) {
                                            // AGENT ONE
                                            mDatabase.child(invitedagents.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {

                                                @Override
                                                public void onDataChange(DataSnapshot snapshotimage) {
                                                    // snapshotagentimage.getValue().toString() + "?sz=200"
                                                    try {
                                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                                .load(snapshotimage.getValue().toString() + "?sz=80")
                                                                .placeholder(R.drawable.full_logo)
                                                                .error(R.drawable.full_logo)


                                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                //.transform(new CropRoundTransformation())
                                                                .into(title_price_pvp_one);
                                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                    } catch (Exception e) {
                                                        //Log.e("Error", e.getMessage());

                                                        e.printStackTrace();
                                                    }


                                                    mDatabase.child(invitedagents.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {

                                                        @Override
                                                        public void onDataChange(DataSnapshot snapshotagent) {
                                                            // snapshotagentimage.getValue().toString() + "?sz=200"

                                                            pvp_name_one.setText(snapshotagent.getValue().toString());
                                                            pvp_name_one.setSelected(true);
                                                            frameone.setVisibility(View.VISIBLE);

                                                        }


                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        }
                                                    });


                                                }


                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                }
                                            });

                                        }

                                        if (joindagents == 2) {
                                            // AGENT TWO
                                            mDatabase.child(invitedagents.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {

                                                @Override
                                                public void onDataChange(DataSnapshot snapshotimage) {
                                                    // snapshotagentimage.getValue().toString() + "?sz=200"
                                                    try {
                                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                                .load(snapshotimage.getValue().toString() + "?sz=80")
                                                                .placeholder(R.drawable.full_logo)
                                                                .error(R.drawable.full_logo)


                                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                //.transform(new CropRoundTransformation())
                                                                .into(title_price_pvp_two);
                                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                    } catch (Exception e) {
                                                        //Log.e("Error", e.getMessage());

                                                        e.printStackTrace();
                                                    }


                                                    mDatabase.child(invitedagents.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {

                                                        @Override
                                                        public void onDataChange(DataSnapshot snapshotagent) {
                                                            // snapshotagentimage.getValue().toString() + "?sz=200"

                                                            pvp_name_two.setText(snapshotagent.getValue().toString());
                                                            pvp_name_two.setSelected(true);
                                                            frametwo.setVisibility(View.VISIBLE);

                                                        }


                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        }
                                                    });


                                                }


                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                }
                                            });
                                        }

                                        if (joindagents == 3) {
                                            // THREE
                                            mDatabase.child(invitedagents.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {

                                                @Override
                                                public void onDataChange(DataSnapshot snapshotimage) {
                                                    // snapshotagentimage.getValue().toString() + "?sz=200"
                                                    try {
                                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                                .load(snapshotimage.getValue().toString() + "?sz=80")
                                                                .placeholder(R.drawable.full_logo)
                                                                .error(R.drawable.full_logo)


                                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                //.transform(new CropRoundTransformation())
                                                                .into(title_price_pvp_three);
                                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                    } catch (Exception e) {
                                                        //Log.e("Error", e.getMessage());

                                                        e.printStackTrace();
                                                    }


                                                    mDatabase.child(invitedagents.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {

                                                        @Override
                                                        public void onDataChange(DataSnapshot snapshotagent) {
                                                            // snapshotagentimage.getValue().toString() + "?sz=200"

                                                            pvp_name_three.setText(snapshotagent.getValue().toString());
                                                            pvp_name_three.setSelected(true);
                                                            framethree.setVisibility(View.VISIBLE);


                                                            mypvplayout.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
                                                            mypvplayout.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;

                                                            ViewTreeObserver vto = pvpnotstartet.getViewTreeObserver();
                                                            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                                                @Override
                                                                public void onGlobalLayout() {
                                                                    //Log.i("PVP Actions --> ", " "+isowner+" key: "+pvpkey+ " canstart: "+canstart);
                                                                    //    Log.i("POMMES 5465rowner -->", " Fix high "+isowner+" key: "+pvpkey+ " canstart: "+canstart);
                                                                    framecardpvp.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                                                                    int height = mypvplayout.getMeasuredHeight();
                                                                    //  pvpnotstartet.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) framecardpvp.getLayoutParams();
                                                                    params.height = height + 25;
                                                                    framecardpvp.setLayoutParams(params);
                                                                    // framecardpvp.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                                                    //framecardpvp.invalidate();
                                                                    //pvpnotstartet.invalidate();
                                                                }
                                                            });

                                                        }


                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        }
                                                    });


                                                }


                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                }
                                            });
                                        }


                                    }


                                    //  Log.i("TEST LOG--->",child.getValue().toString());

                                }


                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            }
                        });


                        title_reshare_btn_pvp.setVisibility(View.VISIBLE);
                        title_reshare_btn_pvp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText("PvP invite link", "https://aod.prxenon.rocks/pvp/AODs_" + pvpkey);
                                clipboard.setPrimaryClip(clip);

                                StyleableToast.makeText(getApplicationContext(), "PvP invite link copied to clipboard", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                Intent shareIntent = ShareCompat.IntentBuilder.from(AODMain.this)
                                        .setText("PvP invite from " + AgentData.getagentname() + "\nhttps://aod.prxenon.rocks/pvp/AODs_" + pvpkey)
                                        .setChooserTitle("Share this PvP with...")
                                        .setType("text/plain")
                                        .getIntent()
                                        .setAction(android.content.Intent.ACTION_SEND);

                                startActivityForResult(shareIntent, 392);


                            }
                        });
                    } else {
                        // SET INVITED AGENTS

                        for (DataSnapshot invitedagents : snapshotpvpdata.child("agents").getChildren()) {

                            //  Log.i("INVITES --> ", " pommes x5 " + invitedagents.getKey());
                            if (invitedagents.getKey().equalsIgnoreCase("1")) {
                                // AGENT ONE
                                try {
                                    PicassoCache.getPicassoInstance(getApplicationContext())
                                            .load(invitedagents.child("agentimage").getValue().toString())
                                            .placeholder(R.drawable.full_logo)
                                            .error(R.drawable.full_logo)


                                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                            //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                            //.transform(new CropRoundTransformation())
                                            .into(title_price_pvp_one);
                                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                } catch (Exception e) {
                                    //Log.e("Error", e.getMessage());

                                    e.printStackTrace();
                                }

                                pvp_name_one.setText(invitedagents.child("agentname").getValue().toString());
                                pvp_name_one.setSelected(true);
                                frameone.setVisibility(View.VISIBLE);


                            }

                            if (invitedagents.getKey().equalsIgnoreCase("2")) {
                                // AGENT ONE
                                try {
                                    PicassoCache.getPicassoInstance(getApplicationContext())
                                            .load(invitedagents.child("agentimage").getValue().toString())
                                            .placeholder(R.drawable.full_logo)
                                            .error(R.drawable.full_logo)


                                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                            //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                            //.transform(new CropRoundTransformation())
                                            .into(title_price_pvp_two);
                                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                } catch (Exception e) {
                                    //Log.e("Error", e.getMessage());

                                    e.printStackTrace();
                                }

                                pvp_name_two.setText(invitedagents.child("agentname").getValue().toString());
                                pvp_name_two.setSelected(true);
                                frametwo.setVisibility(View.VISIBLE);


                            }

                            if (invitedagents.getKey().equalsIgnoreCase("3")) {
                                // AGENT ONE
                                try {
                                    PicassoCache.getPicassoInstance(getApplicationContext())
                                            .load(invitedagents.child("agentimage").getValue().toString())
                                            .placeholder(R.drawable.full_logo)
                                            .error(R.drawable.full_logo)


                                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                            //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                            //.transform(new CropRoundTransformation())
                                            .into(title_price_pvp_three);
                                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                } catch (Exception e) {
                                    //Log.e("Error", e.getMessage());

                                    e.printStackTrace();
                                }

                                pvp_name_three.setText(invitedagents.child("agentname").getValue().toString());
                                pvp_name_three.setSelected(true);
                                framethree.setVisibility(View.VISIBLE);


                            }

                            //  Log.i("TEST LOG--->",child.getValue().toString());

                        }
                    }


                    if (isfinished && !canstart) {
                        title_start_btn_pvp.setEnabled(false);
                        title_start_btn_pvp.setIconResource(R.drawable.ic_hourglass_empty);
                        title_start_btn_pvp.setText("Opponent has given up 1");
                        bt_detailspvp.setVisibility(View.GONE);
                        title_giveup_btn_pvp.setVisibility(View.GONE);
                        title_delet_btn_pvp.setVisibility(View.GONE);
                        title_reshare_btn_pvp.setVisibility(View.GONE);
                        // TODO MaYBE I HAVE STARTET

                    }


                    if (!pvpactive || !pvpfinished) {
                        title_start_btn_pvp.setEnabled(false);
                        title_start_btn_pvp.setIconResource(R.drawable.ic_hourglass_empty);


                        pvpnotstartet.setVisibility(View.VISIBLE);
                        pvpstartet.setVisibility(View.GONE);
                    }

                } else {
                    title_delet_btn_pvp.setVisibility(View.GONE);
                    title_reshare_btn_pvp.setVisibility(View.GONE);


                    // SET IMAGE TOP

                    try {
                        PicassoCache.getPicassoInstance(getApplicationContext())
                                .load(user.getProviderData().get(1).getPhotoUrl().toString())
                                .placeholder(R.drawable.full_logo)
                                .error(R.drawable.full_logo)


                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(title_price_pvp);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }

                    pvp_you.setText(AgentData.getagentname());
                    pvp_you.setSelected(true);


                    // IS INVITE LINK
                    if (snapshotpvpdata.child("art").getValue().toString().equalsIgnoreCase("linkshare")) {

                        // SET ONE TWO THREE
                        // TODO SET IMAGES JOIND
                        mDatabaserunnungpvp.child(pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {

                            @Override
                            public void onDataChange(DataSnapshot snapshotrunning) {
                                int joindagents = 0;
                                for (final DataSnapshot invitedagents : snapshotrunning.getChildren()) {

                                    //    Log.i("INVITES --> ", " pommes x4" + invitedagents.getKey());

                                    if (!invitedagents.getKey().equalsIgnoreCase(user.getUid())) {
                                        joindagents++;

                                        if (joindagents == 1) {
                                            // AGENT ONE
                                            mDatabase.child(invitedagents.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {

                                                @Override
                                                public void onDataChange(DataSnapshot snapshotimage) {
                                                    // snapshotagentimage.getValue().toString() + "?sz=200"
                                                    try {
                                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                                .load(snapshotimage.getValue().toString() + "?sz=80")
                                                                .placeholder(R.drawable.full_logo)
                                                                .error(R.drawable.full_logo)


                                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                //.transform(new CropRoundTransformation())
                                                                .into(title_price_pvp_one);
                                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                    } catch (Exception e) {
                                                        //Log.e("Error", e.getMessage());

                                                        e.printStackTrace();
                                                    }


                                                    mDatabase.child(invitedagents.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {

                                                        @Override
                                                        public void onDataChange(DataSnapshot snapshotagent) {
                                                            // snapshotagentimage.getValue().toString() + "?sz=200"

                                                            pvp_name_one.setText(snapshotagent.getValue().toString());
                                                            pvp_name_one.setSelected(true);
                                                            frameone.setVisibility(View.VISIBLE);

                                                        }


                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        }
                                                    });


                                                }


                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                }
                                            });

                                        }

                                        if (joindagents == 2) {
                                            // AGENT TWO
                                            mDatabase.child(invitedagents.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {

                                                @Override
                                                public void onDataChange(DataSnapshot snapshotimage) {
                                                    // snapshotagentimage.getValue().toString() + "?sz=200"
                                                    try {
                                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                                .load(snapshotimage.getValue().toString() + "?sz=80")
                                                                .placeholder(R.drawable.full_logo)
                                                                .error(R.drawable.full_logo)


                                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                //.transform(new CropRoundTransformation())
                                                                .into(title_price_pvp_two);
                                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                    } catch (Exception e) {
                                                        //Log.e("Error", e.getMessage());

                                                        e.printStackTrace();
                                                    }


                                                    mDatabase.child(invitedagents.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {

                                                        @Override
                                                        public void onDataChange(DataSnapshot snapshotagent) {
                                                            // snapshotagentimage.getValue().toString() + "?sz=200"

                                                            pvp_name_two.setText(snapshotagent.getValue().toString());
                                                            pvp_name_two.setSelected(true);
                                                            frametwo.setVisibility(View.VISIBLE);

                                                        }


                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        }
                                                    });


                                                }


                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                }
                                            });
                                        }

                                        if (joindagents == 3) {
                                            // THREE
                                            mDatabase.child(invitedagents.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {

                                                @Override
                                                public void onDataChange(DataSnapshot snapshotimage) {
                                                    // snapshotagentimage.getValue().toString() + "?sz=200"
                                                    try {
                                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                                .load(snapshotimage.getValue().toString() + "?sz=80")
                                                                .placeholder(R.drawable.full_logo)
                                                                .error(R.drawable.full_logo)


                                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                //.transform(new CropRoundTransformation())
                                                                .into(title_price_pvp_three);
                                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                    } catch (Exception e) {
                                                        //Log.e("Error", e.getMessage());

                                                        e.printStackTrace();
                                                    }


                                                    mDatabase.child(invitedagents.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {

                                                        @Override
                                                        public void onDataChange(DataSnapshot snapshotagent) {
                                                            // snapshotagentimage.getValue().toString() + "?sz=200"

                                                            pvp_name_three.setText(snapshotagent.getValue().toString());
                                                            pvp_name_three.setSelected(true);
                                                            framethree.setVisibility(View.VISIBLE);
                                                            mypvplayout.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
                                                            mypvplayout.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;

                                                            ViewTreeObserver vto = pvpnotstartet.getViewTreeObserver();
                                                            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                                                @Override
                                                                public void onGlobalLayout() {
                                                                    //Log.i("PVP Actions --> ", " "+isowner+" key: "+pvpkey+ " canstart: "+canstart);
                                                                    //   Log.i("POMMES 5465noowner -->", " Fix high "+isowner+" key: "+pvpkey+ " canstart: "+canstart);
                                                                    framecardpvp.getViewTreeObserver().removeGlobalOnLayoutListener(this);

                                                                    int height = mypvplayout.getMeasuredHeight();
                                                                    //  pvpnotstartet.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) framecardpvp.getLayoutParams();
                                                                    params.height = height + 35;
                                                                    framecardpvp.setLayoutParams(params);
                                                                    // framecardpvp.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
                                                                    //framecardpvp.invalidate();
                                                                    //pvpnotstartet.invalidate();
                                                                }
                                                            });
                                                        }


                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        }
                                                    });


                                                }


                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                }
                                            });
                                        }


                                    }


                                    //  Log.i("TEST LOG--->",child.getValue().toString());

                                }


                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            }
                        });
                    } else {
                        // SET INVITED AGENTS


                        for (DataSnapshot invitedagents : snapshotpvpdata.child("agents").getChildren()) {

                            // Log.i("INVITES --> ", " pommes xc" + invitedagents.getKey());


                            if (invitedagents.getKey().equalsIgnoreCase("1")) {
                                // AGENT ONE

                                if (invitedagents.child("agentname").getValue().toString().equalsIgnoreCase(AgentData.getagentname())) {
                                    try {
                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                .load(snapshotpvpdata.child("createrimage").getValue().toString())
                                                .placeholder(R.drawable.full_logo)
                                                .error(R.drawable.full_logo)


                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                //.transform(new CropRoundTransformation())
                                                .into(title_price_pvp_one);
                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                    } catch (Exception e) {
                                        //Log.e("Error", e.getMessage());

                                        e.printStackTrace();
                                    }

                                    pvp_name_one.setText(snapshotpvpdata.child("creater").getValue().toString());
                                    pvp_name_one.setSelected(true);
                                    frameone.setVisibility(View.VISIBLE);
                                } else {
                                    try {
                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                .load(invitedagents.child("agentimage").getValue().toString())
                                                .placeholder(R.drawable.full_logo)
                                                .error(R.drawable.full_logo)


                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                //.transform(new CropRoundTransformation())
                                                .into(title_price_pvp_one);
                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                    } catch (Exception e) {
                                        //Log.e("Error", e.getMessage());

                                        e.printStackTrace();
                                    }

                                    pvp_name_one.setText(invitedagents.child("agentname").getValue().toString());
                                    pvp_name_one.setSelected(true);
                                    frameone.setVisibility(View.VISIBLE);
                                }

                            }

                            if (invitedagents.getKey().equalsIgnoreCase("2")) {
                                // AGENT ONE
                                if (invitedagents.child("agentname").getValue().toString().equalsIgnoreCase(AgentData.getagentname())) {
                                    try {
                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                .load(snapshotpvpdata.child("createrimage").getValue().toString())
                                                .placeholder(R.drawable.full_logo)
                                                .error(R.drawable.full_logo)


                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                //.transform(new CropRoundTransformation())
                                                .into(title_price_pvp_two);
                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                    } catch (Exception e) {
                                        //Log.e("Error", e.getMessage());

                                        e.printStackTrace();
                                    }

                                    pvp_name_two.setText(snapshotpvpdata.child("creater").getValue().toString());
                                    pvp_name_two.setSelected(true);
                                    frametwo.setVisibility(View.VISIBLE);
                                } else {
                                    try {
                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                .load(invitedagents.child("agentimage").getValue().toString())
                                                .placeholder(R.drawable.full_logo)
                                                .error(R.drawable.full_logo)


                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                //.transform(new CropRoundTransformation())
                                                .into(title_price_pvp_two);
                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                    } catch (Exception e) {
                                        //Log.e("Error", e.getMessage());

                                        e.printStackTrace();
                                    }

                                    pvp_name_two.setText(invitedagents.child("agentname").getValue().toString());
                                    pvp_name_two.setSelected(true);
                                    frametwo.setVisibility(View.VISIBLE);
                                }


                            }

                            if (invitedagents.getKey().equalsIgnoreCase("3")) {
                                // AGENT ONE
                                if (invitedagents.child("agentname").getValue().toString().equalsIgnoreCase(AgentData.getagentname())) {
                                    try {
                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                .load(snapshotpvpdata.child("createrimage").getValue().toString())
                                                .placeholder(R.drawable.full_logo)
                                                .error(R.drawable.full_logo)


                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                //.transform(new CropRoundTransformation())
                                                .into(title_price_pvp_three);
                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                    } catch (Exception e) {
                                        //Log.e("Error", e.getMessage());

                                        e.printStackTrace();
                                    }

                                    pvp_name_three.setText(snapshotpvpdata.child("creater").getValue().toString());
                                    pvp_name_three.setSelected(true);
                                    framethree.setVisibility(View.VISIBLE);
                                } else {
                                    try {
                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                .load(invitedagents.child("agentimage").getValue().toString())
                                                .placeholder(R.drawable.full_logo)
                                                .error(R.drawable.full_logo)


                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                                //.transform(new CropRoundTransformation())
                                                .into(title_price_pvp_three);
                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                    } catch (Exception e) {
                                        //Log.e("Error", e.getMessage());

                                        e.printStackTrace();
                                    }

                                    pvp_name_three.setText(invitedagents.child("agentname").getValue().toString());
                                    pvp_name_three.setSelected(true);
                                    framethree.setVisibility(View.VISIBLE);
                                }


                            }

                            //  Log.i("TEST LOG--->",child.getValue().toString());

                        }
                    }


                }

                // START FOR OWNER AND AGENTS
                if (canstart) {
                    // CAN START IS NOT STARTED
                    if (!isstarted) {
                        bt_detailspvp.setVisibility(View.GONE);
                        title_delet_btn_pvp.setVisibility(View.GONE);
                        title_reshare_btn_pvp.setVisibility(View.GONE);
                        if (!title_start_btn_pvp.isEnabled()) {
                            title_start_btn_pvp.setEnabled(true);
                        } else {
                            title_start_btn_pvp.setEnabled(true);
                        }
                        title_start_btn_pvp.setIconResource(R.drawable.ic_directions_run);
                        title_start_btn_pvp.setText("start the PvP");
                        pvp_countown.setVisibility(View.VISIBLE);
                        title_giveup_btn_pvp.setVisibility(View.VISIBLE);


                    } else {
                        final TextRoundCornerProgressBar pvp_progress_own = view.findViewById(R.id.pvp_progress_own);
                        final TextRoundCornerProgressBar pvp_progress_one = view.findViewById(R.id.pvp_progress_one);
                        final TextRoundCornerProgressBar pvp_progress_two = view.findViewById(R.id.pvp_progress_two);
                        final TextRoundCornerProgressBar pvp_progress_three = view.findViewById(R.id.pvp_progress_three);


                        bt_detailspvp.setVisibility(View.VISIBLE);

                        bt_detailspvp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Log.i("Will Open Details->", " 6786");


                                // startscreenshotpvp("pvp", pvpkey, pvpbadgeid, pvptitle, pvpbadgeidint[0]);


                                Intent ipvpdetails = new Intent(getBaseContext(), PvPDetails.class);
                                ipvpdetails.putExtra("finalTheresult", -1);
                                ipvpdetails.putExtra("onactionid", pvpkey);
                                ipvpdetails.putExtra("missionart", "pvp");
                                ipvpdetails.putExtra("finalResultis", -1);
                                ipvpdetails.putExtra("finalIslagenumber", false);
                                ipvpdetails.putExtra("badgestatus", pvpbadgeidint[0]);


                                startActivity(ipvpdetails);


                                // ENDE START PVP
                            }
                        });


                        title_details_btn_pvp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Log.i("Will Update!->", " 6786");


                                if (isStoragePermissionGranted()) {
                                    Log.v(TAG, "Permission is granted");
                                    //File write logic here
                                    if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                                        // mExplosionField.clear();
                                        if (isMyServiceRunning(OverlayScreenService.class)) {
                                            Log.i("SERVICESTATUS", "running");
                                            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                                            stopService(svc);
                                        } else {

                                            mDatabaseaod.child("badge").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot snapshotbadgedata) {

                                                    String badgefield = snapshotbadgedata.child(pvpbadgeid).child("field").getValue().toString();

                                                    Log.i("ÜBERGABE ---> ", " " + pvpbadgeidint[0]);
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                        if (Settings.canDrawOverlays(AODMain.this)) {

                                                            startscreenshotpvp("pvp", pvpkey, badgefield, pvptitle, pvpbadgeidint[0]);
                                                            // startActivityForResult(intent , 7002);
                                                        } else {
                                                            verifyOverlay(AODMain.this);
                                                        }
                                                    } else {
                                                        startscreenshotpvp("pvp", pvpkey, badgefield, pvptitle, pvpbadgeidint[0]);
                                                        //   startActivityForResult(intent , 7002);
                                                    }

                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                }
                                            });


                                        }
                                    } else {
                                        if (isStoragePermissionGranted()) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                            // builder.setMessage("Text not finished...");
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                            builder.setView(dialog);
                                            final TextView input = dialog.findViewById(R.id.caltext);
                                            // input.setAllCaps(true);
                                            input.setTypeface(type);

                                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                            //localTextView1.setTypeface(proto);
                                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    // Log.i("Hallo", "hallo");
                                                    //handleinputcode(input.getText().toString());
                                                    // Todo publish the code to database
                                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                    //i.putExtra("PersonID", personID);
                                                    startActivity(i);
                                                    //finish();

                                                }
                                            });

                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {


                                                    // Todo publish the code to database
                                                }
                                            });

                                            builder.setCancelable(true);

                                            if (!isFinishing()) {
                                                builder.create().show();

                                            }
                                        }
                                    }
                                }


                                // ENDE START PVP
                            }
                        });
                        pvp_countown.setVisibility(View.GONE);

                        View childown = pvp_progress_own;

                        TextView textown = childown.findViewById(R.id.tv_progress);

                        if (currentpvpvalue <= 0) {
                            pvp_progress_own.setProgress(5);
                            pvp_progress_own.setMax(100);
                        }


                        textown.setEllipsize(TextUtils.TruncateAt.END);
                        textown.setMaxLines(1);
                        textown.setSingleLine(true);
                        textown.setSelected(true);
                        pvp_progress_own.setProgressText(AgentData.getagentname());

                        View child1 = pvp_progress_one;
                        View child2 = pvp_progress_two;
                        View child3 = pvp_progress_three;
                        final TextView textone = child1.findViewById(R.id.tv_progress);
                        final TextView texttwo = child2.findViewById(R.id.tv_progress);
                        final TextView textthree = child3.findViewById(R.id.tv_progress);


                        Log.i("NUM AGENTS -->", " -->" + numagents);

                        // GET STATUS Of EACH AGENT
                        mbpvp.child(pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {

                            @Override
                            public void onDataChange(final DataSnapshot snapshotrunningpvpstatus) {

                                if (Boolean.parseBoolean(snapshotrunningpvpstatus.child("active").getValue().toString())) {

                                    mDatabaserunnungpvp.child(pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {
                                        long valone = 0;
                                        long valtwo = 0;
                                        long valthree = 0;
                                        int detailsagents = 1;
                                        final long hoursbetween = 0;
                                        long hoursbetweenone = 0;
                                        long agentstartettimeone = 0;
                                        long agentstartettimetwo = 0;
                                        long agentstartettimethree = 0;
                                        long hoursbetweentwo = 0;
                                        long hoursbetweenthree = 0;
                                        final long pvpruntime = Long.parseLong(snapshotrunningpvpstatus.child("runtime").getValue().toString()) * 1000;
                                        boolean pone = false;
                                        boolean ptwo = false;
                                        boolean pthree = false;

                                        final ArrayList<Long> arrayListnums = new ArrayList<Long>();

                                        @Override
                                        public void onDataChange(final DataSnapshot snapshotrunningagentdetails) {

                                            final long agentstartown = Long.parseLong(Objects.requireNonNull(snapshotrunningagentdetails.child(user.getUid()).child("starttime").getValue()).toString());
                                            final boolean ownfinished = Boolean.parseBoolean(snapshotrunningagentdetails.child(user.getUid()).child("finished").getValue().toString());
                                            for (final DataSnapshot agentpvpdetails : snapshotrunningagentdetails.getChildren()) {

                                                //  Log.i("DETAILS --> ", " pommes x12 " + agentpvpdetails.getKey() + " count = " + snapshotrunningagentdetails.getChildrenCount());


                                                if (!agentpvpdetails.getKey().equalsIgnoreCase(user.getUid())) {


                                                    mDatabase.child(agentpvpdetails.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot snapshotagent) {
                                                            detailsagents++;
                                                            //    Log.i("ROUNDED -->", " num " + detailsagents);

                                                            if (detailsagents == 2) {
                                                                valone = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("currentvalue").getValue().toString());
                                                                //         Log.i("ROUNDED 5667-->", " num " + valone);
                                                                pvp_progress_one.setProgressText(snapshotagent.getValue().toString());
                                                                if (Boolean.parseBoolean(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("started").getValue().toString())) {
                                                                    long agentstartone = Long.parseLong(Objects.requireNonNull(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("starttime").getValue()).toString());

                                                                    hoursbetweenone = agentstartown - agentstartone;
                                                                    agentstartettimeone = (agentstartone + pvpruntime) - System.currentTimeMillis();


                                                                    Log.i("DIFF IS Finished -->", " c " + agentstartettimeone);
                                                                    if (hoursbetweenone >= -900000 && agentstartettimeone >= 450000) {
                                                                        pone = true;
                                                                        arrayListnums.add(valone);
                                                                        pvp_progress_one.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valone))) + ")");
                                                                    } else {
                                                                        valone = 5;
                                                                        arrayListnums.add(Long.parseLong("1"));
                                                                        if (!ownfinished) {
                                                                            pvp_progress_one.setProgressText(snapshotagent.getValue().toString() + "  (hidden)");
                                                                        } else {
                                                                            pvp_progress_one.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valone))) + ")");

                                                                        }
                                                                    }
                                                                } else {
                                                                    valone = 0;
                                                                    arrayListnums.add(Long.parseLong("0"));
                                                                    pvp_progress_one.setProgressText(snapshotagent.getValue().toString() + "   (not started yet)");
                                                                }
                                                                textone.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                                                                textone.setMarqueeRepeatLimit(-1);
                                                                textone.setSingleLine(true);
                                                                textone.setSelected(true);

                                                            } else if (detailsagents == 3) {
                                                                valtwo = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("currentvalue").getValue().toString());
                                                                pvp_progress_two.setProgressText(snapshotagent.getValue().toString());
                                                                if (Boolean.parseBoolean(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("started").getValue().toString())) {
                                                                    long agentstarttwo = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("starttime").getValue().toString());

                                                                    hoursbetweentwo = agentstartown - agentstarttwo;
                                                                    agentstartettimetwo = (agentstarttwo + pvpruntime) - System.currentTimeMillis();


                                                                    Log.i("DIFF IS Finished -->", " c " + agentstartettimetwo);

                                                                    //  Log.i("DIFF IS -->", " c " + hoursbetweentwo);
                                                                    if (hoursbetweentwo >= -900000 && agentstartettimetwo >= 450000) {
                                                                        ptwo = true;
                                                                        arrayListnums.add(valtwo);
                                                                        pvp_progress_two.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valtwo))) + ")");
                                                                    } else {
                                                                        valtwo = 5;
                                                                        arrayListnums.add(Long.parseLong("1"));
                                                                        if (!ownfinished) {
                                                                            pvp_progress_two.setProgressText(snapshotagent.getValue().toString() + "  (hidden)");
                                                                        } else {
                                                                            pvp_progress_two.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valtwo))) + ")");

                                                                        }

                                                                    }
                                                                } else {
                                                                    valtwo = 0;
                                                                    arrayListnums.add(Long.parseLong("0"));
                                                                    pvp_progress_two.setProgressText(snapshotagent.getValue().toString() + "   (not started yet)");
                                                                }


                                                                texttwo.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                                                                texttwo.setMarqueeRepeatLimit(-1);
                                                                texttwo.setSingleLine(true);
                                                                texttwo.setSelected(true);

                                                            } else if (detailsagents == 4) {
                                                                valthree = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("currentvalue").getValue().toString());
                                                                pvp_progress_three.setProgressText(snapshotagent.getValue().toString());
                                                                if (Boolean.parseBoolean(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("started").getValue().toString())) {
                                                                    long agentstartthree = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("starttime").getValue().toString());

                                                                    hoursbetweenthree = agentstartown - agentstartthree;

                                                                    agentstartettimethree = (agentstartthree + pvpruntime) - System.currentTimeMillis();


                                                                    Log.i("DIFF IS Finished -->", " c " + agentstartettimetwo);

                                                                    //Log.i("DIFF IS three -->", " c " + hoursbetweenthree);
                                                                    if (hoursbetweenthree >= -900000 && agentstartettimethree >= 450000) {
                                                                        pthree = true;
                                                                        arrayListnums.add(valthree);
                                                                        pvp_progress_three.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valthree))) + ")");
                                                                    } else {
                                                                        valthree = 5;
                                                                        arrayListnums.add(Long.parseLong("1"));
                                                                        if (!ownfinished) {
                                                                            pvp_progress_three.setProgressText(snapshotagent.getValue().toString() + "  (hidden)");
                                                                        } else {
                                                                            pvp_progress_three.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valthree))) + ")");

                                                                        }

                                                                    }
                                                                } else {
                                                                    valthree = 0;
                                                                    arrayListnums.add(Long.parseLong("0"));
                                                                    pvp_progress_three.setProgressText(snapshotagent.getValue().toString() + "   (not started yet)");
                                                                }


                                                                textthree.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                                                                textthree.setMarqueeRepeatLimit(-1);
                                                                textthree.setSingleLine(true);
                                                                textthree.setSelected(true);

                                                            }

                                                            if (snapshotrunningagentdetails.getChildrenCount() == detailsagents) {
                                                                arrayListnums.add(currentpvpvalue);
                                                                pvp_progress_own.setProgressText(pvp_progress_own.getProgressText() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(currentpvpvalue))) + ")");
                                                                Long maxvalueis = Collections.max(arrayListnums);

                                                                //Log.i("RUNNIMG -->", " pom " + maxvalueis + " c: " + arrayListnums.toString());
                                                                if (maxvalueis <= 0) {
                                                                    //Log.i("RUNNIMG -->", " pom2 " + detailsagents);
                                                                    pvp_progress_own.setProgress(1);
                                                                    pvp_progress_own.setMax(100);

                                                                    pvp_progress_one.setProgress(1);
                                                                    pvp_progress_one.setMax(100);

                                                                    pvp_progress_two.setProgress(1);
                                                                    pvp_progress_two.setMax(100);

                                                                    pvp_progress_three.setProgress(1);
                                                                    pvp_progress_three.setMax(100);
                                                                } else {
                                                                    //Log.i("RUNNIMG -->", " pom3 " + detailsagents);

                                                                    if (currentpvpvalue == maxvalueis) {
                                                                        int width = pvp_progress_own.getWidth();


                                                                        ValueAnimator animator = ValueAnimator.ofInt(0, 100);
                                                                        animator.setInterpolator(new LinearInterpolator());
                                                                        animator.setStartDelay(0);
                                                                        animator.setDuration(2_000);
                                                                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                            @Override
                                                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                int value = (int) valueAnimator.getAnimatedValue();
                                                                                pvp_progress_own.setProgress(value);
                                                                            }
                                                                        });

                                                                        animator.start();
                                                                        // pvp_progress_own.setProgress(100);
                                                                        pvp_progress_own.setMax(110);
                                                                    } else {
                                                                        float percentisown = (float) ((currentpvpvalue * 100) / maxvalueis);

                                                                        int width = pvp_progress_own.getWidth();


                                                                        ValueAnimator animator = ValueAnimator.ofInt(0, (int) percentisown);
                                                                        animator.setInterpolator(new LinearInterpolator());
                                                                        animator.setStartDelay(0);
                                                                        animator.setDuration(2_000);
                                                                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                            @Override
                                                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                int value = (int) valueAnimator.getAnimatedValue();
                                                                                pvp_progress_own.setProgress(value);
                                                                            }
                                                                        });

                                                                        animator.start();
                                                                        //  pvp_progress_own.setProgress(percentisown);
                                                                        pvp_progress_own.setMax(110);
                                                                    }


                                                                    if (valone == maxvalueis) {
                                                                        int width = pvp_progress_one.getWidth();


                                                                        ValueAnimator animator = ValueAnimator.ofInt(0, 100);
                                                                        animator.setInterpolator(new LinearInterpolator());
                                                                        animator.setStartDelay(0);
                                                                        animator.setDuration(2_000);
                                                                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                            @Override
                                                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                int value = (int) valueAnimator.getAnimatedValue();
                                                                                if (value > 55) {
                                                                                    textone.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                }
                                                                                pvp_progress_one.setProgress(value);
                                                                            }
                                                                        });

                                                                        animator.start();
                                                                        pvp_progress_one.setMax(110);
                                                                    } else {
                                                                        float percentisone = (float) ((valone * 100) / maxvalueis);
                                                                        int width = pvp_progress_one.getWidth();


                                                                        ValueAnimator animator = ValueAnimator.ofInt(0, (int) percentisone);
                                                                        animator.setInterpolator(new LinearInterpolator());
                                                                        animator.setStartDelay(0);
                                                                        animator.setDuration(2_000);
                                                                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                            @Override
                                                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                int value = (int) valueAnimator.getAnimatedValue();
                                                                                if (value > 55) {
                                                                                    textone.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                }
                                                                                pvp_progress_one.setProgress(value);
                                                                            }
                                                                        });

                                                                        animator.start();
                                                                        pvp_progress_one.setMax(110);
                                                                    }

                                                                    if (valtwo == maxvalueis) {
                                                                        int width = pvp_progress_two.getWidth();


                                                                        ValueAnimator animator = ValueAnimator.ofInt(0, 100);
                                                                        animator.setInterpolator(new LinearInterpolator());
                                                                        animator.setStartDelay(0);
                                                                        animator.setDuration(2_000);
                                                                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                            @Override
                                                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                int value = (int) valueAnimator.getAnimatedValue();
                                                                                if (value > 55) {
                                                                                    texttwo.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                }
                                                                                pvp_progress_two.setProgress(value);
                                                                            }
                                                                        });

                                                                        animator.start();
                                                                        pvp_progress_two.setMax(110);
                                                                    } else {
                                                                        float percentistwo = (float) ((valtwo * 100) / maxvalueis);
                                                                        int width = pvp_progress_two.getWidth();


                                                                        ValueAnimator animator = ValueAnimator.ofInt(0, (int) percentistwo);
                                                                        animator.setInterpolator(new LinearInterpolator());
                                                                        animator.setStartDelay(0);
                                                                        animator.setDuration(2_000);
                                                                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                            @Override
                                                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                int value = (int) valueAnimator.getAnimatedValue();
                                                                                if (value > 55) {
                                                                                    texttwo.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                }
                                                                                pvp_progress_two.setProgress(value);
                                                                            }
                                                                        });

                                                                        animator.start();
                                                                        pvp_progress_two.setMax(110);
                                                                    }

                                                                    if (valthree == maxvalueis) {
                                                                        int width = pvp_progress_three.getWidth();


                                                                        ValueAnimator animator = ValueAnimator.ofInt(0, 100);
                                                                        animator.setInterpolator(new LinearInterpolator());
                                                                        animator.setStartDelay(0);
                                                                        animator.setDuration(2_000);
                                                                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                            @Override
                                                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                int value = (int) valueAnimator.getAnimatedValue();
                                                                                if (value > 55) {
                                                                                    textthree.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                }
                                                                                pvp_progress_three.setProgress(value);
                                                                            }
                                                                        });

                                                                        animator.start();
                                                                        pvp_progress_three.setMax(110);
                                                                    } else {
                                                                        float percentisthree = (float) ((valthree * 100) / maxvalueis);
                                                                        int width = pvp_progress_three.getWidth();


                                                                        ValueAnimator animator = ValueAnimator.ofInt(0, (int) percentisthree);
                                                                        animator.setInterpolator(new LinearInterpolator());
                                                                        animator.setStartDelay(0);
                                                                        animator.setDuration(2_000);
                                                                        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                            @Override
                                                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                int value = (int) valueAnimator.getAnimatedValue();
                                                                                if (value > 55) {
                                                                                    textthree.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                }
                                                                                pvp_progress_three.setProgress(value);
                                                                            }
                                                                        });

                                                                        animator.start();
                                                                        pvp_progress_three.setMax(110);
                                                                    }
                                                                }

                                                                detailsagents = 1;
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        }
                                                    });
                                                }

                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                        }
                                    });
                                } else {
                                    // THIS PVP ISNT ACTIVE
                                }
                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            }
                        });

                        if (numagents == 2) {

                            pvp_progress_one.setVisibility(View.VISIBLE);


                        } else if (numagents == 3) {
                            pvp_progress_one.setVisibility(View.VISIBLE);
                            pvp_progress_two.setVisibility(View.VISIBLE);
                        } else if (numagents == 4) {
                            pvp_progress_one.setVisibility(View.VISIBLE);
                            pvp_progress_two.setVisibility(View.VISIBLE);
                            pvp_progress_three.setVisibility(View.VISIBLE);
                        }
                        // START ACTIONS PVP DETAILS IF PVP IS RUNNING
                    }


                } else {
                    if (isfinished && !canstart) {
                        title_start_btn_pvp.setEnabled(false);
                        title_start_btn_pvp.setIconResource(R.drawable.ic_hourglass_empty);
                        title_start_btn_pvp.setText("Opponent has given up 2");
                        bt_detailspvp.setVisibility(View.GONE);
                        title_giveup_btn_pvp.setVisibility(View.GONE);
                        // TODO MYBE I HAVE STARTET
                        mDatabase.child(user.getUid()).child("pvp").child(pvpkey).setValue(null);
                    } else {
                        title_start_btn_pvp.setEnabled(false);
                        title_start_btn_pvp.setIconResource(R.drawable.ic_hourglass_empty);
                        title_start_btn_pvp.setText("waiting for agents");
                        title_giveup_btn_pvp.setVisibility(View.GONE);
                    }
                }

                listenerpvpjoin = mbpvp.child(pvpkey).child("active").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshotstaus) {
                        //System.out.println(snapshot.toString() + "687655");  //prints "Do you have data? You'll love Firebase."

                        if (snapshotstaus.exists()) {
                            if (Boolean.parseBoolean(snapshotstaus.getValue().toString())) {
                                title_delet_btn_pvp.setVisibility(View.GONE);
                                title_reshare_btn_pvp.setVisibility(View.GONE);
                                if (!title_start_btn_pvp.isEnabled()) {

                                } else {
                                    title_start_btn_pvp.setEnabled(true);
                                }
                                title_start_btn_pvp.setIconResource(R.drawable.ic_directions_run);
                                title_start_btn_pvp.setText("start the PvP");
                                title_giveup_btn_pvp.setVisibility(View.VISIBLE);
                                //pvp_countown.setVisibility(View.GONE);
                                if (isstarted) {
                                    pvpstartet.setVisibility(View.VISIBLE);
                                    pvpnotstartet.setVisibility(View.GONE);


                                    if (ispvpnr == 1) {
                                        if (timer_pvp_one != null) {
                                            timer_pvp_one.cancel();
                                        }
                                    }
                                    if (ispvpnr == 2) {
                                        if (timer_pvp_two != null) {
                                            timer_pvp_two.cancel();
                                        }
                                    }
                                    if (ispvpnr == 3) {
                                        if (timer_pvp_three != null) {
                                            timer_pvp_three.cancel();
                                        }
                                    }
                                }
                            } else {


                                mbpvp.child(pvpkey).child("finished").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshotfinished) {

                                        if (Boolean.parseBoolean(dataSnapshotfinished.getValue().toString())) {

                                            if (!pvpactive || !pvpfinished) {
                                                bt_detailspvp.setVisibility(View.GONE);
                                                title_start_btn_pvp.setText("Opponent has never started, waiting for action");

                                            } else {
                                                bt_detailspvp.setVisibility(View.GONE);
                                                title_start_btn_pvp.setText("All Opponent have given up");
                                            }
                                            pvpstartet.setVisibility(View.GONE);
                                            pvpnotstartet.setVisibility(View.VISIBLE);
                                            title_start_btn_pvp.setEnabled(false);
                                            title_start_btn_pvp.setIconResource(R.drawable.ic_hourglass_empty);
                                            //    title_start_btn_pvp.setText("All Opponent have given up");
                                            title_giveup_btn_pvp.setVisibility(View.GONE);
                                            // TO DO MYBE I HAVE STARTET
                                            title_delet_btn_pvp.setVisibility(View.GONE);
                                            title_reshare_btn_pvp.setVisibility(View.GONE);

                                            title_update_btn_pvp.setVisibility(View.GONE);

                                            // title_start_btn_pvp.setVisibility(View.GONE);

                                            pvp_countown.setVisibility(View.GONE);


                                        } else {
                                            title_delet_btn_pvp.setVisibility(View.VISIBLE);
                                            title_start_btn_pvp.setEnabled(false);
                                            title_start_btn_pvp.setIconResource(R.drawable.ic_hourglass_empty);
                                            title_start_btn_pvp.setText("waiting for agents");
                                            title_giveup_btn_pvp.setVisibility(View.GONE);
                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError error) {
                                        // Failed to read value

                                    }
                                });


                            }


                            title_giveup_btn_pvp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    AlertDialog.Builder confirmdialog = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                    // builder.setMessage("Text not finished...");
                                    LayoutInflater inflater = getLayoutInflater();
                                    View dialog = inflater.inflate(R.layout.dialog_pvp_quit, null);
                                    confirmdialog.setView(dialog);

                                    TextView agentname = dialog.findViewById(R.id.agentname);
                                    agentname.setText("Are you sure you want to give up and leave the PvP?\n\nNote: Leaving a duel early has a negative impact on your AOD level.");


                                    confirmdialog.setPositiveButton("Yes, leave now", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            if (snapshotpvpdata.child("art").getValue().toString().equalsIgnoreCase("linkshare")) {
                                                giveuppvp(pvpkey, pvpbadgeid, pvptitle, snapshotpvpdata.child("art").getValue().toString(), snapshotpvpdata.child("createruuid").getValue().toString(), "0");

                                            } else {

                                                for (DataSnapshot invitedagents : snapshotpvpdata.child("agents").getChildren()) {

                                                    //Log.i("INVITES --> ", " pommes " + invitedagents.getKey());
                                                    if (invitedagents.child("uuid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                                        giveuppvp(pvpkey, pvpbadgeid, pvptitle, snapshotpvpdata.child("art").getValue().toString(), snapshotpvpdata.child("createruuid").getValue().toString(), invitedagents.getKey());


                                                    } else {
                                                        //Log.i("INVITES --> ", " 2 pommes " + invitedagents.getKey());
                                                        giveuppvp(pvpkey, pvpbadgeid, pvptitle, snapshotpvpdata.child("art").getValue().toString(), snapshotpvpdata.child("createruuid").getValue().toString(), "0");
                                                    }


                                                    //  Log.i("TEST LOG--->",child.getValue().toString());

                                                }
                                            }

                                            //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                        }
                                    });
                                    confirmdialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                            //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                        }
                                    });

                                    confirmdialog.setCancelable(true);

                                    if (!isFinishing()) {
                                        confirmdialog.create().show();

                                    }

                                }
                            });


                            title_start_btn_pvp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Log.i("Will Join!->", " 6786");


                                    if (isStoragePermissionGranted()) {
                                        Log.v(TAG, "Permission is granted");
                                        //File write logic here
                                        if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                                            // mExplosionField.clear();
                                            if (isMyServiceRunning(OverlayScreenService.class)) {
                                                Log.i("SERVICESTATUS", "running");
                                                Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                                                stopService(svc);
                                            } else {
                                                //Log.i("ÜBERGABE ---> ", " " + pvpbadgeidint[0]);
                                                mDatabaseaod.child("badge").addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot snapshotbadgedata) {

                                                        String badgefield = snapshotbadgedata.child(pvpbadgeid).child("field").getValue().toString();

                                                        Log.i("ÜBERGABE ---> ", " " + pvpbadgeidint[0]);
                                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                            if (Settings.canDrawOverlays(AODMain.this)) {

                                                                startscreenshotpvp("pvp", pvpkey, badgefield, pvptitle, pvpbadgeidint[0]);
                                                                // startActivityForResult(intent , 7002);
                                                            } else {
                                                                verifyOverlay(AODMain.this);
                                                            }
                                                        } else {
                                                            startscreenshotpvp("pvp", pvpkey, badgefield, pvptitle, pvpbadgeidint[0]);
                                                            //   startActivityForResult(intent , 7002);
                                                        }

                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                    }
                                                });
                                            }
                                        } else {
                                            if (isStoragePermissionGranted()) {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                                // builder.setMessage("Text not finished...");
                                                LayoutInflater inflater = getLayoutInflater();
                                                View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                                builder.setView(dialog);
                                                final TextView input = dialog.findViewById(R.id.caltext);
                                                // input.setAllCaps(true);
                                                input.setTypeface(type);

                                                //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                                //localTextView1.setTypeface(proto);
                                                builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        // Log.i("Hallo", "hallo");
                                                        //handleinputcode(input.getText().toString());
                                                        // Todo publish the code to database
                                                        Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                        //i.putExtra("PersonID", personID);
                                                        startActivity(i);
                                                        //finish();

                                                    }
                                                });

                                                builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {


                                                        // Todo publish the code to database
                                                    }
                                                });

                                                builder.setCancelable(true);

                                                if (!isFinishing()) {
                                                    builder.create().show();

                                                }
                                            }
                                        }
                                    }


                                    // ENDE START PVP
                                }
                            });
                            //Log.i("STATUS PVP change to->", snapshotstaus.getValue().toString());
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                    }
                });


                if (canstart) {
                    if (!isstarted) {
                        System.out.println("SET 48 Stunden :: " + System.currentTimeMillis());
                        // SET 48 Stunden
                        long diffInMs = Long.parseLong(snapshotpvpdata.child("timestamp").getValue().toString()) + (172800 * 1000) - System.currentTimeMillis();

                        //Log.i("diffpvp ->", diffInMs + " " + ((Long.parseLong(snapshotpvpdata.child("timestamp").getValue().toString()) / 1000) + 172800) + " " + System.currentTimeMillis() / 1000);

                        if (ispvpnr == 1) {
                            timer_pvp_one = new InviteFinish(diffInMs, 1000, pvp_countown, pvpkey, snapshotpvpdata.child("art").getValue().toString(), "tostart", title_start_btn_pvp, title_details_btn_pvp);
                            timer_pvp_one.start();
                        }

                        if (ispvpnr == 2) {
                            timer_pvp_two = new InviteFinish(diffInMs, 1000, pvp_countown, pvpkey, snapshotpvpdata.child("art").getValue().toString(), "tostart", title_start_btn_pvp, title_details_btn_pvp);
                            timer_pvp_two.start();
                        }
                        if (ispvpnr == 3) {
                            timer_pvp_three = new InviteFinish(diffInMs, 1000, pvp_countown, pvpkey, snapshotpvpdata.child("art").getValue().toString(), "tostart", title_start_btn_pvp, title_details_btn_pvp);
                            timer_pvp_three.start();
                        }
                    } else {
                        if (!isfinished) {


                            //System.out.println("SET 48 Stunden :: " + System.currentTimeMillis());
                            // SET 48 Stunden
                            long diffInMs = startpvptime + (Long.parseLong(snapshotpvpdata.child("runtime").getValue().toString()) * 1000) - System.currentTimeMillis();

                            //Log.i("diffpvp ->", diffInMs + " " + (Long.parseLong(snapshotpvpdata.child("runtime").getValue().toString()) + startpvptime) + " " + System.currentTimeMillis() / 1000);

                            if (ispvpnr == 1) {
                                timer_pvp_one_finish = new InviteFinish(diffInMs, 1000, pvp_runtimecountown, pvpkey, snapshotpvpdata.child("art").getValue().toString(), "tofinish", title_start_btn_pvp, title_details_btn_pvp);
                                timer_pvp_one_finish.start();
                            }

                            if (ispvpnr == 2) {
                                timer_pvp_two_finish = new InviteFinish(diffInMs, 1000, pvp_runtimecountown, pvpkey, snapshotpvpdata.child("art").getValue().toString(), "tofinish", title_start_btn_pvp, title_details_btn_pvp);
                                timer_pvp_two_finish.start();
                            }
                            if (ispvpnr == 3) {
                                timer_pvp_three_finish = new InviteFinish(diffInMs, 1000, pvp_runtimecountown, pvpkey, snapshotpvpdata.child("art").getValue().toString(), "tofinish", title_start_btn_pvp, title_details_btn_pvp);
                                timer_pvp_three_finish.start();
                            }
                        } else {
                            // TODO AGENT FINISHED POS 1-3
                            // TODO OTHER NOT FINISHED
                            // TODO ALL FINBISHED
                        }
                    }
                } else {

                    System.out.println("wait for join :: " + System.currentTimeMillis());
                    long diffInMs = Long.parseLong(snapshotpvpdata.child("timestamp").getValue().toString()) + (86040 * 1000) - System.currentTimeMillis();

                    //Log.i("diffpvp ->", diffInMs + " " + ((Long.parseLong(snapshotpvpdata.child("timestamp").getValue().toString()) / 1000) + 86400) + " " + System.currentTimeMillis() / 1000);

                    if (diffInMs < 0) {
                        title_start_btn_pvp.setEnabled(false);
                    }
                    if (ispvpnr == 1) {
                        timer_pvp_one = new InviteFinish(diffInMs, 1000, pvp_countown, pvpkey, snapshotpvpdata.child("art").getValue().toString(), "tojoin", title_start_btn_pvp, title_details_btn_pvp);
                        timer_pvp_one.start();
                    }

                    if (ispvpnr == 2) {
                        timer_pvp_two = new InviteFinish(diffInMs, 1000, pvp_countown, pvpkey, snapshotpvpdata.child("art").getValue().toString(), "tojoin", title_start_btn_pvp, title_details_btn_pvp);
                        timer_pvp_two.start();
                    }
                    if (ispvpnr == 3) {
                        timer_pvp_three = new InviteFinish(diffInMs, 1000, pvp_countown, pvpkey, snapshotpvpdata.child("art").getValue().toString(), "tojoin", title_start_btn_pvp, title_details_btn_pvp);
                        timer_pvp_three.start();
                    }
                }


                title_from_address_pvp.setText("PvP: " + snapshotpvpdata.child("pvptitle").getValue().toString());


                // title_from_address_pvp
                //  title_to_address_pvp.setText(snapshotpvpdata.child("pvptitle").getValue().toString());
                frameloadpvp.setVisibility(View.GONE);
                framecardpvp.setVisibility(View.VISIBLE);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                TextView pvpload_txt = view.findViewById(R.id.pvpload_txt);
                pvpload_txt.setText("Error while loading PvP ID: " + pvpkey);

            }
        });
        // LayoutParams.MATCH_PARENT
    }

    private void deletePvP(final String pvpkey, final String artis, final boolean restart) {

        mDatabase.child(user.getUid()).child("pvp").child(pvpkey).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    //finish();
                    mDatabaserunnungpvp.child(pvpkey).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                mbpvp.child(pvpkey).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();

                                            if (artis.equalsIgnoreCase("invites")) {
                                                if (restart) {
                                                    recreate();
                                                }
                                            } else {
                                                mbpvplinks.child("AODs_" + pvpkey).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            mbpvp.child(pvpkey).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {
                                                                        if (restart) {
                                                                            recreate();
                                                                        }


                                                                    } else if (task.isCanceled()) {

                                                                        if (restart) {
                                                                            recreate();
                                                                        }

                                                                    }
                                                                }

                                                            }).addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(Exception e) {

                                                                    if (restart) {
                                                                        recreate();
                                                                    }

                                                                }
                                                            });


                                                        } else if (task.isCanceled()) {

                                                            if (restart) {
                                                                recreate();
                                                            }

                                                        }
                                                    }

                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(Exception e) {

                                                        if (restart) {
                                                            recreate();
                                                        }

                                                    }
                                                });
                                            }


                                        } else if (task.isCanceled()) {

                                            if (restart) {
                                                recreate();
                                            }

                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        if (restart) {
                                            recreate();
                                        }

                                    }
                                });


                            } else if (task.isCanceled()) {

                                if (restart) {
                                    recreate();
                                }

                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {

                            if (restart) {
                                recreate();
                            }

                        }
                    });


                } else if (task.isCanceled()) {

                    if (restart) {
                        recreate();
                    }

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {

                if (restart) {
                    recreate();
                }

            }
        });


    }

    private void pvpnotstarted(final String pvpkey, final String artis, final boolean restart) {
        // PROOF OTHER AGENTS STARTED
        mDatabaserunnungpvp.child(pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshotpvpagents) {

                int looperagentsstart = 0;
                int anzahlstartedagents = 0;
                boolean otherstarted = false;
                // HAS CHILDREN PROOF > = 2
                if (dataSnapshotpvpagents.getChildrenCount() >= 2) {

                    for (DataSnapshot childjoinedagents : dataSnapshotpvpagents.getChildren()) {
                        looperagentsstart++;
                        // AGENT KEY IS NOT THE OWN KEY
                        if (!childjoinedagents.getKey().equalsIgnoreCase(user.getUid())) {

                            // GET START VALUE
                            if (Boolean.parseBoolean(childjoinedagents.child("started").getValue().toString())) {
                                otherstarted = true;
                                anzahlstartedagents++;
                            }

                        }

                        if (looperagentsstart >= dataSnapshotpvpagents.getChildrenCount()) {
                            // MORE AGENTS STARTET
                            if (anzahlstartedagents >= 2) {
                                // SET START TO TRUE UND FINISHED TO TRUE
                                final Map<String, Object> childUpdatesnotstart = new HashMap<>();
                                childUpdatesnotstart.put("/started", true);
                                childUpdatesnotstart.put("/finished", true);
                                mDatabaserunnungpvp.child(pvpkey).child(user.getUid()).updateChildren(childUpdatesnotstart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            final Map<String, Object> childUpdatesnotstartown = new HashMap<>();
                                            childUpdatesnotstartown.put("/comment", "not started");
                                            childUpdatesnotstartown.put("/finished", true);
                                            mDatabase.child(user.getUid()).child("pvp").child(pvpkey).updateChildren(childUpdatesnotstartown).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        recreate();

                                                    } else if (task.isCanceled()) {
                                                        recreate();
                                                    } else {
                                                        recreate();
                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    recreate();
                                                }
                                            });

                                        } else if (task.isCanceled()) {
                                            recreate();
                                        } else {
                                            recreate();
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        recreate();
                                    }
                                });
                            }
                            // ONLY ONE AGENTS STARTET // SET WINNER
                            else if (anzahlstartedagents == 1) {
                                final Map<String, Object> childUpdatesnotstart = new HashMap<>();
                                childUpdatesnotstart.put("/started", true);
                                childUpdatesnotstart.put("/comment", "not started");
                                childUpdatesnotstart.put("/finished", true);
                                mDatabaserunnungpvp.child(pvpkey).child(user.getUid()).updateChildren(childUpdatesnotstart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            final Map<String, Object> childUpdatesnotstartown = new HashMap<>();
                                            childUpdatesnotstartown.put("/comment", "not started");
                                            childUpdatesnotstartown.put("/finished", true);
                                            mDatabase.child(user.getUid()).child("pvp").child(pvpkey).updateChildren(childUpdatesnotstartown).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        final Map<String, Object> childUpdatesnotstartownc = new HashMap<>();
                                                        childUpdatesnotstartownc.put("/active", false);
                                                        childUpdatesnotstartownc.put("/finished", true);
                                                        childUpdatesnotstartownc.put("/auswertung", false);
                                                        mbpvp.child(pvpkey).updateChildren(childUpdatesnotstartownc).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    recreate();

                                                                } else if (task.isCanceled()) {
                                                                    recreate();
                                                                } else {
                                                                    recreate();
                                                                }
                                                            }

                                                        }).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(Exception e) {
                                                                recreate();
                                                            }
                                                        });

                                                    } else if (task.isCanceled()) {
                                                        recreate();
                                                    } else {
                                                        recreate();
                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    recreate();
                                                }
                                            });

                                        } else if (task.isCanceled()) {
                                            recreate();
                                        } else {
                                            recreate();
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        recreate();
                                    }
                                });
                            }
                            // NO OTHER STARTED DELETE
                            else {
                                deletePvP(pvpkey, artis, true);
                            }

                        }


                    }
                } else {
                    // AGENT IS THE ONLY AGENTS
                    deletePvP(pvpkey, artis, true);
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                //Log.e("TAG", "Failed to read.", error.toException());
                //  errorToast("week", "9004");
            }
        });

    }

    private void initweek(final String theyear, final String months, final String theweek, final String[] days, final boolean isrunning) {
        Log.i("INIT WEEK-->", "gestartet " + isrunning);
        mDatabasemissions.child("week").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshotweekis) {
                Log.i("INIT WEEK SNAP -->", theweek + " " + theyear);
                if (snapshotweekis.hasChild(theweek)) {
                    Log.i("WEEK SNAP -->", "Woche gefunden");
                    // SET TITLEVIEW
                    int monthd = 0;

                    try {
                        monthd = Integer.parseInt(months);
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                    TextView week_txt2 = findViewById(R.id.title_date_label_week);
                    TextView week_txt1 = findViewById(R.id.title_time_label_week);
                    Calendar sDateCalendar = new GregorianCalendar(Integer.parseInt(theyear), monthd, Integer.parseInt(days[0]));
                    Calendar.getInstance(Locale.GERMANY).setTimeZone(TimeZone.getDefault());
                    sDateCalendar.setFirstDayOfWeek(Calendar.MONDAY);
                    int currentweek = Calendar.getInstance(Locale.getDefault()).get(Calendar.WEEK_OF_YEAR);
                    int monthn = Calendar.getInstance(Locale.getDefault()).get(Calendar.MONTH);
                    monthd = monthn;


                    week_txt1.setText(getEndOFWeek(currentweek, Integer.parseInt(theyear)));
                    week_txt2.setText(getStartOFWeek(currentweek, Integer.parseInt(theyear)));

                    final TextView week_txt_cat = findViewById(R.id.weekscat);
                    mDatabaseaod.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshotaod) {
                            TextView weeksub = findViewById(R.id.title_to_address_week);
                            weeksub.setText(snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("title").getValue().toString());
                            // monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield;

                            weekbadgeid = snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("id").getValue().toString();
                            weekbadgetitle = snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("title").getValue().toString();
                            weekbadgeshort1 = snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("short1").getValue().toString();
                            weekbadgeshort2 = snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("short2").getValue().toString();
                            weekbadgefield = snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("field").getValue().toString();
                            weekbadgedrawable = snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("drawable").getValue().toString();
                            weekbadgedrawablegold = snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("drawablegold").getValue().toString();
                            weekbadgedrawableplatin = snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("drawableplatin").getValue().toString();
                            week_txt_cat.setText(snapshotaod.child("cat").child(snapshotweekis.child(theweek).child("cat").getValue().toString()).child("title").getValue().toString());

                            TextView notstartedweekinfo = findViewById(R.id.notstartedweekinfo);


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                notstartedweekinfo.setText(Html.fromHtml(String.format(getString(R.string.goal_title), snapshotweekis.child(theweek).child("goal").getValue().toString(), snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("short1").getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                notstartedweekinfo.setText(Html.fromHtml(String.format(getString(R.string.goal_title), snapshotweekis.child(theweek).child("goal").getValue().toString(), snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("short1").getValue().toString())));

                            }
                            //   notstartedweekinfo.setText(getString(R.string.goal)+": " + snapshotweekis.child(theweek).child("goal").getValue().toString() + " " + snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("short1").getValue().toString());
                            // snapshotweekis.child(themonth).child("id").getValue().toString()

                            theweekgoal = Integer.parseInt(snapshotweekis.child(theweek).child("goal").getValue().toString());
                            theweekgoalplatin = Integer.parseInt(snapshotweekis.child(theweek).child("goalplatin").getValue().toString());
                            theweekgoalblack = Integer.parseInt(snapshotweekis.child(theweek).child("goalblack").getValue().toString());
                            // SET ICON

                            int badgeweekid = getResources().getIdentifier(snapshotaod.child("badge").child(snapshotweekis.child(theweek).child("badge").getValue().toString()).child("drawable").getValue().toString(), "drawable", getPackageName());

                            //badgemonth = getResources().obtainTypedArray(badgemonthid);
                            if (badgeweekid != 0) {
                                badgeweekidscreener = badgeweekid;
                                Log.w("BADGE -->", " id :" + badgeweekid);
                                ImageView week_badge = findViewById(R.id.title_price_week);
                                week_badge.setImageDrawable(getDrawable(badgeweekid));
                            }

                            Bundle params = new Bundle();
                            params.putString("agent", AgentData.getagentname());
                            mFirebaseAnalytics.logEvent("aod_missions_loaded", params);

                            // STARTE GET DATA FOR MONTH
                            if (isrunning) {
                                // TODO GET DETAILS FOR THE AGENT
                                cweekmissionid = snapshotweekis.child(theweek).child("id").getValue().toString();
                                weekgetagentDetails(snapshotweekis.child(theweek).child("id").getValue().toString(), snapshotweekis.child(theweek), theyear, theweek);

                            } else {

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        frameloadweek.setVisibility(View.GONE);
                                        framecardweek.setVisibility(View.VISIBLE);


                                    }
                                }, 1000);
                                Log.i("DATA ID -->", snapshotweekis.child(theweek).child("id").getValue().toString());

                                weeknotstartet.setVisibility(View.VISIBLE);
                                weekstartet.setVisibility(View.GONE);

                                join_btn_week.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                                            //  infoToast(getString(R.string.underconstruction));
                                            Bundle params = new Bundle();
                                            params.putString("agent", AgentData.getagentname());
                                            mFirebaseAnalytics.logEvent("aod_missions_week_joined", params);
                                            mDatabasemissions.child("week").child(theyear).child(theweek).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshotupdate) {
                                                    int upagents = (Integer.parseInt(dataSnapshotupdate.child("agents").getValue().toString()) + 1);
                                                    final Map<String, Object> childUpdatesx = new HashMap<>();
                                                    childUpdatesx.put("/agents", upagents);
                                                    mDatabasemissions.child("week").child(theyear).child(theweek).updateChildren(childUpdatesx).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                //finish();
                                                                joinmission("week", snapshotweekis.child(theweek).child("id").getValue().toString(), snapshotweekis);
                                                            } else if (task.isCanceled()) {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9001");
                                                                errorToast("week", "9001");
                                                            } else {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 9002");
                                                                errorToast("week", "9002");
                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 9003");
                                                            errorToast("week", "9003");

                                                            //signOut();
                                                        }
                                                    });


                                                }

                                                @Override
                                                public void onCancelled(DatabaseError error) {
                                                    // Failed to read value
                                                    Log.e("TAG", "Failed to update.", error.toException());
                                                    errorToast("week", "9004");
                                                }
                                            });
                                            //   joinmission("week", snapshot.child(theweek).child("id").getValue().toString(), snapshot);
                                        } else {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                            // builder.setMessage("Text not finished...");
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                            builder.setView(dialog);
                                            final TextView input = dialog.findViewById(R.id.caltext);
                                            // input.setAllCaps(true);
                                            input.setTypeface(type);

                                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                            //localTextView1.setTypeface(proto);
                                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                    // Log.i("Hallo", "hallo");
                                                    //handleinputcode(input.getText().toString());
                                                    // Todo publish the code to database
                                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                    //i.putExtra("PersonID", personID);
                                                    startActivity(i);
                                                    // finish();

                                                }
                                            });

                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {


                                                    // Todo publish the code to database
                                                }
                                            });

                                            builder.setCancelable(true);

                                            if (!isFinishing()) {
                                                builder.create().show();

                                            }
                                        }
                                    }
                                });

                                // RATE THIS APP

                            }


                        }


                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                        }
                    });
                } else {

                    Log.i("WEEK SNAP-->", "Week noch nicht aktiv");
                    fcweek.setVisibility(View.GONE);
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });


    }

    private void joinmission(final String isart, final String missionid, final DataSnapshot snapshot) {
        // PROOF INGRESS SCREEN IS KALIBRATED
        Log.i("JOIN MISSION -->", missionid);
        bmb_main.setEnabled(false);
        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
        SimpleDateFormat fyear = new SimpleDateFormat("yyyy");
        final String themonth = fmonth.format(new Date());
        String tempthemonth = fmonth.format(new Date());
        final String theyear = fyear.format(new Date());
        DateFormat format = new SimpleDateFormat("dd");
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        Calendar.getInstance(Locale.getDefault()).setTimeZone(TimeZone.getDefault());
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        final String[] days = new String[7];
        for (int i = 0; i < 7; i++) {
            days[i] = format.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        if (Integer.parseInt(days[1]) < Integer.parseInt(days[0]) || Integer.parseInt(days[2]) < Integer.parseInt(days[0]) || Integer.parseInt(days[3]) < Integer.parseInt(days[0]) || Integer.parseInt(days[4]) < Integer.parseInt(days[0]) || Integer.parseInt(days[5]) < Integer.parseInt(days[0]) || Integer.parseInt(days[6]) < Integer.parseInt(days[0])) {
            Log.i("DATE FIX -->", " " + Calendar.DAY_OF_MONTH);
            if (calendar.get(Calendar.DAY_OF_MONTH) <= 7) {
                Log.i("DATE FIX -->", "Tag kleiner 7");
                tempthemonth = String.valueOf(Integer.parseInt(themonth) - 1);
            } else {
                Log.i("DATE FIX -->", "Tag größer 7");
                tempthemonth = String.valueOf(Integer.parseInt(themonth) + 1);
            }
        } else {
            Log.i("DATE FIX -->", "Default Month");
            tempthemonth = themonth;
        }


        String strDate = days[0] + "." + (Integer.parseInt(tempthemonth)) + "." + theyear;
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = null;
        try {
            date = dateFormat.parse(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Calendar now = Calendar.getInstance(Locale.GERMANY);
        now.setTimeZone(TimeZone.getDefault());
        now.setFirstDayOfWeek(Calendar.MONDAY);
        now.setTime(date);

        int currentweek = now.get(Calendar.WEEK_OF_YEAR);
        String cweeksend;
        if (currentweek <= 9) {
            currentweek = Integer.parseInt("0" + currentweek);
            cweeksend = "0" + currentweek;
        } else {
            cweeksend = String.valueOf(currentweek);
        }



        Log.i("CURRENT WEEK FOR", "" + cweeksend);
        final String thefirstweekday = theweekx;//cweeksend + "" + days[0];
        Log.i("MONTH -->", themonth + " " + tempthemonth);
        Log.i("Year -->", theyear);
        Log.i("Dayof Week -->", "" + days[0]);



        //   Log.i("MONTH -->", themonth);
        //   Log.i("Year -->", theyear);
        //   Log.i("Dayof Week -->", "" + days[0]);
        // START JOIN MISSION


        if (isart.equalsIgnoreCase("week")) {
            frameloadweek.setVisibility(View.VISIBLE);
            framecardweek.setVisibility(View.GONE);

            TextView loading_text = findViewById(R.id.weekload_txt);
            loading_text.setText(getString(R.string.join_loading));
            join_btn_month.setEnabled(false);

            final String newmonthsend = tempthemonth;
            //mDatabase.child(user.getUid()).child("missions").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {

            mDatabase.child(user.getUid()).child("missions").child(theyear).child(isart).child(thefirstweekday).child(missionid).child("timestamp").setValue((System.currentTimeMillis() / 1000)).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {

                        addtomission(snapshot, theyear, newmonthsend, thefirstweekday, isart, missionid);

                    } else if (task.isCanceled()) {
                        errorToast(isart, "1001");
                    } else {
                        errorToast(isart, "1002");
                    }
                }


            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());
                    errorToast(isart, "1005");

                    //signOut();
                }
            });

        }
        if (isart.equalsIgnoreCase("month")) {
            frameloadmonth.setVisibility(View.VISIBLE);
            framecardmonth.setVisibility(View.GONE);
            TextView loading_text = findViewById(R.id.monthload_txt);
            loading_text.setText(getString(R.string.join_loading));
            join_btn_week.setEnabled(false);

            mDatabase.child(user.getUid()).child("missions").child(theyear).child(isart).child(themonth).child(missionid).child("timestamp").setValue((System.currentTimeMillis() / 1000)).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {

                        addtomission(snapshot, theyear, themonth, thefirstweekday, isart, missionid);

                    } else if (task.isCanceled()) {
                        errorToast(isart, "1001");
                    } else {
                        errorToast(isart, "1002");
                    }
                }


            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());
                    errorToast(isart, "1005");

                    //signOut();
                }
            });
        }

        if (isart.equalsIgnoreCase("earth")) {
            frameloadmonth.setVisibility(View.VISIBLE);
            framecardmonth.setVisibility(View.GONE);
            TextView loading_text = findViewById(R.id.monthload_txt);
            loading_text.setText(getString(R.string.join_loading));
            join_btn_week.setEnabled(false);

            mDatabase.child(user.getUid()).child("missions").child(theyear).child(isart).child(missionid).child("timestamp").setValue((System.currentTimeMillis() / 1000)).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {

                        addtomission(snapshot, theyear, themonth, thefirstweekday, isart, missionid);

                    } else if (task.isCanceled()) {
                        errorToast(isart, "1001");
                    } else {
                        errorToast(isart, "1002");
                    }
                }


            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());
                    errorToast(isart, "1005");

                    //signOut();
                }
            });
        }


        if (isart.equalsIgnoreCase("mini")) {
            frameloadmini.setVisibility(View.VISIBLE);
            framecardmini.setVisibility(View.GONE);
            TextView loading_text = findViewById(R.id.miniload_txt);
            loading_text.setText(getString(R.string.join_loading));
            join_btn_mini.setEnabled(false);

            mDatabase.child(user.getUid()).child("missions").child(theyear).child(isart).child(missionid).child("timestamp").setValue((System.currentTimeMillis() / 1000)).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {

                        addtomission(snapshot, theyear, themonth, thefirstweekday, isart, missionid);

                    } else if (task.isCanceled()) {
                        errorToast(isart, "1001");
                    } else {
                        errorToast(isart, "1002");
                    }
                }


            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());
                    errorToast(isart, "1005");

                    //signOut();
                }
            });
        }


    }

    private void addtomission(final DataSnapshot snapshot, final String theyear, final String themonth, final String thefirstweekday, final String isart, final String missionid) {
        final Map<String, Object> childUpdates = new HashMap<>();
        //Log.w("RESULTS -> ", "gid: " + gid + "user: " + user.getUid() + " profil " + aprofilimage);
        mDatabaserunnung.child(theyear).child(isart).child(missionid).child(user.getUid()).child("timestamp").setValue(System.currentTimeMillis() / 1000).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    // IF hasmissions
                    childUpdates.put("/timestamp", System.currentTimeMillis() / 1000);
                    childUpdates.put("/uid", user.getUid());
                    if (isart.equalsIgnoreCase("mini")) {
                        childUpdates.put("/uploads", 0);
                    }
                    childUpdates.put("/googleid", user.getProviderData().get(1).getUid());
                    childUpdates.put("/startvalue", 0);
                    childUpdates.put("/lastvalue", 0);
                    childUpdates.put("/currentvalue", 0);
                    childUpdates.put("/finished", false);


                    mDatabaserunnung.child(theyear).child(isart).child(missionid).child(user.getUid()).updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();
                                successfulJoined(snapshot, isart, missionid, theyear, thefirstweekday, themonth);
                            } else if (task.isCanceled()) {
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");
                                errorToast(isart, "3001");
                            } else {
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");
                                errorToast(isart, "3002");
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");
                            errorToast(isart, "3003");

                            //signOut();
                        }
                    });

                }
            }


        });

    }

    private void successfulJoined(DataSnapshot snapshot, String isart, String missionid, String theyear, String thefirstweekday, String themonth) {
        bmb_main.setEnabled(true);

        if (isart.equalsIgnoreCase("week")) {
            frameloadweek.setVisibility(View.GONE);
            framecardweek.setVisibility(View.VISIBLE);


            join_btn_month.setEnabled(true);
        } else if (isart.equalsIgnoreCase("month")) {
            frameloadmonth.setVisibility(View.GONE);
            framecardmonth.setVisibility(View.VISIBLE);


            join_btn_week.setEnabled(true);
        } else if (isart.equalsIgnoreCase("earth")) {
            frameloadearth.setVisibility(View.GONE);
            framecardearth.setVisibility(View.VISIBLE);


            join_btn_earth.setEnabled(true);
        } else if (isart.equalsIgnoreCase("mini")) {
            frameloadmini.setVisibility(View.GONE);
            framecardmini.setVisibility(View.VISIBLE);


            join_btn_mini.setEnabled(true);
        }

        initmissions();

    }

    private void monthgetagentDetails(final String theid, final DataSnapshot snapofdetails, final String theyear, final String themonth) {
        Log.i("THE ID OF EVENT -->", theid);
        Log.i("THE GOAL OF EVENT -->", snapofdetails.child("goal").getValue().toString());
        // INIT DETAILS
        boolean hasranking = Boolean.parseBoolean(snapofdetails.child("ranking").getValue().toString());
        boolean hasprice = Boolean.parseBoolean(snapofdetails.child("price").getValue().toString());
        boolean hasteam = Boolean.parseBoolean(snapofdetails.child("team").getValue().toString());
        boolean hasduell = Boolean.parseBoolean(snapofdetails.child("duell").getValue().toString());
        boolean hasxf = Boolean.parseBoolean(snapofdetails.child("xf").getValue().toString());
        int activeagents = Integer.parseInt(snapofdetails.child("agents").getValue().toString());
        goalgold = Integer.parseInt(snapofdetails.child("goal").getValue().toString());
        goalplatin = Integer.parseInt(snapofdetails.child("goalplatin").getValue().toString());
        goalblack = Integer.parseInt(snapofdetails.child("goalblack").getValue().toString());


        // INIT TEXT VIEWS
        final TextView goal_txt_title = findViewById(R.id.title_pledge);
        final TextView content_avatar_title_month = findViewById(R.id.content_avatar_title_month);
        final TextView content_community_title_month = findViewById(R.id.content_community_title_month);
        final TextView rank_txt_title = findViewById(R.id.title_weight);
        final TextView rank_txt_title_action = findViewById(R.id.rang_title_action);
        rank_txt_title.setOnClickListener(this);

        final TextView progress_txt_title = findViewById(R.id.title_requests_count);
        final TextView startwert_txt_content = findViewById(R.id.content_from_address_1_month);
        final TextView short2_txt_content = findViewById(R.id.content_from_address_2_month);
        final TextView rank_text_content = findViewById(R.id.content_to_address_2_month);
        final TextView rank_text_content_com = findViewById(R.id.content_to_address_2_month_com);
        final TextView goal_txt_content = findViewById(R.id.content_to_go_1_month);
        final TextView togo_txt_content = findViewById(R.id.content_to_go_2_month);
        final TextView togo_txt_content_com = findViewById(R.id.content_to_go_2_month_com);
        final TextView title_txt_content = findViewById(R.id.content_name_view_month);
        final TextView title_txt_community = findViewById(R.id.content_community_view_month);
        final TextView mission_detals_content_txt_month = findViewById(R.id.mission_detals_content_txt_month);
        final TextView mission_community_content_txt_month = findViewById(R.id.mission_community_content_txt_month);
        final TextView progress_txt_content = findViewById(R.id.content_to_address_1_month);
        final TextView goal_txt_content_community = findViewById(R.id.content_to_go_1_month_com);
        final TextView progress_txt_content_com = findViewById(R.id.content_to_address_1_month_com);
        final TextView last_upload_val_txt_content = findViewById(R.id.content_delivery_time_month);
        final TextView deadlinetime_content = findViewById(R.id.content_deadline_time_month);
        final TextView runtime_date_content = findViewById(R.id.content_deadline_date_month);
        final TextView lastupload_time_content = findViewById(R.id.content_delivery_date_month);
        final TextView content_from_badge_month_com = findViewById(R.id.content_from_badge_month_com);
        final ProgressBar community_enl = findViewById(R.id.statusenl);
        final ProgressBar community_res = findViewById(R.id.statusres);
        final ProgressBar status_agent = findViewById(R.id.statusagent);
        final ImageView team_swipe_month = findViewById(R.id.teamswipe_month);

        final RelativeLayout monthhistory_action = findViewById(R.id.monthhistory_action);
        goal_txt_title.setText(snapofdetails.child("goal").getValue().toString());


        themonthgoalcom = Integer.parseInt(snapofdetails.child("community").child("goal").getValue().toString());
        themonthgoalplatincom = Integer.parseInt(snapofdetails.child("community").child("goalplatin").getValue().toString());
        themonthgoalblackcom = Integer.parseInt(snapofdetails.child("community").child("goalblack").getValue().toString());


        // SET DETAILS COMMUNITY

        int goalrechenvaluecom;
        String goaltextvaluecom;
        String monthbadgetoshowcom;
        if (Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < themonthgoalcom) {
            goalrechenvaluecom = themonthgoalcom;
            monthbadgetoshowcom = monthbadgedrawablegold;
            content_community_title_month.setText(String.format(getString(R.string.string_ebene_com), "Gold"));


        } else if (Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < themonthgoalplatincom) {
            goalrechenvaluecom = themonthgoalplatincom;
            monthbadgetoshowcom = monthbadgedrawableplatin;
            content_community_title_month.setText(String.format(getString(R.string.string_ebene_com), "Platin"));

        } else if (Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < themonthgoalblackcom) {
            goalrechenvaluecom = themonthgoalblackcom;
            monthbadgetoshowcom = monthbadgedrawable;

            content_community_title_month.setText(String.format(getString(R.string.string_ebene_com), "Onyx"));
        } else {
            goalrechenvaluecom = themonthgoalblackcom;
            monthbadgetoshowcom = monthbadgedrawable;


        }

        Log.i("RESULT COMM -->", "g " + goalrechenvaluecom + " badge " + monthbadgetoshowcom);
        goal_txt_content_community.setText("" + String.format(Locale.getDefault(), "%,d", goalrechenvaluecom));
        //String.format(Locale.getDefault(),"%,d", goalrechenvaluecom))
        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()))
        progress_txt_content_com.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString())));

        content_from_badge_month_com.setSelected(true);
        last_upload_val_txt_content.setSelected(true);
        togo_txt_content.setSelected(true);

        community_enl.setMax(goalrechenvaluecom);
        community_res.setMax(goalrechenvaluecom);
        String[] action_text2 = getResources().getStringArray(R.array.badge_text_actions_id);
        int action_value2 = Arrays.asList(action_text2).indexOf(monthbadgeid); // pass value

        String action_value_text_community2 = getResources().getStringArray(R.array.badge_text_actions_value_community)[action_value2];

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community_ebene), action_value_text_community2, "" + String.format(Locale.getDefault(), "%,d", goalrechenvaluecom), monthbadgeshort1), Html.FROM_HTML_MODE_COMPACT));

        } else {
            mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community_ebene), action_value_text_community2, "" + String.format(Locale.getDefault(), "%,d", goalrechenvaluecom), monthbadgeshort1)));


        }

        int badgemonthid_content_com = getResources().getIdentifier(monthbadgetoshowcom, "drawable", getPackageName());

        //badgemonth = getResources().obtainTypedArray(badgemonthid);
        if (badgemonthid_content_com != 0) {
            //   Log.w("BADGE -->", " id :"+badgemonthid_content);

            ImageView month_community_content = findViewById(R.id.content_community_month);
            month_community_content.setImageDrawable(getDrawable(badgemonthid_content_com));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            community_enl.setProgress(Integer.parseInt(snapofdetails.child("community").child("enl").getValue().toString()), true);
            community_res.setProgress(Integer.parseInt(snapofdetails.child("community").child("res").getValue().toString()), true);
        } else {
            community_enl.setProgress(Integer.parseInt(snapofdetails.child("community").child("enl").getValue().toString()));
            community_res.setProgress(Integer.parseInt(snapofdetails.child("community").child("res").getValue().toString()));
        }
        int togo_com = goalrechenvaluecom - Integer.parseInt(snapofdetails.child("community").child("current").getValue().toString());
        //togo_txt_content_com.setText("noch "+togo_com);
        //String.format(Locale.getDefault(),"%,d", togo_com)
        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()))
        if (togo_com <= 0) {
            togo_com = Math.abs(togo_com);
            togo_txt_content_com.setText("Completed (+ " + String.format(Locale.getDefault(), "%,d", togo_com) + ")");
        } else {
            togo_txt_content_com.setText("noch " + String.format(Locale.getDefault(), "%,d", togo_com));
        }
        rank_text_content_com.setText("Agents: " + snapofdetails.child("agents").getValue().toString());
        if (hasxf) {
            // TODO HAS XF = COMMUNITY MISSION
        }

        togo_txt_content_com.setSelected(true);

        // GET TEAM TODO
        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotteam) {
                //   Log.i("SNAPP IS ", snapshotteam.child("email").getValue().toString() + "");
                if (snapshotteam.hasChild("xtra")) {
                    Log.i("HAS XTRA", "yepp");

                    pref.edit().putBoolean("xtra", Boolean.parseBoolean(snapshotteam.child("xtra").getValue().toString())).apply();
                } else {
                    Log.i("HAS NO XTRA", "yepp");
                    pref.edit().putBoolean("xtra", false).apply();
                }
                if (snapshotteam.child("reset").hasChild("month")) {
                    content_request_btn_month_manuell.setVisibility(View.VISIBLE);
                }
                if (snapshotteam.hasChild("team")) {
                    Log.i("TEAM-->", "Team gefunden");
                    //    team_month_exist.setVisibility(View.INVISIBLE);
                    team_swipe_month.setVisibility(View.VISIBLE);
                    swipe_team_month.setLockDrag(false);
                    pref.edit().putBoolean("hasteam", true).apply();
                    // TODO INIT TEAM
                    // initmonth(theyear,themonth, true);


                } else {
                    Log.i("TEAM -->", "Kein TEAM gefunden");
                    // initmonth(theyear,themonth, false);
                    team_swipe_month.setVisibility(View.GONE);
                    swipe_team_month.setLockDrag(true);
                    swipe_layout_community.setLockDrag(false);

                    pref.edit().putBoolean("hasteam", false).apply();

                    //  team_month_not_exist.setVisibility(View.INVISIBLE);


                }

                // SUCHE FREUNDE
                if (snapshotteam.hasChild("friends")) {
                    Log.i("Friends-->", "Freunde gefunden");
                    pref.edit().putBoolean("hasfriends", true).apply();


                } else {
                    Log.i("Friends -->", "Keine Freunde gefunden");

                    pref.edit().putBoolean("hasfriends", false).apply();


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
        // GET AGENTSDATA FOR THE MISSION
        Query myTopPostsQuery = mDatabaserunnung.child(theyear).child("month").child(theid).orderByChild("currentvalue");
        final HashMap<String, String> data = new HashMap<String, String>();
        final List<String> listUID = new ArrayList();
        final List<Long> rankUID = new ArrayList();

        // RANKING FOR THE AGENT
        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot child : snapshot.getChildren()) {


                    rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));


                    String test = child.child("uid").getValue().toString();
                    listUID.add(test);

                    //  Log.i("TEST LOG--->",child.getValue().toString());

                }
                Collections.reverse(listUID);
                Collections.reverse(rankUID);


                //  mList.indexOf(user.getUid())
                int ownpositioninList = listUID.indexOf(user.getUid());
                int ownrankis = 0;
                if (ownpositioninList == -1) {

                    rank_txt_title.setText("-");
                    rank_text_content.setText(getString(R.string.rang) + ": ");
                } else {
                    for (int i = 0; i < rankUID.size(); i++) {
                        //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                        if (i == 0) {
                            ownrankis++;
                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                if (rankUID.get(i) > 0) {
                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    rank_txt_title.setText("" + ownrankis);
                                    rank_text_content.setText(getString(R.string.rang) + ": " + ownrankis);
                                } else {
                                    rank_txt_title.setText("-");
                                    rank_text_content.setText(getString(R.string.rang) + ": -");
                                }
                            }
                        } else {
                            if (rankUID.get(i) > rankUID.get(ownpositioninList)) {
                                ownrankis++;

                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title.setText("" + ownrankis);
                                        rank_text_content.setText(getString(R.string.rang) + ": " + ownrankis);
                                    } else {
                                        rank_txt_title.setText("-");
                                        rank_text_content.setText(getString(R.string.rang) + ": -");
                                    }
                                }
                            } else {
                                ownrankis++;
                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title.setText("" + ownrankis);
                                        rank_text_content.setText(getString(R.string.rang) + ": " + ownrankis);
                                    } else {
                                        rank_txt_title.setText("-");
                                        rank_text_content.setText(getString(R.string.rang) + ": -");
                                    }
                                }
                            }

                        }
                    }
                    //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                    if (ownrankis <= 0) {

                        rank_txt_title.setText("-");
                        rank_text_content.setText(getString(R.string.rang) + ": -");
                    }

                }

                //  mDatabaserunnung.child(theyear).child("month").child(theid)
                mDatabaserunnung.child(theyear).child("month").child(theid).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshotmissionagent) {
                        int goalrechenvalue;
                        String goaltextvalue;
                        String monthbadgetoshow;

                        if (Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalgold) {
                            goalrechenvalue = goalgold;
                            monthbadgetoshow = monthbadgedrawablegold;
                            content_avatar_title_month.setText(String.format(getString(R.string.string_ebene), "Gold"));
                            status_agent.setMax(goalgold);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));

                        } else if (Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalplatin) {
                            goalrechenvalue = goalplatin;
                            monthbadgetoshow = monthbadgedrawableplatin;
                            content_avatar_title_month.setText(String.format(getString(R.string.string_ebene), "Platin"));
                            status_agent.setMax(goalplatin);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        } else if (Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalblack) {
                            goalrechenvalue = goalblack;
                            monthbadgetoshow = monthbadgedrawable;
                            content_avatar_title_month.setText(String.format(getString(R.string.string_ebene), "Onyx"));
                            status_agent.setMax(goalblack);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        } else {
                            goalrechenvalue = goalblack;
                            monthbadgetoshow = monthbadgedrawable;
                            status_agent.setMax(goalblack);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        }
                        goaltextvalue = String.valueOf(goalrechenvalue);


                        Long restvalue = (goalrechenvalue) - Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString());
                        //String.format(Locale.getDefault(),"%,d", goaltextvalue)
                        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()))
                        goal_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(goaltextvalue)));
                        goal_txt_title.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(goaltextvalue)));

                        if (restvalue <= 0) {
                            restvalue = Math.abs(restvalue);
                            togo_txt_content.setText(getString(R.string.txt_completed) + " (+ " + String.format(Locale.getDefault(), "%,d", restvalue) + ")");
                        } else {
                            togo_txt_content.setText(getString(R.string.txt_another) + " " + String.format(Locale.getDefault(), "%,d", restvalue));
                        }
                        //  togo_txt_content.setText("noch "+restvalue);
                        progress_txt_title.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString())));
                        startwert_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("startvalue").getValue().toString())));
                        short2_txt_content.setText(monthbadgeshort2);
                        short2_txt_content.setSelected(true);
                        title_txt_content.setText(monthbadgetitle);
                        title_txt_community.setText(monthbadgetitle);
                        //String.format(Locale.getDefault(),"%,d", goaltextvalue)
                        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()))
                        progress_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString())));

                        int badgemonthid_content = getResources().getIdentifier(monthbadgetoshow, "drawable", getPackageName());

                        //badgemonth = getResources().obtainTypedArray(badgemonthid);
                        if (badgemonthid_content != 0) {
                            //   Log.w("BADGE -->", " id :"+badgemonthid_content);
                            ImageView month_badge_content = findViewById(R.id.content_avatar_month);
                            month_badge_content.setImageDrawable(getDrawable(badgemonthid_content));

                        }


                        String[] action_text = getResources().getStringArray(R.array.badge_text_actions_id);
                        int action_value = Arrays.asList(action_text).indexOf(monthbadgeid); // pass value
                        String action_value_text = getResources().getStringArray(R.array.badge_text_actions_value)[action_value];
                        //   String action_value_text_community = getResources().getStringArray(R.array.badge_text_actions_value_community)[action_value];


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            //    mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community), action_value_text_community, snapofdetails.child("community").child("goal").getValue().toString(), monthbadgeshort1), Html.FROM_HTML_MODE_COMPACT));
                            monthetextslice = String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), monthbadgeshort1);
                            mission_detals_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), monthbadgeshort1), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            //   mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community), action_value_text_community, snapofdetails.child("community").child("goal").getValue().toString(), monthbadgeshort1)));

                            mission_detals_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), monthbadgeshort1)));
                            monthetextslice = String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), monthbadgeshort1);
                        }
                        boolean hasoneupload;
                        hasoneupload = Integer.parseInt(snapshotmissionagent.child("lastvalue").getValue().toString()) > 0;
                        if (hasoneupload) {
                            last_upload_val_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("lastvalue").getValue().toString())) + " " + monthbadgeshort1);
                        } else {
                            monthhistory_action.setVisibility(View.GONE);
                            last_upload_val_txt_content.setText(" - ");
                        }

                        int difftofinal = Calendar.getInstance(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH) - Calendar.getInstance(Locale.getDefault()).get(Calendar.DAY_OF_MONTH);
                        Log.i("MONTHS -->", Calendar.getInstance(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH) + " " + Calendar.getInstance(Locale.getDefault()).get(Calendar.DAY_OF_MONTH));
                        String textrunsday;
                        if (difftofinal <= 1) {
                            if (difftofinal <= 0) {
                                textrunsday = getString(R.string.runtime_today) + "";
                            } else {
                                textrunsday = getString(R.string.runtime_oneday) + "";
                            }
                            String givenDateString = Calendar.getInstance(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH) + "." + themonth + "." + theyear + " 23:59:59";
                            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                            try {
                                Date mDate = sdf.parse(givenDateString);
                                long timeInMilliseconds = mDate.getTime();
                                Date currentDateis = new Date();
                                System.out.println("Date in milli :: " + timeInMilliseconds);
                                long diffInMs = mDate.getTime() - currentDateis.getTime();

                                System.out.println("diff:: " + diffInMs + " " + mDate.toString());
                                timer_month = new MonthFinish(diffInMs, 1000);
                                timer_month.start();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }


                        } else {
                            textrunsday = difftofinal + " " + getString(R.string.string_days);
                            runtime_date_content.setText(getString(R.string.string_until) + " " + Calendar.getInstance(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH) + "." + themonth + ". 23:59 " + getResources().getString(R.string.string_clock));

                        }
                        deadlinetime_content.setText(textrunsday);


                        if (hasoneupload) {
                            ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            fcmonth.setLayoutParams(params);
                            fcmonth.requestLayout();
                            monthhistory_action.setVisibility(View.VISIBLE);
                            monthhistory_action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    openHistory(theyear, themonth, theid, "month", AODMain.this, null, -1);
                                    // infoToast("soon :-)");
                                }
                            });
                            Date oldDate = new Date((Long.parseLong(snapshotmissionagent.child("timestamp").getValue().toString()) * 1000));
                            Date currentDate = new Date();

                            long diff = currentDate.getTime() - oldDate.getTime();
                            long seconds = diff / 1000;
                            long minutes = seconds / 60;
                            long hours = minutes / 60;
                            long days = hours / 24;

                            if (oldDate.before(currentDate)) {

                                Log.e("oldDate", "is previous date");
                                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                                        + " hours: " + hours + " days: " + days);
                                if (days >= 1) {
                                    if (days == 1) {

                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_day), "" + days));
                                    } else {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_days), "" + days));
                                    }
                                } else if (hours >= 1) {
                                    if (hours == 1) {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_hour), "" + hours));
                                    } else if (hours > 1 && hours < 23) {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_hours), "" + hours));
                                    } else {
                                        lastupload_time_content.setText(getString(R.string.string_oneday));
                                    }
                                } else if (minutes >= 0) {
                                    if (minutes == 0) {
                                        lastupload_time_content.setText(getString(R.string.string_justnow));
                                    } else if (minutes >= 1 && minutes <= 5) {
                                        lastupload_time_content.setText(getString(R.string.string_justnow));
                                    } else {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_minutes), "" + minutes));
                                    }
                                }
                            } else {
                                lastupload_time_content.setText(getString(R.string.string_nouplaod));
                            }
                        } else {
                            rank_text_content.setText(getString(R.string.rang) + ": -");
                            lastupload_time_content.setText(getString(R.string.string_nouplaod));
                        }

                        lastupload_time_content.setSelected(true);
                        // Log.e("toyBornTime", "" + toyBornTime);

                        //  monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield;

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

        monthnotstartet.setVisibility(View.GONE);
        monthstartet.setVisibility(View.VISIBLE);

        fcmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fcmonth.toggle(false);

                if (!fcmonth.isUnfolded()) {
                    final long now = SystemClock.uptimeMillis();
                    final MotionEvent pressEvent = MotionEvent.obtain(now, now, MotionEvent.ACTION_DOWN, fcmonth.getWidth() / 2, fcmonth.getHeight() / 2, 0);


                    swipe_layout_community.onTouchEvent(pressEvent);
                }


            }
        });
        // TODO GET DETAILS MONTH
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                frameloadmonth.setVisibility(View.GONE);
                framecardmonth.setVisibility(View.VISIBLE);

            }
        }, 1200);
    }

    private void earthgetagentDetails(final String theid, final DataSnapshot snapofdetails, final String theyear, final String themonth) {
        Log.i("THE ID OF EVENT -->", theid);
        Log.i("THE GOAL OF EVENT -->", snapofdetails.child("goal").getValue().toString());
        // INIT DETAILS
        boolean hasranking = Boolean.parseBoolean(snapofdetails.child("ranking").getValue().toString());
        boolean hasprice = Boolean.parseBoolean(snapofdetails.child("price").getValue().toString());
        boolean hasteam = Boolean.parseBoolean(snapofdetails.child("team").getValue().toString());
        boolean hasduell = Boolean.parseBoolean(snapofdetails.child("duell").getValue().toString());
        boolean hasxf = Boolean.parseBoolean(snapofdetails.child("xf").getValue().toString());
        int activeagents = Integer.parseInt(snapofdetails.child("agents").getValue().toString());
        goalgold_earth = Integer.parseInt(snapofdetails.child("goal").getValue().toString());
        goalplatin_earth = Integer.parseInt(snapofdetails.child("goalplatin").getValue().toString());
        goalblack_earth = Integer.parseInt(snapofdetails.child("goalblack").getValue().toString());


        // INIT TEXT VIEWS
        final TextView goal_txt_title = findViewById(R.id.title_pledge_earth);
        final TextView content_avatar_title_month = findViewById(R.id.content_avatar_title_earth);
        final TextView content_community_title_month = findViewById(R.id.content_community_title_earth);
        final TextView rank_txt_title = findViewById(R.id.title_weight_earth);
        final TextView rank_txt_title_action = findViewById(R.id.rang_title_action_earth);
        rank_txt_title.setOnClickListener(this);

        final TextView progress_txt_title = findViewById(R.id.title_requests_count_earth);
        final TextView startwert_txt_content = findViewById(R.id.content_from_address_1_earth);
        final TextView short2_txt_content = findViewById(R.id.content_from_address_2_earth);
        final TextView rank_text_content = findViewById(R.id.content_to_address_2_earth);
        final TextView rank_text_content_com = findViewById(R.id.content_to_address_2_earth_com);
        final TextView goal_txt_content = findViewById(R.id.content_to_go_1_earth);
        final TextView togo_txt_content = findViewById(R.id.content_to_go_2_earth);
        final TextView togo_txt_content_com = findViewById(R.id.content_to_go_2_earth_com);
        final TextView title_txt_content = findViewById(R.id.content_name_view_earth);
        final TextView title_txt_community = findViewById(R.id.content_community_view_earth);
        final TextView mission_detals_content_txt_month = findViewById(R.id.mission_detals_content_txt_earth);
        final TextView mission_community_content_txt_month = findViewById(R.id.mission_community_content_txt_earth);
        final TextView progress_txt_content = findViewById(R.id.content_to_address_1_earth);
        final TextView goal_txt_content_community = findViewById(R.id.content_to_go_1_earth_com);
        final TextView progress_txt_content_com = findViewById(R.id.content_to_address_1_earth_com);
        final TextView last_upload_val_txt_content = findViewById(R.id.content_delivery_time_earth);
        final TextView deadlinetime_content = findViewById(R.id.content_deadline_time_earth);
        final TextView runtime_date_content = findViewById(R.id.content_deadline_date_earth);
        final TextView lastupload_time_content = findViewById(R.id.content_delivery_date_earth);
        final TextView content_from_badge_month_com = findViewById(R.id.content_from_badge_earth);
        final ProgressBar community_enl = findViewById(R.id.statusenl_earth);
        final ProgressBar community_res = findViewById(R.id.statusres_earth);
        final ProgressBar status_agent = findViewById(R.id.statusagent_earth);
        final ImageView team_swipe_month = findViewById(R.id.teamswipe_earth);

        final RelativeLayout monthhistory_action = findViewById(R.id.earthhistory_action);
        goal_txt_title.setText(snapofdetails.child("goal").getValue().toString());


        theearthgoalcom = Integer.parseInt(snapofdetails.child("community").child("goal").getValue().toString());
        theearthgoalplatincom = Integer.parseInt(snapofdetails.child("community").child("goalplatin").getValue().toString());
        theearthgoalblackcom = Integer.parseInt(snapofdetails.child("community").child("goalblack").getValue().toString());
        theearthgoalniacom = Integer.parseInt(snapofdetails.child("community").child("goalnia").getValue().toString());


        // SET DETAILS COMMUNITY

        int goalrechenvaluecom;
        String goaltextvaluecom;
        String monthbadgetoshowcom;
        if (Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < theearthgoalcom) {
            goalrechenvaluecom = theearthgoalcom;
            monthbadgetoshowcom = earthbadgedrawablegold;
            content_community_title_month.setText(String.format(getString(R.string.string_ebene_com), "Gold"));


        } else if (Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < theearthgoalplatincom) {
            goalrechenvaluecom = theearthgoalplatincom;
            monthbadgetoshowcom = earthbadgedrawableplatin;
            content_community_title_month.setText(String.format(getString(R.string.string_ebene_com), "Platin"));

        } else if (Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < theearthgoalblackcom) {
            goalrechenvaluecom = theearthgoalblackcom;
            monthbadgetoshowcom = earthbadgedrawable;

            content_community_title_month.setText(String.format(getString(R.string.string_ebene_com), "Onyx"));
        } else {
            goalrechenvaluecom = theearthgoalblackcom;
            monthbadgetoshowcom = earthbadgedrawable;


        }

        Log.i("RESULT COMM -->", "g " + goalrechenvaluecom + " badge " + monthbadgetoshowcom);
        goal_txt_content_community.setText("" + String.format(Locale.getDefault(), "%,d", goalrechenvaluecom));
        //String.format(Locale.getDefault(),"%,d", goalrechenvaluecom))
        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()))
        progress_txt_content_com.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString())));

        content_from_badge_month_com.setSelected(true);
        last_upload_val_txt_content.setSelected(true);
        togo_txt_content.setSelected(true);

        community_enl.setMax(goalrechenvaluecom);
        community_res.setMax(goalrechenvaluecom);
        String[] action_text2 = getResources().getStringArray(R.array.badge_text_actions_id);
        int action_value2 = Arrays.asList(action_text2).indexOf(earthbadgeid); // pass value

        String action_value_text_community2 = getResources().getStringArray(R.array.badge_text_actions_value_community)[action_value2];

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community_ebene), action_value_text_community2, "" + String.format(Locale.getDefault(), "%,d", goalrechenvaluecom), earthbadgeshort1), Html.FROM_HTML_MODE_COMPACT));

        } else {
            mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community_ebene), action_value_text_community2, "" + String.format(Locale.getDefault(), "%,d", goalrechenvaluecom), earthbadgeshort1)));


        }

        int badgemonthid_content_com = getResources().getIdentifier(monthbadgetoshowcom, "drawable", getPackageName());

        //badgemonth = getResources().obtainTypedArray(badgemonthid);
        if (badgemonthid_content_com != 0) {
            //   Log.w("BADGE -->", " id :"+badgemonthid_content);

            ImageView month_community_content = findViewById(R.id.content_community_earth);
            month_community_content.setImageDrawable(getDrawable(badgemonthid_content_com));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            community_enl.setProgress(Integer.parseInt(snapofdetails.child("community").child("enl").getValue().toString()), true);
            community_res.setProgress(Integer.parseInt(snapofdetails.child("community").child("res").getValue().toString()), true);
        } else {
            community_enl.setProgress(Integer.parseInt(snapofdetails.child("community").child("enl").getValue().toString()));
            community_res.setProgress(Integer.parseInt(snapofdetails.child("community").child("res").getValue().toString()));
        }
        int togo_com = goalrechenvaluecom - Integer.parseInt(snapofdetails.child("community").child("current").getValue().toString());
        //togo_txt_content_com.setText("noch "+togo_com);
        //String.format(Locale.getDefault(),"%,d", togo_com)
        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()))
        if (togo_com <= 0) {
            togo_com = Math.abs(togo_com);
            togo_txt_content_com.setText("Completed (+ " + String.format(Locale.getDefault(), "%,d", togo_com) + ")");
        } else {
            togo_txt_content_com.setText("noch " + String.format(Locale.getDefault(), "%,d", togo_com));
        }
        rank_text_content_com.setText("Agents: " + snapofdetails.child("agents").getValue().toString());
        if (hasxf) {
            // TODO HAS XF = COMMUNITY MISSION
        }

        togo_txt_content_com.setSelected(true);

        // GET TEAM TODO
        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotteam) {
                //   Log.i("SNAPP IS ", snapshotteam.child("email").getValue().toString() + "");
                if (snapshotteam.hasChild("xtra")) {
                    Log.i("HAS XTRA", "yepp");

                    pref.edit().putBoolean("xtra", Boolean.parseBoolean(snapshotteam.child("xtra").getValue().toString())).apply();
                } else {
                    Log.i("HAS NO XTRA", "yepp");
                    pref.edit().putBoolean("xtra", false).apply();
                }
                if (snapshotteam.child("reset").hasChild("month")) {
                    content_request_btn_earth_manuell.setVisibility(View.VISIBLE);
                }
                if (snapshotteam.hasChild("team")) {
                    Log.i("TEAM-->", "Team gefunden");
                    //    team_month_exist.setVisibility(View.INVISIBLE);
                    team_swipe_month.setVisibility(View.VISIBLE);
                    swipe_team_earth.setLockDrag(false);
                    pref.edit().putBoolean("hasteam", true).apply();
                    // TODO INIT TEAM
                    // initmonth(theyear,themonth, true);


                } else {
                    Log.i("TEAM -->", "Kein TEAM gefunden");
                    // initmonth(theyear,themonth, false);
                    team_swipe_month.setVisibility(View.GONE);
                    swipe_team_earth.setLockDrag(true);
                    swipe_layout_community_earth.setLockDrag(false);

                    pref.edit().putBoolean("hasteam", false).apply();

                    //  team_month_not_exist.setVisibility(View.INVISIBLE);


                }

                // SUCHE FREUNDE
                if (snapshotteam.hasChild("friends")) {
                    Log.i("Friends-->", "Freunde gefunden");
                    pref.edit().putBoolean("hasfriends", true).apply();


                } else {
                    Log.i("Friends -->", "Keine Freunde gefunden");

                    pref.edit().putBoolean("hasfriends", false).apply();


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
        // GET AGENTSDATA FOR THE MISSION
        Query myTopPostsQuery = mDatabaserunnung.child(theyear).child("earth").child(theid).orderByChild("currentvalue");
        final HashMap<String, String> data = new HashMap<String, String>();
        final List<String> listUID = new ArrayList();
        final List<Long> rankUID = new ArrayList();

        // RANKING FOR THE AGENT
        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot child : snapshot.getChildren()) {


                    rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));


                    String test = child.child("uid").getValue().toString();
                    listUID.add(test);

                    //  Log.i("TEST LOG--->",child.getValue().toString());

                }
                Collections.reverse(listUID);
                Collections.reverse(rankUID);


                //  mList.indexOf(user.getUid())
                int ownpositioninList = listUID.indexOf(user.getUid());
                int ownrankis = 0;
                if (ownpositioninList == -1) {

                    rank_txt_title.setText("-");
                    rank_text_content.setText(getString(R.string.rang) + ": ");
                } else {
                    for (int i = 0; i < rankUID.size(); i++) {
                        //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                        if (i == 0) {
                            ownrankis++;
                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                if (rankUID.get(i) > 0) {
                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    rank_txt_title.setText("" + ownrankis);
                                    rank_text_content.setText(getString(R.string.rang) + ": " + ownrankis);
                                } else {
                                    rank_txt_title.setText("-");
                                    rank_text_content.setText(getString(R.string.rang) + ": -");
                                }
                            }
                        } else {
                            if (rankUID.get(i) > rankUID.get(ownpositioninList)) {
                                ownrankis++;

                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title.setText("" + ownrankis);
                                        rank_text_content.setText(getString(R.string.rang) + ": " + ownrankis);
                                    } else {
                                        rank_txt_title.setText("-");
                                        rank_text_content.setText(getString(R.string.rang) + ": -");
                                    }
                                }
                            } else {
                                ownrankis++;
                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title.setText("" + ownrankis);
                                        rank_text_content.setText(getString(R.string.rang) + ": " + ownrankis);
                                    } else {
                                        rank_txt_title.setText("-");
                                        rank_text_content.setText(getString(R.string.rang) + ": -");
                                    }
                                }
                            }

                        }
                    }
                    //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                    if (ownrankis <= 0) {

                        rank_txt_title.setText("-");
                        rank_text_content.setText(getString(R.string.rang) + ": -");
                    }

                }

                //  mDatabaserunnung.child(theyear).child("month").child(theid)
                mDatabaserunnung.child(theyear).child("earth").child(theid).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshotmissionagent) {
                        int goalrechenvalue;
                        String goaltextvalue;
                        String monthbadgetoshow;

                        if (Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalgold_earth) {
                            goalrechenvalue = goalgold_earth;
                            monthbadgetoshow = earthbadgedrawablegold;
                            content_avatar_title_month.setText(String.format(getString(R.string.string_ebene), "Gold"));
                            status_agent.setMax(goalgold_earth);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));

                        } else if (Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalplatin_earth) {
                            goalrechenvalue = goalplatin_earth;
                            monthbadgetoshow = earthbadgedrawableplatin;
                            content_avatar_title_month.setText(String.format(getString(R.string.string_ebene), "Platin"));
                            status_agent.setMax(goalplatin_earth);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        } else if (Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalblack_earth) {
                            goalrechenvalue = goalblack_earth;
                            monthbadgetoshow = earthbadgedrawable;
                            content_avatar_title_month.setText(String.format(getString(R.string.string_ebene), "Onyx"));
                            status_agent.setMax(goalblack_earth);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        } else {
                            goalrechenvalue = goalblack_earth;
                            monthbadgetoshow = earthbadgedrawable;
                            status_agent.setMax(goalblack_earth);
                            status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        }
                        goaltextvalue = String.valueOf(goalrechenvalue);


                        Long restvalue = (goalrechenvalue) - Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString());
                        //String.format(Locale.getDefault(),"%,d", goaltextvalue)
                        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()))
                        goal_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(goaltextvalue)));
                        goal_txt_title.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(goaltextvalue)));

                        if (restvalue <= 0) {
                            restvalue = Math.abs(restvalue);
                            togo_txt_content.setText(getString(R.string.txt_completed) + " (+ " + String.format(Locale.getDefault(), "%,d", restvalue) + ")");
                        } else {
                            togo_txt_content.setText(getString(R.string.txt_another) + " " + String.format(Locale.getDefault(), "%,d", restvalue));
                        }
                        //  togo_txt_content.setText("noch "+restvalue);
                        progress_txt_title.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString())));
                        startwert_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("startvalue").getValue().toString())));
                        short2_txt_content.setText(earthbadgeshort2);
                        short2_txt_content.setSelected(true);
                        title_txt_content.setText(earthbadgetitle);
                        title_txt_community.setText(earthbadgetitle);
                        //String.format(Locale.getDefault(),"%,d", goaltextvalue)
                        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()))
                        progress_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString())));

                        int badgemonthid_content = getResources().getIdentifier(monthbadgetoshow, "drawable", getPackageName());

                        //badgemonth = getResources().obtainTypedArray(badgemonthid);
                        if (badgemonthid_content != 0) {
                            //   Log.w("BADGE -->", " id :"+badgemonthid_content);
                            ImageView month_badge_content = findViewById(R.id.content_avatar_earth);
                            month_badge_content.setImageDrawable(getDrawable(badgemonthid_content));

                        }


                        String[] action_text = getResources().getStringArray(R.array.badge_text_actions_id);
                        int action_value = Arrays.asList(action_text).indexOf(earthbadgeid); // pass value
                        String action_value_text = getResources().getStringArray(R.array.badge_text_actions_value)[action_value];
                        //   String action_value_text_community = getResources().getStringArray(R.array.badge_text_actions_value_community)[action_value];


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            //    mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community), action_value_text_community, snapofdetails.child("community").child("goal").getValue().toString(), monthbadgeshort1), Html.FROM_HTML_MODE_COMPACT));
                            monthetextslice = String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), earthbadgeshort1);
                            mission_detals_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), earthbadgeshort1), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            //   mission_community_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community), action_value_text_community, snapofdetails.child("community").child("goal").getValue().toString(), monthbadgeshort1)));

                            mission_detals_content_txt_month.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), earthbadgeshort1)));
                            monthetextslice = String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), earthbadgeshort1);
                        }
                        boolean hasoneupload;
                        hasoneupload = Integer.parseInt(snapshotmissionagent.child("lastvalue").getValue().toString()) > 0;
                        if (hasoneupload) {
                            last_upload_val_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("lastvalue").getValue().toString())) + " " + earthbadgeshort1);
                        } else {
                            monthhistory_action.setVisibility(View.GONE);
                            last_upload_val_txt_content.setText(" - ");
                        }

                        int difftofinal = Calendar.getInstance(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH) - Calendar.getInstance(Locale.getDefault()).get(Calendar.DAY_OF_MONTH);
                        Log.i("MONTHS -->", Calendar.getInstance(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH) + " " + Calendar.getInstance(Locale.getDefault()).get(Calendar.DAY_OF_MONTH));
                        String textrunsday;
                       /* if (difftofinal <= 1) {
                            if (difftofinal <= 0) {
                                textrunsday = getString(R.string.runtime_today) + "";
                            } else {
                                textrunsday = getString(R.string.runtime_oneday) + "";
                            }
                            String givenDateString = Calendar.getInstance(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH) + "." + themonth + "." + theyear + " 23:59:59";
                            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                            try {
                                Date mDate = sdf.parse(givenDateString);
                                long timeInMilliseconds = mDate.getTime();
                                Date currentDateis = new Date();
                                System.out.println("Date in milli :: " + timeInMilliseconds);
                                long diffInMs = mDate.getTime() - currentDateis.getTime();

                                System.out.println("diff:: " + diffInMs + " " + mDate.toString());
                                timer_month = new MonthFinish(diffInMs, 1000);
                                timer_month.start();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }


                        } else {
                            textrunsday = difftofinal + " " + getString(R.string.string_days);
                            runtime_date_content.setText(getString(R.string.string_until) + " " + Calendar.getInstance(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH) + "." + themonth + ". 23:59 " + getResources().getString(R.string.string_clock));

                        }
                        deadlinetime_content.setText(textrunsday);*/
                        deadlinetime_content.setText("20th September 1700 UTC");

                        if (hasoneupload) {
                            ViewGroup.LayoutParams params = fcearth.getLayoutParams();
                            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            fcearth.setLayoutParams(params);
                            fcearth.requestLayout();
                            monthhistory_action.setVisibility(View.VISIBLE);
                            monthhistory_action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    openHistory(theyear, themonth, theid, "earth", AODMain.this, null, -1);
                                    // infoToast("soon :-)");
                                }
                            });
                            Date oldDate = new Date((Long.parseLong(snapshotmissionagent.child("timestamp").getValue().toString()) * 1000));
                            Date currentDate = new Date();

                            long diff = currentDate.getTime() - oldDate.getTime();
                            long seconds = diff / 1000;
                            long minutes = seconds / 60;
                            long hours = minutes / 60;
                            long days = hours / 24;

                            if (oldDate.before(currentDate)) {

                                Log.e("oldDate", "is previous date");
                                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                                        + " hours: " + hours + " days: " + days);
                                if (days >= 1) {
                                    if (days == 1) {

                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_day), "" + days));
                                    } else {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_days), "" + days));
                                    }
                                } else if (hours >= 1) {
                                    if (hours == 1) {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_hour), "" + hours));
                                    } else if (hours > 1 && hours < 23) {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_hours), "" + hours));
                                    } else {
                                        lastupload_time_content.setText(getString(R.string.string_oneday));
                                    }
                                } else if (minutes >= 0) {
                                    if (minutes == 0) {
                                        lastupload_time_content.setText(getString(R.string.string_justnow));
                                    } else if (minutes >= 1 && minutes <= 5) {
                                        lastupload_time_content.setText(getString(R.string.string_justnow));
                                    } else {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_minutes), "" + minutes));
                                    }
                                }
                            } else {
                                lastupload_time_content.setText(getString(R.string.string_nouplaod));
                            }
                        } else {
                            rank_text_content.setText(getString(R.string.rang) + ": -");
                            lastupload_time_content.setText(getString(R.string.string_nouplaod));
                        }

                        lastupload_time_content.setSelected(true);
                        // Log.e("toyBornTime", "" + toyBornTime);

                        //  monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield;

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

        earthnotstartet.setVisibility(View.GONE);
        earthstartet.setVisibility(View.VISIBLE);

        fcearth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fcearth.toggle(false);

                if (!fcearth.isUnfolded()) {
                    final long now = SystemClock.uptimeMillis();
                    final MotionEvent pressEvent = MotionEvent.obtain(now, now, MotionEvent.ACTION_DOWN, fcmonth.getWidth() / 2, fcmonth.getHeight() / 2, 0);


                    swipe_layout_community_earth.onTouchEvent(pressEvent);
                }


            }
        });
        // TODO GET DETAILS MONTH
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                frameloadearth.setVisibility(View.GONE);
                framecardearth.setVisibility(View.VISIBLE);

            }
        }, 1200);
    }

    // WEEK AGENT DETAILS
    private void weekgetagentDetails(final String theid, final DataSnapshot snapofdetails, final String theyear, final String theweek) {
        Log.i("THE ID OF EVENT -->", theid);
        Log.i("THE GOAL OF EVENT -->", snapofdetails.child("goal").getValue().toString());
        // INIT DETAILS
        boolean hasranking_week = Boolean.parseBoolean(snapofdetails.child("ranking").getValue().toString());
        boolean hasprice_week = Boolean.parseBoolean(snapofdetails.child("price").getValue().toString());
        boolean hasteam_week = Boolean.parseBoolean(snapofdetails.child("team").getValue().toString());
        boolean hasduell_week = Boolean.parseBoolean(snapofdetails.child("duell").getValue().toString());
        boolean hasxf_week = Boolean.parseBoolean(snapofdetails.child("xf").getValue().toString());
        int activeagents_week = Integer.parseInt(snapofdetails.child("agents").getValue().toString());
        goalgold_week = Integer.parseInt(snapofdetails.child("goal").getValue().toString());
        goalplatin_week = Integer.parseInt(snapofdetails.child("goalplatin").getValue().toString());
        goalblack_week = Integer.parseInt(snapofdetails.child("goalblack").getValue().toString());


        // INIT TEXT VIEWS
        final TextView goal_txt_title_week = findViewById(R.id.title_pledge_week);
        final TextView content_avatar_title_week = findViewById(R.id.content_avatar_title_week);
        final TextView content_community_title_week = findViewById(R.id.content_community_title_week);
        final TextView rank_txt_title_week = findViewById(R.id.title_weight_week);
        final TextView rank_txt_title_action_week = findViewById(R.id.rang_title_action_week);
        rank_txt_title_week.setOnClickListener(this);

        final TextView progress_txt_title_week = findViewById(R.id.title_requests_count_week);
        final TextView startwert_txt_content_week = findViewById(R.id.content_from_address_1_week);
        final TextView short2_txt_content_week = findViewById(R.id.content_from_address_2_week);
        final TextView rank_text_content_week = findViewById(R.id.content_to_address_2_week);
        final TextView rank_text_content_com_week = findViewById(R.id.content_to_address_2_week_com);
        final TextView goal_txt_content_week = findViewById(R.id.content_to_go_1_week);
        final TextView togo_txt_content_week = findViewById(R.id.content_to_go_2_week);
        final TextView togo_txt_content_com_week = findViewById(R.id.content_to_go_2_week_com);
        final TextView title_txt_content_week = findViewById(R.id.content_name_view_week);
        final TextView title_txt_community_week = findViewById(R.id.content_community_view_week);
        final TextView mission_detals_content_txt_week = findViewById(R.id.mission_detals_content_txt_week);
        final TextView mission_community_content_txt_week = findViewById(R.id.mission_community_content_txt_week);
        final TextView progress_txt_content_week = findViewById(R.id.content_to_address_1_week);
        final TextView goal_txt_content_community_week = findViewById(R.id.content_to_go_1_week_com);
        final TextView progress_txt_content_com_week = findViewById(R.id.content_to_address_1_week_com);
        final TextView last_upload_val_txt_content_week = findViewById(R.id.content_delivery_time_week);
        final TextView deadlinetime_content_week = findViewById(R.id.content_deadline_time_week);
        final TextView runtime_date_content_week = findViewById(R.id.content_deadline_date_week);
        final TextView lastupload_time_content_week = findViewById(R.id.content_delivery_date_week);
        final TextView content_from_badge_week_com = findViewById(R.id.content_from_badge_week_com);
        final ProgressBar community_enl_week = findViewById(R.id.statusenl_week);
        final ProgressBar community_res_week = findViewById(R.id.statusres_week);
        final ProgressBar status_agent_week = findViewById(R.id.statusagent_week);
        final ImageView team_swipe_week = findViewById(R.id.teamswipe_week);
        final RelativeLayout hasteam_frame_month = findViewById(R.id.hasteam_frame);

        final RelativeLayout weekhistory_action = findViewById(R.id.weekhistory_action);
        goal_txt_title_week.setText(snapofdetails.child("goal").getValue().toString());


        theweekgoalcom = Integer.parseInt(snapofdetails.child("community").child("goal").getValue().toString());
        theweekgoalplatincom = Integer.parseInt(snapofdetails.child("community").child("goalplatin").getValue().toString());
        theweekgoalblackcom = Integer.parseInt(snapofdetails.child("community").child("goalblack").getValue().toString());


        // SET DETAILS COMMUNITY

        int goalrechenvaluecom_week;
        String goaltextvaluecom_week;
        String weekbadgetoshowcom;
        if (Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < theweekgoalcom) {
            goalrechenvaluecom_week = theweekgoalcom;
            weekbadgetoshowcom = weekbadgedrawablegold;
            content_community_title_week.setText(String.format(getString(R.string.string_ebene_com), "Gold"));


        } else if (Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < theweekgoalplatincom) {
            goalrechenvaluecom_week = theweekgoalplatincom;
            weekbadgetoshowcom = weekbadgedrawableplatin;
            content_community_title_week.setText(String.format(getString(R.string.string_ebene_com), "Platin"));

        } else if (Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()) < theweekgoalblackcom) {
            goalrechenvaluecom_week = theweekgoalblackcom;
            weekbadgetoshowcom = weekbadgedrawable;

            content_community_title_week.setText(String.format(getString(R.string.string_ebene_com), "Onyx"));
        } else {
            goalrechenvaluecom_week = theweekgoalblackcom;
            weekbadgetoshowcom = weekbadgedrawable;


        }

        Log.i("RESULT COMM -->", "g " + goalrechenvaluecom_week + " badge " + weekbadgetoshowcom);
        goal_txt_content_community_week.setText("" + String.format(Locale.getDefault(), "%,d", goalrechenvaluecom_week));
//String.format(Locale.getDefault(),"%,d", Long.parseLong(String.valueOf(restvalue)))
        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()))
        progress_txt_content_com_week.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString())));

        content_from_badge_week_com.setSelected(true);
        last_upload_val_txt_content_week.setSelected(true);
        togo_txt_content_week.setSelected(true);

        community_enl_week.setMax(goalrechenvaluecom_week);
        community_res_week.setMax(goalrechenvaluecom_week);
        String[] action_text2 = getResources().getStringArray(R.array.badge_text_actions_id);
        int action_value2 = Arrays.asList(action_text2).indexOf(weekbadgeid); // pass value

        String action_value_text_community2 = getResources().getStringArray(R.array.badge_text_actions_value_community)[action_value2];
//String.format(Locale.getDefault(),"%,d", goalrechenvaluecom_week);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mission_community_content_txt_week.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community_ebene), action_value_text_community2, "" + String.format(Locale.getDefault(), "%,d", goalrechenvaluecom_week), weekbadgeshort1), Html.FROM_HTML_MODE_COMPACT));

        } else {
            mission_community_content_txt_week.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community_ebene), action_value_text_community2, "" + String.format(Locale.getDefault(), "%,d", goalrechenvaluecom_week), weekbadgeshort1)));


        }

        int badgeweekid_content_com = getResources().getIdentifier(weekbadgetoshowcom, "drawable", getPackageName());

        //badgeweek = getResources().obtainTypedArray(badgeweekid);
        if (badgeweekid_content_com != 0) {
            //   Log.w("BADGE -->", " id :"+badgeweekid_content);

            ImageView week_community_content = findViewById(R.id.content_community_week);
            week_community_content.setImageDrawable(getDrawable(badgeweekid_content_com));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            community_enl_week.setProgress(Integer.parseInt(snapofdetails.child("community").child("enl").getValue().toString()), true);
            community_res_week.setProgress(Integer.parseInt(snapofdetails.child("community").child("res").getValue().toString()), true);
        } else {
            community_enl_week.setProgress(Integer.parseInt(snapofdetails.child("community").child("enl").getValue().toString()));
            community_res_week.setProgress(Integer.parseInt(snapofdetails.child("community").child("res").getValue().toString()));
        }
        int togo_com = goalrechenvaluecom_week - Integer.parseInt(snapofdetails.child("community").child("current").getValue().toString());
        //togo_txt_content_com.setText("noch "+togo_com);
        if (togo_com <= 0) {
            togo_com = Math.abs(togo_com);
            togo_txt_content_com_week.setText("Completed (+ " + togo_com + ")");
        } else {
            togo_txt_content_com_week.setText("noch " + togo_com);
        }
        rank_text_content_com_week.setText("Agents: " + snapofdetails.child("agents").getValue().toString());
        if (hasxf_week) {
            // TODO HAS XF = COMMUNITY MISSION
        }

        togo_txt_content_com_week.setSelected(true);

        // GET TEAM TODO
        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotteam) {

                if (snapshotteam.hasChild("xtra")) {
                    pref.edit().putBoolean("xtra", Boolean.parseBoolean(snapshotteam.child("xtra").getValue().toString())).apply();
                } else {
                    pref.edit().putBoolean("xtra", false).apply();
                }

                if (snapshotteam.child("reset").hasChild("week")) {
                    content_request_btn_week_manuell.setVisibility(View.VISIBLE);
                }
                if (snapshotteam.hasChild("team")) {
                    Log.i("TEAM-->", "Team gefunden");
                    //    team_week_exist.setVisibility(View.INVISIBLE);
                    team_swipe_week.setVisibility(View.VISIBLE);
                    swipe_team_week.setLockDrag(false);
                    // TODO INIT TEAM
                    hasteam_frame_month.setVisibility(View.VISIBLE);
                    // initweek(theyear,theweek, true);


                } else {
                    Log.i("TEAM -->", "Kein TEAM gefunden");
                    // initweek(theyear,theweek, false);
                    team_swipe_week.setVisibility(View.GONE);

                    swipe_team_week.setLockDrag(true);
                    swipe_layout_community_week.setLockDrag(false);
                    hasteam_frame_month.setVisibility(View.GONE);
                    //  team_week_not_exist.setVisibility(View.INVISIBLE);


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
        // GET AGENTSDATA FOR THE MISSION
        Query myTopPostsQuery = mDatabaserunnung.child(theyear).child("week").child(theid).orderByChild("currentvalue");
        final HashMap<String, String> data = new HashMap<String, String>();
        final List<String> listUID = new ArrayList();
        final List<Long> rankUID = new ArrayList();

        // RANKING FOR THE AGENT
        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot child : snapshot.getChildren()) {


                    rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));


                    String test = child.child("uid").getValue().toString();
                    listUID.add(test);

                    //  Log.i("TEST LOG--->",child.getValue().toString());

                }
                Collections.reverse(listUID);
                Collections.reverse(rankUID);


                //  mList.indexOf(user.getUid())
                int ownpositioninList = listUID.indexOf(user.getUid());
                int ownrankis = 0;
                if (ownpositioninList == -1) {

                    rank_txt_title_week.setText(" - ");

                    rank_text_content_week.setText(getString(R.string.rang) + ": ");
                } else {
                    for (int i = 0; i < rankUID.size(); i++) {
                        //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());

                        if (i == 0) {
                            ownrankis++;
                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                if (rankUID.get(i) > 0) {
                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    rank_txt_title_week.setText("" + ownrankis);
                                    rank_text_content_week.setText(getString(R.string.rang) + ": " + ownrankis);
                                } else {
                                    rank_txt_title_week.setText(" - ");
                                    rank_text_content_week.setText(getString(R.string.rang) + ": -");
                                }
                            }
                        } else {
                            if (rankUID.get(i) > rankUID.get(ownpositioninList)) {
                                ownrankis++;

                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title_week.setText("" + ownrankis);
                                        rank_text_content_week.setText(getString(R.string.rang) + ": " + ownrankis);
                                    } else {
                                        rank_txt_title_week.setText(" - ");

                                        rank_text_content_week.setText(getString(R.string.rang) + ": -");
                                    }
                                }
                            } else {
                                ownrankis++;
                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title_week.setText("" + ownrankis);
                                        rank_text_content_week.setText(getString(R.string.rang) + ": " + ownrankis);
                                    } else {
                                        rank_txt_title_week.setText(" - ");

                                        rank_text_content_week.setText(getString(R.string.rang) + ": -");
                                    }
                                }
                            }

                        }
                    }
                    //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                    if (ownrankis <= 0) {

                        rank_txt_title_week.setText(" - ");
                        rank_text_content_week.setText(getString(R.string.rang) + ": -");
                    }

                }

                //  mDatabaserunnung.child(theyear).child("week").child(theid)
                mDatabaserunnung.child(theyear).child("week").child(theid).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshotmissionagent) {
                        int goalrechenvalue;
                        String goaltextvalue;
                        String weekbadgetoshow;

                        if (Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalgold_week) {
                            goalrechenvalue = goalgold_week;
                            weekbadgetoshow = weekbadgedrawablegold;
                            content_avatar_title_week.setText(String.format(getString(R.string.string_ebene), "Gold"));
                            status_agent_week.setMax(goalgold_week);
                            status_agent_week.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));

                        } else if (Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalplatin_week) {
                            goalrechenvalue = goalplatin_week;
                            weekbadgetoshow = weekbadgedrawableplatin;
                            content_avatar_title_week.setText(String.format(getString(R.string.string_ebene), "Platin"));
                            status_agent_week.setMax(goalplatin_week);
                            status_agent_week.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        } else if (Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()) < goalblack_week) {
                            goalrechenvalue = goalblack_week;
                            weekbadgetoshow = weekbadgedrawable;
                            content_avatar_title_week.setText(String.format(getString(R.string.string_ebene), "Onyx"));
                            status_agent_week.setMax(goalblack_week);
                            status_agent_week.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        } else {
                            goalrechenvalue = goalblack_week;
                            weekbadgetoshow = weekbadgedrawable;
                            status_agent_week.setMax(goalblack_week);
                            status_agent_week.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        }
                        goaltextvalue = String.valueOf(goalrechenvalue);


                        Long restvalue = (goalrechenvalue) - Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString());
                        goal_txt_content_week.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(goalrechenvalue))));
                        goal_txt_title_week.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(goalrechenvalue))));

                        if (restvalue <= 0) {
                            restvalue = Math.abs(restvalue);
                            togo_txt_content_week.setText(getString(R.string.txt_completed) + " (+ " + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(restvalue))) + ")");
                        } else {
                            togo_txt_content_week.setText(getString(R.string.txt_another) + " " + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(restvalue))));
                        }
                        //  togo_txt_content.setText("noch "+restvalue); String.format("%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()));
                        progress_txt_title_week.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString())));
                        startwert_txt_content_week.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("startvalue").getValue().toString())));
                        short2_txt_content_week.setText(weekbadgeshort2);
                        short2_txt_content_week.setSelected(true);
                        title_txt_content_week.setText(weekbadgetitle);
                        title_txt_community_week.setText(weekbadgetitle);
                        progress_txt_content_week.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString())));

                        int badgeweekid_content = getResources().getIdentifier(weekbadgetoshow, "drawable", getPackageName());

                        Log.i("WEEEEEEK BADGE 333 ", weekbadgetoshow);
                        //badgeweek = getResources().obtainTypedArray(badgeweekid);
                        if (badgeweekid_content != 0) {
                            //   Log.w("BADGE -->", " id :"+badgeweekid_content);
                            ImageView week_badge_content = findViewById(R.id.content_avatar_week);
                            week_badge_content.setImageDrawable(getDrawable(badgeweekid_content));

                        }


                        String[] action_text = getResources().getStringArray(R.array.badge_text_actions_id);
                        int action_value = Arrays.asList(action_text).indexOf(weekbadgeid); // pass value
                        String action_value_text = getResources().getStringArray(R.array.badge_text_actions_value)[action_value];
                        //   String action_value_text_community = getResources().getStringArray(R.array.badge_text_actions_value_community)[action_value];


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            //    mission_community_content_txt_week.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community), action_value_text_community, snapofdetails.child("community").child("goal").getValue().toString(), weekbadgeshort1), Html.FROM_HTML_MODE_COMPACT));

                            mission_detals_content_txt_week.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), weekbadgeshort1), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            //   mission_community_content_txt_week.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_community), action_value_text_community, snapofdetails.child("community").child("goal").getValue().toString(), weekbadgeshort1)));

                            mission_detals_content_txt_week.setText(Html.fromHtml(String.format(getString(R.string.mission_text_content_ebene), action_value_text, "" + String.format(Locale.getDefault(), "%,d", goalrechenvalue), weekbadgeshort1)));

                        }
                        boolean hasoneupload;
                        hasoneupload = Integer.parseInt(snapshotmissionagent.child("lastvalue").getValue().toString()) > 0;
                        if (hasoneupload) {
                            //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapshotmissionagent.child("lastvalue").getValue().toString()))
                            last_upload_val_txt_content_week.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("lastvalue").getValue().toString())) + " " + weekbadgeshort1);
                        } else {
                            weekhistory_action.setVisibility(View.GONE);
                            last_upload_val_txt_content_week.setText(" - ");
                        }

                        // DATE

                        // getStartMonthofWeek(currentweek,Integer.parseInt(theyear))
                        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
                        SimpleDateFormat fyear = new SimpleDateFormat("yyyy");
                        final String themonth = getStartMonthofWeek(Integer.parseInt(theweek.substring(0, 2)), Integer.parseInt(theyear));
                        String themonthnext = getEndMonthofWeek(Integer.parseInt(theweek.substring(0, 2)), Integer.parseInt(theyear));

                        final String theyear = fyear.format(new Date());
                        DateFormat format = new SimpleDateFormat("dd");
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        Calendar.getInstance(Locale.getDefault()).setTimeZone(TimeZone.getDefault());
                        calendar.setFirstDayOfWeek(Calendar.MONDAY);
                        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                        final String[] dayss = new String[7];
                        for (int i = 0; i < 7; i++) {
                            dayss[i] = format.format(calendar.getTime());
                            calendar.add(Calendar.DAY_OF_MONTH, 1);
                        }
                        Calendar sDateCalendar = new GregorianCalendar(Integer.parseInt(theyear), Integer.parseInt(themonth), Integer.parseInt(dayss[0]));
                        sDateCalendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                        Calendar.getInstance(Locale.getDefault()).setTimeZone(TimeZone.getDefault());
                        sDateCalendar.setFirstDayOfWeek(Calendar.MONDAY);
                        int currentweek = Integer.parseInt(theweek.substring(0, 2));
                        String cweeksend;

                        cweeksend = String.valueOf(currentweek);


                        Log.i("CURRENT WEEK FOR", "" + cweeksend);
                        final String thefirstweekday = cweeksend + "" + dayss[0];
                        Log.i("MONTH -->", themonth);
                        Log.i("Year -->", theyear);
                        Log.i("Dayof Week -->", "" + dayss[6] + " " + dayss[0]);
                        String givenDateString2, givenDateString2end;
                        if (Integer.parseInt(dayss[6]) < Integer.parseInt(dayss[0])) {
                            Log.i("kleiner", "true");
                            givenDateString2 = dayss[6] + "." + themonthnext + "." + theyear + " 23:59:59";
                        } else {
                            Log.i("kleiner", "false" + themonthnext);
                            givenDateString2 = dayss[6] + "." + themonth + "." + theyear + " 23:59:59";
                        }


                        SimpleDateFormat sdf2 = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                        Date mDate2 = null;
                        try {
                            mDate2 = sdf2.parse(givenDateString2);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long timeInMilliseconds2 = mDate2.getTime();
                        Date currentDateis2 = new Date();


                        long diffInMs2 = timeInMilliseconds2 - currentDateis2.getTime();
                        long difftofinal = diffInMs2 / (86400 * 1000);
                        Log.i("WEEKS -->", "" + difftofinal + "given: " + givenDateString2 + " current:" + currentDateis2);
                        String textrunsday;
                        if (Math.round(difftofinal) < 1) {
                            if (difftofinal <= 0) {
                                textrunsday = getString(R.string.runtime_today) + "";
                            } else {
                                textrunsday = getString(R.string.runtime_oneday) + "";
                            }
                            String givenDateString;
                            if (Integer.parseInt(dayss[6]) < Integer.parseInt(dayss[0])) {
                                givenDateString = dayss[6] + "." + themonthnext + "." + theyear + " 23:59:59";
                            } else {
                                givenDateString = dayss[6] + "." + themonth + "." + theyear + " 23:59:59";
                            }

                            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                            try {
                                Date mDate = sdf.parse(givenDateString);
                                long timeInMilliseconds = mDate.getTime();
                                Date currentDateis = new Date();
                                System.out.println("Date in milli :: " + timeInMilliseconds);
                                long diffInMs = mDate.getTime() - currentDateis.getTime();

                                System.out.println("diffweek:: " + diffInMs + " " + mDate.toString());
                                timer_week = new WeekFinish(diffInMs, 1000);
                                timer_week.start();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }


                        } else {
                            textrunsday = Math.round(difftofinal) + " " + getString(R.string.string_days);

                            if (Integer.parseInt(dayss[6]) < Integer.parseInt(dayss[0])) {

                                runtime_date_content_week.setText(getString(R.string.string_until) + " " + dayss[6] + "." + themonthnext + ". 23:59 Uhr");
                            } else {

                                runtime_date_content_week.setText(getString(R.string.string_until) + " " + dayss[6] + "." + themonth + ". 23:59 Uhr");
                            }


                        }
                        deadlinetime_content_week.setText(textrunsday);


                        if (hasoneupload) {
                            ViewGroup.LayoutParams params = fcweek.getLayoutParams();
                            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            fcweek.setLayoutParams(params);
                            fcweek.requestLayout();
                            weekhistory_action.setVisibility(View.VISIBLE);
                            weekhistory_action.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    openHistory(theyear, theweek, theid, "week", AODMain.this, null, -1);
                                    // infoToast("soon :-)");
                                }
                            });
                            Date oldDate = new Date((Long.parseLong(snapshotmissionagent.child("timestamp").getValue().toString()) * 1000));
                            Date currentDate = new Date();

                            long diff = currentDate.getTime() - oldDate.getTime();
                            long seconds = diff / 1000;
                            long minutes = seconds / 60;
                            long hours = minutes / 60;
                            long days = hours / 24;

                            if (oldDate.before(currentDate)) {

                                Log.e("oldDate", "is previous date");
                                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                                        + " hours: " + hours + " days: " + days);
                                if (days >= 1) {
                                    if (days == 1) {

                                        lastupload_time_content_week.setText(String.format(getString(R.string.string_ago_day), "" + days));
                                    } else {
                                        lastupload_time_content_week.setText(String.format(getString(R.string.string_ago_days), "" + days));
                                    }
                                } else if (hours >= 1) {
                                    if (hours == 1) {
                                        lastupload_time_content_week.setText(String.format(getString(R.string.string_ago_hour), "" + hours));
                                    } else if (hours > 1 && hours < 23) {
                                        lastupload_time_content_week.setText(String.format(getString(R.string.string_ago_hours), "" + hours));
                                    } else {
                                        lastupload_time_content_week.setText(getString(R.string.string_oneday));
                                    }
                                } else if (minutes >= 0) {
                                    if (minutes == 0) {
                                        lastupload_time_content_week.setText(getString(R.string.string_justnow));
                                    } else if (minutes >= 1 && minutes <= 5) {
                                        lastupload_time_content_week.setText(getString(R.string.string_justnow));
                                    } else {
                                        lastupload_time_content_week.setText(String.format(getString(R.string.string_ago_minutes), "" + minutes));
                                    }
                                }
                            } else {
                                lastupload_time_content_week.setText(getString(R.string.string_nouplaod));
                            }
                        } else {
                            rank_text_content_week.setText(getString(R.string.rang) + ": -");
                            lastupload_time_content_week.setText(getString(R.string.string_nouplaod));
                        }

                        lastupload_time_content_week.setSelected(true);
                        // Log.e("toyBornTime", "" + toyBornTime);

                        //  weekbadgetitle, weekbadgeshort1, weekbadgeshort2, weekbadgefield;

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

        weeknotstartet.setVisibility(View.GONE);
        weekstartet.setVisibility(View.VISIBLE);

        fcweek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fcweek.toggle(false);
                //weekview_base.requestDisallowInterceptTouchEvent(false);
                if (!fcweek.isUnfolded()) {
                    final long now = SystemClock.uptimeMillis();
                    final MotionEvent pressEvent = MotionEvent.obtain(now, now, MotionEvent.ACTION_DOWN, fcweek.getWidth() / 2, fcweek.getHeight() / 2, 0);


                    swipe_layout_community.onTouchEvent(pressEvent);
                }


            }
        });
        // TODO GET DETAILS week
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                frameloadweek.setVisibility(View.GONE);
                framecardweek.setVisibility(View.VISIBLE);


            }
        }, 1200);
    }


    private void minigetagentDetails(final String theid, final DataSnapshot snapofdetails, final String theyear, final String themonth) {
        Log.i("THE ID OF EVENT -->", theid);
        Log.i("THE MAX OF MINI>", snapofdetails.child("max").getValue().toString());
        // INIT DETAILS
        boolean hasranking = Boolean.parseBoolean(snapofdetails.child("ranking").getValue().toString());
        boolean hasprice = Boolean.parseBoolean(snapofdetails.child("price").getValue().toString());
        boolean hasteam = Boolean.parseBoolean(snapofdetails.child("team").getValue().toString());
        boolean hasduell = Boolean.parseBoolean(snapofdetails.child("duell").getValue().toString());

        statsart = snapofdetails.child("art").getValue().toString();

        stattab = snapofdetails.child("stats").getValue().toString();
        boolean hasxf = Boolean.parseBoolean(snapofdetails.child("xf").getValue().toString());
        int activeagents = Integer.parseInt(snapofdetails.child("agents").getValue().toString());


        final int maxuploads = Integer.parseInt(snapofdetails.child("max").getValue().toString());

        // INIT TEXT VIEWS
        final TextView goal_txt_title = findViewById(R.id.title_pledge_mini);
        final TextView content_avatar_title_month = findViewById(R.id.content_avatar_title_mini);
        final TextView content_community_title_month = findViewById(R.id.content_community_title_mini);
        final TextView rank_txt_title = findViewById(R.id.title_weight_mini);
        final TextView rank_txt_title_action = findViewById(R.id.rang_title_action_mini);
        rank_txt_title.setOnClickListener(this);

        // progress = remaining
        final TextView progress_txt_title = findViewById(R.id.title_requests_count_mini);
        final TextView startwert_txt_content = findViewById(R.id.content_from_address_1_mini);
        final TextView short2_txt_content = findViewById(R.id.content_from_address_2_mini);
        final TextView rank_text_content = findViewById(R.id.content_to_address_2_mini);
        final TextView rank_text_content_com = findViewById(R.id.content_to_address_2_mini_com);
        final TextView goal_txt_content = findViewById(R.id.content_to_go_1_mini);
        final TextView togo_txt_content = findViewById(R.id.content_to_go_2_mini);
        final TextView togo_txt_content_com = findViewById(R.id.content_to_go_2_mini_com);
        final TextView title_txt_content = findViewById(R.id.content_name_view_mini);
        final TextView title_txt_community = findViewById(R.id.content_community_view_mini);

        final TextView mission_community_content_txt_mini = findViewById(R.id.mission_community_content_txt_mini);
        final TextView progress_txt_content = findViewById(R.id.content_to_address_1_mini);
        final TextView goal_txt_content_community = findViewById(R.id.content_to_go_1_mini_com);
        final TextView progress_txt_content_com = findViewById(R.id.content_to_address_1_mini_com);
        final TextView last_upload_val_txt_content = findViewById(R.id.content_delivery_time_mini);
        final TextView deadlinetime_content = findViewById(R.id.content_deadline_time_mini);
        final TextView runtime_date_content = findViewById(R.id.content_deadline_date_mini);
        final TextView lastupload_time_content = findViewById(R.id.content_delivery_date_mini);
        final TextView content_from_badge_mini_com = findViewById(R.id.content_from_badge_mini_com);
        final ProgressBar community_enl = findViewById(R.id.statusenl);
        final ProgressBar community_res = findViewById(R.id.statusres);
        final ProgressBar status_agent = findViewById(R.id.statusagent);
        final ImageView team_swipe_mini = findViewById(R.id.teamswipe_mini);
        final TextView content_to_go_mini = findViewById(R.id.content_to_go_mini);

        final RelativeLayout minihistory_action = findViewById(R.id.minihistory_action);
        goal_txt_title.setText("-");


        // SET DETAILS COMMUNITY

        int goalrechenvaluecom;
        String goaltextvaluecom;
        String minibadgetoshowcom;


        goal_txt_content_community.setText("-");
        //String.format(Locale.getDefault(),"%,d", goalrechenvaluecom))
        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()))
        progress_txt_content_com.setText("COMMUNITY TXT");

        content_from_badge_mini_com.setSelected(true);
        last_upload_val_txt_content.setSelected(true);
        togo_txt_content.setSelected(true);

        community_enl.setMax(0);
        community_res.setMax(0);
        String[] action_text2 = getResources().getStringArray(R.array.badge_text_actions_id);
        int action_value2 = Arrays.asList(action_text2).indexOf(minibadgeid); // pass value


        mission_community_content_txt_mini.setText("Text1");


        //togo_txt_content_com.setText("noch "+togo_com);
        //String.format(Locale.getDefault(),"%,d", togo_com)
        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapofdetails.child("community").child("current").getValue().toString()))

        //     togo_txt_content_com.setText("TOGO TEXT");

        rank_text_content_com.setText("Agents: " + snapofdetails.child("agents").getValue().toString());
        if (hasxf) {
            // TODO HAS XF = COMMUNITY MISSION
        }

        togo_txt_content_com.setSelected(true);

        // GET TEAM TODO
        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotteam) {
                //   Log.i("SNAPP IS ", snapshotteam.child("email").getValue().toString() + "");
                if (snapshotteam.hasChild("xtra")) {
                    Log.i("HAS XTRA", "yepp");

                    pref.edit().putBoolean("xtra", Boolean.parseBoolean(snapshotteam.child("xtra").getValue().toString())).apply();
                } else {
                    Log.i("HAS NO XTRA", "yepp");
                    pref.edit().putBoolean("xtra", false).apply();
                }

                if (snapshotteam.hasChild("team")) {
                    Log.i("TEAM-->", "Team gefunden");
                    //    team_mini_exist.setVisibility(View.INVISIBLE);
                    team_swipe_mini.setVisibility(View.VISIBLE);
                    swipe_team_mini.setLockDrag(false);
                    pref.edit().putBoolean("hasteam", true).apply();
                    // TODO INIT TEAM
                    // initmini(theyear,themini, true);


                } else {
                    Log.i("TEAM -->", "Kein TEAM gefunden");
                    // initmini(theyear,themini, false);
                    team_swipe_mini.setVisibility(View.GONE);
                    swipe_team_mini.setLockDrag(true);
                    swipe_layout_community.setLockDrag(false);

                    pref.edit().putBoolean("hasteam", false).apply();

                    //  team_mini_not_exist.setVisibility(View.INVISIBLE);


                }

                // SUCHE FREUNDE
                if (snapshotteam.hasChild("friends")) {
                    Log.i("Friends-->", "Freunde gefunden");
                    pref.edit().putBoolean("hasfriends", true).apply();


                } else {
                    Log.i("Friends -->", "Keine Freunde gefunden");

                    pref.edit().putBoolean("hasfriends", false).apply();


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
        // GET AGENTSDATA FOR THE MISSION
        Query myTopPostsQuery = mDatabaserunnung.child(theyear).child("mini").child(theid).orderByChild("currentvalue");
        final HashMap<String, String> data = new HashMap<String, String>();
        final List<String> listUID = new ArrayList();
        final List<Long> rankUID = new ArrayList();

        final List<Long> timestamprank = new ArrayList();

        // RANKING FOR THE AGENT
        myTopPostsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot child : snapshot.getChildren()) {


                    rankUID.add(Long.parseLong(child.child("currentvalue").getValue().toString()));
                    timestamprank.add(Long.parseLong(child.child("timestamp").getValue().toString()));


                    String test = child.child("uid").getValue().toString();
                    listUID.add(test);

                    //  Log.i("TEST LOG--->",child.getValue().toString());

                }
                Collections.reverse(listUID);
                Collections.reverse(rankUID);

                Collections.reverse(timestamprank);

                togo_txt_content.setText("Leader Agentname");
                //  mList.indexOf(user.getUid())
                int ownpositioninList = listUID.indexOf(user.getUid());
                int ownrankis = 0;
                if (ownpositioninList == -1) {

                    rank_txt_title.setText("-");
                    rank_text_content.setText(getString(R.string.rang) + ": ");
                } else {


                    for (int i = 0; i < rankUID.size(); i++) {
                        //   Log.i("UID -->",  "Value:" + listUID.get(i)+ "List Size "+rankUID.size());


                        if (i == 0) {
                            if (rankUID.get(0) > 0) {

                                mDatabase.child(listUID.get(0)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {

                                    @Override
                                    public void onDataChange(DataSnapshot snapshotagent) {
                                        // snapshotagentimage.getValue().toString() + "?sz=200"

                                        if (snapshotagent.getValue().toString().equalsIgnoreCase(AgentData.getagentname())) {
                                            content_to_go_mini.setText("You leads");
                                            if (rankUID.size() == 1) {
                                                togo_txt_content.setText("No other agents");
                                            } else {
                                                togo_txt_content.setText(snapshotagent.getValue().toString());
                                            }
                                        } else {
                                            togo_txt_content.setText(snapshotagent.getValue().toString());
                                        }
                                        togo_txt_content.setSelected(true);


                                    }


                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });
                            }
                            ownrankis++;
                            if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                if (rankUID.get(i) > 0) {
                                    //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    rank_txt_title.setText("" + ownrankis);
                                    rank_text_content.setText(getString(R.string.rang) + ": " + ownrankis);
                                } else {
                                    rank_txt_title.setText("-");
                                    rank_text_content.setText(getString(R.string.rang) + ": -");
                                }
                                if (listUID.size() > 1) {
                                    content_to_go_mini.setText("You leads with");
                                    Long leadswith = rankUID.get(0) - rankUID.get(1);
                                    status_agent.setMax(Integer.parseInt(rankUID.get(i).toString()));
                                    goal_txt_content.setText(String.format(Locale.getDefault(), "%,d", leadswith));
                                } else {

                                    goal_txt_content.setText("NO DATA");
                                }
                            }
                        } else {


                            if (rankUID.get(i) > rankUID.get(ownpositioninList)) {
                                ownrankis++;

                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {

                                    if (timestamprank.get(i) >= timestamprank.get(0)) {
                                        Long needsgoal = rankUID.get(0) - rankUID.get(i);
                                        goal_txt_content.setText(String.format(Locale.getDefault(), "%,d", needsgoal));
                                        status_agent.setMax(Integer.parseInt(rankUID.get(0).toString()));
                                    } else {
                                        status_agent.setMax(Integer.parseInt(rankUID.get(i).toString()) * 100);
                                        content_to_go_mini.setText("to beat");
                                        goal_txt_content.setText("hidden");
                                    }
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title.setText("" + ownrankis);
                                        rank_text_content.setText(getString(R.string.rang) + ": " + ownrankis);
                                    } else {
                                        rank_txt_title.setText("-");
                                        rank_text_content.setText(getString(R.string.rang) + ": -");
                                    }
                                }
                            } else {
                                ownrankis++;
                                if (listUID.get(i).equalsIgnoreCase(user.getUid())) {
                                    if (timestamprank.get(i) >= timestamprank.get(0)) {
                                        Long needsgoal = rankUID.get(0) - rankUID.get(i);
                                        goal_txt_content.setText(String.format(Locale.getDefault(), "%,d", needsgoal));
                                        status_agent.setMax(Integer.parseInt(rankUID.get(0).toString()));
                                    } else {
                                        content_to_go_mini.setText("to beat");
                                        goal_txt_content.setText("hidden");
                                        status_agent.setMax(Integer.parseInt(rankUID.get(i).toString()) * 100);
                                    }
                                    //  Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                    if (rankUID.get(i) > 0) {
                                        //   Log.i("RANKING -->", "POS: " + i + " Value:" + rankUID.get(ownpositioninList));
                                        rank_txt_title.setText("" + ownrankis);
                                        rank_text_content.setText(getString(R.string.rang) + ": " + ownrankis);
                                    } else {
                                        rank_txt_title.setText("-");
                                        rank_text_content.setText(getString(R.string.rang) + ": -");
                                    }
                                }
                            }

                        }
                    }
                    //  Log.i("TEST RANKING--->", "" + (rankUID.get(ownpositioninList)));
                    if (ownrankis <= 0) {

                        rank_txt_title.setText("-");
                        rank_text_content.setText(getString(R.string.rang) + ": -");
                    }

                }

                //  mDatabaserunnung.child(theyear).child("mini").child(theid)
                mDatabaserunnung.child(theyear).child("mini").child(theid).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshotmissionagent) {

                        String goaltextvalue;
                        String minibadgetoshow;


                        status_agent.setProgress(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));


                        goaltextvalue = String.valueOf(Integer.parseInt(snapshotmissionagent.child("currentvalue").getValue().toString()));


                        minibadgetoshow = minibadgedrawablegold;

                        goal_txt_title.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(goaltextvalue)));


                        //  togo_txt_content.setText("noch "+restvalue);
                        int remaining = maxuploads - Integer.parseInt(snapshotmissionagent.child("uploads").getValue().toString());
                        if (remaining > 0) {
                            content_request_btn_mini.setEnabled(true);
                            if (remaining == 1) {
                                progress_txt_title.setText(remaining + " Update");
                                short2_txt_content.setText("Update");
                                short2_txt_content.setSelected(true);
                            } else {
                                progress_txt_title.setText(remaining + " Updates");
                                short2_txt_content.setText("Updates");
                                short2_txt_content.setSelected(true);
                            }
                        } else {
                            content_request_btn_mini.setEnabled(false);
                            content_request_btn_mini.setText("Limit reached");
                            progress_txt_title.setText("0 (closed)");
                            short2_txt_content.setText("no more Update");
                            short2_txt_content.setSelected(true);
                        }
                        // startwert_txt_content = currentvalue
                        startwert_txt_content.setText("" + remaining);

                        title_txt_content.setText(minibadgetitle);
                        title_txt_community.setText(minibadgetitle);
                        //String.format(Locale.getDefault(),"%,d", goaltextvalue)
                        //String.format(Locale.getDefault(),"%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString()))
                        progress_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("currentvalue").getValue().toString())));

                        int badgeminiid_content = getResources().getIdentifier(minibadgetoshow, "drawable", getPackageName());

                        //badgemini = getResources().obtainTypedArray(badgeminiid);
                        if (badgeminiid_content != 0) {
                            //   Log.w("BADGE -->", " id :"+badgeminiid_content);
                            ImageView mini_badge_content = findViewById(R.id.content_avatar_mini);
                            mini_badge_content.setImageDrawable(getDrawable(badgeminiid_content));

                        }


                        String[] action_text = getResources().getStringArray(R.array.badge_text_actions_id);

                        //   String action_value_text_community = getResources().getStringArray(R.array.badge_text_actions_value_community)[action_value];


                        boolean hasoneupload;
                        hasoneupload = Integer.parseInt(snapshotmissionagent.child("lastvalue").getValue().toString()) > 0;
                        if (hasoneupload) {
                            last_upload_val_txt_content.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotmissionagent.child("lastvalue").getValue().toString())) + " " + minibadgeshort1);
                            minihistory_action.setVisibility(View.GONE);
                        } else {
                            minihistory_action.setVisibility(View.GONE);
                            last_upload_val_txt_content.setText(" - ");
                        }


                        Date oldDate2 = new Date((Long.parseLong(snapofdetails.child("id").getValue().toString())));
                        Date currentDate2 = new Date();


                        long diff2 = oldDate2.getTime() - currentDate2.getTime();
                        long seconds2 = diff2 / 1000;
                        long minutes2 = seconds2 / 60;
                        long hours2 = minutes2 / 60;
                        long days2 = hours2 / 24;


                        long difftofinal = diff2;
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                        String textrunsday;
                        if (days2 <= 1) {
                            if (difftofinal <= 0) {
                                textrunsday = getString(R.string.runtime_today) + "";
                            } else {
                                textrunsday = getString(R.string.runtime_oneday) + "";
                            }


                            long timeInMilliseconds = oldDate2.getTime();
                            Date currentDateis = new Date();
                            System.out.println("Date in milli :: " + timeInMilliseconds);
                            long diffInMs = oldDate2.getTime() - currentDateis.getTime();

                            System.out.println("diff:: " + diffInMs + " " + oldDate2.toString());
                            timer_month = new MonthFinish(diffInMs, 1000);
                            timer_month.start();


                        } else {
                            textrunsday = days2 + " " + getString(R.string.string_days);
                            runtime_date_content.setText(sdf.format(oldDate2));

                        }
                        deadlinetime_content.setText(textrunsday);
                        //  deadlinetime_content.setText("tbd Runtime");


                        if (hasoneupload) {
                            ViewGroup.LayoutParams params = fcmini.getLayoutParams();
                            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            fcmini.setLayoutParams(params);
                            fcmini.requestLayout();
                            minihistory_action.setVisibility(View.GONE);

                            Date oldDate = new Date((Long.parseLong(snapshotmissionagent.child("timestamp").getValue().toString()) * 1000));
                            Date currentDate = new Date();

                            long diff = currentDate.getTime() - oldDate.getTime();
                            long seconds = diff / 1000;
                            long minutes = seconds / 60;
                            long hours = minutes / 60;
                            long days = hours / 24;

                            if (oldDate.before(currentDate)) {

                                Log.e("oldDate", "is previous date");
                                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                                        + " hours: " + hours + " days: " + days);
                                if (days >= 1) {
                                    if (days == 1) {

                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_day), "" + days));
                                    } else {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_days), "" + days));
                                    }
                                } else if (hours >= 1) {
                                    if (hours == 1) {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_hour), "" + hours));
                                    } else if (hours > 1 && hours < 23) {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_hours), "" + hours));
                                    } else {
                                        lastupload_time_content.setText(getString(R.string.string_oneday));
                                    }
                                } else if (minutes >= 0) {
                                    if (minutes == 0) {
                                        lastupload_time_content.setText(getString(R.string.string_justnow));
                                    } else if (minutes >= 1 && minutes <= 5) {
                                        lastupload_time_content.setText(getString(R.string.string_justnow));
                                    } else {
                                        lastupload_time_content.setText(String.format(getString(R.string.string_ago_minutes), "" + minutes));
                                    }
                                }
                            } else {
                                lastupload_time_content.setText(getString(R.string.string_nouplaod));
                            }
                        } else {
                            rank_text_content.setText(getString(R.string.rang) + ": -");
                            lastupload_time_content.setText(getString(R.string.string_nouplaod));
                        }

                        lastupload_time_content.setSelected(true);
                        // Log.e("toyBornTime", "" + toyBornTime);

                        //  monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield;

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                });


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

        mininotstartet.setVisibility(View.GONE);
        ministartet.setVisibility(View.VISIBLE);

        fcmini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fcmini.toggle(false);

                if (!fcmini.isUnfolded()) {
                    final long now = SystemClock.uptimeMillis();
                    final MotionEvent pressEvent = MotionEvent.obtain(now, now, MotionEvent.ACTION_DOWN, fcmini.getWidth() / 2, fcmini.getHeight() / 2, 0);


                    swipe_layout_community.onTouchEvent(pressEvent);
                }


            }
        });
        // TODO GET DETAILS mini
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                frameloadmini.setVisibility(View.GONE);
                framecardmini.setVisibility(View.VISIBLE);

            }
        }, 1200);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // Take any action after completing the animation

        // check for zoom in animation
        if (animation == animZoomIn) {
            //  bgani.startAnimation(animZoomNull);
        }
        if (animation == animZoomNull) {
            //  bgani.startAnimation(animZoomIn);
        }

    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onResume() {
        super.onResume();


        XmasSnow.on(this)
                .belowActionBar(true)
                .belowStatusBar(true) // Always true if belowActionBar() is set to true
                .onlyOnChristmas(false) // Only the 25th of december
                .setInterval("11/28/2019", "1/25/2020") // 25th of december to 7th of january (not included). Date format: MM/dd/yyyy
                .start();

        mainprefssettings = PreferenceManager.getDefaultSharedPreferences(AODMain.this);
        new AODConfig();

        new AODConfig("reff", getApplicationContext());
        if (user == null) {
            user = FirebaseAuth.getInstance().getCurrentUser();
        }

        if (isAppInstalled("com.nianticproject.ingress")) {
            pref.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
            primeisinstalled = true;
            try {
                packageInfo = getPackageManager().getPackageInfo("com.nianticproject.ingress", 0);
            } catch (PackageManager.NameNotFoundException e) {
                packageInfo = null;
                e.printStackTrace();
            }

            if (packageInfo != null) {
                min_ingressversion = packageInfo.versionCode >= Integer.parseInt(remoteconfig.getString("min_ingress"));
            }
        } else {
            pref.edit().putBoolean(remoteconfig.getString("pref_calibration"), false).apply();
            primeisinstalled = false;
            min_ingressversion = false;
        }


        if (remoteconfig.getBoolean("redacted")) {
            redacted_allowed = remoteconfig.getBoolean("redacted");
        } else {
            redacted_allowed = false;
        }

        multimission = mainprefssettings.getBoolean("update_missions", false);

        if (mBound) {
            Log.w("Service -_>", "Closed");


            try {
                unbindService(connectionService);
            } catch (Exception i) {
                Log.i("CRASH", "dontknowwhy");
            }
            mService.setCallbacks(null); // unregister
            mBound = false;
            if (isMyServiceRunning(OverlayScreenService.class)) {
                Log.i("SERVICESTATUS", "running");
                Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                stopService(svc);
            }
        }
        bmb_main.setAutoHide(false);
        if (bmb_main.isBoomed()) {
            bmb_main.reboom();
        }

        //findViewById(R.id.hackventicon).setVisibility(View.VISIBLE);
        //findViewById(R.id.hackventicon).setOnClickListener(this);

      /*  if (AgentData.gethackvent()) {
            findViewById(R.id.hackventicon).setVisibility(View.VISIBLE);
            findViewById(R.id.hackventicon).setOnClickListener(this);
        } */

        agentname.setText(AgentData.getagentname());
        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorENL, getTheme()));
        } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
            agentimg.setBorderColor(getResources().getColor(R.color.colorRES, getTheme()));
        } else {
            agentimg.setBorderColor(getResources().getColor(R.color.colorGray, getTheme()));
        }
        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getProviderData().get(1).getPhotoUrl().toString())
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)


                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    //  .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(agentimg);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

        agentimg.setOnClickListener(this);
        new AODConfig("pommes", getApplicationContext());

        // TODO RATING
        //  onAppLaunched(getCurrentRate());
        //  onActionPerformed(getCurrentRate());
        Log.i("ONRESUM -->", "AODMain.class");


        // REDACTED ERLAUBT
        if (redacted_allowed && AgentData.getTrickster().equalsIgnoreCase("green")) {
            initmissions();

        } else if (AgentData.getTrickster().equalsIgnoreCase("red")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

            // builder.setMessage("Text not finished...");
            LayoutInflater inflater = getLayoutInflater();
            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
            builder.setView(dialog);
            final TextView input = dialog.findViewById(R.id.caltext);
            // input.setAllCaps(true);
            input.setTypeface(type);
            input.setText("There are anomalies in your account. Please contact support.");
            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
            //localTextView1.setTypeface(proto);
            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                    //i.putExtra("PersonID", personID);
                    startActivity(i);
                    //finish();

                }
            });


            builder.setCancelable(false);

            if (!isFinishing()) {
                builder.create().show();

            }
        }
        // AKTUELLSTE PRIME VERSION INSTALLIERT
        else if (min_ingressversion && primeisinstalled) {
            final SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(AODMain.this);

            if (prefs2.getString("ingress_appart", "-1").equalsIgnoreCase("0") && !redacted_allowed) {

                AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                // builder.setMessage("Text not finished...");
                LayoutInflater inflater = getLayoutInflater();
                View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                builder.setView(dialog);
                final TextView input = dialog.findViewById(R.id.caltext);
                // input.setAllCaps(true);
                input.setTypeface(type);
                input.setText("redacted Scanner is not longer supported, please update your calibration settings.");
                //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                //localTextView1.setTypeface(proto);
                builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        prefs2.edit().putString("ingress_appart", "1").apply();

                        Intent i = new Intent(getBaseContext(), MainSettings.class);
                        //i.putExtra("PersonID", personID);
                        startActivity(i);
                        //finish();

                    }
                });


                builder.setCancelable(false);

                if (!isFinishing()) {
                    builder.create().show();

                }
            } else {
                initmissions();
            }

        } else {
            if (!min_ingressversion && primeisinstalled) {
                // TODO INGRESS UPDATE

                initmissions();
            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                // builder.setMessage("Text not finished...");
                LayoutInflater inflater = getLayoutInflater();
                View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                builder.setView(dialog);
                final TextView input = dialog.findViewById(R.id.caltext);
                // input.setAllCaps(true);
                input.setTypeface(type);

                input.setText("Ingress Prime is not installed on this device.");
                //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                //localTextView1.setTypeface(proto);
                builder.setPositiveButton("download now", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Log.i("Hallo", "hallo");
                        //handleinputcode(input.getText().toString());
                        // Todo publish the code to database
                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.nianticproject.ingress")));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.nianticproject.ingress")));
                        }
                        finish();

                    }
                });

                builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                        // Todo publish the code to database
                    }
                });

                builder.setCancelable(false);

                if (!isFinishing()) {
                    builder.create().show();

                }

                // TODO PRIME NOT INSTALLED

            }
        }

        new GetNotifications().getFriendRequests(this, getApplicationContext(), notify_count_image, notify_count_text);

        //initnotifications();


    }

    @Override
    public void onStart() {
        super.onStart();
        PackageManager pm = getPackageManager();
        if (AODConfig.isPackageInstalled("rocks.prxenon.aodeblplugin", pm)) {
            new AgentData("ebl", true);


        } else {
            new AgentData("ebl", false);
        }
        Log.i("Start", "901");


    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");
        if (timer_month != null) {
            timer_month.cancel();
        }
        if (timer_week != null) {
            timer_week.cancel();
        }
        if (timer_pvp_one != null) {
            timer_pvp_one.cancel();
        }
        if (timer_pvp_two != null) {
            timer_pvp_two.cancel();
        }
        if (timer_pvp_three != null) {
            timer_pvp_three.cancel();
        }
        if (historybuilder != null) {
            hasdialogbefore = true;
            historybuilder.create().dismiss();
            historybuilder = null;
        }

        if (newagents != null) {
            mbfriendsrequest.removeEventListener(newagents);

        }
        if (newinbox != null) {
            mDatabase.removeEventListener(newinbox);


        }

        if (listenerpvpjoin != null) {
            mbpvp.removeEventListener(listenerpvpjoin);
            listenerpvpjoin = null;
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");
        if (timer_week != null) {
            timer_week.cancel();
        }
        if (timer_month != null) {
            timer_month.cancel();
        }
        if (timer_pvp_one != null) {
            timer_pvp_one.cancel();
        }
        if (timer_pvp_two != null) {
            timer_pvp_two.cancel();
        }
        if (timer_pvp_three != null) {
            timer_pvp_three.cancel();
        }
        if (timerhackvent != null) {
            timerhackvent.cancel();
        }


        if (newagents != null) {
            mbfriendsrequest.removeEventListener(newagents);

        }

        if (newinbox != null) {
            mDatabase.removeEventListener(newinbox);


        }

        if (listenerpvpjoin != null) {
            mbpvp.removeEventListener(listenerpvpjoin);
            listenerpvpjoin = null;
        }
    }

    @Override
    public void onDestroy() {
        if (historybuilder != null) {
            hasdialogbefore = true;
            historybuilder.create().dismiss();
            historybuilder = null;
        }
        if (timer_week != null) {
            timer_week.cancel();
        }
        if (timer_month != null) {
            timer_month.cancel();
        }
        if (timer_pvp_one != null) {
            timer_pvp_one.cancel();
        }
        if (timer_pvp_two != null) {
            timer_pvp_two.cancel();
        }
        if (timer_pvp_three != null) {
            timer_pvp_three.cancel();
        }
        if (mBound) {
            mService.setCallbacks(null); // unregister
            unbindService(connectionService);
            mBound = false;
        }

        if (timerhackvent != null) {
            timerhackvent.cancel();
        }
        if (isMyServiceRunning(OverlayScreenService.class)) {
            Log.i("SERVICESTATUS", "running");
            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
            stopService(svc);
        }

        if (newagents != null) {
            mbfriendsrequest.removeEventListener(newagents);

        }

        if (newinbox != null) {
            mDatabase.removeEventListener(newinbox);


        }

        if (listenerpvpjoin != null) {
            mbpvp.removeEventListener(listenerpvpjoin);
            listenerpvpjoin = null;
        }
        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");


    }

    @Override
    public void onBackPressed() {

        if (progressBar.getVisibility() == View.VISIBLE) {
            Toast.makeText(getApplicationContext(), "Please wait... progress is running", Toast.LENGTH_SHORT).show();

        } else {
            pref.edit().remove("scanedagnet").apply();


            if (bmb_main.isBoomed()) {

                if (bmb_main.isBoomed()) {
                    bmb_main.reboom();
                }
            } else {

                super.onBackPressed();
            }
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Log.i("RESPONSE -->", "" + requestCode + resultCode);

        if (requestCode == REQUEST_OVERLAY) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    // startscreenshot();
                    infoToast("Nochmal");

                }
            }
        }

        // workaroundpvptit
        if (requestCode == 7879 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();


            Log.d("file", "FILE CHANGE OCCURED " + picturePath);


            data.putExtra("screen", picturePath);
            connectToDevicemanuel(data);
            //ImageView imageView = (ImageView) findViewById(R.id.imgView);
            //imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }

        if (requestCode == 7881 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();


            Log.d("file", "FILE CHANGE OCCURED " + picturePath);


            data.putExtra("screen", picturePath);
            connectToDevicemanuel(data);
            //ImageView imageView = (ImageView) findViewById(R.id.imgView);
            //imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }

        if (requestCode == 7880 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();


            Log.d("file", "FILE CHANGE OCCURED " + picturePath);


            data.putExtra("screen", picturePath);
            connectToDevicemanuel(data);
            //ImageView imageView = (ImageView) findViewById(R.id.imgView);
            //imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }

        if (requestCode == 392) {
            System.out.println("Share Done");

        }


    }

    private void startscreenshot(final String month, final String cmonthmissionid) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            String pasteData = "";


            // If it does contain data, decide if you can handle the data.
            if (!clipboard.hasPrimaryClip()) {

                Log.i("CLIP --->", "has no prim clip");
                ClipData clipData = ClipData.newPlainText("clear by aod", "Pommes");
                clipboard.setPrimaryClip(clipData);
                //clipboard.clearPrimaryClip();

            } else if (!clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN)) {

                Log.i("CLIP --->", "has prim clip but not Text");

                ClipData clipData = ClipData.newPlainText("clear by aod", "Pommes");
                clipboard.setPrimaryClip(clipData);
                //clipboard.clearPrimaryClip();
                // since the clipboard has data but it is not plain text

            } else {

                Log.i("CLIP --->", "try clean prime clip");
                ClipData clipData = ClipData.newPlainText("clear by aod", "Pommes");
                clipboard.setPrimaryClip(clipData);
                //  clipboard.clearPrimaryClip();

                // Gets the clipboard as text.
                // pasteData = item.getText().toString();
            }

            // String statsart = snapofdetails.child("art").getValue().toString();

            //  boolean stattab = Boolean.parseBoolean(snapofdetails.child("stats").getValue().toString());
        }


        mDatabasemissions.child(month).child(cmonthmissionid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshotupdate) {


                if (dataSnapshotupdate.hasChild("art")) {
                    if (dataSnapshotupdate.child("art").getValue().toString().equalsIgnoreCase("stats")) {
                        startscreenshotQ(month, cmonthmissionid, dataSnapshotupdate.child("stats").getValue().toString());
                    } else {
                        // TODO screen
                    }
                } else {
                    startscreenshotQ(month, cmonthmissionid, "all");
                }


            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e("TAG", "Failed to update.", error.toException());
                errorToast("mini", "9004");
            }
        });

        //  startscreenshotQ(month, cmonthmissionid);


    }

    private void startscreenshotQ(String month, String cmonthmissionid, String stats) {

        SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(AODMain.this);
        boolean needprime;
        boolean isprime;
        if (prefs2.getBoolean("workaround", false)) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            workaround_actionid = month;
            workaround_month = cmonthmissionid;


            startActivityForResult(i, 7879);
        } else {
            Intent intent = new Intent(this, OverlayScreenService.class);


            Log.i("SERVICESTATUS", "notrunning");
            //  monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield, monthbadgedrawable;
            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
            if (prefs2.getString("ingress_appart", "-1").equalsIgnoreCase("0")) {
                isprime = false;
                svc.putExtra("ingress", "com.nianticlabs.ingress.prime.qa");
            } else {
                isprime = true;
                svc.putExtra("ingress", "com.nianticproject.ingress");
                // svc.putExtra("ingress", "com.android.chrome");
            }
            svc.putExtra("basistext", monthbadgetitle);
            svc.putExtra("activity", getCallingActivity());
            if (month.equalsIgnoreCase("month")) {
                svc.putExtra("comp", monthbadgefield);
            }
            if (month.equalsIgnoreCase("week")) {
                svc.putExtra("comp", weekbadgefield);
            }
            if (month.equalsIgnoreCase("earth")) {
                svc.putExtra("comp", earthbadgefield);
            }
            if (month.equalsIgnoreCase("mini")) {
                svc.putExtra("comp", minibadgefield);
                svc.putExtra("stats", stats);
            }
            svc.putExtra("agentname", AgentData.getagentname());
            svc.putExtra("mid", month);
            svc.putExtra("aid", cmonthmissionid);
            if (month.equalsIgnoreCase("month")) {
                needprime = false;
                svc.putExtra("sysbadge", badgemonthidscreener);
            } else if (month.equalsIgnoreCase("week")) {
                needprime = false;
                svc.putExtra("sysbadge", badgeweekidscreener);
            } else if (month.equalsIgnoreCase("earth")) {
                needprime = true;
                svc.putExtra("sysbadge", badgeearthidscreener);
            } else if (month.equalsIgnoreCase("mini")) {
                needprime = false;
                svc.putExtra("sysbadge", badgeminiidscreener);
            } else if (month.equalsIgnoreCase("aptest")) {
                svc.putExtra("basistext", "AP");
                needprime = false;
                svc.putExtra("sysbadge", badgeearthidscreener);
            } else {
                needprime = false;
                svc.putExtra("sysbadge", 0);
            }

            // PRIME WIRD BENÖTIGT UND ES IST NICHT PRIME
            if (needprime && !isprime) {
                StyleableToast.makeText(getApplicationContext(), "Sorry, this mission only works with Ingress Prime! Please change it in the AOD app settings.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                recreate();
            } else {
                bindService(intent, connectionService, Context.BIND_AUTO_CREATE);

                startService(svc);
            }
        }
    }

    private void startscreenshotpvp(String art, String thepvpkey, String pvpthebadgeid, String pvpthebadgetitle, int thesysbadgeid) {
        SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(AODMain.this);

        Log.i("THEBADGE ID --> ", "" + thesysbadgeid);
        if (prefs2.getBoolean("workaround", false)) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            workaround_actionid = art;
            workaround_month = thepvpkey;


            if (art.equalsIgnoreCase("pvp")) {
                workaroundpvptit = pvpthebadgetitle;
                startActivityForResult(i, 7881);
            } else {
                startActivityForResult(i, 7879);
            }
        } else {
            Intent intent = new Intent(this, OverlayScreenService.class);
            bindService(intent, connectionService, Context.BIND_AUTO_CREATE);

            Log.i("SERVICESTATUS", "notrunning");
            //  monthbadgetitle, monthbadgeshort1, monthbadgeshort2, monthbadgefield, monthbadgedrawable;
            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
            if (prefs2.getString("ingress_appart", "-1").equalsIgnoreCase("0")) {
                svc.putExtra("ingress", "com.nianticlabs.ingress.prime.qa");
            } else {
                svc.putExtra("ingress", "com.nianticproject.ingress");
            }
            svc.putExtra("basistext", pvpthebadgetitle);
            svc.putExtra("activity", getCallingActivity());
            svc.putExtra("comp", pvpthebadgeid);
            svc.putExtra("agentname", AgentData.getagentname());
            svc.putExtra("mid", art);
            svc.putExtra("aid", thepvpkey);
            svc.putExtra("sysbadge", thesysbadgeid);
            startService(svc);


        }
    }

    private void startscreenshotreset(String month, String cmonthmissionid) {
        SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(AODMain.this);

        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        workaround_actionid = month;
        workaround_month = cmonthmissionid;


        startActivityForResult(i, 7880);
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }

            return false;
        }

        return true;
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        Bundle params2 = new Bundle();
        Intent iranking = new Intent(getBaseContext(), RankingMain.class);
        //i.putExtra("PersonID", personID);
        iranking.putExtra("artis", "month");
        iranking.putExtra("theyear", theyearx);
        iranking.putExtra("themonth", themonthx);
        iranking.putExtra("theactionid", cmonthmissionid);
        iranking.putExtra("badgedrawable", monthbadgedrawable);
        iranking.putExtra("monthgold", goalgold);
        iranking.putExtra("monthplatin", goalplatin);
        iranking.putExtra("monthblack", goalblack);

        Intent inboxstart = new Intent(getBaseContext(), InboxMain.class);
        //i.putExtra("PersonID", personID);


        Intent irankingweek = new Intent(getBaseContext(), RankingMain.class);
        //i.putExtra("PersonID", personID);
        irankingweek.putExtra("artis", "week");
        irankingweek.putExtra("theyear", theyearx);
        irankingweek.putExtra("themonth", theweekx);
        irankingweek.putExtra("theactionid", cweekmissionid);
        irankingweek.putExtra("badgedrawable", weekbadgedrawable);
        irankingweek.putExtra("monthgold", goalgold_week);
        irankingweek.putExtra("monthplatin", goalplatin_week);
        irankingweek.putExtra("monthblack", goalblack_week);

        Intent irankingmini = new Intent(getBaseContext(), RankingMain.class);
        //i.putExtra("PersonID", personID);
        irankingmini.putExtra("artis", "mini");
        irankingmini.putExtra("theyear", theyearx);
        irankingmini.putExtra("themonth", cminimissionid);
        irankingmini.putExtra("theactionid", cminimissionid);
        irankingmini.putExtra("badgedrawable", minibadgedrawable);
        irankingmini.putExtra("monthgold", 0);
        irankingmini.putExtra("monthplatin", 0);
        irankingmini.putExtra("monthblack", 0);


        Intent irankingearth = new Intent(getBaseContext(), RankingMain.class);
        //i.putExtra("PersonID", personID);
        irankingearth.putExtra("artis", "earth");
        irankingearth.putExtra("theyear", theyearx);
        irankingearth.putExtra("themonth", cearthmissionid);
        irankingearth.putExtra("theactionid", cearthmissionid);
        irankingearth.putExtra("badgedrawable", earthbadgedrawable);
        irankingearth.putExtra("monthgold", goalgold_earth);
        irankingearth.putExtra("monthplatin", goalplatin_earth);
        irankingearth.putExtra("monthblack", goalblack_earth);
        // goalgold ,goalplatin,goalblack
        switch (v.getId()) {
            case R.id.hackventicon:
                // mExplosionField.clear();
                // StyleableToast.makeText(getApplicationContext(), "#soon (testmodus only)!", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                Intent hackvent = new Intent(getBaseContext(), HackventMain.class);
                startActivity(hackvent);
                //finish();
                break;
            case R.id.notify_count_image:
                // mExplosionField.clear();

                startActivity(inboxstart);
                //finish();
                break;
            case R.id.action_ranking:
                // mExplosionField.clear();
                params2 = new Bundle();
                params2.putString("agent", AgentData.getagentname());
                mFirebaseAnalytics.logEvent("aod_missions_month_rank", params2);
                startActivity(iranking);
                //finish();
                break;
            case R.id.action_ranking_week:
                // mExplosionField.clear();
                params2 = new Bundle();
                params2.putString("agent", AgentData.getagentname());
                mFirebaseAnalytics.logEvent("aod_missions_week_rank", params2);
                startActivity(irankingweek);
                //finish();
                break;
            case R.id.action_ranking_mini:
                // mExplosionField.clear();
                params2 = new Bundle();
                params2.putString("agent", AgentData.getagentname());
                mFirebaseAnalytics.logEvent("aod_missions_mini_rank", params2);
                startActivity(irankingmini);
                //finish();
                break;

            case R.id.getgoalmonth:
                // mExplosionField.clear();
                StyleableToast.makeText(getApplicationContext(), " " + goalgold + " | " + goalplatin + " | " + goalblack, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                //finish();
                break;

            case R.id.getgoalweek:
                // mExplosionField.clear();
                StyleableToast.makeText(getApplicationContext(), " " + goalgold_week + " | " + goalplatin_week + " | " + goalblack_week, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                //finish();
                break;
            case R.id.title_weight:
                // mExplosionField.clear();

                startActivity(iranking);
                //finish();
                break;

            case R.id.action_ranking_earth:
                // mExplosionField.clear();
                params2 = new Bundle();
                params2.putString("agent", AgentData.getagentname());
                mFirebaseAnalytics.logEvent("aod_missions_earth_rank", params2);
                startActivity(irankingearth);
                //finish();
                break;
            case R.id.title_weight_earth:
                // mExplosionField.clear();

                startActivity(irankingearth);
                //finish();
                break;

            case R.id.create_pvp:
                // mExplosionField.clear();
// PROOF ACTIVE PVPs

                mDatabase.child(user.getUid()).child("pvp").addListenerForSingleValueEvent(new ValueEventListener() {
                    long countpvps = 0;
                    long activepvp = 0;

                    @Override
                    public void onDataChange(DataSnapshot snapshotpvps) {

                        if (snapshotpvps.hasChildren()) {

                            for (DataSnapshot childpvp : snapshotpvps.getChildren()) {
                                countpvps++;
                                if (childpvp.child(childpvp.getKey()).hasChild("finished")) {
                                    if (!Boolean.parseBoolean(childpvp.child(childpvp.getKey()).child("finished").getValue().toString())) {
                                        activepvp++;
                                    }
                                }

                                if (countpvps >= snapshotpvps.getChildrenCount()) {
                                    if (activepvp >= 3) {
                                        StyleableToast.makeText(getApplicationContext(), "Sorry, you can only have 3 active / pending PvPs.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                    } else {
                                        if (pref.getBoolean("hasfriends", false)) {
                                            // StyleableToast.makeText(getApplicationContext(), "#soon (testmodus only)!", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                            Intent irankingh = new Intent(getBaseContext(), PvPCreate.class);


                                            startActivity(irankingh);
                                        } else {

                                            if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {

                                                StyleableToast.makeText(getApplicationContext(), "No friends found... only PvP by invite Link possible.", Toast.LENGTH_LONG, R.style.defaulttoast).show();


                                                Intent openPvP = new Intent(AODMain.this, PvPCreate.class);
                                                openPvP.putExtra("nofriends", "linkshare");

                                                startActivity(openPvP);
                                            } else {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                                // builder.setMessage("Text not finished...");
                                                LayoutInflater inflater = getLayoutInflater();
                                                View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                                builder.setView(dialog);
                                                final TextView input = dialog.findViewById(R.id.caltext);
                                                // input.setAllCaps(true);
                                                input.setTypeface(type);

                                                //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                                //localTextView1.setTypeface(proto);
                                                builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        // Log.i("Hallo", "hallo");
                                                        //handleinputcode(input.getText().toString());
                                                        // Todo publish the code to database
                                                        Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                        //i.putExtra("PersonID", personID);
                                                        startActivity(i);
                                                        //finish();

                                                    }
                                                });

                                                builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {


                                                        // Todo publish the code to database
                                                    }
                                                });

                                                builder.setCancelable(true);

                                                if (!isFinishing()) {
                                                    builder.create().show();

                                                }

                                            }

                                        }
                                    }
                                }
                            }

                        } else {
                            if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {

                                if (pref.getBoolean("hasfriends", false)) {
                                    // StyleableToast.makeText(getApplicationContext(), "#soon (testmodus only)!", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                    Intent irankingh = new Intent(getBaseContext(), PvPCreate.class);


                                    startActivity(irankingh);
                                } else {
                                    StyleableToast.makeText(getApplicationContext(), "No friends found... only PvP by invite by link possible.", Toast.LENGTH_LONG, R.style.defaulttoast).show();


                                    Intent openPvP = new Intent(AODMain.this, PvPCreate.class);
                                    openPvP.putExtra("nofriends", "linkshare");

                                    startActivity(openPvP);
                                }
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                // builder.setMessage("Text not finished...");
                                LayoutInflater inflater = getLayoutInflater();
                                View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                builder.setView(dialog);
                                final TextView input = dialog.findViewById(R.id.caltext);
                                // input.setAllCaps(true);
                                input.setTypeface(type);

                                //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                //localTextView1.setTypeface(proto);
                                builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        // Log.i("Hallo", "hallo");
                                        //handleinputcode(input.getText().toString());
                                        // Todo publish the code to database
                                        Intent i = new Intent(getBaseContext(), MainSettings.class);
                                        //i.putExtra("PersonID", personID);
                                        startActivity(i);
                                        //finish();

                                    }
                                });

                                builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {


                                        // Todo publish the code to database
                                    }
                                });

                                builder.setCancelable(true);

                                if (!isFinishing()) {
                                    builder.create().show();

                                }

                            }
                        }


                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                });

                break;
            case R.id.title_weight_week:
                // mExplosionField.clear();

                startActivity(irankingweek);
                //finish();
                break;

            case R.id.agentimage:
                // mExplosionField.clear();
                params2 = new Bundle();
                params2.putString("agent", AgentData.getagentname());
                mFirebaseAnalytics.logEvent("aod_profile_image_action", params2);
                Intent i = new Intent(getBaseContext(), ProfileMain.class);
                //i.putExtra("PersonID", personID);
                startActivity(i);
                //finish();
                break;
            case R.id.is_communitymission:
                // mExplosionField.clear();
                if (swipe_layout_community.isClosed() && !swipe_layout_community.isDragLocked()) {
                    swipe_layout_community.open(true);


                    swipe_team_month.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    swipe_team_month.setVisibility(View.GONE);
                                    communityframe.setAlpha(1f);
                                    communityframe.setVisibility(View.VISIBLE);
                                }
                            });
                    //swipe_team_month.setVisibility(View.GONE);
                    //communityframe.setVisibility(View.VISIBLE);

                    ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcmonth.setLayoutParams(params);
                    fcmonth.requestLayout();


                }
                if (swipe_layout_community.isOpened()) {
                    swipe_layout_community.close(true);


                    communityframe.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    communityframe.setVisibility(View.GONE);
                                    swipe_team_month.setAlpha(1f);
                                    swipe_team_month.setVisibility(View.VISIBLE);
                                }
                            });
                    //  communityframe.setVisibility(View.GONE);
                    //  swipe_team_month.setVisibility(View.VISIBLE);
                    ViewGroup.LayoutParams params = fcmonth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcmonth.setLayoutParams(params);
                    fcmonth.requestLayout();


                }
                //Toast.makeText(v.getContext(), "Map Filter coming soon", Toast.LENGTH_SHORT).show();
                break;

            case R.id.is_communitymission_week:
                // mExplosionField.clear();
                if (swipe_layout_community_week.isClosed() && !swipe_layout_community_week.isDragLocked()) {
                    swipe_layout_community_week.open(true);


                    swipe_team_week.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    swipe_team_week.setVisibility(View.GONE);
                                    communityframe_week.setAlpha(1f);
                                    communityframe_week.setVisibility(View.VISIBLE);
                                }
                            });
                    //swipe_team_month.setVisibility(View.GONE);
                    //communityframe.setVisibility(View.VISIBLE);

                    ViewGroup.LayoutParams params = fcweek.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcweek.setLayoutParams(params);
                    fcweek.requestLayout();


                }
                if (swipe_layout_community_week.isOpened()) {
                    swipe_layout_community_week.close(true);


                    communityframe_week.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    communityframe_week.setVisibility(View.GONE);
                                    swipe_team_week.setAlpha(1f);
                                    swipe_team_week.setVisibility(View.VISIBLE);
                                }
                            });
                    //  communityframe.setVisibility(View.GONE);
                    //  swipe_team_month.setVisibility(View.VISIBLE);
                    ViewGroup.LayoutParams params = fcweek.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcweek.setLayoutParams(params);
                    fcweek.requestLayout();


                }
                //Toast.makeText(v.getContext(), "Map Filter coming soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.is_communitymission_earth:
                // mExplosionField.clear();
                if (swipe_layout_community_earth.isClosed() && !swipe_layout_community_earth.isDragLocked()) {
                    swipe_layout_community_earth.open(true);


                    swipe_team_earth.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    swipe_team_earth.setVisibility(View.GONE);
                                    communityframe_earth.setAlpha(1f);
                                    communityframe_earth.setVisibility(View.VISIBLE);
                                }
                            });
                    //swipe_team_month.setVisibility(View.GONE);
                    //communityframe.setVisibility(View.VISIBLE);

                    ViewGroup.LayoutParams params = fcearth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcearth.setLayoutParams(params);
                    fcearth.requestLayout();


                }
                if (swipe_layout_community_earth.isOpened()) {
                    swipe_layout_community_earth.close(true);


                    communityframe_earth.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    communityframe_earth.setVisibility(View.GONE);
                                    swipe_team_earth.setAlpha(1f);
                                    swipe_team_earth.setVisibility(View.VISIBLE);
                                }
                            });
                    //  communityframe.setVisibility(View.GONE);
                    //  swipe_team_month.setVisibility(View.VISIBLE);
                    ViewGroup.LayoutParams params = fcearth.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    fcearth.setLayoutParams(params);
                    fcearth.requestLayout();


                }
                //Toast.makeText(v.getContext(), "Map Filter coming soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.close_btn_community:
                // mExplosionField.clear();
                if (swipe_layout_community.isClosed() && !swipe_layout_community.isDragLocked()) {
                    swipe_layout_community.open(true);

                    swipe_team_month.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    swipe_team_month.setVisibility(View.GONE);
                                    communityframe.setAlpha(1f);
                                    communityframe.setVisibility(View.VISIBLE);
                                }
                            });
                    // communityframe.setVisibility(View.VISIBLE);
                    // swipe_team_month.setVisibility(View.GONE);
                }
                if (swipe_layout_community.isOpened()) {
                    swipe_layout_community.close(true);

                    communityframe.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    communityframe.setVisibility(View.GONE);
                                    swipe_team_month.setAlpha(1f);
                                    swipe_team_month.setVisibility(View.VISIBLE);
                                }
                            });
                    //communityframe.setVisibility(View.GONE);
                    // swipe_team_month.setVisibility(View.VISIBLE);
                }
                //Toast.makeText(v.getContext(), "Map Filter coming soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.close_btn_community_week:
                // mExplosionField.clear();
                if (swipe_layout_community_week.isClosed() && !swipe_layout_community_week.isDragLocked()) {
                    swipe_layout_community_week.open(true);

                    swipe_team_week.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    swipe_team_week.setVisibility(View.GONE);
                                    communityframe_week.setAlpha(1f);
                                    communityframe_week.setVisibility(View.VISIBLE);
                                }
                            });
                    // communityframe.setVisibility(View.VISIBLE);
                    // swipe_team_month.setVisibility(View.GONE);
                }
                if (swipe_layout_community_week.isOpened()) {
                    swipe_layout_community_week.close(true);

                    communityframe_week.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    communityframe_week.setVisibility(View.GONE);
                                    swipe_team_week.setAlpha(1f);
                                    swipe_team_week.setVisibility(View.VISIBLE);
                                }
                            });
                    //communityframe.setVisibility(View.GONE);
                    // swipe_team_month.setVisibility(View.VISIBLE);
                }
                //Toast.makeText(v.getContext(), "Map Filter coming soon", Toast.LENGTH_SHORT).show();
                break;
            case R.id.close_btn_community_earth:
                // mExplosionField.clear();
                if (swipe_layout_community_earth.isClosed() && !swipe_layout_community_earth.isDragLocked()) {
                    swipe_layout_community_earth.open(true);

                    swipe_team_earth.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    swipe_team_earth.setVisibility(View.GONE);
                                    communityframe_earth.setAlpha(1f);
                                    communityframe_earth.setVisibility(View.VISIBLE);
                                }
                            });
                    // communityframe.setVisibility(View.VISIBLE);
                    // swipe_team_month.setVisibility(View.GONE);
                }
                if (swipe_layout_community_earth.isOpened()) {
                    swipe_layout_community_earth.close(true);

                    communityframe_earth.animate()
                            .translationY(0)
                            .alpha(0.0f)
                            .setDuration(200)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    communityframe_earth.setVisibility(View.GONE);
                                    swipe_team_earth.setAlpha(1f);
                                    swipe_team_earth.setVisibility(View.VISIBLE);
                                }
                            });
                    //communityframe.setVisibility(View.GONE);
                    // swipe_team_month.setVisibility(View.VISIBLE);
                }
                //Toast.makeText(v.getContext(), "Map Filter coming soon", Toast.LENGTH_SHORT).show();
                break;


            case R.id.content_request_btn_mini:

                if (isStoragePermissionGranted()) {
                    Log.v(TAG, "Permission is granted");
                    //File write logic here
                    if (statsart.equalsIgnoreCase("screen")) {
                        if (pref.getBoolean(remoteconfig.getString("pref_calibration_invent"), false)) {
                            // mExplosionField.clear();
                            if (isMyServiceRunning(OverlayScreenService.class)) {
                                Log.i("SERVICESTATUS", "running");
                                Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                                stopService(svc);
                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    if (Settings.canDrawOverlays(AODMain.this)) {

                                        startscreenshot("mini", cminimissionid);
                                        // startActivityForResult(intent , 7002);
                                    } else {
                                        verifyOverlay(AODMain.this);
                                    }
                                } else {
                                    startscreenshot("mini", cminimissionid);
                                    //   startActivityForResult(intent , 7002);
                                }
                            }
                        } else {
                            if (isStoragePermissionGranted()) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                // builder.setMessage("Text not finished...");
                                LayoutInflater inflater = getLayoutInflater();
                                View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                builder.setView(dialog);
                                final TextView input = dialog.findViewById(R.id.caltext);
                                // input.setAllCaps(true);
                                input.setTypeface(type);

                                //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                //localTextView1.setTypeface(proto);
                                builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        // Log.i("Hallo", "hallo");
                                        //handleinputcode(input.getText().toString());
                                        // Todo publish the code to database
                                        Intent i = new Intent(getBaseContext(), MainSettings.class);
                                        //i.putExtra("PersonID", personID);
                                        startActivity(i);
                                        // finish();

                                    }
                                });

                                builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {


                                        // Todo publish the code to database
                                    }
                                });

                                builder.setCancelable(true);

                                if (!isFinishing()) {
                                    builder.create().show();

                                }
                            }
                        }
                    } else {
                        if (isMyServiceRunning(OverlayScreenService.class)) {
                            Log.i("SERVICESTATUS", "running");
                            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                            stopService(svc);
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (Settings.canDrawOverlays(AODMain.this)) {

                                    startscreenshot("mini", cminimissionid);
                                    // startActivityForResult(intent , 7002);
                                } else {
                                    verifyOverlay(AODMain.this);
                                }
                            } else {
                                startscreenshot("mini", cminimissionid);
                                //   startActivityForResult(intent , 7002);
                            }
                        }
                    }
                }


                break;
            case R.id.content_request_btn_month:

                if (isStoragePermissionGranted()) {
                    Log.v(TAG, "Permission is granted");
                    //File write logic here
                    if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                        // mExplosionField.clear();
                        if (isMyServiceRunning(OverlayScreenService.class)) {
                            Log.i("SERVICESTATUS", "running");
                            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                            stopService(svc);
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (Settings.canDrawOverlays(AODMain.this)) {

                                    startscreenshot("month", cmonthmissionid);
                                    // startActivityForResult(intent , 7002);
                                } else {
                                    verifyOverlay(AODMain.this);
                                }
                            } else {
                                startscreenshot("month", cmonthmissionid);
                                //   startActivityForResult(intent , 7002);
                            }
                        }
                    } else {
                        if (isStoragePermissionGranted()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                            // builder.setMessage("Text not finished...");
                            LayoutInflater inflater = getLayoutInflater();
                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                            builder.setView(dialog);
                            final TextView input = dialog.findViewById(R.id.caltext);
                            // input.setAllCaps(true);
                            input.setTypeface(type);

                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                            //localTextView1.setTypeface(proto);
                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Log.i("Hallo", "hallo");
                                    //handleinputcode(input.getText().toString());
                                    // Todo publish the code to database
                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                    //i.putExtra("PersonID", personID);
                                    startActivity(i);
                                    // finish();

                                }
                            });

                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    // Todo publish the code to database
                                }
                            });

                            builder.setCancelable(true);

                            if (!isFinishing()) {
                                builder.create().show();

                            }
                        }
                    }
                }


                break;

            case R.id.content_request_btn_earth:

                if (isStoragePermissionGranted()) {
                    Log.v(TAG, "Permission is granted");
                    //File write logic here
                    if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                        // mExplosionField.clear();
                        if (isMyServiceRunning(OverlayScreenService.class)) {
                            Log.i("SERVICESTATUS", "running");
                            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                            stopService(svc);
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (Settings.canDrawOverlays(AODMain.this)) {

                                    startscreenshot("earth", cearthmissionid);
                                    // startActivityForResult(intent , 7002);
                                } else {
                                    verifyOverlay(AODMain.this);
                                }
                            } else {
                                startscreenshot("earth", cearthmissionid);
                                //   startActivityForResult(intent , 7002);
                            }
                        }
                    } else {
                        if (isStoragePermissionGranted()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                            // builder.setMessage("Text not finished...");
                            LayoutInflater inflater = getLayoutInflater();
                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                            builder.setView(dialog);
                            final TextView input = dialog.findViewById(R.id.caltext);
                            // input.setAllCaps(true);
                            input.setTypeface(type);

                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                            //localTextView1.setTypeface(proto);
                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Log.i("Hallo", "hallo");
                                    //handleinputcode(input.getText().toString());
                                    // Todo publish the code to database
                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                    //i.putExtra("PersonID", personID);
                                    startActivity(i);
                                    //   finish();

                                }
                            });

                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    // Todo publish the code to database
                                }
                            });

                            builder.setCancelable(true);

                            if (!isFinishing()) {
                                builder.create().show();

                            }
                        }
                    }
                }


                break;

            case R.id.content_request_btn_week:
                // mExplosionField.clear();
                if (isStoragePermissionGranted()) {
                    Log.v(TAG, "Permission is granted");
                    //File write logic here
                    if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                        // mExplosionField.clear();
                        if (isMyServiceRunning(OverlayScreenService.class)) {
                            Log.i("SERVICESTATUS", "running");
                            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                            stopService(svc);
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (Settings.canDrawOverlays(AODMain.this)) {

                                    startscreenshot("week", cweekmissionid);
                                    // startActivityForResult(intent , 7002);
                                } else {
                                    verifyOverlay(AODMain.this);
                                }
                            } else {
                                startscreenshot("month", cweekmissionid);
                                //   startActivityForResult(intent , 7002);
                            }
                        }
                    } else {
                        if (isStoragePermissionGranted()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                            // builder.setMessage("Text not finished...");
                            LayoutInflater inflater = getLayoutInflater();
                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                            builder.setView(dialog);
                            final TextView input = dialog.findViewById(R.id.caltext);
                            // input.setAllCaps(true);
                            input.setTypeface(type);

                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                            //localTextView1.setTypeface(proto);
                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Log.i("Hallo", "hallo");
                                    //handleinputcode(input.getText().toString());
                                    // Todo publish the code to database
                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                    //i.putExtra("PersonID", personID);
                                    startActivity(i);
                                    // finish();

                                }
                            });

                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    // Todo publish the code to database
                                }
                            });

                            builder.setCancelable(true);

                            if (!isFinishing()) {
                                builder.create().show();

                            }
                        }
                    }
                }
                break;

            case R.id.content_request_btn_week_manuell:
                // mExplosionField.clear();
                if (isStoragePermissionGranted()) {
                    Log.v(TAG, "Permission is granted");
                    //File write logic here
                    if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                        // mExplosionField.clear();
                        if (isMyServiceRunning(OverlayScreenService.class)) {
                            Log.i("SERVICESTATUS", "running");
                            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                            stopService(svc);
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (Settings.canDrawOverlays(AODMain.this)) {

                                    startscreenshotreset("week", cweekmissionid);
                                    // startActivityForResult(intent , 7002);
                                } else {
                                    verifyOverlay(AODMain.this);
                                }
                            } else {
                                startscreenshotreset("week", cweekmissionid);
                                //   startActivityForResult(intent , 7002);
                            }
                        }
                    } else {
                        if (isStoragePermissionGranted()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                            // builder.setMessage("Text not finished...");
                            LayoutInflater inflater = getLayoutInflater();
                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                            builder.setView(dialog);
                            final TextView input = dialog.findViewById(R.id.caltext);
                            // input.setAllCaps(true);
                            input.setTypeface(type);

                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                            //localTextView1.setTypeface(proto);
                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Log.i("Hallo", "hallo");
                                    //handleinputcode(input.getText().toString());
                                    // Todo publish the code to database
                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                    //i.putExtra("PersonID", personID);
                                    startActivity(i);
                                    //  finish();

                                }
                            });

                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    // Todo publish the code to database
                                }
                            });

                            builder.setCancelable(true);

                            if (!isFinishing()) {
                                builder.create().show();

                            }
                        }
                    }
                }
                break;

            case R.id.content_request_btn_month_manuell:
                // mExplosionField.clear();
                if (isStoragePermissionGranted()) {
                    Log.v(TAG, "Permission is granted");
                    //File write logic here
                    if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {
                        // mExplosionField.clear();
                        if (isMyServiceRunning(OverlayScreenService.class)) {
                            Log.i("SERVICESTATUS", "running");
                            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
                            stopService(svc);
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (Settings.canDrawOverlays(AODMain.this)) {

                                    startscreenshotreset("month", cweekmissionid);
                                    // startActivityForResult(intent , 7002);
                                } else {
                                    verifyOverlay(AODMain.this);
                                }
                            } else {
                                startscreenshotreset("month", cweekmissionid);
                                //   startActivityForResult(intent , 7002);
                            }
                        }
                    } else {
                        if (isStoragePermissionGranted()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                            // builder.setMessage("Text not finished...");
                            LayoutInflater inflater = getLayoutInflater();
                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                            builder.setView(dialog);
                            final TextView input = dialog.findViewById(R.id.caltext);
                            // input.setAllCaps(true);
                            input.setTypeface(type);

                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                            //localTextView1.setTypeface(proto);
                            builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Log.i("Hallo", "hallo");
                                    //handleinputcode(input.getText().toString());
                                    // Todo publish the code to database
                                    Intent i = new Intent(getBaseContext(), MainSettings.class);
                                    //i.putExtra("PersonID", personID);
                                    startActivity(i);
                                    //finish();

                                }
                            });

                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                    // Todo publish the code to database
                                }
                            });

                            builder.setCancelable(true);

                            if (!isFinishing()) {
                                builder.create().show();

                            }
                        }
                    }
                }
                break;
            case R.id.donation_store:
                // mExplosionField.clear();
                params2 = new Bundle();
                params2.putString("agent", AgentData.getagentname());
                mFirebaseAnalytics.logEvent("aod_shop_action", params2);
                Intent idona = new Intent(getBaseContext(), Donation.class);
                //i.putExtra("PersonID", personID);
                startActivity(idona);
                //finish();
                break;

        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    public boolean isNetworkPermissionGranted() {

        // if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        //
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    @Override
    public void connectToDeviceclip(Intent intent) {

        Log.d("IMAGE MAIN", " " + intent.getExtras().getString("screen"));
        if (isMyServiceRunning(OverlayScreenService.class)) {
            Log.i("SERVICESTATUS2 MAIN", "running");
            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
            stopService(svc);
        }
        if (mBound) {
            mService.setCallbacks(null); // unregister
            unbindService(connectionService);
            mBound = false;
        }


        String missionart = intent.getExtras().getString("onactionid");
        String onactionid = intent.getExtras().getString("missionart");
        String neededbadge = intent.getExtras().getString("needbadge");
        String lifetimeclip = intent.getExtras().getString("lifetimeclip");
        String valueclip = intent.getExtras().getString("value");
        String timespanclip = intent.getExtras().getString("timespanclip");
        String agentclip = intent.getExtras().getString("agentclip");
        String statsi = intent.getExtras().getString("stats");

        Log.i("SERVICESTATUS3", neededbadge);


        recognizeTextClip(valueclip, onactionid, missionart, neededbadge, lifetimeclip, timespanclip, agentclip, true, statsi);


        //connectToDevice(intent);
    }

    @Override
    public void connectToDevice(Intent intent) {

        Log.d("IMAGE MAIN", " " + intent.getExtras().getString("screen"));
        if (isMyServiceRunning(OverlayScreenService.class)) {
            Log.i("SERVICESTATUS2 MAIN", "running");
            Intent svc = new Intent(AODMain.this, OverlayScreenService.class);
            stopService(svc);
        }
        if (mBound) {
            mService.setCallbacks(null); // unregister
            unbindService(connectionService);
            mBound = false;
        }


        ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setTitle("AOD Magic");
        pd.setMessage("Please wait...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.show();


        String missionart = intent.getExtras().getString("onactionid");
        String onactionid = intent.getExtras().getString("missionart");
        String neededbadge = intent.getExtras().getString("needbadge");
        Log.i("SERVICESTATUS3", neededbadge);


        Bitmap bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));


        if (bitmap == null) {

            try {
                Thread.sleep(1000); //add another 3 seconds delay
                bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));

            } catch (InterruptedException e) {
                bitmap = null;
                e.printStackTrace();
            }
        }


        if (bitmap == null) {

            try {
                Thread.sleep(2000); //add another 3 seconds delay
                bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));
            } catch (InterruptedException e) {
                bitmap = null;
                e.printStackTrace();
            }
        }

        if (bitmap == null) {

            try {
                Thread.sleep(2000); //add another 3 seconds delay
                bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));
            } catch (InterruptedException e) {
                bitmap = null;
                e.printStackTrace();
            }
        }
        if (bitmap != null) {

            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
            ContentResolver contentResolver = getContentResolver();


            recognizeText(image, onactionid, missionart, neededbadge, pd);

            if (mainprefssettings.getBoolean("save_screen", false)) {
                StyleableToast.makeText(getApplicationContext(), "Screenshot saved", Toast.LENGTH_LONG, R.style.defaulttoast).show();
            } else {

                contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        MediaStore.Images.ImageColumns.DATA + "=?", new String[]{intent.getExtras().getString("screen")});
            }
        } else {
            if (pd != null)
                if (pd.isShowing())
                    if (!isFinishing()) {
                        pd.dismiss();
                        pd = null;
                    }

            StyleableToast.makeText(getApplicationContext(), "Something did not work. Please check the app permissions.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

        }
        //connectToDevice(intent);
    }

    public void connectToDevicemanuel(Intent intent) {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setTitle("AOD Magic");
        pd.setMessage("Please wait...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        //pd.show();
        if (intent.getExtras().getString("screen") == null) {
            StyleableToast.makeText(getApplicationContext(), "ERROR: The screenshot must be saved locally. Create a new screenshot and try again.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
        } else {
            Log.d("IMAGE", " " + intent.getExtras().getString("screen"));

            String missionart = workaround_actionid;
            String onactionid = workaround_month;
            workaround_month = null;
            workaround_actionid = null;
            mDatabase.child(user.getUid()).child("reset").child(missionart).setValue(null);
            Bitmap bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));

            // Bitmap graybitmap = convertToGrayscale(bitmap);
            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);

            recognizeText(image, onactionid, missionart, workaroundpvptit, pd);


            ContentResolver contentResolver = getContentResolver();
            if (mainprefssettings.getBoolean("save_screen", false)) {
                StyleableToast.makeText(getApplicationContext(), "Screenshot saved", Toast.LENGTH_LONG, R.style.defaulttoast).show();
            } else {
                contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        MediaStore.Images.ImageColumns.DATA + "=?", new String[]{intent.getExtras().getString("screen")});
            }

            //connectToDevice(intent);
        }
    }

    @Override
    public void closewindow(final String compx, final String theartid, final String themid, final String basistext, final String stasis) {
        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> rt = am.getRunningTasks(Integer.MAX_VALUE);
        Log.w("Back -->", "pressed k " + rt.size());


        for (int i = 0; i < rt.size(); i++) {

            // bring to front
            if (rt.get(i).baseActivity.toShortString().indexOf("rocks.prxenon.aod") > -1) {
                am.moveTaskToFront(rt.get(i).id, ActivityManager.MOVE_TASK_WITH_HOME);
                Intent svc = new Intent(getApplicationContext(), OverlayScreenService.class);
                stopService(svc);
            }

        }
        if (mBound) {
            mService.setCallbacks(null); // unregister
            unbindService(connectionService);
            mBound = false;
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            try {
                Thread.sleep(2000); //add another 3 seconds delay
                androidQworkaround(compx, theartid, themid, basistext, stasis);
            } catch (InterruptedException e) {
                androidQworkaround(compx, theartid, themid, basistext, stasis);
                e.printStackTrace();
            }


        }


    }

    private void recognizeText(FirebaseVisionImage image, final String onactionid, final String missionart, final String badgeneed, final ProgressDialog pd) {
        final boolean[] correctbage = {false};
        final boolean[] apbadge = {false};
        final boolean[] correctagent = {false};
        final boolean[] islagenumber = {false};
        // [START get_detector_default]
        FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance()
                .getOnDeviceTextRecognizer();
        // [END get_detector_default]

        // [START run_detector]


        detector.processImage(image)
                .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                    @Override
                    public void onSuccess(FirebaseVisionText firebaseVisionText) {
                        // Task completed successfully
                        // [START_EXCLUDE]
                        // [START get_text]

                        String xx = firebaseVisionText.getText();
                        String needbadge;
                        if (missionart.equalsIgnoreCase("month")) {
                            needbadge = monthbadgetitle;
                        } else if (missionart.equalsIgnoreCase("week")) {
                            needbadge = weekbadgetitle;
                        } else if (missionart.equalsIgnoreCase("earth")) {
                            needbadge = earthbadgetitle;
                        } else if (missionart.equalsIgnoreCase("pvp")) {
                            needbadge = badgeneed;
                        } else if (missionart.equalsIgnoreCase("aptest")) {
                            needbadge = badgeneed;
                        } else {
                            needbadge = monthbadgetitle;
                        }
                        boolean found = false;
                        boolean lifetime = false;
                        String debugt = "null";
                        String debugttxt = "DEBUG: ";
                        String iffails = "null";
                        String resultis = "nothing";
                        int calint = pref.getInt("calint", 0);
                        Log.i("CALIBRATION --_>", "" + calint);


                        String needbadge2 = needbadge.replaceAll("I", "1").replaceAll("i", "1").replaceAll("L", "1").replaceAll("l", "1").replaceAll("Z", "2").replaceAll("z", "2");


                        if (needbadge.equalsIgnoreCase("AP")) {
                            apbadge[0] = true;
                            if (xx.contains(needbadge) || xx.contains("PA")) {
                                Log.i(needbadge + "--->", "yes");
                                correctbage[0] = true;
                            }
                        } else {
                            if (needbadge.equalsIgnoreCase("Recharger")) {
                                islagenumber[0] = true;
                            }
                            if (needbadge.equalsIgnoreCase("nator")) {
                                islagenumber[0] = true;
                            }
                            if (xx.contains(needbadge) || xx.contains(needbadge2) || xx.contains("nator")) {
                                Log.i(needbadge + "--->", "yes");
                                correctbage[0] = true;
                            }
                        }


                        for (FirebaseVisionText.TextBlock block : firebaseVisionText.getTextBlocks()) {

                            Log.i("BLOCK--", block.getText());

                            if (AgentData.getagentname().length() >= 3) {
                                String text = block.getText();
                                // IS Trekkerbadge
                                text = text.replaceAll("o", "");
                                text = text.replaceAll("O", "");
                                text = text.replaceAll("l", "");
                                text = text.replaceAll("i", "");
                                text = text.replaceAll("I", "");
                                text = text.replaceAll("L", "");
                                text = text.replaceAll(",", "");
                                text = text.replaceAll("\\.", "");
                                text = text.replaceAll("0", "");
                                text = text.replaceAll("1", "");
                                // text = text.replaceAll("[^0-9]", "");

                                //  ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                // ClipData clip = ClipData.newPlainText("AOD Debug", text.toString());
                                // clipboard.setPrimaryClip(clip);

                                String agentcleanded = AgentData.getagentname();
                                agentcleanded = agentcleanded.replaceAll("o", "");
                                agentcleanded = agentcleanded.replaceAll("O", "");
                                agentcleanded = agentcleanded.replaceAll("l", "");
                                agentcleanded = agentcleanded.replaceAll("i", "");
                                agentcleanded = agentcleanded.replaceAll("I", "");
                                agentcleanded = agentcleanded.replaceAll("L", "");
                                agentcleanded = agentcleanded.replaceAll(",", "");
                                agentcleanded = agentcleanded.replaceAll("\\.", "");
                                agentcleanded = agentcleanded.replaceAll("0", "");
                                agentcleanded = agentcleanded.replaceAll("1", "");
                                agentcleanded = agentcleanded.substring(0, 2);
                                if (text.contains(agentcleanded)) {
                                    Log.i(agentname + "--->", "yes");
                                    correctagent[0] = true;
                                }
                            } else {
                                String textxx = block.getText();
                                if (textxx.contains(AgentData.getagentname())) {
                                    Log.i(agentname + "--->", "yes");
                                    correctagent[0] = true;
                                }
                            }


                            //  Log.w("Block-> ",block.getText()+""+block.getBoundingBox().top);
                            for (FirebaseVisionText.Line line : block.getLines()) {

                                // AP NEEDED
                                if (apbadge[0]) {


                                    // Log.w("Line-> ", line.getText() + " top: " + line.getBoundingBox().top);
                                    if ((line.getText().contains("AP") || line.getText().contains("PA"))) {
                                        //   StyleableToast.makeText(getApplicationContext(), "Liftime not found 1 "+line.getText(), Toast.LENGTH_LONG, R.style.defaulttoast).show();


                                        if (line.getText().contains("Lifetime") || line.getText().contains("Lebenszeit") || line.getText().contains("vital") || line.getText().length() >= 17) {
                                            debugttxt = debugttxt + " 100: " + line.getText();
                                            found = true;
                                            lifetime = true;
                                            resultis = line.getText();
                                            Log.i("Lifetime AP Wert ", resultis);
                                            debugt = "" + block.getBoundingBox().top + " Line1";
                                            iffails = "" + line.getText();
                                        } else if ((line.getText().contains("AP /") || line.getText().contains("AP/") || line.getText().contains("AP 1")) && (!found && !lifetime)) {
                                            debugttxt = debugttxt + " 200: " + line.getText();
                                            found = true;
                                            lifetime = false;
                                            resultis = line.getText();
                                            Log.i("NO Lifetime split AP", resultis);
                                            debugt = "" + block.getBoundingBox().top + " Line1";
                                            iffails = "" + line.getText();
                                        } else {
                                            debugttxt = debugttxt + " 300: " + line.getText();

                                            if (!found && !lifetime) {
                                                found = true;
                                                resultis = line.getText();
                                                Log.i("AP Wert ", resultis);
                                                lifetime = false;
                                                Log.i("NO Lifetime L16", resultis);
                                                debugt = "" + block.getBoundingBox().top + " Line1";
                                                iffails = "" + line.getText();
                                            }
                                        }

                                    }
                                } else {
                                    // ...
                                    Log.w("Line-> ", line.getText() + " top: " + line.getBoundingBox().top);
                                    //if(block.getBoundingBox().top == calint) {
                                    //  Log.w("HERE-> ", line.getText() + " top: " + line.getBoundingBox().top + " calint: " + calint);
                                    listdebug.add(line.getText() + " t " + line.getBoundingBox().top);
                                    if (line.getBoundingBox().top >= (calint - 5) && line.getBoundingBox().top <= (calint + 5)) {
                                        Log.i("IS IN RANGE ", "true");
                                        found = true;
                                        resultis = line.getText();
                                        debugt = "" + block.getBoundingBox().top + " Line1";
                                        iffails = "" + line.getText();

                                               /* DisplayMetrics metrics = getBaseContext().getResources().getDisplayMetrics();
                                                int w = metrics.widthPixels;
                                                int h = metrics.heightPixels;

                                                Bitmap bitmap = Bitmap.createBitmap(
                                                        w, // Width
                                                        h, // Height
                                                        Bitmap.Config.ARGB_8888 // Config
                                                );
                                                Canvas canvas = new Canvas(bitmap);

                                                // Draw a solid color to the canvas background
                                                canvas.drawColor(Color.TRANSPARENT);

                                                Paint paint = new Paint();

                                                paint.setColor(Color.YELLOW);
                                                paint.setStrokeWidth(4);

                                                paint.setStyle(Paint.Style.STROKE);
                                                paint.setAntiAlias(true);



                                                canvas.drawRect(block.getBoundingBox(), paint);


                                                ImageView iv = new ImageView(getApplicationContext());

                                                // Set an image for ImageView
                                                iv.setImageBitmap(bitmap);

                                                // Create layout parameters for ImageView
                                                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

                                                // Add rule to layout parameters
                                                // Add the ImageView below to Button


                                                // Add layout parameters to ImageView
                                                iv.setLayoutParams(lp);
                                                iv.setPadding(25,25,25,25);

                                                // Finally, add the ImageView to layout
                                                baseframe.addView(iv); */

                                    } else if ((line.getBoundingBox().top + 5) >= (calint - 5) && (line.getBoundingBox().top + 5) <= (calint + 5)) {
                                        Log.i("IS IN RANGE 2", "true");
                                        found = true;
                                        resultis = line.getText();
                                        debugt = "" + block.getBoundingBox().top + " Line2";
                                        iffails = "" + line.getText();
                                        Paint paint = new Paint();


                                        paint.setColor(Color.YELLOW);
                                        paint.setStrokeWidth(3);
                                        Canvas canvas = new Canvas();
                                        canvas.drawRect(line.getBoundingBox().left, line.getBoundingBox().top, line.getBoundingBox().right, line.getBoundingBox().bottom, paint);

                                    } else {

                                        if (line.getText().contains("MUs") && needbadge.equalsIgnoreCase("Illuminator")) {
                                            Log.i("IS IN RANGE 4", "true");
                                            found = true;
                                            resultis = line.getText();
                                            debugt = "" + block.getBoundingBox().top + " Line2";
                                            iffails = "" + line.getText();
                                            Paint paint = new Paint();


                                            paint.setColor(Color.YELLOW);
                                            paint.setStrokeWidth(3);
                                            Canvas canvas = new Canvas();
                                            canvas.drawRect(line.getBoundingBox().left, line.getBoundingBox().top, line.getBoundingBox().right, line.getBoundingBox().bottom, paint);
                                        }

                                    }

                                }


                            }
                        }

                        if (pref.getBoolean("xtra", false)) {
                            correctagent[0] = true;
                        }

                        if (found && correctbage[0] && correctagent[0]) {
                            if (!debugt.equalsIgnoreCase("null")) {
                                //  infoToast("DEBUG "+debugt);
                            }

                            String text = listdebug.toString();

                            //  ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                            //  ClipData clip = ClipData.newPlainText("debug_text", text);
                            //  clipboard.setPrimaryClip(clip);
                            //    StyleableToast.makeText(getApplicationContext(), "DEBUG 1 "+resultis, Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            Log.i("WOHOOO ---> ", resultis + " missionID: " + onactionid + " art: " + missionart);
                            int theresult = 0;

                            // IS Trekkerbadge
                            if (resultis.contains("km") || resultis.contains("MUs") || resultis.contains("MU") || resultis.contains("XM")) {

                                if (resultis.contains("km")) {
                                    String[] separated = resultis.split("km");
                                    resultis = separated[0].toUpperCase();
                                } else if (resultis.contains("MUs")) {
                                    String[] separated = resultis.split("MUs");
                                    resultis = separated[0].toUpperCase();
                                } else if (resultis.contains("MU")) {
                                    String[] separated = resultis.split("MU");
                                    resultis = separated[0].toUpperCase();
                                } else if (resultis.contains("XM")) {
                                    String[] separated = resultis.split("XM");
                                    resultis = separated[0].toUpperCase();
                                }


                                if (resultis.contains("x")) {
                                    String[] separated2 = resultis.split("x");
                                    resultis = separated2[0].toUpperCase();
                                    String[] separated3 = resultis.split(" 0");
                                    resultis = separated3[0].toUpperCase();
                                    String[] separated4 = resultis.split(" O");
                                    resultis = separated4[0].toUpperCase();
                                }
                            }

                            if (resultis.contains("x")) {
                                String[] separated3 = resultis.split("x");
                                resultis = separated3[0].toUpperCase();

                                String[] separated5 = resultis.split(" 0");
                                resultis = separated5[0].toUpperCase();
                                String[] separated6 = resultis.split(" O");
                                resultis = separated6[0].toUpperCase();

                            }


                            if (resultis.contains("AP") || resultis.contains("PA")) {
                                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText("debug_text", "RESULT: " + resultis + " " + debugttxt);
                                clipboard.setPrimaryClip(clip);

                                String toconv2 = resultis.replaceAll(",", "").replaceAll("\\.", "").replaceAll("o", "0");


                                // resultis = formatnumber;

                                if (toconv2.contains("Lifetime")) {
                                    String[] separated3 = toconv2.split("Lifetime");
                                    resultis = separated3[0].toUpperCase();
                                } else if (toconv2.length() >= 17) {
                                    if (lifetime && (!toconv2.contains("AP /") || !toconv2.contains("AP/") || !toconv2.contains("PA /") || !toconv2.contains("PA/") || !toconv2.contains("/"))) {
                                        String replacedallspace = toconv2.replaceAll("\\s+", "");
                                        resultis = replacedallspace.replaceAll("[^\\d]", "");
                                    } else {
                                        if (toconv2.contains("AP 1") || toconv2.contains("AP /") || toconv2.contains("AP/") || toconv2.contains("/")) {
                                            if (toconv2.contains(" AP 1")) {
                                                String[] separated3 = toconv2.split(" AP 1");
                                                resultis = separated3[0].toUpperCase();
                                            } else if (toconv2.contains(" AP /")) {
                                                String[] separated3 = toconv2.split(" AP /");
                                                resultis = separated3[0].toUpperCase();
                                            } else if (toconv2.contains(" AP/")) {
                                                String[] separated3 = toconv2.split(" AP/");
                                                resultis = separated3[0].toUpperCase();
                                            } else if (toconv2.contains("/")) {
                                                String[] separated3 = toconv2.split("/");
                                                resultis = separated3[0].toUpperCase();
                                            } else {
                                                String[] separated3 = toconv2.split("AP");
                                                resultis = separated3[0].toUpperCase();
                                            }
                                        } else {
                                            if (toconv2.contains("PA 1") || toconv2.contains("PA /") || toconv2.contains("PA/") || toconv2.contains("/")) {
                                                if (toconv2.contains(" PA 1")) {
                                                    String[] separated3 = toconv2.split(" PA 1");
                                                    resultis = separated3[0].toUpperCase();
                                                } else if (toconv2.contains(" PA /")) {
                                                    String[] separated3 = toconv2.split(" PA /");
                                                    resultis = separated3[0].toUpperCase();
                                                } else if (toconv2.contains(" PA/")) {
                                                    String[] separated3 = toconv2.split(" PA/");
                                                    resultis = separated3[0].toUpperCase();
                                                } else if (toconv2.contains("/")) {
                                                    String[] separated3 = toconv2.split("/");
                                                    resultis = separated3[0].toUpperCase();
                                                } else {
                                                    String[] separated3 = toconv2.split("PA");
                                                    resultis = separated3[0].toUpperCase();
                                                }
                                            } else {
                                                if (toconv2.contains("PA")) {
                                                    String[] separated3 = toconv2.split(" PA");
                                                    resultis = separated3[0].toUpperCase();
                                                } else {
                                                    String[] separated3 = toconv2.split(" AP");
                                                    resultis = separated3[0].toUpperCase();
                                                }
                                            }
                                        }
                                        // resultis = separated3[0].toUpperCase();
                                    }

                                }
                                // resultis.contains("Lebenszeit-AP") || resultis.contains("AP vital") || resultis.length() >= 17) {
                                else {
                                    if (toconv2.contains("AP 1") || toconv2.contains("AP /") || toconv2.contains("AP/") || toconv2.contains("/")) {
                                        if (toconv2.contains(" AP 1")) {
                                            String[] separated3 = toconv2.split(" AP 1");
                                            resultis = separated3[0].toUpperCase();
                                        } else if (toconv2.contains(" AP /")) {
                                            String[] separated3 = toconv2.split(" AP /");
                                            resultis = separated3[0].toUpperCase();
                                        } else if (toconv2.contains(" AP/")) {
                                            String[] separated3 = toconv2.split(" AP/");
                                            resultis = separated3[0].toUpperCase();
                                        } else if (toconv2.contains("/")) {
                                            String[] separated3 = toconv2.split("/");
                                            resultis = separated3[0].toUpperCase();
                                        } else {
                                            String[] separated3 = toconv2.split("AP");
                                            resultis = separated3[0].toUpperCase();
                                        }
                                    } else {

                                        if (toconv2.contains("PA 1") || toconv2.contains("PA /") || toconv2.contains("PA/") || toconv2.contains("/")) {
                                            if (toconv2.contains(" PA 1")) {
                                                String[] separated3 = toconv2.split(" PA 1");
                                                resultis = separated3[0].toUpperCase();
                                            } else if (toconv2.contains(" PA /")) {
                                                String[] separated3 = toconv2.split(" PA /");
                                                resultis = separated3[0].toUpperCase();
                                            } else if (toconv2.contains(" PA/")) {
                                                String[] separated3 = toconv2.split(" PA/");
                                                resultis = separated3[0].toUpperCase();
                                            } else if (toconv2.contains("/")) {
                                                String[] separated3 = toconv2.split("/");
                                                resultis = separated3[0].toUpperCase();
                                            } else {
                                                String[] separated3 = toconv2.split("PA");
                                                resultis = separated3[0].toUpperCase();
                                            }
                                        } else {
                                            if (toconv2.contains("PA")) {
                                                String[] separated3 = toconv2.split(" PA");
                                                resultis = separated3[0].toUpperCase();
                                            } else {
                                                String[] separated3 = toconv2.split(" AP");
                                                resultis = separated3[0].toUpperCase();
                                            }
                                        }
                                    }
                                }


                            }

                            Log.i("WOHOOO2 ---> ", resultis + " missionID: " + onactionid + " art: " + missionart);

                            resultis = resultis.replaceAll("\\s+", "");

                            Log.i("WOHOOO3 ---> ", resultis + " missionID: " + onactionid + " art: " + missionart);

                            if (resultis.contains("40000000")) {
                                if (resultis.contains("140000000")) {
                                    String[] separated3 = resultis.split("140000000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("40000000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("24000000")) {
                                if (resultis.contains("124000000")) {
                                    String[] separated3 = resultis.split("124000000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("24000000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("16000000")) {
                                if (resultis.contains("116000000")) {
                                    String[] separated3 = resultis.split("116000000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("16000000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("17000000")) {
                                if (resultis.contains("117000000")) {
                                    String[] separated3 = resultis.split("117000000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("17000000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("12000000")) {
                                if (resultis.contains("112000000")) {
                                    String[] separated3 = resultis.split("112000000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("12000000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("8400000")) {
                                if (resultis.contains("18400000")) {
                                    String[] separated3 = resultis.split("18400000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("8400000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("6000000")) {
                                if (resultis.contains("16000000")) {
                                    String[] separated3 = resultis.split("16000000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("6000000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("4000000")) {
                                if (resultis.contains("14000000")) {
                                    String[] separated3 = resultis.split("14000000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("4000000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("2400000")) {
                                if (resultis.contains("12400000")) {
                                    String[] separated3 = resultis.split("12400000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("2400000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("600000")) {
                                if (resultis.contains("1600000")) {
                                    String[] separated3 = resultis.split("1600000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("600000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            } else if (resultis.contains("300000")) {
                                if (resultis.contains("1300000")) {
                                    String[] separated3 = resultis.split("1300000");
                                    resultis = separated3[0].toUpperCase();
                                } else {
                                    String[] separated3 = resultis.split("300000");
                                    resultis = separated3[0].toUpperCase();
                                }
                            }


                            String toconvert;

                                    /* if (resultis.contains("AP")) {

                                    //    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                    //    ClipData clip = ClipData.newPlainText("debug_text", "RESULT: " + resultis);
                                    //    clipboard.setPrimaryClip(clip);

                                        if (resultis.contains("Lifetime")) {
                                            String[] separated3 = resultis.split("Lifetime");

                                            resultis = separated3[0].toUpperCase();
                                        } else if (resultis.length() >= 17) {
                                            String nnresult = resultis.replaceAll(",", "");

                                            resultis = resultis.replaceAll("\\s", "");

                                            String[] separated3 = resultis.split("AP");
                                            resultis = separated3[0].toUpperCase();
                                        }
                                        // resultis.contains("Lebenszeit-AP") || resultis.contains("AP vital") || resultis.length() >= 17) {
                                        else {
                                            String[] separated3 = resultis.split("AP");
                                            resultis = separated3[0].toUpperCase();
                                        }
                                    }

                                    String toconvert;

                                    */

                            String lztg = resultis;
                            resultis = resultis.replaceAll("\\s", "");
                            String[] separatedk = resultis.split(" ");
                            resultis = separatedk[0];
                            toconvert = resultis.replaceAll("\\s+", "").replaceAll("Q", "0").replaceAll("G", "5").replaceAll("s", "5").replaceAll("S", "5").replaceAll("o", "0").replaceAll("L", "1").replaceAll("I", "1").replaceAll("O", "0").replaceAll("l", "1").replaceAll("i", "1").replaceAll("B", "3").replaceAll("i", "1").replaceAll("\\|", "1").replaceAll("\\,", "").replaceAll("\\.", "");
                                  /*  toconvert = toconvert.replaceAll("l", "1");
                                    toconvert = toconvert.replaceAll("i", "1");


                                    toconvert = toconvert.replaceAll(",", "");
                                    toconvert = toconvert.replaceAll("\\.", "");
                                    toconvert = toconvert.replaceAll("\\s+", "");
                                    toconvert = toconvert.replaceAll("[^\\d.]", "");*/
                            try {
                                theresult = Integer.parseInt(toconvert);

                            } catch (NumberFormatException nfe) {
                                if (pd != null)
                                    if (pd.isShowing())
                                        if (!isFinishing()) {
                                            pd.dismiss();
                                            // pd = null;
                                        }

                                System.out.println("Could not parse " + nfe);
                            }

                            // StyleableToast.makeText(getApplicationContext(), getString(R.string.value_string) + ": " + theresult, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            final boolean finalIslagenumber = islagenumber[0];
                            final String finalResultis = lztg;
                            final int finalTheresult = theresult;
                            mDatabase.child(user.getUid()).child("badges").child(onactionid).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot snapshotbadge) {
                                    String badgestatus;
                                    if (snapshotbadge.child("badge").exists()) {
                                        badgestatus = snapshotbadge.child("badge").getValue().toString();
                                    } else {
                                        badgestatus = "NULL";

                                    }

                                    if (missionart.equalsIgnoreCase("pvp")) {
                                        if (pd != null)
                                            if (pd.isShowing())
                                                if (!isFinishing()) {
                                                    pd.dismiss();
                                                    //  pd = null;
                                                }

                                        Intent ipvpdetails = new Intent(getBaseContext(), PvPDetails.class);
                                        ipvpdetails.putExtra("finalTheresult", finalTheresult);
                                        ipvpdetails.putExtra("onactionid", onactionid);
                                        ipvpdetails.putExtra("missionart", missionart);
                                        ipvpdetails.putExtra("finalResultis", finalResultis);
                                        ipvpdetails.putExtra("finalIslagenumber", finalIslagenumber);
                                        ipvpdetails.putExtra("badgestatus", badgestatus);

                                        startActivity(ipvpdetails);
                                        Log.i("ONACTION ID PVP ", " ->" + onactionid);
                                    } else {
                                        if (pd != null)
                                            if (pd.isShowing())
                                                if (!isFinishing()) {
                                                    pd.dismiss();
                                                    //  pd = null;
                                                }
                                        writetomission(finalTheresult, onactionid, missionart, finalResultis, finalIslagenumber, badgestatus, false);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    if (pd != null)
                                        if (pd.isShowing())
                                            if (!isFinishing()) {
                                                pd.dismiss();
                                                //  pd = null;
                                            }
                                    //  StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                }
                            });


                            // agentname_txt.setError("Es konnte kein Agent name gefunden werden.");
                        } else {
                            if (correctbage[0] && correctagent[0]) {
                                if (pd != null)
                                    if (pd.isShowing())
                                        if (!isFinishing()) {
                                            pd.dismiss();
                                            //  pd = null;
                                        }
                                infoToast(getString(R.string.error_calibration_proof_main));
                                Bundle paramsl = new Bundle();
                                paramsl.putString("agentname", AgentData.getagentname());
                                paramsl.putString("correct_bade", " " + correctbage[0]);
                                paramsl.putString("correct_agent", " " + correctagent[0]);
                                paramsl.putString("found", " " + found);
                                paramsl.putString("lineint", " " + debugt);
                                paramsl.putString("result", " " + resultis);
                                paramsl.putString("lines", " " + listdebug.toString());
                                mFirebaseAnalytics.logEvent("error_readvalue", paramsl);


                                String text = listdebug.toString();
                                //  ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                //  ClipData clip = ClipData.newPlainText("debug_text", text);
                                //  clipboard.setPrimaryClip(clip);

                            } else if (!correctbage[0] && correctagent[0]) {


                                //  ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                //  ClipData clip = ClipData.newPlainText("debug_text", text);
                                //  clipboard.setPrimaryClip(clip);
                                if (pd != null)
                                    if (pd.isShowing())
                                        if (!isFinishing()) {
                                            pd.dismiss();
                                            //  pd = null;
                                        }
                                infoToast(getString(R.string.error_wrong_badge) + " " + needbadge);

                            } else if (correctbage[0] && !correctagent[0]) {
                                if (pd != null)
                                    if (pd.isShowing())
                                        if (!isFinishing()) {
                                            pd.dismiss();
                                            //  pd = null;
                                        }
                                infoToast(getString(R.string.error_wrong_agent_badge) + " " + AgentData.getagentname());
                                // ACTION TRICKSTER
                                //  ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                //  ClipData clip = ClipData.newPlainText("debug_text", text);
                                //  clipboard.setPrimaryClip(clip);


                                mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshot) {
                                        int trickstercount = 0;

                                        if (snapshot.hasChild("tricksterstatus")) {

                                            trickstercount = Integer.parseInt(snapshot.child("tricksterstatus").child("count").getValue().toString());


                                            final Map<String, Object> tricksterUpdates = new HashMap<>();
                                            // IF hasmissions
                                            tricksterUpdates.put("/count", trickstercount + 1);

                                            Bundle params = new Bundle();
                                            params.putString("agent", AgentData.getagentname());
                                            mFirebaseAnalytics.logEvent("trickster", params);


                                            mDatabase.child(user.getUid()).child("tricksterstatus").updateChildren(tricksterUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        //finish();

                                                    } else if (task.isCanceled()) {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                                                    } else {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");


                                                    //signOut();
                                                }
                                            });
                                        } else {
                                            final Map<String, Object> tricksterUpdates = new HashMap<>();
                                            // IF hasmissions
                                            tricksterUpdates.put("/count", 1);


                                            mDatabase.child(user.getUid()).child("tricksterstatus").updateChildren(tricksterUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        //finish();
                                                        if (pd != null)
                                                            if (pd.isShowing())
                                                                if (!isFinishing()) {
                                                                    pd.dismiss();
                                                                    //  pd = null;
                                                                }
                                                    } else if (task.isCanceled()) {
                                                        if (pd != null)
                                                            if (pd.isShowing())
                                                                if (!isFinishing()) {
                                                                    pd.dismiss();
                                                                    //  pd = null;
                                                                }
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                                                    } else {
                                                        if (pd != null)
                                                            if (pd.isShowing())
                                                                if (!isFinishing()) {
                                                                    pd.dismiss();
                                                                    //  pd = null;
                                                                }
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");

                                                    if (pd != null)
                                                        if (pd.isShowing())
                                                            if (!isFinishing()) {
                                                                pd.dismiss();
                                                                //  pd = null;
                                                            }
                                                    //signOut();
                                                }
                                            });
                                        }


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        if (pd != null)
                                            if (pd.isShowing())
                                                if (!isFinishing()) {
                                                    pd.dismiss();
                                                    //  pd = null;
                                                }
                                        //  StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });

                            } else {
                                if (pd != null)
                                    if (pd.isShowing())
                                        if (!isFinishing()) {
                                            pd.dismiss();
                                            //  pd = null;
                                        }
                                infoToast(getString(R.string.error_badge_or_agent) + " " + needbadge + "  " + AgentData.getagentname());
                                mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshot) {
                                        int trickstercount = 0;

                                        if (snapshot.hasChild("tricksterstatus")) {

                                            trickstercount = Integer.parseInt(snapshot.child("tricksterstatus").child("count").getValue().toString());


                                            final Map<String, Object> tricksterUpdates = new HashMap<>();
                                            // IF hasmissions
                                            tricksterUpdates.put("/count", trickstercount + 1);
                                            Bundle params = new Bundle();
                                            params.putString("agent", AgentData.getagentname());
                                            mFirebaseAnalytics.logEvent("trickster", params);

                                            mDatabase.child(user.getUid()).child("tricksterstatus").updateChildren(tricksterUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        //finish();

                                                    } else if (task.isCanceled()) {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                                                    } else {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");


                                                    //signOut();
                                                }
                                            });
                                        } else {
                                            final Map<String, Object> tricksterUpdates = new HashMap<>();
                                            // IF hasmissions
                                            tricksterUpdates.put("/count", 1);


                                            mDatabase.child(user.getUid()).child("tricksterstatus").updateChildren(tricksterUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        //finish();

                                                    } else if (task.isCanceled()) {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 3001");

                                                    } else {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 3002");

                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 3003");


                                                    //signOut();
                                                }
                                            });
                                        }


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        //  StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });

                            }
                        }
                        // [END get_text]
                        // [END_EXCLUDE]
                    }
                })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                if (pd != null)
                                    if (pd.isShowing())
                                        if (!isFinishing()) {
                                            pd.dismiss();
                                            //  pd = null;
                                        }
                                // Task failed with an exception
                                // ...
                            }
                        });
        // [END run_detector]
    }

    private void recognizeTextClip(String valueof, final String onactionid, final String missionart, final String badgeneed, String lifetimeclip, String timespanclip, String agentclip, final Boolean moraction, final String statsis) {
        boolean correctbage;
        boolean apbadge;
        boolean correctagent;
        boolean islagenumber = false;
        boolean found;
        boolean lifetime;
        String resultis = "nothing";


        final String needbadge;
        if (missionart.equalsIgnoreCase("month")) {
            needbadge = monthbadgetitle;
        } else if (missionart.equalsIgnoreCase("week")) {
            needbadge = weekbadgetitle;
        } else if (missionart.equalsIgnoreCase("earth")) {
            needbadge = earthbadgetitle;
        } else if (missionart.equalsIgnoreCase("pvp")) {
            needbadge = badgeneed;
        } else if (missionart.equalsIgnoreCase("mini")) {
            needbadge = minibadgetitle;
        } else if (missionart.equalsIgnoreCase("aptest")) {
            needbadge = badgeneed;
        } else {
            needbadge = monthbadgetitle;
        }


        if (needbadge.equalsIgnoreCase("Recharger")) {
            islagenumber = true;
        }
        if (needbadge.equalsIgnoreCase("nator")) {
            islagenumber = true;
        }

        if (agentclip.contains(agentname.getText().toString())) {
            Log.i(agentname + "--->", "yes");
            correctagent = true;
        } else {
            correctagent = false;
        }

        if (needbadge.equalsIgnoreCase("AP")) {
            found = true;
            correctbage = true;
            resultis = lifetimeclip;
        } else {
            Log.i("PROOF TIMESPAN CLIP -> ", timespanclip);
            Log.i("PROOF TIMESPAN LIST -> ", AgentData.alltimelist.toString());
            if (AgentData.alltimelist.toString().contains(timespanclip)) {
                correctbage = true;
                found = true;
                resultis = valueof;
            } else {
                if (statsis != null) {
                    if (statsis.equalsIgnoreCase("now")) {
                        if (AgentData.nowlist.toString().contains(timespanclip)) {
                            correctbage = true;
                            found = true;
                            resultis = valueof;
                        } else {
                            correctbage = false;
                            found = true;
                        }
                    } else if (statsis.equalsIgnoreCase("week")) {
                        if (AgentData.weeklist.toString().contains(timespanclip)) {
                            correctbage = true;
                            found = true;
                            resultis = valueof;
                        } else {
                            correctbage = false;
                            found = true;
                        }
                    } else if (statsis.equalsIgnoreCase("month")) {
                        if (AgentData.monthlist.toString().contains(timespanclip)) {
                            correctbage = true;
                            found = true;
                            resultis = valueof;
                        } else {
                            correctbage = false;
                            found = true;
                        }
                    } else {
                        correctbage = false;
                        found = true;
                    }
                } else {

                    correctbage = false;
                    found = true;
                }
            }
        }


        if (found && correctbage && correctagent) {
            Log.i("WOHOOO ---> ", resultis + " missionID: " + onactionid + " art: " + missionart);
            int theresult = 0;
            String toconvert;
            String lztg = resultis;

            try {
                theresult = Integer.parseInt(resultis);

            } catch (NumberFormatException nfe) {


                System.out.println("Could not parse " + nfe);
            }

            final boolean finalIslagenumber = islagenumber;
            final String finalResultis = lztg;
            final int finalTheresult = theresult;

            mDatabase.child(user.getUid()).child("badges").child(onactionid).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshotbadge) {
                    String badgestatus;
                    if (snapshotbadge.child("badge").exists()) {
                        badgestatus = snapshotbadge.child("badge").getValue().toString();
                    } else {
                        badgestatus = "NULL";

                    }

                    if (missionart.equalsIgnoreCase("pvp")) {


                        Intent ipvpdetails = new Intent(getBaseContext(), PvPDetails.class);
                        ipvpdetails.putExtra("finalTheresult", finalTheresult);
                        ipvpdetails.putExtra("onactionid", onactionid);
                        ipvpdetails.putExtra("missionart", missionart);
                        ipvpdetails.putExtra("finalResultis", finalResultis);
                        ipvpdetails.putExtra("finalIslagenumber", finalIslagenumber);
                        ipvpdetails.putExtra("badgestatus", badgestatus);
                        startActivity(ipvpdetails);
                        Log.i("ONACTION ID PVP ", " ->" + onactionid);
                    } else {

                        writetomission(finalTheresult, onactionid, missionart, finalResultis, finalIslagenumber, badgestatus, moraction);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                    //  StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                }
            });
        } else {
            if (correctbage && correctagent) {

                infoToast(getString(R.string.error_calibration_proof_main));
                Bundle paramsl = new Bundle();
                paramsl.putString("agentname", AgentData.getagentname());
                paramsl.putString("correct_bade", " " + correctbage);
                paramsl.putString("correct_agent", " " + correctagent);
                paramsl.putString("found", " " + found);
                paramsl.putString("lineint", " " + "fromclip");
                paramsl.putString("result", " " + resultis);
                paramsl.putString("lines", " " + listdebug.toString());
                mFirebaseAnalytics.logEvent("error_readvalue", paramsl);


                String text = listdebug.toString();
                //  ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                //  ClipData clip = ClipData.newPlainText("debug_text", text);
                //  clipboard.setPrimaryClip(clip);

            } else if (!correctbage && correctagent) {


                //  ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                //  ClipData clip = ClipData.newPlainText("debug_text", text);
                //  clipboard.setPrimaryClip(clip);

                Thread thread = new Thread() {
                    public void run() {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (statsis != null) {
                                    StyleableToast.makeText(AODMain.this, getString(R.string.error_wrong_badge) + " " + statsis + " TAB " + needbadge, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                } else {
                                    StyleableToast.makeText(AODMain.this, getString(R.string.error_wrong_badge) + " ALL-TIME TAB " + needbadge, Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                }
                            }
                        });
                    }
                };
                thread.start();


                // infoToast(getString(R.string.error_wrong_badge) + " " + needbadge);

            } else if (correctbage && !correctagent) {


                Thread thread = new Thread() {
                    public void run() {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                StyleableToast.makeText(AODMain.this, getString(R.string.error_wrong_agent_badge) + " " + AgentData.getagentname(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }
                        });
                    }
                };
                thread.start();

                //  infoToast(getString(R.string.error_wrong_agent_badge) + " " + AgentData.getagentname());
                // ACTION TRICKSTER
                //  ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                //  ClipData clip = ClipData.newPlainText("debug_text", text);
                //  clipboard.setPrimaryClip(clip);


            } else {

                Thread thread = new Thread() {
                    public void run() {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                StyleableToast.makeText(AODMain.this, getString(R.string.error_badge_or_agent) + " " + needbadge + "  " + AgentData.getagentname(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }
                        });
                    }
                };
                thread.start();


                //infoToast(getString(R.string.error_badge_or_agent) + " " + needbadge + "  " + AgentData.getagentname());


            }
        }


        // [END run_detector]
    }

    private void writetomission(final int theresult, final String onactionid, final String missionart, final String iffails, final Boolean islagenumber, final String badgestatus, final Boolean moreaction) {
        final Map<String, Object> childUpdates = new HashMap<>();
        final Map<String, Object> childUpdatesCommunity = new HashMap<>();
        final Map<String, Object> childMissionAgent = new HashMap<>();

        final Map<String, Object> childMissionBadgeAgent = new HashMap<>();

        final boolean setbadges = false;

        final String cmissionmonthweek;
        final boolean onmonth, onearth, onmini;
        if (missionart.equalsIgnoreCase("month")) {
            cmissionmonthweek = themonthx;
            onmonth = true;
            onearth = false;
            onmini = false;
        } else if (missionart.equalsIgnoreCase("week")) {
            cmissionmonthweek = theweekx;
            onmonth = false;
            onearth = false;
            onmini = false;
        } else if (missionart.equalsIgnoreCase("earth")) {
            cmissionmonthweek = onactionid;
            onmonth = false;
            onearth = true;
            onmini = false;
        } else if (missionart.equalsIgnoreCase("mini")) {
            cmissionmonthweek = onactionid;
            onmonth = false;
            onearth = false;
            onmini = true;
        } else if (missionart.equalsIgnoreCase("aptest")) {
            cmissionmonthweek = onactionid;
            onmonth = false;
            onearth = false;
            onmini = false;
        } else {
            onmonth = true;
            onearth = false;
            onmini = false;
            cmissionmonthweek = themonthx;
        }


        // IF hasmissions
        Log.i("ACTION --> ID", " " + onactionid);

        if (missionart.equalsIgnoreCase("aptest")) {

            StyleableToast.makeText(getApplicationContext(), "TEST --> AP: " + theresult, Toast.LENGTH_LONG, R.style.defaulttoast).show();
            //StyleableToast.makeText(getApplicationContext(), "Vielen Dank für den Test", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
        } else {

            mDatabaserunnung.child(theyearx).child(missionart).child(onactionid).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                int currentis;
                int updatetoast;

                @Override
                public void onDataChange(final DataSnapshot snapshotmissionvalues) {
                    childUpdates.put("/timestamp", System.currentTimeMillis() / 1000);
                    childMissionAgent.put("/timestamp", System.currentTimeMillis() / 1000);
                    final boolean initupdate;
                    new AODConfig(getApplicationContext());
                    if (onmini) {
                        initupdate = true;
                        if (Integer.parseInt(snapshotmissionvalues.child("uploads").getValue().toString()) <= 0) {
                            childUpdates.put("/startvalue", theresult);
                            childUpdates.put("/lastvalue", theresult);
                            childUpdates.put("/uploads", 1);
                            childUpdates.put("/currentvalue", theresult);
                            childMissionAgent.put("/value", theresult);
                            childMissionAgent.put("/startvalue", theresult);

                        } else {
                            if (Integer.parseInt(snapshotmissionvalues.child("currentvalue").getValue().toString()) > theresult) {

                            } else {

                                currentis = (theresult - Integer.parseInt(snapshotmissionvalues.child("currentvalue").getValue().toString()));

                                if (Integer.parseInt(snapshotmissionvalues.child("currentvalue").getValue().toString()) > 0) {
                                    updatetoast = theresult - Integer.parseInt(snapshotmissionvalues.child("currentvalue").getValue().toString());
                                } else {
                                    updatetoast = theresult;
                                }
                                childUpdates.put("/lastvalue", theresult);
                                childUpdates.put("/currentvalue", theresult);
                                int updatesc = Integer.parseInt(snapshotmissionvalues.child("uploads").getValue().toString());

                                childUpdates.put("/uploads", updatesc + 1);
                                childMissionAgent.put("/value", theresult);
                                childMissionAgent.put("/startvalue", Integer.parseInt(snapshotmissionvalues.child("startvalue").getValue().toString()));
                            }
                        }
                    } else {
                        if (Integer.parseInt(snapshotmissionvalues.child("startvalue").getValue().toString()) <= 0) {
                            childUpdates.put("/startvalue", theresult);
                            childUpdates.put("/lastvalue", theresult);
                            childMissionAgent.put("/startvalue", theresult);
                            childMissionAgent.put("/value", theresult);

                            initupdate = true;
                        } else {

                            currentis = (theresult - Integer.parseInt(snapshotmissionvalues.child("startvalue").getValue().toString()));

                            if (Integer.parseInt(snapshotmissionvalues.child("currentvalue").getValue().toString()) > 0) {
                                updatetoast = currentis - Integer.parseInt(snapshotmissionvalues.child("currentvalue").getValue().toString());
                            } else {
                                updatetoast = currentis;
                            }
                            initupdate = false;
                            childUpdates.put("/lastvalue", theresult);
                            childUpdates.put("/currentvalue", currentis);
                            childMissionAgent.put("/value", theresult);
                            childMissionAgent.put("/startvalue", Integer.parseInt(snapshotmissionvalues.child("startvalue").getValue().toString()));


                        }
                    }

                    // UPDATE WERT > start und last update
                    if (theresult > Integer.parseInt(snapshotmissionvalues.child("startvalue").getValue().toString()) && theresult > Integer.parseInt(snapshotmissionvalues.child("lastvalue").getValue().toString())) {
                        int factor;
                        if (islagenumber) {
                            factor = 50;
                        } else {
                            factor = 2;
                        }

                        Log.i("DEBUG -->", "" + theresult + " " + (Integer.parseInt(snapshotmissionvalues.child("lastvalue").getValue().toString()) * factor) + " " + factor);
                       /* if (theresult > (Long.parseLong(snapshotmissionvalues.child("lastvalue").getValue().toString()) * factor) && !initupdate) {
                            StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.error_tohigh) + ": " + theresult + ".", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(System.currentTimeMillis()));
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "value too high");
                            bundle.putString(FirebaseAnalytics.Param.CONTENT, "" + iffails + "," + theresult + ", " + AgentData.getagentname());
                            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "scanfail");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                        } else {
*/
                        updateHolder.setVisibility(View.VISIBLE);
                        tickerView.setCharacterLists(TickerUtils.provideNumberList());

                        tickerView.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                        tickerView.setAnimationDuration(1500);
                        tickerView.setGravity(Gravity.CENTER_VERTICAL);

                        tickerView.setAnimationInterpolator(new OvershootInterpolator());
                        tickerView.setText("+ " + updatetoast, true);

                        if (onmonth) {

                            if (currentis < themonthgoal) {
                                childMissionBadgeAgent.put("/badge", null);
                                childUpdates.put("/finished", false);


                                Bundle params = new Bundle();
                                params.putString("level", "GOLD");
                                params.putString("missionid", onactionid);
                                params.putString("art", "start");
                                params.putString("type", "month");
                                mFirebaseAnalytics.logEvent("missionstartart", params);

                            } else if (currentis >= themonthgoal && currentis < themonthgoalplatin && currentis < themonthgoalblack && !badgestatus.equalsIgnoreCase("GOLD")) {
                                // GOLD BADGE ERREICHT
                                childMissionBadgeAgent.put("/badge", "GOLD");


                                Bundle params = new Bundle();
                                params.putString("level", "GOLD");
                                params.putString("missionid", onactionid);
                                params.putString("art", "finished");
                                params.putString("type", "month");
                                params.putInt("value", currentis);

                                mFirebaseAnalytics.logEvent("missionend", params);

                                startKonfetti("GOLD", missionart, weekbadgedrawablegold);


                                Bundle params2 = new Bundle();
                                params2.putString("level", "PLATIN");
                                params2.putString("missionid", onactionid);
                                params2.putString("art", "start");
                                params2.putString("type", "month");
                                mFirebaseAnalytics.logEvent("missionstartart", params2);


                            } else if (currentis >= themonthgoalplatin && currentis < themonthgoalblack && !badgestatus.equalsIgnoreCase("PLATIN")) {
                                // PLATIN BADGE ERREICHT
                                childMissionBadgeAgent.put("/badge", "PLATIN");


                                Bundle params = new Bundle();
                                params.putString("level", "PLATIN");
                                params.putString("missionid", onactionid);
                                params.putString("art", "finished");
                                params.putString("type", "month");
                                params.putInt("value", currentis);

                                mFirebaseAnalytics.logEvent("missionstartend", params);

                                startKonfetti("PLATIN", missionart, weekbadgedrawableplatin);
                                Bundle params2 = new Bundle();
                                params2.putString("level", "ONYX");
                                params2.putString("missionid", onactionid);
                                params2.putString("art", "start");
                                params2.putString("type", "month");
                                mFirebaseAnalytics.logEvent("missionstartart", params2);


                            } else if (currentis >= themonthgoalblack && !badgestatus.equalsIgnoreCase("ONYX")) {
                                childMissionBadgeAgent.put("/badge", "ONYX");

                                Bundle params = new Bundle();
                                params.putString("level", "ONYX");
                                params.putString("missionid", onactionid);
                                params.putString("art", "finished");
                                params.putString("type", "month");
                                params.putInt("value", currentis);


                                mFirebaseAnalytics.logEvent("missionstartend", params);
                                startKonfetti("ONYX", missionart, weekbadgedrawable);
                                // BLACK BADGE ERREICHT

                            }
                        } else {

                            if (onearth) {
                                if (currentis < theearthgoal) {
                                    childMissionBadgeAgent.put("/badge", null);
                                    childUpdates.put("/finished", false);
                                    Bundle params = new Bundle();
                                    params.putString("level", "GOLD");
                                    params.putString("missionid", onactionid);
                                    params.putString("art", "start");
                                    params.putString("type", "earth");
                                    mFirebaseAnalytics.logEvent("missionstartart", params);
                                } else if (currentis >= theearthgoal && currentis < theearthgoalplatin && currentis < theearthgoalblack && !badgestatus.equalsIgnoreCase("GOLD")) {
                                    // GOLD BADGE ERREICHT
                                    childMissionBadgeAgent.put("/badge", "GOLD");

                                    Bundle params = new Bundle();
                                    params.putString("level", "GOLD");
                                    params.putString("missionid", onactionid);
                                    params.putString("art", "finished");
                                    params.putString("type", "earth");
                                    params.putInt("value", currentis);

                                    mFirebaseAnalytics.logEvent("missionend", params);
                                    startKonfetti("GOLD", missionart, earthbadgedrawablegold);
                                    Bundle params2 = new Bundle();
                                    params2.putString("level", "PLATIN");
                                    params2.putString("missionid", onactionid);
                                    params2.putString("art", "start");
                                    params2.putString("type", "earth");
                                    mFirebaseAnalytics.logEvent("missionstartart", params2);

                                } else if (currentis >= theearthgoalplatin && currentis < theearthgoalblack && !badgestatus.equalsIgnoreCase("PLATIN")) {
                                    // PLATIN BADGE ERREICHT
                                    childMissionBadgeAgent.put("/badge", "PLATIN");
                                    Bundle params = new Bundle();
                                    params.putString("level", "PLATIN");
                                    params.putString("missionid", onactionid);
                                    params.putString("art", "finished");
                                    params.putString("type", "earth");
                                    params.putInt("value", currentis);
                                    mFirebaseAnalytics.logEvent("missionend", params);
                                    startKonfetti("PLATIN", missionart, earthbadgedrawableplatin);

                                    Bundle params2 = new Bundle();
                                    params2.putString("level", "ONYX");
                                    params2.putString("missionid", onactionid);
                                    params2.putString("art", "start");
                                    params2.putString("type", "earth");
                                    mFirebaseAnalytics.logEvent("missionstartart", params2);

                                } else if (currentis >= theearthgoalblack && !badgestatus.equalsIgnoreCase("ONYX")) {
                                    childMissionBadgeAgent.put("/badge", "ONYX");

                                    Bundle params = new Bundle();
                                    params.putString("level", "ONYX");
                                    params.putString("missionid", onactionid);
                                    params.putString("art", "finished");
                                    params.putString("type", "earth");
                                    params.putInt("value", currentis);
                                    mFirebaseAnalytics.logEvent("missionend", params);
                                    startKonfetti("ONXY", missionart, earthbadgedrawable);
                                    // BLACK BADGE ERREICHT

                                }
                            } else {


                                if (onmini) {

                                } else {
                                    if (currentis < theweekgoal) {
                                        childMissionBadgeAgent.put("/badge", null);
                                        childUpdates.put("/finished", false);
                                        Bundle params = new Bundle();
                                        params.putString("level", "GOLD");
                                        params.putString("missionid", onactionid);
                                        params.putString("art", "start");
                                        params.putString("type", "week");
                                        mFirebaseAnalytics.logEvent("missionstartart", params);
                                    } else if (currentis >= theweekgoal && currentis < theweekgoalplatin && currentis < theweekgoalblack && !badgestatus.equalsIgnoreCase("GOLD")) {
                                        // GOLD BADGE ERREICHT
                                        childMissionBadgeAgent.put("/badge", "GOLD");

                                        Bundle params = new Bundle();
                                        params.putString("level", "GOLD");
                                        params.putString("missionid", onactionid);
                                        params.putString("art", "finished");
                                        params.putString("type", "week");
                                        params.putInt("value", currentis);
                                        mFirebaseAnalytics.logEvent("missionend", params);
                                        startKonfetti("GOLD", missionart, monthbadgedrawablegold);

                                        Bundle params2 = new Bundle();
                                        params2.putString("level", "PLATIN");
                                        params2.putString("missionid", onactionid);
                                        params2.putString("art", "start");
                                        params2.putString("type", "week");
                                        mFirebaseAnalytics.logEvent("missionstartart", params2);

                                    } else if (currentis >= theweekgoalplatin && currentis < theweekgoalblack && !badgestatus.equalsIgnoreCase("PLATIN")) {
                                        // PLATIN BADGE ERREICHT
                                        childMissionBadgeAgent.put("/badge", "PLATIN");
                                        Bundle params = new Bundle();
                                        params.putString("level", "PLATIN");
                                        params.putString("missionid", onactionid);
                                        params.putString("art", "finished");
                                        params.putString("type", "week");
                                        params.putInt("value", currentis);
                                        mFirebaseAnalytics.logEvent("missionend", params);
                                        startKonfetti("PLATIN", missionart, monthbadgedrawableplatin);

                                        Bundle params2 = new Bundle();
                                        params2.putString("level", "ONYX");
                                        params2.putString("missionid", onactionid);
                                        params2.putString("art", "start");
                                        params2.putString("type", "week");
                                        mFirebaseAnalytics.logEvent("missionstartart", params2);


                                    } else if (currentis >= theweekgoalblack && !badgestatus.equalsIgnoreCase("ONYX")) {
                                        childMissionBadgeAgent.put("/badge", "ONYX");

                                        Bundle params = new Bundle();
                                        params.putString("level", "ONYX");
                                        params.putString("missionid", onactionid);
                                        params.putString("art", "finished");
                                        params.putString("type", "week");
                                        params.putInt("value", currentis);
                                        mFirebaseAnalytics.logEvent("missionend", params);
                                        startKonfetti("ONXY", missionart, monthbadgedrawable);
                                        // BLACK BADGE ERREICHT

                                    }
                                }
                            }
                        }
                        // UPDATE COMMUNITY MISSION
                        mDatabasemissions.child(missionart).child(theyearx).child(cmissionmonthweek).child("community").addListenerForSingleValueEvent(new ValueEventListener() {
                            int community_current, community_current_enl, community_current_res, rechenvalue;

                            public void onDataChange(DataSnapshot snapshotcommunityvalues) {


                                if (initupdate) {

                                } else {
                                    community_current = Integer.parseInt(snapshotcommunityvalues.child("current").getValue().toString());
                                    community_current_enl = Integer.parseInt(snapshotcommunityvalues.child("enl").getValue().toString());
                                    community_current_res = Integer.parseInt(snapshotcommunityvalues.child("res").getValue().toString());
                                    if (Integer.parseInt(snapshotmissionvalues.child("lastvalue").getValue().toString()) > 0) {
                                        rechenvalue = theresult - Integer.parseInt(snapshotmissionvalues.child("lastvalue").getValue().toString());
                                        childUpdatesCommunity.put("/current", community_current + rechenvalue);
                                        //childUpdatesCommunity.put("/current", community_current + rechenvalue);
                                        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                                            childUpdatesCommunity.put("/enl", community_current_enl + rechenvalue);
                                        }
                                        if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                                            childUpdatesCommunity.put("/res", community_current_res + rechenvalue);
                                        }
                                    }
                                }


                                mDatabasemissions.child(missionart).child(theyearx).child(cmissionmonthweek).child("community").updateChildren(childUpdatesCommunity).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            // String newKey = mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(cmissionmonthweek).child(onactionid).push().getKey();

                                        } else if (task.isCanceled()) {
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                            errorToast("Error Community Mission", "40001");
                                        } else {
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                            errorToast("Community Mission", "40002");
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                        errorToast("Community Mission", "40003");

                                        //signOut();
                                    }
                                });

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            }
                        });
                        mDatabaserunnung.child(theyearx).child(missionart).child(onactionid).child(user.getUid()).updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    String newKey = mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(cmissionmonthweek).child(onactionid).push().getKey();
                                    mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(cmissionmonthweek).child(onactionid).child(newKey).updateChildren(childMissionAgent).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                //finish();

                                                mDatabase.child(user.getUid()).child("badges").child(onactionid).updateChildren(childMissionBadgeAgent).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            // String newKey = mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(themonthx).child(onactionid).push().getKey();

                                                            new Handler().postDelayed(new Runnable() {

                                                                /*
                                                                 * Showing splash screen with a timer. This will be useful when you
                                                                 * want to show case your app logo / company
                                                                 */

                                                                @Override
                                                                public void run() {
                                                                    // This method will be executed once the timer is over
                                                                    // Start your app main activity

                                                                    updateHolder.setVisibility(View.GONE);
                                                                    //  tickerView.setText("+" +currentis, true);
                                                                    if (onmonth) {
                                                                        Log.i("MORE --> ", "status - " + multimission);
                                                                        if (multimission && weekrunning && moreaction) {
                                                                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                                                            // builder.setMessage("Text not finished...");
                                                                            LayoutInflater inflater = getLayoutInflater();
                                                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                                                            builder.setView(dialog);
                                                                            final TextView input = dialog.findViewById(R.id.caltext);
                                                                            // input.setAllCaps(true);
                                                                            input.setTypeface(type);
                                                                            input.setText("Would you also like to update the Weekly Mission?");
                                                                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                                                            //localTextView1.setTypeface(proto);
                                                                            builder.setPositiveButton("yes!", new DialogInterface.OnClickListener() {

                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {


                                                                                    moreupdateweek(theyearx, themonthx, true);

                                                                                }
                                                                            });


                                                                            builder.setNeutralButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {

                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {
                                                                                    if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                                                                        startexportebl(AgentData.getEbl_token());
                                                                                    }

                                                                                }
                                                                            });


                                                                            builder.setCancelable(true);

                                                                            if (!isFinishing()) {
                                                                                builder.create().show();

                                                                            }

                                                                        } else {
                                                                            initmonth(theyearx, themonthx, true);
                                                                            if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                                                                startexportebl(AgentData.getEbl_token());
                                                                            }
                                                                        }
                                                                    } else if (onearth) {
                                                                        initearth(theyearx, onactionid, true);
                                                                        if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                                                            startexportebl(AgentData.getEbl_token());
                                                                        }
                                                                    } else if (onmini) {
                                                                        initmini(Long.parseLong(onactionid), true);

                                                                    } else {

                                                                        // SimpleDateFormat fday = new SimpleDateFormat("yyyyMMdd_HHmmss");
                                                                        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
                                                                        SimpleDateFormat fyear = new SimpleDateFormat("yyyy");
                                                                        final String themonth = fmonth.format(new Date());
                                                                        final String theyear = fyear.format(new Date());
                                                                        theyearx = theyear;
                                                                        themonthx = themonth;

                                                                        DateFormat format = new SimpleDateFormat("dd");
                                                                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                                                                        Calendar.getInstance(Locale.getDefault()).setTimeZone(TimeZone.getDefault());
                                                                        calendar.setFirstDayOfWeek(Calendar.MONDAY);
                                                                        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                                                                        final String[] days = new String[7];
                                                                        String tempthemonth = fmonth.format(new Date());
                                                                        for (int i = 0; i < 7; i++) {
                                                                            days[i] = format.format(calendar.getTime());
                                                                            calendar.add(Calendar.DAY_OF_MONTH, 1);
                                                                        }
                                                                        if (Integer.parseInt(days[1]) < Integer.parseInt(days[0]) || Integer.parseInt(days[2]) < Integer.parseInt(days[0]) || Integer.parseInt(days[3]) < Integer.parseInt(days[0]) || Integer.parseInt(days[4]) < Integer.parseInt(days[0]) || Integer.parseInt(days[5]) < Integer.parseInt(days[0]) || Integer.parseInt(days[6]) < Integer.parseInt(days[0])) {
                                                                            if (calendar.get(Calendar.DAY_OF_MONTH) <= 7) {
                                                                                tempthemonth = String.valueOf(Integer.parseInt(themonth) - 1);
                                                                            } else {
                                                                                tempthemonth = String.valueOf(Integer.parseInt(themonth) + 1);
                                                                            }
                                                                        } else {
                                                                            tempthemonth = themonth;
                                                                        }
                                                                        String strDate = days[0] + "." + (Integer.parseInt(tempthemonth)) + "." + theyear;
                                                                        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                                                                        Date date = null;
                                                                        try {
                                                                            date = dateFormat.parse(strDate);
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                        Calendar now = Calendar.getInstance(Locale.GERMAN);
                                                                        now.setTimeZone(TimeZone.getDefault());

                                                                        now.setFirstDayOfWeek(Calendar.MONDAY);
                                                                        now.setTime(date);
                                                                        int currentweek = now.get(Calendar.WEEK_OF_YEAR);
                                                                        String cweeksend;
                                                                        if (currentweek <= 9) {
                                                                            currentweek = Integer.parseInt("0" + currentweek);
                                                                            cweeksend = "0" + currentweek;
                                                                        } else {
                                                                            cweeksend = String.valueOf(currentweek);
                                                                        }
                                                                        if (currentweek <= 9) {
                                                                            currentweek = Integer.parseInt("0" + currentweek);
                                                                            cweeksend = "0" + currentweek;
                                                                        } else {
                                                                            cweeksend = String.valueOf(currentweek);
                                                                        }

                                                                        Log.i("CURRENT WEEK FOR", "" + cweeksend);
                                                                        final String thefirstweekday = cweeksend + "" + days[0];
                                                                        initweek(theyear, tempthemonth, thefirstweekday, days, true);
                                                                        if (multimission && monthrunning && moreaction) {

                                                                            AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                                                            // builder.setMessage("Text not finished...");
                                                                            LayoutInflater inflater = getLayoutInflater();
                                                                            View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                                                            builder.setView(dialog);
                                                                            final TextView input = dialog.findViewById(R.id.caltext);
                                                                            // input.setAllCaps(true);
                                                                            input.setTypeface(type);
                                                                            input.setText("Would you also like to update the Monthly Mission?");
                                                                            //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                                                            //localTextView1.setTypeface(proto);
                                                                            builder.setPositiveButton("Yes!", new DialogInterface.OnClickListener() {

                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {


                                                                                    moreupdatemonth(theyear, themonth, thefirstweekday, days, true);

                                                                                }
                                                                            });

                                                                            builder.setNeutralButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {

                                                                                @Override
                                                                                public void onClick(DialogInterface dialog, int which) {


                                                                                    if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                                                                        startexportebl(AgentData.getEbl_token());
                                                                                    }

                                                                                }
                                                                            });


                                                                            builder.setCancelable(true);

                                                                            if (!isFinishing()) {
                                                                                builder.create().show();

                                                                            }

                                                                        } else {

                                                                            initweek(theyear, tempthemonth, thefirstweekday, days, true);
                                                                            if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                                                                startexportebl(AgentData.getEbl_token());
                                                                            }
                                                                            // initweek(theyearx, theweekx, true);
                                                                        }
                                                                    }
                                                                }
                                                            }, 2500);

                                                        } else if (task.isCanceled()) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                                            errorToast("WRITE BADGE", "40001");
                                                        } else {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                                            errorToast("WRITE BADGE", "40002");
                                                        }
                                                    }

                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(Exception e) {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                                        errorToast("WRITE BADGE", "40003");

                                                        //signOut();
                                                    }
                                                });

                                            } else if (task.isCanceled()) {
                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                                errorToast("Error", "10001");
                                            } else {
                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                                errorToast(missionart, "10002");
                                            }
                                        }

                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                            errorToast(missionart, "10003");

                                            //signOut();
                                        }
                                    });
                                } else if (task.isCanceled()) {
                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                    errorToast("Error", "10001");
                                } else {
                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                    errorToast(missionart, "10002");
                                }
                            }

                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                errorToast(missionart, "10003");

                                //signOut();
                            }
                        });

                    } else {

                        if (stattab != null) {
                            if (stattab.equalsIgnoreCase("now")) {
                                StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.error_tosmall) + ". " + theresult + " " + getResources().getString(R.string.error_noimprovement) + ". Or Wrong Tab selected.. Need NOW Tab", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                            }
                            if (stattab.equalsIgnoreCase("week")) {
                                StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.error_tosmall) + ". " + theresult + " " + getResources().getString(R.string.error_noimprovement) + ". Or Wrong Tab selected.. Need WEEK Tab", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                            }
                            if (stattab.equalsIgnoreCase("month")) {
                                StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.error_tosmall) + ". " + theresult + " " + getResources().getString(R.string.error_noimprovement) + ". Or Wrong Tab selected.. Need MONTH Tab", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                            }
                        } else {
                            Bundle params = new Bundle();
                            params.putString("agent", AgentData.getagentname());
                            params.putString("reason", "too small");
                            params.putString("debug", "" + iffails + " , " + theresult);
                            mFirebaseAnalytics.logEvent("scanfail", params);


                            StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.error_tosmall) + ". " + theresult + " " + getResources().getString(R.string.error_noimprovement) + ".", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(System.currentTimeMillis()));
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "value too small");
                            bundle.putString(FirebaseAnalytics.Param.CONTENT, "" + iffails + " , " + theresult + ", " + AgentData.getagentname());
                            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "scanfail");
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                        }
                        if (onmonth) {
                            Log.i("MORE --> ", "status - " + multimission);
                            if (multimission && weekrunning && moreaction) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                // builder.setMessage("Text not finished...");
                                LayoutInflater inflater = getLayoutInflater();
                                View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                builder.setView(dialog);
                                final TextView input = dialog.findViewById(R.id.caltext);
                                // input.setAllCaps(true);
                                input.setTypeface(type);
                                input.setText("Would you also like to update the Weekly Mission?");
                                //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                //localTextView1.setTypeface(proto);
                                builder.setPositiveButton("YES!", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {


                                        moreupdateweek(theyearx, themonthx, true);

                                    }
                                });

                                builder.setNeutralButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                            startexportebl(AgentData.getEbl_token());
                                        }

                                    }
                                });


                                builder.setCancelable(true);

                                if (!isFinishing()) {
                                    builder.create().show();

                                }

                            } else {
                                initmonth(theyearx, themonthx, true);
                                if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                    startexportebl(AgentData.getEbl_token());
                                }
                            }
                        } else if (onearth) {
                            initearth(theyearx, onactionid, true);
                            if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                startexportebl(AgentData.getEbl_token());
                            }
                        } else if (onmini) {
                            initmini(Long.parseLong(onactionid), true);

                        } else {

                            // SimpleDateFormat fday = new SimpleDateFormat("yyyyMMdd_HHmmss");
                            SimpleDateFormat fmonth = new SimpleDateFormat("MM");
                            SimpleDateFormat fyear = new SimpleDateFormat("yyyy");
                            final String themonth = fmonth.format(new Date());
                            final String theyear = fyear.format(new Date());
                            theyearx = theyear;
                            themonthx = themonth;

                            DateFormat format = new SimpleDateFormat("dd");
                            Calendar calendar = Calendar.getInstance(Locale.getDefault());
                            Calendar.getInstance(Locale.getDefault()).setTimeZone(TimeZone.getDefault());
                            calendar.setFirstDayOfWeek(Calendar.MONDAY);
                            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                            final String[] days = new String[7];
                            for (int i = 0; i < 7; i++) {
                                days[i] = format.format(calendar.getTime());
                                calendar.add(Calendar.DAY_OF_MONTH, 1);
                            }
                            String tempthemonth = themonth;
                            if (Integer.parseInt(days[1]) < Integer.parseInt(days[0]) || Integer.parseInt(days[2]) < Integer.parseInt(days[0]) || Integer.parseInt(days[3]) < Integer.parseInt(days[0]) || Integer.parseInt(days[4]) < Integer.parseInt(days[0]) || Integer.parseInt(days[5]) < Integer.parseInt(days[0]) || Integer.parseInt(days[6]) < Integer.parseInt(days[0])) {
                                if (calendar.get(Calendar.DAY_OF_MONTH) <= 7) {
                                    tempthemonth = String.valueOf(Integer.parseInt(themonth) - 1);
                                } else {
                                    tempthemonth = String.valueOf(Integer.parseInt(themonth) + 1);
                                }
                            } else {
                                tempthemonth = themonth;
                            }

                            String strDate = days[0] + "." + (Integer.parseInt(tempthemonth)) + "." + theyear;
                            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                            Date date = null;
                            try {
                                date = dateFormat.parse(strDate);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Calendar now = Calendar.getInstance(Locale.GERMAN);
                            now.setTimeZone(TimeZone.getDefault());
                            now.setFirstDayOfWeek(Calendar.MONDAY);
                            now.setTime(date);
                            int currentweek = now.get(Calendar.WEEK_OF_YEAR);
                            String cweeksend;
                            if (currentweek <= 9) {
                                currentweek = Integer.parseInt("0" + currentweek);
                                cweeksend = "0" + currentweek;
                            } else {
                                cweeksend = String.valueOf(currentweek);
                            }
                            if (currentweek <= 9) {
                                currentweek = Integer.parseInt("0" + currentweek);
                                cweeksend = "0" + currentweek;
                            } else {
                                cweeksend = String.valueOf(currentweek);
                            }

                            Log.i("CURRENT WEEK FOR", "" + cweeksend);
                            final String thefirstweekday = cweeksend + "" + days[0];
                            initweek(theyear, tempthemonth, thefirstweekday, days, true);
                            if (multimission && monthrunning && moreaction) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);

                                // builder.setMessage("Text not finished...");
                                LayoutInflater inflater = getLayoutInflater();
                                View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                builder.setView(dialog);
                                final TextView input = dialog.findViewById(R.id.caltext);
                                // input.setAllCaps(true);
                                input.setTypeface(type);
                                input.setText("Would you also like to update the Monthly Mission?");
                                //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                //localTextView1.setTypeface(proto);
                                builder.setPositiveButton("Yes!", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {


                                        moreupdatemonth(theyear, themonth, thefirstweekday, days, true);

                                    }
                                });

                                builder.setNeutralButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                            startexportebl(AgentData.getEbl_token());
                                        }
                                    }
                                });


                                builder.setCancelable(true);

                                if (!isFinishing()) {
                                    builder.create().show();

                                }

                            } else {

                                initweek(theyear, tempthemonth, thefirstweekday, days, true);
                                if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                                    startexportebl(AgentData.getEbl_token());
                                }
                                // initweek(theyearx, theweekx, true);
                            }
                        }
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                }
            });
        }

    }

    public void openHistory(final String theyear, final String themonth, final String theid, final String theartis, final Activity aodMain, final HistoryUpdates historyadapterx, int position) {
        LayoutInflater inflater = aodMain.getLayoutInflater();
        hasdialogbefore = false;
        final View dialog = inflater.inflate(R.layout.dialog_history, null);
        if (historybuilder == null) {
            historybuilder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);
            hasdialogbefore = false;
            // builder.setMessage("Text not finished...");

            historybuilder.setView(dialog);
            historybuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    hasdialogbefore = false;
                    if (historybuilder != null) {
                        historybuilder.create().dismiss();
                        historybuilder = null;
                    }
                }
            });
        } else {
            hasdialogbefore = true;
        }
        final List<Long> updatetime = new ArrayList<Long>();
        final List<String> updatevalue = new ArrayList<String>();
        final List<String> updatekey = new ArrayList<String>();


        mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshothistory) {
                String[] updatevalueset;
                Long[] updatetimeset;
                String[] updatekeyset;

                updatekeyset = null;
                updatetimeset = null;
                updatevalueset = null;
                updatetime.clear();
                updatekey.clear();
                updatevalue.clear();
                for (DataSnapshot historyis : snapshothistory.getChildren()) {
                    //  Log.i("timestamp", historyis.getValue().toString());
                    if (historyis.hasChildren()) {
                        updatetime.add(Long.parseLong(historyis.child("timestamp").getValue().toString()));
                        updatevalue.add(historyis.child("value").getValue().toString());
                        updatekey.add(historyis.getKey());

                        Log.i("timestamp ----->", historyis.child("timestamp").getValue().toString());
                    }
                }

                updatevalueset = new String[updatevalue.size()];
                updatetimeset = new Long[updatetime.size()];
                updatekeyset = new String[updatekey.size()];

                updatetimeset = updatetime.toArray(updatetimeset);
                updatevalueset = updatevalue.toArray(updatevalueset);
                updatekeyset = updatekey.toArray(updatekeyset);
                Arrays.sort(updatetimeset, Collections.reverseOrder());
                Arrays.sort(updatevalueset, Collections.reverseOrder());
                Arrays.sort(updatekeyset, Collections.reverseOrder());

                if (theartis.equalsIgnoreCase("month")) {

                    if (historyadapterx == null) {
                        historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, monthbadgeshort1, monthbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                        lv = dialog.findViewById(R.id.listhistory);
                        lv.setAdapter(historyadapter);
                        historyadapter.notifyDataSetChanged();
                    } else {

                        historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, monthbadgeshort1, monthbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                        historyadapter.notifyDataSetChanged();
                    }
                } else if (theartis.equalsIgnoreCase("week")) {

                    if (historyadapterx == null) {
                        historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, weekbadgeshort1, weekbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                        lv = dialog.findViewById(R.id.listhistory);
                        lv.setAdapter(historyadapter);
                        historyadapter.notifyDataSetChanged();
                    } else {

                        historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, weekbadgeshort1, weekbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                        historyadapter.notifyDataSetChanged();
                    }
                } else if (theartis.equalsIgnoreCase("earth")) {

                    if (historyadapterx == null) {
                        historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, earthbadgeshort1, earthbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                        lv = dialog.findViewById(R.id.listhistory);
                        lv.setAdapter(historyadapter);
                        historyadapter.notifyDataSetChanged();
                    } else {

                        historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, earthbadgeshort1, earthbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                        historyadapter.notifyDataSetChanged();
                    }
                } else {
                    if (historyadapterx == null) {
                        historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, weekbadgeshort1, weekbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                        lv = dialog.findViewById(R.id.listhistory);
                        lv.setAdapter(historyadapter);
                        historyadapter.notifyDataSetChanged();
                    } else {

                        historyadapter = new HistoryUpdates(aodMain, updatetime.size(), updatetimeset, updatevalueset, weekbadgeshort1, weekbadgetitle, updatekeyset, theyear, themonth, theartis, theid, historyadapter);
                        historyadapter.notifyDataSetChanged();
                    }
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

        //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
        //localTextView1.setTypeface(proto);

        historybuilder.setPositiveButton("close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                hasdialogbefore = false;
                if (historybuilder != null) {
                    historybuilder.create().dismiss();
                    historybuilder = null;
                }

            }
        });

        if (!hasdialogbefore) {
            historybuilder.setCancelable(true);

        }

        if (!isFinishing()) {
            if (historybuilder != null) {
                if (historybuilder.create().isShowing()) {
                    historybuilder.create().dismiss();
                } else {
                    if (!hasdialogbefore) {

                        historybuilder.create().show();
                    }
                }
            }

        }
    }

    public void removeHistory(final String keyi, final int position, final String theyear, final String themonth, final String theartis, final String theid, final Activity context, final HistoryUpdates historyadapterx) {
        StyleableToast.makeText(context, " " + keyi, Toast.LENGTH_LONG, R.style.defaulttoast).show();


        final Map<String, Object> updaterunning = new HashMap<>();


        // IF hasmissions


        mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(final DataSnapshot snapshotremove) {
                boolean hasmorechilds;
                boolean hasnchild;
                //HAS NEXT
                if (snapshotremove.getChildrenCount() > 0) {
                    hasnchild = true;
                    // HAS MORE Childs
                    // HAS NOT MORE CHilds
                    hasmorechilds = snapshotremove.getChildrenCount() > (position + 2);
                }
                // HAS NO NEXT
                else {
                    hasmorechilds = false;
                    hasnchild = false;
                }

                // IST Das neuste und hat mehrere Childs
                if (position == 0 && hasmorechilds) {

                    int searchpos = 0;
                    String prekey = keyi;
                    String searchkey = "null";
                    for (DataSnapshot postSnapshot : snapshotremove.getChildren()) {
                        Log.i("KEY: pos", postSnapshot.getKey() + " ");
                        if (postSnapshot.hasChildren()) {
                            if (postSnapshot.getKey().equalsIgnoreCase(keyi)) {
                                searchkey = prekey;
                                Log.i("KEYs --> ", "this " + keyi + " davor: " + searchkey);
                            } else {
                                prekey = postSnapshot.getKey();
                            }
                        }
                    }

                    String valueforthis = snapshotremove.child(keyi).child("value").getValue().toString();
                    String valuenew = snapshotremove.child(searchkey).child("value").getValue().toString();
                    String valuestart = snapshotremove.child(keyi).child("startvalue").getValue().toString();
                    String valuerechnen = snapshotremove.child(keyi).child("value").getValue().toString();
                    Map<String, Object> updaterunning2 = new HashMap<>();
                    final int rechnung = Integer.parseInt(valuenew) - Integer.parseInt(valuestart);
                    final int rechnung2 = Integer.parseInt(valuerechnen) - Integer.parseInt(valuenew);
                    updaterunning2.put("/lastvalue", valuenew);
                    updaterunning2.put("/currentvalue", rechnung);
                    updaterunning2.put("/startvalue", snapshotremove.child(searchkey).child("startvalue").getValue().toString());
                    Log.i("USER --> ", " " + user.getUid());
                    Log.i("Data --> ", "y " + theyear + " art:" + theartis + " id: " + theid + " new: " + valuenew + " current:" + rechnung + " start:" + snapshotremove.child(searchkey).child("startvalue"));

                    // TODO Abzug All Community

                    mDatabaserunnung.child(theyear).child(theartis).child(theid).child(user.getUid()).updateChildren(updaterunning2).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();
                                //openHistory(theyear,themonth,theid, theartis, context);
                                mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).child(keyi).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            StyleableToast.makeText(context, "" + (position + 1) + " wurde gelöscht und die Mission zurückgesetzt.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                            hasdialogbefore = false;
                                            historybuilder.create().dismiss();
                                            historybuilder = null;

                                            removefromcommunity(rechnung2, theyear, theartis, theid, themonth, context);


                                        } else if (task.isCanceled()) {

                                            Log.w("ERROR ", context.getClass().getSimpleName() + " 1055 Error");

                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Log.w("ERROR ", context.getClass().getSimpleName() + " 1056 Error error " + e.getMessage());

                                    }
                                });

                            } else if (task.isCanceled()) {
                                Log.w("ERROR ", context.getClass().getSimpleName() + " 10001");

                            } else {
                                Log.w("ERROR ", context.getClass().getSimpleName() + " 10002");

                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Log.w("ERROR ", context.getClass().getSimpleName() + " 10003");


                            //signOut();
                        }
                    });

                }
                // IST das neuste und das eintige
                else if (position == 0 && !hasmorechilds) {

                    updaterunning.put("/lastvalue", 0);
                    updaterunning.put("/currentvalue", 0);
                    updaterunning.put("/startvalue", 0);
                    mDatabaserunnung.child(theyear).child(theartis).child(theid).child(user.getUid()).updateChildren(updaterunning).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();
                                //openHistory(theyear,themonth,theid, theartis, context);
                                mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).child(keyi).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            StyleableToast.makeText(context, "" + (position + 1) + " wurde gelöscht und die Mission zurückgesetzt!", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                            historybuilder.create().dismiss();
                                            hasdialogbefore = false;
                                            historybuilder = null;
                                            context.recreate();

                                        } else if (task.isCanceled()) {

                                            Log.w("ERROR ", context.getClass().getSimpleName() + " 1055 Error");

                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Log.w("ERROR ", context.getClass().getSimpleName() + " 1056 Error error " + e.getMessage());

                                    }
                                });

                            } else if (task.isCanceled()) {
                                Log.w("ERROR ", context.getClass().getSimpleName() + " 10001");

                            } else {
                                Log.w("ERROR ", context.getClass().getSimpleName() + " 10002");

                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Log.w("ERROR ", context.getClass().getSimpleName() + " 10003");


                            //signOut();
                        }
                    });
                } else if (position > 0) {

                    mDatabase.child(user.getUid()).child("missions").child(theyear).child(theartis).child(themonth).child(theid).child(keyi).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();

                                hasdialogbefore = false;
                                historybuilder.create().dismiss();
                                historybuilder = null;
                                StyleableToast.makeText(context, "" + (position + 1) + " wurde gelöscht", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                context.recreate();

                            } else if (task.isCanceled()) {

                                hasdialogbefore = false;
                                historybuilder.create().dismiss();
                                historybuilder = null;

                                Log.w("ERROR ", context.getClass().getSimpleName() + " 1055 Error");

                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {

                            Log.w("ERROR ", context.getClass().getSimpleName() + " 1056 Error error " + e.getMessage());

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(context, getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

    }

    // GIVE UP
    private void giveuppvp(final String pvpkey, String pvpbadgeid, String pvptitle, final String theart, final String owneruid, final String inviteposition) {
        if (listenerpvpjoin != null) {
            mbpvp.removeEventListener(listenerpvpjoin);
            listenerpvpjoin = null;
        }


        mDatabaserunnungpvp.child(pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {

            public void onDataChange(final DataSnapshot snapshotpvprunning) {

                // MORE AKTIVE AGENTS IN
                if (snapshotpvprunning.getChildrenCount() >= 3) {

                    // OPEN LINK FOR NEW AGENTS
                    if (theart.equalsIgnoreCase("linkshare")) {
                        mbpvplinks.child("AODs_" + pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {

                            public void onDataChange(DataSnapshot snapshotagents) {

                                if (!snapshotagents.child("createruuid").getValue().toString().equalsIgnoreCase(user.getUid())) {

                                    int joindagents = Integer.parseInt(snapshotagents.child("agents").getValue().toString());
                                    int updatejoined = joindagents - 1;
                                    mbpvplinks.child("AODs_" + pvpkey).child("agents").setValue(updatejoined);
                                } else {
                                    int getfirst = 0;
                                    for (final DataSnapshot childpvpersatz : snapshotpvprunning.getChildren()) {
                                        getfirst++;
                                        if (getfirst == 1) {
                                            mbpvplinks.child("AODs_" + pvpkey).child("createruuid").setValue(childpvpersatz.getKey());
                                            mbpvp.child(pvpkey).child("createruuid").setValue(childpvpersatz.getKey());
                                            mDatabase.child(childpvpersatz.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {

                                                public void onDataChange(DataSnapshot snapshotagentname) {
                                                    mbpvp.child(pvpkey).child("creater").setValue(snapshotagentname.getValue().toString());

                                                    mDatabase.child(childpvpersatz.getKey()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {

                                                        public void onDataChange(DataSnapshot snapshotprofileimage) {
                                                            mbpvp.child(pvpkey).child("createrimage").setValue(snapshotprofileimage.getValue().toString());
                                                            mbpvplinks.child("AODs_" + pvpkey).child("createrimage").setValue(snapshotprofileimage.getValue().toString());


                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                            //  recreate();
                                                        }
                                                    });

                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                    //  recreate();
                                                }
                                            });


                                        }
                                    }
                                }


                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                //  recreate();
                            }
                        });
                    }


                    if (Integer.parseInt(inviteposition) > 0) {
                        mbpvp.child(pvpkey).child("agents").child(inviteposition).setValue(null);
                    }

                    // REMOVE AGENT FROM PVP
                    mDatabaserunnungpvp.child(pvpkey).child(user.getUid()).setValue(null);
                    mDatabase.child(user.getUid()).child("pvp").child(pvpkey).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {

                                // TODO SET UPDAT STREAM
                                recreate();

                                //  Log.w("ERROR ", this.getClass().getSimpleName() + " 5644 Error ");
                            } else if (task.isCanceled()) {
                                recreate();
                            } else {
                                recreate();
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            recreate();
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                            //signOut();
                        }
                    });


                }
                // IAM THE LAST AGENT IN THE PVP
                else {

                    // DELETE INVITE PVP KEY
                    if (theart.equalsIgnoreCase("linkshare")) {

                        if (user.getUid().equalsIgnoreCase(owneruid) && snapshotpvprunning.getChildrenCount() <= 1) {
                            StyleableToast.makeText(getApplicationContext(), "PvP Invite deleted", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            mbpvplinks.child("AODs_" + pvpkey).setValue(null);


                        } else {

                            mbpvplinks.child("AODs_" + pvpkey).child("agents").addListenerForSingleValueEvent(new ValueEventListener() {

                                public void onDataChange(DataSnapshot snapshotagents) {

                                    int joindagents = Integer.parseInt(snapshotagents.getValue().toString());
                                    int updatejoined = joindagents - 1;
                                    mbpvplinks.child("AODs_" + pvpkey).child("agents").setValue(updatejoined);


                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                    //  recreate();
                                }
                            });
                        }


                    }

                    // REMOVE AGENT FROM PVP

                    mDatabase.child(user.getUid()).child("pvp").child(pvpkey).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {

                                mDatabaserunnungpvp.child(pvpkey).child(user.getUid()).setValue(null);

                                // TODO SET UPDAT STREAM
                                Map<String, Object> updatepvp = new HashMap<>();


                                if (theart.equalsIgnoreCase("linkshare")) {
                                    updatepvp.put("/finished", false);
                                    updatepvp.put("/active", false);
                                } else {
                                    if (Integer.parseInt(inviteposition) > 0) {
                                        mbpvp.child(pvpkey).child("agents").child(inviteposition).setValue(null);

                                        // TODO ALL AGENTS LEAVE
                                    }
                                    updatepvp.put("/active", false);
                                    updatepvp.put("/finished", true);
                                }
                                mbpvp.child(pvpkey).updateChildren(updatepvp).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {


                                            mDatabaserunnungpvp.child(pvpkey).child(user.getUid()).setValue(null);

                                            if (snapshotpvprunning.getChildrenCount() <= 1) {
                                                StyleableToast.makeText(getApplicationContext(), "PvP Invite deleted", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                mbpvp.child(pvpkey).setValue(null);

                                            }
                                            recreate();

                                            //  Log.w("ERROR ", this.getClass().getSimpleName() + " 5644 Error ");
                                        } else if (task.isCanceled()) {
                                            recreate();
                                        } else {
                                            recreate();
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                                        }
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        recreate();
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                                        //signOut();
                                    }
                                });

                                //  Log.w("ERROR ", this.getClass().getSimpleName() + " 5644 Error ");
                            } else if (task.isCanceled()) {
                                recreate();
                            } else {
                                recreate();
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            recreate();
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                            //signOut();
                        }
                    });


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                recreate();
            }
        });


    }

    public void removefromcommunity(final int rechnung, final String theyear, final String theartis, String theid, final String themonth, final Activity context) {

        mDatabasemissions.child(theartis).child(theyear).child(themonth).child("community").addListenerForSingleValueEvent(new ValueEventListener() {
            int community_current, community_current_enl, community_current_res, rechenvalue, enlnew, resnew;

            public void onDataChange(DataSnapshot snapshotcommunityvalues) {
                final Map<String, Object> childUpdatesCommunity = new HashMap<>();

                community_current = Integer.parseInt(snapshotcommunityvalues.child("current").getValue().toString());
                community_current_enl = Integer.parseInt(snapshotcommunityvalues.child("enl").getValue().toString());
                community_current_res = Integer.parseInt(snapshotcommunityvalues.child("res").getValue().toString());


                rechenvalue = community_current - rechnung;

                childUpdatesCommunity.put("/current", rechenvalue);

                if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                    enlnew = community_current_enl - rechnung;
                    childUpdatesCommunity.put("/enl", enlnew);
                } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                    resnew = community_current_res - rechnung;
                    childUpdatesCommunity.put("/res", resnew);

                }

                Log.i("Update community -->", "n: " + rechenvalue + " enl " + enlnew + " res " + resnew + " rechen " + rechnung);


                mDatabasemissions.child(theartis).child(theyear).child(themonth).child("community").updateChildren(childUpdatesCommunity).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            context.recreate();
                            // String newKey = mDatabase.child(user.getUid()).child("missions").child(theyearx).child(missionart).child(themonthx).child(onactionid).push().getKey();

                        } else if (task.isCanceled()) {
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                            errorToast("Error Community Mission", "40001");
                            context.recreate();
                        } else {
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                            errorToast("Community Mission", "40002");
                            context.recreate();
                        }
                    }

                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                        errorToast("Community Mission", "40003");
                        context.recreate();
                        //signOut();
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                context.recreate();
            }
        });


    }

    public void updateVersionInPreferences(Context context, String thisVersion) {
        // save new version number to preferences

        //Log.e("TAG", " "+thisVersion);
        pref.edit().putString("PREFS_VERSION_KEY", thisVersion).apply();


    }

    private void openDialogFragment(DialogFragment dialogStandardFragment) {
        if (dialogStandardFragment != null) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment prev = fm.findFragmentByTag("changelogdemo_dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            dialogStandardFragment.show(ft, "changelogdemo_dialog");
        }
    }

    private void getLogDialog() {
        openDialogFragment(new Fragment_ChangeLog());
    }

    private boolean firstRun() {
        return !this.lastVersion.equals(this.thisVersion);
    }

    private void errorToast(String text, String num) {
        bmb_main.setEnabled(true);
        join_btn_week.setEnabled(true);
        join_btn_month.setEnabled(true);
        frameloadmonth.setVisibility(View.GONE);
        framecardmonth.setVisibility(View.VISIBLE);
        frameloadweek.setVisibility(View.GONE);
        framecardweek.setVisibility(View.VISIBLE);
        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + num, Toast.LENGTH_LONG, R.style.defaulttoast).show();
    }

    private void infoToast(String num) {
        bmb_main.setEnabled(true);
        join_btn_week.setEnabled(true);
        join_btn_month.setEnabled(true);
        frameloadmonth.setVisibility(View.GONE);
        framecardmonth.setVisibility(View.VISIBLE);
        frameloadweek.setVisibility(View.GONE);
        framecardweek.setVisibility(View.VISIBLE);
        StyleableToast.makeText(getApplicationContext(), num, Toast.LENGTH_LONG, R.style.defaulttoast).show();
    }

    private String getStartOFWeek(int enterWeek, int enterYear) {
//enterWeek is week number
//enterYear is year
        Calendar calendar = Calendar.getInstance(Locale.getDefault());

        calendar.clear();

        Calendar.getInstance(Locale.GERMANY).setTimeZone(TimeZone.getDefault());
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM"); // PST`
        Date startDate = calendar.getTime();
        String startDateInStr = formatter.format(startDate);
        System.out.println("...date..." + startDateInStr);
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.add(Calendar.DATE, 6);
        Date enddate = calendar.getTime();
        String endDaString = formatter.format(enddate);
        System.out.println("...date..." + endDaString);

        return "" + startDateInStr + " - ";
    }

    private String getEndOFWeek(int enterWeek, int enterYear) {
//enterWeek is week number
//enterYear is year
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.clear();
        // now.setTimeZone(TimeZone.getTimeZone("Germany"));
        Calendar.getInstance(Locale.GERMANY).setTimeZone(TimeZone.getDefault());
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM"); // PST`
        Date startDate = calendar.getTime();
        String startDateInStr = formatter.format(startDate);
        System.out.println("...date..." + startDateInStr);
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.add(Calendar.DATE, 6);
        Date enddate = calendar.getTime();
        String endDaString = formatter.format(enddate);
        System.out.println("...date..." + endDaString);

        return "" + endDaString;
    }

    private String getStartMonthofWeek(int enterWeek, int enterYear) {
//enterWeek is week number
//enterYear is year
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.clear();
        // now.setTimeZone(TimeZone.getTimeZone("Germany"));
        Calendar.getInstance(Locale.GERMANY).setTimeZone(TimeZone.getDefault());
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("MM"); // PST`
        Date startDate = calendar.getTime();
        String startDateInStr = formatter.format(startDate);
        System.out.println("...date..." + startDateInStr);
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.add(Calendar.DATE, 6);
        Date enddate = calendar.getTime();
        String endDaString = formatter.format(enddate);
        System.out.println("...date..." + endDaString);

        return "" + startDateInStr;
    }

    private String getEndMonthofWeek(int enterWeek, int enterYear) {
//enterWeek is week number
//enterYear is year
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.clear();
        // now.setTimeZone(TimeZone.getTimeZone("Germany"));
        Calendar.getInstance(Locale.GERMANY).setTimeZone(TimeZone.getDefault());
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("MM"); // PST`
        Date startDate = calendar.getTime();
        String startDateInStr = formatter.format(startDate);
        System.out.println("...date..." + startDateInStr);
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.add(Calendar.DATE, 6);
        Date enddate = calendar.getTime();
        String endDaString = formatter.format(enddate);
        System.out.println("...date..." + endDaString);

        return "" + endDaString;
    }

    private void startKonfetti(String gold, String missionart, String monthbadgedrawablegold) {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int newwidth = (size.x) / 2;
        int newheigh = (size.y) / 3;
        int newheigh2 = newheigh / 8;
        viewKonfetti.build()
                .addColors(getResources().getColor(R.color.colorAccent, getTheme()), getResources().getColor(R.color.colorRES, getTheme()), getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()), getResources().getColor(R.color.colorENL, getTheme()), getResources().getColor(R.color.colorWhite, getTheme()))
                .setDirection(0.0, 359.0)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(2000L)
                .addShapes(Shape.RECT, Shape.CIRCLE)
                .addSizes(new Size(12, 5f))
                .setPosition(newwidth, newheigh - newheigh2)
                .burst(200);
    }

    public void notificationResults(long countis, Activity activityis, Context contextis, ImageView imageis, TextView textis) {


        if (countis > 0) {
            imageis.setImageDrawable(contextis.getResources().getDrawable(R.drawable.ic_notifications, contextis.getTheme()));
            textis.setText("" + countis);
            textis.setVisibility(View.VISIBLE);
        } else {
            imageis.setImageDrawable(contextis.getResources().getDrawable(R.drawable.ic_notifications_none, contextis.getTheme()));
            textis.setText("0");
            textis.setVisibility(View.GONE);
        }

    }

    private void createNotificationChannel() {
        String DEFAULT_CHANNEL_ID = getResources().getString(R.string.default_notification_channel_id);
        String AGENTS_CHANNEL_ID = getResources().getString(R.string.agents_notification_channel_id);
        String MONDAYWEEK_ID = getResources().getString(R.string.monday_week_notification_channel_id);
        String EVENTCHANNEL_ID = getResources().getString(R.string.event_notification_channel_id);
        String SUNDAYWEEK_ID = getResources().getString(R.string.sunday_week_notification_channel_id);
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            CharSequence name = getString(R.string.fcm_default_name);
            String description = getString(R.string.fcm_default_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(DEFAULT_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this

            notificationManager.createNotificationChannel(channel);

            CharSequence event_week = getString(R.string.fcm_event);
            String description_event_week = getString(R.string.fcm_event_description);
            int importance_event_week = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel_event_week = new NotificationChannel(EVENTCHANNEL_ID, event_week, importance_event_week);
            channel_event_week.setDescription(description_event_week);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel_event_week);

            CharSequence monday_week = getString(R.string.fcm_monday_week);
            String description_monday_week = getString(R.string.fcm_monday_week_description);
            int importance_monday_week = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel_monday_week = new NotificationChannel(MONDAYWEEK_ID, monday_week, importance_monday_week);
            channel_monday_week.setDescription(description_monday_week);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this

            notificationManager.createNotificationChannel(channel_monday_week);


            CharSequence sunday_week = getString(R.string.fcm_sunday_week);
            String description_sunday_week = getString(R.string.fcm_sunday_week_description);
            int importance_sunday_week = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel_sunday_week = new NotificationChannel(SUNDAYWEEK_ID, sunday_week, importance_sunday_week);
            channel_sunday_week.setDescription(description_sunday_week);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this

            notificationManager.createNotificationChannel(channel_sunday_week);


            CharSequence nameagents = getString(R.string.fcm_agents_name);
            String descriptionagents = getString(R.string.fcm_agents_description);
            int importanceagents = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channelagents = new NotificationChannel(AGENTS_CHANNEL_ID, nameagents, importanceagents);
            channelagents.setDescription(descriptionagents);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this

            notificationManager.createNotificationChannel(channelagents);
        }
    }

    private void androidQworkaround(final String comp, final String theartid, final String themid, final String basistext, final String statsis) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AODMain.this, R.style.CustomDialog);
        Log.d(TAG, "onPrimaryClipChanged");
        ClipboardManager mClipboardManager;
        mClipboardManager =
                (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        ClipData clip = mClipboardManager.getPrimaryClip();
        if (clip != null && clip.getItemCount() > 0) {
            clip.getItemAt(0).getText();
            // builder.setMessage("Text not finished...");
            Log.i("ORKARAOUND TEXT ->", clip.getItemAt(0).getText() + " ");

            String FILENAME = "clipboard-history.tsv";

            File mHistoryFile = new File(getExternalFilesDir(null), FILENAME);


            if (isExternalStorageWritable()) {
                try {

                    if (clip.getItemAt(0).getText().toString().startsWith("Time")) {
                        BufferedWriter writer =
                                new BufferedWriter(new FileWriter(mHistoryFile, false));

                        writer.write(clip.getItemAt(0).getText().toString());
                        writer.newLine();
                        writer.close();


                        // GO

                        // END

                        String line;
                        String[] columns; //contains column names
                        int num_cols;
                        String[] tokens;
                        String valueclip = null;
                        String timespanclip = null;
                        String lifetimeclip = null;
                        String agentclip = null;
                        BufferedReader reader = null;
                        try {
                            //Get the CSVReader instance with specifying the delimiter to be used
                            reader = new BufferedReader(new FileReader(mHistoryFile), ',');

                            line = reader.readLine();
                            columns = line.split("\t");
                            num_cols = columns.length;

                            line = reader.readLine();


                            while (true) {
                                tokens = line.split("\t");

                                if (tokens.length == num_cols) { //if number columns equal to number entries


                                    for (int k = 0; k < num_cols; ++k) { //for each column

                                        if (comp.equalsIgnoreCase(columns[k])) {
                                            valueclip = tokens[k];
                                        }

                                        if (columns[k].equalsIgnoreCase("Lifetime AP")) {
                                            lifetimeclip = tokens[k];
                                        }

                                        if (columns[k].equalsIgnoreCase("Time Span")) {
                                            timespanclip = tokens[k];
                                        }
                                        Log.i("Loop ", " " + columns[k] + " value: " + tokens[k]);

                                        if (columns[k].equalsIgnoreCase("Agent Name")) {
                                            agentclip = tokens[k];
                                        }
                                    }


                                    if ((line = reader.readLine()) != null) {//if not last line

                                    } else {
                                        Log.i("Loop finished", "done x " + comp);
                                        Log.i("Loop done", lifetimeclip + " " + timespanclip + " " + valueclip);

                                        Log.i("Loop done2", themid + " " + theartid + " " + basistext);

                                        Log.i("Loop done3", " " + multimission);

                                        multimission = mainprefssettings.getBoolean("update_missions", false);

                                        recognizeTextClip(valueclip, theartid, themid, basistext, lifetimeclip, timespanclip, agentclip, multimission, statsis);

                                        //  connectclip(valueclip, theartid, themid, basistext, lifetimeclip, timespanclip, agentclip);
                                        break;
                                    }
                                } else {
                                    //line = read.readLine(); //read next line if wish to continue parsing despite error


                                    System.exit(-1); //error message
                                }
                            }


                            reader.close();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block

                            e.printStackTrace();
                        }
                    } else {
                        // TODO wrong input
                        runOnUiThread(new Runnable() {
                            public void run() {
                                StyleableToast.makeText(getApplicationContext(), "No Ingress stats found in your clipboard...", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                            }
                        });

                        //Log.i("WRONG CLIPBOARD DATA", " POMMES");
                    }


                } catch (IOException e) {

                    Log.w(TAG, String.format("Failed to open file %s for writing!",
                            mHistoryFile.getAbsoluteFile()));

                }
            } else {


                Log.w(TAG, "External storage is not writable!");
            }
        } else {
            Log.w(TAG, "No CLIP DATA FOUND");


        }


    }

    private void moreupdateweek(String theyear, String themonth, Boolean status) {
        // TODO INIT MONTH
        String TAG = "ClipboardManager";
        String FILENAME = "clipboard-history.tsv";

        File mHistoryFile;
        String comp = weekbadgefield;
        String line;
        String[] columns; //contains column names
        int num_cols;
        String[] tokens;
        String valueclip = null;
        String timespanclip = null;
        String lifetimeclip = null;
        String agentclip = null;
        BufferedReader reader = null;

        mHistoryFile = new File(getExternalFilesDir(null), FILENAME);
        try {
            //Get the CSVReader instance with specifying the delimiter to be used
            reader = new BufferedReader(new FileReader(mHistoryFile), ',');

            line = reader.readLine();
            columns = line.split("\t");
            num_cols = columns.length;

            line = reader.readLine();


            while (true) {
                tokens = line.split("\t");

                if (tokens.length == num_cols) { //if number columns equal to number entries


                    for (int k = 0; k < num_cols; ++k) { //for each column

                        if (comp.equalsIgnoreCase(columns[k])) {
                            valueclip = tokens[k];
                        }

                        if (columns[k].equalsIgnoreCase("Lifetime AP")) {
                            lifetimeclip = tokens[k];
                        }

                        if (columns[k].equalsIgnoreCase("Time Span")) {
                            timespanclip = tokens[k];
                        }
                        Log.i("Loop ", " " + columns[k] + " value: " + tokens[k]);

                        if (columns[k].equalsIgnoreCase("Agent Name")) {
                            agentclip = tokens[k];
                        }
                    }


                    if ((line = reader.readLine()) != null) {//if not last line

                    } else {
                        Log.i("Loop finished", "done");
                        Log.i("Loop done", lifetimeclip + " " + timespanclip + " " + valueclip);
                        initmonth(theyearx, themonthx, true);
                        recognizeTextClip(valueclip, cweekmissionid, "week", weekbadgetitle, lifetimeclip, timespanclip, agentclip, false, "all");
                        // connectclip(valueclip, theartid, themid, basistext, lifetimeclip, timespanclip, agentclip);
                        break;
                    }
                } else {
                    //line = read.readLine(); //read next line if wish to continue parsing despite error
                    initmonth(theyearx, themonthx, true);
                    System.exit(-1); //error message
                }
            }


            reader.close();

            if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                startexportebl(AgentData.getEbl_token());
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void moreupdatemonth(String theyear, String themonth, String thefirstweekday, String[] days, Boolean status) {


        String TAG = "ClipboardManager";
        String FILENAME = "clipboard-history.tsv";

        File mHistoryFile;
        String comp = monthbadgefield;
        String line;
        String[] columns; //contains column names
        int num_cols;
        String[] tokens;
        String valueclip = null;
        String timespanclip = null;
        String lifetimeclip = null;
        String agentclip = null;
        BufferedReader reader = null;

        mHistoryFile = new File(getExternalFilesDir(null), FILENAME);
        try {
            //Get the CSVReader instance with specifying the delimiter to be used
            reader = new BufferedReader(new FileReader(mHistoryFile), ',');

            line = reader.readLine();
            columns = line.split("\t");
            num_cols = columns.length;

            line = reader.readLine();


            while (true) {
                tokens = line.split("\t");

                if (tokens.length == num_cols) { //if number columns equal to number entries


                    for (int k = 0; k < num_cols; ++k) { //for each column

                        if (comp.equalsIgnoreCase(columns[k])) {
                            valueclip = tokens[k];
                        }

                        if (columns[k].equalsIgnoreCase("Lifetime AP")) {
                            lifetimeclip = tokens[k];
                        }

                        if (columns[k].equalsIgnoreCase("Time Span")) {
                            timespanclip = tokens[k];
                        }
                        Log.i("Loop ", " " + columns[k] + " value: " + tokens[k]);

                        if (columns[k].equalsIgnoreCase("Agent Name")) {
                            agentclip = tokens[k];
                        }
                    }


                    if ((line = reader.readLine()) != null) {//if not last line

                    } else {
                        Log.i("Loop finished", "done " + comp);
                        Log.i("Loop done", lifetimeclip + " " + timespanclip + " " + valueclip);
                        initweek(theyear, themonth, thefirstweekday, days, true);
                        recognizeTextClip(valueclip, cmonthmissionid, "month", monthbadgetitle, lifetimeclip, timespanclip, agentclip, false, "all");
                        // connectclip(valueclip, theartid, themid, basistext, lifetimeclip, timespanclip, agentclip);
                        break;
                    }
                } else {
                    //line = read.readLine(); //read next line if wish to continue parsing despite error
                    initweek(theyear, themonth, thefirstweekday, days, true);
                    System.exit(-1); //error message
                }
            }


            reader.close();

            if (!AgentData.getEbl_token().equalsIgnoreCase("null") && AgentData.getEbl_token().length() > 22) {
                startexportebl(AgentData.getEbl_token());
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @NonNull
    private Rate createSnackBar(ViewGroup root, boolean lightTheme) {
        return new Rate.Builder(this)
                .setMessage(R.string.please_rate_short)
                .setTriggerCount(20)
                .setRepeatCount(80)
                .setMinimumInstallTime(5)
                .setLightTheme(lightTheme)
                .setFeedbackText("Provide feedback...")
                .setFeedbackAction(new OnFeedbackListener() {
                    @Override
                    public void onFeedbackTapped() {
                        Intent emailintent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto", "aod@prxenon.rocks", null));
                        emailintent.putExtra(Intent.EXTRA_SUBJECT, "AOD Feedback from " + AgentData.getagentname());
                        emailintent.putExtra(Intent.EXTRA_TEXT, "AOD Feedback...");
                        startActivity(Intent.createChooser(emailintent, "Send email..."));
                    }

                    @Override
                    public void onRateTapped() {
                        Toast.makeText(AODMain.this, "Redirecting to the Play Store...", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onRequestDismissed(boolean dontAskAgain) {
                        if (dontAskAgain) {
                            Toast.makeText(AODMain.this, "Ok then, I won't bother you again.", Toast.LENGTH_SHORT).show();
                        } else {
                            // Toast.makeText(AODMain.this, "Ok, I'll ask again later.", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .setSnackBarParent(root)
                .setSwipeToDismissVisible(true)
                .build();
    }

    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    private void onActionPerformed(Rate rate) {
        if (!rate.showRequest()) {
            showRemainingCount(rate);
        }
    }

    private void onAppLaunched(Rate rate) {
        rate.count();
        showRemainingCount(rate);
    }

    private Rate getCurrentRate() {

        return mRateBarDark;

    }

    @SuppressLint("ShowToast")
    private synchronized void showRemainingCount(Rate rate) {
        int count = (int) rate.getRemainingCount();
        String message = getResources().getQuantityString(R.plurals.toast_x_more_times, count, count);
        //  Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private boolean isAppInstalled(String packageName) {
        PackageManager pm = AODMain.this.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return pm.getApplicationInfo(packageName, 0).enabled;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            Log.i("Focus---> ", "changed --" + hasFocus);
        } else {

            Log.i("Focus---> ", "changed2 --" + hasFocus);
        }
    }

    private void startexportebl(String ebltoken) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);


        String FILENAME = "clipboard-history.tsv";

        File mHistoryFile;

        String line;

        BufferedReader reader = null;
        StringBuilder text = new StringBuilder();
        mHistoryFile = new File(getExternalFilesDir(null), FILENAME);
        try {
            //Get the CSVReader instance with specifying the delimiter to be used
            reader = new BufferedReader(new FileReader(mHistoryFile), ',');

            //line = reader.readLine();
            //columns = line.split("\t");
            //num_cols = columns.length;

            // line = reader.readLine();
            while ((line = reader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }


            reader.close();


            Log.i("TokenEBL -->", ebltoken);

            String BOBS_MAIN_ACTIIVTY = "rocks.prxenon.aodeblplugin.Update";
            String BOBS_PACKAGE = "rocks.prxenon.aodeblplugin";
            Intent intent = new Intent();
            intent.putExtra("token", ebltoken);
            intent.putExtra("data", text.toString());
            intent.setComponent(new ComponentName(BOBS_PACKAGE, BOBS_MAIN_ACTIIVTY));
            // intent.putExtra(EXTRA_SECRET_MESSAGE, message);
            startActivity(intent);

            /*try {

                HttpURLConnection connection = null;
                URL object = new URL("https://ebl.world/api/stats/tsv");
                connection = (HttpURLConnection) object.openConnection();
                connection.setRequestProperty("token",Token);
                connection.setReadTimeout(15000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);


                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("stats", text.toString());
                String query = builder.build().getEncodedQuery();

                OutputStream os = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(query);
                writer.flush();
                writer.close();
                os.close();

                 connection.connect();

                String responseMsg = connection.getResponseMessage();

                Log.i("Data send ", " "+text.toString());
                Log.i("RESPONSE ", " "+responseMsg);

            } catch (Exception e) {
                Log.e("Error = ", e.toString());
            } */

            Intent intentx = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ebl.world/stats"));
            intentx.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //intent.setData(Uri.parse(url));
            String channelId = getString(R.string.default_notification_channel_id);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intentx, 0);

            NotificationCompat.Builder buildern = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.drawable.ic_done_all)
                    .setContentTitle("EBL Updated")
                    .setContentText("AOD updated your Enlightened Bundesliga values")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            notificationManager.notify(0 /* ID of notification */, buildern.build());

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private void senddata(final String taskis) {
        // if (snapshothackventuser.child("hackvent").child(theyear).child(theday).child("task").getValue().toString().equalsIgnoreCase("task1")) {
        String FILENAME = "clipboard-history.tsv";

        File mHistoryFile;

        String line;

        BufferedReader reader = null;
        StringBuilder text = new StringBuilder();
        String[] columns; //contains column names
        int num_cols;
        String[] tokens;
        JSONObject agentstats = new JSONObject();
        final JSONObject userdata = new JSONObject();
        mHistoryFile = new File(getExternalFilesDir(null), FILENAME);
        try {
            //Get the CSVReader instance with specifying the delimiter to be used
            reader = new BufferedReader(new FileReader(mHistoryFile), ',');

            line = reader.readLine();

            columns = line.split("\t");
            num_cols = columns.length;

            line = reader.readLine();


            while (true) {
                tokens = line.split("\t");

                if (tokens.length == num_cols) { //if number columns equal to number entries


                    for (int k = 0; k < num_cols; ++k) { //for each column

                        if (AgentData.alltimelist.toString().contains(tokens[k])) {

                            agentstats.put(columns[k].trim().replaceAll("[-+.^:,()\\s+]", "").toLowerCase(), "ALLTIME");
                        } else {
                            if (isNumeric(tokens[k])) {
                                agentstats.put(columns[k].trim().replaceAll("[-+.^:,()\\s+]", "").toLowerCase(), Integer.parseInt(tokens[k]));
                            } else {
                                agentstats.put(columns[k].trim().replaceAll("[-+.^:,()\\s+]", "").toLowerCase(), tokens[k]);
                            }
                        }
                        // Log.i("DATA -->", columns[k].trim().replaceAll("[-+.^:,()\\s+]","").toLowerCase() +" " +tokens[k]);


                    }


                    if ((line = reader.readLine()) != null) {//if not last line

                    } else {
                        Log.i("Loop finished", "done ");
                        Log.i("user-->", user.getEmail().toLowerCase());
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.getDefault());
                        String formattedDate = sdf.format(new Date());

                        userdata.put("username", user.getEmail().toLowerCase());
                        userdata.put("givenname", user.getDisplayName());
                        userdata.put("timestamp", formattedDate);

                        userdata.put("task", taskis);


                        userdata.put("agentstats", agentstats);

                        Log.i("STATS ", userdata.toString());


                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                .permitAll().build();
                        StrictMode.setThreadPolicy(policy);


                        try {

                            HttpURLConnection connection = null;
                            URL object = new URL("https://advent.missionday.info/aodIngest");
                            // URL object = new URL("https://advent19.missionday.info/aodIngest");

                            connection = (HttpURLConnection) object.openConnection();
                            connection.setRequestProperty("Accept", "application/json");
                            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                            connection.setRequestProperty("User-Agent", "AOD agent - " + AgentData.getagentname());
                            connection.setReadTimeout(15000);
                            connection.setConnectTimeout(15000);
                            connection.setRequestMethod("POST");
                            connection.setDoInput(true);
                            connection.setDoOutput(true);


                            OutputStream os = connection.getOutputStream();
                            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(os, StandardCharsets.UTF_8));
                            writer.write(userdata.toString());
                            writer.flush();
                            writer.close();
                            os.close();

                            // connection.connect();

                            String responseMsg = connection.getResponseMessage();

                            int responsecode = connection.getResponseCode();

                            // connection.disconnect();

                            Log.i("code ", " " + responsecode);
                            Log.i("RESPONSE ", " " + responseMsg);


                            if (responsecode == 200) {
                                try {


                                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                                    StringBuffer jsonString = new StringBuffer();
                                    String linec2;
                                    while ((linec2 = br.readLine()) != null) {
                                        jsonString.append(linec2);
                                    }
                                    br.close();

                                    JSONObject resultof = new JSONObject(jsonString.toString());
                                    final String statusis = resultof.getString("status");


                                    if (resultof.getString("update") != null) {
                                        JSONObject resultofdetails = new JSONObject(resultof.getString("update"));

                                        if (resultofdetails.getString("msg").equalsIgnoreCase("This is not enough.")) {
                                            StyleableToast.makeText(getApplicationContext(), "Data successfully updated but its not enough for this task. " + responseMsg, Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                            SimpleDateFormat fmonth = new SimpleDateFormat("MM");
                                            SimpleDateFormat fday = new SimpleDateFormat("dd");
                                            final String themonth = fmonth.format(new Date());
                                            final String theday = fday.format(new Date());
                                            String newKey = mDatabase.child(user.getUid()).child("hackvent").child(theyearx).child(theday).child("values").push().getKey();

                                            final Map<String, Object> privUpdates = new HashMap<>();

                                            privUpdates.put("/task", taskis);
                                            privUpdates.put("/progress", resultofdetails.getString("value"));
                                            privUpdates.put("/values/" + newKey, userdata.toString());

                                            mDatabase.child(user.getUid()).child("hackvent").child(theyearx).child(theday).updateChildren(privUpdates);

                                            recreate();
                                        } else {
                                            SimpleDateFormat fmonth = new SimpleDateFormat("MM");
                                            SimpleDateFormat fday = new SimpleDateFormat("dd");
                                            final String themonth = fmonth.format(new Date());
                                            final String theday = fday.format(new Date());
                                            String newKey = mDatabase.child(user.getUid()).child("hackvent").child(theyearx).child(theday).child("values").push().getKey();

                                            final Map<String, Object> privUpdates = new HashMap<>();

                                            privUpdates.put("/task", taskis);
                                            privUpdates.put("/values/" + newKey, userdata.toString());
                                            privUpdates.put("/progress", resultofdetails.getString("value"));
                                            mDatabase.child(user.getUid()).child("hackvent").child(theyearx).child(theday).updateChildren(privUpdates);
                                            StyleableToast.makeText(getApplicationContext(), "Data successfully updated and Mission done! " + resultofdetails.getString("msg"), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                            recreate();
                                        }
                                    } else if (resultof.getString("update") == null) {
                                        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
                                        SimpleDateFormat fday = new SimpleDateFormat("dd");
                                        final String themonth = fmonth.format(new Date());
                                        final String theday = fday.format(new Date());

                                        final int thedayis = (Integer.parseInt(theday) - 1);
                                        mhackvent.child("users").child(theyearx).child(String.valueOf(thedayis)).addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(final DataSnapshot snapshotusers) {

                                                final Map<String, Object> privUpdatesusers = new HashMap<>();

                                                privUpdatesusers.put(AgentData.getagentname(), user.getUid());

                                                mhackvent.child("users").child(theyearx).child(String.valueOf(thedayis)).updateChildren(privUpdatesusers);


                                                String newKey = mDatabase.child(user.getUid()).child("hackvent").child(theyearx).child(theday).child("values").push().getKey();

                                                final Map<String, Object> privUpdates = new HashMap<>();

                                                privUpdates.put("/task", taskis);
                                                privUpdates.put("/values/" + newKey, userdata.toString());
                                                privUpdates.put("/progress", 0);
                                                mDatabase.child(user.getUid()).child("hackvent").child(theyearx).child(theday).updateChildren(privUpdates);
                                                StyleableToast.makeText(getApplicationContext(), "Data successfully sent. Status: " + statusis, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                recreate();

                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                            }
                                        });


                                    }

                                    Log.d("test", "result from server: " + resultof.toString());

                                } catch (IOException e) {

                                    e.printStackTrace();
                                }


                            } else if (responsecode == 401) {

                                StyleableToast.makeText(getApplicationContext(), "no payload " + responseMsg, Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            } else if (responsecode == 406) {
                                StyleableToast.makeText(getApplicationContext(), "No data found or data is too old. " + responseMsg, Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            } else {
                                StyleableToast.makeText(getApplicationContext(), "some other error #7785 " + responseMsg, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }

                            connection.disconnect();

                        } catch (Exception e) {

                            Log.e("Error = ", e.toString());
                        }


                    }
                } else {
                    //line = read.readLine(); //read next line if wish to continue parsing despite error

                }

                break;
            }


            reader.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private byte[] getRequestNonce(String data) {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        byte[] bytes = new byte[24];
        mRandom.nextBytes(bytes);
        try {
            byteStream.write(bytes);
            byteStream.write(data.getBytes());
        } catch (IOException e) {
            return null;
        }

        return byteStream.toByteArray();
    }

    public class MonthFinish extends CountDownTimer {
        final TextView deadlinetime_content = findViewById(R.id.content_deadline_time_month);
        final TextView runtime_date_content = findViewById(R.id.content_deadline_date_month);
        String serverUptimeText;

        public MonthFinish(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            content_request_btn_month.setEnabled(false);
            content_request_btn_month_manuell.setVisibility(View.GONE);
            content_request_btn_month.setText("Beendet");
            deadlinetime_content.setText("Mission Closed");
            runtime_date_content.setText("");
            //wrongscreenownescreen(linkid, isowner, "1");
        }

        @Override
        public void onTick(long millisUntilFinished) {

            Long timerup = millisUntilFinished / 1000;

            if (timerup / 86400 > 0) {
                serverUptimeText = String.format("%d d %d h %d min %d sec",
                        timerup / 86400,
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 > 0) {
                serverUptimeText = String.format("%d h %d min %d sec",
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 <= 0 && ((timerup % 86400) % 3600) / 60 > 0) {
                serverUptimeText = String.format("%d min %d sec",
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else {
                serverUptimeText = String.format("%d sec",
                        ((timerup % 86400) % 3600) % 60
                );
            }

            //theheadline.setTextColor(getResources().getColor(R.color.colorAccent));
            runtime_date_content.setText(serverUptimeText);
        }
    }

    public class WeekFinish extends CountDownTimer {
        final TextView deadlinetime_content_week = findViewById(R.id.content_deadline_time_week);
        final TextView runtime_date_content_week = findViewById(R.id.content_deadline_date_week);
        String serverUptimeText;

        public WeekFinish(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            content_request_btn_week.setEnabled(false);
            content_request_btn_week.setVisibility(View.GONE);
            content_request_btn_week.setText("Beendet");
            deadlinetime_content_week.setText("Mission Closed");
            runtime_date_content_week.setText("");
            //wrongscreenownescreen(linkid, isowner, "1");
        }

        @Override
        public void onTick(long millisUntilFinished) {

            Long timerup = millisUntilFinished / 1000;

            if (timerup / 86400 > 0) {
                serverUptimeText = String.format("%d d %d h %d min %d sec",
                        timerup / 86400,
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 > 0) {
                serverUptimeText = String.format("%d h %d min %d sec",
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 <= 0 && ((timerup % 86400) % 3600) / 60 > 0) {
                serverUptimeText = String.format("%d min %d sec",
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else {
                serverUptimeText = String.format("%d sec",
                        ((timerup % 86400) % 3600) % 60
                );
            }

            //theheadline.setTextColor(getResources().getColor(R.color.colorAccent));
            runtime_date_content_week.setText(serverUptimeText);
        }
    }

    public class InviteFinish extends CountDownTimer {

        String serverUptimeText;
        TextView pvp_counter;
        String thepvpkeydel, thepvpart, thetickart;
        FancyButton btntitle_start_btn_pvp, btntitle_details_btn_pvp;

        public InviteFinish(long millisInFuture, long countDownInterval, TextView counter, String pvpkey, String pvpart, String tickart, FancyButton title_start_btn_pvp, FancyButton title_details_btn_pvp) {
            super(millisInFuture, countDownInterval);
            pvp_counter = counter;
            thepvpkeydel = pvpkey;
            thepvpart = pvpart;
            thetickart = tickart;
            btntitle_details_btn_pvp = title_details_btn_pvp;
            btntitle_start_btn_pvp = title_start_btn_pvp;
        }

        @Override
        public void onFinish() {
            if (thetickart.equalsIgnoreCase("tojoin")) {
                pvp_counter.setText("PvP invitation has expired");
                btntitle_details_btn_pvp.setEnabled(false);
                btntitle_start_btn_pvp.setEnabled(false);

            } else if (thetickart.equalsIgnoreCase("tofinish")) {
                pvp_counter.setText("Your PvP time ended.. please wait now");
                //btntitle_start_btn_pvp.setVisibility(View.GONE);

                btntitle_details_btn_pvp.setEnabled(false);
                btntitle_start_btn_pvp.setEnabled(false);
            } else {
                pvp_counter.setText("You never started this PvP");

                btntitle_details_btn_pvp.setEnabled(false);
                btntitle_start_btn_pvp.setEnabled(false);
            }

            // Todo delete pvp
            if (pvp_counter.getVisibility() == View.VISIBLE) {
                if (listenerpvpjoin != null) {
                    mbpvp.removeEventListener(listenerpvpjoin);
                    listenerpvpjoin = null;
                }

                if (thetickart.equalsIgnoreCase("tojoin")) {
                    deletePvP(thepvpkeydel, thepvpart, false);
                } else if (thetickart.equalsIgnoreCase("tostart")) {

                    pvpnotstarted(thepvpkeydel, thepvpart, false);
                } else if (thetickart.equalsIgnoreCase("tofinish")) {
                    btntitle_details_btn_pvp.setEnabled(false);
                    Log.i("TICK----> ", " --> finish");
                    // TODO GET VALUE PROOF IF ZERO AND DO ACTION
                    mDatabaserunnungpvp.child(thepvpkeydel).child(user.getUid()).child("finished").setValue(true);
                    //  pvpnotstarted(thepvpkeydel, thepvpart, false);
                } else {
                    // TODO NEVER STARTED
                    btntitle_details_btn_pvp.setEnabled(false);
                    //  deletePvP(thepvpkeydel, thepvpart, false);
                }
            }
            //wrongscreenownescreen(linkid, isowner, "1");
        }

        @Override
        public void onTick(long millisUntilFinished) {

            Long timerup = millisUntilFinished / 1000;

            // Log.i("TICKART -->", " -- "+thetickart);

            if (timerup / 86400 > 0) {
                serverUptimeText = String.format("%d d %d h %d min %d sec",
                        timerup / 86400,
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 > 0) {
                serverUptimeText = String.format("%d h %d min %d sec",
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 <= 0 && ((timerup % 86400) % 3600) / 60 > 0) {
                serverUptimeText = String.format("%d min %d sec",
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else {
                serverUptimeText = String.format("%d sec",
                        ((timerup % 86400) % 3600) % 60
                );
            }

            if (thetickart.equalsIgnoreCase("tojoin")) {
                //theheadline.setTextColor(getResources().getColor(R.color.colorAccent));
                pvp_counter.setText("expires in " + serverUptimeText);
            } else if (thetickart.equalsIgnoreCase("tofinish")) {
                //theheadline.setTextColor(getResources().getColor(R.color.colorAccent));
                pvp_counter.setText("You have " + serverUptimeText + " for this PvP.");
                pvp_counter.setTextColor(getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));
            } else {
                pvp_counter.setText("You have to start in " + serverUptimeText);
                pvp_counter.setTextColor(getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));
            }
        }
    }
}

