package rocks.prxenon.aod.pages.pvp;


import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ShareCompat;
import androidx.core.widget.NestedScrollView;
import androidx.viewpager.widget.ViewPager;

import com.akexorcist.roundcornerprogressbar.TextRoundCornerProgressBar;
import com.github.siyamed.shapeimageview.OctogonImageView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.perf.metrics.AddTrace;
import com.muddzdev.styleabletoast.StyleableToast;

import org.apmem.tools.layouts.FlowLayout;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.BuildConfig;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.receiver.NetworkStateReceiver;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnungpvp;
import static rocks.prxenon.aod.basics.AODConfig.mbfriendsrequest;
import static rocks.prxenon.aod.basics.AODConfig.mbpvp;
import static rocks.prxenon.aod.basics.AODConfig.mbpvplinks;
import static rocks.prxenon.aod.basics.AODConfig.networkStateReceiver;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;
import static rocks.prxenon.aod.helper.GetNotifications.newagentsprofile;
import static rocks.prxenon.aod.helper.GetNotifications.newagentsprofileownrequest;


public class PvPDetails extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    // private static final String ALLOWED_CHARACTERS = AgentData.getagentname().toUpperCase();


    public static AlertDialog.Builder friendsbuilder;
    final List<String> pvptitle = new ArrayList();
    final List<Boolean> pvpstatus = new ArrayList();
    final List<String> pvpbadgeid = new ArrayList();
    final List<String> pvpbadgecat = new ArrayList();
    final List<String> pvpunite = new ArrayList();
    final List<String> pvpvalue = new ArrayList();
    final List<Long> pvpruntime = new ArrayList();

    public boolean hasdialogbefore;
    public AlertDialog ad;

    TextView missiontit;
    String[] friendnamenames;
    private Typeface type, proto;
    private TextView mTitleTextView;
    private ViewPager mPager;
    private AlertDialog.Builder dialogBuilder;
    private FrameLayout progressBarHolder_details;
    private CoordinatorLayout coordinator_details;
    private Bundle bundle;
    private CountDownTimer timer_pvp_own;
    private Activity context;

    private NestedScrollView pvp_result_details;


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);

        bundle = getIntent().getExtras();


        context = this;
        // set the view now
        setContentView(R.layout.activity_pvp_details);

        new AODConfig("default", this);
        //Get Firebase auth instance

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);


        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");


        progressBarHolder_details = findViewById(R.id.progressBarHolder_details);
        coordinator_details = findViewById(R.id.coordinator_details);

        pvp_result_details = findViewById(R.id.pvp_result_details);

        coordinator_details.setVisibility(View.GONE);
        progressBarHolder_details.setVisibility(View.VISIBLE);

        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_noaction, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText("PvP DETAILS");

        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);

        progressBarHolder_details.setVisibility(View.VISIBLE);
        new AgentData("allowback", true);

        if (bundle == null) {


            finish();

        } else {

            bindActivity();


            missiontit = findViewById(R.id.selectedtit);


            final RelativeLayout pvp_leftpart = findViewById(R.id.pvp_leftpart);
            final RelativeLayout headframe = findViewById(R.id.headframe);

            final TextView statustxt = findViewById(R.id.status_txt);

            final TextView pvp_art_text = findViewById(R.id.pvp_art_text);
            final ImageView title_price = findViewById(R.id.title_price);
            final TextView title_from_address = findViewById(R.id.title_from_address);
            final TextView pvp_id_text = findViewById(R.id.pvp_id_text);
            final CircleImageView ownerimage = findViewById(R.id.ownerimage);
            final TextView title_requests_count = findViewById(R.id.title_requests_count);
            final TextView startwertown = findViewById(R.id.content_from_address_1_week);
            final TextView startwertownsub = findViewById(R.id.content_from_address_2_week);
            final TextView content_to_address_1_week = findViewById(R.id.content_to_address_1_week);
            final TextView content_delivery_date_badge_week = findViewById(R.id.content_delivery_date_week);
            final TextView content_delivery_time_week = findViewById(R.id.content_delivery_time_week);


            if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                pvp_leftpart.setBackgroundColor(getResources().getColor(R.color.colorENL, getTheme()));
                headframe.setBackgroundColor(getResources().getColor(R.color.colorENL, getTheme()));
            } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                pvp_leftpart.setBackgroundColor(getResources().getColor(R.color.colorRES, getTheme()));
                headframe.setBackgroundColor(getResources().getColor(R.color.colorRES, getTheme()));
            } else {
                pvp_leftpart.setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                headframe.setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
            }

            pvp_id_text.setText("ID: " + bundle.getString("onactionid"));

            try {
                PicassoCache.getPicassoInstance(getApplicationContext())
                        .load(user.getProviderData().get(1).getPhotoUrl().toString())
                        .placeholder(R.drawable.full_logo)
                        .error(R.drawable.full_logo)


                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                        //  .networkPolicy(NetworkPolicy.NO_CACHE)
                        //.transform(new CropRoundTransformation())
                        .into(ownerimage);
                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
            } catch (Exception e) {
                //Log.e("Error", e.getMessage());

                e.printStackTrace();
            }
            /* if (bundle.getString("artis").equalsIgnoreCase("month")) { */

            // GET PVP DETAILS INFORMATION
            mbpvp.child(bundle.getString("onactionid")).addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(final DataSnapshot snapshotpvpdetails) {
                    statustxt.setText("get PvP details");
                    boolean pvpisactive;
                    boolean pvpisfinished;

                    // proof if active
                    pvpisactive = Boolean.parseBoolean(snapshotpvpdetails.child("active").getValue().toString());

                    pvpisfinished = Boolean.parseBoolean(snapshotpvpdetails.child("finished").getValue().toString());

                    // PVP IS AKTIVE AND NOT FINISHED
                    if (pvpisactive && !pvpisfinished) {
                        // GET AOD DATA
                        mDatabaseaod.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshotaod) {

                                statustxt.setText("get badge data");
                                int badgeweekid = getResources().getIdentifier(snapshotaod.child("badge").child(snapshotpvpdetails.child("badgeid").getValue().toString()).child("drawable").getValue().toString(), "drawable", getPackageName());
                                final String pvpshort1, pvpshort2, pvpfield;
                                //badgemonth = getResources().obtainTypedArray(badgemonthid);
                                if (badgeweekid != 0) {
                                    Log.w("BADGE -->", " id :" + badgeweekid);

                                    title_price.setImageDrawable(getDrawable(badgeweekid));
                                }

                                title_from_address.setText(snapshotaod.child("badge").child(snapshotpvpdetails.child("badgeid").getValue().toString()).child("title").getValue().toString());
                                String[] action_text2 = getResources().getStringArray(R.array.badge_text_actions_id);
                                int action_value2 = Arrays.asList(action_text2).indexOf(snapshotpvpdetails.child("badgeid").getValue().toString()); // pass value

                                String action_value_text_community2 = getResources().getStringArray(R.array.badge_text_actions_value)[action_value2];
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    pvp_art_text.setText(Html.fromHtml(String.format(getResources().getString(R.string.challange_txt_short_details), action_value_text_community2, snapshotaod.child("badge").child(snapshotpvpdetails.child("badgeid").getValue().toString()).child("short1").getValue().toString(), snapshotpvpdetails.child("runtime_text").getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                                } else {
                                    pvp_art_text.setText(Html.fromHtml(String.format(getResources().getString(R.string.challange_txt_short_details), action_value_text_community2, snapshotaod.child("badge").child(snapshotpvpdetails.child("badgeid").getValue().toString()).child("short1").getValue().toString(), snapshotpvpdetails.child("runtime_text").getValue().toString())));

                                }
                                pvpshort1 = snapshotaod.child("badge").child(snapshotpvpdetails.child("badgeid").getValue().toString()).child("short1").getValue().toString();
                                pvpshort2 = snapshotaod.child("badge").child(snapshotpvpdetails.child("badgeid").getValue().toString()).child("short2").getValue().toString();
                                pvpfield = snapshotaod.child("badge").child(snapshotpvpdetails.child("badgeid").getValue().toString()).child("field").getValue().toString();
                                final Map<String, Object> childpvpAgent = new HashMap<>();
                                final Map<String, Object> agentpvpstart = new HashMap<>();
                                final Map<String, Object> childUpdates = new HashMap<>();
                                final Map<String, Object> childMissionAgent = new HashMap<>();

                                // GET OWN DATA
                                mDatabaserunnungpvp.child(bundle.getString("onactionid")).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(final DataSnapshot snapshotowndata) {

                                        statustxt.setText("get own PvP data");
                                        boolean ownstarted;
                                        boolean ownfinished;

                                        if (bundle.getInt("finalTheresult") >= 0) {

                                            // OWN PVP STARTED
                                            if (Boolean.parseBoolean(snapshotowndata.child("started").getValue().toString())) {
                                                ownstarted = true;
                                                childpvpAgent.put("/startvalue", Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString()));
                                                agentpvpstart.put("/startvalue", Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString()));
                                            } else {
                                                ownstarted = false;
                                                childpvpAgent.put("/startvalue", bundle.getInt("finalTheresult"));
                                                agentpvpstart.put("/startvalue", bundle.getInt("finalTheresult"));
                                            }
                                            ownfinished = Boolean.parseBoolean(snapshotowndata.child("finished").getValue().toString());

                                            if (!ownstarted && !ownfinished) {
                                                // UPDATE DATA FIREBASE INIT DATA

                                                title_requests_count.setText("0");
                                                //agentpvpstart.put("/currentvalue", 0);
                                                agentpvpstart.put("/started", true);
                                                agentpvpstart.put("/starttime", System.currentTimeMillis());

                                                // agentpvpstart.put("/timestamp", System.currentTimeMillis());
                                                // agentpvpstart.put("/finished", false);
                                                agentpvpstart.put("/lastvalue", bundle.getInt("finalTheresult"));

                                                content_delivery_time_week.setText(String.format(Locale.getDefault(), "%,d", Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString())));

                                                // agentpvpstart.put("/pvpkey", thepvpkey);

                                                //  final String pushkey = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
                                                //  mDatabase.child(user.getUid()).child("pushkey").setValue(pushkey);

                                                Date oldDate = new Date(System.currentTimeMillis());
                                                Date currentDate = new Date();

                                                long diff = currentDate.getTime() - oldDate.getTime();
                                                long seconds = diff / 1000;
                                                long minutes = seconds / 60;
                                                long hours = minutes / 60;
                                                long days = hours / 24;

                                                if (oldDate.before(currentDate)) {


                                                    if (days >= 1) {
                                                        if (days == 1) {

                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_day), "" + days));
                                                        } else {
                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_days), "" + days));
                                                        }
                                                    } else if (hours >= 1) {
                                                        if (hours == 1) {
                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_hour), "" + hours));
                                                        } else if (hours > 1 && hours < 23) {
                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_hours), "" + hours));
                                                        } else {
                                                            content_delivery_date_badge_week.setText(getString(R.string.string_oneday));
                                                        }
                                                    } else if (minutes >= 0) {
                                                        if (minutes == 0) {
                                                            content_delivery_date_badge_week.setText(getString(R.string.string_justnow));
                                                        } else if (minutes >= 1 && minutes <= 5) {
                                                            content_delivery_date_badge_week.setText(getString(R.string.string_justnow));
                                                        } else {
                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_minutes), "" + minutes));
                                                        }
                                                    }
                                                } else {
                                                    content_delivery_date_badge_week.setText(getString(R.string.string_nouplaod));
                                                }

                                                mDatabaserunnungpvp.child(bundle.getString("onactionid")).child(user.getUid()).updateChildren(agentpvpstart).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Log.i("started pvp", " true");
                                                            String newKeypvp = mDatabase.child(user.getUid()).child("pvp").child(bundle.getString("onactionid")).push().getKey();
                                                            //new AgentData("allowback", true);

                                                            childpvpAgent.put("/value", bundle.getInt("finalTheresult"));
                                                            childpvpAgent.put("/timestamp", System.currentTimeMillis() / 1000);
                                                            mDatabase.child(user.getUid()).child("pvp").child(bundle.getString("onactionid")).child(newKeypvp).updateChildren(childpvpAgent).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Void> task) {
                                                                    if (task.isSuccessful()) {
                                                                        finish();

                                                                        Log.w("WUUPHUU ", this.getClass().getSimpleName() + " Gestartet");

                                                                    } else if (task.isCanceled()) {
                                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                                                        //  errorToast("Error", "10001");
                                                                    } else {
                                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                                                        //  errorToast(missionart, "10002");
                                                                    }
                                                                }

                                                            }).addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(Exception e) {
                                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                                                    //   errorToast(missionart, "10003");

                                                                    //signOut();
                                                                }
                                                            });


                                                            //  Log.w("ERROR ", this.getClass().getSimpleName() + " 5644 Error ");
                                                        } else if (task.isCanceled()) {

                                                        } else {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                                                        }
                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(Exception e) {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                                                        //signOut();
                                                    }
                                                });


                                                mbpvp.child(bundle.getString("onactionid")).addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(final DataSnapshot snapshotpvpdatatt) {
                                                        // long diffInMs = startpvptime + (Long.parseLong(snapshotpvpdata.child("runtime").getValue().toString()) * 1000) - System.currentTimeMillis();
                                                        long diffInMs = System.currentTimeMillis() + (Long.parseLong(snapshotpvpdatatt.child("runtime").getValue().toString()) * 1000) - System.currentTimeMillis();
                                                        Log.i("DIF-- > ", " " + diffInMs);
                                                        timer_pvp_own = new PvPDetails.InviteFinish(diffInMs, 1000, content_to_address_1_week, "pvp", "tofinish");
                                                        timer_pvp_own.start();
                                                        title_requests_count.setText(String.format(Locale.getDefault(), "%,d", Integer.parseInt(snapshotowndata.child("currentvalue").getValue().toString())));
                                                        startwertown.setText(String.format(Locale.getDefault(), "%,d", Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString())));
                                                    }


                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        //StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                        // coordinator_details.setVisibility(View.VISIBLE);
                                                        // progressBarHolder_details.setVisibility(View.GONE);
                                                    }
                                                });


                                                // finalTheresult snapshotowndata.child("startvalue").getValue().toString()
                                                startwertown.setText(String.format(Locale.getDefault(), "%,d", bundle.getInt("finalTheresult")));

                                                coordinator_details.setVisibility(View.VISIBLE);
                                                progressBarHolder_details.setVisibility(View.GONE);
                                                // END

                                            } else {

                                                int currentis;

                                                mbpvp.child(bundle.getString("onactionid")).addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(final DataSnapshot snapshotpvpdatatt) {
                                                        // long diffInMs = startpvptime + (Long.parseLong(snapshotpvpdata.child("runtime").getValue().toString()) * 1000) - System.currentTimeMillis();
                                                        long diffInMs = Long.parseLong(snapshotowndata.child("starttime").getValue().toString()) + (Long.parseLong(snapshotpvpdatatt.child("runtime").getValue().toString()) * 1000) - System.currentTimeMillis();
                                                        Log.i("DIF-- > ", " " + diffInMs);
                                                        timer_pvp_own = new PvPDetails.InviteFinish(diffInMs, 1000, content_to_address_1_week, "pvp", "tofinish");
                                                        timer_pvp_own.start();
                                                        title_requests_count.setText(String.format(Locale.getDefault(), "%,d", Integer.parseInt(snapshotowndata.child("currentvalue").getValue().toString())));
                                                        startwertown.setText(String.format(Locale.getDefault(), "%,d", Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString())));
                                                    }


                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        //StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                        // coordinator_details.setVisibility(View.VISIBLE);
                                                        // progressBarHolder_details.setVisibility(View.GONE);
                                                    }
                                                });


                                                Date oldDate = new Date(System.currentTimeMillis());
                                                Date currentDate = new Date();

                                                long diff = currentDate.getTime() - oldDate.getTime();
                                                long seconds = diff / 1000;
                                                long minutes = seconds / 60;
                                                long hours = minutes / 60;
                                                long days = hours / 24;

                                                if (oldDate.before(currentDate)) {

                                                    Log.e("oldDate", "is previous date");
                                                    Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                                                            + " hours: " + hours + " days: " + days);
                                                    if (days >= 1) {
                                                        if (days == 1) {

                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_day), "" + days));
                                                        } else {
                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_days), "" + days));
                                                        }
                                                    } else if (hours >= 1) {
                                                        if (hours == 1) {
                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_hour), "" + hours));
                                                        } else if (hours > 1 && hours < 23) {
                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_hours), "" + hours));
                                                        } else {
                                                            content_delivery_date_badge_week.setText(getString(R.string.string_oneday));
                                                        }
                                                    } else if (minutes >= 0) {
                                                        if (minutes == 0) {
                                                            content_delivery_date_badge_week.setText(getString(R.string.string_justnow));
                                                        } else if (minutes >= 1 && minutes <= 5) {
                                                            content_delivery_date_badge_week.setText(getString(R.string.string_justnow));
                                                        } else {
                                                            content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_minutes), "" + minutes));
                                                        }
                                                    }
                                                } else {
                                                    content_delivery_date_badge_week.setText(getString(R.string.string_nouplaod));
                                                }
                                                currentis = (bundle.getInt("finalTheresult") - Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString()));
                                                //childUpdates.put("/timestamp", System.currentTimeMillis() / 1000);
                                                childMissionAgent.put("/timestamp", System.currentTimeMillis() / 1000);
                                                agentpvpstart.put("/starttime", System.currentTimeMillis());
                                                childUpdates.put("/lastvalue", bundle.getInt("finalTheresult"));
                                                childUpdates.put("/currentvalue", currentis);
                                                childMissionAgent.put("/value", bundle.getInt("finalTheresult"));
                                                childMissionAgent.put("/startvalue", Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString()));

                                                content_delivery_time_week.setText(String.format(Locale.getDefault(), "%,d", bundle.getInt("finalTheresult")));

                                                title_requests_count.setText(String.format(Locale.getDefault(), "%,d", currentis));


                                                if (bundle.getInt("finalTheresult") > Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString()) && bundle.getInt("finalTheresult") > Integer.parseInt(snapshotowndata.child("lastvalue").getValue().toString())) {

                                                    mDatabaserunnungpvp.child(bundle.getString("onactionid")).child(user.getUid()).updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                String newKeypvp = mDatabase.child(user.getUid()).child("pvp").child(bundle.getString("onactionid")).push().getKey();
                                                                mDatabase.child(user.getUid()).child("pvp").child(bundle.getString("onactionid")).child(newKeypvp).updateChildren(childMissionAgent).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                        if (task.isSuccessful()) {
                                                                            finish();

                                                                            coordinator_details.setVisibility(View.VISIBLE);
                                                                            progressBarHolder_details.setVisibility(View.GONE);

                                                                        } else if (task.isCanceled()) {
                                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                                                            coordinator_details.setVisibility(View.VISIBLE);
                                                                            progressBarHolder_details.setVisibility(View.GONE);
                                                                        } else {
                                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                                                            coordinator_details.setVisibility(View.VISIBLE);
                                                                            progressBarHolder_details.setVisibility(View.GONE);
                                                                        }
                                                                    }

                                                                }).addOnFailureListener(new OnFailureListener() {
                                                                    @Override
                                                                    public void onFailure(Exception e) {
                                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                                                        coordinator_details.setVisibility(View.VISIBLE);
                                                                        progressBarHolder_details.setVisibility(View.GONE);

                                                                        //signOut();
                                                                    }
                                                                });
                                                            } else if (task.isCanceled()) {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10001");
                                                                coordinator_details.setVisibility(View.VISIBLE);
                                                                progressBarHolder_details.setVisibility(View.GONE);

                                                            } else {
                                                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10002");
                                                                coordinator_details.setVisibility(View.VISIBLE);
                                                                progressBarHolder_details.setVisibility(View.GONE);
                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 10003");
                                                            coordinator_details.setVisibility(View.VISIBLE);
                                                            progressBarHolder_details.setVisibility(View.GONE);

                                                            //signOut();
                                                        }
                                                    });
                                                } else {
                                                    // TODO NO UPDATE
                                                    coordinator_details.setVisibility(View.VISIBLE);
                                                    progressBarHolder_details.setVisibility(View.GONE);
                                                    finish();
                                                }

                                                // finalTheresult snapshotowndata.child("startvalue").getValue().toString()
                                                startwertown.setText(String.format(Locale.getDefault(), "%,d", Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString())));
                                            }
                                        } else {


                                            content_delivery_time_week.setText(String.format(Locale.getDefault(), "%,d", Integer.parseInt(snapshotowndata.child("lastvalue").getValue().toString())));

                                            Date oldDate = new Date((Long.parseLong(snapshotowndata.child("timestamp").getValue().toString())));
                                            Date currentDate = new Date();

                                            long diff = currentDate.getTime() - oldDate.getTime();
                                            long seconds = diff / 1000;
                                            long minutes = seconds / 60;
                                            long hours = minutes / 60;
                                            long days = hours / 24;

                                            if (oldDate.before(currentDate)) {

                                                Log.e("oldDate", "is previous date");
                                                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                                                        + " hours: " + hours + " days: " + days);
                                                if (days >= 1) {
                                                    if (days == 1) {

                                                        content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_day), "" + days));
                                                    } else {
                                                        content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_days), "" + days));
                                                    }
                                                } else if (hours >= 1) {
                                                    if (hours == 1) {
                                                        content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_hour), "" + hours));
                                                    } else if (hours > 1 && hours < 23) {
                                                        content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_hours), "" + hours));
                                                    } else {
                                                        content_delivery_date_badge_week.setText(getString(R.string.string_oneday));
                                                    }
                                                } else if (minutes >= 0) {
                                                    if (minutes == 0) {
                                                        content_delivery_date_badge_week.setText(getString(R.string.string_justnow));
                                                    } else if (minutes >= 1 && minutes <= 5) {
                                                        content_delivery_date_badge_week.setText(getString(R.string.string_justnow));
                                                    } else {
                                                        content_delivery_date_badge_week.setText(String.format(getString(R.string.string_ago_minutes), "" + minutes));
                                                    }
                                                }
                                            } else {
                                                content_delivery_date_badge_week.setText(getString(R.string.string_nouplaod));
                                            }

                                            mbpvp.child(bundle.getString("onactionid")).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(final DataSnapshot snapshotpvpdatatt) {
                                                    coordinator_details.setVisibility(View.VISIBLE);
                                                    progressBarHolder_details.setVisibility(View.GONE);
                                                    // long diffInMs = startpvptime + (Long.parseLong(snapshotpvpdata.child("runtime").getValue().toString()) * 1000) - System.currentTimeMillis();
                                                    long diffInMs = Long.parseLong(snapshotowndata.child("starttime").getValue().toString()) + (Long.parseLong(snapshotpvpdatatt.child("runtime").getValue().toString()) * 1000) - System.currentTimeMillis();
                                                    Log.i("DIF-- > ", " " + diffInMs);
                                                    timer_pvp_own = new PvPDetails.InviteFinish(diffInMs, 1000, content_to_address_1_week, "pvp", "tofinish");
                                                    timer_pvp_own.start();
                                                    title_requests_count.setText(String.format(Locale.getDefault(), "%,d", Integer.parseInt(snapshotowndata.child("currentvalue").getValue().toString())));
                                                    startwertown.setText(String.format(Locale.getDefault(), "%,d", Integer.parseInt(snapshotowndata.child("startvalue").getValue().toString())));
                                                }


                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    //StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                    coordinator_details.setVisibility(View.VISIBLE);
                                                    progressBarHolder_details.setVisibility(View.GONE);
                                                }
                                            });


                                        }


                                        startwertown.setSelected(true);
                                        startwertownsub.setText(pvpshort2);
                                        startwertownsub.setSelected(true);


                                        //progressBarHolder.setVisibility(View.VISIBLE);

                                    }


                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        //StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                        coordinator_details.setVisibility(View.VISIBLE);
                                        progressBarHolder_details.setVisibility(View.GONE);
                                    }
                                });

                                // END GET OWN DATA


                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                //StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                coordinator_details.setVisibility(View.VISIBLE);
                                progressBarHolder_details.setVisibility(View.GONE);
                            }
                        });


                        // START GEGNER
                        mbpvp.child(bundle.getString("onactionid")).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(final DataSnapshot snapshotpvpdatatt) {

                                final TextRoundCornerProgressBar pvp_progress_own = context.findViewById(R.id.pvp_progress_own);
                                final TextRoundCornerProgressBar pvp_progress_one = context.findViewById(R.id.pvp_progress_one);
                                final TextRoundCornerProgressBar pvp_progress_two = context.findViewById(R.id.pvp_progress_two);
                                final TextRoundCornerProgressBar pvp_progress_three = context.findViewById(R.id.pvp_progress_three);

                                View childown = pvp_progress_own;

                                final TextView textown = childown.findViewById(R.id.tv_progress);


                                // GET OWN DATA
                                mDatabaserunnungpvp.child(bundle.getString("onactionid")).child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(final DataSnapshot snapshotowndata) {


                                        if (Integer.parseInt(snapshotowndata.child("currentvalue").getValue().toString()) <= 0) {
                                            pvp_progress_own.setProgress(5);
                                            pvp_progress_own.setMax(100);
                                        }


                                        textown.setEllipsize(TextUtils.TruncateAt.END);
                                        textown.setMaxLines(1);
                                        textown.setSingleLine(true);
                                        textown.setSelected(true);
                                        pvp_progress_own.setProgressText(AgentData.getagentname());

                                        View child1 = pvp_progress_one;
                                        View child2 = pvp_progress_two;
                                        View child3 = pvp_progress_three;
                                        final TextView textone = child1.findViewById(R.id.tv_progress);
                                        final TextView texttwo = child2.findViewById(R.id.tv_progress);
                                        final TextView textthree = child3.findViewById(R.id.tv_progress);

                                        final RelativeLayout frameprogress = context.findViewById(R.id.frameprogress);

                                        //   LinearLayout a = new LinearLayout(this);


                                        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_pattern);
                                        BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

                                        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

                                        frameprogress.setBackgroundDrawable(bitmapDrawable);


                                        final FlowLayout myRootfriends = context.findViewById(R.id.my_friends_pvp);
                                        myRootfriends.removeAllViews();
                                        //   LinearLayout a = new LinearLayout(this);

                                        myRootfriends.setOrientation(LinearLayout.HORIZONTAL);

                                        Bitmap bmp2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_pattern);
                                        BitmapDrawable bitmapDrawable2 = new BitmapDrawable(bmp2);

                                        bitmapDrawable2.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

                                        myRootfriends.setBackgroundDrawable(bitmapDrawable2);

                                        myRootfriends.removeAllViews();
                                        myRootfriends.setOrientation(LinearLayout.HORIZONTAL);

                                        final LayoutInflater inflater = LayoutInflater.from(context);


                                        // set item content in view
                                        Display display = context.getWindowManager().getDefaultDisplay();
                                        Point size = new Point();
                                        display.getSize(size);
                                        final int newwidth = ((size.x) / 6) - 10;

                                        if (snapshotpvpdatatt.hasChild("/agents/1/agentname")) {
                                            View viewx = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                                            final OctogonImageView mybadge = viewx.findViewById(R.id.badgeimage);
                                            mybadge.getLayoutParams().width = newwidth;
                                            mybadge.getLayoutParams().height = newwidth;
                                            final TextView friendname = viewx.findViewById(R.id.friendname);

                                            friendname.setText(snapshotpvpdatatt.child("agents").child("1").child("agentname").getValue().toString());
                                            friendname.setSelected(true);

                                            try {
                                                PicassoCache.getPicassoInstance(context.getApplicationContext())
                                                        .load(snapshotpvpdatatt.child("agents").child("1").child("agentimage").getValue().toString() + "?sz=100")
                                                        .placeholder(R.drawable.full_logo)
                                                        .error(R.drawable.full_logo)

                                                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                                                        //.transform(new CropRoundTransformation())
                                                        .into(mybadge);
                                                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                            } catch (Exception e) {
                                                //Log.e("Error", e.getMessage());

                                                e.printStackTrace();
                                            }


                                            mybadge.setBorderWidth(4);
                                            mybadge.setBorderColor(context.getColor(R.color.colorLightBrown));

                                            mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                            mybadge.setPadding(8, 8, 8, 8);

                                            myRootfriends.addView(viewx);
                                        }

                                        if (snapshotpvpdatatt.hasChild("/agents/2/agentname")) {
                                            View viewx = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                                            final OctogonImageView mybadge = viewx.findViewById(R.id.badgeimage);
                                            mybadge.getLayoutParams().width = newwidth;
                                            mybadge.getLayoutParams().height = newwidth;
                                            final TextView friendname = viewx.findViewById(R.id.friendname);

                                            friendname.setText(snapshotpvpdatatt.child("agents").child("2").child("agentname").getValue().toString());
                                            friendname.setSelected(true);

                                            try {
                                                PicassoCache.getPicassoInstance(context.getApplicationContext())
                                                        .load(snapshotpvpdatatt.child("agents").child("3").child("agentimage").getValue().toString() + "?sz=100")
                                                        .placeholder(R.drawable.full_logo)
                                                        .error(R.drawable.full_logo)

                                                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                                                        //.transform(new CropRoundTransformation())
                                                        .into(mybadge);
                                                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                            } catch (Exception e) {
                                                //Log.e("Error", e.getMessage());

                                                e.printStackTrace();
                                            }


                                            mybadge.setBorderWidth(4);
                                            mybadge.setBorderColor(context.getColor(R.color.colorLightBrown));

                                            mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                            mybadge.setPadding(8, 8, 8, 8);

                                            myRootfriends.addView(viewx);
                                        }

                                        if (snapshotpvpdatatt.hasChild("/agents/3/agentname")) {
                                            View viewx = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                                            final OctogonImageView mybadge = viewx.findViewById(R.id.badgeimage);
                                            mybadge.getLayoutParams().width = newwidth;
                                            mybadge.getLayoutParams().height = newwidth;
                                            final TextView friendname = viewx.findViewById(R.id.friendname);

                                            friendname.setText(snapshotpvpdatatt.child("agents").child("3").child("agentname").getValue().toString());
                                            friendname.setSelected(true);

                                            try {
                                                PicassoCache.getPicassoInstance(context.getApplicationContext())
                                                        .load(snapshotpvpdatatt.child("agents").child("3").child("agentimage").getValue().toString() + "?sz=100")
                                                        .placeholder(R.drawable.full_logo)
                                                        .error(R.drawable.full_logo)

                                                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                                                        //.transform(new CropRoundTransformation())
                                                        .into(mybadge);
                                                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                            } catch (Exception e) {
                                                //Log.e("Error", e.getMessage());

                                                e.printStackTrace();
                                            }


                                            mybadge.setBorderWidth(4);
                                            mybadge.setBorderColor(context.getColor(R.color.colorLightBrown));

                                            mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                            mybadge.setPadding(8, 8, 8, 8);

                                            myRootfriends.addView(viewx);
                                        }


                                        mDatabaserunnungpvp.child(bundle.getString("onactionid")).addListenerForSingleValueEvent(new ValueEventListener() {
                                            long valone = 0;
                                            long valtwo = 0;
                                            long valthree = 0;
                                            int detailsagents = 1;
                                            long hoursbetween = 0;
                                            long hoursbetweenone = 0;
                                            long hoursbetweentwo = 0;
                                            long hoursbetweenthree = 0;
                                            long agentstartettimeone = 0;
                                            long agentstartettimetwo = 0;
                                            long agentstartettimethree = 0;
                                            long pvpruntime = Long.parseLong(snapshotpvpdatatt.child("runtime").getValue().toString()) * 1000;
                                            boolean pone = false;
                                            boolean ptwo = false;
                                            boolean pthree = false;

                                            ArrayList<Long> arrayListnums = new ArrayList<Long>();

                                            @Override
                                            public void onDataChange(final DataSnapshot snapshotrunningagentdetails) {

                                                final long agentstartown = Long.parseLong(snapshotrunningagentdetails.child(user.getUid()).child("starttime").getValue().toString());
                                                final boolean ownfinished = Boolean.parseBoolean(snapshotrunningagentdetails.child(user.getUid()).child("finished").getValue().toString());

                                                for (final DataSnapshot agentpvpdetails : snapshotrunningagentdetails.getChildren()) {

                                                    Log.i("DETAILS --> ", " pommes x12 " + agentpvpdetails.getKey() + " count = " + snapshotrunningagentdetails.getChildrenCount());

                                                    if (!agentpvpdetails.getKey().equalsIgnoreCase(user.getUid())) {


                                                        mDatabase.child(agentpvpdetails.getKey()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot snapshotagent) {
                                                                detailsagents++;
                                                                Log.i("ROUNDED -->", " num " + detailsagents);

                                                                if (detailsagents == 2) {
                                                                    valone = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("currentvalue").getValue().toString());
                                                                    pvp_progress_one.setProgressText(snapshotagent.getValue().toString());
                                                                    Log.i("ROUNDED 5667-->", " num " + valone);
                                                                    if (Boolean.parseBoolean(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("started").getValue().toString())) {
                                                                        long agentstartone = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("starttime").getValue().toString());

                                                                        hoursbetweenone = agentstartown - agentstartone;

                                                                        agentstartettimeone = (agentstartone + pvpruntime) - System.currentTimeMillis();


                                                                        Log.i("DIFF IS Finished -->", " c " + agentstartettimeone);
                                                                        if (hoursbetweenone >= -900000 && agentstartettimeone >= 450000) {
                                                                            pone = true;
                                                                            arrayListnums.add(valone);
                                                                            pvp_progress_one.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valone))) + ")");
                                                                        } else {
                                                                            valone = 5;
                                                                            arrayListnums.add(Long.parseLong("1"));
                                                                            if (!ownfinished) {
                                                                                pvp_progress_one.setProgressText(snapshotagent.getValue().toString() + "  (hidden)");
                                                                            } else {
                                                                                pvp_progress_one.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valone))) + ")");

                                                                            }
                                                                        }
                                                                    } else {
                                                                        valone = 0;
                                                                        arrayListnums.add(Long.parseLong("0"));
                                                                        pvp_progress_one.setProgressText(snapshotagent.getValue().toString() + "   (not started yet)");
                                                                    }
                                                                    textone.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                                                                    textone.setMarqueeRepeatLimit(-1);
                                                                    textone.setSingleLine(true);
                                                                    textone.setSelected(true);

                                                                } else if (detailsagents == 3) {
                                                                    valtwo = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("currentvalue").getValue().toString());
                                                                    pvp_progress_two.setProgressText(snapshotagent.getValue().toString());
                                                                    if (Boolean.parseBoolean(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("started").getValue().toString())) {
                                                                        long agentstarttwo = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("starttime").getValue().toString());

                                                                        hoursbetweentwo = agentstartown - agentstarttwo;


                                                                        agentstartettimetwo = (agentstarttwo + pvpruntime) - System.currentTimeMillis();


                                                                        Log.i("DIFF IS Finished -->", " c " + agentstartettimetwo);

                                                                        //  Log.i("DIFF IS -->", " c " + hoursbetweentwo);
                                                                        if (hoursbetweentwo >= -900000 && agentstartettimetwo >= 450000) {
                                                                            ptwo = true;
                                                                            arrayListnums.add(valtwo);
                                                                            pvp_progress_two.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valtwo))) + ")");
                                                                        } else {
                                                                            valtwo = 5;
                                                                            arrayListnums.add(Long.parseLong("1"));
                                                                            if (!ownfinished) {
                                                                                pvp_progress_two.setProgressText(snapshotagent.getValue().toString() + "  (hidden)");
                                                                            } else {
                                                                                pvp_progress_two.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valtwo))) + ")");

                                                                            }
                                                                        }
                                                                    } else {
                                                                        valtwo = 0;
                                                                        arrayListnums.add(Long.parseLong("0"));
                                                                        pvp_progress_two.setProgressText(snapshotagent.getValue().toString() + "   (not started yet)");
                                                                    }


                                                                    texttwo.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                                                                    texttwo.setMarqueeRepeatLimit(-1);
                                                                    texttwo.setSingleLine(true);
                                                                    texttwo.setSelected(true);

                                                                } else if (detailsagents == 4) {
                                                                    valthree = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("currentvalue").getValue().toString());
                                                                    pvp_progress_three.setProgressText(snapshotagent.getValue().toString());
                                                                    if (Boolean.parseBoolean(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("started").getValue().toString())) {
                                                                        long agentstartthree = Long.parseLong(snapshotrunningagentdetails.child(agentpvpdetails.getKey()).child("starttime").getValue().toString());

                                                                        hoursbetweenthree = agentstartown - agentstartthree;


                                                                        agentstartettimethree = (agentstartthree + pvpruntime) - System.currentTimeMillis();


                                                                        Log.i("DIFF IS Finished -->", " c " + agentstartettimetwo);

                                                                        //Log.i("DIFF IS three -->", " c " + hoursbetweenthree);
                                                                        if (hoursbetweenthree >= -900000 && agentstartettimethree >= 450000) {
                                                                            pthree = true;
                                                                            arrayListnums.add(valthree);
                                                                            pvp_progress_three.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valthree))) + ")");
                                                                        } else {
                                                                            valthree = 5;
                                                                            arrayListnums.add(Long.parseLong("1"));
                                                                            if (!ownfinished) {
                                                                                pvp_progress_three.setProgressText(snapshotagent.getValue().toString() + "  (hidden)");
                                                                            } else {
                                                                                pvp_progress_three.setProgressText(snapshotagent.getValue().toString() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(String.valueOf(valthree))) + ")");

                                                                            }
                                                                        }
                                                                    } else {
                                                                        valthree = 0;
                                                                        arrayListnums.add(Long.parseLong("0"));
                                                                        pvp_progress_three.setProgressText(snapshotagent.getValue().toString() + "   (not started yet)");
                                                                    }


                                                                    textthree.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                                                                    textthree.setMarqueeRepeatLimit(-1);
                                                                    textthree.setSingleLine(true);
                                                                    textthree.setSelected(true);

                                                                }

                                                                if (snapshotrunningagentdetails.getChildrenCount() == detailsagents) {

                                                                    if (detailsagents == 2) {

                                                                        pvp_progress_one.setVisibility(View.VISIBLE);


                                                                    } else if (detailsagents == 3) {
                                                                        pvp_progress_one.setVisibility(View.VISIBLE);
                                                                        pvp_progress_two.setVisibility(View.VISIBLE);
                                                                    } else if (detailsagents == 4) {
                                                                        pvp_progress_one.setVisibility(View.VISIBLE);
                                                                        pvp_progress_two.setVisibility(View.VISIBLE);
                                                                        pvp_progress_three.setVisibility(View.VISIBLE);
                                                                    }
                                                                    // Integer.parseInt(snapshotowndata.child("currentvalue").getValue().toString()
                                                                    arrayListnums.add(Long.parseLong(snapshotowndata.child("currentvalue").getValue().toString()));
                                                                    pvp_progress_own.setProgressText(pvp_progress_own.getProgressText() + "   (" + String.format(Locale.getDefault(), "%,d", Long.parseLong(snapshotowndata.child("currentvalue").getValue().toString())) + ")");
                                                                    Long maxvalueis = Collections.max(arrayListnums);

                                                                    Log.i("RUNNIMG -->", " pom " + maxvalueis + " c: " + arrayListnums.toString());
                                                                    if (maxvalueis <= 0) {
                                                                        Log.i("RUNNIMG -->", " pom2 " + detailsagents);
                                                                        pvp_progress_own.setProgress(1);
                                                                        pvp_progress_own.setMax(100);

                                                                        pvp_progress_one.setProgress(1);
                                                                        pvp_progress_one.setMax(100);

                                                                        pvp_progress_two.setProgress(1);
                                                                        pvp_progress_two.setMax(100);

                                                                        pvp_progress_three.setProgress(1);
                                                                        pvp_progress_three.setMax(100);
                                                                    } else {
                                                                        Log.i("RUNNIMG -->", " pom3 " + detailsagents);

                                                                        if (Long.parseLong(snapshotowndata.child("currentvalue").getValue().toString()) == maxvalueis) {
                                                                            int width = pvp_progress_own.getWidth();


                                                                            ValueAnimator animator = ValueAnimator.ofInt(0, 100);
                                                                            animator.setInterpolator(new LinearInterpolator());
                                                                            animator.setStartDelay(0);
                                                                            animator.setDuration(2_000);
                                                                            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                                @Override
                                                                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                    int value = (int) valueAnimator.getAnimatedValue();
                                                                                    pvp_progress_own.setProgress(value);
                                                                                }
                                                                            });

                                                                            animator.start();
                                                                            // pvp_progress_own.setProgress(100);
                                                                            pvp_progress_own.setMax(110);
                                                                        } else {
                                                                            float percentisown = (float) ((Long.parseLong(snapshotowndata.child("currentvalue").getValue().toString()) * 100) / maxvalueis);

                                                                            int width = pvp_progress_own.getWidth();


                                                                            ValueAnimator animator = ValueAnimator.ofInt(0, (int) percentisown);
                                                                            animator.setInterpolator(new LinearInterpolator());
                                                                            animator.setStartDelay(0);
                                                                            animator.setDuration(2_000);
                                                                            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                                @Override
                                                                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                    int value = (int) valueAnimator.getAnimatedValue();
                                                                                    pvp_progress_own.setProgress(value);
                                                                                }
                                                                            });

                                                                            animator.start();
                                                                            //  pvp_progress_own.setProgress(percentisown);
                                                                            pvp_progress_own.setMax(110);
                                                                        }


                                                                        if (valone == maxvalueis) {
                                                                            int width = pvp_progress_one.getWidth();


                                                                            ValueAnimator animator = ValueAnimator.ofInt(0, 100);
                                                                            animator.setInterpolator(new LinearInterpolator());
                                                                            animator.setStartDelay(0);
                                                                            animator.setDuration(2_000);
                                                                            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                                @Override
                                                                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                    int value = (int) valueAnimator.getAnimatedValue();
                                                                                    if (value > 35) {
                                                                                        textone.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                    }
                                                                                    pvp_progress_one.setProgress(value);
                                                                                }
                                                                            });

                                                                            animator.start();
                                                                            pvp_progress_one.setMax(110);
                                                                        } else {
                                                                            float percentisone = (float) ((valone * 100) / maxvalueis);
                                                                            int width = pvp_progress_one.getWidth();


                                                                            ValueAnimator animator = ValueAnimator.ofInt(0, (int) percentisone);
                                                                            animator.setInterpolator(new LinearInterpolator());
                                                                            animator.setStartDelay(0);
                                                                            animator.setDuration(2_000);
                                                                            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                                @Override
                                                                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                    int value = (int) valueAnimator.getAnimatedValue();
                                                                                    if (value > 35) {
                                                                                        textone.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                    }
                                                                                    pvp_progress_one.setProgress(value);
                                                                                }
                                                                            });

                                                                            animator.start();
                                                                            pvp_progress_one.setMax(110);
                                                                        }

                                                                        if (valtwo == maxvalueis) {
                                                                            int width = pvp_progress_two.getWidth();


                                                                            ValueAnimator animator = ValueAnimator.ofInt(0, 100);
                                                                            animator.setInterpolator(new LinearInterpolator());
                                                                            animator.setStartDelay(0);
                                                                            animator.setDuration(2_000);
                                                                            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                                @Override
                                                                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                    int value = (int) valueAnimator.getAnimatedValue();
                                                                                    if (value > 35) {
                                                                                        texttwo.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                    }
                                                                                    pvp_progress_two.setProgress(value);
                                                                                }
                                                                            });

                                                                            animator.start();
                                                                            pvp_progress_two.setMax(110);
                                                                        } else {
                                                                            float percentistwo = (float) ((valtwo * 100) / maxvalueis);
                                                                            int width = pvp_progress_two.getWidth();


                                                                            ValueAnimator animator = ValueAnimator.ofInt(0, (int) percentistwo);
                                                                            animator.setInterpolator(new LinearInterpolator());
                                                                            animator.setStartDelay(0);
                                                                            animator.setDuration(2_000);
                                                                            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                                @Override
                                                                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                    int value = (int) valueAnimator.getAnimatedValue();
                                                                                    if (value > 35) {
                                                                                        texttwo.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                    }
                                                                                    pvp_progress_two.setProgress(value);
                                                                                }
                                                                            });

                                                                            animator.start();
                                                                            pvp_progress_two.setMax(110);
                                                                        }

                                                                        if (valthree == maxvalueis) {
                                                                            int width = pvp_progress_three.getWidth();


                                                                            ValueAnimator animator = ValueAnimator.ofInt(0, 100);
                                                                            animator.setInterpolator(new LinearInterpolator());
                                                                            animator.setStartDelay(0);
                                                                            animator.setDuration(2_000);
                                                                            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                                @Override
                                                                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                    int value = (int) valueAnimator.getAnimatedValue();
                                                                                    if (value > 35) {
                                                                                        textthree.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                    }
                                                                                    pvp_progress_three.setProgress(value);
                                                                                }
                                                                            });

                                                                            animator.start();
                                                                            pvp_progress_three.setMax(110);
                                                                        } else {
                                                                            float percentisthree = (float) ((valthree * 100) / maxvalueis);
                                                                            int width = pvp_progress_three.getWidth();


                                                                            ValueAnimator animator = ValueAnimator.ofInt(0, (int) percentisthree);
                                                                            animator.setInterpolator(new LinearInterpolator());
                                                                            animator.setStartDelay(0);
                                                                            animator.setDuration(2_000);
                                                                            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                                                                @Override
                                                                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                                                    int value = (int) valueAnimator.getAnimatedValue();
                                                                                    if (value > 35) {
                                                                                        textthree.setTextColor(getResources().getColor(R.color.colorPrimaryDark, getTheme()));
                                                                                    }
                                                                                    pvp_progress_three.setProgress(value);
                                                                                }
                                                                            });

                                                                            animator.start();
                                                                            pvp_progress_three.setMax(110);
                                                                        }
                                                                    }

                                                                    detailsagents = 1;
                                                                }


                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                            }
                                                        });
                                                    }

                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                //  StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                            }
                                        });
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        //StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });

                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                //StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            }
                        });


                    } else {
                        // TODO IS NOT ACTIVE OR FINISHED TEXT OR POST
                        finish();
                    }

                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    //StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                }
            });


        }

    }

    private void bindActivity() {
        Log.i("Bind", "PvPDetails");

    }


    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();
        pref = getSharedPreferences("AppPref", MODE_PRIVATE);

        // networkStateReceiver = new NetworkStateReceiver();
        // networkStateReceiver.addListener(this);
        // registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


        coordinator_details.setVisibility(View.GONE);
        progressBarHolder_details.setVisibility(View.VISIBLE);


        new AgentData("allowback", true);
        // jointhepvp(frominboxpvpid, getApplicationContext(), PvPDetails.this, inboxid);

        //  findViewById(R.id.pvp_frame).setVisibility(View.VISIBLE);


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");

        //pref.edit().remove("scanedagnet").apply();

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (timer_pvp_own != null) {
            timer_pvp_own.cancel();
        }

        if (newagentsprofile != null) {
            mbfriendsrequest.removeEventListener(newagentsprofile);

        }

        if (newagentsprofileownrequest != null) {
            mDatabase.removeEventListener(newagentsprofileownrequest);

        }

        if (friendsbuilder != null) {
            hasdialogbefore = true;
            friendsbuilder.create().dismiss();
            friendsbuilder = null;
        }

        finish();


    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (newagentsprofile != null) {
            mbfriendsrequest.removeEventListener(newagentsprofile);

        }

        if (newagentsprofileownrequest != null) {
            mDatabase.removeEventListener(newagentsprofileownrequest);

        }

        if (timer_pvp_own != null) {
            timer_pvp_own.cancel();
        }

        finish();


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (timer_pvp_own != null) {
            timer_pvp_own.cancel();
        }

        try {
            if (networkStateReceiver != null) {
                this.unregisterReceiver(networkStateReceiver);
                Log.i("", "broadcastReceiver unregistered");
            }
        } catch (Exception e) {
            Log.i("", "broadcastReceiver is already unregistered");
            networkStateReceiver = null;
        }

        finish();


    }

    @Override
    public void networkAvailable() {


        // coordinator_details.setVisibility(View.VISIBLE);
        // progressBarHolder_details.setVisibility(View.GONE);
        TextView statustxt = findViewById(R.id.status_txt);
        statustxt.setText(getResources().getString(R.string.string_pleaswait));

        /* TODO: Your connection-oriented stuff here */
    }

    @Override
    public void networkUnavailable() {


        coordinator_details.setVisibility(View.GONE);
        progressBarHolder_details.setVisibility(View.VISIBLE);
        TextView statustxt = findViewById(R.id.status_txt);
        statustxt.setText("offline, please re-connect to a network");
        /* TODO: Your disconnection-oriented stuff here */
    }


    @Override
    public void onBackPressed() {


        if (!AgentData.shouldAllowBack) {
            //doSomething();
        } else {
            new AgentData("allowback", true);
            super.onBackPressed();
        }


    }


    private String getCharForNumber(int i) {
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        Log.i("NUM2 -->", ":" + i);
        if (i > 25) {
            return null;
        }
        return Character.toString(alphabet[i]);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveandsharePvP(final String pvpkey, final Map<String, Object> pvpupdate, TextView textinfo, FloatingActionButton fabsave, FloatingActionButton fabreset, AlertDialog.Builder friendsbuilder, AlertDialog ad, NestedScrollView pvp_frame, NestedScrollView pvp_result, final Activity context) {
        pvpupdate.put("/active", false);
        Log.i("Data agents --> ", pvpupdate.toString());


        progressBarHolder_details = context.findViewById(R.id.progressBarHolder_details);
        progressBarHolder_details.setVisibility(View.VISIBLE);
        // pvp_result.setVisibility(View.GONE);
        final TextView statustxt = context.findViewById(R.id.status_txt);
        //statustxt.setText(context.getResources().getString(R.string.string_pleaswait));
        new AgentData("allowback", false);
        statustxt.setText(context.getResources().getString(R.string.pvp_creation_text));

        mbpvp.child(pvpkey).updateChildren(pvpupdate).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    // LINK ERSTELLEN
                    statustxt.setText(context.getResources().getString(R.string.pvp_creation_link_text));

                    final Map<String, Object> agentcreator = new HashMap<>();


                    agentcreator.put("/currentvalue", 0);
                    agentcreator.put("/started", false);
                    agentcreator.put("/owner", true);

                    agentcreator.put("/googleid", user.getProviderData().get(1).getUid());
                    agentcreator.put("/timestamp", System.currentTimeMillis());
                    agentcreator.put("/lastupdate", System.currentTimeMillis());
                    agentcreator.put("/finished", false);
                    agentcreator.put("/lastvalue", 0);
                    agentcreator.put("/startvalue", 0);
                    agentcreator.put("/pvpkey", pvpkey);
                    //String.valueOf(System.currentTimeMillis()
                    mDatabaserunnungpvp.child(pvpkey).child(user.getUid()).updateChildren(agentcreator);


                    mbpvp.child(pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot snapshotinviteagents) {

                            mDatabase.child(user.getUid()).child("pvp").child(pvpkey).child("timestamp").setValue((System.currentTimeMillis())).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                        // Log.w("ERROR ", this.getClass().getSimpleName() + " 5644 Error ");
                                        mDatabase.child(user.getUid()).child("pvp").child(pvpkey).child("finished").setValue(false);

                                        // IS WITH AGENTS
                                        if (snapshotinviteagents.child("art").getValue().toString().equalsIgnoreCase("invites")) {
                                            long anzahlagents = snapshotinviteagents.child("agents").getChildrenCount();
                                            int looperanzahl = 1;
                                            String pushkey = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
                                            mDatabase.child(user.getUid()).child("pushkey").setValue(pushkey);
                                            for (int i = 1; i <= anzahlagents; ++i) {

                                                String sendto = snapshotinviteagents.child("agents").child(String.valueOf(i)).child("uuid").getValue().toString();
                                                new SendPushinBginvite(pushkey, "pvpinvite", AgentData.getagentname(), sendto, snapshotinviteagents.child("pvptitle").getValue().toString(), snapshotinviteagents.child("runtime_text").getValue().toString()).execute();
                                                statustxt.setText("invite send to Agent " + snapshotinviteagents.child("agents").child(String.valueOf(i)).child("agentname").getValue().toString());
                                                final Map<String, Object> agentinbox = new HashMap<>();


                                                agentinbox.put("/text", AgentData.getagentname());
                                                agentinbox.put("/link", pvpkey);
                                                agentinbox.put("/inviteposition", i);

                                                agentinbox.put("/title", "PvP invite");
                                                agentinbox.put("/timestamp", System.currentTimeMillis());
                                                agentinbox.put("/read", false);

                                                agentinbox.put("/fromuid", user.getUid());
                                                mDatabase.child(snapshotinviteagents.child("agents").child(String.valueOf(i)).child("uuid").getValue().toString()).child("newinbox").child(String.valueOf(System.currentTimeMillis())).updateChildren(agentinbox);


                                                if (i == anzahlagents) {
                                                    Log.i("INVITE DONE", " true");
                                                    statustxt.setText("all done...");
                                                    //new AgentData("allowback", true);

                                                    AlertDialog.Builder confirmdialog = new AlertDialog.Builder(context, R.style.CustomDialog);

                                                    // builder.setMessage("Text not finished...");
                                                    LayoutInflater inflater = context.getLayoutInflater();
                                                    View dialog = inflater.inflate(R.layout.dialog_pvp_created, null);
                                                    confirmdialog.setView(dialog);

                                                    TextView agentname = dialog.findViewById(R.id.agentname);
                                                    agentname.setText("PvP created successfully.\n\nNote: You can start as soon as an agent joins the PvP.");

                                                    confirmdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            context.finish();
                                                            //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                                        }
                                                    });


                                                    confirmdialog.setCancelable(false);

                                                    if (!isFinishing()) {
                                                        confirmdialog.create().show();

                                                    }


                                                }


                                            }


                                        }

                                        // IS WITH LINK
                                        else {


                                            statustxt.setText("Link creating...");
                                            final Map<String, Object> pvplinking = new HashMap<>();
                                            pvplinking.put("/link", "AODs_" + pvpkey);
                                            pvplinking.put("/agents", 0);

                                            pvplinking.put("/title", snapshotinviteagents.child("pvptitle").getValue().toString());
                                            pvplinking.put("/pvpid", snapshotinviteagents.child("pvpid").getValue().toString());
                                            pvplinking.put("/badgeid", snapshotinviteagents.child("badgeid").getValue().toString());
                                            pvplinking.put("/badgecat", snapshotinviteagents.child("badgecat").getValue().toString());
                                            pvplinking.put("/createrimage", snapshotinviteagents.child("createrimage").getValue().toString());
                                            pvplinking.put("/timestamp", System.currentTimeMillis());
                                            pvplinking.put("/createruuid", user.getUid());
                                            mbpvplinks.child(snapshotinviteagents.child("pvplink").getValue().toString()).updateChildren(pvplinking).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        Log.i("Link crated", " true");
                                                        statustxt.setText("all done...");
                                                        //new AgentData("allowback", true);

                                                        AlertDialog.Builder confirmdialog = new AlertDialog.Builder(context, R.style.CustomDialog);

                                                        // builder.setMessage("Text not finished...");
                                                        LayoutInflater inflater = context.getLayoutInflater();
                                                        View dialog = inflater.inflate(R.layout.dialog_pvp_created, null);
                                                        confirmdialog.setView(dialog);

                                                        TextView agentname = dialog.findViewById(R.id.agentname);
                                                        agentname.setText("PvP Link created successfully. Share the link now with other agnets\n\nNote: You can start as soon as an agent joins the PvP.");

                                                        confirmdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                Intent shareIntent = ShareCompat.IntentBuilder.from(context)
                                                                        .setText("PvP invite from " + AgentData.getagentname() + "\nhttps://aod.prxenon.rocks/pvp/AODs_" + pvpkey)
                                                                        .setChooserTitle("Share this PvP with...")
                                                                        .setType("text/plain")
                                                                        .getIntent()
                                                                        .setAction(Intent.ACTION_SEND);

                                                                startActivityForResult(shareIntent, 393);

                                                                //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                                            }
                                                        });
                                                        confirmdialog.setNegativeButton("Copy Link and close", new DialogInterface.OnClickListener() {

                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                                                                ClipData clip = ClipData.newPlainText("PvP invite link", "https://aod.prxenon.rocks/pvp/AODs_" + snapshotinviteagents.child("pvpid").getValue().toString());
                                                                clipboard.setPrimaryClip(clip);

                                                                StyleableToast.makeText(getApplicationContext(), "PvP invite link copied to clipboard", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                context.finish();
                                                                //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                                            }
                                                        });


                                                        confirmdialog.setCancelable(true);

                                                        if (!isFinishing()) {
                                                            confirmdialog.create().show();

                                                        }

                                                        //  Log.w("ERROR ", this.getClass().getSimpleName() + " 5644 Error ");
                                                    } else if (task.isCanceled()) {

                                                    } else {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                                                    }
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                                                    //signOut();
                                                }
                                            });


                                        }


                                    } else if (task.isCanceled()) {

                                    } else {
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                                    }
                                }


                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(Exception e) {
                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                                    //signOut();
                                }
                            });


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.i("DATA ERROR", databaseError.getMessage());
                        }
                    });

                    // ENDE LINK ERSTELLEN


                } else if (task.isCanceled()) {
                    Log.w("ERROR ", context.getClass().getSimpleName() + " 90441");

                } else {
                    Log.w("ERROR ", context.getClass().getSimpleName() + " 90552");

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w("ERROR ", context.getClass().getSimpleName() + " 90663");


                //signOut();
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 393) {
            System.out.println("Share done");
            finish();

        }
    }

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    //GET AGENTS DETAILS WITH TOKEN
    private class SendPushinBginvite extends AsyncTask<String, Void, String> {
        //sendpushaccepted
        String thepushid, theart, theagent, sendtoa, titleof, runtimetext;

        public SendPushinBginvite(String s, String art, String agentname, String sendTo, String titleofd, String runtimetextd) {
            thepushid = s;
            theart = art;
            theagent = agentname;
            sendtoa = sendTo;
            titleof = titleofd;
            runtimetext = runtimetextd;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {


            String regurl = BuildConfig.BASEURL + "sendpush_pvp_invite.php?pw=" + thepushid + "&art=" + theart + "&agent=" + theagent + "&uid=" + user.getUid() + "&to=" + sendtoa + "&title=" + titleof + "&runtimetext=" + runtimetext;


            StringBuilder sb = null;
            BufferedReader reader = null;
            String serverResponse = null;
            try {

                URL url = new URL(regurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(200);
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                //Log.e("statusCode", "" + statusCode);
                if (statusCode == 200) {
                    sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                }

                connection.disconnect();
                if (sb != null)
                    serverResponse = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return serverResponse;
        }

        @Override
        protected void onPostExecute(String json) {

            if (json != null) {

                Log.i("RESULT Push", " -->" + json);
            }
        }
    }

    //GET AGENTS DETAILS WITH TOKEN
    private class SendPushinBgaccept extends AsyncTask<String, Void, String> {
        //sendpushaccepted
        String thepushid, theart, theagent, sendtoa, titleof, runtimetext;

        public SendPushinBgaccept(String s, String art, String agentname, String sendTo, String titleofd, String runtimetextd) {
            thepushid = s;
            theart = art;
            theagent = agentname;
            sendtoa = sendTo;
            titleof = titleofd;
            runtimetext = runtimetextd;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {


            String regurl = BuildConfig.BASEURL + "sendpush_pvp_accept.php?pw=" + thepushid + "&art=" + theart + "&agent=" + theagent + "&uid=" + user.getUid() + "&to=" + sendtoa + "&title=" + titleof + "&runtimetext=" + runtimetext;


            StringBuilder sb = null;
            BufferedReader reader = null;
            String serverResponse = null;
            try {

                URL url = new URL(regurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(200);
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                //Log.e("statusCode", "" + statusCode);
                if (statusCode == 200) {
                    sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                }

                connection.disconnect();
                if (sb != null)
                    serverResponse = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return serverResponse;
        }

        @Override
        protected void onPostExecute(String json) {

            if (json != null) {

                Log.i("RESULT Push", " -->" + json);
            }
        }
    }


    public class InviteFinish extends CountDownTimer {

        String serverUptimeText;
        TextView pvp_counter;
        String thepvpart, thetickart;

        // diffInMs, 1000, content_to_address_1_week, "pvp", "tofinish"
        public InviteFinish(long millisInFuture, long countDownInterval, TextView counter, String pvpart, String tickart) {
            super(millisInFuture, countDownInterval);
            pvp_counter = counter;

            thepvpart = pvpart;
            thetickart = tickart;

        }

        @Override
        public void onFinish() {
            if (thetickart.equalsIgnoreCase("tofinish")) {
                pvp_counter.setText("PvP ended");

                // TODO SCORE
                //btntitle_start_btn_pvp.setVisibility(View.GONE);

            }


            //wrongscreenownescreen(linkid, isowner, "1");
        }

        @Override
        public void onTick(long millisUntilFinished) {

            Long timerup = millisUntilFinished / 1000;

            // Log.i("TICKART -->", " -- "+thetickart);

            if (timerup / 86400 > 0) {
                serverUptimeText = String.format("%d d %d h %d min %d sec",
                        timerup / 86400,
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 > 0) {
                serverUptimeText = String.format("%d h %d min %d sec",
                        (timerup % 86400) / 3600,
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else if (timerup / 86400 <= 0 && (timerup % 86400) / 3600 <= 0 && ((timerup % 86400) % 3600) / 60 > 0) {
                serverUptimeText = String.format("%d min %d sec",
                        ((timerup % 86400) % 3600) / 60,
                        ((timerup % 86400) % 3600) % 60
                );
            } else {
                serverUptimeText = String.format("%d sec",
                        ((timerup % 86400) % 3600) % 60
                );
            }

            if (thetickart.equalsIgnoreCase("tofinish")) {
                //theheadline.setTextColor(getResources().getColor(R.color.colorAccent));
                pvp_counter.setText("" + serverUptimeText);
                pvp_counter.setTextColor(getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));
            }
        }
    }
}