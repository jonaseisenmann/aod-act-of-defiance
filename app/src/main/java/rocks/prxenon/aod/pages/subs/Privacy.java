package rocks.prxenon.aod.pages.subs;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;


public class Privacy extends AppCompatActivity {

    private WebView mWebView;
    private TextView mTitleTextView;
    private Typeface type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.privacy_activity);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        setupActionBar();
        mTitleTextView = findViewById(R.id.textfield);

        String largeTextString = new AODConfig(getApplicationContext()).getStringFromRawRes(R.raw.privacy);
        if (largeTextString != null) {  //null check is optional
            mTitleTextView.setText(largeTextString);
        } else {
            //something goes wrong
        }

    }


    private void setupActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_settings, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText("Privacy Policy");
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(25);

    }

}