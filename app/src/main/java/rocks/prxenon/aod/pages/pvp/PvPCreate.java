package rocks.prxenon.aod.pages.pvp;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ShareCompat;
import androidx.core.widget.NestedScrollView;
import androidx.viewpager.widget.ViewPager;

import com.github.kmenager.materialanimatedswitch.MaterialAnimatedSwitch;
import com.github.siyamed.shapeimageview.OctogonImageView;
import com.goodiebag.horizontalpicker.HorizontalPicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.perf.metrics.AddTrace;
import com.muddzdev.styleabletoast.StyleableToast;

import org.apmem.tools.layouts.FlowLayout;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.BuildConfig;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.FriendsSelectAdapter;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.pages.AODMain;
import rocks.prxenon.aod.receiver.NetworkStateReceiver;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnungpvp;
import static rocks.prxenon.aod.basics.AODConfig.mbfriendsrequest;
import static rocks.prxenon.aod.basics.AODConfig.mbpvp;
import static rocks.prxenon.aod.basics.AODConfig.mbpvplinks;
import static rocks.prxenon.aod.basics.AODConfig.mdbroot;
import static rocks.prxenon.aod.basics.AODConfig.networkStateReceiver;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;
import static rocks.prxenon.aod.helper.GetNotifications.newagentsprofile;
import static rocks.prxenon.aod.helper.GetNotifications.newagentsprofileownrequest;


public class PvPCreate extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    // private static final String ALLOWED_CHARACTERS = AgentData.getagentname().toUpperCase();


    public static AlertDialog.Builder friendsbuilder;
    final List<String> pvptitle = new ArrayList();
    final List<Boolean> pvpstatus = new ArrayList();
    final List<String> pvpbadgeid = new ArrayList();
    final List<String> pvpbadgecat = new ArrayList();
    final List<String> pvpunite = new ArrayList();
    final List<String> pvpvalue = new ArrayList();
    final List<Long> pvpruntime = new ArrayList();
    final List<HorizontalPicker.PickerItem> pvpicon = new ArrayList();
    final List<HorizontalPicker.PickerItem> pvpruntimetext = new ArrayList();
    public boolean hasdialogbefore;
    public AlertDialog ad;
    HorizontalPicker.OnSelectionChangeListener listener, listener2;
    HorizontalPicker hpImage;
    HorizontalPicker hpruntime;
    TextView missiontit, selectedruntime;
    String[] friendnamenames;
    private Typeface type, proto;
    private TextView mTitleTextView;
    private ViewPager mPager;
    private AlertDialog.Builder dialogBuilder;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;
    private FloatingActionButton fabreset, fabsave, fabinvite, fabjoin, fabreject;
    private RelativeLayout runtimeframe, againframe, thejoinframe, theinviteframe;
    private Map<String, Object> pvpupdate = new HashMap<>();
    private ListView lv;
    private FriendsSelectAdapter friendsselectadapter;
    private int friendscount = 0;
    private TextView textinfo;
    private NestedScrollView pvp_frame, pvp_result;
    private boolean fromprofile, frominbox, fromlink;
    private String fromprofileid, fromprofileagentname, frominboxpvpid, inboxid;


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_pvp_create);

        bindActivity();
        new AODConfig("default", this);
        //Get Firebase auth instance

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);


        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("agentidpvp")) {
            // intent is not null and your key is not null
            fromlink = false;
            frominbox = false;
            fromprofile = true;
            fromprofileid = getIntent().getExtras().getString("agentidpvp");
            fromprofileagentname = getIntent().getExtras().getString("agentnamepvp");
            inboxid = "null";
        } else if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("nofriends")) {
            // intent is not null and your key is not null
            fromlink = false;
            frominbox = false;
            fromprofile = true;
            fromprofileid = getIntent().getExtras().getString("nofriends");
            fromprofileagentname = getIntent().getExtras().getString("nofriends");
            inboxid = "null";
        } else if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("accepted")) {
            // intent is not null and your key is not null
            if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("fromlink")) {
                fromprofile = false;
                frominbox = false;
                fromlink = true;
                frominboxpvpid = getIntent().getExtras().getString("pvpkey");
                fromprofileid = "null";
                fromprofileagentname = "null";
                inboxid = "null";
            } else {
                fromlink = false;
                fromprofile = false;
                frominbox = true;
                frominboxpvpid = getIntent().getExtras().getString("pvpkey");
                fromprofileid = "null";
                fromprofileagentname = "null";
                inboxid = getIntent().getExtras().getString("inboxid");
            }

        } else {
            fromlink = false;
            frominbox = false;
            fromprofile = false;
            fromprofileid = "null";
            fromprofileagentname = "null";
            inboxid = "null";
        }
        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);
        pvp_frame = findViewById(R.id.pvp_frame);
        pvp_result = findViewById(R.id.pvp_result);


        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_noaction, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(R.string.string_new_pvp_tit);
        runtimeframe = findViewById(R.id.runtimeframe);
        againframe = findViewById(R.id.againframe);
        textinfo = findViewById(R.id.textinfo);
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);

        hpImage = findViewById(R.id.hpImage);
        hpruntime = findViewById(R.id.hpruntimetext);
        missiontit = findViewById(R.id.selectedtit);
        selectedruntime = findViewById(R.id.selectedruntime);
        fabreset = findViewById(R.id.fabreset);
        fabsave = findViewById(R.id.fabsave);
        fabinvite = findViewById(R.id.fabinvite);
        fabjoin = findViewById(R.id.fabjoin);
        fabreject = findViewById(R.id.fabreject);
        thejoinframe = findViewById(R.id.thejoinframe);
        theinviteframe = findViewById(R.id.theinviteframe);

        fabinvite.hide();
        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
            fabsave.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.colorENL, getApplicationContext().getTheme()));
        } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
            fabsave.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.colorRES, getApplicationContext().getTheme()));
        } else {
            fabsave.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.colorPrimaryYellowDark, getApplicationContext().getTheme()));
        }
        fabreset.hide();
        fabsave.hide();
        textinfo.setVisibility(View.GONE);

        fabreset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pvpupdate.clear();
                //  fromprofileid = getIntent().getExtras().getString("agentidpvp");
                //  fromprofileagentname = getIntent().getExtras().getString("agentnamepvp");
                getIntent().removeExtra("agentidpvp");
                getIntent().removeExtra("agentnamepvp");
                // getIntent().setData(null);
                recreate();
            }
        });

        new AgentData("allowback", true);

    }

    private void bindActivity() {


    }


    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();
        pref = getSharedPreferences("AppPref", MODE_PRIVATE);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));


        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);

        runtimeframe.setVisibility(View.GONE);
        pvp_result.setVisibility(View.GONE);

        fabinvite.hide();
        fabsave.hide();
        //textinfo_invite.setVisibility(View.GONE);
        findViewById(R.id.textinfo_invite).setVisibility(View.GONE);
        pvpupdate.clear();
        new AgentData("allowback", true);

        if (frominbox) {
            findViewById(R.id.pvp_frame).setVisibility(View.GONE);
            Log.i("PVP KEY--- >", " " + frominboxpvpid);
            mTitleTextView.setText(getResources().getString(R.string.string_pvp_join_title));
            jointhepvp(frominboxpvpid, getApplicationContext(), PvPCreate.this, inboxid);


        } else if (fromlink) {
            findViewById(R.id.pvp_frame).setVisibility(View.GONE);
            Log.i("PVP KEY--- >", " " + frominboxpvpid);
            mTitleTextView.setText(getResources().getString(R.string.string_pvp_join_title));
            mDatabase.child(user.getUid()).child("pvp").addListenerForSingleValueEvent(new ValueEventListener() {
                long countpvps = 0;
                long activepvp = 0;

                @Override
                public void onDataChange(final DataSnapshot snapshotagentpvps) {


                    if (snapshotagentpvps.hasChildren()) {

                        mDatabase.child(user.getUid()).child("appversion").addListenerForSingleValueEvent(new ValueEventListener() {


                            @Override
                            public void onDataChange(final DataSnapshot snapshotagentappversion) {
                                for (DataSnapshot childpvp : snapshotagentpvps.getChildren()) {
                                    countpvps++;
                                    if (!Boolean.parseBoolean(childpvp.child("finished").getValue().toString())) {
                                        activepvp++;
                                    }

                                    if (countpvps >= snapshotagentpvps.getChildrenCount()) {
                                        if (activepvp >= 3 || !snapshotagentappversion.exists()) {

                                            if (!snapshotagentappversion.exists()) {
                                                StyleableToast.makeText(PvPCreate.this, "Your AOD app version isnt compatible with this PvP", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                            } else {
                                                StyleableToast.makeText(PvPCreate.this, "You have already 3 active/pending PvPs.", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                                            }

                                            finish();
                                        } else {
                                            jointhepvp(frominboxpvpid, getApplicationContext(), PvPCreate.this, inboxid);
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                finish();

                            }
                        });


                    } else {
                        jointhepvp(frominboxpvpid, getApplicationContext(), PvPCreate.this, inboxid);
                    }

                    //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    finish();

                }
            });
        } else {
            findViewById(R.id.pvp_frame).setVisibility(View.VISIBLE);
            loadpvp();
        }


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");

        //pref.edit().remove("scanedagnet").apply();

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (newagentsprofile != null) {
            mbfriendsrequest.removeEventListener(newagentsprofile);

        }

        if (newagentsprofileownrequest != null) {
            mDatabase.removeEventListener(newagentsprofileownrequest);

        }

        if (friendsbuilder != null) {
            hasdialogbefore = true;
            friendsbuilder.create().dismiss();
            friendsbuilder = null;
        }

        if (listener != null) {
            hpImage.setChangeListener(null);
            listener = null;
        }

        if (listener2 != null) {
            hpruntime.setChangeListener(null);
            listener2 = null;
        }

        textinfo.setVisibility(View.GONE);
        fabsave.hide();

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (newagentsprofile != null) {
            mbfriendsrequest.removeEventListener(newagentsprofile);

        }

        if (newagentsprofileownrequest != null) {
            mDatabase.removeEventListener(newagentsprofileownrequest);

        }

        if (listener != null) {
            hpImage.setChangeListener(null);
            listener = null;
        }

        if (listener2 != null) {
            hpruntime.setChangeListener(null);
            listener2 = null;
        }
        if (ad != null) {
            if (ad.isShowing()) {
                ad.dismiss();
            }
        }
        textinfo.setVisibility(View.GONE);
        fabsave.hide();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (friendsbuilder != null) {
            hasdialogbefore = true;
            friendsbuilder.create().dismiss();
            friendsbuilder = null;
        }
        try {
            if (networkStateReceiver != null) {
                this.unregisterReceiver(networkStateReceiver);
                Log.i("", "broadcastReceiver unregistered");
            }
        } catch (Exception e) {
            Log.i("", "broadcastReceiver is already unregistered");
            networkStateReceiver = null;
        }
        if (listener != null) {
            hpImage.setChangeListener(null);
            listener = null;
        }

        if (listener2 != null) {
            hpruntime.setChangeListener(null);
            listener2 = null;
        }
        if (ad != null) {
            if (ad.isShowing()) {
                ad.dismiss();
            }
        }
    }

    @Override
    public void networkAvailable() {


        coordinator.setVisibility(View.VISIBLE);
        progressBarHolder.setVisibility(View.GONE);
        TextView statustxt = findViewById(R.id.status_txt);
        statustxt.setText(getResources().getString(R.string.string_pleaswait));

        /* TODO: Your connection-oriented stuff here */
    }

    @Override
    public void networkUnavailable() {


        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);
        TextView statustxt = findViewById(R.id.status_txt);
        statustxt.setText("offline, please re-connect to a network");
        /* TODO: Your disconnection-oriented stuff here */
    }


    @Override
    public void onBackPressed() {


        if (!AgentData.shouldAllowBack) {
            //doSomething();
        } else {
            new AgentData("allowback", true);
            super.onBackPressed();
        }


    }


    private String getCharForNumber(int i) {
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        Log.i("NUM2 -->", ":" + i);
        if (i > 25) {
            return null;
        }
        return Character.toString(alphabet[i]);
    }


    private void loadpvp() {
        againframe.setVisibility(View.GONE);
        hpImage.setEnabled(true);
        pvpupdate = new HashMap<>();
        pvpupdate.clear();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int newwidth = ((size.x) / 5) - 10;
        pvptitle.clear();
        pvpstatus.clear();
        pvpicon.clear();
        pvpbadgeid.clear();
        pvpbadgecat.clear();
        pvpruntime.clear();
        pvpunite.clear();
        pvpvalue.clear();
        final int[] looperx = {0};
        // Prepare dataset
        //  List<AdvancedExampleCountryPOJO> exampleDataSet = AdvancedExampleCountryPOJO.getActivPvPData(this,getApplicationContext());

        mDatabaseaod.child("badge").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotactivpvp) {
                //snapshot.child("name").getValue();
                Log.w("GET X -->", " id :" + snapshotactivpvp.toString());
                // SetLoginName(snapshot.child("name").getValue().toString());

                for (DataSnapshot childpvp : snapshotactivpvp.getChildren()) {

                    looperx[0]++;
                    //  Log.w("GET X -->", " id :" + looperx[0] +" "+childpvp.getKey());
                    if (Boolean.parseBoolean(snapshotactivpvp.child(childpvp.getKey()).child("pvp").getValue().toString())) {
                        int badgepvpid = getResources().getIdentifier(snapshotactivpvp.child(childpvp.getKey()).child("drawable").getValue().toString(), "drawable", getPackageName());
                        int badgepvperror = getResources().getIdentifier("full_logo", "drawable", getPackageName());

                        pvpbadgeid.add(snapshotactivpvp.child(childpvp.getKey()).child("id").getValue().toString());
                        pvpbadgecat.add(snapshotactivpvp.child(childpvp.getKey()).child("cat").getValue().toString());
                        //badgemonth = getResources().obtainTypedArray(badgemonthid);
                        if (badgepvpid != 0) {
                            //   Log.w("BADGEPVP -->", " id :" + badgepvpid);
                            pvptitle.add(snapshotactivpvp.child(childpvp.getKey()).child("title").getValue().toString());
                            pvpicon.add(new HorizontalPicker.DrawableItem(badgepvpid));
                        } else {
                            pvptitle.add(snapshotactivpvp.child(childpvp.getKey()).child("title").getValue().toString());

                            pvpicon.add(new HorizontalPicker.DrawableItem(badgepvperror));
                        }

                    }


                    if (looperx[0] >= snapshotactivpvp.getChildrenCount()) {

                        //If your picker takes images as items :


                        hpImage.setItems(pvpicon);

                        missiontit.setText("please select a category");

                        listener = new HorizontalPicker.OnSelectionChangeListener() {
                            @Override
                            public void onItemSelect(HorizontalPicker picker, int index) {
                                HorizontalPicker.PickerItem selected = picker.getSelectedItem();
                                missiontit.setText((selected.hasDrawable() ? (pvptitle.get(picker.getSelectedIndex())) + " is selected" : "Select a Mission"));
                                pvpupdate.put("/badgeid", pvpbadgeid.get(picker.getSelectedIndex()));
                                pvpupdate.put("/badgecat", pvpbadgecat.get(picker.getSelectedIndex()));
                                pvpupdate.put("/pvptitle", pvptitle.get(picker.getSelectedIndex()));
                                if (listener2 != null) {
                                    hpruntime.setChangeListener(null);
                                    listener2 = null;
                                }
                                getruntimes(picker.getSelectedIndex());
                                //hpImage.setEnabled(false);
                                //Toast.makeText(MainActivity.this, selected.hasDrawable() ? "Item at " + (picker.getSelectedIndex() + 1) + " is selected" : selected.getText() + " is selected", Toast.LENGTH_SHORT).show();
                            }

                        };


                        hpImage.setChangeListener(listener);
                        coordinator.setVisibility(View.VISIBLE);
                        progressBarHolder.setVisibility(View.GONE);


                    }

                    //  Log.i("TEST LOG--->",child.getValue().toString());

                }

                // Create adapter with our dataset


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("DATA ERROR", databaseError.getMessage());
            }
        });

    }


    private void getruntimes(final int pvpartindex) {
        //hpImage.setEnabled(false);
        hpruntime.setEnabled(true);
        pvpruntime.clear();
        pvpunite.clear();
        pvpvalue.clear();
        pvpruntimetext.clear();
        runtimeframe.setVisibility(View.VISIBLE);
        final int[] looperx2 = {0};
        fabreset.show();
        final Animation animSlideRight = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        animSlideRight.setDuration(300);

        if (!runtimeframe.isShown()) {
            runtimeframe.startAnimation(animSlideRight);
        }

        animSlideRight.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {


            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        // Prepare dataset
        //  List<AdvancedExampleCountryPOJO> exampleDataSet = AdvancedExampleCountryPOJO.getActivPvPData(this,getApplicationContext());

        mDatabaseaod.child("runtimes").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotactivruntimes) {
                //snapshot.child("name").getValue();
                // Log.w("GET X -->", " id :" +snapshotactivruntimes.toString());
                // SetLoginName(snapshot.child("name").getValue().toString());

                for (DataSnapshot childruntimes : snapshotactivruntimes.getChildren()) {

                    looperx2[0]++;
                    //  Log.w("GET X -->", " id :" + looperx[0] +" "+childpvp.getKey());
                    if (Boolean.parseBoolean(snapshotactivruntimes.child(childruntimes.getKey()).child("active").getValue().toString())) {


                        pvpvalue.add(snapshotactivruntimes.child(childruntimes.getKey()).child("value").getValue().toString());
                        pvpruntime.add(Long.parseLong(snapshotactivruntimes.child(childruntimes.getKey()).child("seconds").getValue().toString()));
                        pvpunite.add(snapshotactivruntimes.child(childruntimes.getKey()).child("unite").getValue().toString());
                        //badgemonth = getResources().obtainTypedArray(badgemonthid);

                        pvpruntimetext.add(new HorizontalPicker.TextItem(snapshotactivruntimes.child(childruntimes.getKey()).child("value").getValue().toString() + "\n" + snapshotactivruntimes.child(childruntimes.getKey()).child("unite").getValue().toString()));

                    }


                    if (looperx2[0] >= snapshotactivruntimes.getChildrenCount()) {

                        //If your picker takes images as items :
                        if (pvptitle.get(pvpartindex).equalsIgnoreCase("Trekker")) {
                            int removeone = pvpvalue.indexOf("10");
                            pvpvalue.remove(removeone);
                            pvpruntimetext.remove(removeone);
                            pvpruntime.remove(removeone);
                            pvpunite.remove(removeone);

                            int removetwo = pvpvalue.indexOf("30");
                            pvpvalue.remove(removetwo);
                            pvpruntimetext.remove(removetwo);
                            pvpruntime.remove(removetwo);
                            pvpunite.remove(removetwo);

                          /*  int removethree = pvpvalue.indexOf("60");
                            pvpvalue.remove(removethree);
                            pvpruntimetext.remove(removethree);
                            pvpruntime.remove(removethree);
                            pvpunite.remove(removethree); */
                        }

                        hpruntime.setItems(pvpruntimetext);

                        selectedruntime.setText("please select a runtime");

                        listener2 = new HorizontalPicker.OnSelectionChangeListener() {
                            @Override
                            public void onItemSelect(HorizontalPicker picker, int index) {
                                HorizontalPicker.PickerItem selected = picker.getSelectedItem();
                                selectedruntime.setText(pvpvalue.get(picker.getSelectedIndex()) + " " + pvpunite.get(picker.getSelectedIndex()));
                                pvpupdate.put("/runtime", pvpruntime.get(picker.getSelectedIndex()));
                                pvpupdate.put("/unite", pvpunite.get(picker.getSelectedIndex()));
                                pvpupdate.put("/value", pvpvalue.get(picker.getSelectedIndex()));

                                pvpupdate.put("/runtime_text", pvpvalue.get(picker.getSelectedIndex()) + " " + pvpunite.get(picker.getSelectedIndex()));
                                addagentstopvp(pvpupdate, pvp_frame, pvp_result);
                                //hpruntime.setEnabled(false);
                                //Toast.makeText(MainActivity.this, selected.hasDrawable() ? "Item at " + (picker.getSelectedIndex() + 1) + " is selected" : selected.getText() + " is selected", Toast.LENGTH_SHORT).show();
                            }

                        };


                        hpruntime.setChangeListener(listener2);
                        // coordinator.setVisibility(View.VISIBLE);
                        // progressBarHolder.setVisibility(View.GONE);


                    }

                    //  Log.i("TEST LOG--->",child.getValue().toString());

                }

                // Create adapter with our dataset


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("DATA ERROR", databaseError.getMessage());
            }
        });

    }


    private void addagentstopvp(Map<String, Object> pvpupdate, NestedScrollView pvp_frame, NestedScrollView pvp_result) {
        hpImage.setEnabled(false);
        againframe.setVisibility(View.VISIBLE);
        final FlowLayout myRootfriends = findViewById(R.id.my_friends);
        myRootfriends.removeAllViews();
        //   LinearLayout a = new LinearLayout(this);

        myRootfriends.setOrientation(LinearLayout.HORIZONTAL);
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg_pattern);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        myRootfriends.setBackgroundDrawable(bitmapDrawable);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int newwidth = ((size.x) / 5) - 10;
        final LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View view = inflater.inflate(R.layout.list_pvp_adapter, myRootfriends, false);
        // set item content in view
        final OctogonImageView mybadge = view.findViewById(R.id.badgeimage);
        mybadge.getLayoutParams().width = newwidth;
        mybadge.getLayoutParams().height = newwidth;
        final TextView friendname = view.findViewById(R.id.friendname);

        pvpupdate.put("/creater", AgentData.getagentname());
        pvpupdate.put("/createruuid", user.getUid());
        pvpupdate.put("/createrimage", user.getProviderData().get(1).getPhotoUrl().toString() + "?sz=200");
        friendname.setText(AgentData.getagentname() + "(YOU)");
        friendname.setSelected(true);

        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getProviderData().get(1).getPhotoUrl().toString() + "?sz=200")
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(mybadge);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }


        mybadge.setBorderWidth(4);
        mybadge.setBorderColor(getColor(R.color.colorAccent));

        mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
        mybadge.setPadding(8, 8, 8, 8);
        myRootfriends.addView(view);
        addagentsselect(myRootfriends, pvpupdate, pvp_frame, pvp_result);

    }


    private void addagentsselect(final FlowLayout myRootfriends, final Map<String, Object> pvpupdate, final NestedScrollView pvp_frame, final NestedScrollView pvp_result) {


        myRootfriends.setOrientation(LinearLayout.HORIZONTAL);
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg_pattern);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        myRootfriends.setBackgroundDrawable(bitmapDrawable);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int extra = 3;
        for (int i = 0; i < extra; ++i) {
            final int newwidth = ((size.x) / 7) - 10;
            final LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
            final View view = inflater.inflate(R.layout.list_pvp_adapter_invite, myRootfriends, false);
            // set item content in view
            final OctogonImageView inviteimage = view.findViewById(R.id.inviteimage);
            inviteimage.getLayoutParams().width = newwidth;
            inviteimage.getLayoutParams().height = newwidth;
            final TextView nameofinviteagent = view.findViewById(R.id.nameofinviteagent);

            nameofinviteagent.setText("add " + (i + 1));
            nameofinviteagent.setSelected(true);


            if (i == 0) {
                try {
                    PicassoCache.getPicassoInstance(getApplicationContext())
                            .load(R.drawable.ic_filter_1)

                            .placeholder(R.drawable.ic_filter_1)
                            .error(R.drawable.ic_filter_1)

                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                            //.transform(new CropRoundTransformation())
                            .into(inviteimage);
                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());

                    e.printStackTrace();
                }

                inviteimage.setEnabled(true);
            }

            if (i == 1) {
                try {
                    PicassoCache.getPicassoInstance(getApplicationContext())
                            .load(R.drawable.ic_filter_2)

                            .placeholder(R.drawable.ic_filter_2)
                            .error(R.drawable.ic_filter_2)

                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                            //.transform(new CropRoundTransformation())
                            .into(inviteimage);
                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());

                    e.printStackTrace();
                }


                inviteimage.setEnabled(false);

            }

            if (i == 2) {
                try {
                    PicassoCache.getPicassoInstance(getApplicationContext())
                            .load(R.drawable.ic_filter_3)

                            .placeholder(R.drawable.ic_filter_3)
                            .error(R.drawable.ic_filter_3)

                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                            //.transform(new CropRoundTransformation())
                            .into(inviteimage);
                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());

                    e.printStackTrace();
                }
                inviteimage.setEnabled(false);

            }


            inviteimage.setBorderWidth(3);
            inviteimage.setBorderColor(getColor(R.color.colorAccent));

            inviteimage.setScaleType(ImageView.ScaleType.FIT_CENTER);
            inviteimage.setPadding(8, 8, 8, 8);

            myRootfriends.addView(view);

            final int finalI = i;


            inviteimage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view2) {
                    if (finalI == 0) {
                        RelativeLayout nextfriendrel, currentfriend;
                        TextView currentname;
                        nextfriendrel = (RelativeLayout) myRootfriends.getChildAt(2);
                        currentfriend = (RelativeLayout) myRootfriends.getChildAt(1);
                        currentname = (TextView) currentfriend.getChildAt(1);
                        // currentname.setText("Pommes");
                        OctogonImageView nextfriend, curremtimage;

                        curremtimage = (OctogonImageView) currentfriend.getChildAt(0);
                        nextfriend = (OctogonImageView) nextfriendrel.getChildAt(0);


                        addfrienddialog(nextfriend, curremtimage, currentname, 1, fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result);

                    }
                    if (finalI == 1) {
                        RelativeLayout nextfriendrel, currentfriend;
                        TextView currentname;
                        nextfriendrel = (RelativeLayout) myRootfriends.getChildAt(3);
                        currentfriend = (RelativeLayout) myRootfriends.getChildAt(2);
                        currentname = (TextView) currentfriend.getChildAt(1);
                        //  currentname.setText("Pommes2");
                        OctogonImageView nextfriend, curremtimage;
                        curremtimage = (OctogonImageView) currentfriend.getChildAt(0);
                        nextfriend = (OctogonImageView) nextfriendrel.getChildAt(0);

                        addfrienddialog(nextfriend, curremtimage, currentname, 2, fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result);

                    }
                    if (finalI == 2) {
                        RelativeLayout nextfriendrel, currentfriend;
                        TextView currentname;
                        nextfriendrel = null;
                        currentfriend = (RelativeLayout) myRootfriends.getChildAt(3);
                        currentname = (TextView) currentfriend.getChildAt(1);
                        //currentname.setText("Pommes3");
                        OctogonImageView nextfriend, curremtimage;
                        curremtimage = (OctogonImageView) currentfriend.getChildAt(0);
                        nextfriend = null;

                        addfrienddialog(nextfriend, curremtimage, currentname, 3, fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result);

                    }
                    // StyleableToast.makeText(getApplicationContext(), ""+(finalI +1), Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                }
            });


        }

        if (fromprofile) {
            RelativeLayout nextfriendrel, currentfriend;
            TextView currentname;
            nextfriendrel = (RelativeLayout) myRootfriends.getChildAt(2);
            currentfriend = (RelativeLayout) myRootfriends.getChildAt(1);
            currentname = (TextView) currentfriend.getChildAt(1);
            // currentname.setText("Pommes");
            OctogonImageView nextfriend, curremtimage;

            curremtimage = (OctogonImageView) currentfriend.getChildAt(0);
            nextfriend = (OctogonImageView) nextfriendrel.getChildAt(0);
            if (fromprofileagentname.equalsIgnoreCase("linkshare")) {
                setAgent("linkshare", PvPCreate.this, friendsselectadapter, curremtimage, nextfriend, 1, currentname, ad, "Link share", fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result, friendnamenames);
            } else {
                setAgent("fromprofile", PvPCreate.this, friendsselectadapter, curremtimage, nextfriend, 1, currentname, ad, fromprofileagentname, fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result, friendnamenames);

            }
        }


    }

    private void addfrienddialog(final OctogonImageView nextfriend, final OctogonImageView friend, final TextView friendname, final Integer friendnumber, final FloatingActionButton fabsave, final TextView textinfo, final HorizontalPicker hpruntime, final Map<String, Object> pvpupdate, final NestedScrollView pvp_frame, final NestedScrollView pvp_result) {
        // TODO
        // nextfriend.setEnabled(true);
        // friend.setEnabled(false);


        final List<String> freindkey = new ArrayList<String>();


        mDatabase.child(user.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshotfriends) {
                String[] frienduid;


                frienduid = null;

                int countis = 0;
                freindkey.clear();


                for (DataSnapshot historyis : snapshotfriends.getChildren()) {
                    //  Log.i("timestamp", historyis.getValue().toString());
                    countis++;
                    if (historyis.hasChildren()) {
                        Log.i("GET FREIENDs-->", " " + historyis.getKey());
                        Log.i("GET FREIENDs Status-->", " " + snapshotfriends.child(historyis.getKey()).child("status").getValue());
                        if (Boolean.parseBoolean(snapshotfriends.child(historyis.getKey()).child("status").getValue().toString())) {

                            if (snapshotfriends.child(historyis.getKey()).child("agentuid").exists()) {
                                freindkey.add(historyis.getKey());


                            } else {
                                mDatabase.child(user.getUid()).child("friends").child(historyis.getKey()).setValue(null);
                            }

                        }


                    }

                    if (countis >= snapshotfriends.getChildrenCount()) {
                        frienduid = new String[freindkey.size()];


                        frienduid = freindkey.toArray(frienduid);


                        Arrays.sort(frienduid, Collections.reverseOrder());

                        // START GET FRIENDS NAMES AND ORDER
                        getnamesandorder(frienduid, freindkey, friendnumber, friendname, nextfriend, friend, fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result);


                    }

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });

        //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
        //localTextView1.setTypeface(proto);


    }

    private void getnamesandorder(final String[] frienduid, final List<String> freindkey, final Integer friendnumber, final TextView friendname, final OctogonImageView nextfriend, final OctogonImageView friend, final FloatingActionButton fabsave, final TextView textinfo, final HorizontalPicker hpruntime, final Map<String, Object> pvpupdate, final NestedScrollView pvp_frame, final NestedScrollView pvp_result) {

        LayoutInflater inflater = getLayoutInflater();
        hasdialogbefore = false;
        final View dialog = inflater.inflate(R.layout.dialog_pvp_friend_selcet, null);
        final AutoCompleteTextView agentsearch = dialog.findViewById(R.id.agentsearch);
        final FloatingActionButton fabshare = dialog.findViewById(R.id.fabshare);
        final TextView friendtextpos = dialog.findViewById(R.id.friendtextpos);

        if (friendnumber > 1) {
            fabshare.hide();
            fabshare.setEnabled(false);

        }

        if (friendnumber == 2) {
            friendtextpos.setText("Select the second friend.");

        }

        if (friendnumber == 3) {
            friendtextpos.setText("Select the third friend.");

        }


        if (friendsbuilder == null) {
            friendsbuilder = new AlertDialog.Builder(PvPCreate.this, R.style.CustomDialog);
            hasdialogbefore = false;
            // builder.setMessage("Text not finished...");

            friendsbuilder.setView(dialog);
            friendsbuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    hasdialogbefore = false;
                    if (friendsbuilder != null) {
                        friendsbuilder.create().dismiss();
                        friendsbuilder = null;
                    }
                }
            });
        } else {
            hasdialogbefore = true;
        }

        friendsbuilder.setPositiveButton("close", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                hasdialogbefore = false;
                if (friendsbuilder != null) {
                    friendsbuilder.create().dismiss();
                    friendsbuilder = null;
                }

            }
        });

        if (!hasdialogbefore) {
            friendsbuilder.setCancelable(true);

        }

        if (!isFinishing()) {
            if (friendsbuilder != null) {
                if (friendsbuilder.create().isShowing()) {
                    friendsbuilder.create().dismiss();
                } else {
                    if (!hasdialogbefore) {

                        friendsbuilder.create();
                        ad = friendsbuilder.show();
                    }
                }
            }

        }
        final List<String> friendsnameslist = new ArrayList<String>();

        friendnamenames = null;
        friendsnameslist.clear();
        friendscount = 0;
        for (int i = 0; i < frienduid.length; i++) {

            //Write your code here(allocation/deallocation/store in array etc.)
            //  System.out.println(i + "=" + lv.getItemAtPosition(i));
            //Log.i("Frienduid", frienduid[i]+" count: "+friendscount+" lenth "+frienduid.length);
            final int finalI = i;
            mDatabase.child(frienduid[i]).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(final DataSnapshot snapshotagentname) {
                    friendscount++;
                    friendsnameslist.add(snapshotagentname.getValue().toString());
                    //agent_value.setText(snapshotagentname.getValue().toString());

                    Log.i("Frienduid", frienduid[finalI] + " count: " + friendscount + " lenth " + frienduid.length);

                    if (friendscount >= frienduid.length) {
                        friendnamenames = new String[friendsnameslist.size()];


                        // Collections.sort(Arrays.asList(friendnamenames), Collections.reverseOrder());
                        friendnamenames = friendsnameslist.toArray(friendnamenames);
                        final String[] preffriends = friendsnameslist.toArray(friendnamenames);

                        Arrays.sort(friendnamenames, new Comparator<String>() {
                            @Override
                            public int compare(String s1, String s2) {
                                return s1.compareToIgnoreCase(s2);
                            }
                        });
                        friendsselectadapter = new FriendsSelectAdapter(PvPCreate.this, freindkey.size(), frienduid, friendsselectadapter, friendnumber, nextfriend, friend, friendname, ad, friendnamenames, preffriends, fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result);
                        lv = dialog.findViewById(R.id.listoffriends);
                        lv.setAdapter(friendsselectadapter);
                        friendsselectadapter.notifyDataSetChanged();


                        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(PvPCreate.this,
                                android.R.layout.select_dialog_item, friendnamenames);
                        //Used to specify minimum number of
                        //characters the user has to type in order to display the drop down hint.
                        agentsearch.setThreshold(1);
                        //Setting adapter
                        agentsearch.setAdapter(arrayAdapter);
                        agentsearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                                    long id) {

                                // Toast.makeText(PvPCreate.this, "" + arrayAdapter.getItem(pos).toString(), Toast.LENGTH_SHORT).show();

                                agentsearch.onEditorAction(EditorInfo.IME_ACTION_DONE);
                                setAgent(frienduid[0], PvPCreate.this, friendsselectadapter, friend, nextfriend, friendnumber, friendname, ad, arrayAdapter.getItem(pos), fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result, friendnamenames);
                            }
                        });


                    }
                    //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                }
            });

        }

        fabshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAgent("linkshare", PvPCreate.this, friendsselectadapter, friend, nextfriend, friendnumber, friendname, ad, "Link share", fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result, friendnamenames);

            }
        });

    }

    public void setAgent(String friendid, final Activity context, FriendsSelectAdapter historyadapter, final OctogonImageView friend, final OctogonImageView nextfriend, final Integer friendnumber, final TextView friendname, final AlertDialog ad, final String agentname, final FloatingActionButton fabsave, final TextView textinfo, final HorizontalPicker hpruntime, final Map<String, Object> pvpupdate, final NestedScrollView pvp_frame, final NestedScrollView pvp_result, String[] friendnamenames2) {
        final String pvpkey = mbpvp.push().getKey();
        boolean agentisinvitedbefore = false;
        if (friendid.equalsIgnoreCase("linkshare")) {
            pvpupdate.put("/art", "linkshare");
            pvpupdate.put("/pvpid", pvpkey);

            pvpupdate.put("/pvplink", "AODs_" + pvpkey);
            try {
                PicassoCache.getPicassoInstance(context)
                        .load(R.drawable.full_logo)
                        .placeholder(R.drawable.full_logo)
                        .error(R.drawable.full_logo)

                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                        //.transform(new CropRoundTransformation())
                        .into(friend);
                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
            } catch (Exception e) {
                //Log.e("Error", e.getMessage());

                e.printStackTrace();
            }

            friendname.setText(agentname);
            hpruntime.setEnabled(false);

            textinfo.setVisibility(View.VISIBLE);


            fabsave.show();
        }

        // fromprofile
        else {


            if (friendnumber == 1) {
                pvpupdate.put("/agent1", agentname);
                pvpupdate.put("/agent2", "null");
                pvpupdate.put("/agent3", "null");
            } else if (friendnumber == 2) {

                if (pvpupdate.get("/agent1").toString().equalsIgnoreCase(agentname) || pvpupdate.get("/agent2").toString().equalsIgnoreCase(agentname)) {
                    StyleableToast.makeText(context, "Agent " + agentname + " is already invited.", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                    agentisinvitedbefore = true;
                }

            } else if (friendnumber == 3) {

                if (pvpupdate.get("/agent1").toString().equalsIgnoreCase(agentname) || pvpupdate.get("/agent2").toString().equalsIgnoreCase(agentname)) {
                    StyleableToast.makeText(context, "Agent " + agentname + " is already invited.", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                    agentisinvitedbefore = true;
                }

            }


            if (!agentisinvitedbefore) {


                mdbroot.child(agentname).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotagentid) {


                        // friendname.setText(snapshotagentname.getValue().toString());
                        mDatabase.child(snapshotagentid.getValue().toString()).child("pvp").addListenerForSingleValueEvent(new ValueEventListener() {
                            long countpvps = 0;
                            long activepvp = 0;

                            @Override
                            public void onDataChange(final DataSnapshot snapshotagentpvps) {


                                if (snapshotagentpvps.hasChildren()) {

                                    mDatabase.child(snapshotagentid.getValue().toString()).child("appversion").addListenerForSingleValueEvent(new ValueEventListener() {


                                        @Override
                                        public void onDataChange(final DataSnapshot snapshotagentappversion) {

                                            Log.i("APP VERSION STATUS ", "" + snapshotagentappversion.exists());


                                            for (DataSnapshot childpvp : snapshotagentpvps.getChildren()) {
                                                countpvps++;
                                                if (!Boolean.parseBoolean(childpvp.child("finished").getValue().toString())) {
                                                    activepvp++;
                                                }

                                                if (countpvps >= snapshotagentpvps.getChildrenCount()) {
                                                    if (activepvp >= 3 || !snapshotagentappversion.exists()) {
                                                        textinfo.setVisibility(View.GONE);
                                                        Log.i("APP VERSION STATUS ", "" + snapshotagentappversion.exists());
                                                        friend.setEnabled(true);
                                                        nextfriend.setEnabled(false);
                                                        if (!snapshotagentappversion.exists()) {
                                                            StyleableToast.makeText(context, agentname + " app version is not compatible with PvPs", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                                        } else {
                                                            StyleableToast.makeText(context, "Agent " + agentname + " has already min. 3 active PvPs", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                                        }


                                                        fabsave.hide();
                                                    } else {
                                                        textinfo.setVisibility(View.VISIBLE);

                                                        fabsave.show();
                                                        if (friendnumber == 2) {
                                                            pvpupdate.put("/agent2", agentname);


                                                        } else if (friendnumber == 3) {
                                                            pvpupdate.put("/agent1", pvpupdate.get("/agent1").toString());
                                                            pvpupdate.put("/agent2", pvpupdate.get("/agent2").toString());
                                                            pvpupdate.put("/agent3", agentname);


                                                        }
                                                        pvpupdate.put("/art", "invites");
                                                        pvpupdate.put("/pvpid", pvpkey);
                                                        pvpupdate.put("/pvplink", "AODi_" + pvpkey);

                                                        mDatabase.child(snapshotagentid.getValue().toString()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(final DataSnapshot snapshotagentname) {

                                                                friendname.setText(snapshotagentname.getValue().toString());
                                                                hpruntime.setEnabled(false);
                                                                pvpupdate.put("/agents/" + friendnumber + "/agentname", snapshotagentname.getValue().toString());
                                                                pvpupdate.put("/agents/" + friendnumber + "/uuid", snapshotagentid.getValue().toString());

                                                                //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                                            }
                                                        });

                                                        mDatabase.child(snapshotagentid.getValue().toString()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(final DataSnapshot snapshotagentimg) {

                                                                pvpupdate.put("/agents/" + friendnumber + "/agentimage", snapshotagentimg.getValue().toString());
                                                                try {
                                                                    PicassoCache.getPicassoInstance(context)
                                                                            .load(snapshotagentimg.getValue().toString() + "?sz=80")
                                                                            .placeholder(R.drawable.full_logo)
                                                                            .error(R.drawable.full_logo)

                                                                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                            //.transform(new CropRoundTransformation())
                                                                            .into(friend);
                                                                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                                } catch (Exception e) {
                                                                    //Log.e("Error", e.getMessage());

                                                                    e.printStackTrace();
                                                                }
                                                                //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                        }
                                    });


                                } else {

                                    mDatabase.child(snapshotagentid.getValue().toString()).child("appversion").addListenerForSingleValueEvent(new ValueEventListener() {


                                        @Override
                                        public void onDataChange(final DataSnapshot snapshotagentappversion) {

                                            Log.i("APP VERSION STATUS ", "" + snapshotagentappversion.exists());
                                            if (!snapshotagentappversion.exists()) {
                                                textinfo.setVisibility(View.GONE);
                                                Log.i("APP VERSION STATUS ", "" + snapshotagentappversion.exists());
                                                friend.setEnabled(true);
                                                nextfriend.setEnabled(false);

                                                StyleableToast.makeText(context, agentname + " app version is not compatible with PvPs", Toast.LENGTH_LONG, R.style.defaulttoastb).show();


                                                fabsave.hide();
                                            } else {

                                                textinfo.setVisibility(View.VISIBLE);

                                                fabsave.show();
                                                if (friendnumber == 2) {
                                                    pvpupdate.put("/agent2", agentname);


                                                } else if (friendnumber == 3) {
                                                    pvpupdate.put("/agent1", pvpupdate.get("/agent1").toString());
                                                    pvpupdate.put("/agent2", pvpupdate.get("/agent2").toString());
                                                    pvpupdate.put("/agent3", agentname);


                                                }
                                                pvpupdate.put("/art", "invites");
                                                pvpupdate.put("/pvpid", pvpkey);
                                                pvpupdate.put("/pvplink", "AODi_" + pvpkey);

                                                mDatabase.child(snapshotagentid.getValue().toString()).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(final DataSnapshot snapshotagentname) {

                                                        friendname.setText(snapshotagentname.getValue().toString());
                                                        hpruntime.setEnabled(false);
                                                        pvpupdate.put("/agents/" + friendnumber + "/agentname", snapshotagentname.getValue().toString());
                                                        pvpupdate.put("/agents/" + friendnumber + "/uuid", snapshotagentid.getValue().toString());

                                                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                                    }
                                                });

                                                mDatabase.child(snapshotagentid.getValue().toString()).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(final DataSnapshot snapshotagentimg) {

                                                        pvpupdate.put("/agents/" + friendnumber + "/agentimage", snapshotagentimg.getValue().toString());
                                                        try {
                                                            PicassoCache.getPicassoInstance(context)
                                                                    .load(snapshotagentimg.getValue().toString() + "?sz=80")
                                                                    .placeholder(R.drawable.full_logo)
                                                                    .error(R.drawable.full_logo)

                                                                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                    //.transform(new CropRoundTransformation())
                                                                    .into(friend);
                                                            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                        } catch (Exception e) {
                                                            //Log.e("Error", e.getMessage());

                                                            e.printStackTrace();
                                                        }
                                                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                                    }
                                                });
                                            }

                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                                        }
                                    });


                                }

                                //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                            }
                        });


                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                });
            }
        }


        if (ad != null) {
            ad.dismiss();
        }
        hasdialogbefore = false;

        //friendsbuilder.create().dismiss();
        friendsbuilder = null;

        if (friendnumber < 3 && !friendid.equalsIgnoreCase("linkshare")) {
            if (!agentisinvitedbefore) {
                friend.setEnabled(false);
                nextfriend.setEnabled(true);
            }
        } else {
            if (friendnumber == 3 && !friendid.equalsIgnoreCase("linkshare")) {
                if (!agentisinvitedbefore) {
                    friend.setEnabled(false);
                    //  nextfriend.setEnabled(true);
                }
            } else {
                friend.setEnabled(false);
            }
        }


        fabsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pvpupdate.remove("/agent1");
                pvpupdate.remove("/agent2");
                pvpupdate.remove("/agent3");
                pvpupdate.put("/privacy", false);
                pvpupdate.put("/finished", false);
                pvpupdate.put("/timestamp", System.currentTimeMillis());
                Log.i("DATA ALL -->", "" + pvpupdate.toString());


                fabsave.hide();
                pvp_frame.setVisibility(View.GONE);
                textinfo.setVisibility(View.GONE);
                pvp_result.setVisibility(View.VISIBLE);

                Log.i("RESULTS Creator--->", "" + pvpupdate.get("/creater"));
                Log.i("RESULTS Creator img--->", "" + pvpupdate.get("/createrimage"));

                Log.i("RESULTS Creator img--->", "" + pvpupdate.containsKey("/agents/1/agentname"));

                for (Map.Entry<String, Object> entry : pvpupdate.entrySet()) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    if (key.contains("agents/")) {
                        Log.i("FOUND AGENTS -->", "true");

                    }
                    // Log.i("RESULTS--->", ""+key+" | "+ value);
                }


                final ImageView title_price = context.findViewById(R.id.title_price);
                // Runtime
                TextView title_requests_count = context.findViewById(R.id.title_requests_count);
                RelativeLayout resultload = context.findViewById(R.id.resultload);
                LinearLayout resultcard = context.findViewById(R.id.resultcard);
                TextView textinfo_invite = context.findViewById(R.id.textinfo_invite);
                final TextView title_date_label = context.findViewById(R.id.title_date_label);
                final MaterialAnimatedSwitch pin = context.findViewById(R.id.pin);
                final TextView title_to_address = context.findViewById(R.id.title_to_address);
                TextView pvp_id_text = context.findViewById(R.id.pvp_id_text);
                TextView pvp_art_text = context.findViewById(R.id.pvp_art_text);
                CircleImageView ownerimage = context.findViewById(R.id.ownerimage);
                final TextView pvp_description = context.findViewById(R.id.pvp_description);

                resultload.setVisibility(View.GONE);
                resultcard.setVisibility(View.VISIBLE);

                fabinvite = context.findViewById(R.id.fabinvite);

                // pvpupdate.put("/art", "linkshare");

                pvp_id_text.setText("ID: " + pvpupdate.get("/pvpid").toString());

                if (pvpupdate.get("/art").toString().equalsIgnoreCase("linkshare")) {
                    pvp_art_text.setText("Invite agents via link");
                    textinfo_invite.setText("Save PvP and share invitation link");
                } else {
                    pvp_art_text.setText("PvP from the friends list");
                    textinfo_invite.setText("Save PvP and send invitation to the agents");

                    // SHOW FRIENDS
                }

                textinfo_invite.setVisibility(View.VISIBLE);
                if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                    fabinvite.setBackgroundColor(context.getResources().getColor(R.color.colorENL, context.getTheme()));
                } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                    fabinvite.setBackgroundColor(context.getResources().getColor(R.color.colorRES, context.getTheme()));
                } else {
                    fabinvite.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryYellowDark, context.getTheme()));
                }
                fabinvite.show();


                mDatabaseaod.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshotaod) {


                        int badgeweekid = context.getResources().getIdentifier(snapshotaod.child("badge").child(pvpupdate.get("/badgeid").toString()).child("drawable").getValue().toString(), "drawable", context.getPackageName());

                        //badgemonth = getResources().obtainTypedArray(badgemonthid);
                        if (badgeweekid != 0) {
                            Log.w("BADGE -->", " id :" + badgeweekid);

                            title_price.setImageDrawable(context.getDrawable(badgeweekid));
                        }

                        title_to_address.setText(snapshotaod.child("badge").child(pvpupdate.get("/badgeid").toString()).child("title").getValue().toString());
                        String[] action_text2 = context.getResources().getStringArray(R.array.badge_text_actions_id);
                        int action_value2 = Arrays.asList(action_text2).indexOf(pvpupdate.get("/badgeid").toString()); // pass value

                        String action_value_text_community2 = context.getResources().getStringArray(R.array.badge_text_actions_value)[action_value2];
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            pvp_description.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.challange_txt), action_value_text_community2, snapshotaod.child("badge").child(pvpupdate.get("/badgeid").toString()).child("short1").getValue().toString(), pvpupdate.get("/runtime_text").toString()), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            pvp_description.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.challange_txt), action_value_text_community2, snapshotaod.child("badge").child(pvpupdate.get("/badgeid").toString()).child("short1").getValue().toString(), pvpupdate.get("/runtime_text").toString())));

                        }

                    }


                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                });


                pin.setOnCheckedChangeListener(
                        new MaterialAnimatedSwitch.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(boolean isChecked) {


                                if (isChecked) {
                                    title_date_label.setText("PUBLIC PvP");
                                    pvpupdate.put("/privacy", true);
                                    //pin.toggle();
                                } else {
                                    title_date_label.setText("PRIVATE PvP");
                                    pvpupdate.put("/privacy", false);
                                    // pin.toggle();
                                }
                            }
                        });


                // SET RUNTIME
                title_requests_count.setText(pvpupdate.get("/runtime_text").toString());

                try {
                    PicassoCache.getPicassoInstance(context.getApplicationContext())
                            .load(pvpupdate.get("/createrimage").toString())
                            .placeholder(R.drawable.full_logo)
                            .error(R.drawable.full_logo)


                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                            //  .networkPolicy(NetworkPolicy.NO_CACHE)
                            //.transform(new CropRoundTransformation())
                            .into(ownerimage);
                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                } catch (Exception e) {
                    //Log.e("Error", e.getMessage());

                    e.printStackTrace();
                }


                final FlowLayout myRootfriends = context.findViewById(R.id.my_friends_pvp);
                myRootfriends.removeAllViews();
                //   LinearLayout a = new LinearLayout(this);

                myRootfriends.setOrientation(LinearLayout.HORIZONTAL);

                Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_pattern);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

                bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

                myRootfriends.setBackgroundDrawable(bitmapDrawable);

                myRootfriends.removeAllViews();
                myRootfriends.setOrientation(LinearLayout.HORIZONTAL);

                final LayoutInflater inflater = LayoutInflater.from(context);


                // set item content in view
                Display display = context.getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                final int newwidth = ((size.x) / 6) - 10;

                if (pvpupdate.containsKey("/agents/1/agentname")) {
                    View viewx = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                    final OctogonImageView mybadge = viewx.findViewById(R.id.badgeimage);
                    mybadge.getLayoutParams().width = newwidth;
                    mybadge.getLayoutParams().height = newwidth;
                    final TextView friendname = viewx.findViewById(R.id.friendname);

                    friendname.setText(pvpupdate.get("/agents/1/agentname").toString());
                    friendname.setSelected(true);

                    try {
                        PicassoCache.getPicassoInstance(context.getApplicationContext())
                                .load(pvpupdate.get("/agents/1/agentimage").toString() + "?sz=100")
                                .placeholder(R.drawable.full_logo)
                                .error(R.drawable.full_logo)

                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(mybadge);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }


                    mybadge.setBorderWidth(4);
                    mybadge.setBorderColor(context.getColor(R.color.colorLightBrown));

                    mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    mybadge.setPadding(8, 8, 8, 8);

                    myRootfriends.addView(viewx);
                }

                if (pvpupdate.containsKey("/agents/2/agentname")) {
                    View viewx = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                    final OctogonImageView mybadge = viewx.findViewById(R.id.badgeimage);
                    mybadge.getLayoutParams().width = newwidth;
                    mybadge.getLayoutParams().height = newwidth;
                    final TextView friendname = viewx.findViewById(R.id.friendname);

                    friendname.setText(pvpupdate.get("/agents/2/agentname").toString());
                    friendname.setSelected(true);

                    try {
                        PicassoCache.getPicassoInstance(context.getApplicationContext())
                                .load(pvpupdate.get("/agents/2/agentimage").toString() + "?sz=100")
                                .placeholder(R.drawable.full_logo)
                                .error(R.drawable.full_logo)

                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(mybadge);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }


                    mybadge.setBorderWidth(4);
                    mybadge.setBorderColor(context.getColor(R.color.colorLightBrown));

                    mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    mybadge.setPadding(8, 8, 8, 8);

                    myRootfriends.addView(viewx);
                }

                if (pvpupdate.containsKey("/agents/3/agentname")) {
                    View viewx = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                    final OctogonImageView mybadge = viewx.findViewById(R.id.badgeimage);
                    mybadge.getLayoutParams().width = newwidth;
                    mybadge.getLayoutParams().height = newwidth;
                    final TextView friendname = viewx.findViewById(R.id.friendname);

                    friendname.setText(pvpupdate.get("/agents/3/agentname").toString());
                    friendname.setSelected(true);

                    try {
                        PicassoCache.getPicassoInstance(context.getApplicationContext())
                                .load(pvpupdate.get("/agents/3/agentimage").toString() + "?sz=100")
                                .placeholder(R.drawable.full_logo)
                                .error(R.drawable.full_logo)

                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(mybadge);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }


                    mybadge.setBorderWidth(4);
                    mybadge.setBorderColor(context.getColor(R.color.colorLightBrown));

                    mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    mybadge.setPadding(8, 8, 8, 8);

                    myRootfriends.addView(viewx);
                }


                if (pvpupdate.get("/art").toString().equalsIgnoreCase("linkshare")) {
                    View viewx = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                    final OctogonImageView mybadge = viewx.findViewById(R.id.badgeimage);
                    mybadge.getLayoutParams().width = newwidth;
                    mybadge.getLayoutParams().height = newwidth;
                    final TextView friendname = viewx.findViewById(R.id.friendname);

                    friendname.setText(" ? ? ? ");
                    friendname.setSelected(true);

                    try {
                        PicassoCache.getPicassoInstance(context.getApplicationContext())
                                .load(R.drawable.full_logo)
                                .placeholder(R.drawable.full_logo)
                                .error(R.drawable.full_logo)

                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(mybadge);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }


                    mybadge.setBorderWidth(4);
                    mybadge.setBorderColor(context.getColor(R.color.colorLightBrown));

                    mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    mybadge.setPadding(8, 8, 8, 8);

                    myRootfriends.addView(viewx);


                    View viewx2 = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                    final OctogonImageView mybadge2 = viewx2.findViewById(R.id.badgeimage);
                    mybadge2.getLayoutParams().width = newwidth;
                    mybadge2.getLayoutParams().height = newwidth;
                    final TextView friendname2 = viewx2.findViewById(R.id.friendname);

                    friendname2.setText(" ? ? ? ");
                    friendname2.setSelected(true);

                    try {
                        PicassoCache.getPicassoInstance(context.getApplicationContext())
                                .load(R.drawable.full_logo)
                                .placeholder(R.drawable.full_logo)
                                .error(R.drawable.full_logo)

                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(mybadge2);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }


                    mybadge2.setBorderWidth(4);
                    mybadge2.setBorderColor(context.getColor(R.color.colorLightBrown));

                    mybadge2.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    mybadge2.setPadding(8, 8, 8, 8);

                    myRootfriends.addView(viewx2);


                    View viewx3 = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                    final OctogonImageView mybadge3 = viewx3.findViewById(R.id.badgeimage);
                    mybadge3.getLayoutParams().width = newwidth;
                    mybadge3.getLayoutParams().height = newwidth;
                    final TextView friendname3 = viewx3.findViewById(R.id.friendname);

                    friendname3.setText(" ? ? ? ");
                    friendname3.setSelected(true);

                    try {
                        PicassoCache.getPicassoInstance(context.getApplicationContext())
                                .load(R.drawable.full_logo)
                                .placeholder(R.drawable.full_logo)
                                .error(R.drawable.full_logo)

                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                //.transform(new CropRoundTransformation())
                                .into(mybadge3);
                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                    } catch (Exception e) {
                        //Log.e("Error", e.getMessage());

                        e.printStackTrace();
                    }


                    mybadge3.setBorderWidth(4);
                    mybadge3.setBorderColor(context.getColor(R.color.colorLightBrown));

                    mybadge3.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    mybadge3.setPadding(8, 8, 8, 8);

                    myRootfriends.addView(viewx3);
                }


                fabinvite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        saveandsharePvP(pvpkey, pvpupdate, textinfo, fabsave, fabreset, friendsbuilder, ad, pvp_frame, pvp_result, context);
                        //StyleableToast.makeText(context.getApplicationContext(), "in the next update, promised!!!", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    }
                });

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveandsharePvP(final String pvpkey, final Map<String, Object> pvpupdate, TextView textinfo, FloatingActionButton fabsave, FloatingActionButton fabreset, AlertDialog.Builder friendsbuilder, AlertDialog ad, NestedScrollView pvp_frame, NestedScrollView pvp_result, final Activity context) {
        pvpupdate.put("/active", false);
        Log.i("Data agents --> ", pvpupdate.toString());


        progressBarHolder = context.findViewById(R.id.progressBarHolder);
        progressBarHolder.setVisibility(View.VISIBLE);
        // pvp_result.setVisibility(View.GONE);
        final TextView statustxt = context.findViewById(R.id.status_txt);
        //statustxt.setText(context.getResources().getString(R.string.string_pleaswait));
        new AgentData("allowback", false);
        statustxt.setText(context.getResources().getString(R.string.pvp_creation_text));

        mbpvp.child(pvpkey).updateChildren(pvpupdate).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    // LINK ERSTELLEN
                    statustxt.setText(context.getResources().getString(R.string.pvp_creation_link_text));

                    final Map<String, Object> agentcreator = new HashMap<>();


                    agentcreator.put("/currentvalue", 0);
                    agentcreator.put("/started", false);
                    agentcreator.put("/owner", true);

                    agentcreator.put("/googleid", user.getProviderData().get(1).getUid());
                    agentcreator.put("/timestamp", System.currentTimeMillis());
                    agentcreator.put("/finished", false);
                    agentcreator.put("/lastvalue", 0);
                    agentcreator.put("/startvalue", 0);
                    agentcreator.put("/pvpkey", pvpkey);
                    //String.valueOf(System.currentTimeMillis()
                    mDatabaserunnungpvp.child(pvpkey).child(user.getUid()).updateChildren(agentcreator);


                    mbpvp.child(pvpkey).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(final DataSnapshot snapshotinviteagents) {

                            mDatabase.child(user.getUid()).child("pvp").child(pvpkey).child("timestamp").setValue((System.currentTimeMillis())).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                        // Log.w("ERROR ", this.getClass().getSimpleName() + " 5644 Error ");
                                        mDatabase.child(user.getUid()).child("pvp").child(pvpkey).child("finished").setValue(false);

                                        // IS WITH AGENTS
                                        if (snapshotinviteagents.child("art").getValue().toString().equalsIgnoreCase("invites")) {
                                            long anzahlagents = snapshotinviteagents.child("agents").getChildrenCount();
                                            int looperanzahl = 1;
                                            String pushkey = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
                                            mDatabase.child(user.getUid()).child("pushkey").setValue(pushkey);
                                            for (int i = 1; i <= anzahlagents; ++i) {

                                                String sendto = snapshotinviteagents.child("agents").child(String.valueOf(i)).child("uuid").getValue().toString();
                                                new SendPushinBginvite(pushkey, "pvpinvite", AgentData.getagentname(), sendto, snapshotinviteagents.child("pvptitle").getValue().toString(), snapshotinviteagents.child("runtime_text").getValue().toString()).execute();
                                                statustxt.setText("invite send to Agent " + snapshotinviteagents.child("agents").child(String.valueOf(i)).child("agentname").getValue().toString());
                                                final Map<String, Object> agentinbox = new HashMap<>();


                                                agentinbox.put("/text", AgentData.getagentname());
                                                agentinbox.put("/link", pvpkey);
                                                agentinbox.put("/inviteposition", i);

                                                agentinbox.put("/title", "PvP invite");
                                                agentinbox.put("/timestamp", System.currentTimeMillis());
                                                agentinbox.put("/read", false);

                                                agentinbox.put("/fromuid", user.getUid());
                                                mDatabase.child(snapshotinviteagents.child("agents").child(String.valueOf(i)).child("uuid").getValue().toString()).child("newinbox").child(String.valueOf(System.currentTimeMillis())).updateChildren(agentinbox);


                                                if (i == anzahlagents) {
                                                    Log.i("INVITE DONE", " true");
                                                    statustxt.setText("all done...");
                                                    //new AgentData("allowback", true);

                                                    AlertDialog.Builder confirmdialog = new AlertDialog.Builder(context, R.style.CustomDialog);

                                                    // builder.setMessage("Text not finished...");
                                                    LayoutInflater inflater = context.getLayoutInflater();
                                                    View dialog = inflater.inflate(R.layout.dialog_pvp_created, null);
                                                    confirmdialog.setView(dialog);

                                                    TextView agentname = dialog.findViewById(R.id.agentname);
                                                    agentname.setText("PvP created successfully.\n\nNote: You can start as soon as an agent joins the PvP.");

                                                    confirmdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            context.finish();
                                                            //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                                        }
                                                    });


                                                    confirmdialog.setCancelable(false);

                                                    if (!isFinishing()) {
                                                        confirmdialog.create().show();

                                                    }


                                                }


                                            }


                                        }

                                        // IS WITH LINK
                                        else {


                                            statustxt.setText("Link creating...");
                                            final Map<String, Object> pvplinking = new HashMap<>();
                                            pvplinking.put("/link", "AODs_" + pvpkey);
                                            pvplinking.put("/agents", 0);

                                            pvplinking.put("/title", snapshotinviteagents.child("pvptitle").getValue().toString());
                                            pvplinking.put("/pvpid", snapshotinviteagents.child("pvpid").getValue().toString());
                                            pvplinking.put("/badgeid", snapshotinviteagents.child("badgeid").getValue().toString());
                                            pvplinking.put("/badgecat", snapshotinviteagents.child("badgecat").getValue().toString());
                                            pvplinking.put("/createrimage", snapshotinviteagents.child("createrimage").getValue().toString());
                                            pvplinking.put("/timestamp", System.currentTimeMillis());
                                            pvplinking.put("/createruuid", user.getUid());
                                            mbpvplinks.child(snapshotinviteagents.child("pvplink").getValue().toString()).updateChildren(pvplinking).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        Log.i("Link crated", " true");
                                                        statustxt.setText("all done...");
                                                        //new AgentData("allowback", true);

                                                        AlertDialog.Builder confirmdialog = new AlertDialog.Builder(context, R.style.CustomDialog);

                                                        // builder.setMessage("Text not finished...");
                                                        LayoutInflater inflater = context.getLayoutInflater();
                                                        View dialog = inflater.inflate(R.layout.dialog_pvp_created, null);
                                                        confirmdialog.setView(dialog);

                                                        TextView agentname = dialog.findViewById(R.id.agentname);
                                                        agentname.setText("PvP Link created successfully. Share the link now with other agnets\n\nNote: You can start as soon as an agent joins the PvP.");

                                                        confirmdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                Intent shareIntent = ShareCompat.IntentBuilder.from(context)
                                                                        .setText("PvP invite from " + AgentData.getagentname() + "\nhttps://aod.prxenon.rocks/pvp/AODs_" + pvpkey)
                                                                        .setChooserTitle("Share this PvP with...")
                                                                        .setType("text/plain")
                                                                        .getIntent()
                                                                        .setAction(android.content.Intent.ACTION_SEND);

                                                                startActivityForResult(shareIntent, 393);

                                                                //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                                            }
                                                        });
                                                        confirmdialog.setNegativeButton("Copy Link and close", new DialogInterface.OnClickListener() {

                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                                                                ClipData clip = ClipData.newPlainText("PvP invite link", "https://aod.prxenon.rocks/pvp/AODs_" + snapshotinviteagents.child("pvpid").getValue().toString());
                                                                clipboard.setPrimaryClip(clip);

                                                                StyleableToast.makeText(getApplicationContext(), "PvP invite link copied to clipboard", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                context.finish();
                                                                //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                                            }
                                                        });


                                                        confirmdialog.setCancelable(true);

                                                        if (!isFinishing()) {
                                                            confirmdialog.create().show();

                                                        }

                                                        //  Log.w("ERROR ", this.getClass().getSimpleName() + " 5644 Error ");
                                                    } else if (task.isCanceled()) {

                                                    } else {
                                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                                                    }
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                                                    //signOut();
                                                }
                                            });


                                        }


                                    } else if (task.isCanceled()) {

                                    } else {
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                                    }
                                }


                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(Exception e) {
                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                                    //signOut();
                                }
                            });


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.i("DATA ERROR", databaseError.getMessage());
                        }
                    });

                    // ENDE LINK ERSTELLEN


                } else if (task.isCanceled()) {
                    Log.w("ERROR ", context.getClass().getSimpleName() + " 90441");

                } else {
                    Log.w("ERROR ", context.getClass().getSimpleName() + " 90552");

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w("ERROR ", context.getClass().getSimpleName() + " 90663");


                //signOut();
            }
        });
    }

    private void jointhepvp(final String thepvpkey, Context ctx, final Activity context, final String inboxid) {

        pvp_result.setVisibility(View.VISIBLE);


        final ImageView title_price = context.findViewById(R.id.title_price);
        // Runtime
        final TextView title_requests_count = context.findViewById(R.id.title_requests_count);
        RelativeLayout resultload = context.findViewById(R.id.resultload);
        LinearLayout resultcard = context.findViewById(R.id.resultcard);
        final TextView textinfo_invite = context.findViewById(R.id.textinfo_invite);
        final TextView title_date_label = context.findViewById(R.id.title_date_label);
        final MaterialAnimatedSwitch pin = context.findViewById(R.id.pin);
        final TextView title_to_address = context.findViewById(R.id.title_to_address);
        final TextView pvp_id_text = context.findViewById(R.id.pvp_id_text);
        final TextView pvp_art_text = context.findViewById(R.id.pvp_art_text);
        final CircleImageView ownerimage = context.findViewById(R.id.ownerimage);
        final TextView pvp_description = context.findViewById(R.id.pvp_description);
        final TextView textinfo_join = context.findViewById(R.id.textinfo_join);
        final TextView textinfo_reject = context.findViewById(R.id.textinfo_reject);
        resultload.setVisibility(View.GONE);
        resultcard.setVisibility(View.VISIBLE);

        fabinvite = context.findViewById(R.id.fabinvite);
        fabjoin = context.findViewById(R.id.fabjoin);
        fabreject = context.findViewById(R.id.fabreject);
        thejoinframe = context.findViewById(R.id.thejoinframe);
        theinviteframe = context.findViewById(R.id.theinviteframe);

        theinviteframe.setVisibility(View.GONE);
        thejoinframe.setVisibility(View.VISIBLE);
        // pvpupdate.put("/art", "linkshare");

        mbpvp.child(thepvpkey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshotpvpdata) {

                if (snapshotpvpdata.hasChild("pvpid")) {

                    if (!Boolean.parseBoolean(snapshotpvpdata.child("active").getValue().toString()) && Boolean.parseBoolean(snapshotpvpdata.child("finished").getValue().toString())) {
                        StyleableToast.makeText(PvPCreate.this, "This PvP is no longer available.", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        finish();

                    } else {

                        pvp_id_text.setText("ID: " + snapshotpvpdata.child("pvpid").getValue().toString());

                        if (snapshotpvpdata.child("art").getValue().toString().equalsIgnoreCase("linkshare")) {
                            if (snapshotpvpdata.child("createruuid").getValue().toString().equalsIgnoreCase(user.getUid())) {
                                pvp_art_text.setText("You can't join your own PvP");
                                textinfo_join.setVisibility(View.GONE);
                                StyleableToast.makeText(PvPCreate.this, "You can't join your own PvP", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                finish();
                            } else {

                                mDatabaserunnungpvp.child(thepvpkey).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshotrunning) {


                                        if (snapshotrunning.hasChild(user.getUid())) {
                                            pvp_art_text.setText("You have already joined this PvP");
                                            textinfo_join.setVisibility(View.GONE);
                                            fabreject.hide();
                                            textinfo_reject.setVisibility(View.GONE);
                                            StyleableToast.makeText(PvPCreate.this, "You have already joined this PvP", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                            finish();
                                        } else {
                                            pvp_art_text.setText("Invite agents via link from " + snapshotpvpdata.child("creater").getValue().toString());
                                            textinfo_join.setText("join now");
                                            fabreject.hide();
                                            textinfo_reject.setVisibility(View.GONE);
                                        }

                                    }


                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });

                            }
                        } else {
                            pvp_art_text.setText("PvP from " + snapshotpvpdata.child("creater").getValue().toString());
                            textinfo_join.setText("accept");

                            // SHOW FRIENDS
                        }

                        textinfo_invite.setVisibility(View.VISIBLE);
                        if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                            fabjoin.setBackgroundColor(context.getResources().getColor(R.color.colorENL, context.getTheme()));
                        } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                            fabjoin.setBackgroundColor(context.getResources().getColor(R.color.colorRES, context.getTheme()));
                        } else {
                            fabjoin.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryYellowDark, context.getTheme()));
                        }
                        fabjoin.show();


                        fabjoin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (snapshotpvpdata.child("art").getValue().toString().equalsIgnoreCase("linkshare")) {
                                    mbpvplinks.child("AODs_" + thepvpkey).child("agents").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot snapshotagents) {

                                            int joindagents = Integer.parseInt(snapshotagents.getValue().toString());


                                            //badgemonth = getResources().obtainTypedArray(badgemonthid);
                                            if (joindagents >= 3) {

                                                StyleableToast.makeText(context.getApplicationContext(), "Sorry, limit reached! Try another PvP Link", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                            } else {
                                                int updatejoined = joindagents + 1;
                                                mbpvplinks.child("AODs_" + thepvpkey).child("agents").setValue(updatejoined);
                                                therealpvpjoin(thepvpkey, snapshotpvpdata.child("art").getValue().toString(), context, snapshotpvpdata.child("createruuid").getValue().toString(), inboxid);
                                            }


                                        }


                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                            StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                        }
                                    });

                                } else {
                                    therealpvpjoin(thepvpkey, snapshotpvpdata.child("art").getValue().toString(), context, snapshotpvpdata.child("createruuid").getValue().toString(), inboxid);
                                }
                            }
                        });

                        fabreject.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                therealpvpreject(thepvpkey, snapshotpvpdata.child("art").getValue().toString(), context, snapshotpvpdata.child("createruuid").getValue().toString(), inboxid);

                            }
                        });

                        mDatabaseaod.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshotaod) {


                                int badgeweekid = context.getResources().getIdentifier(snapshotaod.child("badge").child(snapshotpvpdata.child("badgeid").getValue().toString()).child("drawable").getValue().toString(), "drawable", context.getPackageName());

                                //badgemonth = getResources().obtainTypedArray(badgemonthid);
                                if (badgeweekid != 0) {
                                    Log.w("BADGE -->", " id :" + badgeweekid);

                                    title_price.setImageDrawable(context.getDrawable(badgeweekid));
                                }

                                title_to_address.setText(snapshotaod.child("badge").child(snapshotpvpdata.child("badgeid").getValue().toString()).child("title").getValue().toString());
                                String[] action_text2 = context.getResources().getStringArray(R.array.badge_text_actions_id);
                                int action_value2 = Arrays.asList(action_text2).indexOf(snapshotpvpdata.child("badgeid").getValue().toString()); // pass value

                                String action_value_text_community2 = context.getResources().getStringArray(R.array.badge_text_actions_value)[action_value2];
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    pvp_description.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.challange_txt), action_value_text_community2, snapshotaod.child("badge").child(snapshotpvpdata.child("badgeid").getValue().toString()).child("short1").getValue().toString(), snapshotpvpdata.child("runtime_text").getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                                } else {
                                    pvp_description.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.challange_txt), action_value_text_community2, snapshotaod.child("badge").child(snapshotpvpdata.child("badgeid").getValue().toString()).child("short1").getValue().toString(), snapshotpvpdata.child("runtime_text").getValue().toString())));

                                }

                            }


                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                StyleableToast.makeText(context.getApplicationContext(), getString(R.string.erros_tost) + " " + databaseError.getMessage(), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            }
                        });


                        if (Boolean.parseBoolean(snapshotpvpdata.child("privacy").getValue().toString())) {
                            if (!pin.isChecked()) {
                                pin.toggle();
                            }

                            title_date_label.setText("PUBLIC PvP");


                            pin.setEnabled(false);
                            pin.setClickable(false);
                            pin.setActivated(false);


                        } else {
                            title_date_label.setText("PRIVATE PvP");
                            pin.setEnabled(false);
                            pin.setClickable(false);
                            pin.setActivated(false);
                        }


                        // SET RUNTIME
                        title_requests_count.setText(snapshotpvpdata.child("runtime_text").getValue().toString());

                        try {
                            PicassoCache.getPicassoInstance(context.getApplicationContext())
                                    .load(user.getProviderData().get(1).getPhotoUrl().toString())
                                    .placeholder(R.drawable.full_logo)
                                    .error(R.drawable.full_logo)


                                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                    //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                    //.transform(new CropRoundTransformation())
                                    .into(ownerimage);
                            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                        } catch (Exception e) {
                            //Log.e("Error", e.getMessage());

                            e.printStackTrace();
                        }


                        final FlowLayout myRootfriends = context.findViewById(R.id.my_friends_pvp);
                        myRootfriends.removeAllViews();
                        //   LinearLayout a = new LinearLayout(this);

                        myRootfriends.setOrientation(LinearLayout.HORIZONTAL);

                        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_pattern);
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

                        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

                        myRootfriends.setBackgroundDrawable(bitmapDrawable);

                        myRootfriends.removeAllViews();
                        myRootfriends.setOrientation(LinearLayout.HORIZONTAL);

                        final LayoutInflater inflater = LayoutInflater.from(context);


                        // set item content in view
                        Display display = context.getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        final int newwidth = ((size.x) / 6) - 10;

                        if (snapshotpvpdata.hasChild("agents")) {

                            for (DataSnapshot agentsinvited : snapshotpvpdata.child("agents").getChildren()) {
                                View viewx = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                                final OctogonImageView mybadge = viewx.findViewById(R.id.badgeimage);
                                mybadge.getLayoutParams().width = newwidth;
                                mybadge.getLayoutParams().height = newwidth;


                                final TextView friendname = viewx.findViewById(R.id.friendname);
                                String imagetoload;


                                if (agentsinvited.child("agentname").getValue().toString().equalsIgnoreCase(AgentData.getagentname())) {
                                    imagetoload = snapshotpvpdata.child("createrimage").getValue().toString();
                                    friendname.setText(snapshotpvpdata.child("creater").getValue().toString());
                                } else {
                                    imagetoload = agentsinvited.child("agentimage").getValue().toString() + "?sz=100";
                                    friendname.setText(agentsinvited.child("agentname").getValue().toString());
                                }


                                friendname.setSelected(true);

                                try {
                                    PicassoCache.getPicassoInstance(context.getApplicationContext())
                                            .load(imagetoload)
                                            .placeholder(R.drawable.full_logo)
                                            .error(R.drawable.full_logo)

                                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                                            //.transform(new CropRoundTransformation())
                                            .into(mybadge);
                                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                } catch (Exception e) {
                                    //Log.e("Error", e.getMessage());

                                    e.printStackTrace();
                                }


                                mybadge.setBorderWidth(4);
                                mybadge.setBorderColor(context.getColor(R.color.colorLightBrown));

                                mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                mybadge.setPadding(8, 8, 8, 8);

                                myRootfriends.addView(viewx);
                            }
                        }


                        if (snapshotpvpdata.child("art").getValue().toString().equalsIgnoreCase("linkshare")) {
                            View viewx = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                            final OctogonImageView mybadge = viewx.findViewById(R.id.badgeimage);
                            mybadge.getLayoutParams().width = newwidth;
                            mybadge.getLayoutParams().height = newwidth;
                            final TextView friendname = viewx.findViewById(R.id.friendname);

                            //friendname.setText(" ? ? ? ");

                            String imagetoload = snapshotpvpdata.child("createrimage").getValue().toString();
                            friendname.setText(snapshotpvpdata.child("creater").getValue().toString());
                            friendname.setSelected(true);

                            try {
                                PicassoCache.getPicassoInstance(context.getApplicationContext())
                                        .load(imagetoload)
                                        .placeholder(R.drawable.full_logo)
                                        .error(R.drawable.full_logo)

                                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                                        //.transform(new CropRoundTransformation())
                                        .into(mybadge);
                                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                            } catch (Exception e) {
                                //Log.e("Error", e.getMessage());

                                e.printStackTrace();
                            }


                            mybadge.setBorderWidth(4);
                            mybadge.setBorderColor(context.getColor(R.color.colorLightBrown));

                            mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                            mybadge.setPadding(8, 8, 8, 8);

                            myRootfriends.addView(viewx);


                            View viewx2 = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                            final OctogonImageView mybadge2 = viewx2.findViewById(R.id.badgeimage);
                            mybadge2.getLayoutParams().width = newwidth;
                            mybadge2.getLayoutParams().height = newwidth;
                            final TextView friendname2 = viewx2.findViewById(R.id.friendname);

                            friendname2.setText(" ? ? ? ");
                            friendname2.setSelected(true);

                            try {
                                PicassoCache.getPicassoInstance(context.getApplicationContext())
                                        .load(R.drawable.full_logo)
                                        .placeholder(R.drawable.full_logo)
                                        .error(R.drawable.full_logo)

                                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                                        //.transform(new CropRoundTransformation())
                                        .into(mybadge2);
                                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                            } catch (Exception e) {
                                //Log.e("Error", e.getMessage());

                                e.printStackTrace();
                            }


                            mybadge2.setBorderWidth(4);
                            mybadge2.setBorderColor(context.getColor(R.color.colorLightBrown));

                            mybadge2.setScaleType(ImageView.ScaleType.FIT_CENTER);
                            mybadge2.setPadding(8, 8, 8, 8);

                            myRootfriends.addView(viewx2);


                            View viewx3 = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                            final OctogonImageView mybadge3 = viewx3.findViewById(R.id.badgeimage);
                            mybadge3.getLayoutParams().width = newwidth;
                            mybadge3.getLayoutParams().height = newwidth;
                            final TextView friendname3 = viewx3.findViewById(R.id.friendname);

                            friendname3.setText(" ? ? ? ");
                            friendname3.setSelected(true);

                            try {
                                PicassoCache.getPicassoInstance(context.getApplicationContext())
                                        .load(R.drawable.full_logo)
                                        .placeholder(R.drawable.full_logo)
                                        .error(R.drawable.full_logo)

                                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                                        //.transform(new CropRoundTransformation())
                                        .into(mybadge3);
                                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                            } catch (Exception e) {
                                //Log.e("Error", e.getMessage());

                                e.printStackTrace();
                            }


                            mybadge3.setBorderWidth(4);
                            mybadge3.setBorderColor(context.getColor(R.color.colorLightBrown));

                            mybadge3.setScaleType(ImageView.ScaleType.FIT_CENTER);
                            mybadge3.setPadding(8, 8, 8, 8);

                            myRootfriends.addView(viewx3);
                        }


                        fabinvite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // saveandsharePvP(pvpkey,pvpupdate,textinfo,fabsave,fabreset, friendsbuilder, ad, pvp_frame, pvp_result, context);
                                //StyleableToast.makeText(context.getApplicationContext(), "in the next update, promised!!!", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }
                        });
                    }

                } else {
                    StyleableToast.makeText(PvPCreate.this, "PvP not found. ", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {


            }
        });


    }

    private void therealpvpjoin(final String thepvpkey, String art, final Activity context, final String owneruid, final String inboxid) {

        //String.valueOf(System.currentTimeMillis()
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);

        final TextView statustxt = context.findViewById(R.id.status_txt);
        //statustxt.setText(context.getResources().getString(R.string.string_pleaswait));
        new AgentData("allowback", false);
        statustxt.setText("please wait...");

        final Map<String, Object> agentjoin = new HashMap<>();


        agentjoin.put("/currentvalue", 0);
        agentjoin.put("/started", false);
        agentjoin.put("/owner", false);

        agentjoin.put("/googleid", user.getProviderData().get(1).getUid());
        agentjoin.put("/timestamp", System.currentTimeMillis());
        agentjoin.put("/finished", false);
        agentjoin.put("/lastvalue", 0);
        agentjoin.put("/startvalue", 0);
        agentjoin.put("/pvpkey", thepvpkey);

        final String pushkey = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        mDatabase.child(user.getUid()).child("pushkey").setValue(pushkey);

        mDatabaserunnungpvp.child(thepvpkey).child(user.getUid()).updateChildren(agentjoin).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.i("joined", " true");
                    statustxt.setText("Pvp joined...");
                    mbpvp.child(thepvpkey).child("active").setValue(true);
                    //new AgentData("allowback", true);

                    mDatabase.child(user.getUid()).child("pvp").child(thepvpkey).child("timestamp").setValue((System.currentTimeMillis())).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                mDatabase.child(user.getUid()).child("pvp").child(thepvpkey).child("finished").setValue(false);
                                if (!inboxid.equalsIgnoreCase("null")) {
                                    mDatabase.child(user.getUid()).child("newinbox").child(inboxid).setValue(null);
                                }
                                final Map<String, Object> agentinbox = new HashMap<>();


                                agentinbox.put("/text", AgentData.getagentname());
                                agentinbox.put("/link", thepvpkey);
                                agentinbox.put("/inviteposition", 0);

                                agentinbox.put("/title", "PvP accepted");
                                agentinbox.put("/timestamp", System.currentTimeMillis());
                                agentinbox.put("/read", false);

                                agentinbox.put("/fromuid", user.getUid());
                                mDatabase.child(owneruid).child("newinbox").child(String.valueOf(System.currentTimeMillis())).updateChildren(agentinbox);
                                statustxt.setText("sending notification to owner...");
                                new SendPushinBgaccept(pushkey, "pvpaccepted", AgentData.getagentname(), owneruid, "PvP accepted", AgentData.getagentname() + " has accepted your PvP invitation.").execute();

                                AlertDialog.Builder confirmdialog = new AlertDialog.Builder(context, R.style.CustomDialog);

                                // builder.setMessage("Text not finished...");
                                LayoutInflater inflater = context.getLayoutInflater();
                                View dialog = inflater.inflate(R.layout.dialog_pvp_joined, null);
                                confirmdialog.setView(dialog);

                                TextView agentname = dialog.findViewById(R.id.agentname);
                                agentname.setText("PvP successfully joined. Upload your start values\n\nNote: Your time starts as soon as your starting values have been read.");

                                statustxt.setText("all done...");

                                coordinator.setVisibility(View.VISIBLE);
                                progressBarHolder.setVisibility(View.GONE);
                                confirmdialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        if (fromlink) {
                                            Intent ix = new Intent(PvPCreate.this, AODMain.class);
                                            //i.putExtra("PersonID", personID);
                                            startActivity(ix);
                                            finish();
                                        } else {
                                            finish();
                                        }
                                        //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                                    }
                                });


                                confirmdialog.setCancelable(false);

                                if (!isFinishing()) {
                                    confirmdialog.create().show();

                                }
                            } else if (task.isCanceled()) {

                            } else {
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                            }
                        }


                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                            //signOut();
                        }
                    });


                    //  Log.w("ERROR ", this.getClass().getSimpleName() + " 5644 Error ");
                } else if (task.isCanceled()) {

                } else {
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                //signOut();
            }
        });
    }


    private void therealpvpreject(final String thepvpkey, String art, final Activity context, final String owneruid, final String inboxid) {

        //String.valueOf(System.currentTimeMillis()
        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);

        final TextView statustxt = context.findViewById(R.id.status_txt);
        //statustxt.setText(context.getResources().getString(R.string.string_pleaswait));
        new AgentData("allowback", false);
        statustxt.setText("please wait...");
        final String pushkey = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();


        if (!inboxid.equalsIgnoreCase("null")) {
            mDatabase.child(user.getUid()).child("newinbox").child(inboxid).setValue(null);
        }
        final Map<String, Object> agentinbox = new HashMap<>();


        agentinbox.put("/text", AgentData.getagentname());
        agentinbox.put("/link", thepvpkey);
        agentinbox.put("/inviteposition", 0);

        agentinbox.put("/title", "PvP accepted");
        agentinbox.put("/timestamp", System.currentTimeMillis());
        agentinbox.put("/read", false);

        agentinbox.put("/fromuid", user.getUid());
        mDatabase.child(owneruid).child("newinbox").child(String.valueOf(System.currentTimeMillis())).updateChildren(agentinbox);
        statustxt.setText("sending notification to owner...");
        new SendPushinBgaccept(pushkey, "pvprejected", AgentData.getagentname(), owneruid, "PvP rejected", AgentData.getagentname() + " has rejected your PvP invitation.").execute();

        AlertDialog.Builder confirmdialog = new AlertDialog.Builder(context, R.style.CustomDialog);

        // builder.setMessage("Text not finished...");
        LayoutInflater inflater = context.getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_pvp_joined, null);
        confirmdialog.setView(dialog);

        TextView agentname = dialog.findViewById(R.id.agentname);
        agentname.setText("PvP successfully joined. Upload your start values\n\nNote: Your time starts as soon as your starting values have been read.");

        statustxt.setText("all done...");

        coordinator.setVisibility(View.VISIBLE);
        progressBarHolder.setVisibility(View.GONE);

        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 393) {
            System.out.println("Share done");
            finish();

        }
    }

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    //GET AGENTS DETAILS WITH TOKEN
    private class SendPushinBginvite extends AsyncTask<String, Void, String> {
        //sendpushaccepted
        String thepushid, theart, theagent, sendtoa, titleof, runtimetext;

        public SendPushinBginvite(String s, String art, String agentname, String sendTo, String titleofd, String runtimetextd) {
            thepushid = s;
            theart = art;
            theagent = agentname;
            sendtoa = sendTo;
            titleof = titleofd;
            runtimetext = runtimetextd;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {


            String regurl = BuildConfig.BASEURL + "sendpush_pvp_invite.php?pw=" + thepushid + "&art=" + theart + "&agent=" + theagent + "&uid=" + user.getUid() + "&to=" + sendtoa + "&title=" + titleof + "&runtimetext=" + runtimetext;


            StringBuilder sb = null;
            BufferedReader reader = null;
            String serverResponse = null;
            try {

                URL url = new URL(regurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(200);
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                //Log.e("statusCode", "" + statusCode);
                if (statusCode == 200) {
                    sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                }

                connection.disconnect();
                if (sb != null)
                    serverResponse = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return serverResponse;
        }

        @Override
        protected void onPostExecute(String json) {

            if (json != null) {

                Log.i("RESULT Push", " -->" + json);
            }
        }
    }

    //GET AGENTS DETAILS WITH TOKEN
    private class SendPushinBgaccept extends AsyncTask<String, Void, String> {
        //sendpushaccepted
        String thepushid, theart, theagent, sendtoa, titleof, runtimetext;

        public SendPushinBgaccept(String s, String art, String agentname, String sendTo, String titleofd, String runtimetextd) {
            thepushid = s;
            theart = art;
            theagent = agentname;
            sendtoa = sendTo;
            titleof = titleofd;
            runtimetext = runtimetextd;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {


            String regurl = BuildConfig.BASEURL + "sendpush_pvp_accept.php?pw=" + thepushid + "&art=" + theart + "&agent=" + theagent + "&uid=" + user.getUid() + "&to=" + sendtoa + "&title=" + titleof + "&runtimetext=" + runtimetext;


            StringBuilder sb = null;
            BufferedReader reader = null;
            String serverResponse = null;
            try {

                URL url = new URL(regurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(200);
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                //Log.e("statusCode", "" + statusCode);
                if (statusCode == 200) {
                    sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                }

                connection.disconnect();
                if (sb != null)
                    serverResponse = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return serverResponse;
        }

        @Override
        protected void onPostExecute(String json) {

            if (json != null) {

                Log.i("RESULT Push", " -->" + json);
            }
        }
    }
}