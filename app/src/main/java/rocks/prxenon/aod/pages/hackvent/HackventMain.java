package rocks.prxenon.aod.pages.hackvent;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;

import com.github.siyamed.shapeimageview.OctogonImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.perf.metrics.AddTrace;
import com.marcoscg.xmassnow.XmasSnow;
import com.muddzdev.styleabletoast.StyleableToast;

import org.apmem.tools.layouts.FlowLayout;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import rocks.prxenon.aod.BuildConfig;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.receiver.NetworkStateReceiver;

import static rocks.prxenon.aod.basics.AODConfig.mhackvent;
import static rocks.prxenon.aod.basics.AODConfig.networkStateReceiver;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;


public class HackventMain extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private static final String ALLOWED_CHARACTERS = AgentData.getagentname().toUpperCase();
    public static FrameLayout days_container;

    final List<String> friendid = new ArrayList();
    final List<Boolean> daystatus = new ArrayList();
    public long allmydays;
    public long allopenrequests;
    private Typeface type, proto;
    private TextView mTitleTextView;
    private ViewPager mPager;
    private AlertDialog.Builder dialogBuilder;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    private static String getRandomString(final int sizeOfRandomString) {
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sizeOfRandomString; ++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));

        return sb.toString();
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_hackvent);

        bindActivity();
        new AODConfig("default", this);
        //Get Firebase auth instance

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);


        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");

        allmydays = 0;
        allopenrequests = 0;
        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);

        days_container = findViewById(R.id.days_container);


        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_hackvent, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText("HACKVENT");


        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);


    }

    private void bindActivity() {


    }

    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();
        pref = getSharedPreferences("AppPref", MODE_PRIVATE);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        days_container.setVisibility(View.GONE);

        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);


        XmasSnow.on(this)
                .belowActionBar(true)
                .belowStatusBar(true) // Always true if belowActionBar() is set to true
                .onlyOnChristmas(false) // Only the 25th of december
                .setInterval("11/28/2019", "1/7/2020") // 25th of december to 7th of january (not included). Date format: MM/dd/yyyy
                .start();

        loaddays();


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");
        //pref.edit().remove("scanedagnet").apply();

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }


        try {
            if (networkStateReceiver != null) {
                this.unregisterReceiver(networkStateReceiver);
                Log.i("", "broadcastReceiver unregistered");
            }
        } catch (Exception e) {
            Log.i("", "broadcastReceiver is already unregistered");
            networkStateReceiver = null;
        }


    }

    @Override
    public void networkAvailable() {




        /* TODO: Your connection-oriented stuff here */
    }

    @Override
    public void networkUnavailable() {



        /* TODO: Your disconnection-oriented stuff here */
    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();


    }

    private String getCharForNumber(int i) {
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        Log.i("NUM2 -->", ":" + i);
        if (i > 25) {
            return null;
        }
        return Character.toString(alphabet[i]);
    }

    private void loaddays() {
        friendid.clear();
        daystatus.clear();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int newwidth = ((size.x) / 5) - 10;


        final FlowLayout myRootdays = findViewById(R.id.the_days);
        myRootdays.removeAllViews();
        //   LinearLayout a = new LinearLayout(this);

        myRootdays.setOrientation(LinearLayout.HORIZONTAL);

        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg_pattern_hackvent);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        myRootdays.setBackgroundDrawable(bitmapDrawable);

        final LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
      /*  View view2 = inflater.inflate(R.layout.list_nodays_adapter, myRootdays, false);
        // set item content in view
        final TextView mybadge = view2.findViewById(R.id.nodays);
        mybadge.setText("please wait...");
        myRootdays.addView(view2);

       */


        // START CALENDER


        final int sizex = 25;

        if (sizex > 0) {

            for (int i = 1; i < sizex; i++) {

                final int finalI = i;


                int ixx = i - 1;
                mhackvent.child("2019").child("" + ixx).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshothackvent) {
// START DAYS
                        View view = inflater.inflate(R.layout.list_days_adapter, myRootdays, false);
                        // set item content in view
                        final OctogonImageView mybadgeimg = view.findViewById(R.id.badgeimage);
                        mybadgeimg.getLayoutParams().width = newwidth;
                        mybadgeimg.getLayoutParams().height = newwidth;
                        final TextView friendname = view.findViewById(R.id.daysname);

                        friendname.setText("" + finalI);
                        friendname.setSelected(true);

                        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
                        SimpleDateFormat fday = new SimpleDateFormat("dd");
                        final String themonth = fmonth.format(new Date());
                        final String theday = fday.format(new Date());

                        if (Integer.parseInt(themonth) == 12 && (Integer.parseInt(theday) >= finalI && Integer.parseInt(theday) < 25)) {
                            String ddtag;
                            if (finalI <= 9) {
                                ddtag = "0" + finalI;
                            } else {
                                ddtag = String.valueOf(finalI);
                            }
                            {
                                try {
                                    PicassoCache.getPicassoInstance(getApplicationContext())
                                            .load("https://advent.missionday.info/_res/dayimg/2019_Day_" + ddtag + ".jpg")
                                            .placeholder(R.drawable.hackvent)
                                            .error(R.drawable.hackvent)

                                            // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                            // .networkPolicy(NetworkPolicy.NO_CACHE)
                                            //.transform(new CropRoundTransformation())
                                            .into(mybadgeimg);
                                    // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                } catch (Exception e) {
                                    //Log.e("Error", e.getMessage());

                                    e.printStackTrace();
                                }
                            }
                        }


                        mybadgeimg.setBorderWidth(5);
                        mybadgeimg.setBorderColor(getColor(R.color.hackvent_green_dark));

                        mybadgeimg.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        mybadgeimg.setPadding(8, 8, 8, 8);

                        mybadgeimg.setOnLongClickListener(new View.OnLongClickListener() {

                            @Override
                            public boolean onLongClick(View v) {
                                Log.i("LOOONGPRESS", "true ");
                                //   removeFriend(friendid.get(finalI), snapshotagentimage.getValue().toString());
                                return true;
                            }
                        });


                        if (Integer.parseInt(themonth) == 12 && (Integer.parseInt(theday) >= finalI && Integer.parseInt(theday) < 25)) {
                            mybadgeimg.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //your stuff
                                    // Log.i("SinglePress", "true");
                                    final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(HackventMain.this);
                                    View sheetView = getLayoutInflater().inflate(R.layout.dialog_days_action, null);
                                    mBottomSheetDialog.setContentView(sheetView);
                                    mBottomSheetDialog.show();


                                    LinearLayout action_opena = sheetView.findViewById(R.id.daysaction_opena);
                                    action_opena.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            mBottomSheetDialog.dismiss();
                                            AlertDialog.Builder builder = new AlertDialog.Builder(HackventMain.this, R.style.CustomDialog);

                                            // builder.setMessage("Text not finished...");
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialog = inflater.inflate(R.layout.dialog_hackventmission, null);
                                            builder.setView(dialog);
                                            final TextView inputm = dialog.findViewById(R.id.missionis);

                                            inputm.setTypeface(type);
                                            inputm.setText("MISSION A - " + snapshothackvent.child("title-EN").getValue().toString());
                                            final TextView input = dialog.findViewById(R.id.caltext);
                                            input.setTypeface(type);
                                            input.setText(snapshothackvent.child("text1-EN").getValue().toString());
                                            builder.setPositiveButton(getString(R.string.string_hacksend), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Todo publish the code to database
                                                    // Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                    //i.putExtra("PersonID", personID);
                                                    //startActivity(i);
                                                    //finish();
                                                    senddata("task1");

                                                }
                                            });

                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Todo publish the code to database
                                                }
                                            });

                                            builder.setCancelable(true);

                                            if (!isFinishing()) {
                                                builder.create().show();

                                            }
                                        }
                                    });

                                    LinearLayout action_openb = sheetView.findViewById(R.id.daysaction_openb);
                                    action_openb.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            mBottomSheetDialog.dismiss();

                                            AlertDialog.Builder builder = new AlertDialog.Builder(HackventMain.this, R.style.CustomDialog);

                                            // builder.setMessage("Text not finished...");
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialog = inflater.inflate(R.layout.dialog_hackventmission, null);
                                            builder.setView(dialog);
                                            final TextView inputm = dialog.findViewById(R.id.missionis);

                                            inputm.setTypeface(type);
                                            inputm.setText("MISSION B - " + snapshothackvent.child("title-EN").getValue().toString());
                                            final TextView input = dialog.findViewById(R.id.caltext);
                                            input.setTypeface(type);
                                            input.setText(snapshothackvent.child("text2-EN").getValue().toString());
                                            builder.setPositiveButton(getString(R.string.string_hacksend), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Todo publish the code to database
                                                    // Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                    //i.putExtra("PersonID", personID);
                                                    //startActivity(i);
                                                    //finish();

                                                    senddata("task2");

                                                }
                                            });

                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    // Todo publish the code to database
                                                }
                                            });

                                            builder.setCancelable(true);

                                            if (!isFinishing()) {
                                                builder.create().show();

                                            }
                                            // StyleableToast.makeText(getApplicationContext(), "soon", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                        }
                                    });

                                    LinearLayout action_openweb = sheetView.findViewById(R.id.daysaction_openweb);
                                    action_openweb.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            mBottomSheetDialog.dismiss();
                                            String url = "https://advent.missionday.info/";
                                            Intent i = new Intent(Intent.ACTION_VIEW);
                                            i.setData(Uri.parse(url));
                                            startActivity(i);
                                        }
                                    });

                                }
                            });
                        } else {

                            mybadgeimg.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    StyleableToast.makeText(getApplicationContext(), "Day is in the future", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                }
                            });

                        }
                        myRootdays.addView(view);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                        // StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                    }
                });

                if (ixx >= 20) {
                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);
                }
                // END DAYS


            }
        } else {
            StyleableToast.makeText(getApplicationContext(), "no days", Toast.LENGTH_LONG, R.style.defaulttoast).show();
            coordinator.setVisibility(View.VISIBLE);
            progressBarHolder.setVisibility(View.GONE);
        }

        // END KALENDER


    }

    private void senddata(String taskis) {

        String FILENAME = "clipboard-history.tsv";

        File mHistoryFile;

        String line;

        BufferedReader reader = null;
        StringBuilder text = new StringBuilder();
        String[] columns; //contains column names
        int num_cols;
        String[] tokens;
        JSONObject agentstats = new JSONObject();
        JSONObject userdata = new JSONObject();
        mHistoryFile = new File(getExternalFilesDir(null), FILENAME);
        try {
            //Get the CSVReader instance with specifying the delimiter to be used
            reader = new BufferedReader(new FileReader(mHistoryFile), ',');

            line = reader.readLine();

            columns = line.split("\t");
            num_cols = columns.length;

            line = reader.readLine();


            while (true) {
                tokens = line.split("\t");

                if (tokens.length == num_cols) { //if number columns equal to number entries


                    for (int k = 0; k < num_cols; ++k) { //for each column

                        if (tokens[k].equalsIgnoreCase("ALL TIME") || tokens[k].equalsIgnoreCase("GESAMT") || tokens[k].equalsIgnoreCase("TUDO") || tokens[k].equalsIgnoreCase("SIEMPRE") || tokens[k].equalsIgnoreCase("ЗА ВСЕ ВРЕМЯ") || tokens[k].equalsIgnoreCase("ALLE") || tokens[k].equalsIgnoreCase("TOUS TEMPS") || tokens[k].equalsIgnoreCase("OD POCZĄTKU") || tokens[k].equalsIgnoreCase("DALL'INIZIO") || tokens[k].equalsIgnoreCase("全部") || tokens[k].equalsIgnoreCase("Celé období") || tokens[k].equalsIgnoreCase("全期間")) {

                            agentstats.put(columns[k].trim().replaceAll("[-+.^:,()\\s+]", "").toLowerCase(), "ALLTIME");
                        } else {
                            if (isNumeric(tokens[k])) {
                                agentstats.put(columns[k].trim().replaceAll("[-+.^:,()\\s+]", "").toLowerCase(), Integer.parseInt(tokens[k]));
                            } else {
                                agentstats.put(columns[k].trim().replaceAll("[-+.^:,()\\s+]", "").toLowerCase(), tokens[k]);
                            }
                        }
                        // Log.i("DATA -->", columns[k].trim().replaceAll("[-+.^:,()\\s+]","").toLowerCase() +" " +tokens[k]);


                    }


                    if ((line = reader.readLine()) != null) {//if not last line

                    } else {
                        Log.i("Loop finished", "done ");
                        Log.i("user-->", user.getEmail().toLowerCase());
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.getDefault());
                        String formattedDate = sdf.format(new Date());

                        userdata.put("username", user.getEmail().toLowerCase());
                        userdata.put("givenname", user.getDisplayName());
                        userdata.put("timestamp", formattedDate);

                        userdata.put("task", taskis);


                        userdata.put("agentstats", agentstats);

                        Log.i("STATS ", userdata.toString());


                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                                .permitAll().build();
                        StrictMode.setThreadPolicy(policy);


                        try {

                            HttpURLConnection connection = null;
                            URL object = new URL("https://advent.missionday.info/aodIngest");
                            // URL object = new URL("https://advent19.missionday.info/aodIngest");

                            connection = (HttpURLConnection) object.openConnection();
                            connection.setRequestProperty("Accept", "application/json");
                            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                            connection.setRequestProperty("User-Agent", "AOD agent - " + AgentData.getagentname());
                            connection.setReadTimeout(15000);
                            connection.setConnectTimeout(15000);
                            connection.setRequestMethod("POST");
                            connection.setDoInput(true);
                            connection.setDoOutput(true);


                            OutputStream os = connection.getOutputStream();
                            BufferedWriter writer = new BufferedWriter(
                                    new OutputStreamWriter(os, StandardCharsets.UTF_8));
                            writer.write(userdata.toString());
                            writer.flush();
                            writer.close();
                            os.close();

                            // connection.connect();

                            String responseMsg = connection.getResponseMessage();

                            int responsecode = connection.getResponseCode();

                            // connection.disconnect();

                            Log.i("code ", " " + responsecode);
                            Log.i("RESPONSE ", " " + responseMsg);


                            if (responsecode == 200) {
                                try {


                                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));


                                    StringBuffer jsonString = new StringBuffer();
                                    String linec2;
                                    while ((linec2 = br.readLine()) != null) {
                                        jsonString.append(linec2);
                                    }
                                    br.close();

                                    JSONObject resultof = new JSONObject(jsonString.toString());
                                    String statusis = resultof.getString("status");
                                    if (resultof.getString("update") != null) {
                                        JSONObject resultofdetails = new JSONObject(resultof.getString("update"));

                                        if (resultofdetails.getString("msg").equalsIgnoreCase("This is not enough.")) {
                                            StyleableToast.makeText(getApplicationContext(), "Data successfully updated but its not enough for this task. " + responseMsg, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                        } else {
                                            StyleableToast.makeText(getApplicationContext(), "Data successfully updated and Mission done! " + resultofdetails.getString("msg"), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                        }
                                    } else if (resultof.getString("update") == null) {
                                        StyleableToast.makeText(getApplicationContext(), "Data successfully sent. Status: " + statusis, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                    }

                                    Log.d("test", "result from server: " + resultof.toString());

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                            } else if (responsecode == 401) {
                                StyleableToast.makeText(getApplicationContext(), "no payload " + responseMsg, Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            } else if (responsecode == 406) {
                                StyleableToast.makeText(getApplicationContext(), "No data found or data is too old. " + responseMsg, Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            } else {
                                StyleableToast.makeText(getApplicationContext(), "some other error #7785 " + responseMsg, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }

                            connection.disconnect();

                        } catch (Exception e) {

                            Log.e("Error = ", e.toString());
                        }


                    }
                } else {
                    //line = read.readLine(); //read next line if wish to continue parsing despite error

                }

                break;
            }


            reader.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    //GET AGENTS DETAILS WITH TOKEN
    private class SendPushinBg extends AsyncTask<String, Void, String> {
        //sendpushaccepted
        String thepushid, theart, theagent, sendtoa;

        public SendPushinBg(String s, String art, String agentname, String sendTo) {
            thepushid = s;
            theart = art;
            theagent = agentname;
            sendtoa = sendTo;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {


            String regurl = BuildConfig.BASEURL + "sendpush.php?pw=" + thepushid + "&art=" + theart + "&agent=" + theagent + "&uid=" + user.getUid() + "&to=" + sendtoa;
            StringBuilder sb = null;
            BufferedReader reader = null;
            String serverResponse = null;
            try {

                URL url = new URL(regurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(200);
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                //Log.e("statusCode", "" + statusCode);
                if (statusCode == 200) {
                    sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                }

                connection.disconnect();
                if (sb != null)
                    serverResponse = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return serverResponse;
        }

        @Override
        protected void onPostExecute(String json) {

            if (json != null) {


            }
        }
    }
}