package rocks.prxenon.aod.pages;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.viewpager.widget.ViewPager;

import com.github.siyamed.shapeimageview.OctogonImageView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.perf.metrics.AddTrace;
import com.muddzdev.styleabletoast.StyleableToast;

import org.apmem.tools.layouts.FlowLayout;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.BuildConfig;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.basics.SaveFriendScan;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.pages.pvp.PvPCreate;
import rocks.prxenon.aod.receiver.NetworkStateReceiver;
import rocks.prxenon.aod.settings.MainSettings;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mbfriendsrequest;
import static rocks.prxenon.aod.basics.AODConfig.networkStateReceiver;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;
import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;
import static rocks.prxenon.aod.helper.GetNotifications.newagentsprofile;
import static rocks.prxenon.aod.helper.GetNotifications.newagentsprofileownrequest;


public class FriendsMain extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    private static final String ALLOWED_CHARACTERS = AgentData.getagentname().toUpperCase();
    public static FrameLayout friends_container;

    final List<String> friendid = new ArrayList();
    final List<Boolean> friendstatus = new ArrayList();
    public long allmyfriends;
    public long allopenrequests;
    private Typeface type, proto;
    private TextView mTitleTextView;
    private ViewPager mPager;
    private AlertDialog.Builder dialogBuilder;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    private static String getRandomString(final int sizeOfRandomString) {
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sizeOfRandomString; ++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));

        return sb.toString();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true/*Optional*/)
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // set the view now
        setContentView(R.layout.activity_freinds);

        bindActivity();
        new AODConfig("default", this);
        //Get Firebase auth instance

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);


        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");

        allmyfriends = 0;
        allopenrequests = 0;
        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);

        friends_container = findViewById(R.id.friends_container);


        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_noaction, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(R.string.string_friends);


        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);


    }

    private void bindActivity() {


    }


    @Override
    public void onResume() {
        super.onResume();


        new AODConfig();
        pref = getSharedPreferences("AppPref", MODE_PRIVATE);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        friends_container.setVisibility(View.GONE);

        coordinator.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);


        loadfriends();


    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("Start", "901");
        //pref.edit().remove("scanedagnet").apply();

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONStop ROOT", "STOP");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (newagentsprofile != null) {
            mbfriendsrequest.removeEventListener(newagentsprofile);

        }

        if (newagentsprofileownrequest != null) {
            mDatabase.removeEventListener(newagentsprofileownrequest);

        }


    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPAUSE ROOT", "PAUSE");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }

        if (newagentsprofile != null) {
            mbfriendsrequest.removeEventListener(newagentsprofile);

        }

        if (newagentsprofileownrequest != null) {
            mDatabase.removeEventListener(newagentsprofileownrequest);

        }


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");
        if (networkStateReceiver != null) {
            try {
                this.unregisterReceiver(networkStateReceiver);
                networkStateReceiver.removeListener(this);
            } catch (Exception e) {
                Log.i("ONStop ROOT", e.getMessage());
            }
        }


        try {
            if (networkStateReceiver != null) {
                this.unregisterReceiver(networkStateReceiver);
                Log.i("", "broadcastReceiver unregistered");
            }
        } catch (Exception e) {
            Log.i("", "broadcastReceiver is already unregistered");
            networkStateReceiver = null;
        }


    }

    @Override
    public void networkAvailable() {




        /* TODO: Your connection-oriented stuff here */
    }

    @Override
    public void networkUnavailable() {



        /* TODO: Your disconnection-oriented stuff here */
    }


    @Override
    public void onBackPressed() {


        super.onBackPressed();


    }


    private String getCharForNumber(int i) {
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        Log.i("NUM2 -->", ":" + i);
        if (i > 25) {
            return null;
        }
        return Character.toString(alphabet[i]);
    }


    private void loadfriends() {
        friendid.clear();
        friendstatus.clear();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int newwidth = ((size.x) / 5) - 10;


        final FlowLayout myRootfriends = findViewById(R.id.my_friends);
        myRootfriends.removeAllViews();
        //   LinearLayout a = new LinearLayout(this);

        myRootfriends.setOrientation(LinearLayout.HORIZONTAL);

        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg_pattern);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        myRootfriends.setBackgroundDrawable(bitmapDrawable);

        final LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View view2 = inflater.inflate(R.layout.list_nofriends_adapter, myRootfriends, false);
        // set item content in view
        final TextView mybadge = view2.findViewById(R.id.nofriends);
        mybadge.setText("please wait...");
        myRootfriends.addView(view2);
        mDatabase.child(user.getUid()).child("friends").orderByChild("timestamp").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshotfriends) {

                if (snapshotfriends.hasChildren()) {

                    for (DataSnapshot child : snapshotfriends.getChildren()) {

                        // TODO CRASH IF failed

                        if (child.child("agentuid").exists()) {
                            friendid.add(child.child("agentuid").getValue().toString());
                            friendstatus.add(Boolean.parseBoolean(child.child("status").getValue().toString()));
                        }


                        //  Log.i("TEST LOG--->",child.getValue().toString());

                    }

                    Collections.reverse(friendid);
                    Collections.reverse(friendstatus);

                    final int sizex = friendid.size();

                    if (sizex > 0) {

                        for (int i = 0; i < sizex; i++) {

                            final int finalI = i;
                            mDatabase.child(friendid.get(finalI)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(final DataSnapshot snapshotfriendsdetails) {

                                    if (snapshotfriendsdetails.exists()) {
                                        myRootfriends.removeAllViews();
                                        myRootfriends.setOrientation(LinearLayout.HORIZONTAL);
                                        if (friendstatus.get(finalI)) {
                                            mDatabase.child(friendid.get(finalI)).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(final DataSnapshot snapshotagentimage) {
                                                    View view = inflater.inflate(R.layout.list_friends_adapter, myRootfriends, false);
                                                    // set item content in view
                                                    final OctogonImageView mybadge = view.findViewById(R.id.badgeimage);
                                                    mybadge.getLayoutParams().width = newwidth;
                                                    mybadge.getLayoutParams().height = newwidth;
                                                    final TextView friendname = view.findViewById(R.id.friendname);

                                                    friendname.setText(snapshotfriendsdetails.getValue().toString());
                                                    friendname.setSelected(true);

                                                    try {
                                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                                .load(snapshotagentimage.getValue().toString() + "?sz=200")
                                                                .placeholder(R.drawable.full_logo)
                                                                .error(R.drawable.full_logo)

                                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                                                //.transform(new CropRoundTransformation())
                                                                .into(mybadge);
                                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                                    } catch (Exception e) {
                                                        //Log.e("Error", e.getMessage());

                                                        e.printStackTrace();
                                                    }


                                                    mybadge.setBorderWidth(4);
                                                    mybadge.setBorderColor(getColor(R.color.colorLightBrown));

                                                    mybadge.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                                    mybadge.setPadding(8, 8, 8, 8);

                                                    mybadge.setOnLongClickListener(new View.OnLongClickListener() {

                                                        @Override
                                                        public boolean onLongClick(View v) {
                                                            Log.i("LOOONGPRESS", "true ");
                                                            //   removeFriend(friendid.get(finalI), snapshotagentimage.getValue().toString());
                                                            return true;
                                                        }
                                                    });
                                                    mybadge.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View v) {
                                                            //your stuff
                                                            // Log.i("SinglePress", "true");
                                                            final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(FriendsMain.this);
                                                            View sheetView = getLayoutInflater().inflate(R.layout.dialog_friends_action, null);
                                                            mBottomSheetDialog.setContentView(sheetView);
                                                            mBottomSheetDialog.show();

                                                            LinearLayout action_del = sheetView.findViewById(R.id.friendaction_delete);
                                                            action_del.setOnClickListener(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View view) {
                                                                    mBottomSheetDialog.dismiss();
                                                                    //StyleableToast.makeText(getApplicationContext(), "DEL", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                    removeFriend(friendid.get(finalI), snapshotagentimage.getValue().toString());
                                                                }
                                                            });

                                                            LinearLayout action_pvp = sheetView.findViewById(R.id.friendaction_pvp);
                                                            action_pvp.setOnClickListener(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View view) {


                                                                    mDatabase.child(user.getUid()).child("pvp").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        long countpvps = 0;
                                                                        long activepvp = 0;

                                                                        @Override
                                                                        public void onDataChange(DataSnapshot snapshotpvps) {

                                                                            if (snapshotpvps.hasChildren()) {

                                                                                for (DataSnapshot childpvp : snapshotpvps.getChildren()) {
                                                                                    countpvps++;
                                                                                    if (!childpvp.hasChild("finished")) {
                                                                                        activepvp++;
                                                                                    }

                                                                                    if (countpvps >= snapshotpvps.getChildrenCount()) {
                                                                                        if (activepvp >= 3) {
                                                                                            mBottomSheetDialog.dismiss();
                                                                                            StyleableToast.makeText(getApplicationContext(), "Sorry, you can only have 3 active / pending PvPs.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                                        } else {

                                                                                            Intent openPvP = new Intent(FriendsMain.this, PvPCreate.class);
                                                                                            openPvP.putExtra("agentidpvp", friendid.get(finalI));
                                                                                            openPvP.putExtra("agentnamepvp", friendname.getText().toString());
                                                                                            startActivity(openPvP);
                                                                                            finish();

                                                                                        }
                                                                                    }
                                                                                }

                                                                            } else {
                                                                                if (pref.getBoolean(remoteconfig.getString("pref_calibration"), false)) {

                                                                                    Intent openPvP = new Intent(FriendsMain.this, PvPCreate.class);
                                                                                    openPvP.putExtra("agentidpvp", friendid.get(finalI));
                                                                                    openPvP.putExtra("agentnamepvp", friendname.getText().toString());
                                                                                    startActivity(openPvP);
                                                                                    finish();


                                                                                } else {
                                                                                    AlertDialog.Builder builder = new AlertDialog.Builder(FriendsMain.this, R.style.CustomDialog);

                                                                                    // builder.setMessage("Text not finished...");
                                                                                    LayoutInflater inflater = getLayoutInflater();
                                                                                    View dialog = inflater.inflate(R.layout.dialog_calibration, null);
                                                                                    builder.setView(dialog);
                                                                                    final TextView input = dialog.findViewById(R.id.caltext);
                                                                                    // input.setAllCaps(true);
                                                                                    input.setTypeface(type);

                                                                                    //TextView localTextView1 = (TextView) dialog.findViewById(R.id.basistit);
                                                                                    //localTextView1.setTypeface(proto);
                                                                                    builder.setPositiveButton(getString(R.string.settings_txt), new DialogInterface.OnClickListener() {

                                                                                        @Override
                                                                                        public void onClick(DialogInterface dialog, int which) {

                                                                                            // Log.i("Hallo", "hallo");
                                                                                            //handleinputcode(input.getText().toString());
                                                                                            // Todo publish the code to database
                                                                                            Intent i = new Intent(getBaseContext(), MainSettings.class);
                                                                                            //i.putExtra("PersonID", personID);
                                                                                            startActivity(i);
                                                                                            finish();

                                                                                        }
                                                                                    });

                                                                                    builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                                                        @Override
                                                                                        public void onClick(DialogInterface dialog, int which) {
                                                                                            mBottomSheetDialog.dismiss();

                                                                                            // Todo publish the code to database
                                                                                        }
                                                                                    });

                                                                                    builder.setCancelable(true);

                                                                                    if (!isFinishing()) {
                                                                                        builder.create().show();

                                                                                    }

                                                                                }
                                                                            }


                                                                        }

                                                                        @Override
                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {
                                                                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                            mBottomSheetDialog.dismiss();
                                                                        }
                                                                    });

                                                                }
                                                            });

                                                            LinearLayout action_open = sheetView.findViewById(R.id.friendaction_open);
                                                            action_open.setOnClickListener(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View view) {
                                                                    mBottomSheetDialog.dismiss();
                                                                    StyleableToast.makeText(getApplicationContext(), "Profile soon", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                                                }
                                                            });

                                                        }
                                                    });
                                                    myRootfriends.addView(view);
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                                    coordinator.setVisibility(View.VISIBLE);
                                                    progressBarHolder.setVisibility(View.GONE);
                                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                }
                                            });
                                        } else {
                                            if (sizex - 1 <= 0) {
                                                myRootfriends.removeAllViews();
                                                View view = inflater.inflate(R.layout.list_nofriends_adapter, myRootfriends, false);
                                                // set item content in view
                                                final TextView mybadge = view.findViewById(R.id.nofriends);

                                                myRootfriends.addView(view);
                                            }
                                        }

                                        coordinator.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);
                                    } else {
                                        mDatabase.child(user.getUid()).child("friends").child(friendid.get(finalI)).setValue(null);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                    coordinator.setVisibility(View.VISIBLE);
                                    progressBarHolder.setVisibility(View.GONE);
                                    StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                }
                            });

                        }
                    } else {
                        StyleableToast.makeText(getApplicationContext(), "no friends", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                        coordinator.setVisibility(View.VISIBLE);
                        progressBarHolder.setVisibility(View.GONE);
                    }

                } else {

                    myRootfriends.removeAllViews();
                    View view = inflater.inflate(R.layout.list_nofriends_adapter, myRootfriends, false);
                    // set item content in view
                    final TextView mybadge = view.findViewById(R.id.nofriends);

                    myRootfriends.addView(view);
                    coordinator.setVisibility(View.VISIBLE);
                    progressBarHolder.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

            }
        });
    }


    private void removeFriend(final String friendID, String otheragentimageis) {
        Log.i("Friendid", friendID);

        dialogBuilder = new AlertDialog.Builder(this, R.style.CustomDialog);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialog = inflater.inflate(R.layout.dialog_addfriend, null);

        dialogBuilder.setView(dialog);
        dialogBuilder.setCancelable(false);

        final TextView localTextView1 = dialog.findViewById(R.id.basistit);
        final TextView localTextView2 = dialog.findViewById(R.id.textdescr);
        ImageView own = dialog.findViewById(R.id.ownimage);
        CircleImageView other = dialog.findViewById(R.id.otherimage);

        other.setBorderColor(getResources().getColor(R.color.colorAccent));

        final String otheragenturl = otheragentimageis + "?sz=300";
        String personPhotoUrl = user.getProviderData().get(1).getPhotoUrl().toString() + "?sz=300";

        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(personPhotoUrl)
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(own);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(otheragenturl)
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)

                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    // .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(other);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

        mDatabase.child(friendID).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //snapshot.child("name").getValue();
                // SetLoginName(snapshot.child("name").getValue().toString());
                if (snapshot.exists()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                        localTextView1.setText(Html.fromHtml(String.format(getString(R.string.notconnectedwith), snapshot.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                        localTextView2.setText(Html.fromHtml(String.format(getString(R.string.notconnectedwith_next), snapshot.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                    } else {
                        localTextView1.setText(Html.fromHtml(String.format(getString(R.string.notconnectedwith), snapshot.getValue().toString())));
                        localTextView2.setText(Html.fromHtml(String.format(getString(R.string.notconnectedwith_next), snapshot.getValue().toString())));
                    }


                    dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {


                            new SaveFriendScan().removeScannedAgent(FriendsMain.this, getApplicationContext(), friendID);


                        }
                    });

                    dialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {


                        }
                    });


                    if (!isFinishing()) {


                        if (dialog.getParent() == null) {
                            dialogBuilder.create().show();
                        }


                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    //GET AGENTS DETAILS WITH TOKEN
    private class SendPushinBg extends AsyncTask<String, Void, String> {
        //sendpushaccepted
        String thepushid, theart, theagent, sendtoa;

        public SendPushinBg(String s, String art, String agentname, String sendTo) {
            thepushid = s;
            theart = art;
            theagent = agentname;
            sendtoa = sendTo;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... args) {


            String regurl = BuildConfig.BASEURL + "sendpush.php?pw=" + thepushid + "&art=" + theart + "&agent=" + theagent + "&uid=" + user.getUid() + "&to=" + sendtoa;
            StringBuilder sb = null;
            BufferedReader reader = null;
            String serverResponse = null;
            try {

                URL url = new URL(regurl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(200);
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                //Log.e("statusCode", "" + statusCode);
                if (statusCode == 200) {
                    sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                }

                connection.disconnect();
                if (sb != null)
                    serverResponse = sb.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return serverResponse;
        }

        @Override
        protected void onPostExecute(String json) {

            if (json != null) {


            }
        }
    }
}