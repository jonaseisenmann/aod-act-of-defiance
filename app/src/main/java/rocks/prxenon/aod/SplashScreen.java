package rocks.prxenon.aod;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Arrays;

import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.signin.FirebaseUIActivity;

import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;

public class SplashScreen extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1900;
    private boolean isrunning;
    private Handler handler;
    private Runnable runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // new AODConfig("splash", getApplicationContext());
        //  remoteconfig = FirebaseRemoteConfig.getInstance();
        final RelativeLayout myRootfriends = findViewById(R.id.themainbg);


        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.bg_pattern_light);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);

        bitmapDrawable.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);

        myRootfriends.setBackgroundDrawable(bitmapDrawable);

    }


    @Override
    public void onStart() {
        super.onStart();
        new AODConfig("splash", getApplicationContext());
        remoteconfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(30)
                // .setMinimumFetchIntervalInSeconds(3600)
                .build();
        remoteconfig.setConfigSettingsAsync(configSettings);
        handler = new Handler();

        runnable = new Runnable() {
            @Override
            public void run() {
                isrunning = true;
                // This method will be executed once the timer is over
                // Start your app main activity
                final AVLoadingIndicatorView avi = findViewById(R.id.avi);
                final TextView status_text = findViewById(R.id.status_txt);
                final FancyButton googleplay = findViewById(R.id.playstore);

                remoteconfig.fetch(0).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        remoteconfig.activate();
                        avi.smoothToHide();
                        new AgentData("hackvent19", remoteconfig.getBoolean("hackvent19"));
                        new AgentData("tg_news", remoteconfig.getString("tg_news"));
                        new AgentData("tg_en", remoteconfig.getString("tg_en"));
                        new AgentData("tg_de", remoteconfig.getString("tg_de"));
                        new AgentData("tg_ru", remoteconfig.getString("tg_ru"));
                        new AgentData("tg_es", remoteconfig.getString("tg_es"));
                        new AgentData("MERCHANT_ID", remoteconfig.getString("MERCHANT_ID"));
                        new AgentData("redacted", remoteconfig.getBoolean("redacted"));
                        new AgentData("min_ingress", remoteconfig.getString("min_ingress"));
                        new AgentData("sku", Arrays.asList(remoteconfig.getString("sku")), "pommes");
                        new AgentData("sku_extra", Arrays.asList(remoteconfig.getString("sku_extra")), "pommes");
                        new AgentData("alltime", Arrays.asList(remoteconfig.getString("alltime")), "GESAMT,ALL TIME,OD POCZĄTKU, DALL'INIZIO, 全部, Celé období, ЗА ВСЕ ВРЕМЯ, SIEMPRE, ALLE, TOUS TEMPS, 全期間, Vitalício, Någonsin, Tous le temps, SIDEN STARTEN");
                        new AgentData("month", Arrays.asList(remoteconfig.getString("month")), "MONAT, MONTH, MIESIĄC, MESE, 月, Měsíc, ЗА МЕСЯЦ, MES, MAAND, MOIS, 月, Mês, Månad, Mois, 월, MÅNED");
                        new AgentData("week", Arrays.asList(remoteconfig.getString("week")), "WOCHE, WEEK, TYDZIEŃ, SETTIMANA, 周, Týden, ЗА НЕДЕЛЮ, SEMANA, WEEK, SEMAINE, 週, Semana, Vecka, Semaine, 주, UKE");
                        new AgentData("now", Arrays.asList(remoteconfig.getString("now")), "JETZT, NOW, TERAZ, ORA, 当下, Nyní, СЕЙЧАС, AHORA, NU, MAINTENANT, 現在, Agora, nuNu, Maintenant, 현재, NÅ");

                        Log.i("GET EARTH ", " - " + remoteconfig.getString("earthmissionname"));
                        if (AgentData.getappversion("appversion") < remoteconfig.getLong("minversion")) {

                            avi.setVisibility(View.GONE);
                            status_text.setText(getResources().getString(R.string.string_outdate));
                            status_text.setVisibility(View.VISIBLE);
                            googleplay.setVisibility(View.VISIBLE);
                            googleplay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                    finish();
                                }
                            });

                        } else {
                            if (pref.getBoolean("userexist", false)) {
                                Intent i = new Intent(SplashScreen.this, FirebaseUIActivity.class);
                                i.putExtra("login", true);
                                startActivity(i);
                                finish();
                            } else {
                                Intent i = new Intent(SplashScreen.this, IntroActivity.class);
                                startActivity(i);
                                finish();

                            }
                            // close this activity

                        }
                        // Log.w("Appversion --> ", "app: " + AgentData.getappversion("appversion") + " remote: " + remoteconfig.getLong("minversion"));
                    }
                })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                if (pref.getBoolean("userexist", false)) {
                                    Intent i = new Intent(SplashScreen.this, FirebaseUIActivity.class);
                                    i.putExtra("login", true);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Intent i = new Intent(SplashScreen.this, IntroActivity.class);
                                    startActivity(i);
                                    finish();

                                }
                                //   Log.w("Error --> ", "app: " + AgentData.getappversion("appversion") + " remote: " + remoteconfig.getLong("minversion") + " e: " + exception);
                                // Do whatever should be done on failure
                            }
                        });


            }
        };

        handler.postDelayed(runnable, SPLASH_TIME_OUT);

    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("ONDStop Splash", "stoped");
        handler.removeCallbacks(runnable);
        finish();

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("ONPause Splash", "pause");
        handler.removeCallbacks(runnable);
        finish();

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        Log.i("ONDESTROY Splash", "destroyed");
        handler.removeCallbacks(runnable);
        finish();

    }
}
