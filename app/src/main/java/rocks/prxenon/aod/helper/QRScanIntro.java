package rocks.prxenon.aod.helper;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import io.github.dreierf.materialintroscreen.MessageButtonBehaviour;
import io.github.dreierf.materialintroscreen.SlideFragmentBuilder;
import rocks.prxenon.aod.R;


/**
 * Created by PrXenon on 06.09.2017.
 */

public class QRScanIntro extends MaterialIntroActivityPx {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            addSlide(new SlideFragmentBuilder()
                            .backgroundColor(R.color.colorPrimaryDark)
                            .buttonsColor(R.color.colorAccent)
                            .possiblePermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})

                            .image(R.drawable.full_logo)
                            .title(getResources().getString(R.string.string_permission_camera))
                            .description(getResources().getString(R.string.string_permission_camera_decr))
                            .build(),
                    new MessageButtonBehaviour(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showMessage("Please allow the permissions");
                        }
                    }, "AOD need this"));
        } else {
            finish();
        }
    }
}
