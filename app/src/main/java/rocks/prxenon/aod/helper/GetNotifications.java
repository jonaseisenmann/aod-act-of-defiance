package rocks.prxenon.aod.helper;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import rocks.prxenon.aod.pages.AODMain;
import rocks.prxenon.aod.pages.profile.ProfileMain;

import static rocks.prxenon.aod.basics.AODConfig.database;
import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mbfriendsrequest;
import static rocks.prxenon.aod.basics.AODConfig.user;

/**
 * Created by PrXenon on 12.09.2017.
 */

public class GetNotifications {

    public static ValueEventListener newagents, newagentsprofile, newagentsprofileownrequest, newinbox;

    // INIT DATABASE
    public GetNotifications() {
        if (database == null) {
            database = FirebaseDatabase.getInstance();
        }
    }

    // SAVE AGENT AFTER SCAN AND PRESS SAVE
    public void getFriendRequests(final Activity axt, final Context ctx, final ImageView imgis, final TextView textis) {


        newagents = mbfriendsrequest.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            long friendscount = 0;

            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    Log.i("SNAP EXIST NOTIFY", "" + snapshot.getChildrenCount());
                    friendscount = snapshot.getChildrenCount();
                    getmorefrominbox(friendscount, axt, ctx, imgis, textis);
                    //new AODMain().notificationResults(friendscount, axt, ctx, imgis, textis);
                    //  mDatabase.child(md5(email)).child("scrapbook").child(md5(otheremail)).child(randid).child("lng").setValue(lng);

                } else {
                    Log.i("SNAP NOT EXIST NOTIFY", "05");
                    getmorefrominbox(friendscount, axt, ctx, imgis, textis);
                    // new AODMain().notificationResults(friendscount, axt, ctx, imgis, textis);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("SNAP NOT EXIST NOTIFY", "error");
                new AODMain().notificationResults(friendscount, axt, ctx, imgis, textis);
            }
        });


        // qrdatabase.child(qrid).child("scanedagnet").setValue("-");

    }

    public void getmorefrominbox(final long friendscount, final Activity axt, final Context ctx, final ImageView imgis, final TextView textis) {
        if (newinbox != null) {
            mDatabase.removeEventListener(newinbox);
        }
        newinbox = mDatabase.child(user.getUid()).child("newinbox").limitToLast(99).addValueEventListener(new ValueEventListener() {
            long friendscount2 = friendscount;

            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    int looperis = 0;
                    for (DataSnapshot inboxdata : snapshot.getChildren()) {
                        looperis++;
                        //  Log.i("timestamp", historyis.getValue().toString());
                        if (!Boolean.parseBoolean(inboxdata.child("read").getValue().toString())) {
                            friendscount2++;
                        }
                    }

                    if (looperis >= snapshot.getChildrenCount()) {

                        Log.i("SNAP EXIST NOTIFY", "" + snapshot.getChildrenCount());
                        // friendscount2 = snapshot.getChildrenCount();
                        //getmorefrominbox(friendscount, axt, ctx, imgis, textis);
                        new AODMain().notificationResults(friendscount2, axt, ctx, imgis, textis);
                    }
                    //  mDatabase.child(md5(email)).child("scrapbook").child(md5(otheremail)).child(randid).child("lng").setValue(lng);

                } else {
                    Log.i("SNAP NOT EXIST NOTIFY", "05");
                    //getmorefrominbox(friendscount, axt, ctx, imgis, textis);
                    new AODMain().notificationResults(friendscount2, axt, ctx, imgis, textis);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("SNAP NOT EXIST NOTIFY", "error");
                new AODMain().notificationResults(friendscount, axt, ctx, imgis, textis);
            }
        });

    }

    // SAVE AGENT AFTER SCAN AND PRESS SAVE
    public void getFriendRequestsProfile(final Activity axt, final Context ctx, final TextView textis, final TextView alltextfield, final FrameLayout container) {


        newagentsprofile = mbfriendsrequest.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            long friendscount = 0;

            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    Log.i("SNAP EXIST NOTIFY", "" + snapshot.getChildrenCount());
                    friendscount = snapshot.getChildrenCount();
                    //  new ProfileMain().FriendsRequestsOpen(friendscount, axt, ctx, textis);
                    morefriends(friendscount, axt, ctx, textis, alltextfield, container);
                    //  mDatabase.child(md5(email)).child("scrapbook").child(md5(otheremail)).child(randid).child("lng").setValue(lng);

                } else {
                    Log.i("SNAP NOT EXIST NOTIFY", "05");
                    morefriends(friendscount, axt, ctx, textis, alltextfield, container);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("SNAP NOT EXIST NOTIFY", "error");
                new ProfileMain().FriendsRequestsOpen(friendscount, axt, ctx, textis);
            }
        });


        // qrdatabase.child(qrid).child("scanedagnet").setValue("-");

    }


    private void morefriends(final long friendscounti, final Activity axti, final Context ctxi, final TextView textisi, final TextView alltextfieldi, final FrameLayout containeris) {

        newagentsprofileownrequest = mDatabase.child(user.getUid()).child("friends").addValueEventListener(new ValueEventListener() {
            long friendscount = friendscounti;
            long allmyfriends = 0;

            @Override
            public void onDataChange(DataSnapshot snapshotown) {
                if (snapshotown.exists()) {
                    Log.i("SNAP EXIST NOTIFY", "" + snapshotown.getChildrenCount());
                    for (DataSnapshot child : snapshotown.getChildren()) {

                        if (Boolean.parseBoolean(child.child("status").getValue().toString())) {
                            allmyfriends++;
                        } else {
                            friendscount++;
                        }

                        //  Log.i("TEST LOG--->",child.getValue().toString());

                    }

                    new ProfileMain().FriendsRequestsOpenAll(friendscount, allmyfriends, axti, ctxi, textisi, alltextfieldi, containeris);
                    //  mDatabase.child(md5(email)).child("scrapbook").child(md5(otheremail)).child(randid).child("lng").setValue(lng);

                } else {
                    Log.i("SNAP NOT EXIST NOTIFY", "05");
                    new ProfileMain().FriendsRequestsOpenAll(friendscount, allmyfriends, axti, ctxi, textisi, alltextfieldi, containeris);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i("SNAP NOT EXIST NOTIFY", "error");
                new ProfileMain().FriendsRequestsOpenAll(friendscount, allmyfriends, axti, ctxi, textisi, alltextfieldi, containeris);
            }
        });
    }


}

