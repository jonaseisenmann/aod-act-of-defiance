package rocks.prxenon.aod.helper;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.muddzdev.styleabletoast.StyleableToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;
import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;

public class BackgroundUpdate extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {

        // Get intent, action and MIME type
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }
    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            Log.i("DATA -->", sharedText);

            initmissions(sharedText);
            // Update UI to reflect text being shared
        } else {
            finish();
        }
    }

    private void initmissions(String sharedText) {


        // GET THE DATE
        // SimpleDateFormat fday = new SimpleDateFormat("yyyyMMdd_HHmmss");
        SimpleDateFormat fmonth = new SimpleDateFormat("MM");
        SimpleDateFormat fyear = new SimpleDateFormat("yyyy");
        final String themonth = fmonth.format(new Date());
        final String theyear = fyear.format(new Date());
        new AgentData("year", theyear);
        new AgentData("month", themonth);

        final long timmi = System.currentTimeMillis();

        Map<String, String> timestampx = new HashMap<>();
        timestampx = ServerValue.TIMESTAMP;
        mDatabase.child(user.getUid()).child("servertime").setValue(timestampx).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {


                    mDatabase.child(user.getUid()).child("servertime").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshottime) {

                            long tstamp = Long.parseLong(snapshottime.getValue().toString());
                            //Log.i("timestamos---> ", "1 = "+timmi+ " diff: "+(tstamp-timmi)+ " fb: "+ ServerValue.TIMESTAMP.get(".sv").toString());

                            if ((tstamp - timmi) > 3600000 || (tstamp - timmi) < -3600000) {
                                // TODO timemanipulazion
                                StyleableToast.makeText(getApplicationContext(), "Time was manipulated", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            } else {
                                DateFormat format = new SimpleDateFormat("dd");
                                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                                Calendar.getInstance(Locale.getDefault()).setTimeZone(TimeZone.getDefault());
                                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

                                final String[] days = new String[7];
                                for (int i = 0; i < 7; i++) {
                                    days[i] = format.format(calendar.getTime());
                                    calendar.add(Calendar.DAY_OF_MONTH, 1);
                                }

                                new AODConfig();
                                new AODConfig("reff", getApplicationContext());
                                if (user == null) {
                                    user = FirebaseAuth.getInstance().getCurrentUser();
                                }

                                String strDate = days[0] + "." + (Integer.parseInt(themonth)) + "." + theyear;
                                DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
                                Date date = null;
                                try {
                                    date = dateFormat.parse(strDate);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Calendar now = Calendar.getInstance(Locale.getDefault());
                                // now.setTime(date);
                                now.setTimeZone(TimeZone.getDefault());
                                now.setFirstDayOfWeek(Calendar.MONDAY);
                                int currentweek = now.get(Calendar.WEEK_OF_YEAR);
                                String cweeksend;
                                if (currentweek <= 9) {
                                    currentweek = Integer.parseInt("0" + currentweek);
                                    cweeksend = "0" + currentweek;
                                } else {
                                    cweeksend = String.valueOf(currentweek);
                                }


                                // Log.i("CURRENT WEEK FOR", "" + cweeksend);
                                final String thefirstweekday = cweeksend + "" + days[0];
                                new AgentData("week", thefirstweekday);

                                // Log.i("MONTH -->", themonth);
                                //  Log.i("Year -->", theyear);
                                // Log.i("Dayof Week -->", "" + days[0]);

                                mDatabase.child(user.getUid()).child("missions").child(theyear).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshot) {


                                        if (snapshot.child("month").hasChild(themonth)) {
                                            Log.i("MONTH -->", "Monat running gefunden");

                                            // monthrunning = true;
                                            // STARTE MONAT GEFUNDEN
                                            //  initmonth(theyear, themonth, true);
                                            // TODO GET MONTH  BASIS DATA


                                        } else {
                                            Log.i("MONTH -->", "Kein Monat running gefunden");
                                            //   monthrunning = false;
                                            //   initmonth(theyear, themonth, false);


                                        }

                                        Log.i("GET EARTH 2 ", " - " + remoteconfig.getString("earthmissionname"));
                                        // EARTH MISSIONS
                                        if (remoteconfig.getBoolean("earthmission") && snapshot.child("earth").hasChild(remoteconfig.getString("earthmissionname"))) {
                                            Log.i("EARTH -->", "EARTH running gefunden");
                                            // TODO GET EARTH BASIS DATA

                                            //   initearth(theyear, remoteconfig.getString("earthmissionname"), true);


                                        } else {
                                            Log.i("EARTH -->", "Kein EARTH  running gefunden");
                                            if (remoteconfig.getBoolean("earthmission")) {
                                                //   initearth(theyear, remoteconfig.getString("earthmissionname"), false);
                                            } else {
                                                //   fcearth.setVisibility(View.GONE);
                                            }


                                        }


                                        if (snapshot.child("week").hasChild(thefirstweekday)) {
                                            // TODO GET WEEK BASIS DATA
                                            //  weekrunning = true;
                                            // initweek(theyear, themonth, thefirstweekday, days, true);
                                            Log.i("WEEK -->", "Woche gefunden");

                                        } else {

                                            //   weekrunning = false;
                                            //  initweek(theyear, themonth, thefirstweekday, days, false);
                                            Log.i("WEEK -->", "Keine Woche gefunden" + theyear + " " + themonth + " " + thefirstweekday + " " + days);


                                        }

                                        if (snapshot.child("mini").getChildrenCount() > 0) {
                                            for (final DataSnapshot childmini : snapshot.child("mini").getChildren()) {
                                                Log.i("TEST LOG--->", childmini.getKey());
                                                if (System.currentTimeMillis() < Long.parseLong(childmini.getKey())) {
                                                    // TODO GET MINI BASIS DATA
                                                    Log.i("Mini -->", "MINI running gefunden 1");
                                                    Log.i("TEST LOG--->", childmini.getKey());
                                                    // initmini(Long.parseLong(childmini.getKey()), true);
                                                } else {
                                                    //   initmini(System.currentTimeMillis(), false);
                                                }


                                            }


                                            //monthrunning = true;
                                            // STARTE MONAT GEFUNDEN
                                            // initmonth(theyear, themonth, true);


                                        } else {
                                            Log.i("MINI -->", "Kein MINI running gefunden");
                                            // monthrunning = false;
                                            // initmonth(theyear, themonth, false);
                                            // initmini(System.currentTimeMillis(), false);


                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                        finish();
                                    }
                                });


                                mDatabase.child(user.getUid()).child("pvp").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshotpvp) {


                                        if (snapshotpvp.exists()) {
                                            Log.i("PV -->", "PVP gefunden");
                                            // TODO GET PVP BASIS DATA
                                            // STARTE MONAT GEFUNDEN
                                            //initpvp(snapshotpvp, true);


                                        } else {
                                            Log.i("PVP -->", "Kein PVP gefunden");
                                            // initpvp(snapshotpvp, false);


                                        }


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                    }
                                });


                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                        }
                    });


                } else if (task.isCanceled()) {

                } else {
                    Log.w("ERROR ", this.getClass().getSimpleName() + " 10556 Error ");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.w("ERROR ", this.getClass().getSimpleName() + " 1005 Error " + e.getMessage());


                //signOut();
            }
        });


    }
}
