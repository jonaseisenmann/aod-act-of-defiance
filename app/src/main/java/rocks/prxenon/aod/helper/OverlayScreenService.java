package rocks.prxenon.aod.helper;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.pages.AODMain;

import static rocks.prxenon.aod.basics.AODConfig.pref;


public class OverlayScreenService extends Service implements View.OnClickListener {

    private static final String TAG = "ClipboardManager";
    private static final String FILENAME = "clipboard-history.tsv";
    private final IBinder mBinder = new LocalBinder();
    private final Random mGenerator = new Random();
    private ServiceCallBack serviceCallBack;
    private WindowManager windowManager;
    private WindowManager windowManager2;
    // private ImageView chatHead;
    private LinearLayout open, rootv;
    private Button back, backq;
    private ImageView needbadge;
    private Bundle bundle;
    private TextView details;
    private String comp, basistext, agentname, clientis, themid, theartid, stats;
    private Intent getint;
    private LinearLayout rootContent;
    private Activity act;
    private ContentResolver cres;
    private ContentObserver obs;
    private int sysbadge;
    private Timer datalistenertimer;
    private CountDownTimer ctimer;
    private File mHistoryFile;
    private ExecutorService mThreadPool = Executors.newSingleThreadExecutor();
    private ClipboardManager mClipboardManager;
    private ClipboardManager.OnPrimaryClipChangedListener mOnPrimaryClipChangedListener =
            new ClipboardManager.OnPrimaryClipChangedListener() {
                @Override
                public void onPrimaryClipChanged() {
                    Log.d(TAG, "onPrimaryClipChanged");
                    ClipData clip = mClipboardManager.getPrimaryClip();
                    mThreadPool.execute(new WriteHistoryRunnable(
                            clip.getItemAt(0).getText()));
                }
            };

    public void setServiceCallBack(Activity aodMain) {
        act = aodMain;
    }


    public void setCallbacks(ServiceCallBack callbacks) {
        serviceCallBack = callbacks;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    /**
     * method for clients
     */

    public int getRandomNumber() {
        return mGenerator.nextInt(100);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, startId, startId);


        bundle = intent.getExtras();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String NOTIFICATION_CHANNEL_ID = "rocks.prxenon.aod.helper";
            String channelName = "AOD Background Service";
            NotificationChannel chan = null;

            chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_MIN);

            chan.setLightColor(Color.MAGENTA);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("AOD is running in background")
                    .setPriority(NotificationManager.IMPORTANCE_MIN)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .build();
            startForeground(2, notification);
            // initial();


        }

        if (bundle != null) {


            comp = bundle.getString("comp");
            basistext = bundle.getString("basistext");
            agentname = bundle.getString("agentname");
            clientis = bundle.getString("ingress");
            themid = bundle.getString("mid");
            theartid = bundle.getString("aid");
            sysbadge = bundle.getInt("sysbadge");
            stats = bundle.getString("stats");


            Log.i("get data --> ", " " + basistext + " " + sysbadge + " " + comp + "timer ->" + pref.getInt("timer", 25));
            setall(sysbadge);


        }
        return START_STICKY;
    }


    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public void startIngress() {

        // details.setText(Html.fromHtml("AGENT <b>" + agentname + "</b><br><b>1.</b> Öffne dein Agent Profil<br><b>2.</b> Klicke auf die Badge  <b>" + basistext.replaceAll(" ","-") + "</b><br><b>3.</b> Es muss zeigen <b>" + comp + "</b><br>4. Erstelle <b>manuell</b> einen Screenshot."));
        //ingress.setVisibility(View.GONE);
        // Log.i("INGRES ", "started");


        PackageManager manager = getApplicationContext().getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(clientis);

            if (i == null) {
                //return false;
                throw new PackageManager.NameNotFoundException();
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            getApplicationContext().startActivity(i);


        } catch (PackageManager.NameNotFoundException e) {

        }
        // Toast.makeText(this, "Ingress wird gestartet", Toast.LENGTH_SHORT).show();
    }

    private void connectclip(String valueclip, String theartid, String themid, String basistext, String lifetimeclip, String timespanclip, String agentclip, String statsis) {
        Intent intent = new Intent();
        intent.putExtra("value", valueclip);
        intent.putExtra("missionart", theartid);
        intent.putExtra("onactionid", themid);
        intent.putExtra("needbadge", basistext);
        intent.putExtra("lifetimeclip", lifetimeclip);
        intent.putExtra("timespanclip", timespanclip);
        intent.putExtra("agentclip", agentclip);
        intent.putExtra("stats", statsis);

        Log.w("CONNECT from Clip-->", "5565");

        ActivityManager am2 = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.AppTask> tasks = am2.getAppTasks();
        Log.w("Back -->", "pressed x347 " + tasks.size());
        for (ActivityManager.AppTask task : tasks) {
            if (task.getTaskInfo().baseActivity.getPackageName().contains("rocks.prxenon.aod")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    stopForeground(true);
                }
                //Log.i("Point", task.getTaskInfo().baseIntent.getComponent().getPackageName() + " api 21");
                task.moveToFront();

            }

        }

        if (serviceCallBack != null) {
            serviceCallBack.connectToDeviceclip(intent);
        }
        // cres.unregisterContentObserver(obs);
    }

    private void connect(String tmp, String mart, String actionid, String badgetitle) {


        String address = tmp;

        // Create the result Intent and include the MAC address
        Intent intent = new Intent();
        intent.putExtra("screen", address);
        intent.putExtra("missionart", mart);
        intent.putExtra("onactionid", actionid);
        intent.putExtra("needbadge", badgetitle);
        intent.putExtra("stats", stats);
        Log.w("CONNECT Screen-->", address);
        //onactionid, missionart);


        ActivityManager am2 = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.AppTask> tasks = am2.getAppTasks();
        Log.w("Back -->", "pressed x345 " + tasks.size());
        for (ActivityManager.AppTask task : tasks) {
            if (task.getTaskInfo().baseActivity.getPackageName().contains("rocks.prxenon.aod")) {
                //Log.i("Point", task.getTaskInfo().baseIntent.getComponent().getPackageName() + " api 21");
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    stopForeground(true);
                }
                task.moveToFront();

            }

        }

        if (serviceCallBack != null) {
            serviceCallBack.connectToDevice(intent);
        }
        //cres.unregisterContentObserver(obs);

    }

    public void initial() {
       /*
        cres = getContentResolver();

        // Log.d("hai", "deliverSelfNotifications");
        obs =
                (new ContentObserver(new Handler()) {
                    @Override
                    public boolean deliverSelfNotifications() {
                        //Log.d("hai", "deliverSelfNotifications");
                        return super.deliverSelfNotifications();
                    }

                    @Override
                    public void onChange(boolean selfChange) {
                        super.onChange(selfChange);
                    }

                    @Override
                    public void onChange(boolean selfChange, Uri uri) {

                        boolean trickster = true;
                        String currentApp;
                        // Log.i("Trickster Status--->", AgentData.getTrickster());
                        if (AgentData.getTrickster().equalsIgnoreCase("green")) {
                            trickster = false;
                        }

                        if (trickster) {
                            if (!isAccessGranted()) {
                                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                                act.startActivity(intent);
                            } else {
                                UsageStatsManager usm = (UsageStatsManager) act.getSystemService(Context.USAGE_STATS_SERVICE);
                                long time = System.currentTimeMillis();
                                List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 10000 * 10000, time);
                                if (appList != null && appList.size() == 0) {
                                    //Log.d("Executed app", "######### NO APP FOUND ##########");
                                }
                                if (appList != null && appList.size() > 0) {
                                    SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                                    for (UsageStats usageStats : appList) {

                                        //  Log.d("Executed app", "usage stats executed : " + usageStats.getPackageName() + "\t\t ID: ");
                                        mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                                    }
                                    if (mySortedMap != null && !mySortedMap.isEmpty()) {

                                        currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();

                                        if (currentApp.equalsIgnoreCase("com.nianticproject.ingress") || currentApp.equalsIgnoreCase("android") || currentApp.equalsIgnoreCase("com.nianticlabs.ingress.prime.qa")) {
                                            trickster = false;
                                        }
                                        //Log.d("currentApplast", "usage stats executed : " + currentApp + "\t\t ID: ");


                                    }
                                }
                            }
                        }

                        if (trickster) {
                            //connect(path, theartid, themid);
                            StyleableToast.makeText(getApplicationContext(), "Bitte Ingress verwenden ", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                        } else {
                            // Log.i("App----> ", "Pommes");
                            if (uri.toString().matches(MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString() + "/[0-9]+") || uri.toString().matches(MediaStore.Files.getContentUri("external").toString() + "/[0-9]+")) {


                                try (Cursor cursor = getContentResolver().query(uri, new String[]{
                                        MediaStore.Images.Media.DISPLAY_NAME,
                                        MediaStore.Images.Media.DATA
                                }, null, null, null)) {
                                    if (cursor != null && cursor.moveToFirst()) {
                                        final String fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));
                                        final String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                                        // TODO: apply filter on the file name to ensure it's screen shot event
                                        Log.d("file", "FILE CHANGE OCCURED " + fileName + " " + path);

                                        try {
                                            Thread.sleep(2000); //add another 3 seconds delay
                                            connect(path, theartid, themid, basistext);

                                        } catch (InterruptedException e) {
                                            connect(path, theartid, themid, basistext);
                                            e.printStackTrace();
                                        }


                                    }
                                }
                            }
                        }
                        super.onChange(selfChange, uri);
                    }
                }
                );

        if (Build.VERSION.RELEASE == "6.0" || Build.VERSION.RELEASE == "6") {
            cres.registerContentObserver(
                    MediaStore.Files.getContentUri("external"),
                    true, obs);
        } else {
            cres.registerContentObserver(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    true, obs);

        }


        */

    }

    private boolean isAccessGranted() {
        try {
            PackageManager packageManager = getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                    applicationInfo.uid, applicationInfo.packageName);
            return (mode == AppOpsManager.MODE_ALLOWED);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();


        mHistoryFile = new File(getExternalFilesDir(null), FILENAME);
        mClipboardManager =
                (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        mClipboardManager.addPrimaryClipChangedListener(
                mOnPrimaryClipChangedListener);


    }


    public void setall(int sysbadge) {

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);


        // chatHead = new ImageView(this);

        // chatHead.setImageResource(R.drawable.floating);

        open = (LinearLayout) LayoutInflater.from(getApplicationContext())
                .inflate(R.layout.calibrationscreen, null);
        back = open.findViewById(R.id.gobackb);

        backq = open.findViewById(R.id.gobackq);

        // ingress = (Button) open.findViewById(R.id.ingress);

        //details = (TextView) open.findViewById(R.id.textdescr);


        // details.setText(basistext+" "+comp);
        back.setOnClickListener(this);
        backq.setOnClickListener(this);


        needbadge = open.findViewById(R.id.needbadge);
        //Log.i("SYSBADGE -->", " " + sysbadge);
        if (!String.valueOf(sysbadge).equalsIgnoreCase("0")) {
            //Log.i("SYSBADGE3 -->", " " + sysbadge);

            needbadge.setVisibility(View.VISIBLE);
            needbadge.setImageResource(sysbadge);
            needbadge.postInvalidate();
        } else {
            //Log.i("SYSBADGE2 -->", " " + sysbadge);
            needbadge.setVisibility(View.GONE);
            needbadge.postInvalidate();
        }
        // ingress.setOnClickListener(this);
        final WindowManager.LayoutParams params;
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {

            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);


            params.gravity = Gravity.TOP | Gravity.END;
            params.x = 70;
            params.y = 5;


            // windowManager.addView(chatHead, params);
            windowManager.addView(open, params);
        } else {

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        LAYOUT_FLAG,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                                | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                        PixelFormat.TRANSLUCENT);
            } else {
                params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        LAYOUT_FLAG,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                                | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                        PixelFormat.TRANSLUCENT);


            }

            /*
            WindowManager.LayoutParams params = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                    | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
            PixelFormat.TRANSLUCENT);

    WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
    LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
    View myView = inflater.inflate(R.layout.my_view, null);
    myView.setOnTouchListener(new OnTouchListener() {
       @Override
       public boolean onTouch(View v, MotionEvent event) {
           Log.d(TAG, "touch me");
           return true;
       }
     });

             */
            //  params.alpha = 10;

            params.gravity = Gravity.TOP | Gravity.END;
            params.x = 70;
            params.y = 5;

            windowManager.addView(open, params);


            // open.bringToFront();
        }

      /*  open.requestFocus();
        open.requestFocusFromTouch();
        open.setFocusableInTouchMode(true);
        back.setFocusableInTouchMode(true);
        back.requestFocus();
        back.requestFocusFromTouch();

       */

       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            datalistenertimer = new Timer();
            datalistenertimer.scheduleAtFixedRate(new TimerTask() {
                int counter = 1;

                @Override
                public void run() {

                    ClipData clip = mClipboardManager.getPrimaryClip();

                    if(clip != null) {
                        if (clip.getItemCount() > 0) {

                            Log.i("CLIP --> ", clip.getItemAt(0).getText().toString() + " " + counter);
                        }

                    }


                    Log.i("Timer --> ", "Message " + counter);


                    //   showToas("Message "+counter);
                    counter++;

                    if (counter >= pref.getInt("timer", 25)) {
                        backq.callOnClick();


                    }
                }

            }, 0, 1100);  //It will be repeated every 5 seconds

            ctimer = new CountDownTimer((pref.getInt("timer", 25) * 1000), 1000) {

                public void onTick(long millisUntilFinished) {
                    back.setText("wait .. (" + millisUntilFinished / 1000 + ")");
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    back.setText("go back");
                }

            }.start();
        }


        */

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ctimer.cancel();
            datalistenertimer.cancel();
        }
        if (open != null) windowManager.removeView(open);
        mClipboardManager.removePrimaryClipChangedListener(
                mOnPrimaryClipChangedListener);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.gobackb:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                    datalistenertimer = new Timer();
                    datalistenertimer.scheduleAtFixedRate(new TimerTask() {
                        int counter = 1;

                        @Override
                        public void run() {

                            ClipData clip = mClipboardManager.getPrimaryClip();

                            if (clip != null) {
                                if (clip.getItemCount() > 0) {

                                    Log.i("CLIP --> ", clip.getItemAt(0).getText().toString() + " " + counter);
                                }

                            }


                            Log.i("Timer --> ", "Message " + counter);


                            //   showToas("Message "+counter);
                            counter++;

                            if (counter >= 3) {
                                backq.callOnClick();


                            }
                        }

                    }, 0, 1100);  //It will be repeated every 5 seconds

                    ctimer = new CountDownTimer((3 * 1000), 1000) {

                        public void onTick(long millisUntilFinished) {
                            back.setText("wait .. (" + millisUntilFinished / 1000 + ")");
                            //here you can have your logic to set text to edittext
                        }

                        public void onFinish() {
                            back.setText("go back");
                        }

                    }.start();

                    break;
                } else {
                    backq.callOnClick();
                    break;
                }


            case R.id.gobackq:


                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    stopForeground(true);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    ctimer.cancel();
                    datalistenertimer.cancel();
                    if (serviceCallBack != null) {


                        serviceCallBack.closewindow(comp, theartid, themid, basistext, stats);
                    }

                    mClipboardManager.removePrimaryClipChangedListener(
                            mOnPrimaryClipChangedListener);
                    // cres.unregisterContentObserver(obs);


                } else {
                    if (serviceCallBack != null) {


                        serviceCallBack.closewindow(comp, theartid, themid, basistext, stats);
                    }

                    mClipboardManager.removePrimaryClipChangedListener(
                            mOnPrimaryClipChangedListener);
                    // cres.unregisterContentObserver(obs);


                }
                //  Toast.makeText(this, "Back", Toast.LENGTH_SHORT).show();
                break;


        }


    }


    public class LocalBinder extends Binder {
        public OverlayScreenService getService() {
            // Return this instance of LocalService so clients can call public methods
            return OverlayScreenService.this;
        }
    }

    private class WriteHistoryRunnable implements Runnable {
        private final Date mNow;
        private final CharSequence mTextToWrite;


        public WriteHistoryRunnable(CharSequence text) {
            mNow = new Date(System.currentTimeMillis());
            mTextToWrite = text;
        }

        @Override
        public void run() {
            if (TextUtils.isEmpty(mTextToWrite)) {
                mClipboardManager.removePrimaryClipChangedListener(
                        mOnPrimaryClipChangedListener);
                // Don't write empty text to the file
                return;
            }

            if (isExternalStorageWritable()) {
                try {
                    mClipboardManager.removePrimaryClipChangedListener(
                            mOnPrimaryClipChangedListener);
                    Log.i(TAG, "Writing new clip to history:");
                    // Log.i(TAG, mTextToWrite.toString());
                    if (mTextToWrite.toString().startsWith("Time")) {
                        BufferedWriter writer =
                                new BufferedWriter(new FileWriter(mHistoryFile, false));

                        writer.write(mTextToWrite.toString());
                        writer.newLine();
                        writer.close();


                        // GO

                        // END

                        String line;
                        String[] columns; //contains column names
                        int num_cols;
                        String[] tokens;
                        String valueclip = null;
                        String timespanclip = null;
                        String lifetimeclip = null;
                        String agentclip = null;
                        BufferedReader reader = null;
                        try {
                            //Get the CSVReader instance with specifying the delimiter to be used
                            reader = new BufferedReader(new FileReader(mHistoryFile), ',');

                            line = reader.readLine();
                            columns = line.split("\t");
                            num_cols = columns.length;

                            line = reader.readLine();


                            while (true) {
                                tokens = line.split("\t");

                                if (tokens.length == num_cols) { //if number columns equal to number entries


                                    for (int k = 0; k < num_cols; ++k) { //for each column

                                        if (comp.equalsIgnoreCase(columns[k])) {
                                            valueclip = tokens[k];
                                        }

                                        if (columns[k].equalsIgnoreCase("Lifetime AP")) {
                                            lifetimeclip = tokens[k];
                                        }

                                        if (columns[k].equalsIgnoreCase("Time Span")) {
                                            timespanclip = tokens[k];
                                        }
                                        Log.i("Loop ", " " + columns[k] + " value: " + tokens[k]);

                                        if (columns[k].equalsIgnoreCase("Agent Name")) {
                                            agentclip = tokens[k];
                                        }
                                    }


                                    if ((line = reader.readLine()) != null) {//if not last line

                                    } else {
                                        Log.i("Loop finished", "done");
                                        Log.i("Loop done", lifetimeclip + " " + timespanclip + " " + valueclip);
                                        connectclip(valueclip, theartid, themid, basistext, lifetimeclip, timespanclip, agentclip, stats);
                                        break;
                                    }
                                } else {
                                    //line = read.readLine(); //read next line if wish to continue parsing despite error

                                    System.exit(-1); //error message
                                }
                            }


                            reader.close();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    } else {
                        Log.i("INIT COPY STRING ->", mTextToWrite.toString());
                    }


                } catch (IOException e) {
                    Log.w(TAG, String.format("Failed to open file %s for writing!",
                            mHistoryFile.getAbsoluteFile()));
                }
            } else {
                Log.w(TAG, "External storage is not writable!");
            }
        }
    }


}