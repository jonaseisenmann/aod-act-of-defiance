package rocks.prxenon.aod.helper;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.List;
import java.util.Random;

import rocks.prxenon.aod.R;


public class OverlayCalibrationService extends Service implements View.OnClickListener {

    private final IBinder mBinder = new LocalBinder();
    private final Random mGenerator = new Random();
    private ServiceCallBack serviceCallBack;
    private WindowManager windowManager;
    private WindowManager windowManager2;
    // private ImageView chatHead;
    private LinearLayout open, rootv;
    private Button back;
    private Bundle bundle;

    private String comp;
    private Intent getint;
    private LinearLayout rootContent;
    private Activity act;
    private ContentResolver cres;
    private ContentObserver obs;
    private View rootViewis;
    private Bitmap bitmap;

    public void setServiceCallBack(Activity aodMain) {
        act = aodMain;

    }


    public void setCallbacks(ServiceCallBack callbacks) {
        serviceCallBack = callbacks;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * method for clients
     */
    public int getRandomNumber() {
        return mGenerator.nextInt(100);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, startId, startId);


        bundle = intent.getExtras();


        if (bundle != null) {

            comp = bundle.getString("ingress");
            setall();

        }


        initial();
        return START_STICKY;
    }

    public void startIngress() {


        PackageManager manager = getApplicationContext().getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(comp);
            if (i == null) {
                //return false;
                throw new PackageManager.NameNotFoundException();
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            getApplicationContext().startActivity(i);


        } catch (PackageManager.NameNotFoundException e) {

        }
        // Toast.makeText(this, "Ingress wird gestartet", Toast.LENGTH_SHORT).show();
    }

    private void connect(String tmp) {


        String address = tmp;

        // Create the result Intent and include the MAC address
        Intent intent = new Intent();
        intent.putExtra("screen", address);

        Log.w("CONNECT -->", address);


        ActivityManager am2 = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.AppTask> tasks = am2.getAppTasks();
        Log.w("Back -->", "pressed y " + tasks.size());

        for (ActivityManager.AppTask task : tasks) {
            if (task.getTaskInfo().baseActivity.getPackageName().contains("rocks.prxenon.aod")) {
                Log.i("Point", task.getTaskInfo().baseIntent.getComponent().getPackageName() + " api 21");
                task.moveToFront();

            }

        }

        if (serviceCallBack != null) {
            serviceCallBack.connectToDevice(intent);
        }


        cres.unregisterContentObserver(obs);

    }

    public void initial() {
        cres = getContentResolver();

        obs =
                (new ContentObserver(new Handler()) {
                    @Override
                    public boolean deliverSelfNotifications() {
                        //Log.d("hai", "deliverSelfNotifications");
                        return super.deliverSelfNotifications();
                    }

                    @Override
                    public void onChange(boolean selfChange) {
                        super.onChange(selfChange);
                    }

                    @Override
                    public void onChange(boolean selfChange, Uri uri) {


                        // TODO PERMISSION
                        if (uri.toString().matches(MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString() + "/[0-9]+") || uri.toString().matches(MediaStore.Files.getContentUri("external").toString() + "/[0-9]+")) {


                            try (Cursor cursor = getContentResolver().query(uri, new String[]{
                                    MediaStore.Images.Media.DISPLAY_NAME,
                                    MediaStore.Images.Media.DATA
                            }, null, null, null)) {
                                if (cursor != null && cursor.moveToFirst()) {
                                    final String fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME));
                                    final String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                                    // TODO: apply filter on the file name to ensure it's screen shot event
                                    Log.d("file", "FILE CHANGE OCCURED " + fileName + " " + path);

                                    try {
                                        Thread.sleep(2000); //add another 3 seconds delay
                                        connect(path);

                                    } catch (InterruptedException e) {
                                        connect(path);
                                        e.printStackTrace();
                                    }


                                }
                            }
                        }
                        super.onChange(selfChange, uri);
                    }
                }
                );

        if (Build.VERSION.RELEASE == "6.0" || Build.VERSION.RELEASE == "6") {
            cres.registerContentObserver(
                    MediaStore.Files.getContentUri("external"),
                    true, obs);
        } else {
            cres.registerContentObserver(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    true, obs);

        }

    }

    @Override
    public void onCreate() {
        super.onCreate();

       /* windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);


        // chatHead = new ImageView(this);

        // chatHead.setImageResource(R.drawable.floating);

        open = (LinearLayout) LayoutInflater.from(getApplicationContext())
                .inflate(R.layout.calibrationscreen, null);
        back = open.findViewById(R.id.gobackb);

        // details.setText(basistext+" "+comp);
        back.setOnClickListener(this);

        final WindowManager.LayoutParams params;
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {

            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);


            params.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
            params.x = 0;
            params.y = 0;


            // windowManager.addView(chatHead, params);
            windowManager.addView(open, params);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                            | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);


            params.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
            params.x = 0;
            params.y = 0;
            windowManager.addView(open, params);
        }

       /* try {
            open.setOnTouchListener(new View.OnTouchListener() {
                private WindowManager.LayoutParams paramsF = params;

                private int initialX;
                private int initialY;
                private float initialTouchX;
                private float initialTouchY;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:

                            // Get current time in nano seconds.

                            initialX = paramsF.x;
                            initialY = paramsF.y;
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            paramsF.x = initialX + (int) (event.getRawX() - initialTouchX);
                            paramsF.y = initialY + (int) (event.getRawY() - initialTouchY);

                            windowManager.updateViewLayout(open, paramsF);

                            break;
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }

*/
    }

    public void setall() {

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);


        // chatHead = new ImageView(this);

        // chatHead.setImageResource(R.drawable.floating);

        open = (LinearLayout) LayoutInflater.from(getApplicationContext())
                .inflate(R.layout.calibrationscreen, null);
        back = open.findViewById(R.id.gobackb);


        // ingress = (Button) open.findViewById(R.id.ingress);

        //details = (TextView) open.findViewById(R.id.textdescr);


        // details.setText(basistext+" "+comp);
        back.setOnClickListener(this);

        //needbadge = open.findViewById(R.id.needbadge);
        //Log.i("SYSBADGE -->", " " + sysbadge);

        // ingress.setOnClickListener(this);
        final WindowManager.LayoutParams params;
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {

            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);


            params.gravity = Gravity.TOP | Gravity.END;
            params.x = 70;
            params.y = 5;


            // windowManager.addView(chatHead, params);
            windowManager.addView(open, params);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    LAYOUT_FLAG,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                            | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                            | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                            | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    PixelFormat.TRANSLUCENT);


            params.gravity = Gravity.TOP | Gravity.END;
            params.x = 70;
            params.y = 5;
            windowManager.addView(open, params);
        }

       /* try {
            open.setOnTouchListener(new View.OnTouchListener() {
                private WindowManager.LayoutParams paramsF = params;

                private int initialX;
                private int initialY;
                private float initialTouchX;
                private float initialTouchY;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:

                            // Get current time in nano seconds.

                            initialX = paramsF.x;
                            initialY = paramsF.y;
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        case MotionEvent.ACTION_MOVE:
                            paramsF.x = initialX + (int) (event.getRawX() - initialTouchX);
                            paramsF.y = initialY + (int) (event.getRawY() - initialTouchY);

                            windowManager.updateViewLayout(open, paramsF);

                            break;
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            // TODO: handle exception
        }

*/


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (open != null) windowManager.removeView(open);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.gobackb:


                if (serviceCallBack != null) {
                    serviceCallBack.closewindow(comp, null, null, comp, null);
                }

                cres.unregisterContentObserver(obs);


                //Toast.makeText(this, "Back", Toast.LENGTH_SHORT).show();
                break;


        }


    }

    public class LocalBinder extends Binder {
        public OverlayCalibrationService getService() {
            // Return this instance of LocalService so clients can call public methods
            return OverlayCalibrationService.this;
        }
    }


}