package rocks.prxenon.aod.helper;

import android.content.Intent;

public interface ServiceCallBack {

    void connectToDevice(Intent intent);

    void connectToDeviceclip(Intent intent);

    void closewindow(String comp, String theartid, String themid, String basistext, String stats);
}