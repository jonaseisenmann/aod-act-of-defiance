package rocks.prxenon.aod;

import android.Manifest;
import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.FloatRange;
import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;

import io.github.dreierf.materialintroscreen.MessageButtonBehaviour;
import io.github.dreierf.materialintroscreen.SlideFragmentBuilder;
import io.github.dreierf.materialintroscreen.animations.IViewTranslation;
import rocks.prxenon.aod.helper.MaterialIntroActivityPx;
import rocks.prxenon.aod.introfragments.AODSlide;
import rocks.prxenon.aod.introfragments.CustomSlide;
import rocks.prxenon.aod.introfragments.FirstIntroSlide;
import rocks.prxenon.aod.introfragments.NiaIntroSlide;
import rocks.prxenon.aod.signin.FirebaseUIActivity;

public class IntroActivity extends MaterialIntroActivityPx {
    private static final int REQUEST_FILES = 300;
    private static String[] PERMISSIONS_FILES = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableLastSlideAlphaExitTransition(true);

        getBackButtonTranslationWrapper()
                .setEnterTranslation(new IViewTranslation() {
                    @Override
                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
                        view.setAlpha(percentage);
                    }
                });

        addSlide(new FirstIntroSlide());

        addSlide(new AODSlide());
        addSlide(new NiaIntroSlide(),

                new MessageButtonBehaviour(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // showMessage("We provide solutions to make you love your work");

                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(IntroActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(IntroActivity.this);
                        }
                        builder.setTitle("Open Ingress Website")
                                .setMessage("This opens the Ingress website in the external browser.\n\nDo you want to continue?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Uri uri = Uri.parse("https://ingress.com/");
                                        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uri);
                                        startActivity(launchBrowser);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher_round)
                                .show();


                    }
                }, "read more"));
        /*addSlide(new SlideFragmentBuilder()
                        .backgroundColor(R.color.primary)
                        .buttonsColor(R.color.primary_dark)
                        .image(R.drawable.missiondaylogo)
                        .title("Organize your Mission Days")
                        .description("Would you try?")
                        .build(),
                new MessageButtonBehaviour(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showMessage("We provide solutions to make you love your work");
                    }
                }, "Work with love"));
 */


        addSlide(new CustomSlide());


        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            addSlide(new SlideFragmentBuilder()
                    .backgroundColor(R.color.colorPrimaryDark)
                    .buttonsColor(R.color.colorAccent)

                    .title("We need some permissions")
                    .neededPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
                    .image(R.drawable.full_logo)

                    .description("please allow")
                    .build());
        }


    }

    @Override
    public void onFinish() {
        super.onFinish();
        //  Toast.makeText(this, "Try this library in your project! :)", Toast.LENGTH_SHORT).show();
        TaskStackBuilder.create(this)
                .addNextIntentWithParentStack(new Intent(this, FirebaseUIActivity.class))

                .startActivities();
    }

    @Override
    public void onBackPressed() {

        final View rootlayout2 = this.getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snack = Snackbar.make(rootlayout2, "Close AOD ?",
                Snackbar.LENGTH_SHORT)
                .setAction("Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        finishAffinity();

                    }
                });

        snack.show();

    }
}