package rocks.prxenon.aod.scanner;

import android.view.View;
import android.widget.ImageView;

import com.journeyapps.barcodescanner.CaptureActivity;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import rocks.prxenon.aod.R;


public class SmallCaptureActivity extends CaptureActivity {
    ImageView myImage;
    CompoundBarcodeView code;

    protected CompoundBarcodeView initializeContent() {
        setContentView(R.layout.scan_qr);
        myImage = findViewById(R.id.imgqr);
        code = findViewById(R.id.zxing_barcode_scanner);
        code.setVisibility(View.VISIBLE);
        myImage.setVisibility(View.GONE);
        return (CompoundBarcodeView) findViewById(R.id.zxing_barcode_scanner);
    }
}