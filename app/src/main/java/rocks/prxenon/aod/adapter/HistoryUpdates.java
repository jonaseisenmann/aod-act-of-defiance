package rocks.prxenon.aod.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoast.StyleableToast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.pages.AODMain;


public class HistoryUpdates extends BaseAdapter {

    private final Activity context;
    private final int ident;
    private final Long[] timeset;
    private final String[] values, keyis;
    private final String short1, missiontitle;
    private final String themonth, theartis, theid, theyear;
    private final HistoryUpdates historyadapter;


    public HistoryUpdates(Activity context, int ident, Long[] timeset, String[] values, String monthbadgeshort1, String monthbadgetitle, String[] updatekeyset, String theyear, String themonth, String theartis, String theid, HistoryUpdates historyadapter) {

        this.context = context;
        this.ident = ident;
        this.timeset = timeset;
        this.values = values;
        this.short1 = monthbadgeshort1;
        this.missiontitle = monthbadgetitle;
        this.keyis = updatekeyset;
        this.theartis = theartis;
        this.theid = theid;
        this.themonth = themonth;
        this.theyear = theyear;
        this.historyadapter = historyadapter;


    }


    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        // this.cache.initializeCache();
        if (convertView == null) {
            Log.i("View -->", " is null +" + position);
            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_history_updates, null);
            TextView history_value = gridView.findViewById(R.id.history_value);
            TextView history_value_sub = gridView.findViewById(R.id.history_value_sub);
            ImageView history_restore = gridView.findViewById(R.id.history_restore);
            ImageView history_remove = gridView.findViewById(R.id.history_remove);
            TextView history_value_title = gridView.findViewById(R.id.history_value_title);
            RelativeLayout history_restore_action = gridView.findViewById(R.id.history_restore_action);
            RelativeLayout history_remove_action = gridView.findViewById(R.id.history_remove_action);
            RelativeLayout listidemframe = gridView.findViewById(R.id.listidemframe);
            final RelativeLayout listidemframe_btns = gridView.findViewById(R.id.listidemframe_btns);
            final RelativeLayout layoutBottom = gridView.findViewById(R.id.layoutBottom);
            final FancyButton history_confirm = gridView.findViewById(R.id.history_confirm);
            final FancyButton history_confirm_not = gridView.findViewById(R.id.history_confirm_not);

            history_confirm_not.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    layoutBottom.setVisibility(View.VISIBLE);
                    listidemframe_btns.setVisibility(View.GONE);
                }
            });

            history_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    new AODMain().removeHistory(keyis[position], position, theyear, themonth, theartis, theid, context, historyadapter);
                    layoutBottom.setVisibility(View.VISIBLE);
                    listidemframe_btns.setVisibility(View.GONE);
                }
            });
            if (position == 0) {
                history_restore_action.setVisibility(View.GONE);
                listidemframe.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary, context.getTheme()));
                history_value_title.setText("Current Value");
                history_value_title.setTypeface(history_value_title.getTypeface(), Typeface.BOLD);

                history_remove_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layoutBottom.setVisibility(View.GONE);
                        listidemframe_btns.setVisibility(View.VISIBLE);
                        // StyleableToast.makeText(context, "Soon " + timeset[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    }
                });
                //history_restore_action.setEnabled(false);
                // history_restore.setColorFilter(context.getResources().getColor(R.color.colorPrimary, context.getTheme()));

            } else if (position == 1) {
                // history_restore_action.setEnabled(false);

                history_remove_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layoutBottom.setVisibility(View.GONE);
                        listidemframe_btns.setVisibility(View.VISIBLE);
                        // StyleableToast.makeText(context, "Soon " + timeset[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    }
                });

                history_restore_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //StyleableToast.makeText(context, "Soon, Key:" + keyis[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                        layoutBottom.setVisibility(View.GONE);
                        listidemframe_btns.setVisibility(View.VISIBLE);
                    }
                });
            } else if (position > 0) {
                history_restore_action.setEnabled(false);
                //history_restore_action.setEnabled(false);
                history_restore.setColorFilter(context.getResources().getColor(R.color.colorPrimary, context.getTheme()));
                history_remove_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layoutBottom.setVisibility(View.GONE);
                        listidemframe_btns.setVisibility(View.VISIBLE);
                    }
                });

                history_restore_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StyleableToast.makeText(context, "Soon, Key:" + keyis[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    }
                });
            }
            history_value.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(values[position])) + " " + short1);


            // history_value.setText(values[position] + " " + short1);
            //  history_value_sub.setText(""+timeset[position]);
            //


            Log.i("LOG ", values[position]);

            //  String dateString = event_date[position];

            Date oldDate = new Date(timeset[position] * 1000);
            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            long dv = timeset[position] * 1000;// its need to be in milisecond
            Date df = new java.util.Date(dv);

            SimpleDateFormat isoFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

            isoFormat.setTimeZone(TimeZone.getDefault());
            String vv = isoFormat.format(df);
            if (oldDate.before(currentDate)) {

                Log.e("oldDate", "is previous date");
                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                        + " hours: " + hours + " days: " + days);
                if (days >= 1) {

                    history_value_sub.setText(vv);

                } else if (hours >= 1) {
                    if (hours == 1) {
                        history_value_sub.setText("vor " + hours + " Stunde");
                    } else if (hours > 1 && hours < 23) {
                        history_value_sub.setText("vor " + hours + " Stunden");
                    } else {
                        history_value_sub.setText("vor einem Tag");
                    }
                } else if (minutes >= 0) {
                    if (minutes == 0) {
                        history_value_sub.setText("gerade eben");
                    } else if (minutes >= 1 && minutes <= 5) {
                        history_value_sub.setText("gerade eben");
                    } else {
                        history_value_sub.setText("vor " + minutes + " Minuten");
                    }
                }
            } else {
                history_value_sub.setText("noch kein Upload");
            }

            if (position > 0) {
                history_value_sub.setTextColor(context.getResources().getColor(R.color.colorGray, context.getTheme()));
            }

        } else {
            Log.i("View -->", " is not null +" + position);
            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_history_updates, null);
            TextView history_value = gridView.findViewById(R.id.history_value);
            TextView history_value_sub = gridView.findViewById(R.id.history_value_sub);
            ImageView history_restore = gridView.findViewById(R.id.history_restore);
            ImageView history_remove = gridView.findViewById(R.id.history_remove);
            TextView history_value_title = gridView.findViewById(R.id.history_value_title);
            RelativeLayout history_restore_action = gridView.findViewById(R.id.history_restore_action);
            RelativeLayout history_remove_action = gridView.findViewById(R.id.history_remove_action);
            RelativeLayout listidemframe = gridView.findViewById(R.id.listidemframe);
            final RelativeLayout listidemframe_btns = gridView.findViewById(R.id.listidemframe_btns);
            final RelativeLayout layoutBottom = gridView.findViewById(R.id.layoutBottom);
            final FancyButton history_confirm = gridView.findViewById(R.id.history_confirm);
            final FancyButton history_confirm_not = gridView.findViewById(R.id.history_confirm_not);

            history_confirm_not.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    layoutBottom.setVisibility(View.VISIBLE);
                    listidemframe_btns.setVisibility(View.GONE);
                }
            });

            history_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    new AODMain().removeHistory(keyis[position], position, theyear, themonth, theartis, theid, context, historyadapter);
                    layoutBottom.setVisibility(View.VISIBLE);
                    listidemframe_btns.setVisibility(View.GONE);
                }
            });
            if (position == 0) {
                history_restore_action.setVisibility(View.GONE);
                listidemframe.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary, context.getTheme()));
                history_value_title.setText("Current Value");
                history_value_title.setTypeface(history_value_title.getTypeface(), Typeface.BOLD);

                history_remove_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layoutBottom.setVisibility(View.GONE);
                        listidemframe_btns.setVisibility(View.VISIBLE);
                        // StyleableToast.makeText(context, "Soon " + timeset[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    }
                });
                //history_restore_action.setEnabled(false);
                // history_restore.setColorFilter(context.getResources().getColor(R.color.colorPrimary, context.getTheme()));

            } else if (position == 1) {
                // history_restore_action.setEnabled(false);

                history_remove_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layoutBottom.setVisibility(View.GONE);
                        listidemframe_btns.setVisibility(View.VISIBLE);
                        // StyleableToast.makeText(context, "Soon " + timeset[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    }
                });

                history_restore_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //StyleableToast.makeText(context, "Soon, Key:" + keyis[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                        layoutBottom.setVisibility(View.GONE);
                        listidemframe_btns.setVisibility(View.VISIBLE);
                    }
                });
            } else if (position > 0) {
                history_restore_action.setEnabled(false);
                //history_restore_action.setEnabled(false);
                history_restore.setColorFilter(context.getResources().getColor(R.color.colorPrimary, context.getTheme()));
                history_remove_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layoutBottom.setVisibility(View.GONE);
                        listidemframe_btns.setVisibility(View.VISIBLE);
                    }
                });

                history_restore_action.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StyleableToast.makeText(context, "Soon, Key:" + keyis[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    }
                });
            }

            history_value.setText(String.format(Locale.getDefault(), "%,d", Long.parseLong(values[position])) + " " + short1);


            // history_value.setText(values[position] + " " + short1);
            //  history_value_sub.setText(""+timeset[position]);
            //


            Log.i("LOG ", values[position]);

            //  String dateString = event_date[position];

            Date oldDate = new Date(timeset[position] * 1000);
            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            long dv = timeset[position] * 1000;// its need to be in milisecond
            Date df = new java.util.Date(dv);
            String vv = new SimpleDateFormat("dd/MM/yyyy hh:mm").format(df);
            if (oldDate.before(currentDate)) {

                Log.e("oldDate", "is previous date");
                Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                        + " hours: " + hours + " days: " + days);
                if (days >= 1) {

                    history_value_sub.setText(vv);

                } else if (hours >= 1) {
                    if (hours == 1) {
                        history_value_sub.setText("vor " + hours + " Stunde");
                    } else if (hours > 1 && hours < 23) {
                        history_value_sub.setText("vor " + hours + " Stunden");
                    } else {
                        history_value_sub.setText("vor einem Tag");
                    }
                } else if (minutes >= 0) {
                    if (minutes == 0) {
                        history_value_sub.setText("gerade eben");
                    } else if (minutes >= 1 && minutes <= 5) {
                        history_value_sub.setText("gerade eben");
                    } else {
                        history_value_sub.setText("vor " + minutes + " Minuten");
                    }
                }
            } else {
                history_value_sub.setText("noch kein Upload");
            }

            if (position > 0) {
                history_value_sub.setTextColor(context.getResources().getColor(R.color.colorGray, context.getTheme()));
            }
        }

        return gridView;

    }


}