package rocks.prxenon.aod.adapter;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.squareup.picasso.Transformation;

import java.util.List;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.events.NIAEvents;

import static rocks.prxenon.aod.events.NIAEvents.selectcountry;


public class SelectCountry extends ArrayAdapter<String> {

    private final Activity context;
    private final List<String> distinctcountry;


    public SelectCountry(Activity context, List<String> distinctcountry) {
        super(context, R.layout.list_filter, distinctcountry);
        // TODO Auto-generated constructor stub

        this.distinctcountry = distinctcountry;
        this.context = context;

    }

    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }

    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_filter, null);

        // this.cache.initializeCache();
        TextView txtplace = rowView.findViewById(R.id.turfnamet);


        Log.i("Change ->", "detected");
        txtplace.setText(NIAEvents.distinctcountry.get(position));
        if (selectcountry != null) {
            Log.i("Change2 ->", "detected");
            if (selectcountry.contains(NIAEvents.distinctcountry.get(position))) {
                //if(NIAEvents.themyturfs[position]) {
                int padding = txtplace.getPaddingTop();

                txtplace.setBackground(context.getResources().getDrawable(R.drawable.bottom_sharp_active, context.getTheme()));
                txtplace.setEnabled(false);
                txtplace.setTextColor(context.getResources().getColor(R.color.colorWhite, context.getTheme()));
                txtplace.setPadding(padding, padding, padding, padding);
            } else {
                Log.i("Change3 ->", "detected");
                int padding = txtplace.getPaddingTop();
                //Log.i("ISNOTENABLED"+position, "" + (String) (lv.getItemAtPosition(position)));
                txtplace.setBackground(context.getResources().getDrawable(R.drawable.bottom_sharp_gray, context.getTheme()));
                txtplace.setPadding(padding, padding, padding, padding);
                txtplace.setTextColor(context.getResources().getColor(R.color.colorWhite, context.getTheme()));
                txtplace.setEnabled(true);
            }
        }

        return rowView;

    }

    private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public class CropSquareTransformation implements Transformation {
        @Override
        public Bitmap transform(Bitmap b) {

            Bitmap output = Bitmap.createBitmap(b.getWidth(),
                    b.getHeight(), Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, b.getWidth(), b.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = b.getWidth() / 2;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);

            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(b, rect, rect, paint);

            if (output != b) {
                b.recycle();
            }
            return output;
        }

        @Override
        public String key() {
            return "square()";
        }
    }
}