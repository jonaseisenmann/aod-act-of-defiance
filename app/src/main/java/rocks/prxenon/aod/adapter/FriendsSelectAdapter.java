package rocks.prxenon.aod.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;

import com.github.siyamed.shapeimageview.OctogonImageView;
import com.goodiebag.horizontalpicker.HorizontalPicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Arrays;
import java.util.Map;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.pages.pvp.PvPCreate;


public class FriendsSelectAdapter extends BaseAdapter {

    private final Activity context;
    private final int ident;

    private final String[] friendids, friendnamenames, preffriends;

    private final FriendsSelectAdapter historyadapter;
    private final Integer friendnumber;
    private final OctogonImageView nextfriend, friend;
    private final TextView friendname;
    private final AlertDialog friendsbuilder;
    private final FloatingActionButton fabsave;
    private final TextView textinfo;
    private final HorizontalPicker hpruntime;
    private final Map<String, Object> pvpupdate;
    private final NestedScrollView pvp_frame, pvp_result;

    public FriendsSelectAdapter(Activity context, int ident, String[] friendids, FriendsSelectAdapter historyadapter, Integer friendnumber, OctogonImageView nextfriend, OctogonImageView friend, TextView friendname, AlertDialog friendsbuilder, String[] friendnamenames, String[] preffriends, FloatingActionButton fabsave, TextView textinfo, HorizontalPicker hpruntime, Map<String, Object> pvpupdate, NestedScrollView pvp_frame, NestedScrollView pvp_result) {

        this.context = context;
        this.ident = ident;
        this.friendids = friendids;
        this.friend = friend;
        this.friendnumber = friendnumber;
        this.nextfriend = nextfriend;
        this.friendname = friendname;
        this.historyadapter = historyadapter;
        this.friendsbuilder = friendsbuilder;
        this.friendnamenames = friendnamenames;
        this.preffriends = preffriends;
        this.fabsave = fabsave;
        this.textinfo = textinfo;
        this.hpruntime = hpruntime;
        this.pvpupdate = pvpupdate;
        this.pvp_frame = pvp_frame;
        this.pvp_result = pvp_result;

    }


    @Override
    public int getCount() {
        return friendids.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        // this.cache.initializeCache();
        if (convertView == null) {
            //  Log.i("View -->", " is null +" + position);
            // gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_pvp_friend_select, null);
            final TextView agent_value = gridView.findViewById(R.id.agent_value);
            final RelativeLayout history_remove_action = gridView.findViewById(R.id.history_remove_action);

            agent_value.setText(friendnamenames[position]);

            history_remove_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int indexofname = Arrays.asList(friendnamenames).indexOf(preffriends[position]);

                    Log.i("indexes --> ", " original :" + Arrays.asList(friendnamenames).indexOf(friendnamenames[position]) + " new: " + indexofname);

                    new PvPCreate().setAgent(friendids[indexofname], context, historyadapter, friend, nextfriend, friendnumber, friendname, friendsbuilder, friendnamenames[position], fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result, friendnamenames);
                    // StyleableToast.makeText(context, "Soon " + timeset[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                }
            });

        } else {
            // Log.i("View -->", " is not null +" + position);
            gridView = convertView;
            final TextView agent_value = gridView.findViewById(R.id.agent_value);
            final RelativeLayout history_remove_action = gridView.findViewById(R.id.history_remove_action);

            agent_value.setText(friendnamenames[position]);

            history_remove_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int indexofname = Arrays.asList(friendnamenames).indexOf(preffriends[position]);
                    Log.i("indexes --> ", " original :" + Arrays.asList(friendnamenames).indexOf(friendnamenames[position]) + " new: " + indexofname);
                    new PvPCreate().setAgent(friendids[indexofname], context, historyadapter, friend, nextfriend, friendnumber, friendname, friendsbuilder, friendnamenames[position], fabsave, textinfo, hpruntime, pvpupdate, pvp_frame, pvp_result, friendnamenames);
                    // StyleableToast.makeText(context, "Soon " + timeset[position], Toast.LENGTH_LONG, R.style.defaulttoast).show();
                }
            });
        }

        return gridView;

    }


}