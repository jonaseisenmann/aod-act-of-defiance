package rocks.prxenon.aod.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.muddzdev.styleabletoast.StyleableToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.pages.AODMain;
import rocks.prxenon.aod.pages.profile.ProfileMain;
import rocks.prxenon.aod.pages.pvp.PvPCreate;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mbpvp;
import static rocks.prxenon.aod.basics.AODConfig.user;


public class InboxAdapter extends BaseAdapter {

    private final Activity context;
    private List<String> inboxart = new ArrayList();
    private List<String> title = new ArrayList();
    private List<Boolean> readstatus = new ArrayList();
    private List<Long> timestamp = new ArrayList();
    private List<String> agentuid = new ArrayList();
    private List<String> linkidpvp = new ArrayList();
    private List<String> inboxid = new ArrayList();

    //inboxart, title, timestamp, agentuid
    public InboxAdapter(Activity context, List<String> inboxart, List<String> title, List<Long> timestamp, List<String> agentuid, List<Boolean> readstatus, List<String> linkidpvp, List<String> inboxid) {

        this.context = context;
        this.inboxart = inboxart;
        this.title = title;
        this.timestamp = timestamp;
        this.agentuid = agentuid;
        this.readstatus = readstatus;
        this.linkidpvp = linkidpvp;
        this.inboxid = inboxid;

    }


    @Override
    public int getCount() {
        return inboxart.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;


        if (convertView == null) {
            //Log.i("View -->", " is null +" + position);

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_inbox, null);


            final CircleImageView inboximage = gridView.findViewById(R.id.content_agentimage);
            final TextView content_inboxart = gridView.findViewById(R.id.content_inboxart);

            final TextView inbox_date = gridView.findViewById(R.id.inbox_date_header);
            final TextView content_inbox_long = gridView.findViewById(R.id.content_content);
            final ImageView btn_read = gridView.findViewById(R.id.btn_read);
            final ImageView btn_goto = gridView.findViewById(R.id.btn_goto);
            if (readstatus.get(position)) {
                btn_read.setClickable(false);
                btn_read.setEnabled(false);
                btn_read.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_done_all_dis));


            } else {

                btn_read.setClickable(true);
                btn_read.setEnabled(true);
                final Map<String, Object> asread = new HashMap<>();

                asread.put("/read", true);
                btn_read.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).updateChildren(asread).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    //finish();
                                    btn_read.setEnabled(false);
                                    btn_read.setClickable(false);
                                    btn_read.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_done_all_dis));


                                } else if (task.isCanceled()) {

                                }
                            }

                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {


                            }
                        });
                    }
                });
            }

            Date oldDate = new Date(timestamp.get(position));
            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            long dv = timestamp.get(position);// its need to be in milisecond
            Date df = new java.util.Date(dv);
            String vv = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(df);


            Log.e("oldDate", "is previous date");
            Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                    + " hours: " + hours + " days: " + days);
            if (days >= 1) {

                inbox_date.setText(vv);

            } else if (hours >= 1) {
                if (hours == 1) {
                    inbox_date.setText("vor " + hours + " Stunde");
                } else if (hours > 1 && hours < 23) {
                    inbox_date.setText("vor " + hours + " Stunden");
                } else {
                    inbox_date.setText("vor einem Tag");
                }
            } else if (minutes >= 0) {
                if (minutes == 0) {
                    inbox_date.setText("gerade eben");
                } else if (minutes >= 1 && minutes <= 5) {
                    inbox_date.setText("gerade eben");
                } else {
                    inbox_date.setText("vor " + minutes + " Minuten");
                }
            }


            //inbox_date.setText(""+timestamp.get(position));
            if (inboxart.get(position).equalsIgnoreCase("friendrejected")) {
                content_inboxart.setText(context.getResources().getString(R.string.string_friend_rejected));
                btn_goto.setVisibility(View.GONE);

                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotagentname) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_rejected_long), snapshotagentname.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_rejected_long), snapshotagentname.getValue().toString())));

                        }

                        btn_goto.setVisibility(View.GONE);

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });


            } else if (inboxart.get(position).equalsIgnoreCase("friendaccepted")) {
                content_inboxart.setText(context.getResources().getString(R.string.string_friend_accepted));
                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotagentname) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_accepted_long), snapshotagentname.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_accepted_long), snapshotagentname.getValue().toString())));

                        }

                        btn_goto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                final Map<String, Object> asread = new HashMap<>();

                                asread.put("/read", true);

                                mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).updateChildren(asread).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            Intent i = new Intent(context, ProfileMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();


                                        } else if (task.isCanceled()) {
                                            Intent i = new Intent(context, ProfileMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Intent i = new Intent(context, ProfileMain.class);
                                        //i.putExtra("PersonID", personID);
                                        context.startActivity(i);
                                        context.finish();
                                    }
                                });


                            }
                        });

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else if (inboxart.get(position).equalsIgnoreCase("friendrequest")) {
                content_inboxart.setText(context.getResources().getString(R.string.string_friend_accepted));
                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotagentname) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_request_long), snapshotagentname.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_request_long), snapshotagentname.getValue().toString())));

                        }

                        btn_goto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                final Map<String, Object> asread = new HashMap<>();

                                asread.put("/read", true);

                                mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).updateChildren(asread).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            Intent i = new Intent(context, ProfileMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();


                                        } else if (task.isCanceled()) {
                                            Intent i = new Intent(context, ProfileMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Intent i = new Intent(context, ProfileMain.class);
                                        //i.putExtra("PersonID", personID);
                                        context.startActivity(i);
                                        context.finish();
                                    }
                                });


                            }
                        });

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else if (inboxart.get(position).equalsIgnoreCase("pvpinvite")) {

                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotname) {


                        content_inboxart.setText("PvP invite from " + snapshotname.getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                });

                mbpvp.child(linkidpvp.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotpvpdata) {

                        if (snapshotpvpdata.child("pvptitle").exists()) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString(), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString()));

                            }

                            btn_goto.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // pvpid


                                    final Map<String, Object> asread = new HashMap<>();

                                    asread.put("/read", true);

                                    mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).updateChildren(asread).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                //finish();
                                                Intent openPvP = new Intent(context, PvPCreate.class);
                                                openPvP.putExtra("accepted", true);
                                                openPvP.putExtra("pvpkey", snapshotpvpdata.child("pvpid").getValue().toString());
                                                openPvP.putExtra("inboxid", String.valueOf(timestamp.get(position)));
                                                context.startActivity(openPvP);
                                                context.finish();


                                            } else if (task.isCanceled()) {

                                            }
                                        }

                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(Exception e) {

                                            Intent i = new Intent(context, AODMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();
                                        }
                                    });


                                }
                            });
                        } else {
                            content_inbox_long.setText("PvP no longer available. ");
                            btn_goto.setVisibility(View.GONE);
                            btn_read.setVisibility(View.GONE);
                            mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).setValue(null);

                        }

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else if (inboxart.get(position).equalsIgnoreCase("pvprejected")) {

                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotname) {


                        content_inboxart.setText(snapshotname.getValue().toString() + " rejected your invitation.");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                });

                mbpvp.child(linkidpvp.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotpvpdata) {

                        if (snapshotpvpdata.child("pvptitle").exists()) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString(), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString()));

                            }


                        } else {
                            content_inbox_long.setText("PvP no longer available. ");
                            btn_goto.setVisibility(View.GONE);
                            btn_read.setVisibility(View.GONE);
                            mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).setValue(null);

                        }

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else if (inboxart.get(position).equalsIgnoreCase("pvpaccepted")) {

                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotname) {


                        content_inboxart.setText(snapshotname.getValue().toString() + " accepted your invitation.");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                });

                mbpvp.child(linkidpvp.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotpvpdata) {

                        if (snapshotpvpdata.child("pvptitle").exists()) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString(), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString()));

                            }


                        } else {
                            content_inbox_long.setText("PvP no longer available. ");
                            btn_goto.setVisibility(View.GONE);
                            btn_read.setVisibility(View.GONE);
                            mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).setValue(null);

                        }

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else {
                content_inboxart.setText(title.get(position));
                content_inbox_long.setText("");
            }
            content_inboxart.setSelected(true);
            inboximage.setBorderColor(context.getResources().getColor(R.color.colorPrimary));


            if (inboxart.get(position).contains("friend") || inboxart.get(position).contains("pvpinvite") || inboxart.get(position).contains("pvpaccepted") || inboxart.get(position).contains("pvprejected")) {

                mDatabase.child(agentuid.get(position)).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotbadge) {


                        try {
                            PicassoCache.getPicassoInstance(context)
                                    .load(snapshotbadge.getValue().toString() + "?sz=150")
                                    .placeholder(R.drawable.full_logo)
                                    .error(R.drawable.full_logo)


                                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                    //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                    //.transform(new CropRoundTransformation())
                                    .into(inboximage);
                            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                        } catch (Exception e) {
                            //Log.e("Error", e.getMessage());

                            e.printStackTrace();
                        }
                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                });
            }


        } else {
            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_inbox, null);


            final CircleImageView inboximage = gridView.findViewById(R.id.content_agentimage);
            final TextView content_inboxart = gridView.findViewById(R.id.content_inboxart);

            final TextView inbox_date = gridView.findViewById(R.id.inbox_date_header);
            final TextView content_inbox_long = gridView.findViewById(R.id.content_content);
            final ImageView btn_read = gridView.findViewById(R.id.btn_read);
            final ImageView btn_goto = gridView.findViewById(R.id.btn_goto);
            if (readstatus.get(position)) {
                btn_read.setClickable(false);
                btn_read.setEnabled(false);
                btn_read.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_done_all_dis));


            } else {

                btn_read.setClickable(true);
                btn_read.setEnabled(true);
                final Map<String, Object> asread = new HashMap<>();

                asread.put("/read", true);
                btn_read.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).updateChildren(asread).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    //finish();
                                    btn_read.setEnabled(false);
                                    btn_read.setClickable(false);
                                    btn_read.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_done_all_dis));


                                } else if (task.isCanceled()) {

                                }
                            }

                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(Exception e) {


                            }
                        });
                    }
                });
            }

            Date oldDate = new Date(timestamp.get(position));
            Date currentDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;
            long dv = timestamp.get(position);// its need to be in milisecond
            Date df = new java.util.Date(dv);
            String vv = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(df);


            Log.e("oldDate", "is previous date");
            Log.e("Difference: ", " seconds: " + seconds + " minutes: " + minutes
                    + " hours: " + hours + " days: " + days);
            if (days >= 1) {

                inbox_date.setText(vv);

            } else if (hours >= 1) {
                if (hours == 1) {
                    inbox_date.setText("vor " + hours + " Stunde");
                } else if (hours > 1 && hours < 23) {
                    inbox_date.setText("vor " + hours + " Stunden");
                } else {
                    inbox_date.setText("vor einem Tag");
                }
            } else if (minutes >= 0) {
                if (minutes == 0) {
                    inbox_date.setText("gerade eben");
                } else if (minutes >= 1 && minutes <= 5) {
                    inbox_date.setText("gerade eben");
                } else {
                    inbox_date.setText("vor " + minutes + " Minuten");
                }
            }


            //inbox_date.setText(""+timestamp.get(position));
            if (inboxart.get(position).equalsIgnoreCase("friendrejected")) {
                content_inboxart.setText(context.getResources().getString(R.string.string_friend_rejected));
                btn_goto.setVisibility(View.GONE);

                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotagentname) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_rejected_long), snapshotagentname.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_rejected_long), snapshotagentname.getValue().toString())));

                        }

                        btn_goto.setVisibility(View.GONE);

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });


            } else if (inboxart.get(position).equalsIgnoreCase("friendaccepted")) {
                content_inboxart.setText(context.getResources().getString(R.string.string_friend_accepted));
                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotagentname) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_accepted_long), snapshotagentname.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_accepted_long), snapshotagentname.getValue().toString())));

                        }

                        btn_goto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                final Map<String, Object> asread = new HashMap<>();

                                asread.put("/read", true);

                                mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).updateChildren(asread).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            Intent i = new Intent(context, ProfileMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();


                                        } else if (task.isCanceled()) {
                                            Intent i = new Intent(context, ProfileMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Intent i = new Intent(context, ProfileMain.class);
                                        //i.putExtra("PersonID", personID);
                                        context.startActivity(i);
                                        context.finish();
                                    }
                                });


                            }
                        });

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else if (inboxart.get(position).equalsIgnoreCase("friendrequest")) {
                content_inboxart.setText(context.getResources().getString(R.string.string_friend_accepted));
                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotagentname) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_request_long), snapshotagentname.getValue().toString()), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            content_inbox_long.setText(Html.fromHtml(String.format(context.getResources().getString(R.string.string_friend_request_long), snapshotagentname.getValue().toString())));

                        }

                        btn_goto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                final Map<String, Object> asread = new HashMap<>();

                                asread.put("/read", true);

                                mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).updateChildren(asread).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            Intent i = new Intent(context, ProfileMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();


                                        } else if (task.isCanceled()) {
                                            Intent i = new Intent(context, ProfileMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {

                                        Intent i = new Intent(context, ProfileMain.class);
                                        //i.putExtra("PersonID", personID);
                                        context.startActivity(i);
                                        context.finish();
                                    }
                                });


                            }
                        });

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else if (inboxart.get(position).equalsIgnoreCase("pvpinvite")) {

                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotname) {


                        content_inboxart.setText("PvP invite from " + snapshotname.getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                });

                mbpvp.child(linkidpvp.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotpvpdata) {

                        if (snapshotpvpdata.child("pvptitle").exists()) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString(), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString()));

                            }

                            btn_goto.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // pvpid


                                    final Map<String, Object> asread = new HashMap<>();

                                    asread.put("/read", true);

                                    mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).updateChildren(asread).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                //finish();
                                                Intent openPvP = new Intent(context, PvPCreate.class);
                                                openPvP.putExtra("accepted", true);
                                                openPvP.putExtra("pvpkey", snapshotpvpdata.child("pvpid").getValue().toString());
                                                openPvP.putExtra("inboxid", String.valueOf(timestamp.get(position)));
                                                context.startActivity(openPvP);
                                                context.finish();


                                            } else if (task.isCanceled()) {

                                            }
                                        }

                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(Exception e) {

                                            Intent i = new Intent(context, AODMain.class);
                                            //i.putExtra("PersonID", personID);
                                            context.startActivity(i);
                                            context.finish();
                                        }
                                    });


                                }
                            });
                        } else {
                            content_inbox_long.setText("PvP no longer available. ");
                            btn_goto.setVisibility(View.GONE);
                            btn_read.setVisibility(View.GONE);
                            mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).setValue(null);

                        }

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else if (inboxart.get(position).equalsIgnoreCase("pvprejected")) {

                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotname) {


                        content_inboxart.setText(snapshotname.getValue().toString() + " rejected your invitation.");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                });

                mbpvp.child(linkidpvp.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotpvpdata) {

                        if (snapshotpvpdata.child("pvptitle").exists()) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString(), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString()));

                            }


                        } else {
                            content_inbox_long.setText("PvP no longer available. ");
                            btn_goto.setVisibility(View.GONE);
                            btn_read.setVisibility(View.GONE);
                            mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).setValue(null);

                        }

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else if (inboxart.get(position).equalsIgnoreCase("pvpaccepted")) {

                mDatabase.child(agentuid.get(position)).child("agentname").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotname) {


                        content_inboxart.setText(snapshotname.getValue().toString() + " accepted your invitation.");
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                });

                mbpvp.child(linkidpvp.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotpvpdata) {

                        if (snapshotpvpdata.child("pvptitle").exists()) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString(), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                content_inbox_long.setText(Html.fromHtml("<b>PvP Mission: </b>" + snapshotpvpdata.child("pvptitle").getValue().toString() + "<br><b>Runtime: </b> " + snapshotpvpdata.child("runtime_text").getValue().toString()));

                            }


                        } else {
                            content_inbox_long.setText("PvP no longer available. ");
                            btn_goto.setVisibility(View.GONE);
                            btn_read.setVisibility(View.GONE);
                            mDatabase.child(user.getUid()).child("newinbox").child(String.valueOf(timestamp.get(position))).setValue(null);

                        }

                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        content_inbox_long.setText("Error on read Data");
                        btn_goto.setVisibility(View.GONE);
                    }
                });
            } else {
                content_inboxart.setText(title.get(position));
                content_inbox_long.setText("");
            }
            content_inboxart.setSelected(true);
            inboximage.setBorderColor(context.getResources().getColor(R.color.colorPrimary));


            if (inboxart.get(position).contains("friend") || inboxart.get(position).contains("pvpinvite") || inboxart.get(position).contains("pvpaccepted") || inboxart.get(position).contains("pvprejected")) {

                mDatabase.child(agentuid.get(position)).child("profileimage").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(final DataSnapshot snapshotbadge) {


                        try {
                            PicassoCache.getPicassoInstance(context)
                                    .load(snapshotbadge.getValue().toString() + "?sz=150")
                                    .placeholder(R.drawable.full_logo)
                                    .error(R.drawable.full_logo)


                                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                    //  .networkPolicy(NetworkPolicy.NO_CACHE)
                                    //.transform(new CropRoundTransformation())
                                    .into(inboximage);
                            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                        } catch (Exception e) {
                            //Log.e("Error", e.getMessage());

                            e.printStackTrace();
                        }
                        //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                });
            }
        }


        return gridView;

    }


}