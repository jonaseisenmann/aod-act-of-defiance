package rocks.prxenon.aod.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.helper.PicassoCache;
import rocks.prxenon.aod.pages.profile.ProfileMain;


public class FriendsInvitesAdapter extends BaseAdapter {

    private final Activity context;
    private final int uuidofagentsize;

    private final List<String> uuidofagent, imageofagent, nameofagent;
    private final List<Integer> getartofrequest;


    public FriendsInvitesAdapter(Activity context, int ident, List<String> uuidofagent, List<String> imageofagent, List<String> nameofagent, List<Integer> getartofrequest) {

        this.context = context;
        this.uuidofagentsize = ident;


        this.uuidofagent = uuidofagent;
        this.imageofagent = imageofagent;
        this.nameofagent = nameofagent;
        this.getartofrequest = getartofrequest;


    }


    @Override
    public int getCount() {
        return uuidofagent.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;


        // M1901300330
        // W190711400410


        if (convertView == null) {
            Log.i("View -->", " is null +" + position + " artis: " + getartofrequest.get(position));

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_friends_requests, null);


            final CircleImageView rankagentimage = gridView.findViewById(R.id.drawableimage);
            final TextView ranking_agentname = gridView.findViewById(R.id.friend_name);
            final TextView friend_withdraw = gridView.findViewById(R.id.friend_withdraw);
            final TextView friend_dismiss = gridView.findViewById(R.id.friend_dismiss);
            final TextView friend_confirm = gridView.findViewById(R.id.friend_confirm);
            ranking_agentname.setText(nameofagent.get(position));

            rankagentimage.setBorderColor(context.getResources().getColor(R.color.colorAccent));

            final TextView friend_headline = gridView.findViewById(R.id.friend_headline);

            // 1 = I HAVE TO CONFIRM // 2 = I REQUESTED AND WAIT
            if (getartofrequest.get(position) == 1) {
                friend_headline.setText(context.getResources().getString(R.string.string_request_from));
                friend_dismiss.setVisibility(View.VISIBLE);
                friend_confirm.setVisibility(View.VISIBLE);
                final View finalGridView = gridView;
                friend_dismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new ProfileMain().rejectRequest(context, position, getartofrequest.get(position), uuidofagent.get(position), finalGridView);
                    /*    new AODMain().removeHistory(keyis[position], position, theyear, themonth, theartis, theid, context, historyadapter);
                        layoutBottom.setVisibility(View.VISIBLE);
                        listidemframe_btns.setVisibility(View.GONE);
                        */
                    }
                });

                friend_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new ProfileMain().acceptRequest(context, position, getartofrequest.get(position), uuidofagent.get(position), finalGridView);
                    /*    new AODMain().removeHistory(keyis[position], position, theyear, themonth, theartis, theid, context, historyadapter);
                        layoutBottom.setVisibility(View.VISIBLE);
                        listidemframe_btns.setVisibility(View.GONE);
                        */
                    }
                });
            } else {
                friend_headline.setText(context.getResources().getString(R.string.string_pending_request));
                friend_withdraw.setVisibility(View.VISIBLE);
                final View finalGridView = gridView;
                friend_withdraw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new ProfileMain().withdrawRequest(context, position, getartofrequest.get(position), uuidofagent.get(position), finalGridView);
                    /*    new AODMain().removeHistory(keyis[position], position, theyear, themonth, theartis, theid, context, historyadapter);
                        layoutBottom.setVisibility(View.VISIBLE);
                        listidemframe_btns.setVisibility(View.GONE);
                        */
                    }
                });
            }


            try {
                PicassoCache.getPicassoInstance(context)
                        .load(imageofagent.get(position) + "?sz=100")
                        .fit().centerInside()
                        .placeholder(R.drawable.full_logo)
                        .error(R.drawable.full_logo)

                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                        //.transform(new CropRoundTransformation())
                        .into(rankagentimage);
                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
            } catch (Exception e) {
                //Log.e("Error", e.getMessage());

                e.printStackTrace();
            }


        } else {
            Log.i("View -->", " is not null +" + position + " artis: " + getartofrequest.get(position));
            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_friends_requests, null);


            final CircleImageView rankagentimage = gridView.findViewById(R.id.drawableimage);
            final TextView ranking_agentname = gridView.findViewById(R.id.friend_name);
            final TextView friend_withdraw = gridView.findViewById(R.id.friend_withdraw);
            final TextView friend_dismiss = gridView.findViewById(R.id.friend_dismiss);
            final TextView friend_confirm = gridView.findViewById(R.id.friend_confirm);

            final TextView friend_headline = gridView.findViewById(R.id.friend_headline);

            // 1 = I HAVE TO CONFIRM // 2 = I REQUESTED AND WAIT
            if (getartofrequest.get(position) == 1) {
                friend_headline.setText(context.getResources().getString(R.string.string_request_from));
                friend_dismiss.setVisibility(View.VISIBLE);
                friend_confirm.setVisibility(View.VISIBLE);

                final View finalGridView = gridView;
                friend_dismiss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new ProfileMain().rejectRequest(context, position, getartofrequest.get(position), uuidofagent.get(position), finalGridView);
                    /*    new AODMain().removeHistory(keyis[position], position, theyear, themonth, theartis, theid, context, historyadapter);
                        layoutBottom.setVisibility(View.VISIBLE);
                        listidemframe_btns.setVisibility(View.GONE);
                        */
                    }
                });

                friend_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new ProfileMain().acceptRequest(context, position, getartofrequest.get(position), uuidofagent.get(position), finalGridView);
                    /*    new AODMain().removeHistory(keyis[position], position, theyear, themonth, theartis, theid, context, historyadapter);
                        layoutBottom.setVisibility(View.VISIBLE);
                        listidemframe_btns.setVisibility(View.GONE);
                        */
                    }
                });
            } else {
                friend_headline.setText(context.getResources().getString(R.string.string_pending_request));
                friend_withdraw.setVisibility(View.VISIBLE);
                final View finalGridView1 = gridView;
                friend_withdraw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new ProfileMain().withdrawRequest(context, position, getartofrequest.get(position), uuidofagent.get(position), finalGridView1);
                    /*    new AODMain().removeHistory(keyis[position], position, theyear, themonth, theartis, theid, context, historyadapter);
                        layoutBottom.setVisibility(View.VISIBLE);
                        listidemframe_btns.setVisibility(View.GONE);
                        */
                    }
                });
            }


            ranking_agentname.setText(nameofagent.get(position));


            try {
                PicassoCache.getPicassoInstance(context)
                        .load(imageofagent.get(position) + "?sz=100")
                        .fit().centerInside()
                        .placeholder(R.drawable.full_logo)
                        .error(R.drawable.full_logo)

                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                        //.transform(new CropRoundTransformation())
                        .into(rankagentimage);
                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
            } catch (Exception e) {
                //Log.e("Error", e.getMessage());

                e.printStackTrace();
            }


        }


        return gridView;

    }

    private String getStartEndOFWeek(int enterWeek, int enterYear) {
//enterWeek is week number
//enterYear is year
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy"); // PST`
        Date startDate = calendar.getTime();
        String startDateInStr = formatter.format(startDate);
        System.out.println("...date..." + startDateInStr);
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.add(Calendar.DATE, 6);
        Date enddate = calendar.getTime();
        String endDaString = formatter.format(enddate);
        System.out.println("...date..." + endDaString);

        return "" + startDateInStr + " - " + endDaString;
    }

    private String getMonthofyear(int monthis, int enterYear) {
//enterWeek is week number
//enterYear is year
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.MONTH, (monthis - 1));
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy"); // PST`
        Date startDate = calendar.getTime();
        String monthStr = formatter.format(startDate);


        return "" + monthStr + " Mission";
    }
}