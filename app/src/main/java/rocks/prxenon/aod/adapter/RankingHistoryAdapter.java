package rocks.prxenon.aod.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.muddzdev.styleabletoast.StyleableToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.R;

import static rocks.prxenon.aod.basics.AODConfig.mDatabaseaod;


public class RankingHistoryAdapter extends BaseAdapter {

    private final Activity context;
    private List<String> badgeid = new ArrayList();
    private List<String> keymonthweek = new ArrayList();
    private List<Integer> agentssum = new ArrayList();
    private List<Integer> sumall = new ArrayList();
    private List<String> missionid = new ArrayList();
    private List<String> theyearofmission = new ArrayList<>();


    public RankingHistoryAdapter(Activity context, List<String> badgeid, List<String> keymonthweek, List<Integer> agentssum, List<Integer> sumall, List<String> missionid, List<String> theyearofmission) {

        this.context = context;
        this.badgeid = badgeid;
        this.keymonthweek = keymonthweek;
        this.agentssum = agentssum;
        this.sumall = sumall;
        this.missionid = missionid;
        this.theyearofmission = theyearofmission;


    }


    @Override
    public int getCount() {
        return missionid.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;
        final String cartis;

        if (missionid.get(position).startsWith("W")) {
            cartis = "week";
        } else if (missionid.get(position).startsWith("AOD")) {
            cartis = "earth";
        } else if (missionid.get(position).startsWith("Archiv")) {
            cartis = "archiv";
        } else {
            cartis = "month";
        }

        // M1901300330
        // W190711400410


        if (convertView == null) {
            //Log.i("View -->", " is null +" + position);

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_ranking_adapter_history, null);


            final CircleImageView rankagentimage = gridView.findViewById(R.id.drawableimage);
            final TextView ranking_agentname = gridView.findViewById(R.id.mission_title);


            rankagentimage.setBorderColor(context.getResources().getColor(R.color.colorAccent));

            final TextView mission_date = gridView.findViewById(R.id.mission_date);


            if (cartis.equalsIgnoreCase("week")) {

                mission_date.setText(getStartEndOFWeek(Integer.parseInt(keymonthweek.get(position).substring(0, 2)), Integer.parseInt(theyearofmission.get(position))));


            }

            if (cartis.equalsIgnoreCase("month")) {

                mission_date.setText(getMonthofyear(Integer.parseInt(keymonthweek.get(position).substring(0, 2)), Integer.parseInt(theyearofmission.get(position))));


            }

            if (cartis.equalsIgnoreCase("archiv")) {

                mission_date.setText("more HISTORY");


            }

            if (cartis.equalsIgnoreCase("Current")) {

                mission_date.setText("Current Year");


            }

            if (cartis.equalsIgnoreCase("earth")) {
                mission_date.setText("EARTH MISSION");
            }


            mDatabaseaod.child("badge").child(badgeid.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(final DataSnapshot snapshotbadge) {


                    ranking_agentname.setText(snapshotbadge.child("title").getValue().toString());

                    int badgemonthid_content_com = context.getResources().getIdentifier(snapshotbadge.child("drawable").getValue().toString(), "drawable", context.getPackageName());

                    //badgemonth = getResources().obtainTypedArray(badgemonthid);
                    if (badgemonthid_content_com != 0) {
                        //   Log.w("BADGE -->", " id :"+badgemonthid_content);


                        rankagentimage.setImageDrawable(context.getDrawable(badgemonthid_content_com));
                    }
                    //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                }
            });


        } else {
            //  Log.i("View -->", " is not null +" + position);
            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_ranking_adapter_history, null);


            final CircleImageView rankagentimage = gridView.findViewById(R.id.drawableimage);
            final TextView ranking_agentname = gridView.findViewById(R.id.mission_title);

            final TextView mission_date = gridView.findViewById(R.id.mission_date);
            if (cartis.equalsIgnoreCase("week")) {

                mission_date.setText(getStartEndOFWeek(Integer.parseInt(keymonthweek.get(position).substring(0, 2)), Integer.parseInt(theyearofmission.get(position))));


            }

            if (cartis.equalsIgnoreCase("month")) {

                mission_date.setText(getMonthofyear(Integer.parseInt(keymonthweek.get(position).substring(0, 2)), Integer.parseInt(theyearofmission.get(position))));


            }

            if (cartis.equalsIgnoreCase("archiv")) {

                mission_date.setText("more HISTORY");


            }

            if (cartis.equalsIgnoreCase("Current")) {

                mission_date.setText("Current Year");


            }
            if (cartis.equalsIgnoreCase("earth")) {
                mission_date.setText("EARTH MISSION");
            }
            rankagentimage.setBorderColor(context.getResources().getColor(R.color.colorAccent));


            mDatabaseaod.child("badge").child(badgeid.get(position)).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(final DataSnapshot snapshotbadge) {


                    ranking_agentname.setText(snapshotbadge.child("title").getValue().toString());
                    int badgemonthid_content_com = context.getResources().getIdentifier(snapshotbadge.child("drawable").getValue().toString(), "drawable", context.getPackageName());

                    //badgemonth = getResources().obtainTypedArray(badgemonthid);
                    if (badgemonthid_content_com != 0) {
                        //   Log.w("BADGE -->", " id :"+badgemonthid_content);


                        rankagentimage.setImageDrawable(context.getDrawable(badgemonthid_content_com));
                    }
                    //   missiondrawable.add(snapshotbadge.child("drawable").getValue().toString());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    StyleableToast.makeText(context, "Error", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                }
            });

        }


        return gridView;

    }

    private String getStartEndOFWeek(int enterWeek, int enterYear) {
//enterWeek is week number
//enterYear is year
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy"); // PST`
        Date startDate = calendar.getTime();
        String startDateInStr = formatter.format(startDate);
        // System.out.println("...date..."+startDateInStr);
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.add(Calendar.DATE, 6);
        Date enddate = calendar.getTime();
        String endDaString = formatter.format(enddate);
        // System.out.println("...date..."+endDaString);

        return "" + startDateInStr + " - " + endDaString;
    }

    private String getMonthofyear(int monthis, int enterYear) {
//enterWeek is week number
//enterYear is year
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.MONTH, (monthis - 1));
        calendar.set(Calendar.YEAR, enterYear);

        SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy"); // PST`
        Date startDate = calendar.getTime();
        String monthStr = formatter.format(startDate);


        return "" + monthStr + " Mission";
    }
}