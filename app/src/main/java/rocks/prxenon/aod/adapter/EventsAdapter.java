package rocks.prxenon.aod.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.helper.PicassoCache;

import static rocks.prxenon.aod.events.NIAEvents.ispocidlist;


public class EventsAdapter extends BaseAdapter {

    final List<String> eventid;
    final List<String> eventlocation;
    final List<String> eventimage;
    final List<String> eventcountry;
    final List<String> eventregio;
    final List<String> eventtype;
    final List<String> eventcity;
    final List<String> eventmonth;
    final List<String> eventurl;
    final List<Long> eventstringtime;
    private final Activity context;


    public EventsAdapter(Activity context, List<String> eventid, List<String> eventlocation, List<String> eventimage, List<String> eventcountry, List<String> eventregio, List<String> eventtype, List<String> eventcity, List<String> eventmonth, List<Long> eventstringtime, List<String> eventurl) {

        this.context = context;
        this.eventid = eventid;
        this.eventlocation = eventlocation;
        this.eventimage = eventimage;
        this.eventcountry = eventcountry;
        this.eventregio = eventregio;
        this.eventtype = eventtype;
        this.eventcity = eventcity;
        this.eventmonth = eventmonth;
        this.eventstringtime = eventstringtime;
        this.eventurl = eventurl;


    }

    public static String getMonthShortNameEvent(int monthNumber) {
        String monthName = "";

        if (monthNumber >= 0 && monthNumber < 12)
            try {
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.setFirstDayOfWeek(Calendar.MONDAY);
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.MONTH, monthNumber);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM");
                //simpleDateFormat.setCalendar(calendar);
                monthName = simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                if (e != null)
                    e.printStackTrace();
            }
        return monthName;
    }

    @Override
    public int getCount() {
        return eventid.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;


        if (convertView == null) {
            // Log.i("View -->", " is null +" + position);

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_niaevents_adapter, null);

// new CustomTask(myRootfriends, inflater, eventlocation.get(i), eventimage.get(i)).execute((Void[])null);


            final RelativeLayout frameloadevents = gridView.findViewById(R.id.frameloadevents);
            frameloadevents.setVisibility(View.GONE);
            final LinearLayout framecardevents = gridView.findViewById(R.id.framecardevents);
            framecardevents.setVisibility(View.VISIBLE);
            final TextView eventtitle = gridView.findViewById(R.id.title_from_address_earth);
            final TextView country = gridView.findViewById(R.id.title_pledge_earth);

            final TextView monthof = gridView.findViewById(R.id.title_time_label_earth);


            monthof.setText(getMonthShortNameEvent(Integer.parseInt(eventmonth.get(position)) - 1));

            country.setText(eventcountry.get(position));

            final TextView eventarttxt = gridView.findViewById(R.id.title_requests_count_earth);
            eventarttxt.setText(eventtype.get(position));
            final ImageView eventlogov = gridView.findViewById(R.id.title_price_earth);

            final FancyButton bt_haslink = gridView.findViewById(R.id.bt_haslink);


            if (eventurl.get(position).equalsIgnoreCase("no url")) {
                bt_haslink.setVisibility(View.GONE);

            }

            bt_haslink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = eventurl.get(position);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);
                }
            });

            eventtitle.setText(eventcity.get(position));
            if (ispocidlist.contains(eventid.get(position))) {
                eventtitle.setTextColor(context.getResources().getColor(R.color.colorPrimaryYellowDark, context.getTheme()));
            }
            final TextView thedatetext = gridView.findViewById(R.id.title_to_address_earth);

            Date df = new java.util.Date(eventstringtime.get(position));

            SimpleDateFormat dcr = new SimpleDateFormat("MMMM, dd (EEE) yyyy", Locale.US);

            dcr.setTimeZone(TimeZone.getTimeZone("UTC"));
            String thedate = dcr.format(df);


            thedatetext.setText(thedate);

            try {
                PicassoCache.getPicassoInstance(context)
                        .load(eventimage.get(position))
                        .placeholder(R.drawable.full_logo)
                        .error(R.drawable.full_logo)

                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                        //.transform(new CropRoundTransformation())
                        .into(eventlogov);
                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
            } catch (Exception e) {
                //Log.e("Error", e.getMessage());

                e.printStackTrace();
            }

            /* final FoldingCell event_cell = gridView.findViewById(R.id.folding_cell_event);

            event_cell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ViewGroup.LayoutParams params =  event_cell.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    event_cell.setLayoutParams(params);
                    event_cell.requestLayout();
                    event_cell.toggle(false);
                    //weekview_base.requestDisallowInterceptTouchEvent(false);
                    if (!event_cell.isUnfolded()) {
                        final long now = SystemClock.uptimeMillis();
                        final MotionEvent pressEvent = MotionEvent.obtain(now, now, MotionEvent.ACTION_DOWN, event_cell.getWidth() / 2, event_cell.getHeight() / 2, 0);


                        //swipe_layout_community.onTouchEvent(pressEvent);
                    }


                }
            }); */


        } else {
            // Log.i("View -->", " is not null +" + position);
            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.list_niaevents_adapter, null);


            final RelativeLayout frameloadevents = gridView.findViewById(R.id.frameloadevents);
            frameloadevents.setVisibility(View.GONE);
            final LinearLayout framecardevents = gridView.findViewById(R.id.framecardevents);
            framecardevents.setVisibility(View.VISIBLE);
            final TextView eventtitle = gridView.findViewById(R.id.title_from_address_earth);
            final TextView country = gridView.findViewById(R.id.title_pledge_earth);

            country.setText(eventcountry.get(position));

            final FancyButton bt_haslink = gridView.findViewById(R.id.bt_haslink);


            if (eventurl.get(position).equalsIgnoreCase("no url") || eventurl.get(position).equalsIgnoreCase("null")) {
                bt_haslink.setVisibility(View.GONE);
            }
            bt_haslink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = eventurl.get(position);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);
                }
            });
            final TextView monthof = gridView.findViewById(R.id.title_time_label_earth);


            monthof.setText(getMonthShortNameEvent(Integer.parseInt(eventmonth.get(position)) - 1));
            final TextView eventarttxt = gridView.findViewById(R.id.title_requests_count_earth);
            eventarttxt.setText(eventtype.get(position));

            final ImageView eventlogov = gridView.findViewById(R.id.title_price_earth);


            eventtitle.setText(eventcity.get(position));
            if (ispocidlist.contains(eventid.get(position))) {
                eventtitle.setTextColor(context.getResources().getColor(R.color.colorPrimaryYellowDark, context.getTheme()));
            }
            final TextView thedatetext = gridView.findViewById(R.id.title_to_address_earth);

            Date df = new java.util.Date(eventstringtime.get(position));

            SimpleDateFormat dcr = new SimpleDateFormat("MMMM, dd (EEE) yyyy", Locale.US);
            dcr.setTimeZone(TimeZone.getTimeZone("UTC"));
            String thedate = dcr.format(df);

            thedatetext.setText(thedate);
            try {
                PicassoCache.getPicassoInstance(context)
                        .load(eventimage.get(position))
                        .placeholder(R.drawable.full_logo)
                        .error(R.drawable.full_logo)

                        // .memoryPolicy(MemoryPolicy.NO_CACHE )
                        // .networkPolicy(NetworkPolicy.NO_CACHE)
                        //.transform(new CropRoundTransformation())
                        .into(eventlogov);
                // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
            } catch (Exception e) {
                //Log.e("Error", e.getMessage());

                e.printStackTrace();
            }

            /* final FoldingCell event_cell = gridView.findViewById(R.id.folding_cell_event);

             event_cell.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ViewGroup.LayoutParams params =  event_cell.getLayoutParams();
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    event_cell.setLayoutParams(params);
                    event_cell.requestLayout();
                    event_cell.toggle(false);
                    //weekview_base.requestDisallowInterceptTouchEvent(false);
                    if (!event_cell.isUnfolded()) {
                        final long now = SystemClock.uptimeMillis();
                        final MotionEvent pressEvent = MotionEvent.obtain(now, now, MotionEvent.ACTION_DOWN, event_cell.getWidth() / 2, event_cell.getHeight() / 2, 0);


                        //swipe_layout_community.onTouchEvent(pressEvent);
                    }


                }
            }); */

        }


        return gridView;

    }

}