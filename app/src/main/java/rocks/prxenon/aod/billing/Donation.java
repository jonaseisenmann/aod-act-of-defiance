package rocks.prxenon.aod.billing;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.SkuDetails;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.lid.lib.LabelButtonView;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.basics.SaveFriendScan;

import static rocks.prxenon.aod.basics.AODConfig.pref;

public class Donation extends AppCompatActivity implements BillingProcessor.IBillingHandler {
    private static final String ACTIVITY_NUMBER = "activity_num";
    private static final String LOG_TAG = "iabv3";
    private static final String PRODUCT_ID = "aod.supporter.bronze";
    private static final String PRODUCT_ID_SILVER = "aod.supporter.silver";
    private static final String PRODUCT_ID_GOLD = "aod.supporter.gold";
    private static final String SUBSCRIPTION_ID = "aod.supporter.onyx.abo";
    //private static final String SUPPORTERBIOCARD_ID = "aod.supporter.special.biocard2020";
    private static final String SUPPORTERBIOCARD_ID21 = "aod.supporter.special.biocard2021";
    private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt3mm0Av26JaEl0vHv1rucdlg9rBmKniyYMooI0qMubfXPPRd3ShWhyljcBQprI1cXW9wa9+H2ATh8I/4OdwjjGLvTQpJe8GGq5zKHS3sRcaU035vqt+IkeXL2SX7WoStfcjesycVibxWbFn8AXiHIIT7gsUyQfn8PLP68r4eEj6aOYvBBbef9tww7ArwVrzgLNz1tlNoGnZ1dBLkuUKsktKCDBFMH4kzLnkBibdkPMSUlEp2rMQJ6dGb82WS7UbOst4NRMPgNDriX6IDIa51S03fzHrarCHfI6SBcshxomzVsTJfGPuMNHQigcqexmSDXTfRxfjSzW/uuN6u4JkBIwIDAQAB"; // PUT YOUR MERCHANT KEY HERE;
    BillingProcessor bp;
    // put your Google merchant id here (as stated in public profile of your Payments Merchant Center)
    // if filled library will provide protection against Freedom alike Play Market simulators
    private String MERCHANT_ID;
    private boolean readyToPurchase = false;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;

    private Typeface type, proto;
    private TextView mTitleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation);

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);


        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");


        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);


        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_noaction, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText("DONATION");


        MERCHANT_ID = AgentData.getMERVHANTID();
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);

        if (!BillingProcessor.isIabServiceAvailable(this)) {
            showToast("In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16");
        }
        bp = new BillingProcessor(this, LICENSE_KEY, MERCHANT_ID, this);
        bp.initialize();


    }


    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        //showToast("onProductPurchased: " + productId);
        if (productId.equalsIgnoreCase(PRODUCT_ID)) {
            bp.consumePurchase(productId);
        } else if (productId.equalsIgnoreCase(PRODUCT_ID_SILVER)) {
            bp.consumePurchase(productId);
        } else if (productId.equalsIgnoreCase(PRODUCT_ID_GOLD)) {
            bp.consumePurchase(productId);
        } else if (productId.equalsIgnoreCase(SUPPORTERBIOCARD_ID21)) {
            // bp.consumePurchase(productId);
            showToast("THANKS! New Badge added to Profile -> " + productId);
            new SaveFriendScan().redeemBadge(Donation.this, getApplicationContext(), "BIOCARDSUPPORTER2021", "THANKS FOR YOUR SUPPORT");

        } else if (productId.equalsIgnoreCase(SUBSCRIPTION_ID)) {
            // bp.consumePurchase(productId);
        }


        updateTextViews();
    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        showToast("onBillingError: " + errorCode);
    }

    @Override
    public void onBillingInitialized() {
        showToast("onBillingInitialized");
        readyToPurchase = true;
        updateTextViews();
    }

    @Override
    public void onPurchaseHistoryRestored() {
        showToast("onPurchaseHistoryRestored");
        for (String sku : bp.listOwnedProducts())
            Log.d(LOG_TAG, "Owned Managed Product: " + sku);
        for (String sku : bp.listOwnedSubscriptions())
            Log.d(LOG_TAG, "Owned Subscription: " + sku);
        updateTextViews();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateTextViews();
    }

    @Override
    public void onDestroy() {
        if (bp != null)
            bp.release();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }

    private void updateTextViews() {
        String[] extratext = AgentData.getsku_extra().toString().replace("[", "").replace("]", "").split(", ");

        // BRONZE
        TextView text = findViewById(R.id.productIdTextView);
        SkuDetails sku = bp.getPurchaseListingDetails(PRODUCT_ID);
        LabelButtonView lbronze = findViewById(R.id.purchaseButtonbronze);
        lbronze.setLabelText(extratext[0]);
        if (!AgentData.getsku().toString().contains(PRODUCT_ID)) {
            LinearLayout linbronze = findViewById(R.id.supporter_bronze);
            linbronze.setVisibility(View.GONE);
        }
        if (sku != null) {
            text.setText(sku.description + " (" + sku.priceText + ")");
        }

        text = findViewById(R.id.don_bronz);
        if (sku != null) {

            String[] separated = sku.title.split("\\(AOD");
            text.setText(separated[0]);
        }

        // SILVER
        TextView textsilver = findViewById(R.id.productIdTextViewSilver);
        SkuDetails skusilver = bp.getPurchaseListingDetails(PRODUCT_ID_SILVER);
        LabelButtonView lsilver = findViewById(R.id.purchaseButtonsilver);
        lsilver.setLabelText(extratext[1]);
        if (!AgentData.getsku().toString().contains(PRODUCT_ID_SILVER)) {
            LinearLayout linsilver = findViewById(R.id.supporter_silver);
            linsilver.setVisibility(View.GONE);
        }
        if (skusilver != null) {
            textsilver.setText(skusilver.description + " (" + skusilver.priceText + ")");
        }

        textsilver = findViewById(R.id.don_silver);
        if (skusilver != null) {

            String[] separatedsilver = skusilver.title.split("\\(AOD");
            textsilver.setText(separatedsilver[0]);
        }

        // GOLD
        TextView textgold = findViewById(R.id.productIdTextViewGold);
        SkuDetails skugold = bp.getPurchaseListingDetails(PRODUCT_ID_GOLD);

        LabelButtonView lgold = findViewById(R.id.purchaseButtongold);
        lgold.setLabelText(extratext[2]);
        //Log.i("SKU-->", " " + AgentData.getsku().toString());
        if (!AgentData.getsku().toString().contains(PRODUCT_ID_GOLD)) {

            LinearLayout lingold = findViewById(R.id.supporter_gold);
            lingold.setVisibility(View.GONE);
        }
        if (skugold != null) {
            textgold.setText(skugold.description + " (" + skugold.priceText + ")");
        }

        textgold = findViewById(R.id.don_gold);
        if (skugold != null) {

            String[] separatedgold = skugold.title.split("\\(AOD");
            textgold.setText(separatedgold[0]);
        }

        // ABO ONYX
        LabelButtonView laboonyx = findViewById(R.id.purchaseButtonaboonyx);
        laboonyx.setLabelText(extratext[3]);

        TextView textaboonyx = findViewById(R.id.productIdTextViewaboonyx);
        SkuDetails skuaboonyx = bp.getSubscriptionListingDetails(SUBSCRIPTION_ID);
        Log.i("SKU-->", " " + AgentData.getsku().toString());
        if (!AgentData.getsku().toString().contains(SUBSCRIPTION_ID)) {
            Log.i("TRUE-->", " " + AgentData.getsku().toString());
            LinearLayout linaboonyx = findViewById(R.id.subscriber_onyx);
            linaboonyx.setVisibility(View.GONE);
        }
        if (skuaboonyx != null) {
            textaboonyx.setText(skuaboonyx.description + " (" + skuaboonyx.priceText + ")");
        }

        textaboonyx = findViewById(R.id.don_aboonyx);
        if (skuaboonyx != null) {

            String[] separatedaboonyx = skuaboonyx.title.split("\\(AOD");
            if (bp.isSubscribed(SUBSCRIPTION_ID)) {
                LabelButtonView subaboonyxbt = findViewById(R.id.purchaseButtonaboonyx);
                subaboonyxbt.setEnabled(false);
                subaboonyxbt.setLabelText("ABO");
                subaboonyxbt.setLabelVisual(false);
                textaboonyx.setText("You subscribed to " + separatedaboonyx[0]);
            } else {

                textaboonyx.setText(separatedaboonyx[0]);
            }
        }


        // SPECIAL BIOCARD
        LabelButtonView lspecial_biocard = findViewById(R.id.purchaseButtonspecial_biocard);
        lspecial_biocard.setLabelText(extratext[4]);

        TextView textspecial_biocard = findViewById(R.id.productIdTextViewspecial_biocard);
        SkuDetails skuspecial_biocard = bp.getPurchaseListingDetails(SUPPORTERBIOCARD_ID21);
        Log.i("SKU-->", " " + AgentData.getsku().toString());
        if (!AgentData.getsku().toString().contains(SUPPORTERBIOCARD_ID21)) {
            Log.i("TRUE-->", " " + AgentData.getsku().toString());
            LinearLayout linspecial_biocard = findViewById(R.id.spezial_biocard);
            linspecial_biocard.setVisibility(View.GONE);
        }
        if (skuspecial_biocard != null) {
            textspecial_biocard.setText(skuspecial_biocard.description + " (" + skuspecial_biocard.priceText + ")");
        }

        TextView textspecial_biocard2 = findViewById(R.id.don_special_biocard);
        if (skuspecial_biocard != null) {

            String[] separatedspecial_biocard = skuspecial_biocard.title.split("\\(AOD");
            textspecial_biocard2.setText(separatedspecial_biocard[0]);
            for (String sku_owned : bp.listOwnedProducts()) {

                if (sku_owned.equalsIgnoreCase(SUPPORTERBIOCARD_ID21)) {
                    // bp.consumePurchase(sku_owned);

                    lspecial_biocard.setLabelText("THANKS");
                    lspecial_biocard.setEnabled(false);
                    lspecial_biocard.setBackgroundColor(Color.GRAY);
                    textspecial_biocard.setText(skuspecial_biocard.description + " (Thanks for your support)");
                }
                Log.d(LOG_TAG, "Owned Managed Product: " + sku_owned);
            }

        }


        //  text.setText(String.format("%s is%s purchased", PRODUCT_ID, bp.isPurchased(PRODUCT_ID) ? "" : " not"));
        //text = (TextView)findViewById(R.id.subscriptionIdTextView);
        // text.setText(String.format("%s is%s subscribed", SUBSCRIPTION_ID, bp.isSubscribed(SUBSCRIPTION_ID) ? "" : " not"));
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void onClick(View v) {
        if (!readyToPurchase) {
            // showToast("Billing not initialized.");
            return;
        }
        switch (v.getId()) {
            case R.id.purchaseButtonbronze:
                bp.purchase(this, PRODUCT_ID);
                break;
            case R.id.purchaseButtonsilver:
                bp.purchase(this, PRODUCT_ID_SILVER);
                break;
            case R.id.purchaseButtongold:
                bp.purchase(this, PRODUCT_ID_GOLD);
                break;
            case R.id.productDetailsButton:
                SkuDetails sku = bp.getPurchaseListingDetails(PRODUCT_ID);
                showToast(sku != null ? sku.toString() : "Failed to load SKU details");
                break;
            case R.id.productDetailsButtongold:
                SkuDetails skugoldx = bp.getPurchaseListingDetails(PRODUCT_ID_GOLD);

                showToast(skugoldx != null ? skugoldx.toString() : "Failed to load SKU details");
                break;
            case R.id.purchaseButtonspecial_biocard:
                bp.purchase(this, SUPPORTERBIOCARD_ID21);
                break;
            case R.id.purchaseButtonaboonyx:
                bp.subscribe(this, SUBSCRIPTION_ID);
                break;

            case R.id.productDetailsButtonaboonyx:
                SkuDetails subs = bp.getSubscriptionListingDetails(SUBSCRIPTION_ID);
                showToast(subs != null ? subs.toString() : "Failed to load subscription details");
                break;

            default:
                break;
        }
    }


    public void newbadge(boolean resultis, final Activity act, Context ctx, String passcodeid) {

        if (resultis) {
            Log.i("SCANE TRUE --->", "" + resultis);
            AlertDialog.Builder confirmdialog = new AlertDialog.Builder(act, R.style.CustomDialog);

            // builder.setMessage("Text not finished...");
            LayoutInflater inflater = act.getLayoutInflater();
            View dialog = inflater.inflate(R.layout.dialog_redeemed_passcode, null);
            confirmdialog.setView(dialog);

            TextView agentname = dialog.findViewById(R.id.agentname);
            agentname.setText("Thanks for your Support! You unlocked a new badge.");

            confirmdialog.setPositiveButton("Wuhuu", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    act.recreate();
                    //new SaveFriendScan().redeemPasscode(ProfileMain.this, getApplicationContext(), snapshot.child("aodcode").getValue().toString(), snapshot.child("passcode").getValue().toString());


                }
            });


            confirmdialog.setCancelable(true);

            if (!isFinishing()) {
                confirmdialog.create().show();

            }
            //act.recreate();
        } else {
            Log.i("SCANE FAILED --->", "" + resultis);
            act.recreate();
        }

    }

}