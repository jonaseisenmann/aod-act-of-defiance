package rocks.prxenon.aod.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

public class DeleteTokenService extends IntentService {
    public static final String TAG = DeleteTokenService.class.getSimpleName();

    public DeleteTokenService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {

            FirebaseInstanceId.getInstance().deleteInstanceId();

            // Clear current saved token
            //    saveTokenToPrefs("");

            // Check for success of empty token
            //  String tokenCheck = getTokenFromPrefs();
            //   Log.d(TAG, "Token deleted. Proof: " + tokenCheck);

            // Now manually call onTokenRefresh()
            Log.d(TAG, "TOKEN DELETED");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}