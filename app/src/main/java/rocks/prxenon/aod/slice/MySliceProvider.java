package rocks.prxenon.aod.slice;

/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.core.graphics.drawable.IconCompat;
import androidx.slice.Slice;
import androidx.slice.SliceProvider;
import androidx.slice.builders.ListBuilder;
import androidx.slice.builders.SliceAction;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.pages.AODMain;

import static rocks.prxenon.aod.pages.AODMain.getMonthText;
import static rocks.prxenon.aod.slice.MyBroadcastReceiver.ACTION_CHANGE_TEMP;
import static rocks.prxenon.aod.slice.MyBroadcastReceiver.EXTRA_TEMP_VALUE;

public class MySliceProvider extends SliceProvider {
    private static int sReqCode = 0;
    private Context context;

    public static Uri getUri(Context context, String path) {
        return new Uri.Builder()
                .scheme(ContentResolver.SCHEME_CONTENT)
                .authority(context.getPackageName())
                .appendPath(path)
                .build();
    }

    @Override
    public boolean onCreateSliceProvider() {
        context = getContext();
        return true;
    }

    @Override
    public Slice onBindSlice(Uri sliceUri) {
        final String path = sliceUri.getPath();
        switch (path) {

            case "/monthmission":
                return createTestSlice(sliceUri);
        }
        return null;
    }

    private Slice createTestSlice(Uri sliceUri) {
        // Define the actions used in this slice

        // Construct our parent builder
        // Construct the parent.

        Intent intent = new Intent(getContext(), AODMain.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), sliceUri.hashCode(),
                intent, 0);
        @SuppressLint("RestrictedApi") SliceAction openTempActivity = new SliceAction(pendingIntent,
                IconCompat.createWithResource(context, R.drawable.full_logo).toIcon(),
                "AOD Missions");
        ListBuilder listBuilder = new ListBuilder(getContext(), sliceUri, ListBuilder.INFINITY)
                .setAccentColor(0xff0F9D58) // Specify color for tinting icons.
                .setHeader( // Create the header and add to slice.
                        new ListBuilder.HeaderBuilder()
                                .setTitle("AOD Missions")

                                .setSubtitle(getMonthText(getContext()))
                                .setSummary(getMonthText(getContext()))

                ).addRow(new ListBuilder.RowBuilder() // Add a row.
                        .setPrimaryAction(
                                openTempActivity) // A slice always needs a SliceAction.
                        .setTitle("MONTH")

                        .setSubtitle("GOLD | PLATIN | ONYX")
                        .setContentDescription(getMonthText(getContext()))
                        .addEndItem(IconCompat.createWithResource(getContext(), R.drawable.full_logo),
                                ListBuilder.SMALL_IMAGE)
                ).addRow(new ListBuilder.RowBuilder() // Add a row.
                        .setPrimaryAction(
                                openTempActivity) // A slice always needs a SliceAction.
                        .setTitle("WEEK")

                        .setSubtitle("GOLD | PLATIN | ONYX")
                        .setContentDescription(getMonthText(getContext()))
                        .addEndItem(IconCompat.createWithResource(getContext(), R.drawable.full_logo),
                                ListBuilder.SMALL_IMAGE)
                ); // Add more rows if needed...

        // Add the actions to appear at the end of the row


        // Build the slice
        return listBuilder.build();
    }

    private PendingIntent getChangeTempIntent(int value) {
        Intent intent = new Intent(ACTION_CHANGE_TEMP);
        intent.setClass(context, MyBroadcastReceiver.class);
        intent.putExtra(EXTRA_TEMP_VALUE, value);
        return PendingIntent.getBroadcast(getContext(), sReqCode++, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }
}