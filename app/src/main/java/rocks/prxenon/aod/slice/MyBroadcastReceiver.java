package rocks.prxenon.aod.slice;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static rocks.prxenon.aod.pages.AODMain.sTemperature;
import static rocks.prxenon.aod.pages.AODMain.updateTemperature;

public class MyBroadcastReceiver extends BroadcastReceiver {

    public static String ACTION_CHANGE_TEMP = "ACTION_CHANGE_TEMP";
    public static String EXTRA_TEMP_VALUE = "EXTRA_TEMP_VALUE";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (ACTION_CHANGE_TEMP.equals(action) && intent.getExtras() != null) {
            int newValue = intent.getExtras().getInt(EXTRA_TEMP_VALUE, sTemperature);
            updateTemperature(context, newValue);
        }
    }

}