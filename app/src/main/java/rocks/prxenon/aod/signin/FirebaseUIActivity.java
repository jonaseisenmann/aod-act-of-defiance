package rocks.prxenon.aod.signin;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.muddzdev.styleabletoast.StyleableToast;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;
import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.events.NIAEvents;
import rocks.prxenon.aod.pages.AODMain;
import rocks.prxenon.aod.pages.profile.ProfileMain;
import rocks.prxenon.aod.pages.pvp.PvPCreate;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mdbroot;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;

public class FirebaseUIActivity extends AppCompatActivity implements
        View.OnClickListener {

    private static final int RC_SIGN_IN = 123;
    private static final int RC_SIGN_IN_REAUTH = 125;
    private FancyButton delaccount;
    private FancyButton startlogin, importfromscreen;

    private MaterialEditText agentname_txt;
    private MaterialCardView card_view, card_view2;
    private TextView mail_txt, headline_txt;
    private RadioRealButtonGroup factiongroup;
    private ImageButton next_button, next_button2;
    private FrameLayout progressBarHolder;
    private RelativeLayout content_layout;
    private Bundle bundle;
    private Typeface type;
    private ProgressDialog pDialog;
    private SharedPreferences globalsp;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //pref = context.getSharedPreferences("AppPref", MODE_PRIVATE);
        setContentView(R.layout.activity_main);
        new AODConfig();
        new AODConfig("string", getApplicationContext());
        startlogin = findViewById(R.id.startlogin);
        startlogin.setOnClickListener(this);
        delaccount = findViewById(R.id.delgoogle);
        delaccount.setOnClickListener(this);
        agentname_txt = findViewById(R.id.agentname);
        headline_txt = findViewById(R.id.headline_txt);
        importfromscreen = findViewById(R.id.importfromscreen);
        importfromscreen.setOnClickListener(this);
        mail_txt = findViewById(R.id.mail_txt);
        card_view = findViewById(R.id.card_view);
        card_view2 = findViewById(R.id.card_view2);
        next_button = findViewById(R.id.next_button);
        next_button.setOnClickListener(this);
        next_button2 = findViewById(R.id.next_button2);
        next_button2.setOnClickListener(this);
        factiongroup = findViewById(R.id.groupradio);
        progressBarHolder = findViewById(R.id.progressBarHolder);
        content_layout = findViewById(R.id.content_layout);
        content_layout.setVisibility(View.GONE);
        progressBarHolder.setVisibility(View.VISIBLE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        factiongroup.setOnPositionChangedListener(new RadioRealButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(RadioRealButton button, int currentPosition, int lastPosition) {
                if (currentPosition == 0) {
                    factiongroup.setSelectorColor(getColor(R.color.colorENL));
                } else if (currentPosition == 1) {
                    factiongroup.setSelectorColor(getColor(R.color.colorGray));
                } else {
                    factiongroup.setSelectorColor(getColor(R.color.colorRES));
                }
            }
        });
        user = FirebaseAuth.getInstance().getCurrentUser();

        if (bundle == null) {
            bundle = getIntent().getExtras();
        }
        if (user == null) {
            Log.w("userisnull", "true");
            headline_txt.setVisibility(View.GONE);
            progressBarHolder.setVisibility(View.GONE);
            content_layout.setVisibility(View.VISIBLE);

            ShortcutManager shortcutManager = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
                shortcutManager = getSystemService(ShortcutManager.class);
                if (shortcutManager.getDynamicShortcuts().size() > 0) {

                    try {
                        if (shortcutManager.getManifestShortcuts().get(0) != null) {
                            ShortcutInfo shortcut = shortcutManager.getManifestShortcuts().get(0);

                            if (shortcutManager.getManifestShortcuts().get(1) != null) {
                                ShortcutInfo shortcut2 = shortcutManager.getManifestShortcuts().get(1);
                                shortcutManager.removeDynamicShortcuts(Arrays.asList(shortcut.getId(), shortcut2.getId()));
                            } else {
                                shortcutManager.removeDynamicShortcuts(Arrays.asList(shortcut.getId()));
                            }
                        }
                    } catch (IndexOutOfBoundsException e) {

                    }


                }


            }


        } else {
            startlogin.setVisibility(View.GONE);
            delaccount.setVisibility(View.VISIBLE);
            headline_txt.setVisibility(View.VISIBLE);
            Log.w("useris -->", user.getUid());
            for (UserInfo profile : user.getProviderData()) {
                // Id of the provider (ex: google.com)

                if (profile.getProviderId().equalsIgnoreCase("google.com")) {

                    Log.i("GoogleData: ", "" + profile.getUid() + " " + profile.getProviderId());
                }


            }

            if (getIntent().getAction() != null) {
                if (getIntent().getData() != null) {
                    if (getIntent().getData().getPath().contains("/share/")) {
                        Uri uri = getIntent().getData();
                        Log.d("TAG->>>>>>>>>>>>>", "status code2 " + uri);
                        bundle = new Bundle();
                        bundle.putBoolean("login", true);
                        userexist();
                    } else if (getIntent().getData().getPath().contains("/pvp/")) {
                        Uri uri = getIntent().getData();
                        Log.d("TAG->>>>>>>>>>>>>", "status code3 " + uri);
                        bundle = new Bundle();
                        bundle.putBoolean("login", true);
                        userexist();
                    } else {
                        userexist();
                    }
                } else {
                    userexist();
                }
            } else {

                userexist();
            }
        }


    }

    public void createSignInIntent() {
        // [START auth_fui_create_intent]
        // Choose authentication providers
        startlogin.setVisibility(View.GONE);
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                // new AuthUI.IdpConfig.EmailBuilder().build(),

                new AuthUI.IdpConfig.GoogleBuilder().build());


        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setAlwaysShowSignInMethodScreen(false)
                        .setIsSmartLockEnabled(false, false)
                        .setTosAndPrivacyPolicyUrls("https://prxenon.de/privacy.php", "https://prxenon.de/privacy.php")
                        .setLogo(R.drawable.full_logo)
                        .build(),
                RC_SIGN_IN);
        // [END auth_fui_create_intent]
    }

    // [START auth_fui_result]
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in

                user = FirebaseAuth.getInstance().getCurrentUser();


                if (user != null) {
                    headline_txt.setVisibility(View.VISIBLE);
                    createagent(user.getProviderData().get(1).getUid(), user, Objects.requireNonNull(user.getProviderData().get(1).getPhotoUrl().toString()));
                }


                findViewById(R.id.delgoogle).setVisibility(View.VISIBLE);

                // ...
            } else {
                startlogin.setVisibility(View.VISIBLE);
                headline_txt.setVisibility(View.GONE);
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        } else if (requestCode == RC_SIGN_IN_REAUTH) {


            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in

                user = FirebaseAuth.getInstance().getCurrentUser();


                if (user != null) {
                    final Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put("/profileimage", user.getProviderData().get(1).getPhotoUrl().toString());
                    Log.i("NEW IMAGE ----> ", user.getProviderData().get(1).getPhotoUrl().toString());
                    mDatabase.child(user.getUid()).updateChildren(childUpdates);


                    headline_txt.setVisibility(View.VISIBLE);
                    pref.edit().putString("token", response.getIdpToken()).apply();
                    pref.edit().putString("providerid", user.getProviderData().get(1).getProviderId()).apply();
                    // Log.i("TOKEN -->", response.getIdpToken());
                    gotologinnow();
                       /* if (pDialog != null) {
                            if (pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                        } */


                }


                //  findViewById(R.id.delgoogle).setVisibility(View.VISIBLE);

                // ...
            } else {
                startlogin.setVisibility(View.VISIBLE);
                headline_txt.setVisibility(View.GONE);
                //TODO relogin

                // Log.i("ERROR -->", response.getIdpToken());
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }

        } else if (requestCode == 1005 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);


            cursor.close();
            // ImageView imageView = (ImageView) findViewById(R.id.imgView);
            // imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
            recognizeText(image);

            // Bitmap bm=((BitmapDrawable)imageView.getDrawable()).getBitmap();
        }
    }
    // [END auth_fui_result]

    private void recognizeText(FirebaseVisionImage image) {

        // [START get_detector_default]
        FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance()
                .getOnDeviceTextRecognizer();
        // [END get_detector_default]

        // [START run_detector]
        Task<FirebaseVisionText> result =
                detector.processImage(image)
                        .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                            @Override
                            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                                // Task completed successfully
                                // [START_EXCLUDE]
                                // [START get_text]
                                boolean found = false;
                                for (FirebaseVisionText.TextBlock block : firebaseVisionText.getTextBlocks()) {

                                    Rect boundingBox = block.getBoundingBox();
                                    Point[] cornerPoints = block.getCornerPoints();
                                    String text = block.getText().trim();
                                    Log.w("BLOCK-->", text);

                                    Log.w("Box2-> ", "t: " + block.getBoundingBox().top + " l:" + block.getBoundingBox().left + " r:" + block.getBoundingBox().right + " b:" + block.getBoundingBox().bottom);

                                    if (block.getBoundingBox().top >= 177 && block.getBoundingBox().top <= 182 && (block.getBoundingBox().left >= 284 && block.getBoundingBox().left <= 285)) {
                                        found = true;

                                        agentname_txt.setText(block.getLines().get(0).getText());
                                    } else if (block.getText().contains("L ")) {
                                        String[] getname = block.getLines().get(0).getText().split("L ");
                                        found = true;
                                        agentname_txt.setText(getname[0].replace(getname[0].substring(getname[0].length() - 1), ""));

                                    }

                                    for (FirebaseVisionText.Line line : block.getLines()) {
                                        // ...

                                        String linetext = line.getText();
                                        Log.w("LINE-->", linetext);


                                        for (FirebaseVisionText.Element element : line.getElements()) {
                                            // ...


                                            String lineword = element.getText();
                                            Log.w("Box-> ", "t: " + line.getBoundingBox().top + " l:" + line.getBoundingBox().left + " r:" + line.getBoundingBox().right + " b:" + line.getBoundingBox().bottom);

                                            Log.w("Box-> ", "t: " + line.getCornerPoints().toString());
                                            Log.w("Word-->", lineword);
                                        }
                                    }
                                }

                                if (!found) {
                                    agentname_txt.setError(getResources().getString(R.string.reg_noagentname_found));
                                }
                                // [END get_text]
                                // [END_EXCLUDE]
                            }
                        })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {

                                        Bundle params = new Bundle();
                                        params.putBoolean("status", false);
                                        params.putString("methode", "Google");

                                        mFirebaseAnalytics.logEvent("login", params);


                                        // Task failed with an exception
                                        // ...
                                    }
                                });
        // [END run_detector]
    }

    public void createagent(String gid, final FirebaseUser cuser, final String aprofilimage) {
        //final Map<String, String> agentdetails = new HashMap<String, String>();
        final Map<String, Object> childUpdates = new HashMap<>();

        Log.w("RESULTS -> ", "gid: " + gid + "user: " + user.getUid() + " profil " + aprofilimage);
        mDatabase.child(cuser.getUid()).child("googleid").setValue(gid).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                    // IF hasmissions
                    childUpdates.put("/accountcreated", System.currentTimeMillis() / 1000);
                    childUpdates.put("/realname", cuser.getDisplayName());
                    childUpdates.put("/trickster", "green");
                    childUpdates.put("/privacy", 0);
                    childUpdates.put("/profileimage", aprofilimage);
                    childUpdates.put("/email", cuser.getEmail());
                    childUpdates.put("/agb/privacy", true);
                    childUpdates.put("/agb/debug", true);

                    mDatabase.child(cuser.getUid()).updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                //finish();

                                Bundle params = new Bundle();
                                params.putBoolean("status", true);
                                params.putString("methode", "Google");

                                mFirebaseAnalytics.logEvent("login", params);
                                userexist();

                            } else if (task.isCanceled()) {
                                Bundle params = new Bundle();
                                params.putBoolean("status", false);
                                params.putString("methode", "Google");

                                mFirebaseAnalytics.logEvent("login", params);
                                Log.w("ERROR ", this.getClass().getSimpleName() + " 405 Login abgebrochen");
                                StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_loginerror), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(Exception e) {
                            Bundle params = new Bundle();
                            params.putBoolean("status", false);
                            params.putString("methode", "Google");

                            mFirebaseAnalytics.logEvent("login", params);
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 406 Login error " + e.getMessage());
                            StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_sometningwrong), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            //signOut();
                        }
                    });

                }
            }


        });
    }

    //
    public void userexist() {

        if (bundle == null || bundle.isEmpty()) {
            mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.hasChild("agentname") && snapshot.hasChild("faction")) {
                        pref.edit().putBoolean("userexist", Boolean.parseBoolean(snapshot.child("agb").child("privacy").getValue().toString())).apply();
                        Log.w("AGENT AND FACTION FOUND", " " + true + " a: " + snapshot.child("agentname").getValue().toString());
                        AVLoadingIndicatorView avi = findViewById(R.id.avi);
                        avi.smoothToShow();
                        progressBarHolder.setVisibility(View.VISIBLE);
                        content_layout.setVisibility(View.GONE);
                        agentname_txt.setVisibility(View.GONE);
                        card_view.setVisibility(View.GONE);
                        headline_txt.setText(getResources().getString(R.string.string_pleaswait));
                        TextView loading_status = findViewById(R.id.status_txt);
                        loading_status.setText(getResources().getString(R.string.string_connevting));
                        delaccount.setVisibility(View.GONE);
                        new AgentData("agentname", snapshot.child("agentname").getValue().toString());
                        new AgentData("faction", snapshot.child("faction").getValue().toString());

                        if (snapshot.child("trickster").exists()) {
                            new AgentData("trickster", snapshot.child("trickster").getValue().toString());
                        } else {
                            new AgentData("trickster", "green");
                        }

                        if (snapshot.child("filter").exists()) {
                            new AgentData("filter", true);
                            if (snapshot.child("filter").child("country").exists()) {
                                new AgentData("filtercountry", true);
                                new AgentData("filterdatacountry", (List<String>) snapshot.child("filter").child("country").getValue());
                            } else {
                                new AgentData("filtercountry", false);
                            }

                            if (snapshot.child("filter").child("regio").exists()) {
                                new AgentData("filterregio", true);
                                new AgentData("filterdataregio", (List<String>) snapshot.child("filter").child("regio").getValue());
                            } else {
                                new AgentData("filterregio", false);
                            }
                            if (snapshot.child("filter").child("city").exists()) {
                                new AgentData("filtercity", true);
                                new AgentData("filterdatacity", (List<String>) snapshot.child("filter").child("city").getValue());
                            } else {
                                new AgentData("filtercity", false);
                            }
                            if (snapshot.child("filter").child("type").exists()) {
                                new AgentData("filtertype", true);
                                new AgentData("filterdatatype", (List<String>) snapshot.child("filter").child("type").getValue());
                            } else {
                                new AgentData("filtertype", false);
                            }

                        } else {
                            new AgentData("filter", false);
                            new AgentData("filtercountry", false);
                            new AgentData("filterregio", false);
                            new AgentData("filtercity", false);
                            new AgentData("filtertype", false);
                        }

                        FirebaseMessaging.getInstance().subscribeToTopic("aod_news");
                        globalsp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        //   LinearLayout a = new LinearLayout(this);


                        if (!globalsp.getBoolean("notifyset", false)) {
                            globalsp.edit().putBoolean("reminderweek_monday", true).apply();
                            globalsp.edit().putBoolean("reminderweek_sunday", true).apply();
                            globalsp.edit().putBoolean("event_channel", false).apply();
                            globalsp.edit().putBoolean("aod_news", true).apply();
                            globalsp.edit().putBoolean("notifyset", true).apply();

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("event_channel");
                            FirebaseMessaging.getInstance().subscribeToTopic("reminderweek_monday");
                            FirebaseMessaging.getInstance().subscribeToTopic("reminderweek_sunday");

                        }
                        if (getIntent().getAction() != null) {
                            if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.pages.profile.ProfileMain.openqrfromAction")) {

                                Intent i = new Intent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.openqrfromAction").setClass(getApplicationContext(), ProfileMain.class));
                                // i.putExtra("login", true);
                                startActivity(i);
                                finish();

                            } else if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.pages.profile.ProfileMain.scanqrfromAction")) {
                                Intent i = new Intent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.scanqrfromAction").setClass(getApplicationContext(), ProfileMain.class));
                                // i.putExtra("login", true);
                                startActivity(i);
                                finish();
                            } else if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.events.profile.NIAEvents.startfromhome")) {
                                Intent i = new Intent(getApplicationContext(), NIAEvents.class);
                                // i.putExtra("login", true);
                                startActivity(i);
                                finish();
                            } else {

                                Intent i = new Intent(getApplicationContext(), AODMain.class);
                                // i.putExtra("login", true);
                                startActivity(i);
                                finish();

                            }
                        } else {
                            Intent i = new Intent(getApplicationContext(), AODMain.class);
                            // i.putExtra("login", true);
                            startActivity(i);
                            finish();
                        }
                    } else {
                        if (snapshot.exists()) {
                            progressBarHolder.setVisibility(View.GONE);
                            content_layout.setVisibility(View.VISIBLE);
                            agentname_txt.setVisibility(View.VISIBLE);
                            mail_txt.setVisibility(View.VISIBLE);
                            importfromscreen.setVisibility(View.GONE);
                            type = ResourcesCompat.getFont(getApplicationContext(), R.font.main);
                            agentname_txt.setAccentTypeface(type);
                            card_view.setRadius(30);
                            card_view.setElevation(5);
                            card_view.setCardElevation(5);
                            card_view.setCardBackgroundColor((getColor(R.color.colorDarker)));

                            card_view.setVisibility(View.VISIBLE);
                            card_view2.setRadius(30);
                            card_view2.setElevation(5);
                            card_view2.setCardElevation(5);
                            card_view2.setCardBackgroundColor((getColor(R.color.colorDarker)));
                            mail_txt.setText(user.getEmail());
                            // StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_loginerror)+" CODE 46887", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                        } else {
                            // TODO
                            StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_loginerror) + "  CODE 43211", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_sometningwrong), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    Log.i("Error DB -->", this.getClass().getSimpleName() + " 407 " + databaseError.getMessage());
                }
            });
        } else {
            new AgentData("trickster", "green");


            List<AuthUI.IdpConfig> providers = Arrays.asList(
                    // new AuthUI.IdpConfig.EmailBuilder().build(),

                    new AuthUI.IdpConfig.GoogleBuilder().build());


            headline_txt.setText(getResources().getString(R.string.string_loginproof));

            // Create and launch sign-in intent
            startActivityForResult(
                    AuthUI.getInstance()

                            .createSignInIntentBuilder()

                            .setAvailableProviders(providers)
                            .setTosAndPrivacyPolicyUrls("https://prxenon.de/privacy.php", "https://prxenon.de/privacy.php")
                            .setLogo(R.drawable.full_logo)
                            .setAlwaysShowSignInMethodScreen(false)
                            .setIsSmartLockEnabled(false, false)

                            .build(),
                    RC_SIGN_IN_REAUTH);

        }
    }


    private void gotologinnow() {

        if (bundle.getBoolean("login")) {

            headline_txt.setText(getResources().getString(R.string.string_pleaswait));

            mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.hasChild("agentname") && snapshot.hasChild("faction")) {
                        new AgentData("agentname", snapshot.child("agentname").getValue().toString());
                        new AgentData("faction", snapshot.child("faction").getValue().toString());
                        if (snapshot.child("trickster").exists()) {
                            new AgentData("trickster", snapshot.child("trickster").getValue().toString());
                        } else {
                            new AgentData("trickster", "green");
                        }

                        if (snapshot.child("filter").exists()) {
                            new AgentData("filter", true);
                            if (snapshot.child("filter").child("country").exists()) {
                                new AgentData("filtercountry", true);
                                new AgentData("filterdatacountry", (List<String>) snapshot.child("filter").child("country").getValue());
                            } else {
                                new AgentData("filtercountry", false);
                            }

                            if (snapshot.child("filter").child("regio").exists()) {
                                new AgentData("filterregio", true);
                                new AgentData("filterdataregio", (List<String>) snapshot.child("filter").child("regio").getValue());
                            } else {
                                new AgentData("filterregio", false);
                            }
                            if (snapshot.child("filter").child("city").exists()) {
                                new AgentData("filtercity", true);
                                new AgentData("filterdatacity", (List<String>) snapshot.child("filter").child("city").getValue());
                            } else {
                                new AgentData("filtercity", false);
                            }
                            if (snapshot.child("filter").child("type").exists()) {
                                new AgentData("filtertype", true);
                                new AgentData("filterdatatype", (List<String>) snapshot.child("filter").child("type").getValue());
                            } else {
                                new AgentData("filtertype", false);
                            }

                        } else {
                            new AgentData("filter", false);
                            new AgentData("filtercountry", false);
                            new AgentData("filterregio", false);
                            new AgentData("filtercity", false);
                            new AgentData("filtertype", false);
                        }

                        FirebaseMessaging.getInstance().subscribeToTopic("aod_news");

                        globalsp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        //   LinearLayout a = new LinearLayout(this);


                        if (!globalsp.getBoolean("notifyset", false)) {
                            globalsp.edit().putBoolean("reminderweek_monday", true).apply();
                            globalsp.edit().putBoolean("reminderweek_sunday", true).apply();
                            globalsp.edit().putBoolean("event_channel", false).apply();
                            globalsp.edit().putBoolean("aod_news", true).apply();
                            globalsp.edit().putBoolean("notifyset", true).apply();

                            FirebaseMessaging.getInstance().unsubscribeFromTopic("event_channel");
                            FirebaseMessaging.getInstance().subscribeToTopic("reminderweek_monday");
                            FirebaseMessaging.getInstance().subscribeToTopic("reminderweek_sunday");

                        }
                        if (!globalsp.getBoolean("notifysetpvp", false)) {
                            globalsp.edit().putBoolean("agent_channel", true).apply();
                            globalsp.edit().putBoolean("notifysetpvp", true).apply();

                            FirebaseMessaging.getInstance().subscribeToTopic("privat_" + user.getUid());


                        }
                        Bundle params = new Bundle();
                        params.putBoolean("status", true);
                        params.putString("methode", "Default");

                        mFirebaseAnalytics.logEvent("login", params);
                        // StyleableToast.makeText(getApplicationContext(), "Login erfolgreich", Toast.LENGTH_LONG, R.style.defaulttoastb).show();
                        if (getIntent().getAction() != null) {
                            if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.pages.profile.ProfileMain.openqrfromAction")) {
                                finishAndRemoveTask();
                                Intent i = new Intent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.openqrfromAction").setClass(getApplicationContext(), ProfileMain.class));
                                // i.putExtra("login", true);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();

                            } else if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.pages.profile.ProfileMain.scanqrfromAction")) {
                                finishAndRemoveTask();
                                Intent i = new Intent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.scanqrfromAction").setClass(getApplicationContext(), ProfileMain.class));
                                // i.putExtra("login", true);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            } else if (getIntent().getData().getPath().contains("/share/")) {
                                finishAndRemoveTask();
                                String agentcodeget = getIntent().getData().toString();
                                String[] agentcodedata = agentcodeget.split("/share/");
                                Log.d("TAG55 ->>>>>>>>>>>>>", "status code2 " + agentcodedata[1]);
                                Intent i = new Intent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.savefromlink").setClass(getApplicationContext(), ProfileMain.class));
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.putExtra("agentcode", agentcodedata[1]);
                                // i.putExtra("login", true);
                                startActivity(i);
                                finish();


                            } else if (getIntent().getData().getPath().contains("/pvp/")) {
                                finishAndRemoveTask();
                                String pvpcode = getIntent().getData().toString();
                                String[] pvpcodedata = pvpcode.split("/pvp/AODs_");
                                Log.d("TAG55 ->>>>>>>>>>>>>", "status code3 " + pvpcodedata[1]);
                               /* Intent i = new Intent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.savefromlink").setClass(getApplicationContext(), ProfileMain.class));
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.putExtra("agentcode", pvpcodedata[1]);
                                // i.putExtra("login", true);
                                startActivity(i);
                                finish(); */
                                Intent i = new Intent(getApplicationContext(), PvPCreate.class);
                                Intent openPvP = new Intent(FirebaseUIActivity.this, PvPCreate.class);
                                openPvP.putExtra("accepted", true);
                                openPvP.putExtra("fromlink", true);
                                openPvP.putExtra("pvpkey", pvpcodedata[1]);

                                startActivity(openPvP);
                                finish();
                                // i.putExtra("login", true);
                                // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);

                                //StyleableToast.makeText(getApplicationContext(), "soon", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                            } else {
                                finishAndRemoveTask();
                                Intent i = new Intent(getApplicationContext(), AODMain.class);
                                // i.putExtra("login", true);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            }
                        } else {
                            //  finishAffinity();
                            Intent i = new Intent(getApplicationContext(), AODMain.class);
                            // i.putExtra("login", true);
                            // i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_sometningwrong), Toast.LENGTH_LONG, R.style.defaulttoast).show();

                }
            });


        } else {
            mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.hasChild("agentname") && snapshot.hasChild("faction")) {
                        pref.edit().putBoolean("userexist", Boolean.parseBoolean(snapshot.child("agb").child("privacy").getValue().toString())).apply();
                        Log.w("AGENT AND FACTION FOUND", " " + true + " a: " + snapshot.child("agentname").getValue().toString());
                        AVLoadingIndicatorView avi = findViewById(R.id.avi);
                        avi.smoothToShow();
                        progressBarHolder.setVisibility(View.VISIBLE);
                        content_layout.setVisibility(View.GONE);
                        agentname_txt.setVisibility(View.GONE);
                        card_view.setVisibility(View.GONE);
                        headline_txt.setText(getResources().getString(R.string.string_pleaswait));
                        TextView loading_status = findViewById(R.id.status_txt);
                        loading_status.setText(getResources().getString(R.string.string_connevting));
                        delaccount.setVisibility(View.GONE);
                        new AgentData("agentname", snapshot.child("agentname").getValue().toString());
                        new AgentData("faction", snapshot.child("faction").getValue().toString());
                        Bundle params = new Bundle();
                        params.putBoolean("status", true);
                        params.putString("methode", "new Login");

                        mFirebaseAnalytics.logEvent("login", params);
                        if (getIntent().getAction() != null) {
                            if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.pages.profile.ProfileMain.openqrfromAction")) {

                                Intent i = new Intent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.openqrfromAction").setClass(getApplicationContext(), ProfileMain.class));
                                // i.putExtra("login", true);
                                startActivity(i);
                                finish();

                            } else if (getIntent().getAction().equalsIgnoreCase("rocks.prxenon.aod.pages.profile.ProfileMain.scanqrfromAction")) {
                                Intent i = new Intent(new Intent("rocks.prxenon.aod.pages.profile.ProfileMain.scanqrfromAction").setClass(getApplicationContext(), ProfileMain.class));
                                // i.putExtra("login", true);
                                startActivity(i);
                                finish();
                            } else {
                                Intent i = new Intent(getApplicationContext(), AODMain.class);
                                // i.putExtra("login", true);
                                startActivity(i);
                                finish();
                            }
                        } else {
                            Intent i = new Intent(getApplicationContext(), AODMain.class);
                            // i.putExtra("login", true);
                            startActivity(i);
                            finish();
                        }
                    } else {
                        if (snapshot.exists()) {
                            progressBarHolder.setVisibility(View.GONE);
                            content_layout.setVisibility(View.VISIBLE);
                            agentname_txt.setVisibility(View.VISIBLE);
                            mail_txt.setVisibility(View.VISIBLE);
                            // importfromscreen.setVisibility(View.VISIBLE);
                            type = ResourcesCompat.getFont(getApplicationContext(), R.font.main);
                            agentname_txt.setAccentTypeface(type);
                            card_view.setRadius(30);
                            card_view.setElevation(5);
                            card_view.setCardElevation(5);
                            card_view.setCardBackgroundColor((getColor(R.color.colorDarker)));

                            card_view.setVisibility(View.VISIBLE);
                            card_view2.setRadius(30);
                            card_view2.setElevation(5);
                            card_view2.setCardElevation(5);
                            card_view2.setCardBackgroundColor((getColor(R.color.colorDarker)));
                            mail_txt.setText(user.getEmail());
                        } else {
                            // TODO
                            StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_loginerror), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_sometningwrong), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    Log.i("Error DB -->", this.getClass().getSimpleName() + " 407 " + databaseError.getMessage());
                }
            });
        }
    }

    public void signOut() {
        // [START auth_fui_signout]
        pref.edit().remove("userexist").apply();
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        finish();
                    }
                });
        // [END auth_fui_signout]
    }

    public void delete() {
        // [START auth_fui_delete]
        AuthUI.getInstance()
                .delete(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        finish();
                    }
                });
        // [END auth_fui_delete]
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startlogin:
                // mExplosionField.clear();
                createSignInIntent();
                break;
            case R.id.delgoogle:
                // mExplosionField.clear();
                signOut();
                break;
            case R.id.importfromscreen:
                // mExplosionField.clear();
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 1005);
                break;
            case R.id.next_button:
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {

                }
                // mExplosionField.clear();
                final Animation animSlideLeft = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
                final Animation animSlideRight = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
                if (Objects.requireNonNull(agentname_txt.getText()).toString().trim().length() < 3 || agentname_txt.getText().toString().trim().contains(" ")) {
                    agentname_txt.setError(getResources().getString(R.string.string_bad_agent));
                    delaccount.setVisibility(View.VISIBLE);
                } else {

                    final Map<String, Object> agentupdate = new HashMap<>();
                    agentupdate.put("/agentname", agentname_txt.getText().toString().trim());
                    importfromscreen.setVisibility(View.GONE);
                    mail_txt.setVisibility(View.GONE);
                    delaccount.setVisibility(View.GONE);
                    // content_layout.setVisibility(View.GONE);
                    progressBarHolder.setVisibility(View.VISIBLE);

                    mdbroot.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshotexist) {
                            if (snapshotexist.hasChild(agentname_txt.getText().toString().trim())) {
                                content_layout.setVisibility(View.VISIBLE);
                                progressBarHolder.setVisibility(View.GONE);

                                //importfromscreen.setVisibility(View.VISIBLE);
                                mail_txt.setVisibility(View.VISIBLE);
                                // delaccount.setVisibility(View.VISIBLE);
                                agentname_txt.setError(getResources().getString(R.string.string_agent_exist));
                            } else {
                                mDatabase.child(user.getUid()).updateChildren(agentupdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //finish();
                                            new AgentData("agentname", agentname_txt.getText().toString().trim());

                                            card_view.startAnimation(animSlideLeft);
                                            card_view2.startAnimation(animSlideRight);
                                            card_view2.setVisibility(View.VISIBLE);
                                            headline_txt.startAnimation(animSlideLeft);
                                            headline_txt.startAnimation(animSlideRight);
                                            headline_txt.setText(getResources().getString(R.string.string_reg_setfaction));
                                            content_layout.setVisibility(View.VISIBLE);
                                            progressBarHolder.setVisibility(View.GONE);
                                            agentname_txt.setError(null);
                                        } else if (task.isCanceled()) {
                                            content_layout.setVisibility(View.VISIBLE);
                                            progressBarHolder.setVisibility(View.GONE);
                                            Log.w("ERROR ", this.getClass().getSimpleName() + " 505 Login abgebrochen");
                                            //  importfromscreen.setVisibility(View.VISIBLE);
                                            mail_txt.setVisibility(View.VISIBLE);
                                            delaccount.setVisibility(View.VISIBLE);
                                        }
                                    }

                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(Exception e) {
                                        content_layout.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 506 Login error " + e.getMessage());
                                        //  importfromscreen.setVisibility(View.VISIBLE);
                                        mail_txt.setVisibility(View.VISIBLE);
                                        delaccount.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            StyleableToast.makeText(getApplicationContext(), "Error 526", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            Log.i("Error DB -->", this.getClass().getSimpleName() + " 407 " + databaseError.getMessage());
                        }
                    });


                    // agentname_txt.setFloatingLabelText("A");
                }
                animSlideLeft.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        importfromscreen.setVisibility(View.GONE);
                        mail_txt.setVisibility(View.GONE);
                        delaccount.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        card_view.setVisibility(View.GONE);
                        factiongroup.setPosition(1, true);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                animSlideRight.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {


                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;
            case R.id.next_button2:
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {

                }
                // mExplosionField.clear();
                final Animation animSlideLeft2 = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
                final Animation animSlideRight2 = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
                final String faction;
                final Map<String, Object> agentupdate2 = new HashMap<>();
                if (factiongroup.getPosition() == 0) {
                    faction = "ENLIGHTENED";
                } else if (factiongroup.getPosition() == 1) {
                    faction = "ANONYM";
                } else {

                    faction = "RESISTANCE";
                }

                agentupdate2.put("/faction", faction);
                //content_layout.setVisibility(View.GONE);
                progressBarHolder.setVisibility(View.VISIBLE);
                mDatabase.child(user.getUid()).updateChildren(agentupdate2).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            //finish();
                            new AgentData("faction", faction);
                            card_view2.startAnimation(animSlideLeft2);

                            headline_txt.startAnimation(animSlideLeft2);
                            headline_txt.startAnimation(animSlideRight2);
                            headline_txt.setText(getResources().getString(R.string.string_pleaswait));

                            mdbroot.child(AgentData.getagentname()).setValue(user.getUid()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        //finish();


                                        userexist();
                                        // delaccount.setVisibility(View.VISIBLE);
                                        // content_layout.setVisibility(View.VISIBLE);
                                        // progressBarHolder.setVisibility(View.GONE);

                                    } else if (task.isCanceled()) {
                                        content_layout.setVisibility(View.VISIBLE);
                                        progressBarHolder.setVisibility(View.GONE);
                                        Log.w("ERROR ", this.getClass().getSimpleName() + " 1055 Login abgebrochen");

                                    }
                                }

                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(Exception e) {
                                    content_layout.setVisibility(View.VISIBLE);
                                    progressBarHolder.setVisibility(View.GONE);
                                    Log.w("ERROR ", this.getClass().getSimpleName() + " 1056 Login error " + e.getMessage());

                                }
                            });


                        } else if (task.isCanceled()) {
                            content_layout.setVisibility(View.VISIBLE);
                            progressBarHolder.setVisibility(View.GONE);
                            Log.w("ERROR ", this.getClass().getSimpleName() + " 605 Login abgebrochen");

                        }
                    }

                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        content_layout.setVisibility(View.VISIBLE);
                        progressBarHolder.setVisibility(View.GONE);
                        Log.w("ERROR ", this.getClass().getSimpleName() + " 606 Login error " + e.getMessage());

                    }
                });

                animSlideLeft2.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        card_view2.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                animSlideRight2.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {


                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                break;
        }
    }
}