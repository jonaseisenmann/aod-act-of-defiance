package rocks.prxenon.aod.introfragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.Nullable;

import io.github.dreierf.materialintroscreen.SlideFragment;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;

public class AODSlide extends SlideFragment {
    private CheckBox checkBox, checkBox2;
    private TextView textvield;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_game_slide, container, false);
        checkBox = view.findViewById(R.id.checkBox);
        checkBox2 = view.findViewById(R.id.checkBox2);


        textvield = view.findViewById(R.id.textfield);

        String largeTextString = new AODConfig(getContext()).getStringFromRawRes(R.raw.thegame);
        if (largeTextString != null) {  //null check is optional
            textvield.setText(largeTextString);
        } else {
            //something goes wrong
        }
        return view;
    }

    @Override
    public int backgroundColor() {
        return R.color.colorPrimaryDark;
    }

    @Override
    public int buttonsColor() {
        return R.color.colorAccent;
    }

    @Override
    public boolean canMoveFurther() {
        return true;
    }


}