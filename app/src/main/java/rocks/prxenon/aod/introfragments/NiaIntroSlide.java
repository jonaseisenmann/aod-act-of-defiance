package rocks.prxenon.aod.introfragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.Nullable;

import io.github.dreierf.materialintroscreen.SlideFragment;
import rocks.prxenon.aod.R;


public class NiaIntroSlide extends SlideFragment {
    private CheckBox checkBox;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_nia_slide, container, false);
        //checkBox = (CheckBox) view.findViewById(R.id.checkBox);

        return view;
    }

    @Override
    public int backgroundColor() {
        return R.color.colorPrimaryDark;
    }

    @SuppressWarnings("unused")
    public void hideBackButton() {


    }

    @Override
    public int buttonsColor() {
        return R.color.colorAccent;
    }


    @Override
    public String cantMoveFurtherErrorMessage() {
        return getString(R.string.error_message);
    }
}