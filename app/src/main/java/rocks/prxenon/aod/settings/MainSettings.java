package rocks.prxenon.aod.settings;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NavUtils;


import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.muddzdev.styleabletoast.StyleableToast;

import java.util.Arrays;
import java.util.List;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.pages.subs.LibsUsed;
import rocks.prxenon.aod.pages.subs.Privacy;
import rocks.prxenon.aod.signin.FirebaseUIActivity;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mDatabaserunnung;
import static rocks.prxenon.aod.basics.AODConfig.mdbroot;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;
import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class MainSettings extends AppCompatPreferenceActivity {

    public static final int REQUEST_CODE = 892;
    public static final int REQUEST_CODE_CAL = 893;
    private static final int RC_SIGN_IN = 123;
    private static PreferenceScreen mainprevview;
    private static Preference privacy_status, notify_opens_action, screen_size, plugins_text, export_ebl, calibration, calibration_inventory, account_delete, app_version, account_email, debug_text, trickster_status, used_libs, developer_name, privacy_terms, tg_news, tg_en, tg_de, tg_ru, tg_es;
    private static SharedPreferences prefs;
    private static ListPreference ingress_appart, ingress_prime_timer;
    private static CheckBoxPreference workaround;
    private static Context ctx;
    private static Activity atx;
    private static PreferenceGroup basics_settings;
    private static SharedPreferences globalsp;
    private static SwitchPreference screen_save, update_missions;
    private static PackageInfo packageInfo;
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static final Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        int index;
        boolean redacted_allowed = false;
        boolean min_ingressversion = false;

        @Override
        public boolean onPreferenceChange(Preference preference, final Object value) {


            if (isAppInstalled("com.nianticproject.ingress")) {
                Log.i("INGRESS V-->", " " + packageInfo.versionCode);


                if (packageInfo.versionCode >= Integer.parseInt(remoteconfig.getString("min_ingress"))) {
                    min_ingressversion = true;
                }

            }

            if (remoteconfig.getBoolean("redacted")) {
                redacted_allowed = remoteconfig.getBoolean("redacted");
            }

            //Log.i("DATA needed -->", "redacted: " + redacted_allowed + " mini: " + min_ingressversion);
            Log.w("CHANGE -->", preference.getKey() + " " + value.toString());
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.

                if (preference.getKey().equalsIgnoreCase("ingress_appart")) {
                    ListPreference listPreference = (ListPreference) preference;
                    index = listPreference.findIndexOfValue(stringValue);

                    preference.setSummary(

                            index >= 0
                                    ? listPreference.getEntries()[index]
                                    : null);

                    // Log.w("VALUE --->", Integer.parseInt(value.toString()) + " ." + value.toString());
                    calibration.setEnabled(false);
                    calibration.setSummary(("calibartion not longer required for " + listPreference.getEntries()[index]));

                    calibration_inventory.setEnabled(true);
                    prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
                    prefs.edit().putInt("prever", Integer.parseInt(value.toString())).apply();
                    screen_save.setChecked(false);
                    screen_save.setEnabled(false);
                    basics_settings.removePreference(debug_text);
                    preference.setSummary(listPreference.getEntries()[index] + " v" + packageInfo.versionName);
                    basics_settings.addPreference(update_missions);
                    update_missions.setEnabled(true);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    if (preference.getKey().equalsIgnoreCase("ingress_prime_timer")) {
                        ListPreference listPreference = (ListPreference) preference;
                        index = listPreference.findIndexOfValue(stringValue);

                        preference.setSummary(

                                index >= 0
                                        ? listPreference.getEntries()[index]
                                        : null);

                        //  Log.w("VALUE --->", Integer.parseInt(value.toString()) + " ." + value.toString());


                        prefs.edit().putInt("timer", Integer.parseInt(value.toString())).apply();

                    }
                }


            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                if (preference.getKey().equalsIgnoreCase("workaround")) {
                    if (stringValue.equalsIgnoreCase("true")) {
                        preference.setSummary(atx.getResources().getString(R.string.settings_workaround_man_sum));
                        basics_settings.addPreference(workaround);
                        basics_settings.removePreference(ingress_appart);
                        calibration.setEnabled(true);
                        calibration.setSummary("");
                        //prefs.edit().remove("calint").apply();
                        prefs.edit().remove("prever").apply();
                        prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
                    } else {
                        if (prefs.getInt("calint", 0) == 0 && (Build.VERSION.RELEASE == "6.0" || Build.VERSION.RELEASE == "6" || Build.VERSION.RELEASE == "6.0.1" || Build.VERSION.SDK_INT <= Build.VERSION_CODES.M)) {
                            basics_settings.addPreference(workaround);
                            prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), false).apply();
                            preference.setSummary(atx.getResources().getString(R.string.settings_forandroidsix));

                        }

                        basics_settings.addPreference(ingress_appart);
                    }
                } else {
                    preference.setSummary(stringValue);
                }
            }
            return true;
        }
    };
    private Typeface type, proto;
    private TextView mTitleTextView;

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    private static void bindPreferenceSummaryToValue2(CheckBoxPreference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getBoolean(preference.getKey(), false));
    }

    private static void bindPreferenceSummaryToValue3(SwitchPreference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getBoolean(preference.getKey(), false));
    }


    private static void bindPreferenceSummaryToValue4(SwitchPreference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getBoolean(preference.getKey(), false));
    }

    private static void deleteall() {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(ctx, android.R.style.Theme_Material_Dialog_Alert);
        builder.setTitle(R.string.settings_del_title)
                .setMessage(R.string.settings_del_desc)
                .setPositiveButton(R.string.settings_oknext, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        deletereal();

                        // StyleableToast.makeText(ctx, "soon", Toast.LENGTH_LONG, R.style.defaulttoastb).show();

                    }
                })
                .setNegativeButton(R.string.cancel_txt, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing

                    }
                })

                .show();
    }

    private static void deletereal() {

        final ProgressDialog progress = new ProgressDialog(atx);
        progress.setTitle("Löschen");
        progress.setMessage("löschen wird vorbereitet...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
        FirebaseMessaging.getInstance().unsubscribeFromTopic("event_channel");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("reminderweek_monday");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("reminderweek_sunday");
        FirebaseMessaging.getInstance().unsubscribeFromTopic("privat_" + user.getUid());
        FirebaseMessaging.getInstance().subscribeToTopic("aod_news");
        globalsp.edit().putBoolean("notifyset", false).apply();
        globalsp.edit().putBoolean("notifysetpvp", false).apply();
        globalsp.edit().remove("reminderweek_monday").apply();
        globalsp.edit().remove("reminderweek_sunday").apply();
        globalsp.edit().remove("agent_channel").apply();
        globalsp.edit().remove("aod_news").apply();
        globalsp.edit().remove("event_channel").apply();


        ShortcutManager shortcutManager = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N_MR1) {
            shortcutManager = atx.getSystemService(ShortcutManager.class);
            if (shortcutManager.getDynamicShortcuts().size() > 0) {
                try {
                    if (shortcutManager.getManifestShortcuts().get(0) != null) {
                        ShortcutInfo shortcut = shortcutManager.getManifestShortcuts().get(0);

                        if (shortcutManager.getManifestShortcuts().get(1) != null) {
                            ShortcutInfo shortcut2 = shortcutManager.getManifestShortcuts().get(1);
                            shortcutManager.removeDynamicShortcuts(Arrays.asList(shortcut.getId(), shortcut2.getId()));
                        } else {
                            shortcutManager.removeDynamicShortcuts(Arrays.asList(shortcut.getId()));
                        }
                    }
                } catch (IndexOutOfBoundsException e) {
                    // list.add( index, new Object() );
                }


            }


        }
// To dismiss the dialog
        // progress.dismiss();

        // START LÖSCHEN USER DATA

        // START DELETE ROOT
        mdbroot.child(AgentData.getagentname()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progress.setMessage("Agentname wird freigegeben");
                if (task.isSuccessful()) {
                    //finish();
                    // START DELETE Missions
                    mDatabase.child(user.getUid()).child("missions").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapmissions) {

                            if (snapmissions.hasChildren()) {
                                if (snapmissions.child(snapmissions.getKey()).hasChild("month")) {
                                    for (DataSnapshot missionsmonth : snapmissions.child(snapmissions.getKey()).getChildren()) {
                                        //  Log.i("timestamp", historyis.getValue().toString());
                                        if (missionsmonth.hasChildren()) {
                                            // GET MONTHS
                                            for (DataSnapshot missionsmonthdetails : missionsmonth.getChildren()) {
                                                //  Log.i("timestamp", historyis.getValue().toString());
                                                if (missionsmonthdetails.hasChildren()) {
                                                    // DELETE RUNNING
                                                    mDatabaserunnung.child(snapmissions.getKey()).child("month").child(missionsmonthdetails.getKey()).child(user.getUid()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            progress.setMessage("Missions werden gelöscht");
                                                            if (task.isSuccessful()) {
                                                                //finish();
                                                                // START DELETE Missions


                                                            } else if (task.isCanceled()) {
                                                                progress.dismiss();
                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            progress.dismiss();
                                                            //signOut();
                                                        }
                                                    });

                                                }
                                            }
                                        }
                                    }
                                }

                                if (snapmissions.child(snapmissions.getKey()).hasChild("week")) {
                                    for (DataSnapshot missionsweek : snapmissions.child(snapmissions.getKey()).getChildren()) {
                                        //  Log.i("timestamp", historyis.getValue().toString());
                                        if (missionsweek.hasChildren()) {
                                            // GET MONTHS
                                            for (DataSnapshot missionsweekdetails : missionsweek.getChildren()) {
                                                //  Log.i("timestamp", historyis.getValue().toString());
                                                if (missionsweekdetails.hasChildren()) {
                                                    // DELETE RUNNING
                                                    mDatabaserunnung.child(snapmissions.getKey()).child("week").child(missionsweekdetails.getKey()).child(user.getUid()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            progress.setMessage("Missions werden gelöscht");
                                                            if (task.isSuccessful()) {
                                                                //finish();
                                                                // START DELETE Missions


                                                            } else if (task.isCanceled()) {
                                                                progress.dismiss();
                                                            }
                                                        }

                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(Exception e) {
                                                            progress.dismiss();
                                                            //signOut();
                                                        }
                                                    });

                                                }
                                            }
                                        }
                                    }
                                }


                                mDatabase.child(user.getUid()).child("agentcode").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshotagentcode) {
                                        if (snapshotagentcode.exists()) {
                                            mDatabaserunnung.child(snapshotagentcode.getValue().toString()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    progress.setMessage("Agent Code wird gelöscht");
                                                    if (task.isSuccessful()) {
                                                        //finish();
                                                        // START DELETE Agent
                                                        mDatabaserunnung.child(user.getUid()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                progress.setMessage("Agent Daten werden gelöscht");
                                                                if (task.isSuccessful()) {
                                                                    //finish();
                                                                    List<AuthUI.IdpConfig> providers = Arrays.asList(
                                                                            // new AuthUI.IdpConfig.EmailBuilder().build(),

                                                                            new AuthUI.IdpConfig.GoogleBuilder().build());

                                                                    progress.dismiss();
                                                                    // Create and launch sign-in intent
                                                                    atx.startActivityForResult(
                                                                            AuthUI.getInstance()
                                                                                    .createSignInIntentBuilder()
                                                                                    .setAvailableProviders(providers)
                                                                                    .setTosAndPrivacyPolicyUrls("https://prxenon.de/privacy.php", "https://prxenon.de/privacy.php")
                                                                                    .setLogo(R.drawable.full_logo)
                                                                                    .build(),
                                                                            RC_SIGN_IN);


                                                                } else if (task.isCanceled()) {
                                                                    progress.dismiss();
                                                                }
                                                            }

                                                        }).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(Exception e) {
                                                                progress.dismiss();
                                                                //signOut();
                                                            }
                                                        });


                                                    } else if (task.isCanceled()) {
                                                        progress.dismiss();
                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    progress.dismiss();
                                                    //signOut();
                                                }
                                            });
                                        } else {
                                            mDatabaserunnung.child(user.getUid()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    progress.setMessage("Agent Daten werden gelöscht");
                                                    if (task.isSuccessful()) {
                                                        //finish();
                                                        List<AuthUI.IdpConfig> providers = Arrays.asList(
                                                                // new AuthUI.IdpConfig.EmailBuilder().build(),

                                                                new AuthUI.IdpConfig.GoogleBuilder().build());

                                                        progress.dismiss();
                                                        // Create and launch sign-in intent
                                                        atx.startActivityForResult(
                                                                AuthUI.getInstance()
                                                                        .createSignInIntentBuilder()
                                                                        .setAvailableProviders(providers)
                                                                        .setTosAndPrivacyPolicyUrls("https://prxenon.de/privacy.php", "https://prxenon.de/privacy.php")
                                                                        .setLogo(R.drawable.full_logo)
                                                                        .build(),
                                                                RC_SIGN_IN);


                                                    } else if (task.isCanceled()) {
                                                        progress.dismiss();
                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    progress.dismiss();
                                                    //signOut();
                                                }
                                            });
                                        }

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        progress.dismiss();
                                    }
                                });


                            } else {
                                // IF NOTHING TO DELETE TODO NEXT STEP
                                mDatabase.child(user.getUid()).child("agentcode").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot snapshotagentcode) {
                                        if (snapshotagentcode.exists()) {
                                            mDatabaserunnung.child(snapshotagentcode.getValue().toString()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    progress.setMessage("Agent Code wird gelöscht");
                                                    if (task.isSuccessful()) {
                                                        //finish();
                                                        // START DELETE Agent
                                                        mDatabase.child(user.getUid()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                progress.setMessage("Agent Daten werden gelöscht");
                                                                if (task.isSuccessful()) {
                                                                    //finish();
                                                                    List<AuthUI.IdpConfig> providers = Arrays.asList(
                                                                            // new AuthUI.IdpConfig.EmailBuilder().build(),

                                                                            new AuthUI.IdpConfig.GoogleBuilder().build());

                                                                    progress.dismiss();
                                                                    // Create and launch sign-in intent
                                                                    atx.startActivityForResult(
                                                                            AuthUI.getInstance()
                                                                                    .createSignInIntentBuilder()
                                                                                    .setAlwaysShowSignInMethodScreen(false)
                                                                                    .setIsSmartLockEnabled(false, false)
                                                                                    .setAvailableProviders(providers)
                                                                                    .setTosAndPrivacyPolicyUrls("https://prxenon.de/privacy.php", "https://prxenon.de/privacy.php")
                                                                                    .setLogo(R.drawable.full_logo)
                                                                                    .build(),
                                                                            RC_SIGN_IN);


                                                                } else if (task.isCanceled()) {
                                                                    progress.dismiss();
                                                                }
                                                            }

                                                        }).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(Exception e) {
                                                                progress.dismiss();
                                                                //signOut();
                                                            }
                                                        });


                                                    } else if (task.isCanceled()) {
                                                        progress.dismiss();
                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    progress.dismiss();
                                                    //signOut();
                                                }
                                            });
                                        } else {
                                            mDatabase.child(user.getUid()).setValue(null).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    progress.setMessage("Agent Daten werden gelöscht");
                                                    if (task.isSuccessful()) {
                                                        //finish();
                                                        List<AuthUI.IdpConfig> providers = Arrays.asList(
                                                                // new AuthUI.IdpConfig.EmailBuilder().build(),

                                                                new AuthUI.IdpConfig.GoogleBuilder().build());

                                                        progress.dismiss();
                                                        // Create and launch sign-in intent
                                                        atx.startActivityForResult(
                                                                AuthUI.getInstance()
                                                                        .createSignInIntentBuilder()
                                                                        .setAvailableProviders(providers)
                                                                        .setAlwaysShowSignInMethodScreen(false)
                                                                        .setIsSmartLockEnabled(false, false)

                                                                        .setTosAndPrivacyPolicyUrls("https://prxenon.de/privacy.php", "https://prxenon.de/privacy.php")
                                                                        .setLogo(R.drawable.full_logo)
                                                                        .build(),
                                                                RC_SIGN_IN);


                                                    } else if (task.isCanceled()) {
                                                        progress.dismiss();
                                                    }
                                                }

                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(Exception e) {
                                                    progress.dismiss();
                                                    //signOut();
                                                }
                                            });
                                        }


                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        progress.dismiss();
                                    }
                                });

                            }


                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            progress.dismiss();

                        }
                    });


                } else if (task.isCanceled()) {
                    progress.dismiss();
                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                progress.dismiss();
                //signOut();
            }
        });


    }

    public static boolean isStoragePermissionGranted(Context contextis, Activity activityis) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (contextis.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("MAINSETTINGS", "Permission is granted");
                return true;
            } else {

                Log.v("MAINSETTINGS", "Permission is revoked");
                ActivityCompat.requestPermissions(atx, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("MAINSETTINGS", "Permission is granted");
            return true;
        }
    }

    private static boolean isAppInstalled(String packageName) {
        PackageManager pm = atx.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return pm.getApplicationInfo(packageName, 0).enabled;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        String ebltoken;

        if (extras != null) {

            if (extras.getString("token_ebl").length() > 25) {
                ebltoken = extras.getString("token_ebl");
                pref.edit().putString("ebl_token", ebltoken).apply();
                new AgentData("ebl_token", ebltoken);
                //startexportebl(ebltoken);

            } else {
                ebltoken = null;
                new AgentData("ebl_token", "null");
                pref.edit().putString("ebl_toke", "null").apply();
                Log.i("TokenEBL -->", "is null");
            }
            // and get whatever type user account id is
        }
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");
        calibration = findPreference("ingress_callibration");
        calibration_inventory = findPreference("calibration_inventory");
        app_version = findPreference("app_version");
        account_email = findPreference("account_email");
        debug_text = findPreference("debug_key");
        trickster_status = findPreference("trickster_status");
        used_libs = findPreference("libs_btn");
        privacy_terms = findPreference("privacy_terms");
        developer_name = findPreference("developer_name");
        account_delete = findPreference("account_delete");
        screen_save = (SwitchPreference) findPreference("save_screen");
        update_missions = (SwitchPreference) findPreference("update_missions");
        prefs = getSharedPreferences("AppPref", MODE_PRIVATE);
        export_ebl = findPreference("export_ebl");
        plugins_text = findPreference("plugins_text");
        globalsp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        ctx = getApplicationContext();
        atx = MainSettings.this;
        if (isAppInstalled("com.nianticproject.ingress")) {
            try {
                packageInfo = ctx.getPackageManager().getPackageInfo("com.nianticproject.ingress", 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }

        setupActionBar();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Log.w("RESULT2 --->", requestCode + "");
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {

                String requiredValue = data.getStringExtra("key");
                //Log.w("RESULT --->", requiredValue);
                if (data.getIntExtra("claint", 0) != 0) {
                    prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
                    prefs.edit().putInt("calint", data.getIntExtra("claint", 0)).apply();
                    calibration.setSummary(getResources().getString(R.string.settings_cal_suc));
                    debug_text.setSummary(prefs.getString("debugger", "" + R.string.settings_debug_sum));
                    int testconvert = 0;
                    String toconvert = data.getStringExtra("testvalue").replaceAll("\\s+", "");
                    toconvert = toconvert.replace('o', '0');
                    toconvert = toconvert.replace('O', '0');
                    toconvert = toconvert.replace('l', '1');
                    toconvert = toconvert.replace('i', '1');
                    toconvert = toconvert.replace('I', '1');
                    toconvert = toconvert.replace('L', '1');
                    toconvert = toconvert.replace(",", "");
                    toconvert = toconvert.replace(".", "");

                    try {
                        // testconvert = Integer.parseInt(toconvert.replaceAll("[\\s+]", ""));
                        //Log.w("Test --->", "" + toconvert);
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                    StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.settings_cal_suc_value) + " " + toconvert, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                }
            }

            if (requestCode == REQUEST_CODE_CAL && resultCode == RESULT_OK) {

                String requiredValue = data.getStringExtra("key");
                //Log.w("RESULT --->", requiredValue);
                if (data.getIntExtra("claint_invent", 0) != 0) {
                    prefs.edit().putBoolean(remoteconfig.getString("pref_calibration_invent"), true).apply();
                    prefs.edit().putInt("calint_invent", data.getIntExtra("claint_invent", 0)).apply();
                    calibration_inventory.setSummary(getResources().getString(R.string.settings_cal_suc));
                    //debug_text.setSummary(prefs.getString("debugger", "" + R.string.settings_debug_sum));
                    int testconvert = 0;
                    String toconvert = data.getStringExtra("testvalue").replaceAll("\\s+", "");
                    toconvert = toconvert.replace('o', '0');
                    toconvert = toconvert.replace('O', '0');
                    toconvert = toconvert.replace('l', '1');
                    toconvert = toconvert.replace('i', '1');
                    toconvert = toconvert.replace('I', '1');
                    toconvert = toconvert.replace('L', '1');
                    toconvert = toconvert.replace(",", "");
                    toconvert = toconvert.replace(".", "");

                    try {
                        // testconvert = Integer.parseInt(toconvert.replaceAll("[\\s+]", ""));
                        //Log.w("Test --->", "" + toconvert);
                    } catch (NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                    }
                    StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.settings_cal_suc_value) + " " + toconvert, Toast.LENGTH_LONG, R.style.defaulttoast).show();
                }
            }
            if (requestCode == 654) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Settings.canDrawOverlays(this)) {
                        // startscreenshot();
                        Intent intent = new Intent(atx,
                                ScreenCalibration.class);
                        atx.startActivityForResult(intent, REQUEST_CODE);

                    }
                }
            }

            if (requestCode == RC_SIGN_IN) {
                IdpResponse response = IdpResponse.fromResultIntent(data);

                if (resultCode == RESULT_OK) {
                    // Successfully signed in

                    user = FirebaseAuth.getInstance().getCurrentUser();


                    if (user != null) {
                        AuthUI.getInstance()
                                .delete(atx)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //Log.d("DELETED", "User account deleted.");

                                            pref.edit().clear().apply();

                                            Intent i = new Intent(atx, FirebaseUIActivity.class);
                                            i.putExtra("login", false);
                                            atx.startActivity(i);
                                            atx.finish();
                                        } else {

                                            //Log.d("ERROR", "User account not deleted.");

                                        }
                                    }
                                });


                    }


                    // ...
                } else {

                }
            }
        } catch (Exception ex) {
            Toast.makeText(MainSettings.this, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_settings, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(getResources().getString(R.string.settings_txt));
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(25);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!super.onMenuItemSelected(featureId, item)) {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    @Override
    public void onBackPressed() {
        // REMOVE QR FROM FIREBASE

        // startActivity(new Intent(MainSettings.this, AODMain.class));
        //finish();
        // new API("qrcloser", this).tempscanCoderemove(user.getEmail(),theqrstring, nameofagent);

        super.onBackPressed();


    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class MyPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);
            ctx = getContext();
            atx = getActivity();
            new AODConfig("init", ctx);


            prefs = getActivity().getSharedPreferences("AppPref", MODE_PRIVATE);
            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            debug_text = findPreference("debug_key");
            calibration = findPreference("ingress_callibration");
            calibration_inventory = findPreference("ingress_inventory_callibration");
            account_email = findPreference("account_email");
            trickster_status = findPreference("trickster_status");
            trickster_status.setSummary(AgentData.getTrickster());
            privacy_terms = findPreference("privacy_terms");
            account_email.setSummary(user.getEmail());
            used_libs = findPreference("libs_btn");
            developer_name = findPreference("developer_name");
            ingress_appart = (ListPreference) findPreference("ingress_appart");
            ingress_prime_timer = (ListPreference) findPreference("ingress_prime_timer");
            privacy_status = findPreference("privacy_status");
            notify_opens_action = findPreference("notify_opens_action");
            screen_size = findPreference("app_size");
            screen_save = (SwitchPreference) findPreference("save_screen");
            update_missions = (SwitchPreference) findPreference("update_missions");
            export_ebl = findPreference("export_ebl");
            plugins_text = findPreference("plugins_text");
            float density = getResources().getDisplayMetrics().density;

            String screensizeset = "other | " + density;
            if (density >= 0.75) {
                screensizeset = "LDPI | " + density;
            }
            if (density >= 1.0) {
                screensizeset = "MDPI | " + density;
            }
            if (density >= 1.5) {
                screensizeset = "HDPI | " + density;
            }
            if (density >= 2.0) {
                screensizeset = "XHDPI | " + density;
            }
            if (density >= 3.0) {
                screensizeset = "XXHDPI | " + density;
            }
            if (density >= 4.0) {
                screensizeset = "XXXHDPI | " + density;
            }
            if (density >= 5.0) {
                screensizeset = "XXXXHDPI | " + density;
            }
            screen_size.setSummary(screensizeset);

            debug_text.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx, R.style.CustomDialog);

                    // builder.setMessage("Text not finished...");
                    LayoutInflater inflater = atx.getLayoutInflater();
                    View dialog = inflater.inflate(R.layout.dialog_setcalibration, null);
                    builder.setView(dialog);
                    final EditText input = dialog.findViewById(R.id.inputcodetext);

                    input.setLines(1);
                    input.setHint(R.string.debug_text);
                    builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            prefs.edit().putInt("calint", Integer.parseInt(input.getText().toString())).apply();
                            debug_text.setSummary("manuell -> " + input.getText().toString());
                            prefs.edit().putString("debugger", getResources().getString(R.string.settings_cal_suc_value_man) + " " + input.getText().toString()).apply();

                            prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
                            // Todo publish the code to database
                        }
                    });

                    builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            // Todo publish the code to database
                        }
                    });

                    builder.setCancelable(true);

                    if (!atx.isFinishing()) {
                        builder.create().show();

                    }

                    //open browser or intent here
                    return true;
                }
            });
            debug_text = findPreference("debug_key");
            debug_text.setSummary(prefs.getString("debugger", "" + getResources().getString(R.string.settings_debug_sum)));
            app_version = findPreference("app_version");
            app_version.setSummary("v" + AgentData.getappversion("appversion"));
            tg_news = findPreference("link_telegram_news");
            tg_en = findPreference("link_telegram_en");
            tg_de = findPreference("link_telegram_de");
            tg_ru = findPreference("link_telegram_ru");
            tg_es = findPreference("link_telegram_es");
            account_delete = findPreference("account_delete");

            if (AgentData.getTG_news() != null) {

                tg_news.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(AgentData.getTG_news()));
                        startActivity(i);
                        //open browser or intent here
                        return true;
                    }
                });

                tg_en.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(AgentData.getTG_en()));
                        startActivity(i);
                        //open browser or intent here
                        return true;
                    }
                });
                privacy_status.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                        Intent ix = new Intent(ctx, PrivacySettings.class);
                        //i.putExtra("PersonID", personID);
                        startActivity(ix);
                        atx.finish();

                        //open browser or intent here
                        return true;
                    }
                });
                notify_opens_action.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                        Intent ix = new Intent(ctx, NotifySettings.class);
                        //i.putExtra("PersonID", personID);
                        startActivity(ix);
                        atx.finish();

                        //open browser or intent here
                        return true;
                    }
                });
                tg_de.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(AgentData.getTG_de()));
                        startActivity(i);
                        //open browser or intent here
                        return true;
                    }
                });
                tg_ru.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(AgentData.getTG_ru()));
                        startActivity(i);
                        //open browser or intent here
                        return true;
                    }
                });
                tg_es.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    public boolean onPreferenceClick(Preference preference) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(AgentData.getTG_es()));
                        startActivity(i);
                        //open browser or intent here
                        return true;
                    }
                });
            }
            account_delete.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    deleteall();

                    //open browser or intent here
                    return true;
                }
            });
            basics_settings = (PreferenceGroup) findPreference("basics_settings");
            privacy_terms.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Intent ix = new Intent(ctx, Privacy.class);
                    //i.putExtra("PersonID", personID);
                    startActivity(ix);

                    //open browser or intent here
                    return true;
                }
            });
            used_libs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Intent ix = new Intent(ctx, LibsUsed.class);
                    //i.putExtra("PersonID", personID);
                    startActivity(ix);

                    //open browser or intent here
                    return true;
                }
            });
            developer_name.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {

                    String url = "https://www.paypal.me/PrXenon";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                    //open browser or intent here
                    return true;
                }
            });

            export_ebl.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    String BOBS_MAIN_ACTIIVTY = "rocks.prxenon.aodeblplugin.EBLSettings";
                    String BOBS_PACKAGE = "rocks.prxenon.aodeblplugin";
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName(BOBS_PACKAGE, BOBS_MAIN_ACTIIVTY));
                    // intent.putExtra(EXTRA_SECRET_MESSAGE, message);
                    startActivity(intent);
                    atx.finish();
                    //startexportebl();
                    return true;
                }
            });

            trickster_status.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                    atx.startActivity(intent);

                    //open browser or intent here
                    return true;
                }
            });

            calibration.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    if (workaround.isChecked()) {
                        Intent intent = new Intent(getActivity(),
                                ScreenCalibrationWorkaround.class);
                        getActivity().startActivityForResult(intent, REQUEST_CODE);
                    } else {
                        if (!Settings.canDrawOverlays(atx)) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + atx.getPackageName()));
                            atx.startActivityForResult(intent, 654);
                        } else {
                            if (isStoragePermissionGranted(ctx, atx)) {
                                Intent intent = new Intent(getActivity(),
                                        ScreenCalibration.class);
                                getActivity().startActivityForResult(intent, REQUEST_CODE);
                            } else {
                                StyleableToast.makeText(ctx, "Storage perrmission needed", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }
                        }
                    }

                    //open browser or intent here
                    return true;
                }
            });

            calibration_inventory.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    if (workaround.isChecked()) {
                        StyleableToast.makeText(ctx, "Android 6 is not supported", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    } else {
                        if (!Settings.canDrawOverlays(atx)) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + atx.getPackageName()));
                            atx.startActivityForResult(intent, 654);
                        } else {
                            if (isStoragePermissionGranted(ctx, atx)) {
                                Intent intent = new Intent(getActivity(),
                                        ScreenCalibrationInventory.class);
                                getActivity().startActivityForResult(intent, REQUEST_CODE_CAL);
                            } else {
                                StyleableToast.makeText(ctx, "Storage perrmission needed", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                            }
                        }
                    }

                    //open browser or intent here
                    return true;
                }
            });

            workaround = (CheckBoxPreference) findPreference("workaround");
            ingress_prime_timer = (ListPreference) findPreference("ingress_prime_timer");
            mainprevview = getPreferenceScreen();
            if (Build.VERSION.RELEASE == "6.0" || Build.VERSION.RELEASE == "6" || Build.VERSION.RELEASE == "6.0.1" || Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                ((PreferenceGroup) findPreference("basics_settings")).addPreference(workaround);
            } else {
                ((PreferenceGroup) findPreference("basics_settings")).removePreference(workaround);
                // ((PreferenceGroup) findPreference("basics_settings")).addPreference(workaround);

            }
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                ((PreferenceGroup) findPreference("basics_settings")).removePreference(ingress_prime_timer);

            }


            if (AgentData.geteblinstalled()) {

                ((PreferenceGroup) findPreference("extensions")).removePreference(plugins_text);
                ((PreferenceGroup) findPreference("extensions")).addPreference(export_ebl);
            } else {
                ((PreferenceGroup) findPreference("extensions")).addPreference(plugins_text);
                ((PreferenceGroup) findPreference("extensions")).removePreference(export_ebl);
                // ((PreferenceGroup) findPreference("basics_settings")).addPreference(workaround);

            }
            bindPreferenceSummaryToValue2(workaround);
            bindPreferenceSummaryToValue3(screen_save);
            bindPreferenceSummaryToValue4(update_missions);

            if (prefs.getBoolean(remoteconfig.getString("pref_calibration_invent"), false)) {
                calibration_inventory.setSummary(getResources().getString(R.string.settings_cal_suc));
            }
            /*
            prefs.edit().putBoolean(remoteconfig.getString("pref_calibration_invent"), true).apply();
                    prefs.edit().putInt("calint_invent", data.getIntExtra("claint_invent", 0)).apply();
                    calibration_inventory.setSummary(getResources().getString(R.string.settings_cal_suc));
             */

            if (workaround.isChecked()) {

            } else {
                bindPreferenceSummaryToValue(findPreference("ingress_appart"));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    bindPreferenceSummaryToValue(findPreference("ingress_prime_timer"));
                }
            }


        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                // startActivity(new Intent(getActivity(), AODMain.class));
                atx.finish();

                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
}
