package rocks.prxenon.aod.settings;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.muddzdev.styleabletoast.StyleableToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.CalibrationFix;
import rocks.prxenon.aod.helper.OverlayCalibrationService;
import rocks.prxenon.aod.helper.ServiceCallBack;

import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;


public class ScreenCalibrationInventory extends AppCompatActivity implements ServiceCallBack {

    private static final int REQUEST_OVERLAY = 2;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int NUM_PAGES = 5;
    private static final String[] PERMISSIONS_OVERLAY = {
            Manifest.permission.SYSTEM_ALERT_WINDOW
    };
    OverlayCalibrationService mService;
    boolean mBound = false;
    private SharedPreferences prefs;
    private Typeface type, proto;
    private TextView mTitleTextView;
    private FancyButton startcal;
    private final ServiceConnection connectionService = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(" VEG2", "onServiceConnected");
            OverlayCalibrationService.LocalBinder scan = (OverlayCalibrationService.LocalBinder) service;
            mService = scan.getService();
            mService.setServiceCallBack(ScreenCalibrationInventory.this);
            mService.setCallbacks(ScreenCalibrationInventory.this); // register
            mService.startIngress();
            mBound = true;


        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

            Log.d("TAG", "onServiceDisconnected");
            mBound = false;
        }
    };


    public static void verifyOverlay(Activity activity) {
        // Check if we have write permission
        if (!Settings.canDrawOverlays(activity)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + activity.getPackageName()));
            activity.startActivityForResult(intent, REQUEST_OVERLAY);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screencalibration_invent);


        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");
        prefs = getSharedPreferences("AppPref", MODE_PRIVATE);
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_settings, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(getResources().getString(R.string.title_activity_calibration));
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(25);


        startcal = findViewById(R.id.startcal_btn);

        startcal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(ScreenCalibrationInventory.this, OverlayCalibrationService.class);
                bindService(intent, connectionService, Context.BIND_AUTO_CREATE);
                SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(ScreenCalibrationInventory.this);
                Log.i("SERVICESTATUS", "notrunning" + "a. " + prefs2.getString("ingress_appart", "-1"));
                Intent svc = new Intent(ScreenCalibrationInventory.this, OverlayCalibrationService.class);
                if (prefs2.getString("ingress_appart", "-1").equalsIgnoreCase("0")) {
                    svc.putExtra("ingress", "com.nianticlabs.ingress.prime.qa");
                } else {
                    svc.putExtra("ingress", "com.nianticproject.ingress");
                }


                startService(svc);
            }


                    /*
                     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    StorageManager sm = (StorageManager) getApplicationContext().getSystemService(Context.STORAGE_SERVICE);


                    Intent ix = sm.getPrimaryStorageVolume().createOpenDocumentTreeIntent();


                    ix.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                            | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);

                    startActivityForResult(ix, 42);

                }
                     */
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mBound) {
            Log.w("Service -_>", "Closed");

            unbindService(connectionService);
            mService.setCallbacks(null); // unregister
            mBound = false;
        }

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {

        if (mBound) {
            unbindService(connectionService);
            mService.setCallbacks(null); // unregister

            mBound = false;
        }

        if (isMyServiceRunning(OverlayCalibrationService.class)) {
            Log.i("SERVICESTATUS", "running");
            Intent svc = new Intent(ScreenCalibrationInventory.this, OverlayCalibrationService.class);
            stopService(svc);
        }
        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // REMOVE QR FROM FIREBASE

        Intent intent = getIntent();
        intent.putExtra("key", "Pommes");
        intent.putExtra("claint_invent", 0);
        setResult(RESULT_OK, intent);
        finish();


    }

    @Override
    public void connectToDeviceclip(Intent intent) {

    }

    @Override
    public void connectToDevice(Intent intent) {
        Log.d("IMAGE CALIBRATION", " " + intent.getExtras().getString("screen"));
        if (isMyServiceRunning(OverlayCalibrationService.class)) {
            Log.i("SERVICESTATUS2 CAL", "running");
            Intent svc = new Intent(ScreenCalibrationInventory.this, OverlayCalibrationService.class);
            stopService(svc);
        }
        if (mBound) {

            unbindService(connectionService);
            mService.setCallbacks(null); // unregister
            mBound = false;
        }


        ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setTitle("AOD Magic");
        pd.setMessage("Please wait...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.show();

        Bitmap bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));


        if (bitmap == null) {

            try {
                Thread.sleep(1000); //add another 3 seconds delay
                bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));

            } catch (InterruptedException e) {
                bitmap = null;
                e.printStackTrace();
            }
        }


        if (bitmap == null) {

            try {
                Thread.sleep(2000); //add another 3 seconds delay
                bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));
            } catch (InterruptedException e) {
                bitmap = null;
                e.printStackTrace();
            }
        }

        if (bitmap == null) {

            try {
                Thread.sleep(2000); //add another 3 seconds delay
                bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));
            } catch (InterruptedException e) {
                bitmap = null;
                e.printStackTrace();
            }
        }


        if (bitmap != null) {

            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
            ContentResolver contentResolver = getContentResolver();


            recognizeText(image, pd);

            contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    MediaStore.Images.ImageColumns.DATA + "=?", new String[]{intent.getExtras().getString("screen")});
        } else {
            pd.dismiss();
            StyleableToast.makeText(getApplicationContext(), "Something did not work. Please check the app permissions.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

        }
       /* if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted");
                callBroadCast();
            } else {
                System.out.println("file not Deleted");
            }
        } */

        //connectToDevice(intent);
    }

    @Override
    public void closewindow(String s, String theartid, String themid, String comp, String stats) {
        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> rt = am.getRunningTasks(Integer.MAX_VALUE);
        Log.w("Back -->", "pressed l " + rt.size());


        for (int i = 0; i < rt.size(); i++) {

            // bring to front
            if (rt.get(i).baseActivity.toShortString().indexOf("rocks.prxenon.aod") > -1) {
                am.moveTaskToFront(rt.get(i).id, ActivityManager.MOVE_TASK_WITH_HOME);
                Intent svc = new Intent(ScreenCalibrationInventory.this, OverlayCalibrationService.class);
                stopService(svc);
            }

        }
        if (mBound) {

            unbindService(connectionService);
            mService.setCallbacks(null); // unregister
            mBound = false;
        }
    }


    private void recognizeText(FirebaseVisionImage image, final ProgressDialog pd) {

        Log.w("MBOUND ----> ", "" + mBound);
        // [START get_detector_default]
        FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance()
                .getOnDeviceTextRecognizer();
        // [END get_detector_default]


        // [START run_detector]
        Task<FirebaseVisionText> result =
                detector.processImage(image)
                        .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                            @Override
                            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                                // Task completed successfully
                                // [START_EXCLUDE]
                                // [START get_text]
                                boolean foundx = false;
                                int blockis = -1;
                                int blockis2 = -1;
                                Map<String, Object> wertex = new HashMap<>();
                                String testvalue = "Fehler, der Wert wurde nicht gefunden.";
                                int calibration = 0;

                                SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(ScreenCalibrationInventory.this);
                                Log.i("SERVICESTATUS", "notrunning" + "a. " + prefs2.getString("ingress_appart", "-1"));
                                Intent svc = new Intent(ScreenCalibrationInventory.this, OverlayCalibrationService.class);
                                if (firebaseVisionText.getText().contains("Resonator")) {
                                    int blocksize = firebaseVisionText.getTextBlocks().size();

                                    if (prefs2.getString("ingress_appart", "-1").equalsIgnoreCase("1")) {

                                        Log.i("WEG -->", "1");
                                        for (FirebaseVisionText.TextBlock block : firebaseVisionText.getTextBlocks()) {

                                            blockis++;
                                            Rect boundingBox = block.getBoundingBox();
                                            Point[] cornerPoints = block.getCornerPoints();
                                            String text = block.getText();
                                            // Log.w("Block-> ", text);
                                            wertex.put("" + block.getBoundingBox().top, block.getText());


                                            for (FirebaseVisionText.Line line : block.getLines()) {
                                                // ...

                                                Log.w("Line-> ", "" + line.getText());
                                                if ((line.getText().startsWith("L1") || line.getText().startsWith("L2") || line.getText().startsWith("L3") || line.getText().startsWith("L7")) && line.getText().length() >= 5) {
                                                    foundx = true;

                                                    testvalue = firebaseVisionText.getTextBlocks().get(blockis).getText();
                                                    calibration = firebaseVisionText.getTextBlocks().get(blockis).getLines().get(0).getBoundingBox().top;
//                                                    prefs.edit().putString("debugger", "" + calibration + " BlockA: " + text + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis - 2).getText() + " | Block3: " + firebaseVisionText.getTextBlocks().get(blockis - 3).getText()).apply();

                                                    //   Log.w("7765ghg" ,line.getText()+ " p: "+blockis );

                                                    Log.w("Result11-> ", testvalue);

                                                    Log.w("Result22-> ", testvalue);
                                                }

                                            }
                                    /*    if (text.equalsIgnoreCase("Connector")) {
                                            foundx = true;
                                            testvalue = firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getText();
                                           // Log.w("Line Connector-> ", "t: " + firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getBoundingBox().top + " l:" + firebaseVisionText.getTextBlocks().get(blockis).getLines().get(0).getBoundingBox().left);
                                            calibration = firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getBoundingBox().top;
                                            prefs.edit().putString("debug_key",""+calibration).apply();

                                        } */

                                            // Log.w("Box2-> ", "t: " + block.getBoundingBox().top + " l:" + block.getBoundingBox().left + " r:" + block.getBoundingBox().right + " b:" + block.getBoundingBox().bottom);


                                        }
                                        //  ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                        //  ClipData clip = ClipData.newPlainText("AOD Debug", wertex.toString());
                                        // clipboard.setPrimaryClip(clip);
                                        // Log.i("LOG ", wertex.toString());
                                        // sendlog(wertex.toString());

                                        if (foundx && (!testvalue.equalsIgnoreCase("5000") && !testvalue.equalsIgnoreCase("25000") && !testvalue.equalsIgnoreCase("1000") && !testvalue.equalsIgnoreCase("50"))) {

                                            String[] separated = testvalue.toLowerCase().split("x");

                                            testvalue = separated[1].replaceAll("[^\\d.]", "");
                                            Log.w("Result33-> ", testvalue);
                                            Intent intent = getIntent();
                                            intent.putExtra("key", "Pommes");
                                            intent.putExtra("claint_invent", calibration);
                                            intent.putExtra("testvalue", testvalue);
                                            setResult(RESULT_OK, intent);
                                            pd.dismiss();
                                            finish();
                                        } else {
                                            pd.dismiss();
                                            // TODO LIST WITH
                                            StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.string_error_clibration), Toast.LENGTH_LONG, R.style.defaulttoastb).show();


                                        }
                                    }
                                } else {
                                    pd.dismiss();
                                    StyleableToast.makeText(getApplicationContext(), "No Resonators for calibration found", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                }
                                // [END get_text]
                                // [END_EXCLUDE]
                            }
                        })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w("LINE-->", e.getMessage());
                                        // Task failed with an exception
                                        // ...
                                    }
                                });


        // [END run_detector]
    }

    private void sendlog(String lli) {
        //  Crashlytics.log(Log.DEBUG, "tag", lli);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        Log.i("RESPONSE -->", "" + requestCode + resultCode);


        if (requestCode == 42 && resultCode == RESULT_OK && null != data) {
            Intent intent = new Intent(ScreenCalibrationInventory.this, OverlayCalibrationService.class);
            bindService(intent, connectionService, Context.BIND_AUTO_CREATE);
            SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(ScreenCalibrationInventory.this);
            Log.i("SERVICESTATUS RESULT", "notrunning" + "a. " + prefs2.getString("ingress_appart", "-1"));
            Intent svc = new Intent(ScreenCalibrationInventory.this, OverlayCalibrationService.class);
            if (prefs2.getString("ingress_appart", "-1").equalsIgnoreCase("0")) {
                svc.putExtra("ingress", "com.nianticlabs.ingress.prime.qa");
            } else {
                svc.putExtra("ingress", "com.nianticproject.ingress");
            }

            Uri uri = data.getData();
            if (uri == null) {

            } else {
                Log.i("SERVICESTATUS RESULT2", "notrunning" + "a. " + prefs2.getString("ingress_appart", "-1"));
                getApplicationContext().getContentResolver()
                        .takePersistableUriPermission(uri,
                                Intent.FLAG_GRANT_READ_URI_PERMISSION);

            }
            startService(svc);
        }


    }
}