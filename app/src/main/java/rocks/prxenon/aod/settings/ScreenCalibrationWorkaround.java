package rocks.prxenon.aod.settings;

import android.Manifest;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.muddzdev.styleabletoast.StyleableToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.CalibrationFix;
import rocks.prxenon.aod.helper.OverlayCalibrationService;
import rocks.prxenon.aod.helper.ServiceCallBack;

import static rocks.prxenon.aod.basics.AplicationStart.remoteconfig;


public class ScreenCalibrationWorkaround extends AppCompatActivity implements ServiceCallBack {


    private static final int REQUEST_OVERLAY = 2;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int NUM_PAGES = 5;
    private static final String[] PERMISSIONS_OVERLAY = {
            Manifest.permission.SYSTEM_ALERT_WINDOW
    };
    private SharedPreferences prefs;
    private Typeface type, proto;
    private TextView mTitleTextView;
    private FancyButton startcal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screencalibration_workaround);


        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");
        prefs = getSharedPreferences("AppPref", MODE_PRIVATE);
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_settings, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText("Kalibrierung");
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(25);


        startcal = findViewById(R.id.startcal_btn);

        startcal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 7876);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 7876 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();


            Log.d("file", "FILE CHANGE OCCURED " + picturePath);


            data.putExtra("screen", picturePath);
            connectToDevice(data);
            //ImageView imageView = (ImageView) findViewById(R.id.imgView);
            //imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDestroy() {


        super.onDestroy();
        Log.i("ONDESTROY ROOT", "destroyed");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // REMOVE QR FROM FIREBASE

        Intent intent = getIntent();
        intent.putExtra("key", "Pommes");
        intent.putExtra("claint", 0);
        setResult(RESULT_OK, intent);
        finish();


    }

    @Override
    public void connectToDeviceclip(Intent intent) {

    }

    @Override
    public void connectToDevice(Intent intent) {

        if (intent.getExtras().getString("screen") == null) {
            StyleableToast.makeText(getApplicationContext(), "Fehler: Der Screenshot muss Local gespeichert sein. Erstelle einen neuen Screenshot.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
        } else {
            Bitmap bitmap = BitmapFactory.decodeFile(intent.getExtras().getString("screen"));
            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);

            // Log.i("URI OF FILE-->", Uri.fromFile(myFile).toString());
            //   File fdelete = new File(intent.getExtras().getString("screen"));
            ContentResolver contentResolver = getContentResolver();
            contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    MediaStore.Images.ImageColumns.DATA + "=?", new String[]{intent.getExtras().getString("screen")});
       /* if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted");
                callBroadCast();
            } else {
                System.out.println("file not Deleted");
            }
        } */
            recognizeText(image);
        }
        //connectToDevice(intent);
    }

    @Override
    public void closewindow(String s, String theartid, String themid, String comp, String stats) {
        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> rt = am.getRunningTasks(Integer.MAX_VALUE);
        Log.w("Back -->", "pressed z " + rt.size());


        for (int i = 0; i < rt.size(); i++) {

            // bring to front
            if (rt.get(i).baseActivity.toShortString().indexOf("rocks.prxenon.aod") > -1) {
                am.moveTaskToFront(rt.get(i).id, ActivityManager.MOVE_TASK_WITH_HOME);
                Intent svc = new Intent(ScreenCalibrationWorkaround.this, OverlayCalibrationService.class);
                stopService(svc);
            }

        }

    }


    private void recognizeText(FirebaseVisionImage image) {

        // [START get_detector_default]
        FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance()
                .getOnDeviceTextRecognizer();
        // [END get_detector_default]


        // [START run_detector]
        Task<FirebaseVisionText> result =
                detector.processImage(image)
                        .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                            @Override
                            public void onSuccess(FirebaseVisionText firebaseVisionText) {
                                // Task completed successfully
                                // [START_EXCLUDE]
                                // [START get_text]
                                boolean foundx = false;
                                int blockis = -1;
                                int blockis2 = -1;
                                Map<String, Object> wertex = new HashMap<>();
                                String testvalue = "Fehler, der Wert wurde nicht gefunden.";
                                int calibration = 0;

                                SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(ScreenCalibrationWorkaround.this);
                                Log.i("SERVICESTATUS", "notrunning" + "a. " + prefs2.getString("ingress_appart", "-1"));
                                Intent svc = new Intent(ScreenCalibrationWorkaround.this, OverlayCalibrationService.class);
                                if (firebaseVisionText.getText().contains("Connector")) {
                                    int blocksize = firebaseVisionText.getTextBlocks().size();

                                    if (prefs2.getString("ingress_appart", "-1").equalsIgnoreCase("1")) {

                                        Log.i("WEG -->", "1");
                                        for (FirebaseVisionText.TextBlock block : firebaseVisionText.getTextBlocks()) {

                                            blockis++;
                                            Rect boundingBox = block.getBoundingBox();
                                            Point[] cornerPoints = block.getCornerPoints();
                                            String text = block.getText();
                                            // Log.w("Block-> ", text);
                                            wertex.put("" + block.getBoundingBox().top, block.getText());


                                            for (FirebaseVisionText.Line line : block.getLines()) {
                                                // ...

                                                // Log.w("Line-> ", "" + line.getText());
                                                if (line.getText().contains("Connector")) {
                                                    foundx = true;

                                                    if ((blockis + 1) >= firebaseVisionText.getTextBlocks().size()) {
                                                        //    StyleableToast.makeText(getApplicationContext(), "Leider hat das nicht geklappt.", Toast.LENGTH_LONG, R.style.defaulttoast).show();

                                                        testvalue = firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getElements().get(0).getText();
                                                        calibration = firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getBoundingBox().top;
                                                        prefs.edit().putString("debugger", "" + calibration + " Block: " + text + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis - 2).getText() + " | Block3: " + firebaseVisionText.getTextBlocks().get(blockis - 3).getText()).apply();

                                                    } else {
                                                        testvalue = firebaseVisionText.getTextBlocks().get(blockis + 1).getLines().get(0).getElements().get(0).getText();

                                                        calibration = firebaseVisionText.getTextBlocks().get(blockis + 1).getLines().get(0).getBoundingBox().top;
                                                        // testvalue = firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getText();
                                                        // Log.w("Line Connector-> ", "t: " + firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getBoundingBox().top + " l:" + firebaseVisionText.getTextBlocks().get(blockis).getLines().get(0).getBoundingBox().left);
                                                        // calibration = firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getBoundingBox().top;
                                                        prefs.edit().putString("debugger", "" + calibration + " Block: " + text + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis - 2).getText()).apply();

                                                    }
                                                }

                                            }
                                    /*    if (text.equalsIgnoreCase("Connector")) {
                                            foundx = true;
                                            testvalue = firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getText();
                                           // Log.w("Line Connector-> ", "t: " + firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getBoundingBox().top + " l:" + firebaseVisionText.getTextBlocks().get(blockis).getLines().get(0).getBoundingBox().left);
                                            calibration = firebaseVisionText.getTextBlocks().get(blockis - 1).getLines().get(0).getBoundingBox().top;
                                            prefs.edit().putString("debug_key",""+calibration).apply();

                                        } */

                                            // Log.w("Box2-> ", "t: " + block.getBoundingBox().top + " l:" + block.getBoundingBox().left + " r:" + block.getBoundingBox().right + " b:" + block.getBoundingBox().bottom);


                                        }
                                        //  ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                        //  ClipData clip = ClipData.newPlainText("AOD Debug", wertex.toString());
                                        //  clipboard.setPrimaryClip(clip);
                                        // Log.i("LOG ", wertex.toString());
                                        // sendlog(wertex.toString());

                                        testvalue = testvalue.replace('o', '0');
                                        testvalue = testvalue.replace('O', '0');
                                        testvalue = testvalue.replace('l', '1');
                                        testvalue = testvalue.replace('i', '1');
                                        testvalue = testvalue.replace('I', '1');
                                        testvalue = testvalue.replace('L', '1');
                                        testvalue = testvalue.replace(",", "");
                                        testvalue = testvalue.replace(".", "");

                                        if (foundx && (!testvalue.equalsIgnoreCase("5000") && !testvalue.equalsIgnoreCase("25000") && !testvalue.equalsIgnoreCase("1000") && !testvalue.equalsIgnoreCase("50"))) {

                                            Log.w("Result-> ", testvalue);
                                            Intent intent = getIntent();
                                            intent.putExtra("key", "Pommes");
                                            intent.putExtra("claint", calibration);
                                            intent.putExtra("testvalue", testvalue);
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        } else {
                                            // TODO LIST WITH
                                            StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.string_error_clibration), Toast.LENGTH_LONG, R.style.defaulttoastb).show();


                                            final AlertDialog.Builder builder = new AlertDialog.Builder(ScreenCalibrationWorkaround.this, R.style.CustomDialog);

                                            // builder.setMessage("Text not finished...");
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialog = inflater.inflate(R.layout.dialog_calibration_fix, null);
                                            builder.setView(dialog);
                                            final GridView gridviewvalues = dialog.findViewById(R.id.gridvalues);
                                            // input.setAllCaps(true);

                                            final List<String> boxindex = new ArrayList<String>();
                                            final List<String> boxvalue = new ArrayList<String>();
                                            for (Map.Entry<String, Object> entry : wertex.entrySet()) {
                                                String valisg = entry.getValue().toString().replaceAll("[^\\d.]", "");
                                                valisg = valisg.replace(".", "");
                                                valisg = valisg.replace(",", "");

                                                try {
                                                    double d = Double.parseDouble(valisg);
                                                    boxindex.add(entry.getKey());

                                                    boxvalue.add(valisg);
                                                } catch (NumberFormatException nfe) {

                                                }

                                                // do what you have to do here
                                                // In your case, another loop.
                                            }
                                            final CalibrationFix adapter = new CalibrationFix(ScreenCalibrationWorkaround.this, wertex.size(), boxindex, boxvalue);
                                            gridviewvalues.setAdapter(adapter);


                                            gridviewvalues.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    //text.setText((String) (lv.getItemAtPosition(position)));
                                                    prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
                                                    prefs.edit().putInt("calint", Integer.parseInt(boxindex.get(position))).apply();
                                                    //calibration.setSummary(getResources().getString(R.string.settings_cal_suc));
                                                    // debug_text.setSummary(prefs.getString("debugger", ""+R.string.settings_debug_sum));
                                                    int testconvert = 0;
                                                    String toconvert = boxvalue.get(position).replaceAll("\\s+", "");
                                                    toconvert = toconvert.replace('o', '0');
                                                    toconvert = toconvert.replace('O', '0');
                                                    toconvert = toconvert.replace('l', '1');
                                                    toconvert = toconvert.replace('i', '1');
                                                    toconvert = toconvert.replace('I', '1');
                                                    toconvert = toconvert.replace('L', '1');
                                                    toconvert = toconvert.replace(",", "");
                                                    toconvert = toconvert.replace(".", "");
                                                    //  Log.i("ITEM_VALUE", "" + (String) (boxvalue.get(position)));
                                                    Log.i("ITEM_VALUE", "" + boxvalue.get(position) + " ->" + boxindex.get(position));
                                                    builder.create().dismiss();
                                                    Intent intent = getIntent();
                                                    intent.putExtra("key", "Pommes");
                                                    intent.putExtra("claint", boxindex.get(position));
                                                    intent.putExtra("testvalue", boxvalue.get(position));
                                                    setResult(RESULT_OK, intent);
                                                    finish();

                                                }
                                            });

                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {


                                                    // Todo publish the code to database
                                                }
                                            });

                                            builder.setCancelable(true);

                                            if (!isFinishing()) {
                                                builder.create().show();

                                            }

                                        }
                                    } else {
                                        for (FirebaseVisionText.TextBlock block : firebaseVisionText.getTextBlocks()) {
                                            // Crashlytics.log(Log.DEBUG, "tag", "Block: " + block.getText() + " " + block.getBoundingBox().top);
                                            blockis2++;
                                            Rect boundingBox = block.getBoundingBox();
                                            Point[] cornerPoints = block.getCornerPoints();
                                            String text = block.getText();
                                            //  Log.w("Block-> ", block.getText());
                                            wertex.put("" + block.getBoundingBox().top, block.getText());
                                            for (FirebaseVisionText.Line line : block.getLines()) {
                                                //  Log.w("Line-> ", line.getText());

                                                if (blockis2 <= 2 && line.getText().equalsIgnoreCase("ALL TIME MONTH WEEK NOW")) {
                                                    if (firebaseVisionText.getTextBlocks().get(blockis2 + 1).getText().equalsIgnoreCase("25,000") || firebaseVisionText.getTextBlocks().get(blockis2 - 1).getText().equalsIgnoreCase("25.000")) {
                                                        foundx = true;
                                                        if (firebaseVisionText.getTextBlocks().get(blockis2 + 3).getText().equalsIgnoreCase("50") || firebaseVisionText.getTextBlocks().get(blockis2 - 4).getText().equalsIgnoreCase("5O")) {
                                                            testvalue = firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getElements().get(0).getText();
                                                            calibration = firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getBoundingBox().top;
                                                            //    Log.w("Line Connector-> ", "t: " + firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getBoundingBox().top + " l:" + firebaseVisionText.getTextBlocks().get(blockis2 +4).getLines().get(0).getBoundingBox().left);
                                                            prefs.edit().putString("debugger", "" + calibration + " Block: " + line.getText() + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis2 + 1).getText() + " | Block3: " + firebaseVisionText.getTextBlocks().get(blockis2 + 2).getText() + " | Block4: " + firebaseVisionText.getTextBlocks().get(blockis2 + 3).getText()).apply();
                                                            if (testvalue.equalsIgnoreCase("0")) {
                                                                foundx = false;
                                                                Log.w("Error 4-> ", testvalue);
                                                            }
                                                        } else {
                                                            testvalue = firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getElements().get(0).getText();
                                                            calibration = firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getBoundingBox().top;
                                                            Log.w("Line Connector-> ", "t: " + firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getBoundingBox().top + " l:" + firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getBoundingBox().left);
                                                            prefs.edit().putString("debugger", "" + calibration + " Block: " + line.getText() + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis2 + 1).getText() + " | Block3: " + firebaseVisionText.getTextBlocks().get(blockis2 + 2).getText() + " | Block4: " + firebaseVisionText.getTextBlocks().get(blockis2 + 3).getText()).apply();
                                                            if (testvalue.equalsIgnoreCase("0")) {
                                                                foundx = false;
                                                                Log.w("Error 3-> ", testvalue);
                                                            }
                                                        }
                                                    }
                                                } else if (line.getText().equalsIgnoreCase("100,000") || line.getText().equalsIgnoreCase("100.000")) {
                                                    if (blockis2 - 1 < 0 || blockis2 - 1 >= firebaseVisionText.getTextBlocks().size()) {
                                                        foundx = false;
                                                    } else {
                                                        if (firebaseVisionText.getTextBlocks().get(blockis2 - 1).getText().equalsIgnoreCase("25,000") || firebaseVisionText.getTextBlocks().get(blockis2 - 1).getText().equalsIgnoreCase("25.000")) {
                                                            foundx = true;
                                                            if (firebaseVisionText.getTextBlocks().get(blockis2 - 4).getText().equalsIgnoreCase("50") || firebaseVisionText.getTextBlocks().get(blockis2 - 4).getText().equalsIgnoreCase("5O")) {
                                                                testvalue = firebaseVisionText.getTextBlocks().get(blockis2 - 5).getLines().get(0).getElements().get(0).getText();
                                                                calibration = firebaseVisionText.getTextBlocks().get(blockis2 - 5).getLines().get(0).getBoundingBox().top;
                                                                Log.w("Line Connector-> ", "t: " + firebaseVisionText.getTextBlocks().get(blockis2 - 5).getLines().get(0).getBoundingBox().top + " l:" + firebaseVisionText.getTextBlocks().get(blockis2 - 5).getLines().get(0).getBoundingBox().left);
                                                                prefs.edit().putString("debugger", "" + calibration + " Block: " + line.getText() + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis2 - 2).getText() + " | Block3: " + firebaseVisionText.getTextBlocks().get(blockis2 - 3).getText() + " | Block4: " + firebaseVisionText.getTextBlocks().get(blockis2 - 4).getText()).apply();
                                                                if (testvalue.equalsIgnoreCase("0")) {
                                                                    foundx = false;
                                                                    Log.w("Error 4-> ", testvalue);
                                                                }
                                                            } else {
                                                                testvalue = firebaseVisionText.getTextBlocks().get(blockis2 - 4).getLines().get(0).getElements().get(0).getText();
                                                                calibration = firebaseVisionText.getTextBlocks().get(blockis2 - 4).getLines().get(0).getBoundingBox().top;
                                                                Log.w("Line Connector-> ", "t: " + firebaseVisionText.getTextBlocks().get(blockis2 - 4).getLines().get(0).getBoundingBox().top + " l:" + firebaseVisionText.getTextBlocks().get(blockis2 - 4).getLines().get(0).getBoundingBox().left);
                                                                prefs.edit().putString("debugger", "" + calibration + " Block: " + line.getText() + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis2 - 1).getText() + " | Block3: " + firebaseVisionText.getTextBlocks().get(blockis2 - 2).getText() + " | Block4: " + firebaseVisionText.getTextBlocks().get(blockis2 - 3).getText()).apply();
                                                                if (testvalue.equalsIgnoreCase("0")) {
                                                                    foundx = false;
                                                                    Log.w("Error 3-> ", testvalue);
                                                                }
                                                            }

                                                        } else if (firebaseVisionText.getTextBlocks().get(blockis2 + 1).getText().equalsIgnoreCase("25,000") || firebaseVisionText.getTextBlocks().get(blockis2 + 1).getText().equalsIgnoreCase("25.000")) {

                                                            if (blocksize <= blockis + 5) {
                                                                foundx = true;
                                                                if (firebaseVisionText.getTextBlocks().get(blockis2 + 4).getText().equalsIgnoreCase("50") || firebaseVisionText.getTextBlocks().get(blockis2 + 4).getText().equalsIgnoreCase("5O")) {
                                                                    testvalue = firebaseVisionText.getTextBlocks().get(blockis2 + 5).getLines().get(0).getElements().get(0).getText();
                                                                    calibration = firebaseVisionText.getTextBlocks().get(blockis2 + 5).getLines().get(0).getBoundingBox().top;
                                                                    Log.w("Line Connector-> ", "t: " + firebaseVisionText.getTextBlocks().get(blockis2 + 5).getLines().get(0).getBoundingBox().top + " l:" + firebaseVisionText.getTextBlocks().get(blockis2 + 5).getLines().get(0).getBoundingBox().left);
                                                                    prefs.edit().putString("debugger", "" + calibration + " Block: " + line.getText() + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis2 + 2).getText() + " | Block3: " + firebaseVisionText.getTextBlocks().get(blockis2 + 3).getText() + " | Block4: " + firebaseVisionText.getTextBlocks().get(blockis2 + 4).getText()).apply();
                                                                    if (testvalue.equalsIgnoreCase("0")) {
                                                                        foundx = false;
                                                                        Log.w("Error 1-> ", testvalue);
                                                                    }
                                                                } else {
                                                                    foundx = true;
                                                                    testvalue = firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getElements().get(0).getText();
                                                                    calibration = firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getBoundingBox().top;
                                                                    Log.w("Line Connector-> ", "t: " + firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getBoundingBox().top + " l:" + firebaseVisionText.getTextBlocks().get(blockis2 + 4).getLines().get(0).getBoundingBox().left);
                                                                    prefs.edit().putString("debugger", "" + calibration + " Block: " + line.getText() + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis2 + 1).getText() + " | Block3: " + firebaseVisionText.getTextBlocks().get(blockis2 + 2).getText() + " | Block4: " + firebaseVisionText.getTextBlocks().get(blockis2 + 3).getText()).apply();
                                                                    if (testvalue.equalsIgnoreCase("0")) {
                                                                        foundx = false;
                                                                        Log.w("Error 2-> ", testvalue);
                                                                    }
                                                                }
                                                            } else {
                                                                prefs.edit().putString("debugger", "Block: " + line.getText() + " | Block2: " + firebaseVisionText.getTextBlocks().get(blockis2 - 1).getText()).apply();

                                                            }

                                                        }
                                                    }


                                                }

                                            }

                                            // Log.w("Box2-> ", "t: " + block.getBoundingBox().top + " l:" + block.getBoundingBox().left + " r:" + block.getBoundingBox().right + " b:" + block.getBoundingBox().bottom);


                                        }

                                        //  ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                                        //  ClipData clip = ClipData.newPlainText("AOD Debug", wertex.toString());
                                        //  clipboard.setPrimaryClip(clip);
                                        testvalue = testvalue.replace('o', '0');
                                        testvalue = testvalue.replace('O', '0');
                                        testvalue = testvalue.replace('l', '1');
                                        testvalue = testvalue.replace('i', '1');
                                        testvalue = testvalue.replace('I', '1');
                                        testvalue = testvalue.replace('L', '1');
                                        testvalue = testvalue.replace(",", "");
                                        testvalue = testvalue.replace(".", "");

                                        if (foundx && (!testvalue.equalsIgnoreCase("5000") && !testvalue.equalsIgnoreCase("25000") && !testvalue.equalsIgnoreCase("1000") && !testvalue.equalsIgnoreCase("50"))) {

                                            Log.w("Result-> ", testvalue);
                                            Intent intent = getIntent();
                                            intent.putExtra("key", "Pommes");
                                            intent.putExtra("claint", calibration);
                                            intent.putExtra("testvalue", testvalue);
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        } else {

                                            // TODO LIST WITH
                                            StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.string_error_clibration), Toast.LENGTH_LONG, R.style.defaulttoastb).show();


                                            final AlertDialog.Builder builder = new AlertDialog.Builder(ScreenCalibrationWorkaround.this, R.style.CustomDialog);

                                            // builder.setMessage("Text not finished...");
                                            LayoutInflater inflater = getLayoutInflater();
                                            View dialog = inflater.inflate(R.layout.dialog_calibration_fix, null);
                                            builder.setView(dialog);
                                            final GridView gridviewvalues = dialog.findViewById(R.id.gridvalues);
                                            // input.setAllCaps(true);

                                            final List<String> boxindex = new ArrayList<String>();
                                            final List<String> boxvalue = new ArrayList<String>();
                                            for (Map.Entry<String, Object> entry : wertex.entrySet()) {
                                                String valisg = entry.getValue().toString().replaceAll("[^\\d.]", "");
                                                valisg = valisg.replace(".", "");
                                                valisg = valisg.replace(",", "");

                                                try {
                                                    double d = Double.parseDouble(valisg);
                                                    boxindex.add(entry.getKey());

                                                    boxvalue.add(valisg);
                                                } catch (NumberFormatException nfe) {

                                                }

                                                // do what you have to do here
                                                // In your case, another loop.
                                            }
                                            final CalibrationFix adapter = new CalibrationFix(ScreenCalibrationWorkaround.this, wertex.size(), boxindex, boxvalue);
                                            gridviewvalues.setAdapter(adapter);


                                            gridviewvalues.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                    //text.setText((String) (lv.getItemAtPosition(position)));
                                                    prefs.edit().putBoolean(remoteconfig.getString("pref_calibration"), true).apply();
                                                    prefs.edit().putInt("calint", Integer.parseInt(boxindex.get(position))).apply();
                                                    //calibration.setSummary(getResources().getString(R.string.settings_cal_suc));
                                                    // debug_text.setSummary(prefs.getString("debugger", ""+R.string.settings_debug_sum));
                                                    int testconvert = 0;
                                                    String toconvert = boxvalue.get(position).replaceAll("\\s+", "");
                                                    toconvert = toconvert.replace('o', '0');
                                                    toconvert = toconvert.replace('O', '0');
                                                    toconvert = toconvert.replace('l', '1');
                                                    toconvert = toconvert.replace('i', '1');
                                                    toconvert = toconvert.replace('I', '1');
                                                    toconvert = toconvert.replace('L', '1');
                                                    toconvert = toconvert.replace(",", "");
                                                    toconvert = toconvert.replace(".", "");
                                                    //  Log.i("ITEM_VALUE", "" + (String) (boxvalue.get(position)));
                                                    Log.i("ITEM_VALUE", "" + boxvalue.get(position) + " ->" + boxindex.get(position));
                                                    builder.create().dismiss();
                                                    Intent intent = getIntent();
                                                    intent.putExtra("key", "Pommes");
                                                    intent.putExtra("claint", boxindex.get(position));
                                                    intent.putExtra("testvalue", boxvalue.get(position));
                                                    setResult(RESULT_OK, intent);
                                                    finish();

                                                }
                                            });

                                            builder.setNegativeButton(getString(R.string.cancel_txt), new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {


                                                    // Todo publish the code to database
                                                }
                                            });

                                            builder.setCancelable(true);

                                            if (!isFinishing()) {
                                                builder.create().show();

                                            }
                                        }

                                    }
                                } else {
                                    StyleableToast.makeText(getApplicationContext(), "Connector Badge nicht gefunden.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                                }
                                // [END get_text]
                                // [END_EXCLUDE]
                            }
                        })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w("LINE-->", e.getMessage());
                                        // Task failed with an exception
                                        // ...
                                    }
                                });
        // [END run_detector]
    }

    private void sendlog(String lli) {
        //Crashlytics.log(Log.DEBUG, "tag", lli);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}