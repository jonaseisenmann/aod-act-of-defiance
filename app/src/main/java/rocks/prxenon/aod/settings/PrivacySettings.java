package rocks.prxenon.aod.settings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.basics.AODConfig;
import rocks.prxenon.aod.basics.AgentData;

import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.mbgdpr;
import static rocks.prxenon.aod.basics.AODConfig.user;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class PrivacySettings extends AppCompatPreferenceActivity {

    public static final int REQUEST_CODE = 892;
    static String keygdpr;
    private static PreferenceScreen mainprevview;
    private static ListPreference profile_visible;
    private static Preference gdpr_key;
    private static SharedPreferences prefs;
    private static Context ctx;
    private static Activity atx;
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        int index;

        @Override
        public boolean onPreferenceChange(Preference preference, final Object value) {
            Log.w("CHANGE -->", preference.getKey() + " " + value.toString());
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;

                index = listPreference.findIndexOfValue(stringValue);

                if (preference.getKey().equalsIgnoreCase("profile_visible")) {

                    Log.w("VALUE --->", Integer.parseInt(value.toString()) + " ." + value.toString());

                    final Map<String, Object> privUpdates = new HashMap<>();


                    privUpdates.put("/privacy", Integer.parseInt(value.toString()));

                    mDatabase.child(user.getUid()).updateChildren(privUpdates);
                    new AgentData("privacystatus", Integer.parseInt(value.toString()));

                }

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                if (preference.getKey().equalsIgnoreCase("workaround")) {

                } else {
                    preference.setSummary(stringValue);
                }
            }
            return true;
        }
    };
    private static AlertDialog.Builder builderkey;
    private static boolean diaopen;
    private static AlertDialog builderdialog;
    private Typeface type, proto;
    private TextView mTitleTextView;

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), String.valueOf(AgentData.privacystatus)));
    }

    private static void bindPreferenceSummaryToValue2(CheckBoxPreference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getBoolean(preference.getKey(), false));
    }

    private static void creategdpdr() {
        final String uuidis = user.getUid();
        final String currentAgentname = AgentData.getagentname();

        //StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
        builderkey = new AlertDialog.Builder(ctx, R.style.CustomDialog);
        diaopen = true;
        // builder.setMessage("Text not finished...");
        LayoutInflater inflater = atx.getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_downloadkey, null);
        builderkey.setView(dialog);
        final TextView input = dialog.findViewById(R.id.gdprkey);

        mbgdpr.child(keygdpr).setValue(user.getUid()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    //finish();


                    input.setText(keygdpr);

                } else if (task.isCanceled()) {
                    input.setText("error");

                    Log.w("ERROR ", this.getClass().getSimpleName() + " 605 Login abgebrochen");

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                input.setText("error");
                Log.w("ERROR ", this.getClass().getSimpleName() + " 606 Login error " + e.getMessage());

            }
        });


        builderkey.setPositiveButton("invalidate", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                mbgdpr.child(keygdpr).setValue(null);
                diaopen = false;
                atx.recreate();
                // Todo publish the code to database
            }
        });

        builderkey.setCancelable(false);

        if (!atx.isFinishing()) {
            builderkey.create().show();

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");

        prefs = getSharedPreferences("AppPref", MODE_PRIVATE);
        ctx = getApplicationContext();
        atx = PrivacySettings.this;
        keygdpr = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();


        setupActionBar();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_settings, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(getResources().getString(R.string.pref_privacy_tit));
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(25);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!super.onMenuItemSelected(featureId, item)) {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    @Override
    public void onBackPressed() {
        // REMOVE QR FROM FIREBASE

        startActivity(new Intent(PrivacySettings.this, MainSettings.class));
        finish();
        // new API("qrcloser", this).tempscanCoderemove(user.getEmail(),theqrstring, nameofagent);

        //  super.onBackPressed();


    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("RESUME PRIC -->", "woop");


        mbgdpr.child(keygdpr).setValue(null);
    }

    @Override
    public void onStop() {
        super.onStop();

        mbgdpr.child(keygdpr).setValue(null);
        startActivity(new Intent(PrivacySettings.this, MainSettings.class));
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mbgdpr.child(keygdpr).setValue(null);

    }

    @Override
    public void onPause() {
        super.onPause();

        mbgdpr.child(keygdpr).setValue(null);

        finish();
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class MyPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_privacy);
            setHasOptionsMenu(true);
            ctx = getContext();
            atx = getActivity();
            new AODConfig("init", ctx);
            profile_visible = (ListPreference) findPreference("profile_visible");


            prefs = getActivity().getSharedPreferences("AppPref", MODE_PRIVATE);

            gdpr_key = findPreference("gdpr_key");

            gdpr_key.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    creategdpdr();

                    return true;
                }
            });
            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.


            mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (!snapshot.child("privacy").exists()) {
                        final Map<String, Object> privUpdates = new HashMap<>();

                        privUpdates.put("/privacy", 0);

                        mDatabase.child(user.getUid()).updateChildren(privUpdates);

                        SharedPreferences prefssettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        prefssettings.edit().putString("profile_visible", "0").apply();
                        new AgentData("privacystatus", 0);
                        bindPreferenceSummaryToValue(findPreference("profile_visible"));
                        profile_visible.setValue("0");
                    } else {
                        int index = profile_visible.findIndexOfValue(snapshot.child("privacy").getValue().toString());
                        Log.i("VALUE SET INIT -->", "" + snapshot.child("privacy").getValue().toString());
                        profile_visible.setValue(snapshot.child("privacy").getValue().toString());
                        // Set the summary to reflect the new value.
                        profile_visible.setSummary(
                                index >= 0
                                        ? profile_visible.getEntries()[index]
                                        : null);
                        profile_visible.setValue(snapshot.child("privacy").getValue().toString());
                        new AgentData("privacystatus", Integer.parseInt(snapshot.child("privacy").getValue().toString()));
                        profile_visible.setValue(snapshot.child("privacy").getValue().toString());
                        bindPreferenceSummaryToValue(findPreference("profile_visible"));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    bindPreferenceSummaryToValue(findPreference("profile_visible"));
                    //  StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_sometningwrong), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                    //  Log.i("Error DB -->", this.getClass().getSimpleName() + " 407 " + databaseError.getMessage().toString());
                }
            });


            mainprevview = getPreferenceScreen();


        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), MainSettings.class));
                atx.finish();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
//   mbgdpr.child(user.getUid()).setValue(null);
}
