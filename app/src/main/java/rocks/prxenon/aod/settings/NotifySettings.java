package rocks.prxenon.aod.settings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;

import com.google.firebase.messaging.FirebaseMessaging;

import rocks.prxenon.aod.R;
import rocks.prxenon.aod.SplashScreen;
import rocks.prxenon.aod.basics.AODConfig;

import static rocks.prxenon.aod.basics.AODConfig.user;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class NotifySettings extends AppCompatPreferenceActivity {

    public static final int REQUEST_CODE = 892;

    private static PreferenceScreen notifyprefsview;
    private static Preference notify_reset, notify_open;
    private static PreferenceCategory notify_settings;
    private static SwitchPreference reminderweek_monday;
    private static SharedPreferences prefs;
    private static Context ctx;
    private static Activity atx;
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        int index;

        @Override
        public boolean onPreferenceChange(Preference preference, final Object value) {
            Log.w("CHANGE -->", preference.getKey() + " " + value.toString());
            String stringValue = value.toString();

            if (preference instanceof SwitchPreference) {


                prefs.edit().putBoolean(preference.getKey(), Boolean.parseBoolean(value.toString())).apply();

            } else {

                preference.setSummary(stringValue);

            }
            return true;
        }
    };

    private Typeface type, proto;
    private TextView mTitleTextView;


    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getBoolean(preference.getKey(), false));

        if (preference.getKey().equalsIgnoreCase("agent_channel")) {
            if (PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                    .getBoolean(preference.getKey(), false)) {
                FirebaseMessaging.getInstance().subscribeToTopic("privat_" + user.getUid());

            } else {
                FirebaseMessaging.getInstance().unsubscribeFromTopic("privat_" + user.getUid());

            }


        }
        if (preference.getKey().equalsIgnoreCase("reminderweek_monday")) {
            if (PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                    .getBoolean(preference.getKey(), false)) {
                FirebaseMessaging.getInstance().subscribeToTopic("reminderweek_monday");

            } else {
                FirebaseMessaging.getInstance().unsubscribeFromTopic("reminderweek_monday");

            }
        }

        if (preference.getKey().equalsIgnoreCase("reminderweek_sunday")) {
            if (PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                    .getBoolean(preference.getKey(), false)) {
                FirebaseMessaging.getInstance().subscribeToTopic("reminderweek_sunday");
            } else {
                FirebaseMessaging.getInstance().unsubscribeFromTopic("reminderweek_sunday");

            }
        }

        if (preference.getKey().equalsIgnoreCase("event_channel")) {
            if (PreferenceManager.getDefaultSharedPreferences(preference.getContext())
                    .getBoolean(preference.getKey(), false)) {
                FirebaseMessaging.getInstance().subscribeToTopic("event_channel");
            } else {
                FirebaseMessaging.getInstance().unsubscribeFromTopic("event_channel");

            }
        }


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs = getSharedPreferences("AppPref", MODE_PRIVATE);
        ctx = getApplicationContext();
        atx = NotifySettings.this;


        setupActionBar();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_settings, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText(getResources().getString(R.string.pref_notify_tit));
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);

        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(21);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!super.onMenuItemSelected(featureId, item)) {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    @Override
    public void onBackPressed() {
        // REMOVE QR FROM FIREBASE

        startActivity(new Intent(NotifySettings.this, MainSettings.class));
        finish();
        // new API("qrcloser", this).tempscanCoderemove(user.getEmail(),theqrstring, nameofagent);

        //  super.onBackPressed();


    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("RESUME PRIC -->", "woop");


    }

    @Override
    public void onStop() {
        super.onStop();


        //startActivity(new Intent(NotifySettings.this, MainSettings.class));
        //finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }

    @Override
    public void onPause() {
        super.onPause();


        //finish();
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class MyPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_notify);
            setHasOptionsMenu(true);
            ctx = getContext();
            atx = getActivity();
            new AODConfig("init", ctx);
            notifyprefsview = getPreferenceScreen();
            notify_reset = findPreference("notify_reset");

            notify_settings = (PreferenceCategory) findPreference("notify_settings");
            notify_open = findPreference("notify_open");
            prefs = PreferenceManager.getDefaultSharedPreferences(atx);


            // notifyprefsview.removePreference(notify_settings);
            notify_open.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    Intent intent = new Intent();
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
                        intent.putExtra(Settings.EXTRA_APP_PACKAGE, ctx.getPackageName());
                        intent.putExtra(Settings.EXTRA_CHANNEL_ID, "aod_default_channel");
                    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                        intent.putExtra("app_package", ctx.getPackageName());
                        intent.putExtra("app_uid", ctx.getApplicationInfo().uid);
                    } else {
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setData(Uri.parse("package:" + ctx.getPackageName()));
                    }

                    ctx.startActivity(intent);


                    return true;
                }
            });


            notify_reset.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    FirebaseMessaging.getInstance().unsubscribeFromTopic("reminderweek_monday");
                    FirebaseMessaging.getInstance().unsubscribeFromTopic("reminderweek_sunday");
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(" event_channel");

                    FirebaseMessaging.getInstance().unsubscribeFromTopic("privat_" + user.getUid());
                    FirebaseMessaging.getInstance().subscribeToTopic("aod_news");
                    prefs.edit().putBoolean("notifyset", false).apply();
                    prefs.edit().putBoolean("notifysetpvp", false).apply();
                    prefs.edit().remove("reminderweek_monday").apply();
                    prefs.edit().remove("reminderweek_sunday").apply();
                    prefs.edit().remove("event_channel").apply();

                    prefs.edit().remove("agent_channel").apply();
                    prefs.edit().remove("aod_news").apply();
                    Intent i = new Intent(atx, SplashScreen.class);
                    i.putExtra("login", true);
                    atx.startActivity(i);
                    atx.finish();
                    return true;
                }
            });
            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.


            bindPreferenceSummaryToValue(findPreference("reminderweek_monday"));
            bindPreferenceSummaryToValue(findPreference("reminderweek_sunday"));
            bindPreferenceSummaryToValue(findPreference("agent_channel"));
            bindPreferenceSummaryToValue(findPreference("event_channel"));

            bindPreferenceSummaryToValue(findPreference("aod_news"));


        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), MainSettings.class));
                atx.finish();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }
//   mbgdpr.child(user.getUid()).setValue(null);
}
