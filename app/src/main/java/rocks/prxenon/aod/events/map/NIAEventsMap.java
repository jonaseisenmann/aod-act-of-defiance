package rocks.prxenon.aod.events.map;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.github.siyamed.shapeimageview.OctogonImageView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerView;
import com.mapbox.mapboxsdk.plugins.markerview.MarkerViewManager;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.muddzdev.styleabletoast.StyleableToast;

import org.apmem.tools.layouts.FlowLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import de.hdodenhof.circleimageview.CircleImageView;
import rocks.prxenon.aod.BuildConfig;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.SelectCountry;
import rocks.prxenon.aod.adapter.SelectType;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.helper.PicassoCache;

import static rocks.prxenon.aod.basics.AODConfig.mBevents;
import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;

public class NIAEventsMap extends AppCompatActivity implements SearchView.OnClickListener, OnMapReadyCallback {


    public static final HashMap<String, LatLng> COUNTRY_ISOS = new HashMap<String, LatLng>() {{
        put("AD", new LatLng(42.546245, 1.601554));
        put("AE", new LatLng(23.424076, 53.847818));
        put("AF", new LatLng(33.93911, 67.709953));
        put("AG", new LatLng(17.060816, -61.796428));
        put("AI", new LatLng(18.220554, -63.068615));
        put("AL", new LatLng(41.153332, 20.168331));
        put("AM", new LatLng(40.069099, 45.038189));
        put("AN", new LatLng(12.226079, -69.060087));
        put("AO", new LatLng(-11.202692, 17.873887));
        put("AQ", new LatLng(-75.250973, -0.071389));
        put("AR", new LatLng(-38.416097, -63.616672));
        put("AS", new LatLng(-14.270972, -170.132217));
        put("AT", new LatLng(47.516231, 14.550072));
        put("AU", new LatLng(-25.274398, 133.775136));
        put("AW", new LatLng(12.52111, -69.968338));
        put("AZ", new LatLng(40.143105, 47.576927));
        put("BA", new LatLng(43.915886, 17.679076));
        put("BB", new LatLng(13.193887, -59.543198));
        put("BD", new LatLng(23.684994, 90.356331));
        put("BE", new LatLng(50.503887, 4.469936));
        put("BF", new LatLng(12.238333, -1.561593));
        put("BG", new LatLng(42.733883, 25.48583));
        put("BH", new LatLng(25.930414, 50.637772));
        put("BI", new LatLng(-3.373056, 29.918886));
        put("BJ", new LatLng(9.30769, 2.315834));
        put("BM", new LatLng(32.321384, -64.75737));
        put("BN", new LatLng(4.535277, 114.727669));
        put("BO", new LatLng(-16.290154, -63.588653));
        put("BR", new LatLng(-14.235004, -51.92528));
        put("BS", new LatLng(25.03428, -77.39628));
        put("BT", new LatLng(27.514162, 90.433601));
        put("BV", new LatLng(-54.423199, 3.413194));
        put("BW", new LatLng(-22.328474, 24.684866));
        put("BY", new LatLng(53.709807, 27.953389));
        put("BZ", new LatLng(17.189877, -88.49765));
        put("CA", new LatLng(56.130366, -106.346771));
        put("CC", new LatLng(-12.164165, 96.870956));
        put("CD", new LatLng(-4.038333, 21.758664));
        put("CF", new LatLng(6.611111, 20.939444));
        put("CG", new LatLng(-0.228021, 15.827659));
        put("CH", new LatLng(46.818188, 8.227512));
        put("CI", new LatLng(7.539989, -5.54708));
        put("CK", new LatLng(-21.236736, -159.777671));
        put("CL", new LatLng(-35.675147, -71.542969));
        put("CM", new LatLng(7.369722, 12.354722));
        put("CN", new LatLng(35.86166, 104.195397));
        put("CO", new LatLng(4.570868, -74.297333));
        put("CR", new LatLng(9.748917, -83.753428));
        put("CU", new LatLng(21.521757, -77.781167));
        put("CV", new LatLng(16.002082, -24.013197));
        put("CX", new LatLng(-10.447525, 105.690449));
        put("CY", new LatLng(35.126413, 33.429859));
        put("CZ", new LatLng(49.817492, 15.472962));
        put("DE", new LatLng(51.165691, 10.451526));
        put("DJ", new LatLng(11.825138, 42.590275));
        put("DK", new LatLng(56.26392, 9.501785));
        put("DM", new LatLng(15.414999, -61.370976));
        put("DO", new LatLng(18.735693, -70.162651));
        put("DZ", new LatLng(28.033886, 1.659626));
        put("EC", new LatLng(-1.831239, -78.183406));
        put("EE", new LatLng(58.595272, 25.013607));
        put("EG", new LatLng(26.820553, 30.802498));
        put("EH", new LatLng(24.215527, -12.885834));
        put("ER", new LatLng(15.179384, 39.782334));
        put("ES", new LatLng(40.463667, -3.74922));
        put("ET", new LatLng(9.145, 40.489673));
        put("FI", new LatLng(61.92411, 25.748151));
        put("FJ", new LatLng(-16.578193, 179.414413));
        put("FK", new LatLng(-51.796253, -59.523613));
        put("FM", new LatLng(7.425554, 150.550812));
        put("FO", new LatLng(61.892635, -6.911806));
        put("FR", new LatLng(46.227638, 2.213749));
        put("GA", new LatLng(-0.803689, 11.609444));
        put("GB", new LatLng(55.378051, -3.435973));
        put("GD", new LatLng(12.262776, -61.604171));
        put("GE", new LatLng(42.315407, 43.356892));
        put("GF", new LatLng(3.933889, -53.125782));
        put("GG", new LatLng(49.465691, -2.585278));
        put("GH", new LatLng(7.946527, -1.023194));
        put("GI", new LatLng(36.137741, -5.345374));
        put("GL", new LatLng(71.706936, -42.604303));
        put("GM", new LatLng(13.443182, -15.310139));
        put("GN", new LatLng(9.945587, -9.696645));
        put("GP", new LatLng(16.995971, -62.067641));
        put("GQ", new LatLng(1.650801, 10.267895));
        put("GR", new LatLng(39.074208, 21.824312));
        put("GS", new LatLng(-54.429579, -36.587909));
        put("GT", new LatLng(15.783471, -90.230759));
        put("GU", new LatLng(13.444304, 144.793731));
        put("GW", new LatLng(11.803749, -15.180413));
        put("GY", new LatLng(4.860416, -58.93018));
        put("GZ", new LatLng(31.354676, 34.308825));
        put("HK", new LatLng(22.396428, 114.109497));
        put("HM", new LatLng(-53.08181, 73.504158));
        put("HN", new LatLng(15.199999, -86.241905));
        put("HR", new LatLng(45.1, 15.2));
        put("HT", new LatLng(18.971187, -72.285215));
        put("HU", new LatLng(47.162494, 19.503304));
        put("ID", new LatLng(-0.789275, 113.921327));
        put("IE", new LatLng(53.41291, -8.24389));
        put("IL", new LatLng(31.046051, 34.851612));
        put("IM", new LatLng(54.236107, -4.548056));
        put("IN", new LatLng(20.593684, 78.96288));
        put("IO", new LatLng(-6.343194, 71.876519));
        put("IQ", new LatLng(33.223191, 43.679291));
        put("IR", new LatLng(32.427908, 53.688046));
        put("IS", new LatLng(64.963051, -19.020835));
        put("IT", new LatLng(41.87194, 12.56738));
        put("JE", new LatLng(49.214439, -2.13125));
        put("JM", new LatLng(18.109581, -77.297508));
        put("JO", new LatLng(30.585164, 36.238414));
        put("JP", new LatLng(36.204824, 138.252924));
        put("KE", new LatLng(-0.023559, 37.906193));
        put("KG", new LatLng(41.20438, 74.766098));
        put("KH", new LatLng(12.565679, 104.990963));
        put("KI", new LatLng(-3.370417, -168.734039));
        put("KM", new LatLng(-11.875001, 43.872219));
        put("KN", new LatLng(17.357822, -62.782998));
        put("KP", new LatLng(40.339852, 127.510093));
        put("KR", new LatLng(35.907757, 127.766922));
        put("KW", new LatLng(29.31166, 47.481766));
        put("KY", new LatLng(19.513469, -80.566956));
        put("KZ", new LatLng(48.019573, 66.923684));
        put("LA", new LatLng(19.85627, 102.495496));
        put("LB", new LatLng(33.854721, 35.862285));
        put("LC", new LatLng(13.909444, -60.978893));
        put("LI", new LatLng(47.166, 9.555373));
        put("LK", new LatLng(7.873054, 80.771797));
        put("LR", new LatLng(6.428055, -9.429499));
        put("LS", new LatLng(-29.609988, 28.233608));
        put("LT", new LatLng(55.169438, 23.881275));
        put("LU", new LatLng(49.815273, 6.129583));
        put("LV", new LatLng(56.879635, 24.603189));
        put("LY", new LatLng(26.3351, 17.228331));
        put("MA", new LatLng(31.791702, -7.09262));
        put("MC", new LatLng(43.750298, 7.412841));
        put("MD", new LatLng(47.411631, 28.369885));
        put("ME", new LatLng(42.708678, 19.37439));
        put("MG", new LatLng(-18.766947, 46.869107));
        put("MH", new LatLng(7.131474, 171.184478));
        put("MK", new LatLng(41.608635, 21.745275));
        put("ML", new LatLng(17.570692, -3.996166));
        put("MM", new LatLng(21.913965, 95.956223));
        put("MN", new LatLng(46.862496, 103.846656));
        put("MO", new LatLng(22.198745, 113.543873));
        put("MP", new LatLng(17.33083, 145.38469));
        put("MQ", new LatLng(14.641528, -61.024174));
        put("MR", new LatLng(21.00789, -10.940835));
        put("MS", new LatLng(16.742498, -62.187366));
        put("MT", new LatLng(35.937496, 14.375416));
        put("MU", new LatLng(-20.348404, 57.552152));
        put("MV", new LatLng(3.202778, 73.22068));
        put("MW", new LatLng(-13.254308, 34.301525));
        put("MX", new LatLng(23.634501, -102.552784));
        put("MY", new LatLng(4.210484, 101.975766));
        put("MZ", new LatLng(-18.665695, 35.529562));
        put("NA", new LatLng(-22.95764, 18.49041));
        put("NC", new LatLng(-20.904305, 165.618042));
        put("NE", new LatLng(17.607789, 8.081666));
        put("NF", new LatLng(-29.040835, 167.954712));
        put("NG", new LatLng(9.081999, 8.675277));
        put("NI", new LatLng(12.865416, -85.207229));
        put("NL", new LatLng(52.132633, 5.291266));
        put("NO", new LatLng(60.472024, 8.468946));
        put("NP", new LatLng(28.394857, 84.124008));
        put("NR", new LatLng(-0.522778, 166.931503));
        put("NU", new LatLng(-19.054445, -169.867233));
        put("NZ", new LatLng(-40.900557, 174.885971));
        put("OM", new LatLng(21.512583, 55.923255));
        put("PA", new LatLng(8.537981, -80.782127));
        put("PE", new LatLng(-9.189967, -75.015152));
        put("PF", new LatLng(-17.679742, -149.406843));
        put("PG", new LatLng(-6.314993, 143.95555));
        put("PH", new LatLng(12.879721, 121.774017));
        put("PK", new LatLng(30.375321, 69.345116));
        put("PL", new LatLng(51.919438, 19.145136));
        put("PM", new LatLng(46.941936, -56.27111));
        put("PN", new LatLng(-24.703615, -127.439308));
        put("PR", new LatLng(18.220833, -66.590149));
        put("PS", new LatLng(31.952162, 35.233154));
        put("PT", new LatLng(39.399872, -8.224454));
        put("PW", new LatLng(7.51498, 134.58252));
        put("PY", new LatLng(-23.442503, -58.443832));
        put("QA", new LatLng(25.354826, 51.183884));
        put("RE", new LatLng(-21.115141, 55.536384));
        put("RO", new LatLng(45.943161, 24.96676));
        put("RS", new LatLng(44.016521, 21.005859));
        put("RU", new LatLng(61.52401, 105.318756));
        put("RW", new LatLng(-1.940278, 29.873888));
        put("SA", new LatLng(23.885942, 45.079162));
        put("SB", new LatLng(-9.64571, 160.156194));
        put("SC", new LatLng(-4.679574, 55.491977));
        put("SD", new LatLng(12.862807, 30.217636));
        put("SE", new LatLng(60.128161, 18.643501));
        put("SG", new LatLng(1.352083, 103.819836));
        put("SH", new LatLng(-24.143474, -10.030696));
        put("SI", new LatLng(46.151241, 14.995463));
        put("SJ", new LatLng(77.553604, 23.670272));
        put("SK", new LatLng(48.669026, 19.699024));
        put("SL", new LatLng(8.460555, -11.779889));
        put("SM", new LatLng(43.94236, 12.457777));
        put("SN", new LatLng(14.497401, -14.452362));
        put("SO", new LatLng(5.152149, 46.199616));
        put("SR", new LatLng(3.919305, -56.027783));
        put("ST", new LatLng(0.18636, 6.613081));
        put("SV", new LatLng(13.794185, -88.89653));
        put("SY", new LatLng(34.802075, 38.996815));
        put("SZ", new LatLng(-26.522503, 31.465866));
        put("TC", new LatLng(21.694025, -71.797928));
        put("TD", new LatLng(15.454166, 18.732207));
        put("TF", new LatLng(-49.280366, 69.348557));
        put("TG", new LatLng(8.619543, 0.824782));
        put("TH", new LatLng(15.870032, 100.992541));
        put("TJ", new LatLng(38.861034, 71.276093));
        put("TK", new LatLng(-8.967363, -171.855881));
        put("TL", new LatLng(-8.874217, 125.727539));
        put("TM", new LatLng(38.969719, 59.556278));
        put("TN", new LatLng(33.886917, 9.537499));
        put("TO", new LatLng(-21.178986, -175.198242));
        put("TR", new LatLng(38.963745, 35.243322));
        put("TT", new LatLng(10.691803, -61.222503));
        put("TV", new LatLng(-7.109535, 177.64933));
        put("TW", new LatLng(23.69781, 120.960515));
        put("TZ", new LatLng(-6.369028, 34.888822));
        put("UA", new LatLng(48.379433, 31.16558));
        put("UG", new LatLng(1.373333, 32.290275));
        put("US", new LatLng(37.09024, -95.712891));
        put("UY", new LatLng(-32.522779, -55.765835));
        put("UZ", new LatLng(41.377491, 64.585262));
        put("VA", new LatLng(41.902916, 12.453389));
        put("VC", new LatLng(12.984305, -61.287228));
        put("VE", new LatLng(6.42375, -66.58973));
        put("VG", new LatLng(18.420695, -64.639968));
        put("VI", new LatLng(18.335765, -64.896335));
        put("VN", new LatLng(14.058324, 108.277199));
        put("VU", new LatLng(-15.376706, 166.959158));
        put("WF", new LatLng(-13.768752, -177.156097));
        put("WS", new LatLng(-13.759029, -172.104629));
        put("XK", new LatLng(42.602636, 20.902977));
        put("YE", new LatLng(15.552727, 48.516388));
        put("YT", new LatLng(-12.8275, 45.166244));
        put("ZA", new LatLng(-30.559482, 22.937506));
        put("ZM", new LatLng(-13.133897, 27.849332));
        put("ZW", new LatLng(-19.015438, 29.154857));
    }};
    public static List<String> ispocidlist = new ArrayList<>();
    public static List<String> distinctcountry = new ArrayList<>();
    public static List<String> distinctregio = new ArrayList<>();
    public static List<String> distincttype = new ArrayList<>();
    public static List<String> distinctcity = new ArrayList<>();
    public static List<String> selectcountry = new ArrayList<String>();
    public static List<String> selecteventtype = new ArrayList<String>();
    public static List<String> distinctcountryemea = new ArrayList<>();
    public static List<String> distinctcountryamer = new ArrayList<>();
    public static List<String> distinctcountryapec = new ArrayList<>();
    // public static List<String> distinctcountryapec = new ArrayList<>();
    public static ImageButton btn_ispoc;
    final List<String> eventid = new ArrayList();
    final List<Boolean> eventstatus = new ArrayList();
    final List<String> eventcity = new ArrayList();
    final List<String> eventlocation = new ArrayList();
    final List<String> eventtype = new ArrayList();
    final List<String> eventimage = new ArrayList();
    final List<String> eventday = new ArrayList();
    final List<String> eventmonth = new ArrayList();
    final List<String> eventyear = new ArrayList();
    final List<String> eventlat = new ArrayList();
    final List<String> eventlon = new ArrayList();
    final List<String> eventregio = new ArrayList();
    final List<String> eventcountry = new ArrayList<>();
    final List<String> alleventcountry = new ArrayList<>();
    final List<String> alleventtype = new ArrayList<>();
    final List<Long> eventstringtime = new ArrayList<>();
    final List<String> eventurl = new ArrayList<>();
    AlertDialog dialogcountry, dialogtype;
    int anzahl = 0;
    private boolean readyToPurchase = false;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;
    private Typeface type, proto;
    private TextView mTitleTextView;
    private TextView agentname;
    private CircleImageView agentimg;
    private SelectCountry adapter;
    private SelectType adaptertype;
    private boolean amerset, emeaset, apacset;
    private MapView mapView;
    private MapboxMap mapboxMap;
    private GeoJsonSource source;
    private FeatureCollection featureCollection;
    private MarkerView markerView;
    private MarkerViewManager markerViewManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, BuildConfig.MAPBOXKEY);

        setContentView(R.layout.activity_niaevents_map);

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);


        mapView = findViewById(R.id.mapView);


        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);


        new AgentData();
        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");


        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);


        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_events, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText("NIA EVENTS MAP");

        btn_ispoc = actionBar.findViewById(R.id.btn_omap);
        btn_ispoc.setVisibility(View.GONE);


        agentname = findViewById(R.id.agentname);
        agentimg = findViewById(R.id.agentimage);
        agentname.setTypeface(proto);


        agentname.setText(AgentData.getagentname());
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);


        String locale = getResources().getConfiguration().locale.getCountry();

        Log.i("COUNTRYCODE ", " " + locale);

        // initevents(AgentData.gethasfilter(), AgentData.gethasfiltercountry(), AgentData.gethasfiltertype(), AgentData.gethasfilterregio(), AgentData.getfilterdatacountry(), null, null);

    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;
        mapboxMap.setStyle(new Style.Builder().fromUri(BuildConfig.MAPBOXURL), new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(@NonNull Style style) {


                markerViewManager = new MarkerViewManager(mapView, mapboxMap);

                // COUNTRY_ISOS.get(getResources().getConfiguration().locale.getCountry())
                if (COUNTRY_ISOS.containsKey(getResources().getConfiguration().locale.getCountry())) {
                    mapboxMap.setMinZoomPreference(4);
                    mapboxMap.setMaxZoomPreference(14);
                    mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(COUNTRY_ISOS.get(getResources().getConfiguration().locale.getCountry())), 6));
                }


            }
        });
    }


    private void initevents(final Boolean filteraktiv, final Boolean filtercountry, final Boolean filterart, Boolean filterregio, final List<String> country, final List<String> art, List<String> regio) {


        //Log.i("resume-->", " "+country.toString());
        boolean ispocornot = false;

        eventcity.clear();
        eventcountry.clear();
        alleventcountry.clear();
        alleventtype.clear();
        eventday.clear();
        eventid.clear();
        eventimage.clear();
        eventlat.clear();
        eventlocation.clear();
        eventlon.clear();
        eventmonth.clear();
        eventregio.clear();
        eventstatus.clear();
        eventtype.clear();
        eventurl.clear();
        eventyear.clear();
        eventstringtime.clear();
        ispocidlist.clear();
        distinctcity.clear();
        distinctcountry.clear();
        distinctregio.clear();
        distincttype.clear();
        distinctcountryemea.clear();
        distinctcountryamer.clear();
        distinctcountryapec.clear();


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int newwidth = ((size.x) / 9) - 10;

        final LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        mBevents.child("events").orderByChild("stringtime").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshotevents) {

                for (final DataSnapshot childevents : snapshotevents.getChildren()) {

                    // TODO CRASH IF failed
                    if (Boolean.parseBoolean(childevents.child("active").getValue().toString())) {

                        if (childevents.child("type").getValue().toString().equalsIgnoreCase("First Saturday")) {

                            if (childevents.child("details").exists()) {
                                //ispocornot
                                if (childevents.child("details").child("enl_poc").getValue().toString().equalsIgnoreCase(AgentData.getagentname()) || childevents.child("details").child("res_poc").getValue().toString().equalsIgnoreCase(AgentData.getagentname())) {
                                    ispocidlist.add(childevents.child("eventid").getValue().toString());


                                }
                            }
                        }


                        if (childevents.hasChild("latlon")) {

                            boolean hasmore = false;
                            if (childevents.child("latlon").getValue().toString().contains(",")) {
                                String[] separated = childevents.child("latlon").getValue().toString().split(",");
                                if (separated.length > 0) {

                                    hasmore = eventlat.contains(childevents.child("latlon").getValue().toString()) || eventcity.contains(childevents.child("city").getValue().toString());

                                    Double lat = Double.parseDouble(separated[0]); // this will contain "Fruit"
                                    Double lng = Double.parseDouble(separated[1]);

                                    View view = inflater.inflate(R.layout.events_marker_adapter, null);
                                    OctogonImageView imageView = view.findViewById(R.id.eventimage);
                                    view.setLayoutParams(new RelativeLayout.LayoutParams(newwidth, newwidth));

                                    final TextView eventname = view.findViewById(R.id.eventname);

                                    eventname.setText(childevents.child("type").getValue().toString() + " | " + childevents.child("city").getValue().toString());
                                    eventname.setSelected(true);


                                    try {
                                        PicassoCache.getPicassoInstance(getApplicationContext())
                                                .load(childevents.child("img").getValue().toString())
                                                .placeholder(R.drawable.full_logo)
                                                .error(R.drawable.full_logo)

                                                // .memoryPolicy(MemoryPolicy.NO_CACHE )
                                                // .networkPolicy(NetworkPolicy.NO_CACHE)
                                                //.transform(new CropRoundTransformation())
                                                .into(imageView);
                                        // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
                                    } catch (Exception e) {
                                        //Log.e("Error", e.getMessage());

                                        e.printStackTrace();
                                    }


                                    imageView.setBorderWidth(5);
                                    imageView.setBorderColor(getColor(R.color.colorAccent));

                                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                    imageView.setPadding(8, 8, 8, 8);


                                    /*
                                      if (eventlat.contains(lat - 0.035 + "," + lng + 0.035)) {
                                                eventlat.add(lat + 0.035 + "," + (lng - 0.035));
                                                markerView = new MarkerView(new LatLng(lat + 0.035, lng - 0.035), view);
                                            }
                                            else {
                                                eventlat.add(lat + 0.025 + "," + lng + 0.025);
                                                markerView = new MarkerView(new LatLng(lat + 0.025, lng + 0.025), view);
                                            }
                                     */

                                    if (hasmore) {

                                        if (eventlat.contains(lat + 0.025 + "," + lng + 0.025)) {

                                            if (eventlat.contains(lat - 0.035 + "," + lng + 0.035)) {
                                                if (eventlat.contains(lat + 0.035 + "," + (lng - 0.035))) {
                                                    eventlat.add(lat - 0.025 + "," + (lng + 0.015));
                                                    markerView = new MarkerView(new LatLng(lat - 0.025, lng + 0.015), view);
                                                } else {
                                                    eventlat.add(lat + 0.035 + "," + (lng - 0.035));
                                                    markerView = new MarkerView(new LatLng(lat + 0.035, lng - 0.035), view);
                                                }
                                            } else {
                                                eventlat.add(lat - 0.035 + "," + lng + 0.035);
                                                markerView = new MarkerView(new LatLng(lat - 0.035, lng + 0.035), view);
                                            }
                                        } else {

                                            eventlat.add(lat + 0.025 + "," + lng + 0.025);
                                            markerView = new MarkerView(new LatLng(lat + 0.025, lng + 0.025), view);


                                        }
                                    } else {
                                        eventlat.add(childevents.child("latlon").getValue().toString());
                                        markerView = new MarkerView(new LatLng(lat, lng), view);

                                    }


                                    view.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {


                                            if (childevents.child("url").exists()) {


                                                String url = childevents.child("url").getValue().toString();
                                                if (url.equalsIgnoreCase("null")) {

                                                } else {
                                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                                    i.setData(Uri.parse(url));
                                                    startActivity(i);
                                                }
                                                Log.i("URL-->", "" + childevents.child("eventid").getValue().toString());

                                            } else {
                                                Log.i("URL-->", "nourl");

                                            }

                                        }
                                    });

                                    // markerView = new MarkerView(new LatLng(lat, lng), view);
                                    markerViewManager.addMarker(markerView);


                                    Log.i("TEST LOG--->", "" + lat + " " + lng + "hasmore -> " + hasmore);
                                } else {
                                    Log.i("NO LAT LON-->", "" + childevents.child("latlon").getValue().toString());
                                    eventlat.add("0,0");
                                }
                            } else {
                                Log.i("NO LAT LON-->", "" + childevents.child("latlon").getValue().toString());
                                eventlat.add("0,0");
                            }
                        } else {
                            eventlat.add("0,0");
                        }
                        eventid.add(childevents.child("eventid").getValue().toString());
                        eventlocation.add(childevents.child("location").getValue().toString());
                        eventimage.add(childevents.child("img").getValue().toString());
                        eventregio.add(childevents.child("regio").getValue().toString());
                        eventtype.add(childevents.child("type").getValue().toString());
                        if (childevents.child("url").exists()) {
                            eventurl.add(childevents.child("url").getValue().toString());
                        } else {
                            eventurl.add("no url");
                        }
                        eventcity.add(childevents.child("city").getValue().toString());
                        if (childevents.child("country").exists()) {
                            eventcountry.add(childevents.child("country").getValue().toString());
                            alleventcountry.add(childevents.child("country").getValue().toString());
                        } else {
                            eventcountry.add("UNKNOWN");
                            alleventcountry.add("UNKNOWN");
                        }

                        alleventtype.add(childevents.child("type").getValue().toString());
                        if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                            distinctcountryamer.add(childevents.child("regio").getValue().toString());
                        }
                        if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                            distinctcountryemea.add(childevents.child("regio").getValue().toString());
                        }
                        if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                            distinctcountryapec.add(childevents.child("regio").getValue().toString());
                        }
                        eventstatus.add(Boolean.parseBoolean(childevents.child("active").getValue().toString()));
                        long dv = Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000;// its need to be in milisecond
                        eventstringtime.add(Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000);
                        Date df = new Date(dv);

                        String day = new SimpleDateFormat("dd", Locale.US).format(df);
                        String month = new SimpleDateFormat("MM", Locale.US).format(df);
                        String year = new SimpleDateFormat("yyyy", Locale.US).format(df);

                        eventday.add(day);
                        eventmonth.add(month);
                        eventyear.add(year);

                        Log.i("DATE DAY -->", day);
                        Log.i("DATE MONTH -->", month);
                        Log.i("DATE YEAR -->", year);
                        Log.i("REGIO-->", childevents.child("regio").getValue().toString());


                    }

                }

                final int sizex = alleventcountry.size();
                if (sizex > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        distinctcountry = alleventcountry.stream().distinct().collect(Collectors.<String>toList());
                        distinctregio = eventregio.stream().distinct().collect(Collectors.<String>toList());
                        distinctcity = eventcity.stream().distinct().collect(Collectors.<String>toList());
                        distincttype = alleventtype.stream().distinct().collect(Collectors.<String>toList());


                        distinctcountryemea = distinctcountryemea.stream().distinct().collect(Collectors.<String>toList());
                        distinctcountryapec = distinctcountryapec.stream().distinct().collect(Collectors.<String>toList());
                        distinctcountryamer = distinctcountryamer.stream().distinct().collect(Collectors.<String>toList());

                        distinctcountry.sort(String.CASE_INSENSITIVE_ORDER);
                        distinctregio.sort(String.CASE_INSENSITIVE_ORDER);
                        distinctcity.sort(String.CASE_INSENSITIVE_ORDER);
                        distincttype.sort(String.CASE_INSENSITIVE_ORDER);


                    }


                    // TODO AKTION
                }


                //  Log.i("TEST LOG--->",child.getValue().toString());


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();


            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        mapView.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();

        mapView.onResume();
        //mapView.getMapAsync(this);
        new AgentData();
        agentname.setText(AgentData.getagentname());


        if (!AgentData.getFaction().isEmpty()) {
            if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                agentimg.setBorderColor(getResources().getColor(R.color.colorENL, getTheme()));
            } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                agentimg.setBorderColor(getResources().getColor(R.color.colorRES, getTheme()));
            } else {
                agentimg.setBorderColor(getResources().getColor(R.color.colorGray, getTheme()));
            }

        }

        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                if (AgentData.getFaction().isEmpty()) {
                    if (snapshot.child("faction").getValue().toString().equalsIgnoreCase("ENLIGHTENED")) {
                        agentimg.setBorderColor(getResources().getColor(R.color.colorENL, getTheme()));
                    } else if (snapshot.child("faction").getValue().toString().equalsIgnoreCase("RESISTANCE")) {
                        agentimg.setBorderColor(getResources().getColor(R.color.colorRES, getTheme()));
                    } else {
                        agentimg.setBorderColor(getResources().getColor(R.color.colorGray, getTheme()));
                    }

                }

                initevents(false, false, false, false, null, null, null);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_sometningwrong), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                initevents(false, false, false, false, null, null, null);

            }
        });

        // Log.i("RESUME DATA -->", "hasfilter: "+AgentData.gethasfilter() + " second: "+AgentData.hasfilter+ "country: "+AgentData.gethasfiltercountry()+ " cdata: "+AgentData.filterdatacountry.toString());

        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getProviderData().get(1).getPhotoUrl().toString())
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)


                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    //  .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(agentimg);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();


        if (markerViewManager != null) {


            markerViewManager.onDestroy();

        }

        mapView.onDestroy();


    }

    @Override
    public void onStop() {


        mapView.onStop();
        super.onStop();
    }

    @Override
    public void onPause() {


        mapView.onPause();
        super.onPause();
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btn_ispoc:
                // Log.i("Country --",  Locale.getDefault().getISO3Country());
                //initevents(true, true, false, false, Locale.getDefault().getISO3Country(), null, null);
                StyleableToast.makeText(getApplicationContext(), "You are a POC of a First Saturday. Soon you can activate your city for a FS AOD event here.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                break;

            default:
                break;
        }
    }


    private class CustomTask extends AsyncTask<Void, Void, Void> {
        FlowLayout flow;
        LayoutInflater infl;
        String cityof;
        String imageof;
        ImageView eventlogov;
        View theview;


        public CustomTask(FlowLayout myRootfriends, View view) {
            flow = myRootfriends;
            theview = view;
        }

        protected Void doInBackground(Void... param) {

            Log.i("Background -->", "jep");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    flow.addView(theview);
                }
            });


            return null;
        }

        protected void onPostExecute(Void param) {
            Log.i("Finisched -->", "jep");
        }
    }

}