package rocks.prxenon.aod.events;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.muddzdev.styleabletoast.StyleableToast;

import org.apmem.tools.layouts.FlowLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import de.hdodenhof.circleimageview.CircleImageView;
import mehdi.sakout.fancybuttons.FancyButton;
import rocks.prxenon.aod.R;
import rocks.prxenon.aod.adapter.EventsAdapter;
import rocks.prxenon.aod.adapter.SelectCountry;
import rocks.prxenon.aod.adapter.SelectType;
import rocks.prxenon.aod.basics.AgentData;
import rocks.prxenon.aod.events.map.NIAEventsMap;
import rocks.prxenon.aod.helper.PicassoCache;

import static rocks.prxenon.aod.basics.AODConfig.mBevents;
import static rocks.prxenon.aod.basics.AODConfig.mDatabase;
import static rocks.prxenon.aod.basics.AODConfig.pref;
import static rocks.prxenon.aod.basics.AODConfig.user;

public class NIAEvents extends AppCompatActivity implements SearchView.OnClickListener {

    public static AlertDialog.Builder filterbuilder;
    public static List<String> ispocidlist = new ArrayList<>();
    public static List<String> distinctcountry = new ArrayList<>();
    public static List<String> distinctregio = new ArrayList<>();
    public static List<String> distincttype = new ArrayList<>();
    public static List<String> distinctcity = new ArrayList<>();
    public static List<String> selectcountry = new ArrayList<String>();
    public static List<String> selecteventtype = new ArrayList<String>();
    public static List<String> distinctcountryemea = new ArrayList<>();
    public static List<String> distinctcountryamer = new ArrayList<>();
    public static List<String> distinctcountryapec = new ArrayList<>();
    public static ImageButton btn_ispoc, btn_omap;
    final List<String> eventid = new ArrayList();
    final List<Boolean> eventstatus = new ArrayList();
    final List<String> eventcity = new ArrayList();
    final List<String> eventlocation = new ArrayList();
    final List<String> eventtype = new ArrayList();
    final List<String> eventimage = new ArrayList();
    final List<String> eventday = new ArrayList();
    final List<String> eventmonth = new ArrayList();
    final List<String> eventyear = new ArrayList();
    final List<String> eventlat = new ArrayList();
    final List<String> eventlon = new ArrayList();
    final List<String> eventregio = new ArrayList();
    final List<String> eventcountry = new ArrayList<>();
    final List<String> alleventcountry = new ArrayList<>();
    final List<String> alleventtype = new ArrayList<>();
    final List<Long> eventstringtime = new ArrayList<>();
    final List<String> eventurl = new ArrayList<>();
    AlertDialog dialogcountry, dialogtype;
    int anzahl = 0;
    private boolean readyToPurchase = false;
    private FrameLayout progressBarHolder;
    private CoordinatorLayout coordinator;
    private Typeface type, proto;
    private TextView mTitleTextView;
    private ListView rankinglist;
    private FancyButton filter_nearme, filter_all, filter_type;
    private EventsAdapter adapter5;
    private TextView agentname;
    private CircleImageView agentimg;
    private SelectCountry adapter;
    private SelectType adaptertype;
    private boolean amerset, emeaset, apacset;
    private SwipeRefreshLayout mySwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_niaevents);

        pref = getSharedPreferences("AppPref", MODE_PRIVATE);


        new AgentData();
        new AgentData("prefs", pref);
        type = Typeface.createFromAsset(getAssets(), "main.ttf");
        proto = Typeface.createFromAsset(getAssets(), "main.ttf");


        progressBarHolder = findViewById(R.id.progressBarHolder);
        coordinator = findViewById(R.id.coordinator);


        ActionBar mActionBar = getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        View actionBar = mInflater.inflate(R.layout.custom_actionbar_events, null);
        mTitleTextView = actionBar.findViewById(R.id.title_text);
        mTitleTextView.setText("NIA EVENTS");

        btn_ispoc = actionBar.findViewById(R.id.btn_ispoc);
        btn_omap = actionBar.findViewById(R.id.btn_omap);

        btn_omap.setOnClickListener(this);
        agentname = findViewById(R.id.agentname);
        agentimg = findViewById(R.id.agentimage);
        agentname.setTypeface(proto);


        agentname.setText(AgentData.getagentname());
        mActionBar.setCustomView(actionBar);
        mActionBar.setDisplayShowCustomEnabled(true);
        ((Toolbar) actionBar.getParent()).setContentInsetsAbsolute(0, 0);


        mTitleTextView.setTypeface(type);
        mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28f);

        rankinglist = findViewById(R.id.eventlist);

        filter_nearme = findViewById(R.id.filter_nearme);
        filter_nearme.setOnClickListener(this);

        filter_all = findViewById(R.id.filter_all);
        filter_all.setOnClickListener(this);

        filter_type = findViewById(R.id.filter_typeof);
        filter_type.setOnClickListener(this);


        mySwipeRefreshLayout = findViewById(R.id.swiperefresh);

        mySwipeRefreshLayout.setRefreshing(false);


        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mySwipeRefreshLayout.setRefreshing(true);

                        onResume();


                    }
                }
        );
        // initevents(AgentData.gethasfilter(), AgentData.gethasfiltercountry(), AgentData.gethasfiltertype(), AgentData.gethasfilterregio(), AgentData.getfilterdatacountry(), null, null);

    }

    private void initevents(final Boolean filteraktiv, final Boolean filtercountry, final Boolean filterart, Boolean filterregio, final List<String> country, final List<String> art, List<String> regio) {


        //Log.i("resume-->", " "+country.toString());
        boolean ispocornot = false;

        eventcity.clear();
        eventcountry.clear();
        alleventcountry.clear();
        alleventtype.clear();
        eventday.clear();
        eventid.clear();
        eventimage.clear();
        eventlat.clear();
        eventlocation.clear();
        eventlon.clear();
        eventmonth.clear();
        eventregio.clear();
        eventstatus.clear();
        eventtype.clear();
        eventurl.clear();
        eventyear.clear();
        eventstringtime.clear();
        ispocidlist.clear();
        distinctcity.clear();
        distinctcountry.clear();
        distinctregio.clear();
        distincttype.clear();
        distinctcountryemea.clear();
        distinctcountryamer.clear();
        distinctcountryapec.clear();
        rankinglist.setAdapter(null);
        mBevents.child("events").orderByChild("stringtime").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot snapshotevents) {

                for (DataSnapshot childevents : snapshotevents.getChildren()) {

                    // TODO CRASH IF failed
                    if (Boolean.parseBoolean(childevents.child("active").getValue().toString())) {

                        if (childevents.child("type").getValue().toString().equalsIgnoreCase("First Saturday")) {

                            if (childevents.child("details").exists()) {
                                //ispocornot
                                if (childevents.child("details").child("enl_poc").getValue().toString().equalsIgnoreCase(AgentData.getagentname()) || childevents.child("details").child("res_poc").getValue().toString().equalsIgnoreCase(AgentData.getagentname())) {
                                    btn_ispoc.setVisibility(View.VISIBLE);
                                    ispocidlist.add(childevents.child("eventid").getValue().toString());

                                    btn_ispoc.setOnClickListener(NIAEvents.this);
                                }
                            }
                        }

                        // AKTIVER FILTER
                        if (filteraktiv) {
                            // LAND FILTER
                            if (filtercountry) {

                                selectcountry = country;
                                filter_all.setBorderColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                filter_nearme.setBorderColor(getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));

                                // Log.i("Pommes --->", " "+country.toString());
                                if (country != null && childevents.child("country").exists())
                                    if (country.contains(childevents.child("country").getValue().toString())) {

                                        // ART FILTER
                                        if (filterart) {
                                            selecteventtype = art;
                                            filter_type.setBorderColor(getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));
                                            if (childevents.child("type").exists())
                                                if (art.contains(childevents.child("type").getValue().toString())) {
                                                    eventid.add(childevents.child("eventid").getValue().toString());
                                                    eventlocation.add(childevents.child("location").getValue().toString());
                                                    eventimage.add(childevents.child("img").getValue().toString());
                                                    eventregio.add(childevents.child("regio").getValue().toString());
                                                    eventtype.add(childevents.child("type").getValue().toString());
                                                    if (childevents.child("url").exists()) {
                                                        eventurl.add(childevents.child("url").getValue().toString());
                                                    } else {
                                                        eventurl.add("no url");
                                                    }
                                                    eventcity.add(childevents.child("city").getValue().toString());
                                                    eventcountry.add(childevents.child("country").getValue().toString());
                                                    alleventcountry.add(childevents.child("country").getValue().toString());
                                                    alleventtype.add(childevents.child("type").getValue().toString());
                                                    if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                                                        distinctcountryamer.add(childevents.child("regio").getValue().toString());
                                                    }
                                                    if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                                                        distinctcountryemea.add(childevents.child("regio").getValue().toString());
                                                    }
                                                    if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                                                        distinctcountryapec.add(childevents.child("regio").getValue().toString());
                                                    }
                                                    eventstatus.add(Boolean.parseBoolean(childevents.child("active").getValue().toString()));
                                                    long dv = Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000;// its need to be in milisecond
                                                    eventstringtime.add(Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000);
                                                    Date df = new java.util.Date(dv);

                                                    String day = new SimpleDateFormat("dd", Locale.US).format(df);
                                                    String month = new SimpleDateFormat("MM", Locale.US).format(df);
                                                    String year = new SimpleDateFormat("yyyy", Locale.US).format(df);

                                                    eventday.add(day);
                                                    eventmonth.add(month);
                                                    eventyear.add(year);
                                                } else {
                                                    alleventtype.add(childevents.child("type").getValue().toString());
                                                    alleventcountry.add(childevents.child("country").getValue().toString());
                                                    if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                                                        distinctcountryamer.add(childevents.child("regio").getValue().toString());
                                                    }
                                                    if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                                                        distinctcountryemea.add(childevents.child("regio").getValue().toString());
                                                    }
                                                    if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                                                        distinctcountryapec.add(childevents.child("regio").getValue().toString());
                                                    }
                                                }
                                        }

                                        // KEIN ART FILTER
                                        else {
                                            filter_type.setBorderColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                            eventid.add(childevents.child("eventid").getValue().toString());
                                            eventlocation.add(childevents.child("location").getValue().toString());
                                            eventimage.add(childevents.child("img").getValue().toString());
                                            eventregio.add(childevents.child("regio").getValue().toString());
                                            eventtype.add(childevents.child("type").getValue().toString());
                                            if (childevents.child("url").exists()) {
                                                eventurl.add(childevents.child("url").getValue().toString());
                                            } else {
                                                eventurl.add("no url");
                                            }
                                            eventcity.add(childevents.child("city").getValue().toString());
                                            eventcountry.add(childevents.child("country").getValue().toString());
                                            alleventcountry.add(childevents.child("country").getValue().toString());
                                            alleventtype.add(childevents.child("type").getValue().toString());
                                            if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                                                distinctcountryamer.add(childevents.child("regio").getValue().toString());
                                            }
                                            if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                                                distinctcountryemea.add(childevents.child("regio").getValue().toString());
                                            }
                                            if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                                                distinctcountryapec.add(childevents.child("regio").getValue().toString());
                                            }
                                            eventstatus.add(Boolean.parseBoolean(childevents.child("active").getValue().toString()));
                                            long dv = Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000;// its need to be in milisecond
                                            eventstringtime.add(Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000);
                                            Date df = new java.util.Date(dv);

                                            String day = new SimpleDateFormat("dd", Locale.US).format(df);
                                            String month = new SimpleDateFormat("MM", Locale.US).format(df);
                                            String year = new SimpleDateFormat("yyyy", Locale.US).format(df);

                                            eventday.add(day);
                                            eventmonth.add(month);
                                            eventyear.add(year);
                                        }
                                    } else {
                                        alleventcountry.add(childevents.child("country").getValue().toString());
                                        alleventtype.add(childevents.child("type").getValue().toString());
                                        if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                                            distinctcountryamer.add(childevents.child("regio").getValue().toString());
                                        }
                                        if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                                            distinctcountryemea.add(childevents.child("regio").getValue().toString());
                                        }
                                        if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                                            distinctcountryapec.add(childevents.child("regio").getValue().toString());
                                        }
                                    }
                            }
                            // KEIN LAND FILTER
                            else {
                                if (filterart) {
                                    selecteventtype = art;
                                    filter_type.setBorderColor(getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));
                                    if (art != null && childevents.child("type").exists())
                                        if (art.contains(childevents.child("type").getValue().toString())) {
                                            eventid.add(childevents.child("eventid").getValue().toString());
                                            eventlocation.add(childevents.child("location").getValue().toString());
                                            eventimage.add(childevents.child("img").getValue().toString());
                                            eventregio.add(childevents.child("regio").getValue().toString());
                                            eventtype.add(childevents.child("type").getValue().toString());
                                            if (childevents.child("url").exists()) {
                                                eventurl.add(childevents.child("url").getValue().toString());
                                            } else {
                                                eventurl.add("no url");
                                            }
                                            eventcity.add(childevents.child("city").getValue().toString());
                                            if (childevents.hasChild("country")) {
                                                eventcountry.add(childevents.child("country").getValue().toString());
                                                alleventcountry.add(childevents.child("country").getValue().toString());
                                            } else {
                                                eventcountry.add("UNKNOWN");
                                                alleventcountry.add("UNKNOWN");
                                            }

                                            alleventtype.add(childevents.child("type").getValue().toString());
                                            if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                                                distinctcountryamer.add(childevents.child("regio").getValue().toString());
                                            }
                                            if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                                                distinctcountryemea.add(childevents.child("regio").getValue().toString());
                                            }
                                            if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                                                distinctcountryapec.add(childevents.child("regio").getValue().toString());
                                            }
                                            eventstatus.add(Boolean.parseBoolean(childevents.child("active").getValue().toString()));
                                            long dv = Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000;// its need to be in milisecond
                                            eventstringtime.add(Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000);
                                            Date df = new java.util.Date(dv);

                                            String day = new SimpleDateFormat("dd", Locale.US).format(df);
                                            String month = new SimpleDateFormat("MM", Locale.US).format(df);
                                            String year = new SimpleDateFormat("yyyy", Locale.US).format(df);

                                            eventday.add(day);
                                            eventmonth.add(month);
                                            eventyear.add(year);
                                        } else {
                                            if (childevents.hasChild("country")) {
                                                alleventcountry.add(childevents.child("country").getValue().toString());
                                                alleventtype.add(childevents.child("type").getValue().toString());
                                                if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                                                    distinctcountryamer.add(childevents.child("regio").getValue().toString());
                                                }
                                                if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                                                    distinctcountryemea.add(childevents.child("regio").getValue().toString());
                                                }
                                                if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                                                    distinctcountryapec.add(childevents.child("regio").getValue().toString());
                                                }
                                            } else {
                                                alleventcountry.add("UNKNOWN");
                                                alleventtype.add(childevents.child("type").getValue().toString());
                                                if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                                                    distinctcountryamer.add(childevents.child("regio").getValue().toString());
                                                }
                                                if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                                                    distinctcountryemea.add(childevents.child("regio").getValue().toString());
                                                }
                                                if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                                                    distinctcountryapec.add(childevents.child("regio").getValue().toString());
                                                }
                                            }
                                        }
                                }

                                // KEIN ART FILTER
                                else {
                                    filter_nearme.setBorderColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                    eventid.add(childevents.child("eventid").getValue().toString());
                                    eventlocation.add(childevents.child("location").getValue().toString());
                                    eventimage.add(childevents.child("img").getValue().toString());
                                    eventregio.add(childevents.child("regio").getValue().toString());
                                    eventtype.add(childevents.child("type").getValue().toString());
                                    if (childevents.child("url").exists()) {
                                        eventurl.add(childevents.child("url").getValue().toString());
                                    } else {
                                        eventurl.add("no url");
                                    }
                                    eventcity.add(childevents.child("city").getValue().toString());
                                    eventcountry.add(childevents.child("country").getValue().toString());
                                    alleventcountry.add(childevents.child("country").getValue().toString());
                                    alleventtype.add(childevents.child("type").getValue().toString());
                                    if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                                        distinctcountryamer.add(childevents.child("regio").getValue().toString());
                                    }
                                    if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                                        distinctcountryemea.add(childevents.child("regio").getValue().toString());
                                    }
                                    if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                                        distinctcountryapec.add(childevents.child("regio").getValue().toString());
                                    }
                                    eventstatus.add(Boolean.parseBoolean(childevents.child("active").getValue().toString()));
                                    long dv = Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000;// its need to be in milisecond
                                    eventstringtime.add(Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000);
                                    Date df = new java.util.Date(dv);

                                    String day = new SimpleDateFormat("dd", Locale.US).format(df);
                                    String month = new SimpleDateFormat("MM", Locale.US).format(df);
                                    String year = new SimpleDateFormat("yyyy", Locale.US).format(df);

                                    eventday.add(day);
                                    eventmonth.add(month);
                                    eventyear.add(year);
                                }
                            }
                        }
                        // NO FILTER
                        else {
                            filter_type.setBorderColor(getResources().getColor(R.color.colorWhite, getTheme()));
                            filter_all.setBorderColor(getResources().getColor(R.color.colorPrimaryYellowDark, getTheme()));
                            filter_nearme.setBorderColor(getResources().getColor(R.color.colorWhite, getTheme()));
                            eventid.add(childevents.child("eventid").getValue().toString());
                            eventlocation.add(childevents.child("location").getValue().toString());
                            eventimage.add(childevents.child("img").getValue().toString());
                            eventregio.add(childevents.child("regio").getValue().toString());
                            eventtype.add(childevents.child("type").getValue().toString());
                            if (childevents.child("url").exists()) {
                                eventurl.add(childevents.child("url").getValue().toString());
                            } else {
                                eventurl.add("no url");
                            }
                            eventcity.add(childevents.child("city").getValue().toString());
                            if (childevents.child("country").exists()) {
                                eventcountry.add(childevents.child("country").getValue().toString());
                                alleventcountry.add(childevents.child("country").getValue().toString());
                            } else {
                                eventcountry.add("UNKNOWN");
                                alleventcountry.add("UNKNOWN");
                            }

                            alleventtype.add(childevents.child("type").getValue().toString());
                            if (childevents.child("regio").getValue().toString().equalsIgnoreCase("AMER")) {
                                distinctcountryamer.add(childevents.child("regio").getValue().toString());
                            }
                            if (childevents.child("regio").getValue().toString().equalsIgnoreCase("EMEA")) {
                                distinctcountryemea.add(childevents.child("regio").getValue().toString());
                            }
                            if (childevents.child("regio").getValue().toString().equalsIgnoreCase("APAC")) {
                                distinctcountryapec.add(childevents.child("regio").getValue().toString());
                            }
                            eventstatus.add(Boolean.parseBoolean(childevents.child("active").getValue().toString()));
                            long dv = Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000;// its need to be in milisecond
                            eventstringtime.add(Long.valueOf(childevents.child("stringtime").getValue().toString()) * 1000);
                            Date df = new java.util.Date(dv);

                            String day = new SimpleDateFormat("dd", Locale.US).format(df);
                            String month = new SimpleDateFormat("MM", Locale.US).format(df);
                            String year = new SimpleDateFormat("yyyy", Locale.US).format(df);

                            eventday.add(day);
                            eventmonth.add(month);
                            eventyear.add(year);

                            Log.i("DATE DAY -->", day);
                            Log.i("DATE MONTH -->", month);
                            Log.i("DATE YEAR -->", year);
                            Log.i("REGIO-->", childevents.child("regio").getValue().toString());
                        }
                    }

                }

                final int sizex = alleventcountry.size();
                if (sizex > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        distinctcountry = alleventcountry.stream().distinct().collect(Collectors.<String>toList());
                        distinctregio = eventregio.stream().distinct().collect(Collectors.<String>toList());
                        distinctcity = eventcity.stream().distinct().collect(Collectors.<String>toList());
                        distincttype = alleventtype.stream().distinct().collect(Collectors.<String>toList());


                        distinctcountryemea = distinctcountryemea.stream().distinct().collect(Collectors.<String>toList());
                        distinctcountryapec = distinctcountryapec.stream().distinct().collect(Collectors.<String>toList());
                        distinctcountryamer = distinctcountryamer.stream().distinct().collect(Collectors.<String>toList());

                        distinctcountry.sort(String.CASE_INSENSITIVE_ORDER);
                        distinctregio.sort(String.CASE_INSENSITIVE_ORDER);
                        distinctcity.sort(String.CASE_INSENSITIVE_ORDER);
                        distincttype.sort(String.CASE_INSENSITIVE_ORDER);

                    }
                    adapter5 = new EventsAdapter(NIAEvents.this, eventid, eventlocation, eventimage, eventcountry, eventregio, eventtype, eventcity, eventmonth, eventstringtime, eventurl);

                    rankinglist.setAdapter(adapter5);
                    adapter5.notifyDataSetChanged();

                    rankinglist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Log.i("URL -->", eventcity.get(position) + " " + eventurl.get(position));
                        }
                    });

                }


                mySwipeRefreshLayout.setRefreshing(false);
                //  Log.i("TEST LOG--->",child.getValue().toString());


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                coordinator.setVisibility(View.VISIBLE);
                progressBarHolder.setVisibility(View.GONE);
                StyleableToast.makeText(getApplicationContext(), getString(R.string.erros_tost), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                mySwipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new AgentData();
        agentname.setText(AgentData.getagentname());


        if (!AgentData.getFaction().isEmpty()) {
            if (AgentData.getFaction().equalsIgnoreCase("ENLIGHTENED")) {
                agentimg.setBorderColor(getResources().getColor(R.color.colorENL, getTheme()));
            } else if (AgentData.getFaction().equalsIgnoreCase("RESISTANCE")) {
                agentimg.setBorderColor(getResources().getColor(R.color.colorRES, getTheme()));
            } else {
                agentimg.setBorderColor(getResources().getColor(R.color.colorGray, getTheme()));
            }

        }

        mDatabase.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                if (AgentData.getFaction().isEmpty()) {
                    if (snapshot.child("faction").getValue().toString().equalsIgnoreCase("ENLIGHTENED")) {
                        agentimg.setBorderColor(getResources().getColor(R.color.colorENL, getTheme()));
                    } else if (snapshot.child("faction").getValue().toString().equalsIgnoreCase("RESISTANCE")) {
                        agentimg.setBorderColor(getResources().getColor(R.color.colorRES, getTheme()));
                    } else {
                        agentimg.setBorderColor(getResources().getColor(R.color.colorGray, getTheme()));
                    }

                }
                if (snapshot.child("filter").exists()) {
                    new AgentData("filter", true);
                    if (snapshot.child("filter").child("country").exists()) {
                        new AgentData("filtercountry", true);
                        new AgentData("filterdatacountry", (List<String>) snapshot.child("filter").child("country").getValue());
                        List<String> vvl = (List<String>) snapshot.child("filter").child("country").getValue();

                        if (snapshot.child("filter").child("type").exists()) {
                            new AgentData("filtertype", true);
                            new AgentData("filterdatatype", (List<String>) snapshot.child("filter").child("type").getValue());
                            List<String> vvl2 = (List<String>) snapshot.child("filter").child("type").getValue();

                            initevents(true, true, true, false, vvl, vvl2, null);

                        } else {

                            new AgentData("filtertype", false);
                            initevents(true, true, false, false, vvl, null, null);
                        }


                    } else {

                        new AgentData("filtercountry", false);

                        if (snapshot.child("filter").child("type").exists()) {
                            new AgentData("filtertype", true);
                            new AgentData("filterdatatype", (List<String>) snapshot.child("filter").child("type").getValue());
                            List<String> vvl2 = (List<String>) snapshot.child("filter").child("type").getValue();
                            initevents(true, false, true, false, null, vvl2, null);
                        } else {
                            new AgentData("filter", false);
                            new AgentData("filtercountry", false);
                            new AgentData("filterregio", false);
                            new AgentData("filtercity", false);
                            new AgentData("filtertype", false);
                            initevents(false, false, false, false, null, null, null);
                        }
                    }


                } else {
                    new AgentData("filter", false);
                    new AgentData("filtercountry", false);
                    new AgentData("filterregio", false);
                    new AgentData("filtercity", false);
                    new AgentData("filtertype", false);
                    initevents(false, false, false, false, null, null, null);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // StyleableToast.makeText(getApplicationContext(), getResources().getString(R.string.toast_sometningwrong), Toast.LENGTH_LONG, R.style.defaulttoast).show();
                initevents(false, false, false, false, null, null, null);

            }
        });

        // Log.i("RESUME DATA -->", "hasfilter: "+AgentData.gethasfilter() + " second: "+AgentData.hasfilter+ "country: "+AgentData.gethasfiltercountry()+ " cdata: "+AgentData.filterdatacountry.toString());

        try {
            PicassoCache.getPicassoInstance(getApplicationContext())
                    .load(user.getProviderData().get(1).getPhotoUrl().toString())
                    .placeholder(R.drawable.full_logo)
                    .error(R.drawable.full_logo)


                    // .memoryPolicy(MemoryPolicy.NO_CACHE )
                    //  .networkPolicy(NetworkPolicy.NO_CACHE)
                    //.transform(new CropRoundTransformation())
                    .into(agentimg);
            // new DownloadImageTask(cache,headerimg).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, personPhotoUrl);
        } catch (Exception e) {
            //Log.e("Error", e.getMessage());

            e.printStackTrace();
        }

        apacset = false;
        amerset = false;
        emeaset = false;

    }

    @Override
    public void onDestroy() {
        selectcountry.clear();
        selecteventtype.clear();
        apacset = false;
        amerset = false;
        emeaset = false;
        super.onDestroy();
    }

    @Override
    public void onStop() {
        selectcountry.clear();
        selecteventtype.clear();
        apacset = false;
        amerset = false;
        emeaset = false;
        super.onStop();
    }

    @Override
    public void onPause() {
        selectcountry.clear();
        selecteventtype.clear();
        apacset = false;
        amerset = false;
        emeaset = false;
        super.onPause();
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_omap:
                // Log.i("Country --",  Locale.getDefault().getISO3Country());
                //initevents(true, true, false, false, Locale.getDefault().getISO3Country(), null, null);
                Intent ix = new Intent(getBaseContext(), NIAEventsMap.class);
                //i.putExtra("PersonID", personID);
                startActivity(ix);
                break;
            case R.id.btn_ispoc:
                // Log.i("Country --",  Locale.getDefault().getISO3Country());
                //initevents(true, true, false, false, Locale.getDefault().getISO3Country(), null, null);
                StyleableToast.makeText(getApplicationContext(), "You are a POC of a First Saturday. Soon you can activate your city for a FS AOD event here.", Toast.LENGTH_LONG, R.style.defaulttoast).show();
                break;
            case R.id.filter_nearme:
                // Log.i("Country --",  Locale.getDefault().getISO3Country());
                //initevents(true, true, false, false, Locale.getDefault().getISO3Country(), null, null);
                filterby();
                break;
            case R.id.filter_all:
                //Log.i("Country --",  getApplicationContext().getResources().getConfiguration().locale.getDisplayCountry(Locale.US));
                initevents(false, AgentData.gethasfiltercountry(), AgentData.gethasfiltertype(), AgentData.gethasfilterregio(), AgentData.getfilterdatacountry(), null, null);

                break;
            case R.id.filter_typeof:
                // Log.i("Country --",  Locale.getDefault().getISO3Country());
                //initevents(true, true, false, false, Locale.getDefault().getISO3Country(), null, null);
                filterbytype();
                break;
            default:
                break;
        }
    }

    private void filterby() {


        LayoutInflater inflater = getLayoutInflater();

        final View dialog = inflater.inflate(R.layout.dialog_filter_country, null);

        final TextView friendtextpos = dialog.findViewById(R.id.friendtextpos);

        final TextView emea = dialog.findViewById(R.id.regio_emea);
        final TextView apac = dialog.findViewById(R.id.regio_apac);
        final TextView amer = dialog.findViewById(R.id.regio_amer);

        final GridView gridlv = dialog.findViewById(R.id.listofcountrys);

        adapter = new SelectCountry(this, distinctcountry);
        gridlv.setAdapter(null);
        gridlv.setAdapter(adapter);

        if (AgentData.gethasfilter()) {
            if (AgentData.gethasfiltercountry()) {

                selectcountry = AgentData.getfilterdatacountry();

            }
        }


        emea.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!emeaset) {
                    dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                    dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                    final int padding = emea.getPaddingTop();


                    emea.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_active_regio, getTheme()));

                    emea.setPadding(padding, padding, padding, padding);

                    int j = 0;
                    emeaset = true;
                    final List<String> eventcountryemea = new ArrayList<>();
                    while (eventcountry.size() > j) {


                        if (eventregio.get(j).equalsIgnoreCase("EMEA")) {
                            anzahl++;
                            eventcountryemea.add(eventcountry.get(j));

                        }
                        j++;


                        if (eventcountry.size() >= j) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                selectcountry.addAll(eventcountryemea.stream().distinct().collect(Collectors.<String>toList()));
                                selectcountry = selectcountry.stream().distinct().collect(Collectors.<String>toList());
                                selectcountry.sort(String.CASE_INSENSITIVE_ORDER);

                                adapter.notifyDataSetChanged();
                            }

                            if (anzahl < 1) {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                            } else {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                            }
                        }
                    }


                } else {
                    int j = 0;
                    emeaset = false;
                    final int padding = emea.getPaddingTop();


                    emea.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));

                    emea.setPadding(padding, padding, padding, padding);


                    final List<String> eventcountryemea = new ArrayList<>();
                    while (eventcountry.size() > j) {


                        if (eventregio.get(j).equalsIgnoreCase("EMEA")) {
                            anzahl--;
                            eventcountryemea.add(eventcountry.get(j));

                        }
                        j++;


                        if (eventcountry.size() >= j) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                selectcountry.removeAll(eventcountryemea.stream().distinct().collect(Collectors.<String>toList()));
                                selectcountry = selectcountry.stream().distinct().collect(Collectors.<String>toList());
                                selectcountry.sort(String.CASE_INSENSITIVE_ORDER);

                                adapter.notifyDataSetChanged();
                            }

                            if (anzahl < 1) {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                            } else {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                            }
                        }
                    }
                }


            }
        });


        apac.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!apacset) {
                    dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                    dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                    final int padding = apac.getPaddingTop();


                    apac.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_active_regio, getTheme()));

                    apac.setPadding(padding, padding, padding, padding);

                    int j = 0;
                    apacset = true;
                    final List<String> eventcountryapac = new ArrayList<>();
                    while (eventcountry.size() > j) {


                        if (eventregio.get(j).equalsIgnoreCase("APAC")) {
                            anzahl++;
                            eventcountryapac.add(eventcountry.get(j));
                            Log.i("Test --->", " " + eventcountry.get(j));

                        }
                        j++;


                        if (eventcountry.size() >= j) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                selectcountry.addAll(eventcountryapac.stream().distinct().collect(Collectors.<String>toList()));
                                selectcountry = selectcountry.stream().distinct().collect(Collectors.<String>toList());
                                selectcountry.sort(String.CASE_INSENSITIVE_ORDER);

                                adapter.notifyDataSetChanged();
                            }

                            if (anzahl < 1) {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                            } else {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                            }
                        }
                    }
                } else {
                    final int padding = apac.getPaddingTop();
                    apac.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));

                    apac.setPadding(padding, padding, padding, padding);

                    int j = 0;
                    apacset = false;
                    final List<String> eventcountryapac = new ArrayList<>();
                    while (eventcountry.size() > j) {


                        if (eventregio.get(j).equalsIgnoreCase("APAC")) {
                            anzahl--;
                            eventcountryapac.add(eventcountry.get(j));
                            Log.i("Test --->", " " + eventcountry.get(j));

                        }
                        j++;


                        if (eventcountry.size() >= j) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                selectcountry.removeAll(eventcountryapac.stream().distinct().collect(Collectors.<String>toList()));
                                selectcountry = selectcountry.stream().distinct().collect(Collectors.<String>toList());
                                selectcountry.sort(String.CASE_INSENSITIVE_ORDER);

                                adapter.notifyDataSetChanged();
                            }

                            if (anzahl < 1) {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                            } else {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                            }
                        }
                    }
                }


            }
        });

        amer.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!amerset) {

                    dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                    dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                    final int padding = amer.getPaddingTop();


                    amer.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_active_regio, getTheme()));

                    amer.setPadding(padding, padding, padding, padding);

                    int j = 0;
                    amerset = true;
                    final List<String> eventcountryamer = new ArrayList<>();
                    while (eventcountry.size() > j) {


                        if (eventregio.get(j).equalsIgnoreCase("AMER")) {
                            anzahl++;
                            eventcountryamer.add(eventcountry.get(j));
                            Log.i("Test --->", " " + eventcountry.get(j));

                        }
                        j++;


                        if (eventcountry.size() >= j) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                selectcountry.addAll(eventcountryamer.stream().distinct().collect(Collectors.<String>toList()));
                                selectcountry = selectcountry.stream().distinct().collect(Collectors.<String>toList());
                                selectcountry.sort(String.CASE_INSENSITIVE_ORDER);

                                adapter.notifyDataSetChanged();
                            }

                            if (anzahl < 1) {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                            } else {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                            }
                        }
                    }
                } else {

                    final int padding = apac.getPaddingTop();
                    amer.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));

                    amer.setPadding(padding, padding, padding, padding);
                    int j = 0;
                    amerset = false;
                    final List<String> eventcountryamer = new ArrayList<>();
                    while (eventcountry.size() > j) {


                        if (eventregio.get(j).equalsIgnoreCase("AMER")) {
                            anzahl--;
                            eventcountryamer.add(eventcountry.get(j));
                            Log.i("Test --->", " " + eventcountry.get(j));

                        }
                        j++;


                        if (eventcountry.size() >= j) {

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                selectcountry.removeAll(eventcountryamer.stream().distinct().collect(Collectors.<String>toList()));
                                selectcountry = selectcountry.stream().distinct().collect(Collectors.<String>toList());
                                selectcountry.sort(String.CASE_INSENSITIVE_ORDER);

                                adapter.notifyDataSetChanged();
                            }

                            if (anzahl < 1) {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                            } else {
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                                dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                            }
                        }
                    }
                }


            }
        });


        if (AgentData.gethasfilter()) {
            anzahl = selectcountry.size();
        }
        gridlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //text.setText((String) (lv.getItemAtPosition(position)));
                Log.i("ITEM_CLICKED", "" + gridlv.getItemAtPosition(position));

                Log.i("FILTER", "" + AgentData.gethasfilter());
                final TextView tv = view.findViewById(R.id.turfnamet);


                if (!AgentData.gethasfilter()) {
                    if (selectcountry.contains(gridlv.getItemAtPosition(position))) {
                        anzahl--;
                        if (anzahl < 1) {
                            dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                        } else {
                            dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                        }
                        Log.i("REMOVED ", "" + selectcountry.indexOf(gridlv.getItemAtPosition(position)));
                        int pi = selectcountry.indexOf(gridlv.getItemAtPosition(position));
                        selectcountry.remove(gridlv.getItemAtPosition(position));


                        // themyturfs = mycountry
                        Log.i("ISENABLED" + position, "" + gridlv.getItemAtPosition(position));
                        final int padding = tv.getPaddingTop();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));
                                tv.setEnabled(false);
                                tv.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                tv.setPadding(padding, padding, padding, padding);
                            }
                        });
                        adapter.notifyDataSetChanged();

                        //  sturfs.add((String) (lv.getItemAtPosition(position)));
                    } else {
                        anzahl++;
                        selectcountry.add((String) (gridlv.getItemAtPosition(position)));
                        dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));

                        final int padding = tv.getPaddingTop();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));
                                tv.setEnabled(false);
                                tv.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                tv.setPadding(padding, padding, padding, padding);
                            }
                        });

                        adapter.notifyDataSetChanged();


                        // mycountry.add(true);


                        // theturfs = selectcountry
                        // themyturfs =  mycountry
                        //  saveturfs.setVisibility(View.VISIBLE);
                        //  saveturfs.setEnabled(true);
                        // TODO SHOW SAVE BUTTON


                    }


                } else {

                    if (selectcountry.contains(gridlv.getItemAtPosition(position))) {
                        anzahl--;
                        if (anzahl < 1) {
                            dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                        } else {
                            dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                            dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                        }
                        Log.i("REMOVED ", "" + selectcountry.indexOf(gridlv.getItemAtPosition(position)));
                        int pi = selectcountry.indexOf(gridlv.getItemAtPosition(position));
                        selectcountry.remove(gridlv.getItemAtPosition(position));


                        // themyturfs = mycountry
                        Log.i("ISENABLED" + position, "" + gridlv.getItemAtPosition(position));
                        final int padding = tv.getPaddingTop();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));
                                tv.setEnabled(false);
                                tv.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                tv.setPadding(padding, padding, padding, padding);
                            }
                        });
                        adapter.notifyDataSetChanged();

                        //  sturfs.add((String) (lv.getItemAtPosition(position)));
                    } else {
                        anzahl++;
                        selectcountry.add((String) (gridlv.getItemAtPosition(position)));
                        dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));

                        final int padding = tv.getPaddingTop();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));
                                tv.setEnabled(false);
                                tv.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                tv.setPadding(padding, padding, padding, padding);
                            }
                        });

                        adapter.notifyDataSetChanged();


                        // mycountry.add(true);


                        // theturfs = selectcountry
                        // themyturfs =  mycountry
                        //  saveturfs.setVisibility(View.VISIBLE);
                        //  saveturfs.setEnabled(true);
                        // TODO SHOW SAVE BUTTON


                    }


                    // resettheturfs();
                }
            }
        });
        filterbuilder = new AlertDialog.Builder(NIAEvents.this, R.style.CustomDialog);

        // builder.setMessage("Text not finished...");

        filterbuilder.setView(dialog);
        filterbuilder.setCancelable(false);
        filterbuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

                if (filterbuilder != null) {
                    filterbuilder.create().dismiss();
                    filterbuilder = null;
                    selectcountry.clear();
                    anzahl = 0;
                }
            }
        });
        filterbuilder.setPositiveButton("set", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (filterbuilder != null) {
                    filterbuilder.create().dismiss();
                    filterbuilder = null;
                    savefilter(selectcountry);
                    if (selectcountry.size() > 0) {
                        initevents(true, true, AgentData.gethasfiltertype(), AgentData.gethasfilterregio(), selectcountry, null, null);
                    } else {
                        new AgentData("filter", false);
                        new AgentData("filtercountry", false);
                        new AgentData("filterdatacountry", selectcountry);
                        initevents(false, false, AgentData.gethasfiltertype(), AgentData.gethasfilterregio(), selectcountry, null, null);

                    }
                    // TODO SET FILTER
                }

            }
        });


        filterbuilder.setNeutralButton("reset", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

             /*  if (filterbuilder != null) {
                    //filterbuilder.create().dismiss();
                    //filterbuilder = null;
                    selectcountry = null;
                    anzahl = 0;
                    adapter.notifyDataSetChanged();

                    // TODO SET FILTER
                } */

            }
        });

        filterbuilder.setNegativeButton("abort", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (filterbuilder != null) {
                    filterbuilder.create().dismiss();
                    filterbuilder = null;

                    //adapter.notifyDataSetChanged();
                    anzahl = 0;
                    amerset = false;
                    emeaset = false;
                    apacset = false;
                    Intent intent = getIntent();
                    overridePendingTransition(0, 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(intent);


                }

            }
        });


        if (!isFinishing()) {
            if (filterbuilder != null) {
                if (filterbuilder.create().isShowing()) {
                    filterbuilder.create().dismiss();
                } else {


                    dialogcountry = filterbuilder.create();

                    dialogcountry.show();
                    if (anzahl < 1) {
                        dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                    } else {
                        dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                        dialogcountry.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                    }

                    dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Boolean wantToCloseDialog = false;
                            //Do stuff, possibly set wantToCloseDialog to true then...
                            if (wantToCloseDialog) {
                                dialogcountry.dismiss();
                            } else {
                                //savefilter(selectcountry);
                                final int padding = emea.getPaddingTop();


                                emea.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));

                                emea.setPadding(padding, padding, padding, padding);
                                amer.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));

                                amer.setPadding(padding, padding, padding, padding);

                                apac.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));

                                apac.setPadding(padding, padding, padding, padding);
                                dialogcountry.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                                List<String> pre = selectcountry;
                                selectcountry.clear();
                                anzahl = 0;
                                adapter.notifyDataSetChanged();

                                amerset = false;
                                emeaset = false;
                                apacset = false;
                            }
                            //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
                        }
                    });
                }
            }


        }
    }

    private void filterbytype() {


        LayoutInflater inflater = getLayoutInflater();

        final View dialog = inflater.inflate(R.layout.dialog_filter_type, null);

        final TextView friendtextpos = dialog.findViewById(R.id.friendtextpos);


        final GridView gridlv = dialog.findViewById(R.id.listoftypes);

        adaptertype = new SelectType(this, distincttype);
        gridlv.setAdapter(null);
        gridlv.setAdapter(adaptertype);


        if (AgentData.gethasfiltertype()) {


            anzahl = selecteventtype.size();
        }
        gridlv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //text.setText((String) (lv.getItemAtPosition(position)));
                Log.i("ITEM_CLICKED", "" + gridlv.getItemAtPosition(position));

                Log.i("FILTER", "" + AgentData.gethasfilter());
                final TextView tv = view.findViewById(R.id.turfnamet);
                if (!AgentData.gethasfilter()) {
                    if (selecteventtype.contains(gridlv.getItemAtPosition(position))) {
                        anzahl--;
                        if (anzahl < 1) {
                            dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                        } else {
                            dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                        }
                        Log.i("REMOVED ", "" + selecteventtype.indexOf(gridlv.getItemAtPosition(position)));
                        int pi = selecteventtype.indexOf(gridlv.getItemAtPosition(position));
                        selecteventtype.remove(gridlv.getItemAtPosition(position));


                        // themyturfs = mycountry
                        Log.i("ISENABLED" + position, "" + gridlv.getItemAtPosition(position));
                        final int padding = tv.getPaddingTop();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));
                                tv.setEnabled(false);
                                tv.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                tv.setPadding(padding, padding, padding, padding);
                            }
                        });
                        adaptertype.notifyDataSetChanged();

                        //  sturfs.add((String) (lv.getItemAtPosition(position)));
                    } else {
                        anzahl++;
                        selecteventtype.add((String) (gridlv.getItemAtPosition(position)));
                        dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));

                        final int padding = tv.getPaddingTop();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));
                                tv.setEnabled(false);
                                tv.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                tv.setPadding(padding, padding, padding, padding);
                            }
                        });

                        adaptertype.notifyDataSetChanged();


                        // mycountry.add(true);


                        // theturfs = selectcountry
                        // themyturfs =  mycountry
                        //  saveturfs.setVisibility(View.VISIBLE);
                        //  saveturfs.setEnabled(true);
                        // TODO SHOW SAVE BUTTON


                    }


                } else {

                    if (selecteventtype.contains(gridlv.getItemAtPosition(position))) {
                        anzahl--;
                        if (anzahl < 1) {
                            dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                        } else {
                            dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                            dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                        }
                        Log.i("REMOVED ", "" + selecteventtype.indexOf(gridlv.getItemAtPosition(position)));
                        int pi = selecteventtype.indexOf(gridlv.getItemAtPosition(position));
                        selecteventtype.remove(gridlv.getItemAtPosition(position));


                        // themyturfs = mycountry
                        Log.i("ISENABLED" + position, "" + gridlv.getItemAtPosition(position));
                        final int padding = tv.getPaddingTop();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));
                                tv.setEnabled(false);
                                tv.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                tv.setPadding(padding, padding, padding, padding);
                            }
                        });
                        adaptertype.notifyDataSetChanged();

                        //  sturfs.add((String) (lv.getItemAtPosition(position)));
                    } else {
                        anzahl++;
                        selecteventtype.add((String) (gridlv.getItemAtPosition(position)));
                        dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));

                        final int padding = tv.getPaddingTop();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tv.setBackground(getResources().getDrawable(R.drawable.bottom_sharp_gray, getTheme()));
                                tv.setEnabled(false);
                                tv.setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                                tv.setPadding(padding, padding, padding, padding);
                            }
                        });

                        adaptertype.notifyDataSetChanged();


                        // TODO SHOW SAVE BUTTON


                    }


                    // resettheturfs();
                }
            }
        });
        filterbuilder = new AlertDialog.Builder(NIAEvents.this, R.style.CustomDialog);

        // builder.setMessage("Text not finished...");

        filterbuilder.setView(dialog);
        filterbuilder.setCancelable(false);
        filterbuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

                if (filterbuilder != null) {
                    filterbuilder.create().dismiss();
                    filterbuilder = null;
                    selectcountry.clear();
                    anzahl = 0;
                }
            }
        });
        filterbuilder.setPositiveButton("set", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (filterbuilder != null) {
                    filterbuilder.create().dismiss();
                    filterbuilder = null;
                    savefiltertype(selecteventtype);
                    if (selecteventtype.size() > 0) {
                        if (AgentData.gethasfiltercountry()) {
                            initevents(true, true, true, false, selectcountry, selecteventtype, null);
                        } else {
                            initevents(true, false, true, false, null, selecteventtype, null);
                        }
                    } else {
                        if (AgentData.gethasfiltercountry()) {

                        } else {
                            new AgentData("filter", false);
                        }
                        new AgentData("filtertype", false);
                        new AgentData("filterdatatype", selecteventtype);
                        initevents(false, false, false, false, null, null, null);

                    }
                    // TODO SET FILTER
                }

            }
        });


        filterbuilder.setNeutralButton("reset", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

             /*  if (filterbuilder != null) {
                    //filterbuilder.create().dismiss();
                    //filterbuilder = null;
                    selectcountry = null;
                    anzahl = 0;
                    adapter.notifyDataSetChanged();

                    // TODO SET FILTER
                } */

            }
        });

        filterbuilder.setNegativeButton("abort", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (filterbuilder != null) {
                    filterbuilder.create().dismiss();
                    filterbuilder = null;

                    //adapter.notifyDataSetChanged();
                    anzahl = 0;

                    Intent intent = getIntent();
                    overridePendingTransition(0, 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(intent);


                }

            }
        });


        if (!isFinishing()) {
            if (filterbuilder != null) {
                if (filterbuilder.create().isShowing()) {
                    filterbuilder.create().dismiss();
                } else {


                    dialogtype = filterbuilder.create();

                    dialogtype.show();
                    if (anzahl < 1) {
                        dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.greytrans, getTheme()));
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorGray, getTheme()));
                    } else {
                        dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.VISIBLE);
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent, getTheme()));
                        dialogtype.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorWhite, getTheme()));
                    }

                    dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Boolean wantToCloseDialog = false;
                            //Do stuff, possibly set wantToCloseDialog to true then...
                            if (wantToCloseDialog) {
                                dialogtype.dismiss();
                            } else {
                                //savefilter(selectcountry);

                                dialogtype.getButton(AlertDialog.BUTTON_NEUTRAL).setVisibility(View.GONE);

                                selecteventtype.clear();
                                anzahl = 0;
                                adaptertype.notifyDataSetChanged();


                            }
                            //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
                        }
                    });
                }
            }


        }
    }

    private void savefilter(final List<String> selecteditem) {
        final Map<String, Object> agentupdate = new HashMap<>();
        final boolean filterexist;
        if (selecteditem.size() > 0) {
            filterexist = true;
            agentupdate.put("/country", selecteditem);
        } else {
            selecteditem.clear();
            filterexist = false;
            agentupdate.put("/country", null);
        }
        mDatabase.child(user.getUid()).child("filter").updateChildren(agentupdate).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    //finish();
                    Log.w("SAVED", this.getClass().getSimpleName() + "Countrys");
                    new AgentData("filterdatacountry", selecteditem);
                    new AgentData("filtercountry", filterexist);
                    new AgentData("filter", filterexist);
                } else if (task.isCanceled()) {

                    Log.w("ERROR ", this.getClass().getSimpleName() + " 505 Login abgebrochen");

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {

                Log.w("ERROR ", this.getClass().getSimpleName() + " 506 Login error " + e.getMessage());
                //  importfromscreen.setVisibility(View.VISIBLE);

            }
        });
    }

    private void savefiltertype(final List<String> selecteditem) {
        final Map<String, Object> agentupdate = new HashMap<>();
        final boolean filterexist;
        if (selecteditem.size() > 0) {
            filterexist = true;
            agentupdate.put("/type", selecteditem);
        } else {
            selecteditem.clear();
            filterexist = false;
            agentupdate.put("/type", null);
        }
        mDatabase.child(user.getUid()).child("filter").updateChildren(agentupdate).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    //finish();
                    Log.w("SAVED", this.getClass().getSimpleName() + "Countrys");
                    new AgentData("filterdatatype", selecteditem);
                    new AgentData("filtertype", filterexist);
                    new AgentData("filter", filterexist);
                } else if (task.isCanceled()) {

                    Log.w("ERROR ", this.getClass().getSimpleName() + " 505 Login abgebrochen");

                }
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {

                Log.w("ERROR ", this.getClass().getSimpleName() + " 506 Login error " + e.getMessage());
                //  importfromscreen.setVisibility(View.VISIBLE);

            }
        });
    }

    private class CustomTask extends AsyncTask<Void, Void, Void> {
        FlowLayout flow;
        LayoutInflater infl;
        String cityof;
        String imageof;
        ImageView eventlogov;
        View theview;


        public CustomTask(FlowLayout myRootfriends, View view) {
            flow = myRootfriends;
            theview = view;
        }

        protected Void doInBackground(Void... param) {

            Log.i("Background -->", "jep");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    flow.addView(theview);
                }
            });


            return null;
        }

        protected void onPostExecute(Void param) {
            Log.i("Finisched -->", "jep");
        }
    }

}