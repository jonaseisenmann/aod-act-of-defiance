AOD is a game extension for Ingress. Weekly and monthly tasks await you, or duel with your friends or agents around the world.

#GAMEPLAY
Join a challenge, read your starting values and you're ready to go.

#DUELL / PvP
Create a duel and invite other agents to compete against you.
Choose your duel from different categories, set the duel duration.

#SPECIAL EVENTS
Special days or weekends can be a global challenge.
Score for your faction or work together on a mission.